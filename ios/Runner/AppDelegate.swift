import UIKit
import Flutter
import GoogleMaps

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
  /*   To use Add Google Maps, with API key
       Release     AIzaSyDv-xF70dOaz9mpNcV8_USJrrTL-uJMxiE
       Debug       AIzaSyBT8U97Fhlp6avWcjE8ETzx6E5uLCPd_GI
  */

    GMSServices.provideAPIKey("AIzaSyBT8U97Fhlp6avWcjE8ETzx6E5uLCPd_GI")
    if #available(iOS 10.0, *) {
        UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
      }
    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
