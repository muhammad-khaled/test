import 'dart:async';
import 'dart:convert';
import 'dart:io';
import '../../provider/productsInCartProvider.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import '../Controls.dart';

import '../dataControl/dataModule.dart';
import 'package:path_provider/path_provider.dart';
import 'package:dio/dio.dart';
import 'package:path/path.dart';
import '../globals.dart' as globals;

Future<ResponseApi> getUserOrder(String lang, String token) async {
  token= "Bearer "+ token;
  print(token);
  String Url = SystemControls.baseURL+lang+ SystemControls.baseURL2+
      "getOrders?accountId="+SystemControls.AccountId;
  Map<String, String> headerParams = {
    HttpHeaders.authorizationHeader: token,
      "Accept": 'application/json',
  };
  print(Url);
  final response =
  await Dio().get(Url, options: Options(
    headers: headerParams,
  ));

  print("(response.data)");
  print((response.data));
  print("(response.data)");
  return ResponseApi.fromJson((response.data));
}
Future<ResponseApi> getUserOrderById(String lang, String token,
    String orderId) async {
  token= "Bearer "+ token;
  print(token);
  String Url = SystemControls.baseURL+lang+ SystemControls.baseURL2+
      "getOrder?accountId="+SystemControls.AccountId+
      "&orderId="+orderId;

  Map<String, String> headerParams = {
    HttpHeaders.authorizationHeader: token,
    "Accept": 'application/json',
  };
  final response =
  await Dio().get(Url, options: Options(
    headers: headerParams,
  ));

  return ResponseApi.fromJson((response.data));
}
Future<ResponseApi> cancelOrder(String lang, String token,
    String orderId) async {
  token= "Bearer "+ token;
  print(token);
  String Url = SystemControls.baseURL+lang+ SystemControls.baseURL2+
      "cancelOrder?accountId="+SystemControls.AccountId+
      "&orderId="+orderId;
  Map<String, String> headerParams = {
    HttpHeaders.authorizationHeader: token,
    "Accept": 'application/json',
  };
  final response =
  await Dio().post(Url, options: Options(
    headers: headerParams,
  ));
  print((response.data));
  return ResponseApi.fromJson((response.data));
}

Future<ResponseApi> CreateOrder(String lang, String token, String address,
    String notes, List <dynamic> items, String gps,
    String follow_delivery_time, int payment_method,
    File imgCart,String ImgName,
    String itemsNotes, int selectedAreaId,BuildContext context) async {
  token= "Bearer "+ token;
  print(token);
  String Url = SystemControls.baseURL+lang+ SystemControls.baseURL2+"postOrder";
  var cart = Provider.of<CartStateProvider>(context,listen: false);
  var dio = new Dio();

  dio.options.baseUrl = Url;
  dio.options.connectTimeout = 50000; //5s
  dio.options.receiveTimeout = 50000;

  Map<String, String> headerParams = {
    HttpHeaders.authorizationHeader: token,
    "Content-Type": 'application/json',
    "Accept": 'application/json',
  };
  dio.options.headers = headerParams;

  FormData body = FormData.fromMap({
    "accountId" : SystemControls.AccountId,
    "delivery_address" : address,
    "orders_notes_for_items": itemsNotes,
    "notes": notes,
    "items": items.length !=0 ? json.encode(items):null,
    "gps" : gps,
    "orders_follow_delivery_time" : follow_delivery_time,
    //"payment_method" : payment_method,
    "payments_id": payment_method,
    "areaId": selectedAreaId,
    "coupons_code": cart.couponType!=""? cart.couponController.text:"",
    "image" : imgCart != null ?( imgCart.path!=null&&imgCart.path!=""?await MultipartFile.fromFile(imgCart.path,filename: imgCart.path.split("/").last)
        :ImgName):null
  });
  // if(globals.show_areas != "0") {
  //   body.putIfAbsent("areaId", () => selectedAreaId);
  // }


  print('My order created');
  print(selectedAreaId);
  print('My order created');

  // var bodyJson = json.encode(body);
  // print(bodyJson.toString());
  // print(formData.toString());
  /*var responses1 = await dio.post("",
      data: bodyJson,
      /*options: Options(
        method: 'POST',
      )*/
  );*/
  var responses = await dio.post(Url,options: Options(
    headers: headerParams,
    followRedirects: false,
    validateStatus: (status) {
      return status < 500;
    },
  ),data: body);

  print("Start Dio ...........");
  if(responses.statusCode ==200){
    cart.couponType="";
    cart.couponValue="";
    cart.couponController.clear();
    imgCart = null;
    print(ResponseApi.fromJson(responses.data));
  }
  return ResponseApi.fromJson(responses.data);



}


Future<ResponseApi> AddNewAddressApi(String lang, String token, String address,
    String addressTitle, String gps, String AddressDefault) async {
  token= "Bearer "+ token;
  String Url = SystemControls.baseURL+lang+
      SystemControls.baseURL2+ "storeCutomerAddress";

  Map<String, String> headerParams = {
    HttpHeaders.authorizationHeader: token,
    "Content-Type": 'application/json',
    "Accept": 'application/json',
  };

  Map<String, dynamic> body = {
    "accountId" : SystemControls.AccountId,
    "address_name" : addressTitle,
    "address_title" : address,
    "address_gps" : gps,
    "address_default": AddressDefault,
  };
  var bodyJson = json.encode(body);
  print(bodyJson);
  try{
    final response =
    await Dio().post(Url,data: bodyJson, options: Options(
      headers: headerParams,
    ));
    print((response.data));
    return ResponseApi.fromJson((response.data));
  }on SocketException catch(_){
    return null;
  }
}
Future<ResponseApi> getUserAddressesApi(String lang, String token) async {
  token= "Bearer "+ token;
  String Url = SystemControls.baseURL+lang+SystemControls.baseURL2+
      "getCstomerAddresses?accountId="+SystemControls.AccountId;

  Map<String, String> headerParams = {
    HttpHeaders.authorizationHeader: token,
    "Accept": 'application/json',
  };

  final response =
  await Dio().get(Url, options: Options(
    headers: headerParams,
  ));
  return ResponseApi.fromJson((response.data));
}

Future<ResponseApi> removeAddressApi(String lang, String token,
    int addresses_id) async {
  token= "Bearer "+ token;
  String Url = SystemControls.baseURL+lang+ SystemControls.baseURL2+
      "deleteCutomerAddress";

  Map<String, String> headerParams = {
    HttpHeaders.authorizationHeader: token,
    "Content-Type": 'application/json',
    "Accept": 'application/json',
  };

  Map<String, dynamic> body = {
    "accountId" : SystemControls.AccountId,
    "addresses_id" : addresses_id,
  };
  var bodyJson = json.encode(body);
  print(bodyJson);
  try{
    final response =
    await Dio().post(Url,data: bodyJson, options: Options(
      headers: headerParams,
    ));
    print((response.data));
    return ResponseApi.fromJson((response.data));
  }on SocketException catch(_){
    return null;
  }
}
Future<ResponseApi> editAddressApi(String lang, String token, String address,
    String addressTitle, String gps, String AddressDefault,int addresId) async {
  token= "Bearer "+ token;
  String Url = SystemControls.baseURL+lang+ SystemControls.baseURL2+
      "updateCutomerAddress";

  Map<String, String> headerParams = {
    HttpHeaders.authorizationHeader: token,
    "Content-Type": 'application/json',
    "Accept": 'application/json',
  };

  Map<String, dynamic> body = {
    "accountId" : SystemControls.AccountId,
    "addresses_id": addresId,
    "address_name" : addressTitle,
    "address_title" : address,
    "address_gps" : gps,
    "address_default": AddressDefault,
  };
  var bodyJson = json.encode(body);
  print(bodyJson);
  try{
    final response =
    await Dio().post(Url,data: bodyJson, options: Options(
      headers: headerParams,
    ));
    print((response.data));
    return ResponseApi.fromJson((response.data));
  }on SocketException catch(_){
    return null;
  }
}

class CartMeal{
  int meals_id;
  String meals_price;
  String meals_title;
  String meals_img;
  String meals_desc;
  String type;
  dynamic count;
  int choice_id;
  String choice_text;
  String follow_delivery_time;
  String quantityType;
  String minQuantity;
  String couponValue;

  CartMeal(
      {this.meals_id, this.meals_price, this.meals_title,this.meals_img,
        this.meals_desc,this.count, this.type, this.follow_delivery_time,
        this.choice_id, this.choice_text,this.quantityType,this.minQuantity,couponValue}
      );

  factory CartMeal.fromJson(Map<String, dynamic> json){
    return CartMeal(
      meals_id: json['meals_id'],
      meals_price: json['meals_price'],
      meals_title: json['meals_title'],
      meals_img: json['meals_img'],
      meals_desc: json['meals_desc'],
      count: json['count'],
      type: json['type'],
      follow_delivery_time : json['follow_delivery_time'],
      choice_id: json['choice'],
      choice_text: json['choice_text'],
      quantityType: json['products_type'],
      minQuantity: json['products_lowest_weight_available'],
      couponValue: json['coupons_value'],
    );
  }

  Map<String, dynamic> toJson(){
    return{
      'meals_id': meals_id,
      'meals_price': meals_price,
      'meals_title': meals_title,
      'meals_img': meals_img,
      'meals_desc': meals_desc,
      'count': count,
      'type': type,
      'follow_delivery_time': follow_delivery_time,
      'choice': choice_id,
      'choice_text' : choice_text,
      'products_type' : quantityType,
      'products_lowest_weight_available' : minQuantity,
      'coupons_value' : couponValue,
    };
  }
}

class CartList{
  List<dynamic> Cmeals = <dynamic>[];
  CartList({this.Cmeals});
  factory CartList.fromJson(Map<String, dynamic> json){
    return CartList(
      Cmeals: json['Cmeals'],
    );
  }
  Map<String, dynamic> toJson(){
    return{
      'Cmeals': Cmeals,
    };
  }
}

Future<String> _localPath() async{
  final direction = await getApplicationDocumentsDirectory();

  return direction.path;
}

Future<File> _localFile() async{
  final path = await _localPath();

  return File('$path/CartList.json');
}

Future<CartList> readCart() async{
  try{
    final file = await _localFile();
    String get= await file.readAsString();
    //print("Cart "+get);

    return CartList.fromJson(json.decode(get));
  }
  catch(e){
    print(e);
    return null;
  }
}

Future<File> writeToCartList(dynamic meal) async {
  print("Update lang local to " + meal.toString());
  final file = await _localFile();
  // Write the file
  return file.writeAsString('$meal');
}

clear_cart_file()async{
  writeToCartList("");
}

write_to_file(dynamic S)async{

  var LocalList = await readCart();

  List <dynamic> cc = <dynamic>[];

  //cc = LocalList.Cmeals;
  var bodyjson;
  if(LocalList  != null){
    print(LocalList.Cmeals.length);
    cc = LocalList.Cmeals;

    //LocalList.Cmeals.add(CartMeal.fromJson(S));

    print(LocalList.Cmeals.length);

    //bodyjson = LocalList.toJson();

  }

  cc.add(S);
  Map<String, dynamic> list ={
    'Cmeals': cc,
  };
  bodyjson = json.encode(list);

  print("json: " + bodyjson.toString());

  writeToCartList(bodyjson);

}
UpdateList(List <dynamic> cc)async{
  Map<String, dynamic> list ={
    'Cmeals': cc,
  };
  var bodyjson = json.encode(list);

  print("json: " + bodyjson.toString());
  //dynamic obj = AddMeal.toJson();

  writeToCartList(bodyjson);

}

Future<CartList> read_from_file()async{
  var list = await readCart();
  return list;
}