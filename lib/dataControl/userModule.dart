import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import '../Controls.dart';
import '../dataControl/dataModule.dart';

import '../globals.dart' as globals;

Future<ResponseApi> getUserNotification(String lang, String token) async {
  token= "Bearer "+ token;
  print(token);
  String Url = SystemControls.baseURL+lang+SystemControls.baseURL2+
      "getCustomerNotification?accountId="+SystemControls.AccountId;
  Map<String, String> headerParams = {
    HttpHeaders.authorizationHeader: token,
    "Accept": 'application/json',
  };
  print(Url);
  final response =  await Dio().get(Url, options: Options(
    headers: headerParams,
  ));
  print('Notification');
  print((response.data));
  return ResponseApi.fromJson((response.data));
}

Future<ResponseApi> checkPhoneNumber(String lang,
    String phone, String countryCode) async {

  String Url = SystemControls.baseURL+lang+SystemControls.baseURL2+
      "checkPhoneNumber?accountId="+
      SystemControls.AccountId+"&phone="+phone+
      "&customers_country_code="+ countryCode;

  Map<String, String> headerParams = {
    //"Content-Type": 'application/x-www-form-urlencoded',
    "Accept": 'application/json',
  };
  print(Url);

  try{
    final response =
    await Dio().get(Url, options: Options(
      headers: headerParams,
    ));

    print((response.data));
    return ResponseApi.fromJson((response.data));

  }on SocketException catch(_){
    return null;
  }


}

Future<ResponseApi> opinionApi(String lang, String name, String phone,
    String complain, int orderId) async {
  String Url = SystemControls.baseURL+lang+SystemControls.baseURL2+
      "postContact";
  Map<String, String> headerParams = {
    "Accept": 'application/json',
  };
//TODO get branch_id from initial data
  Map body = {
    "accountId" : SystemControls.AccountId,
    "contacts_name" : name,
    "contacts_phone" : phone,
    "contacts_message" : complain,
    "orderId" :orderId==null?"":orderId.toString()
  };
  var bodyjson = json.encode(body);

  try{
    final response =
    await Dio().post(Url,data: body, options: Options(
      headers: headerParams,
    ));

    print((response.data));
    return ResponseApi.fromJson((response.data));
  }on SocketException catch(_){
    return null;
  }

}

Future<ResponseApi> registerApi(String lang, String name, String email,
    String phone, String password,String dateOfBirth, String gender,
    String countryCode) async {

  String Url = SystemControls.baseURL+lang+SystemControls.baseURL2+
      "register";

  Map<String, String> headerParams = {
    //"Content-Type": 'application/x-www-form-urlencoded',
    "Accept": 'application/json',
  };

  Map body = {
    "accountId" : SystemControls.AccountId,
    "customers_name" : name,
    "customers_email" : email,
    "customers_phone" : phone,
    "customers_country_code" : countryCode,
    "customers_birthday" : dateOfBirth,
    "customers_gender" : gender,
    "password" : password,
    "device_token" : globals.device_token,
    "phone_verified" : "1",
  };
  //var bodyjson = json.encode(body);
  print(body.toString());
  try{
    final response =
    await Dio().post(Url,data: body, options: Options(
      headers: headerParams,
    ));

    print((response.data));
    return ResponseApi.fromJson((response.data));
  }on SocketException catch(_){
    return null;
  }
}

Future<ResponseApi> loginApi(String lang, String phone, String password,
    String countryCode) async {
  String Url = SystemControls.baseURL+lang+SystemControls.baseURL2+
      "login";
  Map<String, String> headerParams = {
    "Accept": 'application/json',
  };

  Map body = {
    "accountId" : SystemControls.AccountId,
    "phone" : phone,
    "customers_country_code" : countryCode,
    "password" : password,
    "device_token" : globals.device_token
  };
  print(body);
  try{
    final response =    await Dio().post(Url,data: body, options: Options(
      headers: headerParams,
    ));


    print((response.data));

    return ResponseApi.fromJson((response.data));

  }on SocketException catch(_){
    return null;
  }
}

Future<ResponseApi> forgotPasswordApi(String lang, String email) async {
  String Url = SystemControls.baseURL+lang+SystemControls.baseURL2+
      "forgotPassword";
  Map<String, String> headerParams = {
    "Accept": 'application/json',
  };

  Map body = {
    "accountId" : SystemControls.AccountId,
    "email" : email,
  };

  try{
    final response =    await Dio().post(Url,data: body, options: Options(
      headers: headerParams,
    ));
    return ResponseApi.fromJson((response.data));
  }on SocketException catch(_){
    return null;
  }
}
Future<ResponseApi> mobileForgotPasswordApi(String lang,
    String phone, String password,String countryCode) async {

  String Url = SystemControls.baseURL+lang+SystemControls.baseURL2+
      "forgotPasswordMobile";

  Map<String, String> headerParams = {
    "Accept": 'application/json',
    //HttpHeaders.authorizationHeader: token,
  };

  Map body = {
    "accountId" : SystemControls.AccountId,
    "phone" : phone,
    "customers_country_code" : countryCode,
    "password" : password,
  };

  try{
    final response =    await Dio().post(Url,data: body, options: Options(
      headers: headerParams,
    ));

    print((response.data));
    return ResponseApi.fromJson((response.data));
  }on SocketException catch(_){
    return null;
  }

}

Future<ResponseApi> updateUserApi(String token, String lang, String name, String email,
    String phone,String dateOfBirth, String gender,String countryCode,bool verified) async {

  token= "Bearer "+ token;
  String Url = SystemControls.baseURL+lang+SystemControls.baseURL2+
      "customerUpdate";

  Map<String, String> headerParams = {
    //"Content-Type": 'application/x-www-form-urlencoded',
    "Accept": 'application/json',
    HttpHeaders.authorizationHeader: token,
  };

  Map body = {
    "accountId" : SystemControls.AccountId,
    "customers_name" : name,
    "customers_email" : email,
    "customers_phone" : phone,
    "customers_country_code" : countryCode,
    "customers_birthday" : dateOfBirth,
    "customers_gender" : gender,
    "send_verification_code" : verified == false?"0":"1",
    "phone_verified" : "1",
  };
  print(body);

  try{
    final response =    await Dio().post(Url,data: body, options: Options(
      headers: headerParams,
    ));
    print("ggggggggggggggggg");
    print((response.data["data"]["customer"]["email_verified"]));
    print((response.data["data"]["email_verified"]));
    print("ggggggggggggggggg");
    SystemControls().storeDataToPreferences("email_verified", response.data["data"]["customer"]["email_verified"]);
    return ResponseApi.fromJson((response.  data));

  }on SocketException catch(_){
    return null;
  }


}

Future<ResponseApi> changePasswordApi(String token, String lang,
    String newPassword, String oldPassword) async {

  token= "Bearer "+ token;
  String Url = SystemControls.baseURL+lang+SystemControls.baseURL2+
      "changePassword";

  Map<String, String> headerParams = {
    "Accept": 'application/json',
    HttpHeaders.authorizationHeader: token,
  };

  Map body = {
    "accountId" : SystemControls.AccountId,
    "old_password" : oldPassword,
    "new_password" : newPassword,
  };

  try{
    final response =    await Dio().post(Url,data: body, options: Options(
      headers: headerParams,
    ));

    print(("response.data"));
    print((response.data));
    print(("response.data"));
    return ResponseApi.fromJson((response.data));
  }on SocketException catch(_){
    return null;
  }

}
Future<ResponseApi> logoutApi(String token, String lang) async {

  token= "Bearer "+ token;
  String Url = SystemControls.baseURL+lang+SystemControls.baseURL2+
      "logout";

  Map<String, String> headerParams = {
    "Accept": 'application/json',
    HttpHeaders.authorizationHeader: token,
  };

  Map body = {
    "accountId" : SystemControls.AccountId,
  };

  final response =    await Dio().post(Url,data: body, options: Options(
    headers: headerParams,
  ));

  print((response.data));
  return ResponseApi.fromJson((response.data));

}

class regisResp{
  final customer Customer;

  regisResp({
    this.Customer
  });

  factory regisResp.fromJson(Map<String, dynamic> json){
    return regisResp(
      Customer: json['customer'],
    );
  }
}

class customer{
  final String api_token;
  final String name;
  final String email;
  final String customer_phone;
  final int customer_id;

  customer(
      {this.api_token, this.name,this.email,this.customer_phone, this.customer_id}
      );

  factory customer.fromJson(Map<String, dynamic> json){
    return customer(
      api_token: json['api_token'],
      name: json['name'],
      email: json['email'],
      customer_phone: json['customer_phone'],
      customer_id: json['customer_id'],
    );
  }
}

class loginRes{
  final bool response;
  final String message;
  final dynamic data;

  loginRes(
      {this.response, this.message,this.data}
      );

  factory loginRes.fromJson(Map<String, dynamic> json){
    return loginRes(
      response: json['response'],
      message: json['message'],
      data: json['data'],
    );
  }
}