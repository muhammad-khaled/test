import 'dart:async';
import 'dart:convert';
import 'dart:io';
import '../../locale/share_preferences_con.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../Controls.dart';
import '../main.dart';

Future<ResponseApi> BranchesApi(String lang) async {
  String Url2 = SystemControls.baseURL+lang+
      SystemControls.baseURL2+"branches?accountId="+SystemControls.AccountId;

  print(DateTime.now());
  print(Url2);

  Map<String, String> headerParams = {
    "Accept": 'application/json',
  };
  final response =
  await Dio().get(Url2, options: Options(
    headers: headerParams,
  ));
  if (response.statusCode == 200) {
    //print(response.body);
    return ResponseApi.fromJson((response.data));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

Future<ResponseApi> SearchWithtitleApi(String lang,String title,String token,String pageNum) async {
  token= "Bearer "+ token;
  String Url2 = SystemControls.baseURL+lang+
      SystemControls.baseURL2+"products?accountId="+
      SystemControls.AccountId+"&title="+title+"&page="+pageNum;

  print(DateTime.now());
  print(Url2);

  Map<String, String> headerParams = {
    HttpHeaders.authorizationHeader: token,
    "Accept": 'application/json',
  };
  final response =
  await Dio().get(Url2, options: Options(
    headers: headerParams,
  ));


  print((response.data));
  if (response.statusCode == 200) {
    return ResponseApi.fromJson((response.data));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

Future<ResponseApi> homeItemsGet(String lang,String token,String pageNum) async {
  token= "Bearer "+ token;
  String Url2 = SystemControls.baseURL+lang+
      SystemControls.baseURL2+"products?accountId="+
      SystemControls.AccountId+"&home=1"+"&page="+pageNum;

  print(Url2);

  Map<String, String> headerParams = {
    HttpHeaders.authorizationHeader: token,
    "Accept": 'application/json',
  };
  final response =
  await Dio().get(Url2, options: Options(
    headers: headerParams,
  ));


  if (response.statusCode == 200) {


    return ResponseApi.fromJson((response.data));


  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

Future<ResponseApi> CategoryListApi(String lang,int cateId,String token,String pageNum) async {
  token= "Bearer "+ token;
  String Url2 = SystemControls.baseURL+lang+
      SystemControls.baseURL2+"products?accountId="+
      SystemControls.AccountId+"&cateId="+cateId.toString()+"&page="+pageNum;

  print(DateTime.now());
  print(Url2);

  Map<String, String> headerParams = {
    HttpHeaders.authorizationHeader: token,
    "Accept": 'application/json',
  };
  final response =
  await Dio().get(Url2, options: Options(
    headers: headerParams,
  ));

  if (response.statusCode == 200) {
    return ResponseApi.fromJson((response.data));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

Future<ResponseApi> getOffersListApi(String lang) async {
  String Url2 = SystemControls.baseURL+lang+
      SystemControls.baseURL2+"offers?accountId="+
      SystemControls.AccountId;

  print(DateTime.now());
  print(Url2);

  Map<String, String> headerParams = {
    "Accept": 'application/json',
  };
  final response =
  await Dio().get(Url2, options: Options(
    headers: headerParams,
  ));

  if (response.statusCode == 200) {
    return ResponseApi.fromJson((response.data));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

Future<ResponseApi> getSearchCategories(String lang) async {
  String Url2 = SystemControls.baseURL+lang+
      SystemControls.baseURL2+"categoriesTreeList?accountId="+
      SystemControls.AccountId;

  print(DateTime.now());
  print(Url2);

  Map<String, String> headerParams = {
    "Accept": 'application/json',
  };
  final response =
  await Dio().get(Url2, options: Options(
    headers: headerParams,
  ));

  if (response.statusCode == 200) {
    return ResponseApi.fromJson((response.data));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

class categoryListM{
  List<dynamic> meals;
  categoryListM({this.meals});

  factory categoryListM.fromJson(Map<String, dynamic> json){
    return categoryListM(
      meals: json['meals'],
    );
  }
}

Future<ResponseApi> fetchList(String lang,String token) async {
  token= "Bearer "+ token;
  String Url2 = SystemControls.baseURL+lang+SystemControls.baseURL2+
      "home?accountId="+SystemControls.AccountId;

  print(DateTime.now());
  print(Url2);

  Map<String, String> headerParams = {
    HttpHeaders.authorizationHeader: token,
    "Accept": 'application/json',
  };
  final response =
  await Dio().get(Url2, options: Options(
    headers: headerParams,
  ));

  print((response.data));
  if (response.statusCode == 200) {
    return ResponseApi.fromJson((response.data));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

Future<ResponseApi> InitialApi(String lang,BuildContext context) async {
  String Url2 = SystemControls.baseURL+lang+ SystemControls.baseURL2+
      "initial?accountId="+SystemControls.AccountId;
  final SystemControls storg = SystemControls();

  print(DateTime.now());
  print(Url2);

  Map<String, String> headerParams = {
    "Accept": 'application/json',
  };
  final response =
  await Dio().get(Url2, options: Options(
    headers: headerParams,
  ));
  if (response.statusCode == 200) {
    ResponseApi  responseApi= ResponseApi.fromJson((response.data));
   // await SystemControls().storeDataToPreferences("lang", responseApi.data["setting"]["settings_default_language"]);
    print(responseApi.data["setting"]["settings_default_language"]);

    SharedPreferences sharedPreferencesHasRegistration =
    await   SharedPreferences.getInstance();
    sharedPreferencesHasRegistration.setBool('apHasRegistration', responseApi.data["hasRegistration"]);

    SharedPreferences sharedPreferencesAppInfo =
    await   SharedPreferences.getInstance();
    sharedPreferencesAppInfo.setString('appInfo', jsonEncode(responseApi.data["info"]));

    SharedPreferences accountsShowAdvertisements =
    await   SharedPreferences.getInstance();
    accountsShowAdvertisements.setString('accountsShowAdvertisements', responseApi.data["account"]["accounts_show_advertisements"]);
    SharedPreferences accounts_show_coupons =
    await   SharedPreferences.getInstance();
    accounts_show_coupons.setString('accounts_show_coupons', responseApi.data["account"]["accounts_show_coupons"]);




    if(await storg.getApiLanguage() == false){
      if(responseApi.data["setting"]["settings_default_language"]=='en'){
        EasyLocalization.of(context).setLocale(Locale('en', 'EN'));
      await  storg.storeDataToPreferences('lang', responseApi.data["setting"]["settings_default_language"]);
        SharedPreferences sharedPreferences =
        await   SharedPreferences.getInstance();
        print(await SharePreferenceData().getApiLanguage());

        sharedPreferences.setBool('ApiLanguage', true);
        print("ddddffffddddffff");
        print(await storg.getLanguageCode());
        print(await storg.getApiLanguage());
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>MyApp()));
      }else{
        EasyLocalization.of(context).setLocale(Locale('ar', 'EG'));
        await storg.storeDataToPreferences('lang', responseApi.data["setting"]["settings_default_language"]);
        SharedPreferences sharedPreferences =
        await   SharedPreferences.getInstance();
        sharedPreferences.setBool('ApiLanguage', true);
        print("ddddffffddddffff");
        print(await storg.getLanguageCode());
        print(await storg.getApiLanguage());

        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>MyApp()));

      }



    }
   //  SharedPreferences sharedPreferencesGetEndTime =
   //  await SharedPreferences.getInstance();
   //  sharedPreferencesGetEndTime.setString("lang", responseApi.data["setting"]["settings_default_language"]);

    //print(response.body);
    return ResponseApi.fromJson((response.data));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

Future<ResponseProductApi> getMealById(String lang,String mealId) async {
  String Url2 = SystemControls.baseURL+lang+SystemControls.baseURL2+
      "product/"+mealId+"?accountId="+SystemControls.AccountId;
  print('data time data');
  print(DateTime.now());
  print(Url2);

  Map<String, String> headerParams = {
    "Accept": 'application/json',
  };
  final response =
  await Dio().get(Url2, options: Options(
    headers: headerParams,
  ));;
  print('\n\n\n get by id');
  print((response.data));
  if (response.statusCode == 200) {

    return ResponseProductApi.fromJson((response.data));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

class ResponseApi{
  final int status;
  final String message;
  final List<dynamic> errors;
  final dynamic data;

  ResponseApi(
      {this.status, this.message, this.errors,this.data}
      );

  factory ResponseApi.fromJson(Map<String, dynamic> json){
    return ResponseApi(
      status: json['status'],
      message: json['message'],
      errors: json['errors'],
      data: json['data'],
    );
  }
}



class ResponseProductApi{
  final int status;
  final String message;
  final List<dynamic> errors;
  final dynamic data;

  ResponseProductApi(
      {this.status, this.message, this.errors,this.data}
      );

  factory ResponseProductApi.fromJson(Map<String, dynamic> json){
    return ResponseProductApi(
      status: json['status'],
      message: json['message'],
      errors: json['errors'],
      data: json['data'],
    );
  }
}

class menuList{
  List<dynamic> homeMeals;
  List<dynamic> categories;
  List<dynamic> offers;
  menuList({
    this.homeMeals, this.categories,this.offers}
      );
  factory menuList.fromJson(Map<String, dynamic> json){
    return menuList(
      homeMeals: json['homeMeals'],
      categories: json['categories'],
      offers: json['offers'],
    );
  }
}

class mealDe{
  dynamic meal;
  mealDe({
  this.meal
  });
  factory mealDe.fromJson(Map<String, dynamic> json){
    return mealDe(
      meal: json['meal'],
    );
  }
}

class Meal{
  final int meals_id;
  final String meals_price;
  final String meals_title;
  final String meals_img;
  final String meals_desc;
  final Category category;
  List<dynamic> suggestions;

  Meal(
      {this.meals_id, this.meals_price, this.meals_title,this.meals_img,
        this.category, this.meals_desc,this.suggestions}
      );

  factory Meal.fromJson(Map<String, dynamic> json){
    return Meal(
      meals_id: json['meals_id'],
      meals_price: json['meals_price'],
      meals_title: json['meals_title'],
      meals_img: json['meals_img'],
      category: json['category'],
      meals_desc: json['meals_desc'],
      suggestions: json['suggestions'],

    );
  }
}

class Offer{
  final int offers_id;
  final int FK_menu_branches_id;
  final String offers_img_mobile;
  final String offers_img_tab;
  final String offers_price;
  final String offers_status;
  final int offers_position;
  final String offers_title;
  final String offers_desc;

  Offer(
      {this.offers_id, this.FK_menu_branches_id, this.offers_img_mobile,
        this.offers_img_tab, this.offers_price, this.offers_status,
        this.offers_position, this.offers_title, this.offers_desc}
      );

  factory Offer.fromJson(Map<String, dynamic> json){
    return Offer(
      offers_id: json['offers_id'],
      FK_menu_branches_id: json['FK_menu_branches_id'],
      offers_img_mobile: json['offers_img_mobile'],
      offers_img_tab: json['offers_img_tab'],
      offers_price: json['offers_price'],
      offers_status: json['offers_status'],
      offers_position: json['offers_position'],
      offers_title: json['offers_title'],
      offers_desc: json['offers_desc'],

    );
  }
}

class Category{
  final int categories_id;
  final String categories_img;
  final String categories_title;

  Category(
      {this.categories_id, this.categories_img,this.categories_title}
      );

  factory Category.fromJson(Map<String, dynamic> json){
    return Category(
      categories_id: json['categories_id'],
      categories_img: json['categories_img'],
      categories_title: json['categories_title'],

    );
  }
}

class Initial{
  final dynamic account;
  final dynamic info;
  final dynamic setting;
  List<dynamic> langs;
  final String defaultLang;
  final bool hasRegistration;

  Initial({
    this.account, this.info, this.setting, this.langs, this.defaultLang,
    this.hasRegistration
  });

  factory Initial.fromJson(Map<dynamic, dynamic> json){
    return Initial(
      account: json['account'],
      info: json['info'],
      setting: json['setting'],
      langs: json['langs'],
      defaultLang: json['defaultLang'],
      hasRegistration: json['hasRegistration'],

    );
  }
}

class RestaurantInfo{
  final int infos_id;
  final int FK_accounts_id;
  final String infos_phone1;
  final String infos_phone2;
  final String infos_phone3;

  final String infos_gps;
  final String infos_facebook;
  final String infos_twitter;
  final String infos_youtube;
  final String infos_instagram;
  final String infos_snapchat;
  final String infos_whatsapp;
  final String infos_foursquare;
  final String infos_about;

  RestaurantInfo({
    this.infos_id, this.FK_accounts_id, this.infos_phone1, this.infos_phone2,
    this.infos_phone3, this.infos_gps, this.infos_facebook, this.infos_twitter,
    this.infos_youtube, this.infos_instagram, this.infos_snapchat, this.infos_whatsapp,
    this.infos_foursquare, this.infos_about
});

  factory RestaurantInfo.fromJson(Map<String, dynamic> json){
    return RestaurantInfo(
      infos_id: json['infos_id'],
      FK_accounts_id: json['FK_accounts_id'],
      infos_phone1: json['infos_phone1'],
      infos_phone2: json['infos_phone2'],
      infos_phone3: json['infos_phone3'],
      infos_gps: json['infos_gps'],
      infos_facebook: json['infos_facebook'],
      infos_twitter: json['infos_twitter'],
      infos_youtube: json['infos_youtube'],
      infos_instagram: json['infos_instagram'],
      infos_snapchat: json['infos_snapchat'],
      infos_whatsapp: json['infos_whatsapp'],
      infos_foursquare: json['infos_foursquare'],
      infos_about: json['infos_about'],
    );
  }

}

class Account{
  final int accounts_id;
  final String accounts_name;
  final String accounts_img_app;
  final String accounts_show_branches;
  final String accounts_show_menu_branches;
  final String accounts_status;
  final int accounts_position;

  Account(
      {this.accounts_id, this.accounts_name,this.accounts_img_app,
        this.accounts_show_branches, this.accounts_show_menu_branches, this.accounts_status,
        this.accounts_position
      }
      );

  factory Account.fromJson(Map<String, dynamic> json){
    return Account(
      accounts_id: json['accounts_id'],
      accounts_name: json['accounts_name'],
      accounts_img_app: json['accounts_img_app'],
      accounts_show_branches: json['accounts_show_branches'],
      accounts_show_menu_branches: json['accounts_show_menu_branches'],
      accounts_status: json['accounts_status'],
      accounts_position: json['accounts_position'],


    );
  }
}

class Setting{
  final int settings_id;
  final String settings_default_language;
  final int settings_pagination_count;
  final String accounts_show_branches;
  final String accounts_show_menu_branches;
  final String settings_currency;

  Setting(
      {this.settings_id, this.settings_default_language,this.settings_pagination_count,
        this.accounts_show_branches, this.accounts_show_menu_branches,
        this.settings_currency,
      }
      );

  factory Setting.fromJson(Map<String, dynamic> json){
    return Setting(
      settings_id: json['settings_id'],
      settings_default_language: json['settings_default_language'],
      settings_pagination_count: json['settings_pagination_count'],
      accounts_show_branches: json['accounts_show_branches'],
      accounts_show_menu_branches: json['accounts_show_menu_branches'],
      settings_currency: json['settings_currency'],
    );
  }
}

class Langs{
  final int languages_id;
  final String languages_name;
  final String languages_code;
  final String languages_dir;
  final String languages_img;
  final String languages_status;
  final int languages_order;
  final int languages_position;

  Langs(
      {this.languages_id, this.languages_name,this.languages_code,
        this.languages_dir, this.languages_img, this.languages_status,
        this.languages_order, this.languages_position
      }
      );

  factory Langs.fromJson(Map<String, dynamic> json){
    return Langs(
      languages_id: json['languages_id'],
      languages_name: json['languages_name'],
      languages_code: json['languages_code'],
      languages_dir: json['languages_dir'],
      languages_img: json['languages_img'],
      languages_status: json['languages_status'],
      languages_order: json['languages_order'],
      languages_position: json['languages_position'],
    );
  }
}
