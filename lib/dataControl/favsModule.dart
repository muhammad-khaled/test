import 'dart:async';
import 'dart:convert';
import 'dart:io';
import '../../provider/Drawer/DrawerProvider.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import '../Controls.dart';

import '../dataControl/dataModule.dart';

Future<ResponseApi> getFavsList(String lang, String token) async {
  token= "Bearer "+ token;
  String Url = SystemControls.baseURL+lang+SystemControls.baseURL2+
      "customerFavs?accountId="+SystemControls.AccountId;

  Map<String, String> headerParams = {
    HttpHeaders.authorizationHeader: token,
    "Accept": 'application/json',
  };

  final response =
  await Dio().get(Url, options: Options(
    headers: headerParams,
  ));

  if (response.statusCode == 200) {
    return ResponseApi.fromJson((response.data));
  }
  else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

Future<ResponseApi> updateFavsItem(String lang, String token, int mealId,BuildContext context) async {
  token= "Bearer "+ token;
  print(token);
  String Url = SystemControls.baseURL+lang+SystemControls.baseURL2+
      "updateFavs";

  Map<String, String> headerParams = {
    HttpHeaders.authorizationHeader: token,
    "Accept": 'application/json',
  };

  Map body = {
    "accountId" : SystemControls.AccountId,
    "productId" : mealId.toString()
  };
  final response =
  await Dio().post(Url,data: body, options: Options(
    headers: headerParams,
  ));
  Provider.of<DrawerProvider>(context,listen: false).fetchFavoriteProduct(context);

  return ResponseApi.fromJson((response.data));
}

class favsList{
  List<dynamic> favs;
  favsList({
    this.favs}
      );
  factory favsList.fromJson(Map<String, dynamic> json){
    return favsList(
      favs: json['favs'],
    );
  }
}

class addRes{
  final bool response;
  final String msg;
  addRes({
    this.response, this.msg}
      );
  factory addRes.fromJson(Map<String, dynamic> json){
    return addRes(
      response: json['response'],
      msg: json['msg'],
    );
  }
}