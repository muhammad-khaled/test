
import '../../provider/HomeProvider/SearchProvider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import '../provider/DiscountProvider.dart';
import '../provider/NotificaionProvider.dart';
import '../provider/ProductProvider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'Controls.dart';
import 'package:sizer/sizer.dart';

import 'package:sizer/sizer.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'locale/HmoeMethods.dart';
import 'provider/Drawer/DrawerProvider.dart';
import 'provider/HomeProvider/HomeProvider.dart';
import 'provider/ProviderTheamTwo/DisplayCategoryTheamTwo.dart';
import 'screens/splash.dart';
import 'provider/productsInCartProvider.dart';

import 'package:geolocator/geolocator.dart';
import 'services/pushNotificationServices.dart';
import 'globals.dart' as globals;

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  print('Handling a background message ${message.messageId}');
}

void main()async{
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await Firebase.initializeApp();

  // Set the background messaging handler early on, as a named top-level function
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);


 await Firebase.initializeApp();
  runApp(EasyLocalization(
      supportedLocales: [
        Locale('ar', 'EG'),
      Locale('en', 'EN'),



      ],
      path: 'assets/langs',
      saveLocale: true,


      child: MyApp()));
}

class MyApp extends StatelessWidget{
  //String lang = 'test';
  @override
  Widget build(BuildContext context) {
    Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high).then(
            (Position position){
          globals.latitude= position.latitude;
          globals.longitude = position.longitude;
          print("Position .... main " + position.toString());
        });
    return MultiProvider(providers: [
      ChangeNotifierProvider(create: (context)=>HomeProvider()),
      ChangeNotifierProvider(create: (context)=>DrawerProvider()),
      ChangeNotifierProvider(create: (context)=>SearchProvider()),
      ChangeNotifierProvider(create: (context)=>ProductProvider()),
      ChangeNotifierProvider(create: (context)=>CartStateProvider()),
      ChangeNotifierProvider(create: (context)=>NotificationProvider()),
      ChangeNotifierProvider(create: (context)=>DisplayCategoryTheamTwoProvider()),

      ChangeNotifierProvider(create: (context)=>DiscountProvider()),
    ],
      child: Sizer(
          builder: (context, orientation, deviceType) {
            return MaterialApp(
              debugShowCheckedModeBanner: false,

              localizationsDelegates: context.localizationDelegates,
              supportedLocales: context.supportedLocales,
              locale: context.locale,

              theme: ThemeData(
                appBarTheme: AppBarTheme(
                  color: Colors.white,
                  iconTheme: IconThemeData(
                    color: Colors.black,

                  ),
                  titleTextStyle: TextStyle(color: Colors.black,fontSize: 12.sp,fontWeight: FontWeight.bold)

                ),
                  primaryColor: Colors.white//SystemControls.MainColor,
              ),
              home: splash(EasyLocalization.of(context).currentLocale.languageCode),
            );
          }
      )

    );
    // return MaterialApp(
    //   debugShowCheckedModeBanner: false,
    //
    //   localizationsDelegates: [
    //     AppLocalizations.delegate,
    //     //GlobalMaterialLocalizations.delegate,
    //     //GlobalWidgetsLocalizations.delegate,
    //   ],
    //   theme: ThemeData(
    //       primaryColor: Colors.white//SystemControls.MainColor,
    //   ),
    //   home: splash(),
    // );
  }
}
