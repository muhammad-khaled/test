import 'package:flutter/material.dart';
import '../Controls.dart';
import '../widgets/SearchPainter.dart';
import 'package:flutter/painting.dart';
import 'package:easy_localization/easy_localization.dart';

import '../globals.dart' as globals;

class SearchBar extends StatefulWidget implements PreferredSizeWidget {
  SearchBar({
    Key key,
    @required this.onCancelSearch,
    @required this.onSearchQuerySubmit,
  }) : super(key: key);

  final VoidCallback onCancelSearch;
  //final Function(String) onSearchQueryChanged;
  final Function(String) onSearchQuerySubmit;

  @override
  Size get preferredSize => Size.fromHeight(56.0);

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> with SingleTickerProviderStateMixin {
  String searchQuery = '';
  TextEditingController _searchFieldController = TextEditingController();

  clearSearchQuery() {
    widget.onSearchQuerySubmit(_searchFieldController.text);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Container(
        height: 80,
        decoration: BoxDecoration(
          color: globals.accountColorsData['SearchBGColor']!=null
              ? Color(int.parse(globals.accountColorsData['SearchBGColor'])) : SystemControls.SearchBGColorz,

        ),
        padding: EdgeInsets.all(5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Container(
                    height: 50,
                    decoration: BoxDecoration(
                      //color: Colors.white,
                      borderRadius: BorderRadius.all(
                          Radius.circular(SystemControls.RadiusCircValue)
                      ),
                      border: Border.all(
                          color: globals.accountColorsData['TextdetailsColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                          width: 1)
                    ),
                    child: TextField(
                      controller: _searchFieldController,
                      cursorColor: Colors.black,
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.search,
                            color: Colors.black),
                        hintText: "search".tr().toString(),
                        hintStyle: TextStyle(color: Colors.black),
                        border: InputBorder.none,
                        fillColor: globals.accountColorsData['BGColor']!=null
                            ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
                      ),
                      onSubmitted: widget.onSearchQuerySubmit,
                      //onChanged: widget.onSearchQueryChanged,
                    ),
                  ),
                ),
                InkWell(
                  child: Container(
                    height: 40,
                    width: 40,
                    margin: EdgeInsetsDirectional.fromSTEB(10, 0, 0, 0),
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          blurRadius: 2.0, // has the effect of softening the shadow
                          spreadRadius: 0.5, // has the effect of extending the shadow
                          offset: Offset(
                            2, // horizontal, move right 10
                            3.0, // vertical, move down 10
                          ),
                        )
                      ],
                    ),
                    child: Icon(Icons.send,
                        color: globals.accountColorsData['TextOnMainColor']!=null
                            ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                      size: 20,),
                  ),
                  onTap: clearSearchQuery,
                ),
                IconButton(
                  icon: Icon(Icons.close,
                      color: Colors.black),
                  onPressed: widget.onCancelSearch,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}