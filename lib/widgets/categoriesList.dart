import 'package:flutter/material.dart';
import '../dataControl/dataModule.dart';
import '../Controls.dart';
import 'package:easy_localization/easy_localization.dart';

import '../screens/categoryListNew.dart';

import 'package:cached_network_image/cached_network_image.dart';
import '../widgets/UI_Widgets_CategoriesList.dart';

import '../globals.dart' as globals;

///This Class use only at the home layout
/// never use at any other part
class CategoryList extends StatelessWidget{

  CategoryList(this._categories,this.selectedId, this._CartMeals, this.Lang,
      this.UserToken, this.AppCurrency,this._offers, this.CatStorageKey,
      this.design_Control,this.hasRegistration,this.appInfo);

  List<dynamic> _categories = <dynamic>[];
  List<dynamic> _CartMeals = <dynamic>[];
  List<dynamic> _offers = <dynamic>[];

  int selectedId;
  String Lang;
  String UserToken;
  String AppCurrency;
  bool hasRegistration;
  int design_Control;
  dynamic appInfo;

  Color CatBGColor(int param){
    if(param == selectedId){
      return globals.accountColorsData['MainColor']!=null
          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz;
    }
    else
      return Colors.white;
  }
//TODO REad it to change show
  Route _createRoute(int index) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) =>
          CategoryMealsListNew(_CartMeals, _categories, Lang, selectedId,UserToken,
          AppCurrency, index,design_Control,hasRegistration,appInfo),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset.zero;
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }
  double CatStorageKey;

  Widget build(BuildContext context) {

    return Container(
      color: Colors.white,
      child: ListView(
          padding: EdgeInsets.zero,
          shrinkWrap : true,
          physics: ScrollPhysics(),
          scrollDirection: Axis.horizontal,
          children:[
            InkWell(
              child: OfferCardItem(context, selectedId,Lang),
              onTap: (){
                Navigator.pushReplacement(context,
                    _createRoute(-2));
              },
            ),
            ListView.builder(
                padding: EdgeInsets.zero,
              shrinkWrap : true,
              physics: ScrollPhysics(),
              scrollDirection: Axis.horizontal,
              itemCount: _categories.length,
              itemBuilder: (context, index) {
                if (_categories[index]['categories_parent_id']!=null) {
                  return Container();
                }
                else {
                  return InkWell(
                    child: CategoryCardItem(
                        _categories[index], selectedId, Lang),
                    onTap: () {
                      print(
                          "offset .....    ..... " + CatStorageKey.toString());
                      print(index);

                      Navigator.pushReplacement(context,
                          _createRoute(_categories[index]['categories_id'])
                      );
                    },
                  );
                }
              }
          ),
        ]
      ),
    );
  }
}