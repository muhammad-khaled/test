import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../Controls.dart';
import 'package:easy_localization/easy_localization.dart';

import '../screens/categoryListNew.dart';

import 'package:cached_network_image/cached_network_image.dart';

import '../globals.dart' as globals;

class CategoryListVertical extends StatelessWidget{

  CategoryListVertical(this._categories,this.selectedId, this._CartMeals,
      this.Lang, this.UserToken, this.AppCurrency,this._offers,
      this.CatStorageKey, this.design_Control,this.hasRegistration,this.appInfo){
    _scrollController = new ScrollController(initialScrollOffset: CatStorageKey);
  }

  List<dynamic> _categories = <dynamic>[];
  List<dynamic> _CartMeals = <dynamic>[];
  List<dynamic> _offers = <dynamic>[];
  final ImagePicker _picker = ImagePicker();

  int selectedId;
  String Lang;
  String UserToken;
  String AppCurrency;
  bool hasRegistration;
  dynamic appInfo;
  int design_Control;

  Color CatBGColor(int param){
    if(param == selectedId){
      return globals.accountColorsData['MainColor']!=null
          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz;
    }
    else
      return Colors.white;
  }
//TODO REad it to change show
  Route _createRoute(int index) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) =>
          CategoryMealsListNew(_CartMeals, _categories, Lang, selectedId,UserToken,
              AppCurrency, index, design_Control,hasRegistration,appInfo),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset.zero;
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  double CatStorageKey;
  //ScrollController _scrollController = new ScrollController(initialScrollOffset: 30.0);
  ScrollController _scrollController;


  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 5,left: 5,top: 5),
      color: Colors.white,
      //alignment: Alignment.centerRight,
      child: ListView(
          controller: _scrollController,
          scrollDirection: Axis.vertical,
          padding: EdgeInsets.zero,
          children:[
            Container(
              margin: EdgeInsets.only(top: 5,bottom: 5),
              child: Image.asset("assets/categoriesTabHeaderImg.png",
                color: globals.accountColorsData['MainColor']!=null
                    ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                fit: BoxFit.fitWidth,),
            ),
            InkWell(
              child: Container(
                  decoration: BoxDecoration(
                    color: CatBGColor(-2),//Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.all(Radius.circular(0)),
                  ),
                  margin: EdgeInsets.all(5),
                  padding: EdgeInsets.all(5),
                  child: Column(
                    children: <Widget>[
                      /*Container(
                      //TODO set fixed image or get offers image
                      child: CachedNetworkImage(
                        imageUrl: SystemControls().GetImgPath("1579433449_offers_27_black.png","original"),
                        height: 30,
                      ),
                    ),*/
                      SystemControls().GetSVGImagesAsset(
                          'assets/Icons/offer.svg', 25,
                          Colors.black),
                      Padding(padding: EdgeInsets.all(5)),
                      Container(
                        child: new Center(
                            child: new Text("offers".tr().toString(),
                              style: TextStyle(
                                fontFamily: Lang,
                                fontWeight: FontWeight.normal,
                                fontSize: SystemControls.font4,
                                height: 1.2,
                              ),
                              textAlign: TextAlign.center,
                              maxLines: 2,
                            )
                        ),
                      ),
                    ],
                  )
              ),
              onTap: (){
                Navigator.pushReplacement(context,
                    _createRoute(-2));
              },
            ),
            Container(
              margin: EdgeInsets.only(top: 5,bottom: 5),
              child: Image.asset("assets/rowBottom.png",
                color: globals.accountColorsData['MainColor']!=null
                    ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                fit: BoxFit.fitWidth,),
            ),
            ListView.builder(
                shrinkWrap : true,
                physics: ScrollPhysics(),
                scrollDirection: Axis.vertical,
                itemCount: _categories.length,
                itemBuilder: (context, index) {
                  if (_categories[index]['categories_parent_id']!=null) {
                    return Container();
                  }
                  else {
                    if (index == _categories.length - 1) {
                      return Column(
                        children: <Widget>[
                          InkWell(
                            child: Container(
                                decoration: BoxDecoration(
                                  color: CatBGColor(
                                      _categories[index]['categories_id']), //Colors.white,//Theme.of(context).primaryColor,
                                  //borderRadius: BorderRadius.all(Radius.circular(SystemControls.RadiusCircValue)),
                                ),
                                margin: EdgeInsets.all(5),
                                padding: EdgeInsets.all(5),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      child:
                                      Stack(
                                        children: <Widget>[
                                          //Center(child: SystemControls().circularProgress()),
                                          CachedNetworkImage(
                                            imageUrl: SystemControls()
                                                .GetImgPath(
                                                _categories[index]['categories_img'],
                                                "original"),
                                            height: 30,
                                          ), /*
                                Center(
                                  child: Image.network(
                                    SystemControls().GetImgPath(
                                        _categories[index]['categories_img'],"original"),
                                    height: 30,
                                  ),
                                ),*/
                                        ],
                                      ),
                                    ),
                                    Padding(padding: EdgeInsets.all(5)),
                                    Container(
                                      child: new Center(
                                          child: new Text(
                                            _categories[index]['categories_title'],
                                            //list[position],
                                            style: TextStyle(
                                              fontFamily: Lang,
                                              fontWeight: FontWeight.normal,
                                              fontSize: SystemControls.font4,
                                              height: 1.2,
                                            ),
                                            textAlign: TextAlign.center,
                                            maxLines: 2,
                                          )
                                      ),
                                    ),
                                  ],
                                )
                            ),
                            onTap: () {
                              Navigator.pushReplacement(context,
                                  _createRoute(
                                      _categories[index]['categories_id'])
                              );
                            },
                          ),
                        ],
                      );
                    }
                    return Column(
                      children: <Widget>[
                        InkWell(
                          child: Container(
                              decoration: BoxDecoration(
                                color: CatBGColor(
                                    _categories[index]['categories_id']), //Colors.white,//Theme.of(context).primaryColor,
                                //borderRadius: BorderRadius.all(Radius.circular(SystemControls.RadiusCircValue)),
                              ),
                              margin: EdgeInsets.all(5),
                              padding: EdgeInsets.all(5),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    child:
                                    Stack(
                                      children: <Widget>[
                                        //Center(child: SystemControls().circularProgress()),
                                        CachedNetworkImage(
                                          imageUrl: SystemControls().GetImgPath(
                                              _categories[index]['categories_img'],
                                              "original"),
                                          height: 30,
                                        ), /*
                                Center(
                                  child: Image.network(
                                    SystemControls().GetImgPath(
                                        _categories[index]['categories_img'],"original"),
                                    height: 30,
                                  ),
                                ),*/
                                      ],
                                    ),
                                  ),
                                  Padding(padding: EdgeInsets.all(5)),
                                  Container(
                                    child: new Center(
                                        child: new Text(
                                          _categories[index]['categories_title'],
                                          //list[position],
                                          style: TextStyle(
                                            fontFamily: Lang,
                                            fontWeight: FontWeight.normal,
                                            fontSize: SystemControls.font4,
                                            height: 1.2,
                                          ),
                                          textAlign: TextAlign.center,
                                          maxLines: 2,
                                        )
                                    ),
                                  ),
                                ],
                              )
                          ),
                          onTap: () {
                            Navigator.pushReplacement(context,
                                _createRoute(
                                    _categories[index]['categories_id'])
                            );
                          },
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 5, bottom: 5),
                          child: Image.asset("assets/rowBottom.png",
                            color: globals.accountColorsData['MainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                            fit: BoxFit.fitWidth,),
                        ),
                      ],
                    );
                  }
                }
            ),
            Container(
              margin: EdgeInsets.only(top: 5,bottom: 5),
              child: RotatedBox(
                quarterTurns: 10,
                child: Image.asset("assets/categoriesTabHeaderImg.png",
                  color: globals.accountColorsData['MainColor']!=null
                      ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                  fit: BoxFit.fitWidth,),
              ),
            )
          ]
      ),
    );
  }
}