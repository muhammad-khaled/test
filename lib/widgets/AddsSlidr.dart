import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cached_network_image/cached_network_image.dart';

import '../screens/IteamDetails2.dart';
import '../dataControl/dataModule.dart';
import '../Controls.dart';

import '../globals.dart' as globals;
//import 'package:video_player/video_player.dart';

//import 'package:ext_video_player/ext_video_player.dart';

class AddsSliderProductDetails extends StatefulWidget{
  double sliderHeight;
  int initialPage;
  final List<dynamic> _slider;
  final String _imag;
  final String _video;
  int sliderSize=0;
  AddsSliderProductDetails(this._slider,this._imag,this._video,
      this.sliderHeight,this.initialPage){
    if(_slider != null) {
      print("_Sliderrrrrr");
      sliderSize = _slider.length;
    }
    if(_imag != null)
      sliderSize++;
    if(_video != null)
      sliderSize++;
    print("slider size ????? "+sliderSize.toString());
  }
  @override
  _SliderProductDetails createState() => _SliderProductDetails();
}

List<T> map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) {
    result.add(handler(i, list[i]));
  }
  return result;
}

class _SliderProductDetails extends State<AddsSliderProductDetails>{
  int _current = 0;
/*
  VideoPlayerController _controller;

  Future<ClosedCaptionFile> _loadCaptions() async {
    final String fileContents = await DefaultAssetBundle.of(context).loadString('assets/bumble_bee_captions.srt');
    return SubRipCaptionFile(fileContents);
  }*/
  @override
  void initState() {
    super.initState();
    /*
    if(widget._video != null) {
      _controller = VideoPlayerController.network(//widget._video
        'https://flutter.github.io/assets-for-api-docs/assets/videos/bee.mp4',
        closedCaptionFile: _loadCaptions(),
      );
      _controller.addListener(() {
        setState(() {
          print("_controller.addListener");
        });
      });
      _controller.setLooping(true);
      _controller.initialize();
    }*/
  }
  @override
  void dispose() {
    if(widget._video != null) {
      //_controller.dispose();
    }
    super.dispose();
  }

  Widget build(BuildContext context){
    if (widget.sliderSize > 0) {
      return Column(
        children: <Widget>[
          CarouselSlider.builder(
              options: CarouselOptions(

                height: widget.sliderHeight,
                autoPlayCurve: Curves.fastOutSlowIn,
                aspectRatio: 16 / 9,
                viewportFraction: 1.0,
                initialPage: widget.initialPage,
                enableInfiniteScroll: true,
                enlargeCenterPage: true,
                reverse: false,
                autoPlay: false,

                scrollDirection: Axis.horizontal,
                onPageChanged: (index,d){
                  setState(() {
                    _current = index;

                  });
                },
              ),
              itemCount: widget.sliderSize,
              itemBuilder: (BuildContext context,int index,__) {
                if(widget._slider == null || index>= widget._slider.length){
                  if(index == widget.sliderSize-1 && widget._video != null){
                    return Container();
                    /*
                  return InkWell(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        //color: Colors.black26
                      ),
                      child:
                      Stack(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: 250,
                            child: AspectRatio(
                              aspectRatio: _controller.value.aspectRatio,
                              child: Stack(
                                alignment: Alignment.bottomCenter,
                                children: <Widget>[
                                  VideoPlayer(_controller),
                                  ClosedCaption(text: _controller.value.caption.text),
                                  _PlayPauseOverlay(controller: _controller),
                                  VideoProgressIndicator(
                                    _controller,
                                    allowScrubbing: true,
                                  ),
                                ],
                              ),
                            )
                          ),
                        ],
                      ),
                      height: 190,
                    ),
                    onTap: (){
                      /*if(widget._slider == null || index>= widget._slider.length) {
                        _FullImgScreen(context, SystemControls().GetImgPath(
                            widget._imag, "original"));
                      }*/
                    },
                  );*/
                  }
                  return InkWell(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        //color: Colors.black26
                      ),
                      child:
                      Stack(
                        children: <Widget>[
                          CachedNetworkImage(
                            imageUrl: SystemControls()
                                .GetImgPath(widget._imag,"original"),
                            placeholder: (context, url) => Center(
                                child: SystemControls().circularProgress()),
                            errorWidget: (context, url, error) => Center(child: Icon(Icons.error),),
                            fit: BoxFit.contain,
                            width: MediaQuery.of(context).size.width,
                            height: 250,
                          ),
                        ],
                      ),
                      height: 250,
                    ),
                    onTap: (){
                      if(widget._slider == null || index>= widget._slider.length) {
                        _FullImgScreen(context, SystemControls().GetImgPath(
                            widget._imag, "original"));
                      }
                    },
                  );
                }
                return InkWell(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      //color: Colors.black26
                    ),
                    child:
                    Stack(
                      children: <Widget>[
                        CachedNetworkImage(
                          imageUrl: SystemControls()
                              .GetImgPath("1597854403_dinazaki_GG001_RED_1-1.jpg","original"),
                          placeholder: (context, url) => Center(
                              child: SystemControls().circularProgress()),
                          errorWidget: (context, url, error) => Center(child: Icon(Icons.error),),
                          fit: BoxFit.cover,
                          width: MediaQuery.of(context).size.width,
                        ),
                      ],
                    ),
                    height: 190,
                  ),
                );
              }

          ),
          //_sliderDots2(),
        ],
      );
    }
    return Center();
  }

  LinearGradient UI_commonLinearGradient = LinearGradient(
    begin: FractionalOffset.topCenter,
    end: FractionalOffset.bottomCenter,
    colors: [
      Colors.transparent,
      Colors.black.withOpacity(0.4),
    ],
    stops: [
      0.0,
      1.0,
    ],
  );

  Future<void> _FullImgScreen(BuildContext context, String imgURL) {
    return showDialog<void>(
      context: context,
      //barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {

        return GestureDetector(
          child: Container(
            width: MediaQuery.of(context).size.width,
            color: Color.fromRGBO(0, 0, 0, 0.8),
            child:  Stack(
              children: <Widget>[
                GestureDetector(
                  child: Center(
                    child: Container(
                      child: CachedNetworkImage(
                        imageUrl:  imgURL,
                        placeholder: (context, url) => Center(child: SystemControls().circularProgress(),),
                        fit: BoxFit.contain,
                        width: MediaQuery.of(context).size.width,
                        fadeInCurve: Curves.bounceInOut,
                        fadeInDuration: const Duration(seconds: 2),
                      ),
                    ),
                  ),
                  onTap: () {
                    //Navigator.pop(context);
                  },
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    margin: EdgeInsets.all(20),
                    child: Icon(Icons.cancel,color: Colors.white,size: 30,),
                  ),
                )
              ],
            ),
          ),
          onTap: () {
            Navigator.pop(context);
          },
        );
      },
    );
  }

  Widget _sliderDots2() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: map<Widget>(
        widget._slider,
            (index, url) {
          return Container(
              width: 6.0,
              height: 6.0,
              margin: EdgeInsets.symmetric(vertical: 6.0, horizontal: 3.0),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: _current == index
                    ? SystemControls.MainColorz
                    : SystemControls.MainColorOpacityz,
              ));
        },
      ),
    );
  }
}
/*
class _PlayPauseOverlay extends StatelessWidget {
  const _PlayPauseOverlay({Key key, this.controller}) : super(key: key);

  final VideoPlayerController controller;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AnimatedSwitcher(
          duration: Duration(milliseconds: 50),
          reverseDuration: Duration(milliseconds: 200),
          child: controller.value.isPlaying
              ? SizedBox.shrink()
              : Container(
            color: Colors.black26,
            child: Center(
              child: Icon(
                Icons.play_arrow,
                color: Colors.white,
                size: 100.0,
              ),
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            controller.value.isPlaying ? controller.pause() : controller.play();
          },
        ),
      ],
    );
  }
}
*/
/*
class _SliderProductDetails extends State<SliderProductDetails>{
  List<dynamic> _slidersList;
  _SliderProductDetails(this._slidersList);

  int _current = 0;
  Widget build(BuildContext context){
    return Column(
      children: <Widget>[
        CarouselSlider.builder(
          viewportFraction: 1.0,
          initialPage: 0,
          height: 150,
          enableInfiniteScroll: false,
          reverse: false,
          autoPlay: false,
          scrollDirection: Axis.horizontal,
          onPageChanged: (index) {
            setState(() {
              _current = index;
            });
          },
          itemCount: _slidersList.length,
          itemBuilder: (BuildContext context,int index) =>
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(right: 5,left: 5),
                      width: MediaQuery.of(context).size.width,
                      height: 95,
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        child: CachedNetworkImage(
                          imageUrl: _slidersList[index]['image'],
                          placeholder: (context, url) => Center(child: SystemControls().circularProgress(),),
                          errorWidget: (context, url, error) => Center(child: Icon(Icons.broken_image),),
                          fit: BoxFit.fitHeight,
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                        ),
                      )
                    // ),
                  ),
                ],
              ),
        ),
        _sliderDots2(),
      ],
    );
  }

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }
  Widget _sliderDots2() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: map<Widget>(
        _slidersList,
            (index, url) {
          return Container(
            width: 6.0,
            height: 6.0,
            margin: EdgeInsets.symmetric(vertical: 6.0, horizontal: 3.0),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: _current == index
                    ? Color.fromRGBO(201, 16, 11, 1)
                    : Color.fromRGBO(201, 16, 11, 0.5)),
          );
        },
      ),
    );
  }
/*
  LinearGradient UI_commonLinearGradient = LinearGradient(
    begin: FractionalOffset.topCenter,
    end: FractionalOffset.bottomCenter,
    colors: [
      Colors.transparent,
      Colors.black.withOpacity(0.4),
    ],
    stops: [
      0.0,
      1.0,
    ],
  );*/
}
*/