import 'package:flutter/material.dart';
import '../Controls.dart';
import '../globals.dart' as globals;

class MyTooltip extends StatelessWidget {
  final Widget child;
  final String message;


  MyTooltip({@required this.message, @required this.child});

  bool show = false;
  @override
  Widget build(BuildContext context) {
    final key = GlobalKey<State<Tooltip>>();
    return Tooltip(
      key: key,
      message: message,
      showDuration: Duration(seconds: 1),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: globals.accountColorsData['MainColor']!=null
            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,      ),
      textStyle: TextStyle(
        color: globals.accountColorsData['TextOnMainColor']!=null
            ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
        fontSize: SystemControls.font4,
        fontWeight: FontWeight.bold,
      ),

      child: InkWell(
        onTap: () => _onTap(key),
        child: child,
      ),
    );
  }

  void _onTap(GlobalKey<State<Tooltip>> key ) {
    print('clickkkkked');

    final dynamic tooltip = key.currentState;
    tooltip.ensureTooltipVisible();
    if(show ==true){
      show = false;

    }

  }
}