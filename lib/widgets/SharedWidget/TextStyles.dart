


import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../../globals.dart' as globals;

import '../../Controls.dart';

TextStyle textStyle(BuildContext context) {
  return TextStyle(
    fontSize: SystemControls.font3,
    fontFamily: EasyLocalization.of(context).currentLocale.languageCode,
    color: globals.accountColorsData['TextHeaderColor'] != null
        ? Color(int.parse(globals.accountColorsData['TextHeaderColor']))
        : SystemControls.TextHeaderColorz,
  );
}


Color itemBGColor() {
  return globals.accountColorsData['MainColorOpacity'] != null
      ? Color(int.parse(globals.accountColorsData['MainColorOpacity']))
      : SystemControls.MainColorOpacityz;
}