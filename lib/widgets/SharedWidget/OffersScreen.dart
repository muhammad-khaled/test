







import '../../provider/productsInCartProvider.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';

import '../../globals.dart' as globals;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:decorated_icon/decorated_icon.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:sizer/sizer.dart';

import '../../Controls.dart';
import '../UI_Alert_Widgets.dart';
class OfferScreen extends StatelessWidget {
   OfferScreen({Key key,this.offer,this.appCurrency,this.token}) : super(key: key);
  var offer;
  final String appCurrency;
  final String token;


  void _onShareTap(String shareUrl,BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final RenderBox box = context.findRenderObject();
    Share.share(shareUrl,
        sharePositionOrigin: Rect.fromLTWH(0, 0, size.width, size.height / 2)
      //sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size,
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton:  Consumer<CartStateProvider>(builder: (context,cart,_){
        return   Container(
          width: MediaQuery.of(context).size.width-60 ,
          child: MaterialButton(
            height: 48,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            onPressed: (){
              // cart.resetSize();
              cart.showAddToCartOffer(
                  context, offer, cart.cartList,appCurrency,cart);  },
            color: globals.accountColorsData['MainColor'] != null
                ? Color(int.parse(globals.accountColorsData['MainColor']))
                : SystemControls.MainColorz,
            child: Text("addToCart".tr().toString(),style: TextStyle(color: globals.accountColorsData['TextOnMainColor']!=null
                ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz),),

          ),
        );
      },),
      body: Container(
        margin: EdgeInsets.only(left: 16, right: 16, top: 2, bottom: 2),

        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,

            children: [
              Center(
                child: Container(
                  height: 30.h,
                  width: 200.w,
                  child: Stack(
                    fit: StackFit.expand,
                    children: [
                      Container(
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(20),
                          child: CachedNetworkImage(
                            imageUrl: SystemControls()
                                .GetImgPath(offer.offersImgMobile, "medium"),
                            placeholder: (context, url) => Center(
                              child:
                              SystemControls().circularProgress(),
                            ),
                            errorWidget: (context, url, error) =>
                                Center(
                                  child: Icon(Icons.broken_image),
                                ),
                            fit: BoxFit.contain,
                            height: 10.h,
                          ),
                        ),
                      ),
                      EasyLocalization.of(context).currentLocale.languageCode =="en"? Positioned(
                        left: 7.1,
                        top: 36,
                        child:      InkWell(
                          onTap: (){
                            Navigator.pop(context);
                          },
                          child: DecoratedIcon(
                            Icons.arrow_back,
                            color: Colors.black,
                            size: 30.0,
                            shadows: [
                              BoxShadow(
                                blurRadius: 60.0,
                                color: Colors.white,
                              ),
                              BoxShadow(
                                blurRadius: 1.0,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),) :Positioned(
                        right: 7.1,
                        top: 36,
                        child:      InkWell(
                          onTap: (){
                            Navigator.pop(context);
                          },
                          child: DecoratedIcon(
                            Icons.arrow_back,
                            color: Colors.black,
                            size: 30.0,
                            shadows: [
                              BoxShadow(
                                blurRadius: 60.0,
                                color: Colors.white,
                              ),
                              BoxShadow(
                                blurRadius: 1.0,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),),



                    ],
                  ),
                ),
              ),

              SizedBox(height: 1  .h,),
              Container(
                width: MediaQuery.of(context).size.width,
                child: Row(
                  children: [
                    Container(
                        width: 60.w,
                        child: Text(
                          "${offer.offersTitle.toString()}",
                          style: TextStyle(fontWeight: FontWeight.bold,color:  globals.accountColorsData['TextdetailsColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,),
                          maxLines: 4,
                        ),),
                    Spacer(),
                    IconButton(onPressed: () async {
                      String shareLink = globals
                          .accounts_product_url +
                          "/" +
                          offer.offersId
                              .toString();
                      print(shareLink);
                      _onShareTap(shareLink,context);
                    }, icon: Icon(Icons.share,
                      size: 25,
                      color: globals.accountColorsData['BorderColor'] != null
                          ? Color(
                          int.parse(globals.accountColorsData['BorderColor']))
                          : SystemControls.BorderColorz,)),
                    SizedBox(
                      width: 2.w,
                    ),



                  ],
                ),
              ),

              SizedBox(height: 1  .h,),
              Text(
                  "${offer.offersPrice} $appCurrency",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,color: globals.accountColorsData[
                  'MainColor'] !=
                      null
                      ? Color(int.parse(
                      globals.accountColorsData[
                      'MainColor']))
                      : SystemControls
                      .MainColorz)),

              SizedBox(height: 1  .h,),
              Container(
                child: Text(
                  "${offer.offersDesc != null ? offer.offersDesc : ""}",
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
