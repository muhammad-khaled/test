




import '../../widgets/SharedWidget/OffersScreen.dart';

import '../../Models/DiscountModel.dart';
import '../../Models/HomeModel/CategoryDetailModel.dart';
import '../../Models/HomeModel/HomeProductsModel.dart';
import '../../Models/HomeModel/applicationHomeModel.dart';
import '../../provider/HomeProvider/HomeProvider.dart';
import '../../services/HomeRepo.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import '../../Controls.dart';
import 'NoItemScreen.dart';
import 'ProductItmsList.dart';
import 'ProgressIndecator.dart';




class DisplayProductWidget extends StatelessWidget {
   DisplayProductWidget({Key key,this.cartList,this.token,this.appCurrency}) : super(key: key);
List<dynamic> cartList;
final String token;
final String appCurrency;
  @override
  Widget build(BuildContext context) {

    Provider.of<HomeProvider>(context, listen: false).fetchHomeItems(
        context, Provider.of<HomeProvider>(context, listen: false).currentPage);
    return Consumer<HomeProvider>(builder: (context, homeProvider, _) {
      switch (homeProvider.isCategory) {
        case -2:
          return Container(
            child: StreamBuilder<HomeProductsModel>(
              stream: homeProvider.streamHomeItems,
              builder: (context, snap) {
                if (snap.hasData) {
                  if (snap.data.data.pagination.lastPage ==
                      homeProvider.currentPage) {
                    homeProvider.showLoader = false;
                  }
                  homeProvider.lastPage = snap.data.data.pagination.lastPage;
                  homeProvider.currentPage =
                      snap.data.data.pagination.currentPage;
                  return snap.data.data.homeProducts.length ==0? NoItemWidget(): ProductsGridView(
                    token: token,
                    cartList:cartList,
                    products: snap.data.data.homeProducts,
                    appCurrency: appCurrency,
                    search: false,
                  );
                } else {
                  return Container(
                    child: Indicator(),
                  );
                }
              },
            ),
          );
          break;
        case -1:
          return Container(
            child: Column(
              children: [
                FutureBuilder<ApplicationHomeModel>(
                  future: ApplicationHomeResponseRepo(context: context)
                      .getHomeApplicationItems,
                  builder: (context, snap) {
                    if (snap.hasData) {
                      snap.data.data.offers
                          .removeWhere((element) => element.offersPrice == "0");
                      return ListView.builder(
                        shrinkWrap: true,
                        physics: ScrollPhysics(),
                        itemCount: snap.data.data.offers.length,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: (){
                              snap.data.data.offers[index].offersPrice !="0" ?  Navigator.push(context, MaterialPageRoute(builder: (context)=>OfferScreen(offer: snap.data.data.offers[index],token: token,appCurrency: appCurrency,))): print('');


                            },
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                height: 20.h,
                                width: 100.w,
                                child: CachedNetworkImage(
                                    imageUrl: SystemControls().GetImgPath(
                                        snap.data.data.offers[index]
                                            .offersImgMobile,
                                        "medium"),
                                    fit: BoxFit.fill,
                                    height: 20.h,
                                    width: 100.w,
                                    errorWidget: (context, url, error) =>
                                        Icon(Icons.error)),
                              ),
                            ),
                          );
                        },
                      );
                    } else {
                      return Container(
                        height: 30.h,
                        child: Indicator(),
                      );
                    }
                  },
                ),
                StreamBuilder<DiscountDetailsModel>(
                  stream: homeProvider.streamDiscount,
                  builder: (context, snap) {

                    if (snap.hasData) {
                      if (homeProvider.currentPage == homeProvider.lastPage) {
                        homeProvider.showLoader = false;
                      }
                      if (homeProvider.currentPage == homeProvider.lastPage) {
                        homeProvider.showLoader = false;
                      }
                      homeProvider.lastPage =
                          snap.data.data.pagination.lastPage;
                      homeProvider.currentPage =
                          snap.data.data.pagination.currentPage;

                      homeProvider.productDiscountList.removeWhere((element) => element.productsPriceAfterSale =="");
                      homeProvider.productDiscountList.toSet();
                      return snap.data.data.products.length ==0? NoItemWidget(): ProductsGridView(
                        token: token,

                        cartList:cartList,
                        products: homeProvider.productDiscountList.toSet().toList(),
                        appCurrency: appCurrency,
                        search: false,
                      );
                    } else {
                      return Container(
                        height: 30.h,
                        child: Indicator(),
                      );
                    }
                  },
                ),
              ],
            ),
          );
          break;
        case 0:
          return Container(
            child: StreamBuilder<CategoriesDetailModel>(
              stream: homeProvider.streamProductById,
              builder: (context, snap) {
                if (snap.hasData) {
                  if (homeProvider.currentPage == homeProvider.lastPage) {
                    homeProvider.showLoader = false;
                  }
                  homeProvider.lastPage = snap.data.data.pagination.lastPage;
                  homeProvider.currentPage =
                      snap.data.data.pagination.currentPage;

                  return ProductsGridView(
                    token: token,

                    cartList:cartList,
                    products: homeProvider.productByIdList.toSet().toList(),
                    appCurrency: appCurrency,
                    search: false,
                  );
                } else {
                  return Container(
                    height: 30.h,
                    child: Indicator(),
                  );
                }
              },
            ),
          );
          break;

        default:
          return Container();
      }
    });
  }
}