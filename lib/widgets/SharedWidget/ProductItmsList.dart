import 'package:easy_localization/easy_localization.dart';

import '../../provider/HomeProvider/HomeProvider.dart';
import '../../provider/HomeProvider/SearchProvider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import '../../Controls.dart';

import 'ProductCardBase.dart';
import 'ProgressIndecator.dart';

class ProductsGridView extends StatelessWidget {
  ProductsGridView(
      {this.products,
      this.appCurrency,
      this.cartList,
      this.search,
      this.token});

  final List<dynamic> products;
  List<dynamic> cartList;
  final String appCurrency;
  final String token;
  bool search;
  @override
  Widget build(BuildContext context) {
    int count = (((MediaQuery.of(context).size.width - 5) / 400)).round();
    count *= SystemControls.MinNumberOfItemsOnRow;
    int deviceShortestSide = MediaQuery.of(context).size.shortestSide.round();
    int cardHei;
    int imgCardRatio;
    if (deviceShortestSide > 415) {
      cardHei = 300;
      imgCardRatio = 5;
    } else {
      if (SystemControls.cardViewItemNumber == 2) {
        cardHei = 225;
      } else {
        cardHei = 250;
      }
      imgCardRatio = 5;
    }

    return Consumer2<HomeProvider, SearchProvider>(
        builder: (context, homeProvider, searchProvider, _) {
      return Column(
        children: [
          GridView.builder(
              itemCount: products.length,
              shrinkWrap: true,
              physics: ScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: MediaQuery.of(context).size.width<1000 ?2:3,
                childAspectRatio: EasyLocalization.of(context).currentLocale.languageCode == "ar"? 1.6 / 2.7 :2/2.7,
              ),
              itemBuilder: (context, index) {
                // print(products[index].productsType.compareTo("count"));
                return ProductItemCardBase(
                    token: token,
                    product: products[index],
                    appCurrency: appCurrency,
                    cartList: cartList);
              }),
          search != null
              ? search != true
                  ? homeProvider.showLoader == true
                      ? Container(
                          height: 15.h,
                          width: 100.w,
                          child: Indicator(),
                        )
                      : Container()
                  : searchProvider.showLoader == true
                      ? Container(
                          height: 15.h,
                          width: 100.w,
                          child: Indicator(),
                        )
                      : Container()
              : Container()
        ],
      );
    });
  }
}
