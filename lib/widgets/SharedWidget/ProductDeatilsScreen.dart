import 'dart:io';

import 'package:flutter_html/flutter_html.dart';

import '../../Models/HomeModel/ProductByIdModel.dart';
import '../../services/HomeRepo.dart';
import '../../widgets/SharedWidget/ProgressIndecator.dart';

import '../../Controls.dart';
import '../../Models/HomeModel/sharedModel.dart';
import '../../dataControl/favsModule.dart';

import '../../provider/HomeProvider/HomeProvider.dart';
import '../../provider/productsInCartProvider.dart';
import '../../widgets/SharedWidget/ProductCardBase.dart';
import '../../widgets/Video.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';
import '../../globals.dart' as globals;
import 'package:easy_localization/easy_localization.dart';
import 'package:decorated_icon/decorated_icon.dart';

import '../UI_Alert_Widgets.dart';

class ProductItemsDetailsScreen extends StatefulWidget {
   ProductItemsDetailsScreen(
      {Key key, this.id, this.video, this.appCurrency, this.product,this.cartList,this.token})
      : super(key: key);
  final int id;
  final String video;
   ProductShared product;
  List<dynamic> cartList;
  final String appCurrency;
  final String token;

  @override
  _ProductItemsDetailsScreenState createState() =>
      _ProductItemsDetailsScreenState();
}

class _ProductItemsDetailsScreenState extends State<ProductItemsDetailsScreen> {
  YoutubePlayerController _controller;
  SwiperController swiperController;
  List items = [];
  void initState() {
    super.initState();
    print('id id id id');
    print(widget.product.productsId);
    print('id');
    items.add(widget.product.productsImg);
    categoryTitleList.clear();
    widget.product.categorySeries.forEach((element) {
      categoryTitleList.add(element.categoriesTitle);
    });
    categoryTitle.writeAll(categoryTitleList,'/');
    print("categoryTitleList");
    print(categoryTitleList.length);
    widget.product.images !=null ?    widget.product.images.forEach((element) {
      print('looowest${widget.product.productsLowestWeightAvailable}');
      items.add(element.productsImagesName);
    }): print('');

    if (widget.product.productsVideo != null) {
      items.add(widget.product.productsVideo);
    }
    swiperController = SwiperController();
    if (widget.video != null) {
      var video = widget.video.split("/");

      _controller = YoutubePlayerController(
        initialVideoId: '${widget.video}',
        params: YoutubePlayerParams(
          playlist: ["${video.last}"],
          startAt: const Duration(minutes: 1, seconds: 36),
          showControls: true,
          showFullscreenButton: true,
          desktopMode: true,
          privacyEnhanced: true,
          useHybridComposition: true,
        ),
      );
      _controller.onEnterFullscreen = () {
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.landscapeLeft,
          DeviceOrientation.landscapeRight,
        ]);
        print('Entered Fullscreen');
      };
      _controller.onExitFullscreen = () {
        print('Exited Fullscreen');
      };
    }
  }
  void _onShareTap(String shareUrl) {
    final Size size = MediaQuery.of(context).size;
    final RenderBox box = context.findRenderObject();
    Share.share(shareUrl,
        sharePositionOrigin: Rect.fromLTWH(0, 0, size.width, size.height / 2)
      //sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size,
    );
  }
  String SelectedSize = "";
  List categoryTitleList=<String>[];
  final categoryTitle = StringBuffer();
  @override
  Widget build(BuildContext context) {







    return Container(
      color: Colors.white,

      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
          floatingActionButton:  Consumer<CartStateProvider>(builder: (context,cart,_){
            return   Container(
              width: MediaQuery.of(context).size.width-60 ,
              child: MaterialButton(
                height: 48,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                onPressed: (){
                  // cart.resetSize();
                  cart.showAddToCartProduct(
                      context, widget.product, cart.cartList,widget.appCurrency,cart);  },
                color: globals.accountColorsData['MainColor'] != null
                    ? Color(int.parse(globals.accountColorsData['MainColor']))
                    : SystemControls.MainColorz,
                child: Text("addToCart".tr().toString(),style: TextStyle(color: globals.accountColorsData['TextOnMainColor']!=null
                    ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz),),

              ),
            );
          },),
          // appBar: AppBar(
          //   title: Text(
          //     "${widget.product.productsTitle}",
          //     style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
          //   ),
          //   centerTitle: true,
          // ),

          body: Container(
            margin: EdgeInsets.only(left: 16, right: 16, top: 2, bottom: 2),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 5,
                  ),
                  Center(
                    child: Container(

                      width: 200.w,
                      child: Stack(
                        children: [
                          Column(
                            children: [
                            Consumer<HomeProvider>(builder: (context,home,_){
                              return   Container(
                                height: 30.h,
                                child: Swiper(
                                  controller: swiperController,
                                  onIndexChanged: (index){
                                    home.changeSwiperIndex(index);
                                  },
                                  fade: .0,
                                  scale: .80,
                                  viewportFraction: 1,
                                  itemCount: items.length,
                                  pagination: new SwiperPagination(
                                    alignment: Alignment.bottomCenter,
                                    builder: new DotSwiperPaginationBuilder(
                                        color: Colors.transparent,
                                        activeColor:Colors.transparent),
                                  ),

                                  outer: false,
                                  itemBuilder: (context, index) {
                                    const player = YoutubePlayerIFrame(
                                      aspectRatio: 16 / 9,
                                    );
                                    return Container(
                                      child: (items.length - 1 == index &&
                                          widget.video != null)
                                          ? (Platform.isIOS
                                          ? YoutubeCustomWidget(
                                          video: "${widget.video.split("/").last}")
                                          : YoutubePlayerControllerProvider(
                                        // Passing controller to widgets below.
                                        controller: _controller,
                                        child: Container(
                                          width:
                                          MediaQuery.of(context).size.width /
                                              1.6,
                                          child: player,
                                        ),
                                      ))
                                          : ClipRRect(
                                        borderRadius: BorderRadius.circular(0),
                                        child: CachedNetworkImage(
                                          imageUrl: SystemControls()
                                              .GetImgPath(items[index], "medium"),
                                          placeholder: (context, url) => Center(
                                            child:
                                            SystemControls().circularProgress(),
                                          ),
                                          errorWidget: (context, url, error) =>
                                              Center(
                                                child: Icon(Icons.broken_image),
                                              ),
                                          fit: BoxFit.contain,
                                          height: 10.h,
                                        ),
                                      ),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        color: Colors.transparent,
                                      ),
                                    );
                                  },
                                ),
                              );
                            }),
                              Container(
                                width: MediaQuery.of(context).size.width/1.1,
                                height: 100,
                                child: Center(
                                  child: Wrap(
                                    children: List.generate(items.length, (index){

                                      return Consumer<HomeProvider>(builder: (context,home,_){
                                       return InkWell(
                                         onTap: (){
                                           swiperController.move(index);
                                           home.changeSwiperIndex(index);

                                         },
                                         child: Padding(
                                           padding: const EdgeInsets.all(8.0),
                                           child: Container(
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(8),
                                                color:  globals
                                                    .accountColorsData[
                                                'BorderColor'] !=
                                                    null
                                                    ? Color(int.parse(globals
                                                    .accountColorsData[
                                                'BorderColor']))
                                                    .withOpacity(.2)
                                                    : SystemControls.BorderColorz,

                                                border: Border.all(color:home.currentIndex ==index? globals.accountColorsData['MainColor']!=null
                                                    ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz :Colors.transparent),

                                              ),

                                              height: 6.h,
                                              width: 13.w,

                                              child: Container(
                                                child: (items.length - 1 == index &&
                                                    widget.video != null)
                                                    ? Icon(Icons.video_collection_rounded)
                                                    : Container(
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(8),

                                                  ),
                                                  height: 6.h,
                                                  width: 13.w,
                                                  child: ClipRRect(
                                                    borderRadius: BorderRadius.circular(8),
                                                    child: CachedNetworkImage(
                                                      imageUrl: SystemControls()
                                                          .GetImgPath(items[index], "thumbnail"),
                                                      placeholder: (context, url) => Center(
                                                        child:
                                                        SystemControls().circularProgress(),
                                                      ),
                                                      errorWidget: (context, url, error) =>
                                                          Center(
                                                            child: Icon(Icons.broken_image),
                                                          ),
                                                      fit: BoxFit.contain,


                                                    ),
                                                  ),
                                                ),
                                                decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(8),
                                                  color: Colors.transparent,
                                                ),
                                              ),

                                            ),
                                         ),
                                       );
                                      },);
                                    }),
                                  ),
                                ),
                              ),
                            ],
                          ),



                         EasyLocalization.of(context).currentLocale.languageCode =="en"? Positioned(
                            left: 7.1,
                            top: 10,
                            child:      InkWell(
                              onTap: (){
                                Navigator.pop(context);
                              },
                              child: DecoratedIcon(
                                Icons.arrow_back,
                                color: Colors.black,
                                size: 30.0,
                                shadows: [
                                  BoxShadow(
                                    blurRadius: 60.0,
                                    color: Colors.white,
                                  ),
                                  BoxShadow(
                                    blurRadius: 1.0,
                                    color: Colors.white,
                                  ),
                                ],
                              ),
                            ),) :Positioned(
                           right: 7.1,
                           top: 10,
                           child:      InkWell(
                             onTap: (){
                               Navigator.pop(context);
                             },
                             child: DecoratedIcon(
                               Icons.arrow_back,
                               color: Colors.black,
                               size: 30.0,
                               shadows: [
                                 BoxShadow(
                                   blurRadius: 60.0,
                                   color: Colors.white,
                                 ),
                                 BoxShadow(
                                   blurRadius: 1.0,
                                   color: Colors.white,
                                 ),
                               ],
                             ),
                           ),),



                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 3.h,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      children: [
                        Container(
                            width: 60.w,
                            child: Text(
                              "${categoryTitle.toString()}",
                              style: TextStyle(fontWeight: FontWeight.bold,color:  globals.accountColorsData['TextdetailsColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,),
                              maxLines: 4,
                            )),
                        Spacer(),
                        IconButton(onPressed: () async {
                          String shareLink = globals
                              .accounts_product_url +
                              "/" +
                              widget.product.productsId
                                  .toString();
                          print(shareLink);
                          _onShareTap(shareLink);
                        }, icon: Icon(Icons.share,
                          size: 25,
                          color: globals.accountColorsData['BorderColor'] != null
                              ? Color(
                              int.parse(globals.accountColorsData['BorderColor']))
                              : SystemControls.BorderColorz,)),
                        SizedBox(
                          width: 2.w,
                        ),
                        Consumer<HomeProvider>(
                          builder: (context, home, _) {
                            return IconButton(
                                onPressed: () async {
                                  if (widget.token == "") {
                                    ErrorDialogAlertGoTOLogin(
                                        context,
                                        "mustLogin".tr().toString(),
                                        EasyLocalization.of(context)
                                            .currentLocale
                                            .languageCode);
                                  } else {
                                    if (widget.product.isFav == 1) {
                                      widget.product.isFav = 0;
                                    } else {
                                      widget.product.isFav = 1;
                                    }
                                    // onFavSubmit(_meals);

                                    var res = await updateFavsItem(
                                        EasyLocalization.of(context)
                                            .currentLocale
                                            .languageCode,
                                        widget.token,
                                        widget.product.productsId,context);
                                    if (res.status == 200) {
                                      SuccDialogAlertStaySameLayout(
                                          context, res.message,ValueKey("product${widget.product.productsId}"));
                                    } else {
                                      if (res.status == 401) {
                                        SystemControls().LogoutSetUserData(
                                            context,
                                            EasyLocalization.of(context)
                                                .currentLocale
                                                .languageCode);
                                      }

                                      if (widget.product.isFav == 1) {
                                        widget.product.isFav = 0;
                                      } else {
                                        widget.product.isFav = 1;
                                      }
                                      // onFavSubmit(_meals);

                                      ErrorDialogAlert(context, res.message);
                                    }
                                  }
                                  home.notifyListeners();
                                },
                                icon: widget.product.isFav == 1
                                    ? Icon(
                                  Icons.favorite,
                                  size: 25,
                                  color: Colors.red,
                                )
                                    : Icon(
                                  Icons.favorite,
                                  size: 25,
                                  color: globals.accountColorsData['BorderColor'] != null
                                      ? Color(
                                      int.parse(globals.accountColorsData['BorderColor']))
                                      : SystemControls.BorderColorz,
                                ));
                          },
                        ),


                      ],
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Container(
                    width: 100.w,
                    child: Row(
                      children: [

                        Container(
                          width: MediaQuery.of(context).size.width-80 ,
                          child: Text(
                              "${widget.product.productsTitle}",

                            style: TextStyle(fontWeight: FontWeight.bold),
                            maxLines: 3,
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: widget.product.productsPriceAfterSale != ""
                        ? Row(
                      children: [
                        Text(
                            "${widget.product.productsPrice} ${widget.appCurrency}",
                            style: TextStyle(
                                decoration: TextDecoration.lineThrough,
                                fontWeight: FontWeight.bold,
                                fontSize: 16,color: globals.accountColorsData['TextdetailsColor'] !=
                                null
                                ? Color(int.parse(globals
                                .accountColorsData['TextdetailsColor']))
                                : SystemControls.TextdetailsColorz,)),
                        SizedBox(width: 2.5.w,),
                        Text(
                            "${widget.product.productsPriceAfterSale} ${widget.appCurrency}",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16,color: globals.accountColorsData[
                            'MainColor'] !=
                                null
                                ? Color(int.parse(
                                globals.accountColorsData[
                                'MainColor']))
                                : SystemControls
                                .MainColorz)),
                        Spacer(),
                      ],
                    )
                        : Text("${widget.product.productsPrice}"),
                  ),

                  SizedBox(
                    height: 2.h,
                  ),

                  widget.product.choices != null ?  widget.product.choices.length != 0
                      ? Container(
                          child: Text(
                            "size".tr().toString(),
                          ),
                        )
                      : Container():Container(),
                  SizedBox(
                    height:widget.product.choices != null ? widget.product.choices.length != 0 ? 2.h : 0:0,
                  ),
               widget.product.choices != null ?   widget.product.choices.length != 0
                      ? Consumer<CartStateProvider>(builder: (context, cart, _) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            child: Wrap(
                              children: List.generate(
                                  widget.product.choices.length,
                                  (index) => Padding(
                                        padding: const EdgeInsets.only(
                                            left: 8, right: 8),
                                        child: Container(
                                          width: 15.w,
                                          child: MaterialButton(
                                            onPressed: () {
                                              cart.changeSize(index,widget.product.choices[index].choicesId,widget.product.choices[index].choicesTitle);


                                            },
                                            child: Text(
                                              "${widget.product.choices[index].choicesTitle}",
                                              style: TextStyle(
                                                  color:
                                                      cart.selectedSize ==
                                                              index
                                                          ?    globals.accountColorsData['TextOnMainColor']!=null
                                                          ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
                                                          : Colors.black),
                                            ),
                                            color: cart.selectedSize ==
                                                    index
                                                ? globals.accountColorsData[
                                                            'MainColor'] !=
                                                        null
                                                    ? Color(int.parse(
                                                        globals.accountColorsData[
                                                            'MainColor']))
                                                    : SystemControls.MainColorz
                                                : Colors.grey,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                          ),
                                        ),
                                      )),
                            ),
                          );
                        })
                      : Container():Container(),
                  SizedBox(
                    height: 2.h,
                  ),
                  Container(
                    child: Html(
                      data: """${widget.product.productsDesc != null ? widget.product.productsDesc : ""}""",
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),

                  FutureBuilder<ProductByIdModel>(future: GetProductByIdRepo(context: context,id: widget.product.productsId).getProductById,builder: (context,snap){
                    if(snap.hasData){
                      return snap.data.data.product.suggestions.length ==0?Container(): Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: Text(
                              "suggestions".tr().toString(),
                              textScaleFactor: 1.2,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            height: 40.h,
                            width: MediaQuery.of(context).size.width,
                            child: ListView.builder(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: snap.data.data.product.suggestions.length,
                              itemBuilder: (context, index) {

                                return ProductItemCardBase(
                                  token: widget.token,
                                  cartList: widget.cartList,
                                  appCurrency: widget.appCurrency,
                                  product: snap.data.data.product.suggestions[index],
                                );
                              },
                            ),
                          ),
                        ],
                      );
                    }else {
                      return Container(
                        child: Center(child: Indicator(),),
                      );
                    }
                  },),
                  // widget.product.suggestions != null ?   widget.product.suggestions.length != 0
                  //     ? Container(
                  //         child: Text(
                  //           "suggestions".tr().toString(),
                  //           textScaleFactor: 1.2,
                  //           style: TextStyle(fontWeight: FontWeight.bold),
                  //         ),
                  //       )
                  //     : Container():Container(),
                  // widget.product.suggestions != null ?    widget.product.suggestions.length != 0
                  //     ? Container(
                  //   height: 40.h,
                  //         width: MediaQuery.of(context).size.width,
                  //         child: ListView.builder(
                  //           shrinkWrap: true,
                  //           scrollDirection: Axis.horizontal,
                  //           itemCount: widget.product.suggestions.length,
                  //           itemBuilder: (context, index) {
                  //
                  //                 return ProductItemCardBase(
                  //               token: widget.token,
                  //               cartList: widget.cartList,
                  //               appCurrency: widget.appCurrency,
                  //               product: widget.product.suggestions[index],
                  //             );
                  //           },
                  //         ),
                  //       )
                  //     : Container():Container(),

                  SizedBox(height: 8.h,),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
