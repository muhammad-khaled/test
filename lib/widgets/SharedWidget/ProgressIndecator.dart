





import 'package:flutter/material.dart';
import '../../Controls.dart';
import '../../globals.dart' as globals;

class Indicator extends StatelessWidget {
  const Indicator({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(
            globals.accountColorsData['MainColor'] != null
                ? Color(int.parse(globals.accountColorsData['MainColor']))
                : SystemControls.MainColorz),
      ),
    );
  }
}