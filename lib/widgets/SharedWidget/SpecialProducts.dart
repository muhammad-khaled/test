


import '../../Models/HomeModel/HomeProductsModel.dart';

import '../../Models/DiscountModel.dart';
import '../../provider/productsInCartProvider.dart';
import '../../widgets/SharedWidget/ProductItmsList.dart';
import '../../widgets/SharedWidget/ProgressIndecator.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../provider/HomeProvider/HomeProvider.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'NoItemScreen.dart';

class SpecialProducts extends StatefulWidget {
  const SpecialProducts({Key key,this.appCurrency,this.token}) : super(key: key);
final String token;
final String appCurrency;

  @override
  _SpecialProductsState createState() => _SpecialProductsState();
}

class _SpecialProductsState extends State<SpecialProducts> {
  ScrollController scrollController;
  scrollListener(){
    var home = Provider.of<HomeProvider>(context, listen: false);

    print(scrollController.position.pixels);
    print(scrollController.position.maxScrollExtent);

    if (home.currentPage < home.lastPage &&
        scrollController.position.pixels >=
            scrollController.position.maxScrollExtent ) {
      if (home.currentPage < home.nextPage) {
        print('products');
        home.fetchHomeItems(context,home.currentPage++);
      }

    }}
  @override
  void initState() {
    // TODO: implement initState
    scrollController = ScrollController();
    scrollController.addListener(scrollListener);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title:                   Text("specialProduct".tr().toString(),style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 17),),

        centerTitle: true,
      ),
      body:
      Consumer2<HomeProvider,CartStateProvider>(builder: (context,home,cart,_){
        return    Container(
        child: NotificationListener<ScrollNotification>(
          onNotification: ( scrollNotification){

            if(scrollNotification.metrics.pixels ==
                scrollNotification.metrics.maxScrollExtent &&home.currentPage < home.lastPage &&home.currentPage < home.nextPage){
              print(scrollNotification.metrics.pixels);

              home.currentPage++;
              print(home.currentPage);
              home.fetchHomeItems(
                  context, home.currentPage);
            }
            return false;
          },

          child: SingleChildScrollView(
            child: Column(
              children: [
                StreamBuilder<HomeProductsModel>(
                    stream: home.streamHomeItems,
                    builder: (context,snap){
                      if(home.specialProducts.length >1){
                        if(snap.hasData){
                          if (snap.data.data.pagination.lastPage ==
                              home.currentPage) {
                            home.showLoader = false;

                          }
                          home.lastPage = snap.data.data.pagination.lastPage;
                          home.currentPage = snap.data.data.pagination.currentPage;
                          print('ffffffffffffffff');
                          print(home.bestSaleList.length);
                          print(snap.data.data.homeProducts.length);
                          print(home.lastPage);
                          print(home.currentPage);
                          print("home.lastPage${home.lastPage}");
                          return snap.data.data.homeProducts.length ==0? NoItemWidget(): ProductsGridView(cartList: cart.cartList,token: widget.token,appCurrency: widget.appCurrency,products: home.specialProducts,search: false,);
                        }else{
                          return  Container();
                        }}else{
                        return Container(
                          height: 70.h,
                          child: Center(child: Indicator(),),
                        );
                      }
                    }),



              ],
            ),
          ),
        ),
      ); }),
    );
  }
}
