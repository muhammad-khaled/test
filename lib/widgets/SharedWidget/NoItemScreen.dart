




import 'dart:async';

import '../../widgets/SharedWidget/ProgressIndecator.dart';

import '../../Controls.dart';
import '../../globals.dart' as globals;

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
class NoItemWidget extends StatefulWidget {
  const NoItemWidget({Key key}) : super(key: key);

  @override
  _NoItemWidgetState createState() => _NoItemWidgetState();
}

class _NoItemWidgetState extends State<NoItemWidget> {

  Timer _timer;
  int _start = 3  ;
  bool show = false;

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
          (Timer timer) {
        if (_start == 0) {
          setState(() {
            show =true;
            timer.cancel();
          });
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    startTimer();
    super.initState();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _timer.cancel();
  }
  @override
  Widget build(BuildContext context) {

    return Center(
      child: Container(
        height: 70.h,
        width: MediaQuery.of(context).size.width/1.2,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
          show != true ? Indicator():  Text("noItem".tr().toString(),style: TextStyle(fontWeight: FontWeight.bold,color:  globals.accountColorsData[
            'MainColor'] !=
                null
                ? Color(int.parse(
                globals.accountColorsData[
                'MainColor']))
                : SystemControls.MainColorz,fontSize: 14.sp),textAlign: TextAlign.center,),
          ],
        ),
      ),
    );
  }
}
