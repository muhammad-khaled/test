


import '../../../Models/HomeModel/sharedModel.dart';

import '../../../provider/productsInCartProvider.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import '../../../globals.dart' as globals;

import '../../../Controls.dart';
class RadioButtonWidgetDetails extends StatefulWidget {
   RadioButtonWidgetDetails({this.product});
   ProductShared product;


  @override
  _RadioButtonWidgetDetailsState createState() => _RadioButtonWidgetDetailsState();
}

class _RadioButtonWidgetDetailsState extends State<RadioButtonWidgetDetails> {

  int value =0;
  @override
  Widget build(BuildContext context) {
    var homeProvider = Provider.of<CartStateProvider>(context,listen: false);
    return   Column(
        children: List.generate(5, (index) => Column(
          children: [
            InkWell(
              onTap: (){
                setState(() {

                  homeProvider.radioButtonValue = index;

                });
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: Row(
                  children: [
                    Text("${widget.product.productsLowestWeightAvailable != null ?widget.product.productsLowestWeightAvailable *(index +1): index+1}",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
                    SizedBox(width: 3.w,),

                    Text(
                      widget.product.productsType ==
                          'weight'
                          ? 'KG'.tr().toString()
                          :'piece'.tr().toString(),
                    ),
                    Spacer(),
                    Radio(

                        activeColor: globals.accountColorsData['MainColor'] != null
                            ? Color(int.parse(globals.accountColorsData['MainColor']))
                            : SystemControls.MainColorz,

                        value: index, groupValue: homeProvider.radioButtonValue, onChanged: (v){

                    setState(() {
                      homeProvider.radioButtonValue = v;
                    });




                    })
                  ],
                ),
              ),
            ),

            Divider(),
          ],
        ),)
    );
  }
}


class RadioButtonWidget extends StatefulWidget {
  RadioButtonWidget({this.product});
  ProductShared product;


  @override
  _RadioButtonWidgetState createState() => _RadioButtonWidgetState();
}

class _RadioButtonWidgetState extends State<RadioButtonWidget> {

  int value ;
  @override
  Widget build(BuildContext context) {
    // var cart = Provider.of<CartStateProvider>(context,listen: false);
    return   Consumer<CartStateProvider>(builder: (context,cart,_){
      return Container(
        width: MediaQuery.of(context).size.width,
        child: widget.product.choices != null ? Wrap(
          children: List.generate(
              widget.product.choices.length,
                  (index) => Padding(
                padding: const EdgeInsets.only(
                    left: 8, right: 8),
                child: Container(
                  width: 15.w,
                  child: MaterialButton(
                    onPressed: () {
                      cart.changeSize(index,widget.product.choices[index].choicesId,widget.product.choices[index].choicesTitle);


                    },
                    child: Text(
                      "${widget.product.choices[index].choicesTitle}",
                      style: TextStyle(
                          color:
                          cart.selectedSize ==
                              index
                              ?    globals.accountColorsData['TextOnMainColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
                              : Colors.black),
                    ),
                    color: cart.selectedSize ==
                        index
                        ? globals.accountColorsData[
                    'MainColor'] !=
                        null
                        ? Color(int.parse(
                        globals.accountColorsData[
                        'MainColor']))
                        : SystemControls.MainColorz
                        : Colors.grey,
                    shape: RoundedRectangleBorder(
                        borderRadius:
                        BorderRadius.circular(10)),
                  ),
                ),
              )),
        ):Container(),
      );
    });
  }
}
