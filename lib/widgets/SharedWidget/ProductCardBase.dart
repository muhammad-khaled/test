import '../../provider/Drawer/DrawerProvider.dart';
import 'package:flutter/cupertino.dart';

import '../../dataControl/favsModule.dart';
import '../../provider/HomeProvider/HomeProvider.dart';
import '../../provider/productsInCartProvider.dart';
import '../../widgets/SharedWidget/ProductDeatilsScreen.dart';
import '../../widgets/ToolTipWidget.dart';
import 'package:provider/provider.dart';

import '../../globals.dart' as globals;

import '../../Models/HomeModel/sharedModel.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import '../../Controls.dart';
import 'package:easy_localization/easy_localization.dart';

import '../UI_Alert_Widgets.dart';

class ProductItemCardBase extends StatefulWidget {
  ProductItemCardBase(
      {Key key, this.product, this.appCurrency, this.cartList, this.token,this.width=55,this.height=40,this.arabicFont=10.5})
      : super(key: key);
  ProductShared product;
  List<dynamic> cartList;
  final String appCurrency;
  final String token;
  double width ;
  double height ;
  double arabicFont;

  @override
  State<ProductItemCardBase> createState() => _ProductItemCardBaseState();
}

class _ProductItemCardBaseState extends State<ProductItemCardBase> {
  GlobalKey _toolTipKey = GlobalKey();

  bool color = false;

  @override
  void initState() {
    // TODO: implement initState
    color = widget.product.isFav == 1 ? true : false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Provider.of<HomeProvider>(context,listen: false).changeSwiperIndex(0);

        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ProductItemsDetailsScreen(
                  id: widget.product.productsId,
                  video: widget.product.productsVideo,
                  appCurrency: widget.appCurrency,
                  product: widget.product,
                  cartList: widget.cartList,
                  token: widget.token,
                )));
      },
      child: Container(
        height: widget.height.h,
        width: widget.width.w,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(
                    color: globals.accountColorsData['BorderColor'] != null
                        ? Color(
                            int.parse(globals.accountColorsData['BorderColor']))
                        : SystemControls.BorderColorz,
                    width: .5)),
            child: Stack(
              fit: StackFit.expand,
              children: [
                Container(
                  padding: EdgeInsets.only(left: 8,right: 8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          Provider.of<HomeProvider>(context,listen: false).changeSwiperIndex(0);

                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ProductItemsDetailsScreen(
                                      id: widget.product.productsId,
                                      video: widget.product.productsVideo,
                                      appCurrency: widget.appCurrency,
                                      product: widget.product,
                                      cartList: widget.cartList,
                                      token: widget.token)));
                        },
                        child: Center(
                          child: Container(

                            child: CachedNetworkImage(
                              imageUrl: SystemControls()
                                  .GetImgPath(widget.product.productsImg, "medium"),
                              placeholder: (context, url) => Center(
                                child: SystemControls().circularProgress(),
                              ),
                              errorWidget: (context, url, error) => Center(
                                child: Icon(Icons.broken_image),
                              ),
                              fit: BoxFit.fitHeight,
                              height: 14.h,
                              width: widget.width.w,
                            ),
                          ),
                        ),
                      ),
                      Spacer(),

                      InkWell(
                        onTap: () {
                          Provider.of<HomeProvider>(context,listen: false).changeSwiperIndex(0);

                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ProductItemsDetailsScreen(
                                        id: widget.product.productsId,
                                        video: widget.product.productsVideo,
                                        appCurrency: widget.appCurrency,
                                        product: widget.product,
                                        cartList: widget.cartList,
                                        token: widget.token,
                                      )));
                        },
                        child: Text(
                          "${widget.product.category.categoriesTitle}",
                          style: TextStyle(color: globals.accountColorsData['TextdetailsColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,fontSize: 10.sp),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Spacer(),

                      InkWell(
                          onTap: () {
                            Provider.of<HomeProvider>(context,listen: false).changeSwiperIndex(0);

                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        ProductItemsDetailsScreen(
                                            id: widget.product.productsId,
                                            video: widget.product.productsVideo,
                                            appCurrency: widget.appCurrency,
                                            product: widget.product,
                                            cartList: widget.cartList,
                                            token: widget.token)));
                          },
                          child: Container(
                            height: EasyLocalization.of(context).currentLocale.languageCode =="en" ? 5.h:7.h,
                            child: Text("${widget.product.productsTitle}",
                                maxLines: 2, overflow: TextOverflow.ellipsis,style: TextStyle(color: globals.accountColorsData['TextHeaderColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,fontSize: 11.sp),),
                          )),

                      // product.productsPriceAfterSale.isNotEmpty ? Container(
                      //   width: 50.w,
                      //   child: Row(
                      //     crossAxisAlignment: CrossAxisAlignment.start,
                      //     children: [
                      //     Container(
                      //       width: 20.w,
                      //       child: Text("${product.productsPriceAfterSale} $appCurrency",style: TextStyle(
                      //         color: globals.accountColorsData[
                      //         'MainColor'] !=
                      //             null
                      //             ? Color(int.parse(globals
                      //             .accountColorsData['MainColor']))
                      //             : SystemControls.MainColorz,
                      //       ),   overflow: TextOverflow.ellipsis,),
                      //     ),
                      //     SizedBox(width: 3.w,),
                      //       Container(
                      //         width: 20.w,
                      //         child: Text("${product.productsPrice} $appCurrency",style: TextStyle(
                      //          decoration: TextDecoration.lineThrough
                      //         ), overflow: TextOverflow.ellipsis),
                      //       ),
                      //     ],
                      //   ),
                      // ):Text("${product.productsPrice} $appCurrency",style: TextStyle(
                      //     // decoration: TextDecoration.lineThrough
                      // ),   overflow: TextOverflow.ellipsis,),
                      RichText(
                      maxLines: 1,


                        text:  TextSpan(

                          text: widget.product.productsPriceAfterSale.isNotEmpty ? "${widget.product.productsPriceAfterSale} ${widget.appCurrency}" : "${widget.product.productsPrice} ${widget.appCurrency}",

                          style: TextStyle(
                            color:  globals.accountColorsData['MainColor'] != null
                                ? Color(int.parse(globals.accountColorsData['MainColor']))
                                : SystemControls.MainColorz,fontWeight: FontWeight.bold,fontSize: EasyLocalization.of(context).currentLocale.languageCode =="ar" ? widget.arabicFont.sp:12.5.sp
                          ),
                          children: [

                            TextSpan(text: '   '),
                            widget.product.productsPriceAfterSale.isNotEmpty ? TextSpan(


                                text: '${widget.product.productsPrice} ${widget.appCurrency}',
                                style: TextStyle(fontSize: EasyLocalization.of(context).currentLocale.languageCode =="ar" ? (widget.arabicFont-.5).sp:10.sp,fontWeight: FontWeight.normal,decoration: TextDecoration.lineThrough,color: globals.accountColorsData['TextdetailsColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz)):TextSpan(text: ""),

                          ],
                        ),
                      ),

                      Spacer(),
                      widget.product.productsAvailable == "0"
                          ? MyTooltip(
                              message: "message",
                              child: Consumer<CartStateProvider>(
                                builder: (context, homeProvider, _) {
                                  return Center(
                                    child: Container(
                                      width:
                                          MediaQuery.of(context).size.width ,
                                      child: MaterialButton(
                                        elevation: 0,

                                        height: 40,
                                        onPressed: () {
                                          // homeProvider.showAddToCart(
                                          //     context, product, cartList);
                                        },
                                        color: Colors.grey,
                                        child: Text(
                                          "addToCart".tr().toString(),
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(8),
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              )):
                          // : product.choices.length <= 0
                          //     ?
                      Consumer<CartStateProvider>(
                                  builder: (context, cart, _) {
                                    return Center(
                                      child: Container(
                                        width:
                                            MediaQuery.of(context).size.width ,
                                        child: MaterialButton(
                                          elevation: 0,
                                          height: 40,

                                          onPressed: () {
                                            cart.resetSize();
                                            cart.showAddToCartProduct(
                                                context, widget.product, widget.cartList,widget.appCurrency,cart);
                                          },
                                          color: globals.accountColorsData[
                                                      'MainColor'] !=
                                                  null
                                              ? Color(int.parse(
                                                  globals.accountColorsData[
                                                      'MainColor']))
                                              : SystemControls.MainColorz,
                                          child: Text(
                                            "addToCart".tr().toString(),
                                            style: TextStyle(color: Colors.white),
                                          ),
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(8),

                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              // : Center(
                              //     child: Container(
                              //       width: MediaQuery.of(context).size.width,
                              //
                              //       child: MaterialButton(
                              //         height: 38,
                              //         onPressed: () {
                              //
                              //         },
                              //         color:
                              //             globals.accountColorsData['MainColor'] !=
                              //                     null
                              //                 ? Color(int.parse(globals
                              //                     .accountColorsData['MainColor']))
                              //                 : SystemControls.MainColorz,
                              //         child: Text(
                              //           "showDetails".tr().toString(),
                              //           style: TextStyle(color: Colors.white),
                              //         ),
                              //         shape: RoundedRectangleBorder(
                              //           borderRadius: BorderRadius.circular(10),
                              //         ),
                              //       ),
                              //     ),
                              //   ),
                      SizedBox(height: 8,),
                    ],
                  ),
                ),
                Positioned(
                  top: 11.h,
                  left: 0,
                  child: widget.product.productsPriceAfterSale.isNotEmpty
                      ? Container(
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.only(topLeft: Radius.circular(0)),
                              // color:
                              // globals.accountColorsData['MainColor'] != null
                              //     ? Color(int.parse(
                              //     globals.accountColorsData['MainColor']))
                              //     : SystemControls.MainColorz
                              color: Colors.red),
                          child: Center(
                              child: Text(
                                  // ${"off".tr().toString()}
                            "${((1 - (double.parse("${widget.product.productsPriceAfterSale}") / double.parse("${widget.product.productsPrice}"))) * 100).toInt()}%",
                            style: TextStyle(color:  globals.accountColorsData['TextOnMainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz),
                          )),
                          height: 3.h,
                          width: 12.w,
                        )
                      : Container(),
                ),
                Positioned(
                    top: 0,
                    right: 0,
                    child: Consumer2<HomeProvider,DrawerProvider>(
                      builder: (context, home,fav, _) {
                        return IconButton(

                            splashColor: Colors.transparent,
                            hoverColor: Colors.transparent,
                            highlightColor: Colors.transparent,

                            onPressed: () async {
                              fav.fetchFavoriteProduct(context);

                              if (widget.token == "") {
                                ErrorDialogAlertGoTOLogin(
                                    context,
                                    "mustLogin".tr().toString(),
                                    EasyLocalization.of(context)
                                        .currentLocale
                                        .languageCode);
                              } else {

                                setState(() {
                                  color =! color;

                                });
                                if (widget.product.isFav == 1) {
                                  widget.product.isFav = 0;

                                } else {
                                  widget.product.isFav = 1;
                                }
                                // onFavSubmit(_meals);

                                var res = await updateFavsItem(
                                    EasyLocalization.of(context)
                                        .currentLocale
                                        .languageCode,
                                    widget.token,
                                    widget.product.productsId,context);
                                if (res.status == 200) {
                                  SuccDialogAlertStaySameLayout(
                                      context, res.message,ValueKey("Product${widget.product.productsId}"));
                                } else {
                                  if (res.status == 401) {
                                    SystemControls().LogoutSetUserData(
                                        context,
                                        EasyLocalization.of(context)
                                            .currentLocale
                                            .languageCode);
                                  }

                                  if (widget.product.isFav == 1) {
                                    widget.product.isFav = 0;
                                  } else {
                                    widget.product.isFav = 1;
                                  }
                                  // onFavSubmit(_meals);

                                  ErrorDialogAlert(context, res.message);
                                }
                              }
                              home.notifyListeners();
                            },
                            icon: ( widget.product.isFav == 1 || color == true)
                                ? Icon(
                                    Icons.favorite,
                                    size: 25,
                                    color: Colors.red,
                                  )
                                : Icon(
                                    Icons.favorite,
                                    size: 25,
                              color: globals.accountColorsData['BorderColor'] != null
                                  ? Color(
                                  int.parse(globals.accountColorsData['BorderColor']))
                                  : SystemControls.BorderColorz,
                                  ));
                      },
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
