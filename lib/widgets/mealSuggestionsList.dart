  import 'package:flutter/material.dart';
import '../dataControl/dataModule.dart';
import '../Controls.dart';
import '../screens/IteamDetails2.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../locale/app_localization.dart';

import '../globals.dart' as globals;

//import 'package:cached_network_image/cached_network_image.dart';

class SuggestionList extends StatelessWidget{

  List<dynamic> _CartMeals = <dynamic>[];
  String Lang;
  String AppCurrency;
  String UserToken;
  int design_Control;
  bool hasRegistration;
  final Function(dynamic,String) onClickIcon;

  SuggestionList(this._meals, this._CartMeals,this.Lang, this.AppCurrency,
      this.UserToken,this.design_Control,this.onClickIcon,this.hasRegistration);

  List<dynamic> _meals = <dynamic>[];
  int selectedIndex = -1;
  onAddToCart(int index){
    onClickIcon(_meals[index],'products_');
  }
  Widget build(BuildContext context) {
    if(_meals != null && _meals.length > 0){
      return Container(
        margin: EdgeInsets.only(right: 5,left: 5,top: 5,bottom: 5),
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            padding: EdgeInsets.zero,
            itemCount: _meals.length,
            itemBuilder: (context, index) {
              bool findOfferPrice = false;
              if(_meals[index]['products_price_after_sale']!=null&&
                  _meals[index]['products_price_after_sale']!=""){
                findOfferPrice = true;
              }
              return InkWell(
                child: Container(
                  width: 160,
                  margin: EdgeInsets.all(5),
                  padding: EdgeInsets.all(0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(SystemControls.RadiusCircValue)),
                    color: Colors.white,
                    border: Border.all(
                      width: 1,
                      color: globals.accountColorsData['BorderColor']!=null
                          ? Color(int.parse(globals.accountColorsData['BorderColor'])) : SystemControls.BorderColorz,
                    ),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(
                        Radius.circular(SystemControls.RadiusCircValue)),
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            height: 115,
                            child: Stack(
                              children: <Widget>[
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: CachedNetworkImage(
                                    imageUrl: SystemControls().GetImgPath(_meals[index]['products_img'],"medium"),
                                    placeholder: (context, url) => Center(child: SystemControls().circularProgress(),),
                                    errorWidget: (context, url, error) => Center(child: Icon(Icons.broken_image),),
                                    fit: BoxFit.contain,
                                    height: 120,
                                  ),
                                ),
                                Column(
                                  children: <Widget>[
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      child: Container(
                                        //height: 50,
                                        padding: EdgeInsets.all(4),
                                        decoration: BoxDecoration(
                                          color: globals.accountColorsData['MainColor']!=null
                                              ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,//Colors.green,
                                          borderRadius: BorderRadius.only(
                                            bottomRight: Radius.circular(SystemControls.RadiusCircValue),),
                                        ),
                                        child: RichText(
                                          textAlign: TextAlign.start,
                                          text: new TextSpan(
                                            children: <TextSpan>[
                                              findOfferPrice?(
                                                  TextSpan(
                                                    text: _meals[index]['products_price']==null? "n"
                                                        : _meals[index]['products_price']+" "+ AppCurrency,
                                                    style: new TextStyle(
                                                      fontSize: SystemControls.font5,
                                                      fontFamily: Lang,
                                                      color: globals.accountColorsData['TextOnMainColor']!=null
                                                          ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                                                      fontWeight: FontWeight.normal,
                                                      decoration: TextDecoration.lineThrough,
                                                    ),
                                                  )
                                              ):(
                                                  TextSpan()
                                              ),
                                              findOfferPrice?(
                                                  TextSpan(text: '\n',)
                                              ):(
                                                  TextSpan()
                                              ),
                                              new TextSpan(
                                                text: findOfferPrice
                                                    ? _meals[index]['products_price_after_sale']+" "+ AppCurrency
                                                    : _meals[index]['products_price']+" "+ AppCurrency,
                                                style: TextStyle(
                                                  fontSize: SystemControls.font4,
                                                  fontFamily: Lang,
                                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.only(top: 3,right: 10,left: 10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    //mainAxisSize: MainAxisSize.max,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 8,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text(_meals[index]['products_title'],//"mix grill",
                                              style: TextStyle(
                                                fontFamily: Lang,
                                                fontSize: SystemControls.font2,
                                                color: globals.accountColorsData['TextHeaderColor']!=null
                                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                                fontWeight: FontWeight.normal,
                                                height: 1,
                                              ),
                                              maxLines: 1,
                                              textAlign: TextAlign.start,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ), //Meal title & cal count
                                  Padding(padding: EdgeInsets.all(4)),
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 2,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text(_meals[index]['category']==null?
                                            (""):(
                                                _meals[index]['category']['categories_title'].toString()
                                            ),
                                              style: TextStyle(
                                                fontFamily: Lang,
                                                fontSize: SystemControls.font3,
                                                color: globals.accountColorsData['TextdetailsColor']!=null
                                                    ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                fontWeight: FontWeight.normal,
                                                height: 0.8,
                                              ),
                                              maxLines: 1,
                                              textAlign: TextAlign.start,
                                            ),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: InkWell(
                                          child: Container(
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: <Widget>[
                                                hasRegistration?(
                                                    Container(
                                                      decoration: SystemControls().mealButtonDecoration(),
                                                      height: 30,
                                                      width: 30,
                                                      child: InkWell(
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          children: <Widget>[
                                                            SystemControls().GetSVGImagesAsset(
                                                                "assets/Icons/cart.svg",
                                                                20,
                                                                Colors.black),
                                                          ],
                                                        ),
                                                      ),
                                                    )
                                                ):(
                                                    Container()),
                                              ],
                                            ),
                                          ),
                                          onTap: (){
                                            onAddToCart(index);
                                          },
                                        ),
                                      ),
                                    ],
                                  ), //Category title
                                ],
                              ),
                            ),
                            flex: 3,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                onTap: ()async{
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(
                          builder: (
                              BuildContext context) =>
                              ItemDetails(null,_CartMeals,Lang,AppCurrency,
                                  UserToken,design_Control,false,
                                  _meals[index]['products_id'].toString(),hasRegistration,false,0,_meals[index]["products_available"])
                      )
                  );
                  //TODO Use new Item Details
                  /*
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) =>
                      MealDetails(null,_CartMeals,Lang,AppCurrency,
                              UserToken,design_Control,false,
                              _meals[index]['products_id'].toString(),hasRegistration)//_meals[index])
                      ));

                   */
                },

              );
            }
        ),
      );
    }
    else{
      return Container();
    }
  }
}