





import '../../Controls.dart';
import '../../globals.dart' as globals;

import '../../Models/DrawerModel/FavoriteModel.dart';
import '../../provider/Drawer/DrawerProvider.dart';
import '../../widgets/SharedWidget/ProductItmsList.dart';
import '../../widgets/SharedWidget/ProgressIndecator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:sizer/sizer.dart';
class FavoritePage extends StatefulWidget {
   FavoritePage({Key key,this.token,this.appCurrency,this.cartList}) : super(key: key);

  final String appCurrency;
  final String token;
  List<dynamic> cartList;


  @override
  FavoritePageState createState() => FavoritePageState();
}

class FavoritePageState extends State<FavoritePage> {
  @override
  void initState() {
    // TODO: implement initState
    Provider.of<DrawerProvider>(context,listen: false).fetchFavoriteProduct(context);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text("favorite".tr().toString()),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
            Consumer<DrawerProvider>(builder: (context,drawer,_){
              return  StreamBuilder<FavoriteModel>(
                     stream: drawer.streamFavorite,
                  builder: (context,snap){
                   if(snap.hasData){
                     return snap.data.data.favs.length ==0? Container(
                       height: 50.h,
                       width: MediaQuery.of(context).size.width,
                       child: Center(
                         child: Text("noItemFav".tr().toString(),style: TextStyle(fontWeight: FontWeight.bold,color:  globals.accountColorsData[
                         'MainColor'] !=
                             null
                             ? Color(int.parse(
                             globals.accountColorsData[
                             'MainColor']))
                             : SystemControls.MainColorz,fontSize: 14.sp),textAlign: TextAlign.center,),
                       ),
                     ): ProductsGridView(
                       cartList: widget.cartList,
                       appCurrency: widget.appCurrency,
                       search: null ,
                       products: snap.data.data.favs,
                       token: widget.token,
                     );
                   }return Container(
                     child: Center(
                       child: Indicator(),
                     ),
                   );
                  });
            })
            ],
          ),
        ),
      ),
    );
  }
}
