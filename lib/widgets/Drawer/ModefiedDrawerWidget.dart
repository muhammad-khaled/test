import 'dart:convert';

import '../../dataControl/cartModule.dart';
import '../../dataControl/dataModule.dart';
import '../../dataControl/userModule.dart';
import '../../provider/HomeProvider/HomeProvider.dart';
import '../../provider/productsInCartProvider.dart';
import '../../screens/Branches.dart';
import '../../screens/ChooseLayout.dart';
import '../../screens/HomePageScreenTheamTwo/HomePageScreenTheamTwo.dart';
import '../../screens/HomeTheamOne/HomePageScreenTheamOne.dart';
import '../../screens/aboutUs.dart';
import '../../screens/login.dart';
import '../../screens/myAddresses.dart';
import '../../screens/opinion.dart';
import '../../screens/ordersList.dart';
import '../../screens/privacy.dart';
import '../../screens/updateUser.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import '../../globals.dart' as globals;

import '../../Controls.dart';
import '../../main.dart';
import '../UI_Alert_Widgets.dart';
import 'FavoritePage.dart';

class ModifiedDrawerWidget extends StatefulWidget {
  ModifiedDrawerWidget(
      {Key key,
      this.token,
      this.hasRegistration,
      this.isLogin,
      this.currency,
      this.appInfo,
      this.drawer})
      : super(key: key);
  final String token;
  final String currency;
  final dynamic appInfo;
  final bool hasRegistration;
  final bool isLogin;
  final Function drawer;

  @override
  State<ModifiedDrawerWidget> createState() => _ModifiedDrawerWidgetState();
}

class _ModifiedDrawerWidgetState extends State<ModifiedDrawerWidget> {
  Future<CartList> localCart = read_from_file();

  Future<ResponseApi> resp2;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    var homeProvider = Provider.of<HomeProvider>(context,listen: false);
    homeProvider.GetCustomerData();
  }

  @override
  Widget build(BuildContext context) {
    // var homeProvider = Provider.of<HomeProvider>(context,listen: false);
    // homeProvider.GetCustomerData();
    return Consumer<HomeProvider>(
      builder: (context, home, _) {

        return Scaffold(
          backgroundColor: globals
              .accountColorsData[
          'BorderColor'] !=
              null
              ? Color(int.parse(globals
              .accountColorsData[
          'BorderColor']))
              .withOpacity(.2)
              : SystemControls.BorderColorz,
          body: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: [
                Consumer<CartStateProvider>(
                  builder: (context, cart, _) {
                    return FutureBuilder<CartList>(
                      future: localCart,
                      builder: (context, Cartsnap) {
                        if (Cartsnap.connectionState ==
                            ConnectionState.done &&
                            Cartsnap.hasData) {
                          cart.setCartList(Cartsnap.data.Cmeals);

                          CartStateProvider cartState =
                          Provider.of<CartStateProvider>(context,
                              listen: false);
                          if (cartState.productsCount == "0" &&
                              cart.cartList.length != 0) {
                            cartState.setCurrentProductsCount(
                                cart.cartList.length.toString());
                          }
                          return Container();
                        }
                        return Container();
                      },
                    );
                  },
                ),
                Container(
                  color: Colors.white,
                  padding: EdgeInsets.only(top: 35, right: 40, left: 40),
                  height: 30.h,
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: [
                      Image.asset(

                                'assets/drawerLogo.png',
                        height: 130,
                        fit: BoxFit.contain,
                      ),
                      SizedBox(
                        height: 3.h,
                      ),
                      home.userName != null
                          ? RichText(
                        text: TextSpan(
                            text: "${"hello".tr().toString()} ", //"home",
                            style: TextStyle(color: globals.accountColorsData['TextHeaderColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,fontSize: 12.sp,fontWeight: FontWeight.bold),
                            children: <TextSpan>[
                              new TextSpan(
                                  text: ' ${home.userName}',
                                  style: new TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 17)),
                            ]),
                      )
                          : Text(""),

                    ],
                  ),
                ),

                Container(
                 decoration: BoxDecoration(
                   borderRadius: BorderRadius.only(topRight: Radius.circular(30),topLeft: Radius.circular(30)),
                   // color: globals
                   //     .accountColorsData[
                   // 'BorderColor'] !=
                   //     null
                   //     ? Color(int.parse(globals
                   //     .accountColorsData[
                   // 'BorderColor']))
                   //     .withOpacity(.2)
                   //     : SystemControls.BorderColorz,
                 ),
                  child: Column(
                    children: [
                      Container(
                        height: 3.h,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.white,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(topRight: Radius.circular(50),topLeft: Radius.circular(50)),
                            color: globals
                                .accountColorsData[
                            'BorderColor'] !=
                                null
                                ? Color(int.parse(globals
                                .accountColorsData[
                            'BorderColor']))
                                .withOpacity(.2)
                                : SystemControls.BorderColorz
                          ),
                        ),
                      ),
                      SystemControls.theme !="2"?   DrawerContainer(onTap: (){
                        home.changeIndex(-2,-2);
                        home.fetchHomeItems(context, home.currentPage);
                        widget.drawer();
                      },iconData: FontAwesomeIcons.home,title: "home".tr().toString(),):Container(),
                     widget.hasRegistration == true ? widget.isLogin == true ?   DrawerContainer(onTap: (){Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  updateUser(EasyLocalization.of(context).currentLocale.languageCode, widget.token, SystemControls.designControl)));},iconData: FontAwesomeIcons.userAlt,title: "profile".tr().toString(),):
                     widget.token == "" ? DrawerContainer(onTap: (){
                                        Navigator.push(
                         context,
                         MaterialPageRoute(
                             builder: (BuildContext context) =>
                                 login(EasyLocalization.of(context).currentLocale.languageCode, SystemControls.designControl)));}


                                 ,iconData: FontAwesomeIcons.signInAlt,title: "login".tr().toString(),):Container():DrawerContainer(onTap: (){Navigator.push(
                         context,
                         MaterialPageRoute(
                             builder: (BuildContext context) =>
                                 updateUser(EasyLocalization.of(context).currentLocale.languageCode, widget.token, SystemControls.designControl)));},iconData: FontAwesomeIcons.userAlt,title: "profile".tr().toString(),),

                      widget.hasRegistration == true ? DrawerContainer(onTap: (){  if (widget.isLogin == false) {
                        ErrorDialogAlertGoTOLogin(
                            context,
                            "mustLogin".tr().toString(),
                            EasyLocalization.of(context).currentLocale.languageCode);
                      } else {
                        // Navigator.pop(context);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext
                                context) =>
                                    MyAddresses(
                                        EasyLocalization.of(context).currentLocale.languageCode,
                                        widget.token,
                                        widget.currency,
                                        SystemControls.designControl)));
                      }},iconData: FontAwesomeIcons.mapMarkerAlt,title: "address".tr().toString(),):Container(),
                      widget.hasRegistration == true ? Consumer<CartStateProvider>(builder: (context,cart,_){
                        return DrawerContainer(onTap: (){  if (widget.isLogin == false) {
                          ErrorDialogAlertGoTOLogin(
                              context,
                              "mustLogin".tr().toString(),
                              EasyLocalization.of(context).currentLocale.languageCode);
                        } else {
                          // Navigator.pop(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext
                                  context) =>
                                      FavoritePage(token: widget.token,appCurrency: widget.currency,cartList: cart.cartList,)));
                        }},iconData: FontAwesomeIcons.solidHeart  ,title: "favorite".tr().toString(),);
                      },):Container(),
                      widget.hasRegistration == true ? DrawerContainer(onTap: (){  if (widget.isLogin == false) {
                        ErrorDialogAlertGoTOLogin(
                            context,
                            "mustLogin".tr().toString(),
                            EasyLocalization.of(context).currentLocale.languageCode);
                      } else {
                        // Navigator.pop(context);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext
                                context) =>
                                    OrderList(
                                        EasyLocalization.of(context).currentLocale.languageCode,
                                        widget.token,
                                        widget.currency,
                                        SystemControls.designControl)));
                      }},iconData: FontAwesomeIcons.fileInvoiceDollar,title: "orders".tr().toString(),):Container(),
                      DrawerContainer(onTap: (){      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  opinion(
                                      EasyLocalization.of(context).currentLocale.languageCode,
                                      SystemControls.designControl,
                                      widget.token)));},iconData: FontAwesomeIcons.chalkboardTeacher,title: "opinion".tr().toString(),),
                      DrawerContainer(onTap: (){        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  AboutUs(jsonDecode(widget.appInfo), EasyLocalization.of(context).currentLocale.languageCode,
                                      SystemControls.designControl)));},iconData: FontAwesomeIcons.infoCircle,title: "aboutUs".tr().toString(),),
                      DrawerContainer(onTap: (){            Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  PrivacyPolicy(
                                      jsonDecode(widget.appInfo), EasyLocalization.of(context).currentLocale.languageCode,
                                      SystemControls.designControl)));},iconData: FontAwesomeIcons.shieldAlt,title: "privacyPolicy".tr().toString(),),
                      DrawerContainer(onTap: () {
                        Navigator.pop(context);
                        print("on click button 22 ");
                        SystemControls.width = 45;
                        if (EasyLocalization.of(context).currentLocale.languageCode == "en") {
                          resp2 = InitialApi("ar",context);
                          Center(
                            child:
                            SystemControls().circularProgress(),
                          );

                          SystemControls()
                              .storeDataToPreferences("lang", "ar")
                              .then((value) {
                            //appLanguage.changeLanguage(Locale("ar"));
                            // AppLocalizations.of(context).load();
                            EasyLocalization.of(context).setLocale(Locale('ar', 'EG'));
                            print('\nSaving Done $value....\n');
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MyApp()),
                                ModalRoute.withName('/'));
                          });
                        } else {
                          Center(
                            child:
                            SystemControls().circularProgress(),
                          );
                          //AppLocalizations(Locale('ar', ''));
                          resp2 = InitialApi("en",context);

                          SystemControls()
                              .storeDataToPreferences("lang", "en")
                              .then((value) {
                            EasyLocalization.of(context).setLocale(Locale('en', 'EN'));
                            print('\nSaving Done $value....\n');
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MyApp()),
                                ModalRoute.withName('/'));
                          });
                        }

                      },iconData: FontAwesomeIcons.globe,title: "changeLanguage".tr().toString(),),
                      SystemControls.branchesValid ?
                      DrawerContainer(onTap: (){                   Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext
                              context) =>
                                  BranchesLayout(
                                      EasyLocalization.of(context).currentLocale.languageCode,
                                      widget.token,
                                      SystemControls.designControl)));},iconData: FontAwesomeIcons.codeBranch,title: "branches".tr().toString(),):Container(),
                  SystemControls.branchesValid ?
                      DrawerContainer(onTap: (){                                       Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext
                              context) =>
                                  ChooseLayout(
                                      EasyLocalization.of(context).currentLocale.languageCode,
                                      SystemControls.designControl)));},iconData: Icons.alt_route_rounded,title: "Chosser".tr().toString(),):Container(),

                        widget.isLogin == true ? DrawerContainer(onTap: ()async{
                          var res = await logoutApi(widget.token,EasyLocalization.of(context).currentLocale.languageCode);
                          if (res.status == 200) {
                            print(res.message);
                          }
                          SystemControls().LogoutSetUserData(context, EasyLocalization.of(context).currentLocale.languageCode);
                          switch(SystemControls.theme){
                            case "1":
                              Navigator.pushReplacement(context, MaterialPageRoute(  builder: (BuildContext context) => HomePageScreenTheamOne()));
                              break;
                            case "2":
                              Navigator.pushReplacement(context, MaterialPageRoute(  builder: (BuildContext context) => HomePageScreenTheamTwo()));
                              break;
                          }
                        },iconData: FontAwesomeIcons.signOutAlt,title: "logout".tr().toString(),):Container()

                    ],
                  ),

                )
              ],
            ),
          ),
        );
      },
    );
  }
}


class DrawerContainer extends StatelessWidget {
  const DrawerContainer({Key key,this.iconData,this.title,this.onTap}) : super(key: key);
   final String title;
   final IconData iconData;
   final Function onTap;
  @override
  Widget build(BuildContext context) {
    return      InkWell(
      onTap: onTap,
      child: Container(
        height: 50,
        width: MediaQuery.of(context).size.width,
        child: Row(
          children: [
            SizedBox(width: 5.w,),

            Icon(iconData ,color: globals
                .accountColorsData[
            'BorderColor'] !=
                null
                ? Color(int.parse(globals
                .accountColorsData[
            'BorderColor'],),)

                : SystemControls.BorderColorz,size: 22,),
            SizedBox(width: 5.w,),
            Text("$title",style: TextStyle(color: globals.accountColorsData['TextHeaderColor']!=null
                ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,fontSize: 12.sp),),
            Spacer(),
         Icon(Icons.chevron_right,color: globals
                .accountColorsData[
            'BorderColor'] !=
                null
                ? Color(int.parse(globals
                .accountColorsData[
            'BorderColor']))

                : SystemControls.BorderColorz,),
            SizedBox(width: 5.w,)
          ],
        ),
      ),
    );
  }
}
