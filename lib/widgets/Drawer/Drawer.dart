



import 'dart:convert';

import '../../dataControl/cartModule.dart';
import '../../dataControl/dataModule.dart';

import '../../Controls.dart';
import '../../dataControl/userModule.dart';
import '../../provider/HomeProvider/HomeProvider.dart';
import '../../provider/productsInCartProvider.dart';
import '../../screens/Branches.dart';
import '../../screens/ChooseLayout.dart';
import '../../screens/HomePageScreenTheamTwo/HomePageScreenTheamTwo.dart';
import '../../screens/HomeTheamOne/HomePageScreenTheamOne.dart';
import '../../screens/aboutUs.dart';
import '../../screens/login.dart';
import '../../screens/myAddresses.dart';
import '../../screens/opinion.dart';
import '../../screens/ordersList.dart';
import '../../screens/privacy.dart';
import '../../screens/updateUser.dart';
import '../../widgets/Drawer/FavoritePage.dart';
import '../../widgets/SharedWidget/TextStyles.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';
import '../UI_Alert_Widgets.dart';

class DrawerWidget extends StatelessWidget {
   DrawerWidget({Key key,this.token,this.hasRegistration,this.isLogin,this.currency,this.appInfo,this.drawer}) : super(key: key);
   final String token;
   final String currency;
   final dynamic appInfo;
   final bool hasRegistration;
   final bool isLogin;
   final Function drawer;
  Future<CartList> localCart = read_from_file();
   Future<ResponseApi> resp2;
  @override
  Widget build(BuildContext context) {
    var homeProvider = Provider.of<HomeProvider>(context,listen: false);
    homeProvider.GetCustomerData();

    return Consumer<HomeProvider>(builder: (context,home,_){
      return  SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            Consumer<CartStateProvider>(
              builder: (context, cart, _) {
                return FutureBuilder<CartList>(
                  future: localCart,
                  builder: (context, Cartsnap) {
                    if (Cartsnap.connectionState ==
                        ConnectionState.done &&
                        Cartsnap.hasData) {
                      cart.setCartList(Cartsnap.data.Cmeals);

                      CartStateProvider cartState =
                      Provider.of<CartStateProvider>(context,
                          listen: false);
                      if (cartState.productsCount == "0" &&
                          cart.cartList.length != 0) {
                        cartState.setCurrentProductsCount(
                            cart.cartList.length.toString());
                      }
                      return Container();
                    }
                    return Container();
                  },
                );
              },
            ),
            Container(
              padding: EdgeInsets.only(top: 20, right: 40, left: 40),
              height: 150,
              child: Center(
                child: Image.asset(
                  EasyLocalization.of(context).currentLocale.languageCode == "ar" ? 'assets/logoNameSide.png':'assets/logoNameSideEn.png',
                  height: 80,
                ),
              ),
            ),
            Divider(),
            home.userName != null ?    Container(
              child: ListTile(

                title:  home.userName != null ? RichText(
                  text: TextSpan(
                      text:
                      "${"hello".tr().toString()} ", //"home",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 18,

                      ),
                      children: <TextSpan>[
                        new TextSpan(
                            text: ' ${home.userName}',
                            style: new TextStyle(fontWeight: FontWeight.bold,fontSize: 17)),
                      ]
                  ),

                ):Text(""),
                onTap: () {
                },
              ),
            ):Container(width: 0,height: 0,),

            home.userName != null ?   Divider():Container(width: 0,height: 0,),
         SystemControls.AccountId !="7"?   Container(
              child: ListTile(
                title: Text(
                  "home".tr().toString(), //"home",
                  style: textStyle(context),
                ),
                onTap: () {
                  home.changeIndex(-2,-2);
                  home.fetchHomeItems(context, home.currentPage);
                  drawer();
                },
              ),
            ): Container(),
            SystemControls.AccountId !="7"? Divider():Container(),
            hasRegistration == true ?(
            isLogin == false ?setLoginArea(context):   Container(
              color: itemBGColor(),
              child: ListTile(
                title: Text(
                  "profile".tr().toString(),
                  style: textStyle(context),
                ),
                onTap: () {
                  // Navigator.pop(context);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              updateUser(EasyLocalization.of(context).currentLocale.languageCode, token, SystemControls.designControl)));
                },
              ),
            )
            ):Container(height: 0,width: 0,),
            Divider(),
            hasRegistration == true
                ? (Column(
              children: <Widget>[
                Container(
                  color: itemBGColor(),
                  child: ListTile(
                    title: Text(
                      "addresses".tr().toString(),
                      style: textStyle(context),
                    ),
                    onTap: () {
                      if (isLogin == false) {
                        ErrorDialogAlertGoTOLogin(
                            context,
                            "mustLogin".tr().toString(),
                            EasyLocalization.of(context).currentLocale.languageCode);
                      } else {
                        // Navigator.pop(context);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext
                                context) =>
                                    MyAddresses(
                                        EasyLocalization.of(context).currentLocale.languageCode,
                                        token,
                                        currency,
                                        SystemControls.designControl)));
                      }
                    },
                  ),
                ), //my addresses
                Divider(),

                 Consumer<CartStateProvider>(builder: (context,cart,_){
                   return    Container(
                     color: itemBGColor(),
                     child: ListTile(
                       title: Text(
                         "favorite".tr().toString(),
                         style: textStyle(context),
                       ),
                       onTap: () {
                         if (isLogin == false) {
                           ErrorDialogAlertGoTOLogin(
                               context,
                               "mustLogin".tr().toString(),
                               EasyLocalization.of(context).currentLocale.languageCode);
                         } else {
                           // Navigator.pop(context);
                           Navigator.push(
                               context,
                               MaterialPageRoute(
                                   builder: (BuildContext
                                   context) =>
                                       FavoritePage(token: token,appCurrency: currency,cartList: cart.cartList,)));
                         }
                       },
                     ),
                   );
                 }) ,//favorite
                Divider(),

                Container(
                  color: itemBGColor(),
                  child: ListTile(
                    title: Text(
                      "orders".tr().toString(),
                      style: textStyle(context),
                    ),
                    onTap: () {
                      if (isLogin == false) {
                        ErrorDialogAlertGoTOLogin(
                            context,
                            "mustLogin".tr().toString(),
                            EasyLocalization.of(context).currentLocale.languageCode);
                      } else {
                        // Navigator.pop(context);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext
                                context) =>
                                    OrderList(
                                        EasyLocalization.of(context).currentLocale.languageCode,
                                        token,
                                        currency,
                                        SystemControls.designControl)));
                      }
                    },
                  ),
                ), //orders
                Divider(),
              ],
            ))
                : (Container()),
            Container(
              child: ListTile(
                title: Text(
                  "opinion".tr().toString(),
                  style: textStyle(context),
                ),
                onTap: () {
                  // Navigator.pop(context);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              opinion(
                                 EasyLocalization.of(context).currentLocale.languageCode,
                                  SystemControls.designControl,
                                  token)));
                },
              ),
            ), //opinion
            Divider(),

            Container(
              child: ListTile(
                title: Text(
                  "aboutUs".tr().toString(),
                  style: textStyle(context),
                ),
                onTap: () {
                  // Navigator.pop(context);


                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              AboutUs(jsonDecode(appInfo), EasyLocalization.of(context).currentLocale.languageCode,
                                  SystemControls.designControl)));
                },
              ),
            ), //aboutUs
            Divider(),
            Container(
              child: ListTile(
                title: Text(
                  "privacyPolicy".tr().toString(),
                  style: textStyle(context),
                ),
                onTap: () {
                  // Navigator.pop(context);


                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              PrivacyPolicy(
                                  jsonDecode(appInfo), EasyLocalization.of(context).currentLocale.languageCode,
                                  SystemControls.designControl)));
                },
              ),
            ), //aboutUs
            Divider(),
            Container(
              child: ListTile(
                title: Text(
                  "changeLanguage".tr().toString(),
                  style: textStyle(context),
                ),
                onTap: () {
                  Navigator.pop(context);
                  print("on click button 22 ");
                  SystemControls.width = 45;
                  if (EasyLocalization.of(context).currentLocale.languageCode == "en") {
                   resp2 = InitialApi("ar",context);
                    Center(
                      child:
                      SystemControls().circularProgress(),
                    );

                    SystemControls()
                        .storeDataToPreferences("lang", "ar")
                        .then((value) {
                      //appLanguage.changeLanguage(Locale("ar"));
                      // AppLocalizations.of(context).load();
                      EasyLocalization.of(context).setLocale(Locale('ar', 'EG'));
                      print('\nSaving Done $value....\n');
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MyApp()),
                          ModalRoute.withName('/'));
                    });
                  } else {
                    Center(
                      child:
                      SystemControls().circularProgress(),
                    );
                    //AppLocalizations(Locale('ar', ''));
                    resp2 = InitialApi("en",context);

                    SystemControls()
                        .storeDataToPreferences("lang", "en")
                        .then((value) {
                      EasyLocalization.of(context).setLocale(Locale('en', 'EN'));
                      print('\nSaving Done $value....\n');
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MyApp()),
                          ModalRoute.withName('/'));
                    });
                  }

                },
              ),
            ),
            Divider(),
          isLogin == true
                ? (setLogoutArea(context))
                : (Container()),

            SystemControls.branchesValid
                ? (Column(
              children: <Widget>[
                Container(
                  child: ListTile(
                    title: Text(
                      "branches".tr().toString(),
                      style: textStyle(context),
                    ),
                    onTap: () {
                      // Navigator.pop(context);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext
                              context) =>
                                  BranchesLayout(
                                      EasyLocalization.of(context).currentLocale.languageCode,
                                      token,
                                     SystemControls.designControl)));
                    },
                  ),
                ),

              ],
            ))
                : (Container()),
            isLogin == true  ? Divider():Container(height: 0,width: 0,),
            SystemControls.branchesValid
                ? (Column(
              children: <Widget>[
                Container(
                  child: ListTile(
                    title: Text(
                      "Chosser",
                      style: textStyle(context),
                    ),
                    onTap: () {
                      // Navigator.pop(context);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext
                              context) =>
                                  ChooseLayout(
                                      EasyLocalization.of(context).currentLocale.languageCode,
                                     SystemControls.designControl)));
                    },
                  ),
                ),

              ],
            ))
                : (Container()),
          ],
        ),
      );
    },);
  }

  setLoginArea(BuildContext context) {
    if (token == "") {
      return Column(
        children: <Widget>[
          Container(
            color: itemBGColor(),
            child: ListTile(
              title: Text(
                "login".tr().toString(),
                style: textStyle(context),
              ),
              onTap: () {
                // Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            login(EasyLocalization.of(context).currentLocale.languageCode, SystemControls.designControl)));
              },
            ),
          ), //Login
          // dividingLineWhiteContainer(),
        ],
      );
    } else {
      return Column(
        children: <Widget>[
          Container(
            color: itemBGColor(),
            child: ListTile(
              title: Text(
                "profile".tr().toString(),
                style: textStyle(context),
              ),
              onTap: () {
                // Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => updateUser(
                            EasyLocalization.of(context).currentLocale.languageCode,token, SystemControls.designControl)));
              },
            ),
          ), //Logout

          /*Container(
            child: ListTile(
              title: Text(AppLocalizations.of(context).translate("changePassword"),
                style: Text_Style(),
              ),
              onTap: (){
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (BuildContext context) =>
                        updatePassword(Lang,Token,design_Control)
                    ));
              },
            ),
          ), //changePassword
          dividingLineContainer(),*/
        ],
      );
    }
    return Container();
  }

  setLogoutArea(BuildContext context) {
    if (token !="") {
      return Column(
        children: <Widget>[
          Container(
            //color: ItemBGColor(),
            child: ListTile(
              title: Text(
                "logout".tr().toString(),
                style: textStyle(context),
              ),
              onTap: () async {

                var res = await logoutApi(token,EasyLocalization.of(context).currentLocale.languageCode);
                if (res.status == 200) {
                  print(res.message);
                }
                SystemControls().LogoutSetUserData(context, EasyLocalization.of(context).currentLocale.languageCode);
                switch(SystemControls.theme){

                  case "1":
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => HomePageScreenTheamOne(),)

                    );
                    break; case "2":
                  Navigator.of(context).popUntil((route) => route.isFirst);


                  Navigator.pushReplacement(context,
                      MaterialPageRoute(
                        builder: (BuildContext context) => HomePageScreenTheamTwo(),)

                  );
                  break;
                }
              },
            ),
          ), //Logout
        ],
      );
    }
    return Container();
  }
}
