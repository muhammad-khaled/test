import 'dart:io';

import '../provider/ProductProvider.dart';
import '../widgets/Video.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

import '../screens/IteamDetails2.dart';
import '../dataControl/dataModule.dart';
import '../Controls.dart';

import '../globals.dart' as globals;
//import 'package:video_player/video_player.dart';

//import 'package:ext_video_player/ext_video_player.dart';

class SliderProductDetails extends StatefulWidget{
  double sliderHeight;
  int initialPage;
  final List<dynamic> _slider;
  dynamic _meal;
  final String _imag;
  final String mealId;
  final String _video;
    final CarouselController buttonCarouselController ;

  int sliderSize=0;
  SliderProductDetails(this._slider,this._imag,this._video,
      this.sliderHeight,this.initialPage,this.buttonCarouselController,this.mealId,this._meal){
    if(_slider != null) {
      print("_Sliderrrrrr");
      sliderSize = _slider.length;
    }
    if(_imag != null)
      sliderSize++;
    if(_video != null)
      sliderSize++;
    print("slider size ????? "+sliderSize.toString());
  }
  @override
  _SliderProductDetails createState() => _SliderProductDetails();
}

List<T> map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) {
    result.add(handler(i, list[i]));
  }
  return result;
}

class _SliderProductDetails extends State<SliderProductDetails>{
  // int _current = 0;
  CarouselController buttonCarouselController = CarouselController();

  // VideoPlayerController _controller;
   YoutubePlayerController _controller;

  Future<ClosedCaptionFile> _loadCaptions() async {
    final String fileContents = await DefaultAssetBundle.of(context).loadString('assets/bumble_bee_captions.srt');
    return SubRipCaptionFile(fileContents);
  }
  @override
  void initState() {
    super.initState();
    print('MealId : ${widget._meal["products_id"]}');

    if(widget._video != null) {
      var video = widget._video.split("/");
      print(video);
      print("video");
      print(video);
      print(video);
      print("video");
      _controller = YoutubePlayerController(
        initialVideoId: '${widget._video}',

        params:  YoutubePlayerParams(

          playlist: ["${video.last}"],
          startAt: const Duration(minutes: 1, seconds: 36),
          showControls: true,
          showFullscreenButton: true,
          desktopMode: true,
          privacyEnhanced: true,
          useHybridComposition: true,
        ),
      );
      _controller.onEnterFullscreen = () {
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.landscapeLeft,
          DeviceOrientation.landscapeRight,
        ]);
        print('Entered Fullscreen');
      };
      _controller.onExitFullscreen = () {
        print('Exited Fullscreen');
      };

    }
  }
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();

  }
  @override
  void dispose() {
    if(widget._video != null) {
      // _controller.dispose();
    }
    super.dispose();
  }

  Widget build(BuildContext context){
    if (widget.sliderSize > 0) {
      return Column(
        children: <Widget>[
         FutureBuilder(
             future: getMealById("en","${widget._meal["products_id"]}"),

             builder: (context,snap){

           if(snap.hasData){
             List<String> images=[];

              if(snap.data.data["product"]["products_img"]!= null){
                images.add("${snap.data.data["product"]["products_img"]}");
              }

             snap.data.data["product"]["images"].forEach((element) {
               images.add(element["products_images_name"]);
             });
             int length = images.length;
             if(snap.data.data["product"]["products_video"]!=null){
               length  ++;
             }
             print('length : $length');
                  return Consumer<ProductProvider>(builder: (context,product,_){
                    return  CarouselSlider.builder(
                        carouselController: widget.buttonCarouselController,
                        options: CarouselOptions(

                          height: widget.sliderHeight,
                          autoPlayCurve: Curves.fastOutSlowIn,
                          aspectRatio: 16 / 9,
                          viewportFraction: 1.0,
                          initialPage: widget.initialPage,
                          enableInfiniteScroll: true,
                          enlargeCenterPage: true,
                          reverse: false,
                          autoPlay: false,

                          scrollDirection: Axis.horizontal,
                          onPageChanged: (index,d){
                              // _current = index;

                                  product.changeProductIndex(index);


                          },
                        ),
                        itemCount: length==0?1:length,
                        itemBuilder: (BuildContext context,int index,__) {
                          if(widget._slider == null || index>= widget._slider.length){
                            const player = YoutubePlayerIFrame(
                              aspectRatio: 16/9,
                            );

                            /*
                  return InkWell(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        //color: Colors.black26
                      ),
                      child:
                      Stack(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: 250,
                            child: AspectRatio(
                              aspectRatio: _controller.value.aspectRatio,
                              child: Stack(
                                alignment: Alignment.bottomCenter,
                                children: <Widget>[
                                  VideoPlayer(_controller),
                                  ClosedCaption(text: _controller.value.caption.text),
                                  _PlayPauseOverlay(controller: _controller),
                                  VideoProgressIndicator(
                                    _controller,
                                    allowScrubbing: true,
                                  ),
                                ],
                              ),
                            )
                          ),
                        ],
                      ),
                      height: 190,
                    ),
                    onTap: (){
                      /*if(widget._slider == null || index>= widget._slider.length) {
                        _FullImgScreen(context, SystemControls().GetImgPath(
                            widget._imag, "original"));
                      }*/
                    },
                  );*/

                            return InkWell(
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                  //color: Colors.black26
                                ),
                                child:
                                Stack(
                                  children: <Widget>[
                                    (index == length-1&& length!=1) ? Positioned(
                                      left: 45,
                                      top: 20,
                                      child: Platform.isIOS ?YoutubeCustomWidget(video:"${widget._video.split("/").last}"): YoutubePlayerControllerProvider(
                                        // Passing controller to widgets below.
                                        controller: _controller,
                                        child: Container(
                                          width: MediaQuery.of(context).size.width/1.3,
                                          child:player,
                                        ),
                                      ),
                                    ) :CachedNetworkImage(
                                      imageUrl: SystemControls()
                                          .GetImgPath(length ==0?widget._imag:images[index],"original"),
                                      placeholder: (context, url) => Center(
                                          child: SystemControls().circularProgress()),
                                      errorWidget: (context, url, error) => Center(child: Icon(Icons.error),),
                                      fit: BoxFit.contain,
                                      width: MediaQuery.of(context).size.width,
                                      height: 250,
                                    ),
                                  ],
                                ),
                                height: 250,
                              ),
                              onTap: (){
                                if(widget._slider != null ) {
                                  List<String> images =[];
                                  images.add(widget._imag);
                                  snap.data.data["product"]["images"].forEach((element) {
                                    images.add(element["products_images_name"]);
                                  });
                                  images.add("widget._imag");
                                  images.forEach((element) {
                                    print(element);
                                  });
                                  images[index] != "widget._imag" ? _FullImgScreen(context, SystemControls().GetImgPath(
                                      length==0 ? widget._imag : images[index] ,"original")): print('');

                                }

                              },
                            );
                          }
                          const player = YoutubePlayerIFrame(
                            aspectRatio: 16/9,
                          );
                          return InkWell(
                            onTap: (){

                              if(widget._slider != null ) {
                                List<String> images =[];
                                images.add(widget._imag);
                                snap.data.data["product"]["images"].forEach((element) {
                                  images.add(element["products_images_name"]);
                                });
                                images.add(widget._imag);
                                images.forEach((element) {
                                  print(element);
                                });
                                images[index] != "widget._imag" ? _FullImgScreen(context, SystemControls().GetImgPath(
                                    length==0 ? widget._imag : images[index] ,"original")):print("");
                              }
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                //color: Colors.black26
                              ),
                              child:
                              Stack(
                                children: <Widget>[
                                  (index == length-1&& length!=1) ? Positioned(
                                    left: 45,
                                    top: 20,
                                    child: Platform.isIOS ?YoutubeCustomWidget(video:"${widget._video.split("/").last}"): YoutubePlayerControllerProvider(
                                      // Passing controller to widgets below.
                                      controller: _controller,
                                      child: Container(
                                        width: MediaQuery.of(context).size.width/1.3,
                                        child:player,
                                      ),
                                    ),
                                  ) :
                                  CachedNetworkImage(
                                    imageUrl: SystemControls()
                                        .GetImgPath(length ==0?widget._imag:images[index],"original"),
                                    placeholder: (context, url) => Center(
                                        child: SystemControls().circularProgress()),
                                    errorWidget: (context, url, error) => Center(child: Icon(Icons.error),),
                                    fit: BoxFit.contain,
                                    width: MediaQuery.of(context).size.width,
                                    height: 250,
                                  ),
                                ],
                              ),
                              height: 190,
                            ),
                          );
                        }

                    );
                  });
           }else{
             return CachedNetworkImage(
               imageUrl: SystemControls()
                   .GetImgPath(widget._imag,"original"),
               placeholder: (context, url) => Center(
                   child: SystemControls().circularProgress()),
               errorWidget: (context, url, error) => Center(child: Icon(Icons.error),),
               fit: BoxFit.contain,
               width: MediaQuery.of(context).size.width,
               height: 250,
             );
           }
         })
          //_sliderDots2(),
        ],
      );
    }
    return Center();
  }

  LinearGradient UI_commonLinearGradient = LinearGradient(
    begin: FractionalOffset.topCenter,
    end: FractionalOffset.bottomCenter,
    colors: [
      Colors.transparent,
      Colors.black.withOpacity(0.4),
    ],
    stops: [
      0.0,
      1.0,
    ],
  );

  Future<void> _FullImgScreen(BuildContext context, String imgURL) {
    return showDialog<void>(
      context: context,
      //barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {

        return GestureDetector(
          child: Container(
            width: MediaQuery.of(context).size.width,
            color: Color.fromRGBO(0, 0, 0, 0.8),
            child:  Stack(
              children: <Widget>[
                GestureDetector(
                  child: Center(
                    child: Container(
                      child: CachedNetworkImage(
                        imageUrl:  imgURL,
                        placeholder: (context, url) => Center(child: SystemControls().circularProgress(),),
                        fit: BoxFit.contain,
                        width: MediaQuery.of(context).size.width,
                        fadeInCurve: Curves.bounceInOut,
                        fadeInDuration: const Duration(seconds: 2),
                      ),
                    ),
                  ),
                  onTap: () {
                    //Navigator.pop(context);
                  },
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    margin: EdgeInsets.all(20),
                    child: Icon(Icons.cancel,color: Colors.white,size: 30,),
                  ),
                )
              ],
            ),
          ),
          onTap: () {
            Navigator.pop(context);
          },
        );
      },
    );
  }

  // Widget _sliderDots2() {
  //   return Row(
  //     mainAxisAlignment: MainAxisAlignment.center,
  //     children: map<Widget>(
  //       widget._slider,
  //           (index, url) {
  //         return Container(
  //           width: 6.0,
  //           height: 6.0,
  //           margin: EdgeInsets.symmetric(vertical: 6.0, horizontal: 3.0),
  //           decoration: BoxDecoration(
  //               shape: BoxShape.circle,
  //               color: _current == index
  //                   ? SystemControls.MainColorz
  //                   : SystemControls.MainColorOpacityz,
  //         ));
  //       },
  //     ),
  //   );
  // }
}
/*
class _PlayPauseOverlay extends StatelessWidget {
  const _PlayPauseOverlay({Key key, this.controller}) : super(key: key);

  final VideoPlayerController controller;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AnimatedSwitcher(
          duration: Duration(milliseconds: 50),
          reverseDuration: Duration(milliseconds: 200),
          child: controller.value.isPlaying
              ? SizedBox.shrink()
              : Container(
            color: Colors.black26,
            child: Center(
              child: Icon(
                Icons.play_arrow,
                color: Colors.white,
                size: 100.0,
              ),
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            controller.value.isPlaying ? controller.pause() : controller.play();
          },
        ),
      ],
    );
  }
}
*/
/*
class _SliderProductDetails extends State<SliderProductDetails>{
  List<dynamic> _slidersList;
  _SliderProductDetails(this._slidersList);

  int _current = 0;
  Widget build(BuildContext context){
    return Column(
      children: <Widget>[
        CarouselSlider.builder(
          viewportFraction: 1.0,
          initialPage: 0,
          height: 150,
          enableInfiniteScroll: false,
          reverse: false,
          autoPlay: false,
          scrollDirection: Axis.horizontal,
          onPageChanged: (index) {
            setState(() {
              _current = index;
            });
          },
          itemCount: _slidersList.length,
          itemBuilder: (BuildContext context,int index) =>
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(right: 5,left: 5),
                      width: MediaQuery.of(context).size.width,
                      height: 95,
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        child: CachedNetworkImage(
                          imageUrl: _slidersList[index]['image'],
                          placeholder: (context, url) => Center(child: SystemControls().circularProgress(),),
                          errorWidget: (context, url, error) => Center(child: Icon(Icons.broken_image),),
                          fit: BoxFit.fitHeight,
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                        ),
                      )
                    // ),
                  ),
                ],
              ),
        ),
        _sliderDots2(),
      ],
    );
  }

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }
  Widget _sliderDots2() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: map<Widget>(
        _slidersList,
            (index, url) {
          return Container(
            width: 6.0,
            height: 6.0,
            margin: EdgeInsets.symmetric(vertical: 6.0, horizontal: 3.0),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: _current == index
                    ? Color.fromRGBO(201, 16, 11, 1)
                    : Color.fromRGBO(201, 16, 11, 0.5)),
          );
        },
      ),
    );
  }
/*
  LinearGradient UI_commonLinearGradient = LinearGradient(
    begin: FractionalOffset.topCenter,
    end: FractionalOffset.bottomCenter,
    colors: [
      Colors.transparent,
      Colors.black.withOpacity(0.4),
    ],
    stops: [
      0.0,
      1.0,
    ],
  );*/
}
*/
class Controls extends StatelessWidget {
  ///
  const Controls();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _space,
          MetaDataSection(),
          _space,
          SourceInputSection(),
          _space,
          PlayPauseButtonBar(),
          _space,
          VolumeSlider(),
          _space,
          PlayerStateSection(),
        ],
      ),
    );
  }

  Widget get _space => const SizedBox(height: 10);
}
class MetaDataSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return YoutubeValueBuilder(builder: (context, value) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _Text('Title', value.metaData.title),
          const SizedBox(height: 10),
          _Text('Channel', value.metaData.author),
          const SizedBox(height: 10),
          _Text(
            'Playback Quality',
            value.playbackQuality ?? '',
          ),
          const SizedBox(height: 10),
          Row(
            children: [
              _Text('Video Id', value.metaData.videoId),
              const Spacer(),
              const _Text(
                'Speed',
                '',
              ),
              YoutubeValueBuilder(
                builder: (context, value) {
                  return DropdownButton(
                    value: value.playbackRate,
                    isDense: true,
                    underline: const SizedBox(),
                    items: PlaybackRate.all
                        .map(
                          (rate) => DropdownMenuItem(
                        child: Text(
                          '${rate}x',
                          style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                        value: rate,
                      ),
                    )
                        .toList(),
                    onChanged: (double newValue) {
                      if (newValue != null) {
                        context.ytController.setPlaybackRate(newValue);
                      }
                    },
                  );
                },
              ),
            ],
          ),
        ],
      );
    });
  }
}

class _Text extends StatelessWidget {
  final String title;
  final String value;

  const _Text(this.title, this.value);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        text: '$title : ',
        style: TextStyle(
          color: Theme.of(context).primaryColor,
          fontWeight: FontWeight.bold,
        ),
        children: [
          TextSpan(
            text: value,
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontWeight: FontWeight.w300,
            ),
          ),
        ],
      ),
    );
  }
}
class PlayPauseButtonBar extends StatelessWidget {
  final ValueNotifier<bool> _isMuted = ValueNotifier(false);
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        IconButton(
          icon: const Icon(Icons.skip_previous),
          onPressed: context.ytController.previousVideo,
        ),
        YoutubeValueBuilder(
          builder: (context, value) {
            return IconButton(
              icon: Icon(
                value.playerState == PlayerState.playing
                    ? Icons.pause
                    : Icons.play_arrow,
              ),
              onPressed: value.isReady
                  ? () {
                value.playerState == PlayerState.playing
                    ? context.ytController.pause()
                    : context.ytController.play();
              }
                  : null,
            );
          },
        ),
        ValueListenableBuilder<bool>(
          valueListenable: _isMuted,
          builder: (context, isMuted, _) {
            return IconButton(
              icon: Icon(isMuted ? Icons.volume_off : Icons.volume_up),
              onPressed: () {
                _isMuted.value = !isMuted;
                isMuted
                    ? context.ytController.unMute()
                    : context.ytController.mute();
              },
            );
          },
        ),
        IconButton(
          icon: const Icon(Icons.skip_next),
          onPressed: context.ytController.nextVideo,
        ),
      ],
    );
  }
}
class PlayerStateSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return YoutubeValueBuilder(
      builder: (context, value) {
        return AnimatedContainer(
          duration: const Duration(milliseconds: 800),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0),
            color: _getStateColor(value.playerState),
          ),
          width: double.infinity,
          padding: const EdgeInsets.all(8.0),
          child: Text(
            value.playerState.toString(),
            style: const TextStyle(
              fontWeight: FontWeight.w300,
              color: Colors.white,
            ),
            textAlign: TextAlign.center,
          ),
        );
      },
    );
  }

  Color _getStateColor(PlayerState state) {
    switch (state) {
      case PlayerState.unknown:
        return Colors.grey[700];
      case PlayerState.unStarted:
        return Colors.pink;
      case PlayerState.ended:
        return Colors.red;
      case PlayerState.playing:
        return Colors.blueAccent;
      case PlayerState.paused:
        return Colors.orange;
      case PlayerState.buffering:
        return Colors.yellow;
      case PlayerState.cued:
        return Colors.blue[900];
      default:
        return Colors.blue;
    }
  }
}
class SourceInputSection extends StatefulWidget {
  @override
  _SourceInputSectionState createState() => _SourceInputSectionState();
}

class _SourceInputSectionState extends State<SourceInputSection> {
   TextEditingController _textController;
  String _playlistType;

  @override
  void initState() {
    super.initState();
    _textController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return YoutubeValueBuilder(
      builder: (context, value) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            DropdownButton<String>(
              isExpanded: true,
              hint: Text(
                ' -- Choose playlist type',
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontWeight: FontWeight.w400,
                ),
              ),
              value: _playlistType,
              items: PlaylistType.all
                  .map(
                    (type) => DropdownMenuItem(
                  child: Text(
                    type,
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  value: type,
                ),
              )
                  .toList(),
              onChanged: (value) {
                _playlistType = value;
                setState(() {});
              },
            ),
            const SizedBox(height: 10),
            TextField(
              enabled: value.isReady,
              controller: _textController,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: _hint,
                helperText: _helperText,
                fillColor: Theme.of(context).primaryColor.withAlpha(20),
                filled: true,
                hintStyle: TextStyle(
                  fontWeight: FontWeight.w300,
                  color: Theme.of(context).primaryColor,
                ),
                suffixIcon: IconButton(
                  icon: const Icon(Icons.clear),
                  onPressed: () => _textController.clear(),
                ),
              ),
            ),
            const SizedBox(height: 10),
            GridView.count(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              childAspectRatio: 20 / 6,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              crossAxisCount: 2,
              children: [
                _Button(
                  action: 'LOAD',
                  onTap: () {
                    context.ytController
                        .load(_cleanId(_textController.text) ?? '');
                  },
                ),
                _Button(
                  action: 'CUE',
                  onTap: () {
                    context.ytController
                        .cue(_cleanId(_textController.text) ?? '');
                  },
                ),
                _Button(
                  action: 'LOAD PLAYLIST',
                  onTap: _playlistType == null
                      ? null
                      : () {
                    context.ytController.loadPlaylist(
                      _textController.text,
                      listType: _playlistType,
                    );
                  },
                ),
                _Button(
                  action: 'CUE PLAYLIST',
                  onTap: _playlistType == null
                      ? null
                      : () {
                    context.ytController.cuePlaylist(
                      _textController.text,
                      listType: _playlistType,
                    );
                  },
                ),
              ],
            ),
          ],
        );
      },
    );
  }

  String get _helperText {
    switch (_playlistType) {
      case PlaylistType.search:
        return '"avengers trailer", "nepali songs"';
      case PlaylistType.playlist:
        return '"PLj0L3ZL0ijTdhFSueRKK-mLFAtDuvzdje", ...';
      case PlaylistType.channel:
        return '"pewdiepie", "tseries"';
    }
    return null;
  }

  String get _hint {
    switch (_playlistType) {
      case PlaylistType.search:
        return 'Enter keywords to search';
      case PlaylistType.playlist:
        return 'Enter playlist id';
      case PlaylistType.channel:
        return 'Enter channel name';
    }
    return 'Enter youtube \<video id\> or \<link\>';
  }

  String _cleanId(String source) {
    if (source.startsWith('http://') || source.startsWith('https://')) {
      return YoutubePlayerController.convertUrlToId(source);
    } else if (source.length != 11) {
      _showSnackBar('Invalid Source');
    }
    return source;
  }

  void _showSnackBar(String message) {
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message,
          textAlign: TextAlign.center,
          style: const TextStyle(
            fontWeight: FontWeight.w300,
            fontSize: 16.0,
          ),
        ),
        backgroundColor: Theme.of(context).primaryColor,
        behavior: SnackBarBehavior.floating,
        elevation: 1.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50.0),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }
}

class _Button extends StatelessWidget {
  final VoidCallback onTap;
  final String action;

  const _Button({
    Key key,
     this.onTap,
     this.action,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      color: Theme.of(context).primaryColor,
      onPressed: onTap == null
          ? null
          : () {
        onTap?.call();
        FocusScope.of(context).unfocus();
      },
      disabledColor: Colors.grey,
      disabledTextColor: Colors.black,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 14.0),
        child: Text(
          action,
          style: const TextStyle(
            fontSize: 18.0,
            color: Colors.white,
            fontWeight: FontWeight.w300,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
class VolumeSlider extends StatelessWidget {
  final _volume = ValueNotifier<int>(100);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        const Text(
          "Volume",
          style: TextStyle(fontWeight: FontWeight.w300),
        ),
        Expanded(
          child: ValueListenableBuilder<int>(
            valueListenable: _volume,
            builder: (context, volume, _) {
              return Slider(
                inactiveColor: Colors.transparent,
                value: volume.toDouble(),
                min: 0.0,
                max: 100.0,
                divisions: 10,
                label: '$volume',
                onChanged: (value) {
                  _volume.value = value.round();
                  context.ytController.setVolume(volume);
                },
              );
            },
          ),
        ),
      ],
    );
  }
}