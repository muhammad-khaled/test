import 'dart:convert';

import '../Models/NotificationModel.dart';
import '../dataControl/userModule.dart';
import '../provider/NotificaionProvider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../provider/productsInCartProvider.dart';
import '../Controls.dart';
import '../dataControl/dataModule.dart';
import '../screens/home.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import '../dataControl/cartModule.dart';
import 'package:cached_network_image/cached_network_image.dart';
// import 'package:easy_localization/easy_localization.dart';

import '../globals.dart' as globals;

Widget AppBarElementsflexibleSpace(int UI_Code){
  return Image(
    image: AssetImage('assets/HeaderBGFinal.png'),
    fit: BoxFit.cover,
  );
  if(UI_Code == 1) {
    return Image(
      image: AssetImage('assets/AppBGFinal.png'),
      fit: BoxFit.cover,
    );
  }
  else{
    return Image(
        image: AssetImage('')
    );
  }
}
Color AppBarBGColor(){
  return globals.accountColorsData['AppbarBGColor']!=null
      ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz;
}
Widget AppBarTitleImg(String lang){
  return Container(
    //child: Center(
      child: Image.asset( lang == "ar" ? 'assets/logoNameSide.png':'assets/logoNameSideEn.png',
        fit: BoxFit.contain,
        height: 50,
      ),
    //)
  );
}
Widget DrawerIcon(BuildContext context, int design_Control){
  if(design_Control == 1) {
    return Builder(
      builder: (context)=> IconButton(
        icon: SystemControls().GetSVGImagesAsset(
            "assets/Icons/more.svg",
            20,
            Colors.black),
        onPressed: () {
          Scaffold.of(context).openDrawer();
        },
      ),
    );
  }
  else{
    return Container();
  }
}
Widget LayoutBG(int UI_Code){
  return Container(
    decoration: BoxDecoration(
      color: globals.accountColorsData['BGColor']!=null
          ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
      image: DecorationImage(
          image: AssetImage("assets/AppBGFinal.jpeg"),
          repeat: ImageRepeat.repeat
        //fit: BoxFit.cover
      ),
    ),
  );
  if(UI_Code == 1){
    return Container(
      decoration: BoxDecoration(
        color: globals.accountColorsData['BGColor']!=null
            ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
        image: DecorationImage(
            image: AssetImage("assets/AppBGFinal.png"),
            repeat: ImageRepeat.repeat
          //fit: BoxFit.cover
        ),
      ),
    );
  }
  else{
    return Container(
      decoration: BoxDecoration(
        color: globals.accountColorsData['BGColor']!=null
            ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
      ),
    );
  }
}
/*
GetCaruttontx(BuildContext context, String Lang,
    List<dynamic> _CartMeals){
  Future<responceFe> resp2 = InitialApi(Lang);
  return FutureBuilder<responceFe>(
      future: resp2,
      builder: (context,AppBarsnap){
        if(AppBarsnap.hasData
            && AppBarsnap.data.data['hasRegistration'] == true){
          print("hasRegistration");
          return Stack(
            children: <Widget>[
              Container(
                child: Center(
                  child: IconButton(icon: Icon(Icons.shopping_cart), onPressed: (){
                    /*Navigator.push(context,
                        MaterialPageRoute(builder: (BuildContext context) =>
                            CartLayout(Lang,Token,AppCurrency,design_Control)
                        ));*/
                  }),
                ),
              ),
              Container(
                width: 26,
                height: 26,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(13)),
                ),
                child: Center(
                  child: Text(_CartMeals.length.toString(),style: TextStyle(
                    color: Colors.black,
                    fontSize: SystemControls.font4,
                    fontWeight: FontWeight.bold,
                  ),textAlign: TextAlign.center,),
                ),
              ),
            ],
          );
        }
        return Container();
      }
  );
}*/








Widget AppBarHomeIConButton(BuildContext context, String Lang, String imgUrl){
  DefaultCacheManager manager = new DefaultCacheManager();
  return Container();
}

Widget AppBarCartButton(BuildContext context, VoidCallback onClickIcon
    , String CartCount, bool hasRegistration){

  final cartState = Provider.of<CartStateProvider>(context,listen: false);
  if(hasRegistration == true){
    return InkWell(
      child: Stack(
        children: <Widget>[
          Container(
            child: Center(
              child: IconButton(
                icon: SystemControls().GetSVGImagesAsset(
                    "assets/Icons/cart.svg",
                    25,
                    Colors.black),
                //onPressed: onClickIcon,
              ),
            ),
          ),
          Consumer<CartStateProvider>(builder: (context,cart,_){
            return       Container(
              margin: EdgeInsets.only(top: 2),
              width: 22,
              height: 22,
              decoration: BoxDecoration(
                //color: Colors.white,
                color: globals.accountColorsData['MainColor']!=null
                    ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                borderRadius: BorderRadius.all(Radius.circular(11)),
              ),
              child: Center(
                child: Text(cart.productsCount,
                  style: TextStyle(
                    color: globals.accountColorsData['TextOnMainColor']!=null
                        ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                    fontSize: SystemControls.font4,
                    fontWeight: FontWeight.bold,
                  ),textAlign: TextAlign.center,),
              ),
            );
          })
        ],
      ),
      onTap: onClickIcon,
    );
  }
  else{
    return Container(
      width: 48,
    );
  }
  /*
  return FutureBuilder<responceFe>(
      future: resp2,
      builder: (context,AppBarsnap){
        if(AppBarsnap.hasData
            && AppBarsnap.data.data['hasRegistration'] == true){
          print("hasRegistration");
          return InkWell(
            child: Stack(
              children: <Widget>[
                Container(
                  child: Center(
                    child: IconButton(
                      icon: SystemControls().GetSVGImagesAsset(
                          "assets/Icons/cart.svg",
                          25,
                          Colors.black),
                      //onPressed: onClickIcon,
                    ),
                  ),
                ),
                Container(
                  width: 26,
                  height: 26,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(13)),
                  ),
                  child: Center(
                    child: Text(CartCount,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: SystemControls.font4,
                        fontWeight: FontWeight.bold,
                      ),textAlign: TextAlign.center,),
                  ),
                ),
              ],
            ),
            onTap: onClickIcon,
          );
        }
        return Container(
          width: 48,
        );
      }
  );*/
}

Widget AppBarNotificationButton(BuildContext context, VoidCallback onClickIcon
    , bool hasRegistration ,String Lang,String Token){
  Future<ResponseApi> respNe;
  respNe = getUserNotification(Lang,Token);

  final notificationProvider = Provider.of<NotificationProvider>(context,listen: false);
  if(hasRegistration == true){
    return InkWell(
      child: Stack(
        children: [
          Container(
            child: Center(
              child: IconButton(
                icon: SystemControls().GetSVGImagesAsset(
                    "assets/Icons/notification.svg",25,null),
                /*SystemControls().GetSVGImagesAsset(
                    "assets/Icons/cart.svg",
                    25,
                    Colors.black),*/
                //onPressed: onClickIcon,
              ),
            ),
          ),
          Consumer<CartStateProvider>(builder: (context,cart,_){
            return       Container(
              margin: EdgeInsets.only(top: 2),
              width: 22,
              height: 22,
              decoration: BoxDecoration(
                //color: Colors.white,
                color: globals.accountColorsData['MainColor']!=null
                    ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                borderRadius: BorderRadius.all(Radius.circular(11)),
              ),
              child: Center(
                child: Consumer<NotificationProvider>(builder: (context,notification,_){
                  return   Text("${notification.counter==null ?"0":notification.counter}",
                    style: TextStyle(
                      color: globals.accountColorsData['TextOnMainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                      fontSize: SystemControls.font4,
                      fontWeight: FontWeight.bold,
                    ),textAlign: TextAlign.center,);
                },)
              ),
            );
          }),

        ],
      ),
      onTap: onClickIcon,
    );
  }
  else{
    return Container(
      width: 48,
    );
  }
  /*
  return FutureBuilder<responceFe>(
      future: resp2,
      builder: (context,AppBarsnap){
        if(AppBarsnap.hasData
            && AppBarsnap.data.data['hasRegistration'] == true){
          print("hasRegistration");
          return InkWell(
            child: Stack(
              children: <Widget>[
                Container(
                  child: Center(
                    child: IconButton(
                      icon: SystemControls().GetSVGImagesAsset(
                          "assets/Icons/cart.svg",
                          25,
                          Colors.black),
                      //onPressed: onClickIcon,
                    ),
                  ),
                ),
                Container(
                  width: 26,
                  height: 26,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(13)),
                  ),
                  child: Center(
                    child: Text(CartCount,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: SystemControls.font4,
                        fontWeight: FontWeight.bold,
                      ),textAlign: TextAlign.center,),
                  ),
                ),
              ],
            ),
            onTap: onClickIcon,
          );
        }
        return Container(
          width: 48,
        );
      }
  );*/
}

/*
//This cars to be use at all the application parts
//this function reaturn only the view of the card
//**** card action part will be with the function calling
Widget MenuItemCardzzz(BuildContext context, dynamic _item, String AppCurrency,
    bool type_offer, String typeText
    //Function(BuildContext,dynamic,bool) onClickAddToCart, VoidCallback onClickFavButton
    ){
  String title;
  String imgURL;
  String price;
  String calouries;
  String CatName;
  String CatImgURL;

  title = _item[typeText+'title'].toString();
  price = _item[typeText+'price'];
  calouries = _item[typeText+'calories'].toString();
  if(type_offer == true){
    imgURL = _item['offers_img_mobile'];
  }
  else{
    imgURL = _item[typeText+'img'];
  }
  if(_item['category'] == null){
    CatName = AppLocalizations.of(context).translate("offers");
    CatImgURL = "null";
  }
  else{
    CatName = _item['category']['categories_title'];
    CatImgURL = _item['category']['categories_img'];
  }

  return Container(
    margin: EdgeInsets.all(5),
    padding: EdgeInsets.all(0),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(SystemControls.RadiusCircValue)),
      color: Colors.white,
    ),
    child: ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(SystemControls.RadiusCircValue)),
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Stack(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                    child: Stack(
                      children: <Widget>[
                        Hero(
                          tag: imgURL,
                          child: CachedNetworkImage(
                            imageUrl: SystemControls().GetImgPath(imgURL,"medium"),
                            placeholder: (context, url) => Center(child: SystemControls().circularProgress(),),
                            fit: BoxFit.cover,
                            width: MediaQuery.of(context).size.width,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            color: SystemControls.MainColor,//Colors.green,
                            borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(SystemControls.RadiusCircValue),),
                          ),
                          child: Text(
                            price+" "+AppCurrency,
                            style: TextStyle(
                              fontSize: SystemControls.font2,
                              fontFamily: Lang,
                              color: SystemControls.TextHeaderColor,
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.start,
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
              flex: 7,
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: 5,right: 10,left: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(padding: EdgeInsets.all(4)),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 8,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(title,//"mix grill",
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontSize: SystemControls.font2,
                                  color: SystemControls.TextHeaderColor,
                                  fontWeight: FontWeight.normal,
                                  height: 1,
                                ),
                                maxLines: 1,
                                textAlign: TextAlign.start,
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(calouries=='null'? ""
                                  :calouries
                                  + AppLocalizations.of(context).translate("calories"),
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontSize: SystemControls.font3,
                                  color: SystemControls.TextdetailsColor,
                                  fontWeight: FontWeight.normal,
                                  height: 0.8,
                                ),
                                maxLines: 1,
                                textAlign: TextAlign.start,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ), //Meal title & cal count
                    Padding(padding: EdgeInsets.all(8)),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 5,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              type_offer?(
                                  SystemControls().GetSVGImagesAsset(
                                      'assets/Icons/offer.svg', 25,
                                      Colors.black)
                              ):(
                                  CachedNetworkImage(
                                    imageUrl: SystemControls().GetImgPath(
                                        CatImgURL,"original"),
                                    height: 25,
                                  )
                              ),
                              Padding(padding: EdgeInsets.all(5)),
                              Text(CatName,
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontSize: SystemControls.font3,
                                  color: SystemControls.TextdetailsColor,
                                  fontWeight: FontWeight.normal,
                                  height: 0.8,
                                ),
                                maxLines: 1,
                                textAlign: TextAlign.start,
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 5,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Container(
                                decoration: SystemControls().mealButtonDecoration(),
                                height: 40,
                                width: 40,
                                child: InkWell(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      SystemControls().GetSVGImagesAsset(
                                          "assets/Icons/cart.svg",
                                          25,
                                          Colors.black),
                                    ],
                                  ),
                                  onTap: (){}//onClickAddToCart(context,_item,type_offer),
                                ),
                              ),
                              Padding(padding: EdgeInsets.all(8)),
                              type_offer?(
                                  Container()
                              ):(
                                  Container(
                                    decoration: SystemControls().mealButtonDecoration(),
                                    height: 40,
                                    width: 40,
                                    child: InkWell(
                                      onTap: (){},//onClickFavButton,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          SystemControls().GetSVGImagesAsset(
                                              "assets/Icons/heart.svg",
                                              25,
                                              Colors.black),
                                        ],
                                      ),
                                    ),
                                  )
                              ),
                            ],
                          ),
                        ),
                      ],
                    ), //Category title
                  ],
                ),
              ),
              flex: 3,
            )
          ],
        ),
      ),
    ),
  );
}
*/*/

// GetDirection(String lang){
//   if(lang == "ar"){
//     return TextDirection.rtl;
//   }
//   else{
//     return TextDirection.ltr;
//   }
// }