import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../Controls.dart';
import 'package:easy_localization/easy_localization.dart';
import '../globals.dart' as globals;

InputFieldDecoration(String lableTextIn,String Lang){
  return InputDecoration(
    labelText: lableTextIn,
    labelStyle: TextStyle(
        fontFamily: Lang,
        fontWeight: FontWeight.normal,
        fontSize: SystemControls.font3,
        color: globals.accountColorsData['TextHeaderColor']!=null
            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
    ),
    fillColor: Colors.white,
    contentPadding: EdgeInsets.only(right: 10,left: 10,top: 20,bottom: 0),
    border: new OutlineInputBorder(
      borderRadius: new BorderRadius.circular(10.0),
      borderSide: new BorderSide(),
    ),
    enabledBorder: const OutlineInputBorder(
      borderSide: const BorderSide(color: Colors.black, width: 0.0),
    ),
    alignLabelWithHint: true,
  );
}
InputFieldDecorationPhone(String lableTextIn,String Lang,){
  return InputDecoration(
    labelText: lableTextIn,
    labelStyle: TextStyle(
        fontFamily: Lang,
        fontWeight: FontWeight.normal,
        fontSize: SystemControls.font3,
        color: globals.accountColorsData['TextHeaderColor']!=null
            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
    ),
    fillColor: Colors.white,
    contentPadding: EdgeInsets.only(right: 10,left: 10,top: 20,bottom: 0),
    border: new OutlineInputBorder(
      borderRadius: Lang =="en" ? BorderRadius.only(topRight: Radius.circular(10),bottomRight: Radius.circular(10)):BorderRadius.only(topLeft: Radius.circular(10),bottomLeft: Radius.circular(10)),
      borderSide: new BorderSide(),
    ),
    enabledBorder:  OutlineInputBorder(
      borderRadius: Lang =="en" ? BorderRadius.only(topRight: Radius.circular(10),bottomRight: Radius.circular(10)):BorderRadius.only(topLeft: Radius.circular(10),bottomLeft: Radius.circular(10)),

      borderSide:  BorderSide(color: Colors.black, width: 0.0),
    ),
    alignLabelWithHint: true,
  );
}InputFieldDecorationPhoneforget(String lableTextIn,String Lang,){
  return InputDecoration(
    labelText: lableTextIn,
    labelStyle: TextStyle(
        fontFamily: Lang,
        fontWeight: FontWeight.normal,
        fontSize: SystemControls.font3,
        color: globals.accountColorsData['TextHeaderColor']!=null
            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
    ),
    fillColor: Colors.white,
    contentPadding: EdgeInsets.only(right: 10,left: 10,top: 20,bottom: 0),
    border: new OutlineInputBorder(
      borderRadius: Lang =="en" ? BorderRadius.only(topRight: Radius.circular(10),bottomRight: Radius.circular(10)):BorderRadius.only(topLeft: Radius.circular(10),bottomLeft: Radius.circular(10)),
      borderSide: new BorderSide(),
    ),
    enabledBorder:  OutlineInputBorder(
      borderRadius: Lang =="en" ? BorderRadius.only(topRight: Radius.circular(10),bottomRight: Radius.circular(10)):BorderRadius.only(topLeft: Radius.circular(10),bottomLeft: Radius.circular(10)),

      borderSide:  BorderSide(color: Colors.black, width: 0.0),
    ),
    alignLabelWithHint: true,
  );
}