




import '../../Models/HomeModel/sharedModel.dart';
import '../../screens/categoryListNew.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'package:easy_localization/easy_localization.dart';


import 'package:cached_network_image/cached_network_image.dart';

import '../../Controls.dart';
import '../../globals.dart' as globals;

class CategoryListVerticalNew extends StatelessWidget{

  CategoryListVerticalNew(
      {this.categories,
      this.selectedId,
      this.cartMeals,
      this.lang,
      this.userToken,
      this.appCurrency,
 
      this.catStorageKey,
      this.designControl,
      this.hasRegistration,
      this.appInfo}){
    _scrollController = new ScrollController(initialScrollOffset: catStorageKey);
  }

  List<SharedCategory> categories =[] ;
  List<dynamic> cartMeals = <dynamic>[];
  final ImagePicker _picker = ImagePicker();

  int selectedId;
  String lang;
  String userToken;
  String appCurrency;
  bool hasRegistration;
  dynamic appInfo;
  int designControl;

  Color CatBGColor(int param){
    if(param == selectedId){
      return globals.accountColorsData['MainColor']!=null
          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz;
    }
    else
      return Colors.white;
  }
//TODO REad it to change show
  Route _createRoute(int index) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) =>
          CategoryMealsListNew(cartMeals, categories, lang, selectedId,userToken,
              appCurrency, index, designControl,hasRegistration,appInfo),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset.zero;
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  double catStorageKey;
  //ScrollController _scrollController = new ScrollController(initialScrollOffset: 30.0);
  ScrollController _scrollController;


  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 5,left: 5,top: 5),
      color: Colors.white,
      //alignment: Alignment.centerRight,
      child: ListView(
          controller: _scrollController,
          scrollDirection: Axis.vertical,
          padding: EdgeInsets.zero,
          children:[
            Container(
              margin: EdgeInsets.only(top: 5,bottom: 5),
              child: Image.asset("assets/categoriesTabHeaderImg.png",
                color: globals.accountColorsData['MainColor']!=null
                    ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                fit: BoxFit.fitWidth,),
            ),
            InkWell(
              child: Container(
                  decoration: BoxDecoration(
                    color: CatBGColor(-2),//Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.all(Radius.circular(0)),
                  ),
                  margin: EdgeInsets.all(5),
                  padding: EdgeInsets.all(5),
                  child: Column(
                    children: <Widget>[
                      /*Container(
                      //TODO set fixed image or get offers image
                      child: CachedNetworkImage(
                        imageUrl: SystemControls().GetImgPath("1579433449_offers_27_black.png","original"),
                        height: 30,
                      ),
                    ),*/
                      SystemControls().GetSVGImagesAsset(
                          'assets/Icons/offer.svg', 25,
                          Colors.black),
                      Padding(padding: EdgeInsets.all(5)),
                      Container(
                        child: new Center(
                            child: new Text("offers".tr().toString(),
                              style: TextStyle(
                                fontFamily: lang,
                                fontWeight: FontWeight.normal,
                                fontSize: SystemControls.font4,
                                height: 1.2,
                              ),
                              textAlign: TextAlign.center,
                              maxLines: 2,
                            )
                        ),
                      ),
                    ],
                  )
              ),
              onTap: (){
                Navigator.pushReplacement(context,
                    _createRoute(-2));
              },
            ),
            Container(
              margin: EdgeInsets.only(top: 5,bottom: 5),
              child: Image.asset("assets/rowBottom.png",
                color: globals.accountColorsData['MainColor']!=null
                    ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                fit: BoxFit.fitWidth,),
            ),
            ListView.builder(
                shrinkWrap : true,
                physics: ScrollPhysics(),
                scrollDirection: Axis.vertical,
                itemCount: categories.length,
                itemBuilder: (context, index) {
                  if (categories[index].categoriesParentId!=null) {
                    return Container();
                  }
                  else {
                    if (index == categories.length - 1) {
                      return Column(
                        children: <Widget>[
                          InkWell(
                            child: Container(
                                decoration: BoxDecoration(
                                  color: CatBGColor(
                                      categories[index].categoriesId), //Colors.white,//Theme.of(context).primaryColor,
                                  //borderRadius: BorderRadius.all(Radius.circular(SystemControls.RadiusCircValue)),
                                ),
                                margin: EdgeInsets.all(5),
                                padding: EdgeInsets.all(5),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      child:
                                      Stack(
                                        children: <Widget>[
                                          //Center(child: SystemControls().circularProgress()),
                                          CachedNetworkImage(
                                            imageUrl: SystemControls()
                                                .GetImgPath(
                                                categories[index].categoriesImg,
                                                "original"),
                                            height: 30,
                                          ), /*
                                Center(
                                  child: Image.network(
                                    SystemControls().GetImgPath(
                                        _categories[index]['categories_img'],"original"),
                                    height: 30,
                                  ),
                                ),*/
                                        ],
                                      ),
                                    ),
                                    Padding(padding: EdgeInsets.all(5)),
                                    Container(
                                      child: new Center(
                                          child: new Text(
                                            categories[index].categoriesTitle,
                                            //list[position],
                                            style: TextStyle(
                                              fontFamily: lang,
                                              fontWeight: FontWeight.normal,
                                              fontSize: SystemControls.font4,
                                              height: 1.2,
                                            ),
                                            textAlign: TextAlign.center,
                                            maxLines: 2,
                                          )
                                      ),
                                    ),
                                  ],
                                )
                            ),
                            onTap: () {
                              Navigator.pushReplacement(context,
                                  _createRoute(
                                      categories[index].categoriesId)
                              );
                            },
                          ),
                        ],
                      );
                    }
                    return Column(
                      children: <Widget>[
                        InkWell(
                          child: Container(
                              decoration: BoxDecoration(
                                color: CatBGColor(
                                    categories[index].categoriesId), //Colors.white,//Theme.of(context).primaryColor,
                                //borderRadius: BorderRadius.all(Radius.circular(SystemControls.RadiusCircValue)),
                              ),
                              margin: EdgeInsets.all(5),
                              padding: EdgeInsets.all(5),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    child:
                                    Stack(
                                      children: <Widget>[
                                        //Center(child: SystemControls().circularProgress()),
                                        CachedNetworkImage(
                                          imageUrl: SystemControls().GetImgPath(
                                              categories[index].categoriesImg,
                                              "original"),
                                          height: 30,
                                        ), /*
                                Center(
                                  child: Image.network(
                                    SystemControls().GetImgPath(
                                        _categories[index]['categories_img'],"original"),
                                    height: 30,
                                  ),
                                ),*/
                                      ],
                                    ),
                                  ),
                                  Padding(padding: EdgeInsets.all(5)),
                                  Container(
                                    child: new Center(
                                        child: new Text(
                                          categories[index].categoriesImg,
                                          //list[position],
                                          style: TextStyle(
                                            fontFamily: lang,
                                            fontWeight: FontWeight.normal,
                                            fontSize: SystemControls.font4,
                                            height: 1.2,
                                          ),
                                          textAlign: TextAlign.center,
                                          maxLines: 2,
                                        )
                                    ),
                                  ),
                                ],
                              )
                          ),
                          onTap: () {
                            Navigator.pushReplacement(context,
                                _createRoute(
                                    categories[index].categoriesId)
                            );
                          },
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 5, bottom: 5),
                          child: Image.asset("assets/rowBottom.png",
                            color: globals.accountColorsData['MainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                            fit: BoxFit.fitWidth,),
                        ),
                      ],
                    );
                  }
                }
            ),
            Container(
              margin: EdgeInsets.only(top: 5,bottom: 5),
              child: RotatedBox(
                quarterTurns: 10,
                child: Image.asset("assets/categoriesTabHeaderImg.png",
                  color: globals.accountColorsData['MainColor']!=null
                      ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                  fit: BoxFit.fitWidth,),
              ),
            )
          ]
      ),
    );
  }
}