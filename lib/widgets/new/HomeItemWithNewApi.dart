import '../../Models/HomeModel/sharedModel.dart';

import '../ToolTipWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'dart:async';

import '../../provider/productsInCartProvider.dart';
import '../../dataControl/dataModule.dart';
import '../../Controls.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../screens/IteamDetails2.dart';

import '../../dataControl/cartModule.dart';
import '../../dataControl/favsModule.dart';

import 'package:cached_network_image/cached_network_image.dart';
import '../UI_Alert_Widgets.dart';
import '../UI_Wigdets.dart';

import '../../globals.dart' as globals;

class HomeItemsListWithNewApi extends StatelessWidget{

  String lang;
  String userToken;
  String appCurrency;
  int designControll;
  bool hasRegistration;

  List<ProductShared> products;
  List<dynamic> cartData = <dynamic>[];
  Function(List<dynamic>) onAddTocartSubmit;
  Function(List<dynamic>) onFavSubmit;

  HomeItemsListWithNewApi(
      {this.products,
      this.cartData,
      this.lang,
      this.userToken,
      this.appCurrency,
      this.designControll,
      this.hasRegistration,
      this.onAddTocartSubmit,
      this.onFavSubmit});

  FindMealAtCart(int mealId){
    for(int i=0; i<cartData.length; i++){
      if(cartData[i]['products_id'] == mealId && cartData[i]['type'] == "product"){
        return i;
      }
    }
    return -1;
  }

  int amount =1;


  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    //int count = (((mediaQuery.size.width-10)/shortestSideCount)).round();
    int count = (((MediaQuery.of(context).size.width-10)/380)).round();
    count *=SystemControls.MinNumberOfItemsOnRow;

    int deviceShortestSide = MediaQuery.of(context).size.shortestSide.round();
    int cardHei;
    int imgCardRatio;
    if(deviceShortestSide > 415){
      cardHei = 300;
      imgCardRatio =5;
    }
    else {
      if(SystemControls.cardViewItemNumber==2){
        cardHei = 225;
      }
      else{
        cardHei = 250;
      }
      imgCardRatio =5;
    }

    return Container(
      margin: EdgeInsets.all(5),
      child: GridView.builder(

        shrinkWrap : true,
        physics: ScrollPhysics(),
        itemCount: products.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: count,
          childAspectRatio:// 1,
          (((mediaQuery.size.width-(10*count))/count) /cardHei),
        ),
        itemBuilder: (BuildContext, int index){
          String catTitle1,catTitle2;
          String catImg;
          if(products[index].categorySeries != null){
            List<dynamic> _category_series = products[index].categorySeries;
            catTitle1 = products[index].categorySeries[0].categoriesTitle;
            catImg = products[index].category.categoriesImg;
            if(_category_series.length>1){
              catTitle2 = products[index].categorySeries[1].categoriesTitle;
            }
            else{
              catTitle2 = "";
            }
          }
          bool findOfferPrice = false;
          if(products[index].productsPriceAfterSale!=null&&
              products[index].productsPriceAfterSale.toString()!=""){
            findOfferPrice = true;
          }
          //print("onn scrol");
          return InkWell(
            onTap: (){
              Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(builder: (context) =>
                  //TODO Use new Item Details
                  ItemDetails(products[index],cartData,lang,
                      appCurrency,userToken,designControll,false,null,
                      hasRegistration,false,0,products[index].productsAvailable)
                  ),(Route<dynamic> route) => true
              );
            },
            child: Container(
              margin: EdgeInsets.all(3),
              padding: EdgeInsets.all(0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(SystemControls.RadiusCircValue)),
                  color: Colors.white,
                  border: Border.all(
                    width: 1,
                    color: globals.accountColorsData['BorderColor']!=null
                        ? Color(int.parse(globals.accountColorsData['BorderColor'])) : SystemControls.BorderColorz,
                  )
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(SystemControls.RadiusCircValue)),
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width,
                              //height: cardHei*0.67,
                              child:
                              Stack(
                                children: <Widget>[
                                  CachedNetworkImage(
                                    imageUrl: SystemControls().GetImgPath(products[index].productsImg.toString(),"medium"),
                                    placeholder: (context, url) => Center(child: SystemControls().circularProgress(),),
                                    errorWidget: (context, url, error) => Center(child: Icon(Icons.broken_image),),
                                    fit: BoxFit.contain,
                                    width: MediaQuery.of(context).size.width,
                                    height: MediaQuery.of(context).size.height,
                                  ),
                                  Container(
                                    height: imgCardRatio*6.0,
                                    width: cardHei.toDouble(),
                                    child: Row(
                                      children: [
                                        Spacer(),
                                        products[index].productsPriceAfterSale.toString().isNotEmpty ?  Container(
                                          width: cardHei.toDouble()/4,

                                          height: imgCardRatio*6.0,
                                          decoration: BoxDecoration(
                                              color:  globals.accountColorsData['MainColor']!=null
                                                  ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                                              borderRadius: lang =="ar"? BorderRadius.only(topLeft: Radius.circular(5),bottomRight: Radius.circular(15)):BorderRadius.only(topRight: Radius.circular(5),bottomLeft: Radius.circular(15))
                                          ),
                                          child: Center(child: Text("${((1-(double.parse("${products[index].productsPriceAfterSale}")/double.parse("${products[index].productsPrice}")))*100).toInt()}%", style: TextStyle(
                                            fontSize: SystemControls.font3,
                                            fontFamily: lang,
                                            color: globals.accountColorsData['TextOnMainColor']!=null
                                                ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                                            fontWeight: FontWeight.bold,
                                            height: 0.9,
                                          ),),),
                                        )  :Container()
                                      ],
                                    ),

                                  ),
                                ],
                              ),
                            ),
                            Column(
                              children: <Widget>[
                                Expanded(
                                  flex: 5,
                                  child: hasRegistration?(
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.all(8),
                                            decoration: SystemControls().mealButtonDecoration(),
                                            //height: 50,
                                            //width: 50,
                                            child: InkWell(
                                              onTap: ()async{
                                                if(userToken == ""){
                                                  ErrorDialogAlertGoTOLogin(context,
                                                      "mustLogin".tr().toString(),lang);
                                                }
                                                else{
                                                  if(products[index].isFav==1){
                                                    products[index].isFav = 0;
                                                  }
                                                  else{
                                                    products[index].isFav = 1;
                                                  }
                                                  onFavSubmit(products);

                                                  var res = await updateFavsItem(lang, userToken,
                                                      products[index].productsId,context);
                                                  if(res.status ==200){
                                                    SuccDialogAlertStaySameLayout(context, res.message,ValueKey("catssegoryList1"));

                                                  }
                                                  else{
                                                    if(res.status == 401){
                                                      SystemControls().LogoutSetUserData(context, lang);
                                                    }

                                                    if(products[index].isFav==1){
                                                      products[index].isFav = 0;
                                                    }
                                                    else{
                                                      products[index].isFav = 1;
                                                    }
                                                    onFavSubmit(products);

                                                    ErrorDialogAlert(context, res.message);
                                                  }
                                                }
                                              },
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                  products[index].isFav==0?(
                                                      SystemControls().GetSVGImagesAsset(
                                                          "assets/Icons/heart.svg",
                                                          18,
                                                          null)
                                                  ):(
                                                      SystemControls().GetSVGImagesAsset(
                                                          "assets/Icons/heart.svg",
                                                          18,
                                                          globals.accountColorsData['MainColor']!=null
                                                              ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz)
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      )
                                  ):(
                                      Container()
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                        flex: imgCardRatio,
                      ),
                      Expanded(
                        child: Stack(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 0,right: 7,left: 7),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(top: 8,bottom: 0),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 7,
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              RichText(
                                                textAlign: TextAlign.start,
                                                text: new TextSpan(
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                      text: findOfferPrice
                                                          ? products[index].productsPriceAfterSale.toString()+" "+ appCurrency+ "  "
                                                          : products[index].productsPrice.toString()+" "+ appCurrency + "  ",
                                                      style: TextStyle(
                                                        fontSize: SystemControls.font3,
                                                        fontFamily: lang,
                                                        color: globals.accountColorsData['TextHeaderColor']!=null
                                                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                                        fontWeight: FontWeight.bold,
                                                        height: 0.9,
                                                      ),
                                                    ),

                                                    findOfferPrice?(
                                                        TextSpan(text: ' ',)
                                                    ):(
                                                        TextSpan()
                                                    ),
                                                    findOfferPrice?(
                                                        TextSpan(
                                                          text: products[index].productsPrice==null? "n"
                                                              : products[index].productsPrice.toString()+" "+ appCurrency,
                                                          style: new TextStyle(
                                                            fontSize: SystemControls.font4,
                                                            fontFamily: lang,
                                                            color: globals.accountColorsData['TextdetailsColor']!=null
                                                                ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                            fontWeight: FontWeight.normal,
                                                            decoration: TextDecoration.lineThrough,
                                                            height: 0.9,
                                                          ),
                                                        )
                                                    ):(
                                                        TextSpan()
                                                    ),
                                                    TextSpan(
                                                      text: products[index].productsType=='weight'?
                                                      'priceForWeight'.tr().toString()
                                                          :'priceForCount'.tr().toString(),
                                                      style: TextStyle(
                                                        fontSize: SystemControls.font3,
                                                        fontFamily: lang,
                                                        color: globals.accountColorsData['TextHeaderColor']!=null
                                                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                                        fontWeight: FontWeight.bold,
                                                        height: 0.9,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                maxLines: 1,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ), //Price
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 7,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              //height: 38,
                                              child: Text(products[index].productsTitle,
                                                style: TextStyle(
                                                  fontFamily: lang,
                                                  fontSize: SystemControls.font4,
                                                  color: globals.accountColorsData['TextdetailsColor']!=null
                                                      ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                  fontWeight: FontWeight.normal,
                                                  height: 1.2,
                                                ),
                                                maxLines: 2,
                                                textAlign: TextAlign.start,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ), //
                                  Padding(
                                    padding: EdgeInsets.only(top: 3,bottom: 5),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        products[index].choices != null && products[index].choices.length > 0?
                                        Container():
                                        Expanded(
                                          flex: 4,
                                          child: InkWell(
                                            onTap: (){
                                              dynamic startVal;
                                              if(products[index].productsType != "weight") { //Product follow weight or num
                                                startVal = 1;
                                              }
                                              else{
                                                startVal = double.parse(products[index].productsAvailable);
                                              }
                                              showBottomSelectQuantity(context,startVal,index,amount);
                                            },
                                            child: Container(
                                              padding: EdgeInsets.only(right: 5,left: 5),
                                              height: 30,
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(SystemControls.RadiusCircValue/2),),
                                                border: Border.all(color:globals.accountColorsData['TextdetailsColor']!=null
                                                    ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                    width: 1),
                                              ),
                                              //color: Colors.cyan,
                                              child: Row(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  RichText(
                                                    textAlign: TextAlign.start,
                                                    text: new TextSpan(
                                                      children: <TextSpan>[
                                                        TextSpan(
                                                          text: amount.toString(),
                                                          style: TextStyle(
                                                            fontFamily: lang,
                                                            fontSize: SystemControls.font4,
                                                            color: globals.accountColorsData['TextdetailsColor']!=null
                                                                ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                            fontWeight: FontWeight.normal,
                                                            height: 1.2,
                                                          ),
                                                        ),
                                                        TextSpan(
                                                          text: "  ",
                                                          style: TextStyle(
                                                            fontSize: SystemControls.font3,
                                                            fontFamily: lang,
                                                            color: globals.accountColorsData['TextHeaderColor']!=null
                                                                ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                                            fontWeight: FontWeight.bold,
                                                            height: 0.9,
                                                          ),
                                                        ),
                                                        TextSpan(
                                                          text: products[index].productsType=='weight'?
                                                          'KG'.tr().toString()
                                                              :'piece'.tr().toString(),
                                                          style: TextStyle(
                                                            fontFamily: lang,
                                                            fontSize: SystemControls.font4,
                                                            color: globals.accountColorsData['TextdetailsColor']!=null
                                                                ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                            fontWeight: FontWeight.normal,
                                                            height: 1.2,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    maxLines: 1,
                                                  ),
                                                  Icon(
                                                    Icons.keyboard_arrow_down,
                                                    color: Colors.black,),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                        SizedBox(width: 10,),
                                        products[index].productsAvailable !="0"?  Expanded(
                                          flex: 3,
                                          child: hasRegistration?(
                                              Container(
                                                padding: EdgeInsets.only(right: 10,left: 10),
                                                decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.all(
                                                    Radius.circular(SystemControls.RadiusCircValue/2),),
                                                  border: Border.all(color:globals.accountColorsData['TextdetailsColor']!=null
                                                      ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                      width: 1),
                                                ),
                                                //padding: EdgeInsets.only(right: 8,left: 8),
                                                height: 30,
                                                //width: 40,
                                                child: InkWell(
                                                  child: Row(
                                                    mainAxisSize: MainAxisSize.max,
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    children: <Widget>[
                                                      Text(products[index].choices != null && products[index].choices.length > 0?
                                                      "showDetails".tr().toString()
                                                          : "",//AppLocalizations.of(context).translate("addToCart"),
                                                        style: TextStyle(
                                                          fontFamily: lang,
                                                          fontSize: SystemControls.font3,
                                                          color: globals.accountColorsData['TextHeaderColor']!=null
                                                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                                          fontWeight: FontWeight.normal,
                                                          height: 1,
                                                        ),
                                                        maxLines: 1,
                                                        textAlign: TextAlign.start,
                                                      ),
                                                      products[index].choices != null && products[index].choices.length > 0?
                                                      Container()
                                                          :Icon(Icons.add_shopping_cart,color: Colors.black,size: 18,),
                                                    ],
                                                  ),
                                                  onTap: ()async{
                                                    if(products[index].choices != null && products[index].choices.length > 0){
                                                      Navigator.pushAndRemoveUntil(context,
                                                          MaterialPageRoute(builder: (context) =>
                                                          //TODO Use new Item Details
                                                          ItemDetails(products[index],cartData,lang,
                                                              appCurrency,userToken,designControll,false,null, hasRegistration,false,0,products[index].productsAvailable)
                                                          ),(Route<dynamic> route) => true
                                                      );
                                                    }
                                                    else if (products[index].productsAvailable!=null
                                                        || products[index].productsAvailable=="1") {
                                                      print("add to cart");
                                                      int sear = FindMealAtCart(
                                                          products[index].productsId);
                                                      //TODO Remove it to add all items already at the cart
                                                      if (true) { //sear == -1
                                                        dynamic AddMeal = CartMeal();

                                                        AddMeal.quantityType =
                                                        products[index].productsType;//Product follow weight or num
                                                        AddMeal.minQuantity =
                                                        products[index].productsLowestWeightAvailable;

                                                        AddMeal.meals_id =
                                                        products[index].productsId;
                                                        AddMeal.meals_title =
                                                        products[index].productsTitle;
                                                        AddMeal.meals_img =
                                                        products[index].productsImg;
                                                        AddMeal.meals_desc =
                                                        products[index].productsDesc;
                                                        AddMeal.quantityType =
                                                        products[index].productsType;//Product follow weight or num
                                                        if (findOfferPrice) {
                                                          AddMeal.meals_price =
                                                              products[index].productsPriceAfterSale.toString();
                                                        }
                                                        else {
                                                          AddMeal.meals_price =
                                                              products[index].productsPrice.toString();
                                                        }
                                                        AddMeal
                                                            .follow_delivery_time =
                                                        products[index].category.categoriesFollowDeliveryTime;
                                                        AddMeal.type = "product";
                                                        // if(products[index]['quantity'] == null){
                                                        //   products[index]['quantity'] = 1;
                                                        // }
                                                        AddMeal.count = amount;

                                                        dynamic obj = AddMeal
                                                            .toJson();
                                                        cartData.add(obj);

                                                        //onAddTocartSubmit(_CartMeals);

                                                        CartStateProvider cartState = Provider.of<CartStateProvider>(context,listen: false);
                                                        int x = int.parse(cartState.productsCount);
                                                        x++;
                                                        cartState.setCurrentProductsCount(x.toString());

                                                        write_to_file(obj);

                                                        SuccDialogAlertStaySameLayout(
                                                            context,

                                                            'orderAddtoCart'.tr().toString(),ValueKey("categossaryList1"));
                                                      }
                                                      else {
                                                        // ErrorDialogAlert(context,
                                                        //     AppLocalizations.of(
                                                        //         context)
                                                        //         .translate(
                                                        //         'mealAleadyAdded'));
                                                        //TODO if want to increase meal count
                                                        //_CartMeals[sear]['count']++;
                                                        //await UpdateList(_CartMeals);
                                                      }
                                                    }
                                                    else{
                                                      ErrorDialogAlert(context,"غير متوفر");
                                                    }
                                                  },
                                                ),
                                              )
                                          ):(
                                              Container()
                                          ),
                                        ): Expanded(
                                          flex: 3,
                                          child: hasRegistration?  MyTooltip(
                                            message: "productNotExist".tr().toString(),
                                            child: Container(
                                              padding: EdgeInsets.only(right: 10,left: 10),
                                              decoration: BoxDecoration(
                                                color: Colors.grey[100*3],
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(SystemControls.RadiusCircValue/2),),
                                                border: Border.all(color:globals.accountColorsData['TextdetailsColor']!=null
                                                    ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                    width: 1),
                                              ),
                                              //padding: EdgeInsets.only(right: 8,left: 8),
                                              height: 30,
                                              child: Icon(Icons.add_shopping_cart),

                                            ),
                                          ):Container(),
                                        ) ,
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            /*
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Row(
                                children: [
                                  _meals[index]["products_lowest_weight_available"] != null?(
                                      DropDownStatefulWidget(
                                          _meals[index]["products_lowest_weight_available"])
                                  ):(
                                      Container()
                                      //DropDownStatefulWidget("1")
                                  ),
                                ],
                              ),
                            )*/
                          ],
                        ),
                        flex: 4,
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
          // return InkWell(
          //   onTap: (){
          //     Navigator.pushAndRemoveUntil(context,
          //         MaterialPageRoute(builder: (context) =>
          //             ItemDetails(_meals[index],_CartMeals,Lang,
          //                 AppCurrency,UserToken,design_Control,false,null,
          //                 hasRegistration,false,0,_meals[index]["products_available"])
          //         ),(Route<dynamic> route) => true
          //     );
          //   },
          //   child: Container(
          //     margin: EdgeInsets.all(3),
          //     padding: EdgeInsets.all(0),
          //     decoration: BoxDecoration(
          //         borderRadius: BorderRadius.all(Radius.circular(SystemControls.RadiusCircValue)),
          //         color: Colors.white,
          //         border: Border.all(
          //           width: 1,
          //           color: globals.accountColorsData['BorderColor']!=null
          //               ? Color(int.parse(globals.accountColorsData['BorderColor'])) : SystemControls.BorderColorz,
          //         )
          //     ),
          //     child: ClipRRect(
          //       borderRadius: BorderRadius.all(Radius.circular(SystemControls.RadiusCircValue)),
          //       child: Container(
          //         child: Column(
          //           mainAxisAlignment: MainAxisAlignment.start,
          //           children: <Widget>[
          //             Expanded(
          //               child: Stack(
          //                 children: <Widget>[
          //                   Container(
          //                     width: MediaQuery.of(context).size.width,
          //                     //height: cardHei*0.67,
          //                     child:
          //                     Stack(
          //                       children: <Widget>[
          //                         Hero(
          //                           tag: _meals[index]['products_img'],
          //                           child: CachedNetworkImage(
          //                             imageUrl: SystemControls().GetImgPath(_meals[index]['products_img'],"medium"),
          //                             placeholder: (context, url) => Center(child: SystemControls().circularProgress(),),
          //                             fit: BoxFit.contain,
          //                             width: MediaQuery.of(context).size.width,
          //                             height: MediaQuery.of(context).size.height,
          //                           ),
          //                         ),
          //                         /*Center(child: SystemControls().circularProgress()),
          //                         Center(
          //                           child: Image.network(
          //                             SystemControls().GetImgPath(_meals[index]['meals_img'],"medium"),
          //                             fit: BoxFit.cover,
          //                             width: MediaQuery.of(context).size.width,
          //                           ),
          //                         ),
          //                         Center(
          //                           child: Image.network(
          //                             SystemControls().GetImgPath(_meals[index]['meals_img'],"original"),
          //                             fit: BoxFit.cover,
          //                             width: MediaQuery.of(context).size.width,
          //
          //                           ),
          //                         ),*/
          //                       ],
          //                     ),
          //                   ),
          //                   Column(
          //                     children: <Widget>[
          //                       Container(
          //                         alignment: Alignment.centerLeft,
          //                         child: Container(
          //                           //height: 50,
          //                           padding: EdgeInsets.all(4),
          //                           decoration: BoxDecoration(
          //                             color: globals.accountColorsData['MainColor']!=null
          //                                 ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,//Colors.green,
          //                             borderRadius: BorderRadius.only(
          //                               bottomRight: Radius.circular(SystemControls.RadiusCircValue),),
          //                           ),
          //                           child: RichText(
          //                             textAlign: TextAlign.start,
          //                             text: new TextSpan(
          //                               children: <TextSpan>[
          //                                 findOfferPrice?(
          //                                     TextSpan(
          //                                       text: _meals[index]['products_price']==null? "n"
          //                                           : _meals[index]['products_price'].toString()+" "+ AppCurrency,
          //                                       style: new TextStyle(
          //                                         fontSize: SystemControls.font5,
          //                                         fontFamily: Lang,
          //                                         color: globals.accountColorsData['TextdetailsColor']!=null
          //                                             ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
          //                                         fontWeight: FontWeight.normal,
          //                                         decoration: TextDecoration.lineThrough,
          //                                       ),
          //                                     )
          //                                 ):(
          //                                     TextSpan()
          //                                 ),
          //                                 findOfferPrice?(
          //                                     TextSpan(text: '\n',)
          //                                 ):(
          //                                     TextSpan()
          //                                 ),
          //                                 new TextSpan(
          //                                   text: findOfferPrice
          //                                       ? _meals[index]['products_price_after_sale'].toString()+" "+ AppCurrency
          //                                       : _meals[index]['products_price'].toString()+" "+ AppCurrency,
          //                                   style: TextStyle(
          //                                     fontSize: SystemControls.font4,
          //                                     fontFamily: Lang,
          //                                     color: globals.accountColorsData['TextHeaderColor']!=null
          //                                         ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
          //                                     fontWeight: FontWeight.bold,
          //                                   ),
          //                                 ),
          //                               ],
          //                             ),
          //                           ),
          //                         ),
          //                       )
          //                     ],
          //                   )
          //                 ],
          //               ),
          //               flex: imgCardRatio,
          //             ),
          //             Expanded(
          //               child: Container(
          //                 margin: EdgeInsets.only(top: 0,right: 5,left: 5),
          //                 child: Column(
          //                   mainAxisAlignment: MainAxisAlignment.start,
          //                   crossAxisAlignment: CrossAxisAlignment.start,
          //                   children: <Widget>[
          //                     Padding(padding: EdgeInsets.all(4)),
          //                     Row(
          //                       mainAxisSize: MainAxisSize.max,
          //                       mainAxisAlignment: MainAxisAlignment.center,
          //                       children: <Widget>[
          //                         Expanded(
          //                           flex: 7,
          //                           child: Column(
          //                             crossAxisAlignment: CrossAxisAlignment.start,
          //                             mainAxisAlignment: MainAxisAlignment.center,
          //                             children: <Widget>[
          //                               Text(_meals[index]['products_title'],
          //                                 style: TextStyle(
          //                                   fontFamily: Lang,
          //                                   fontSize: SystemControls.font3,
          //                                   color: globals.accountColorsData['TextHeaderColor']!=null
          //                                       ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
          //                                   fontWeight: FontWeight.normal,
          //                                   height: 1,
          //                                 ),
          //                                 maxLines: 1,
          //                                 textAlign: TextAlign.start,
          //                               ),
          //                             ],
          //                           ),
          //                         ),
          //                         SystemControls.cardViewItemNumber==1?(
          //                             Expanded(
          //                               flex: 3,
          //                               child: Column(
          //                                 crossAxisAlignment: CrossAxisAlignment.end,
          //                                 mainAxisAlignment: MainAxisAlignment.center,
          //                                 children: <Widget>[
          //                                   Text(_meals[index]['products_calories']==null? ""
          //                                       :_meals[index]['products_calories'].toString()
          //                                       + AppLocalizations.of(context).translate("calories"),
          //                                     style: TextStyle(
          //                                       fontFamily: Lang,
          //                                       fontSize: SystemControls.font4,
          //                                       color: globals.accountColorsData['TextdetailsColor']!=null
          //                                           ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
          //                                       fontWeight: FontWeight.normal,
          //                                       height: 0.8,
          //                                     ),
          //                                     maxLines: 1,
          //                                     textAlign: TextAlign.left,
          //                                   ),
          //                                 ],
          //                               ),
          //                             )
          //                         ):(
          //                             Container()
          //                         ),
          //                       ],
          //                     ), //Meal title & cal count
          //                     Padding(padding: EdgeInsets.all(2)),
          //                     SystemControls.cardViewItemNumber==1?(
          //                         Padding(padding: EdgeInsets.all(4))
          //                     ):(
          //                         Container()
          //                     ),
          //                     Row(
          //                       mainAxisSize: MainAxisSize.max,
          //                       mainAxisAlignment: MainAxisAlignment.center,
          //                       children: <Widget>[
          //                         Expanded(
          //                           flex: 6,
          //                           child: Row(
          //                             crossAxisAlignment: CrossAxisAlignment.center,
          //                             mainAxisAlignment: MainAxisAlignment.start,
          //                             children: <Widget>[
          //                               SystemControls.cardViewItemNumber==1?(
          //                                   CachedNetworkImage(
          //                                     imageUrl: SystemControls().GetImgPath(
          //                                         catImg,"original"),
          //                                     height: 25,
          //                                   )
          //                               ):(
          //                                   Container()
          //                               ),
          //                               SystemControls.cardViewItemNumber==1?(
          //                                   Padding(padding: EdgeInsets.all(4))
          //                               ):(
          //                                   Container()
          //                               ),
          //                               InkWell(
          //                                 onTap: (){},
          //                                 child: Text(catTitle1,
          //                                   style: TextStyle(
          //                                     fontFamily: Lang,
          //                                     fontSize: SystemControls.font4,
          //                                     color: globals.accountColorsData['TextdetailsColor']!=null
          //                                         ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
          //                                     fontWeight: FontWeight.normal,
          //                                     height: 0.8,
          //                                   ),
          //                                   maxLines: 1,
          //                                   textAlign: TextAlign.start,
          //                                 ),
          //                               ),
          //                               catTitle2!=""?(
          //                                   Padding(
          //                                     padding: EdgeInsets.all(4),
          //                                     child: Text("/",
          //                                       style: TextStyle(
          //                                         fontFamily: Lang,
          //                                         fontSize: SystemControls.font4,
          //                                         color: globals.accountColorsData['TextdetailsColor']!=null
          //                                             ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
          //                                         fontWeight: FontWeight.normal,
          //                                         height: 0.8,
          //                                       ),
          //                                       maxLines: 1,
          //                                       textAlign: TextAlign.start,
          //                                     ),
          //                                   )
          //                               ):(Container()),
          //                               InkWell(
          //                                 onTap: (){},
          //                                 child: Text(catTitle2,
          //                                   style: TextStyle(
          //                                     fontFamily: Lang,
          //                                     fontSize: SystemControls.font4,
          //                                     color: globals.accountColorsData['TextdetailsColor']!=null
          //                                         ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
          //                                     fontWeight: FontWeight.normal,
          //                                     height: 0.8,
          //                                   ),
          //                                   maxLines: 1,
          //                                   textAlign: TextAlign.start,
          //                                 ),
          //                               ),
          //                             ],
          //                           ),
          //                         ),
          //                         SystemControls.cardViewItemNumber==1?(
          //                             Expanded(
          //                               flex: 5,
          //                               child: hasRegistration?(
          //                                   Row(
          //                                     crossAxisAlignment: CrossAxisAlignment.end,
          //                                     mainAxisAlignment: MainAxisAlignment.end,
          //                                     children: <Widget>[
          //                                       Container(
          //                                         decoration: SystemControls().mealButtonDecoration(),
          //                                         height: 40,
          //                                         width: 40,
          //                                         child: InkWell(
          //                                           onTap: ()async{
          //                                             if(UserToken == ""){
          //                                               ErrorDialogAlertGoTOLogin(context,
          //                                                   AppLocalizations.of(context).translate("mustLogin"),Lang);
          //                                             }
          //                                             else{
          //                                               var res = await updateFavsItem(Lang, UserToken,
          //                                                   _meals[index]['products_id']);
          //                                               if(res.status ==200){
          //                                                 SuccDialogAlertStaySameLayout(context, res.message);
          //                                                 if(_meals[index]['is_fav']==1){
          //                                                   _meals[index]['is_fav'] = 0;
          //                                                 }
          //                                                 else{
          //                                                   _meals[index]['is_fav'] = 1;
          //                                                 }
          //                                                 onFavSubmit(_meals);
          //                                               }
          //                                               else if(res.status == 401){
          //                                                 SystemControls().LogoutSetUserData(context, Lang);
          //                                                 ErrorDialogAlertBackHome(context, res.message);
          //                                               }
          //                                               else{
          //                                                 ErrorDialogAlert(context, res.message);
          //                                               }
          //                                             }
          //                                           },
          //                                           child: Row(
          //                                             mainAxisAlignment: MainAxisAlignment.center,
          //                                             children: <Widget>[
          //                                               _meals[index]['is_fav']==0?(
          //                                                   SystemControls().GetSVGImagesAsset(
          //                                                       "assets/Icons/heart.svg",
          //                                                       25,
          //                                                       null)
          //                                               ):(
          //                                                   SystemControls().GetSVGImagesAsset(
          //                                                       "assets/Icons/heart.svg",
          //                                                       25,
          //                                                       globals.accountColorsData['MainColor']!=null
          //                                                           ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz)
          //                                               ),
          //                                             ],
          //                                           ),
          //                                         ),
          //                                       ),
          //                                       Padding(padding: EdgeInsets.all(4)),
          //                                       Container(
          //                                         decoration: SystemControls().mealButtonDecoration(),
          //                                         //padding: EdgeInsets.only(right: 8,left: 8),
          //                                         height: 40,
          //                                         width: 40,
          //                                         child: InkWell(
          //                                           child: Row(
          //                                             mainAxisAlignment: MainAxisAlignment.center,
          //                                             children: <Widget>[
          //                                               //ToDo if _items[index][typeText+'available'] == null or != "1"
          //                                               //Show othr icon with line
          //                                               SystemControls().GetSVGImagesAsset(
          //                                                   "assets/Icons/cart.svg",
          //                                                   25,
          //                                                   Colors.black),
          //                                             ],
          //                                           ),
          //                                           onTap: ()async{
          //                                             if (_meals[index]['products_available']!=null
          //                                                 || _meals[index]['products_available']=="1") {
          //                                               print("add to cart");
          //                                               int sear = FindMealAtCart(
          //                                                   _meals[index]['products_id']);
          //                                               //TODO Remove it to add all items already at the cart
          //                                               if (true) { //sear == -1
          //                                                 dynamic AddMeal = CartMeal();
          //                                                 AddMeal.meals_id =
          //                                                 _meals[index]['products_id'];
          //                                                 AddMeal.meals_title =
          //                                                 _meals[index]['products_title'];
          //                                                 AddMeal.meals_img =
          //                                                 _meals[index]['products_img'];
          //                                                 AddMeal.meals_desc =
          //                                                 _meals[index]['products_desc'];
          //                                                 if (findOfferPrice) {
          //                                                   AddMeal.meals_price =
          //                                                   _meals[index]['products_price_after_sale'].toString();
          //                                                 }
          //                                                 else {
          //                                                   AddMeal.meals_price =
          //                                                   _meals[index]['products_price'].toString();
          //                                                 }
          //                                                 AddMeal
          //                                                     .follow_delivery_time =
          //                                                 _meals[index]['category']['categories_follow_delivery_time'];
          //                                                 AddMeal.type = "product";
          //                                                 AddMeal.count = 1;
          //
          //                                                 dynamic obj = AddMeal
          //                                                     .toJson();
          //                                                 _CartMeals.add(obj);
          //
          //                                                 onAddTocartSubmit(
          //                                                     _CartMeals);
          //
          //                                                 write_to_file(obj);
          //
          //                                                 SuccDialogAlertStaySameLayout(
          //                                                     context,
          //                                                     AppLocalizations.of(
          //                                                         context)
          //                                                         .translate(
          //                                                         'orderAddtoCart'));
          //                                               }
          //                                               else {
          //                                                 ErrorDialogAlert(context,
          //                                                     AppLocalizations.of(
          //                                                         context)
          //                                                         .translate(
          //                                                         'mealAleadyAdded'));
          //                                                 //TODO if want to increase meal count
          //                                                 //_CartMeals[sear]['count']++;
          //                                                 //await UpdateList(_CartMeals);
          //                                               }
          //                                             }
          //                                           },
          //                                         ),
          //                                       ),
          //                                     ],
          //                                   )
          //                               ):(
          //                                   Container()
          //                               ),
          //                             )
          //                         ):(
          //                             Container()
          //                         ),
          //                       ],
          //                     ), //Category title
          //
          //                     SystemControls.cardViewItemNumber==2?(
          //                         Padding(padding: EdgeInsets.all(4))
          //                     ):(
          //                         Container()
          //                     ),
          //                     SystemControls.cardViewItemNumber==2?(
          //                         Row(
          //                           mainAxisSize: MainAxisSize.max,
          //                           mainAxisAlignment: MainAxisAlignment.center,
          //                           children: <Widget>[
          //                             Expanded(
          //                               flex: 3,
          //                               child: Column(
          //                                 crossAxisAlignment: CrossAxisAlignment.start,
          //                                 mainAxisAlignment: MainAxisAlignment.center,
          //                                 children: <Widget>[
          //                                   Text(_meals[index]['products_calories']==null? ""
          //                                       :_meals[index]['products_calories'].toString()
          //                                       + AppLocalizations.of(context).translate("calories"),
          //                                     style: TextStyle(
          //                                       fontFamily: Lang,
          //                                       fontSize: SystemControls.font4,
          //                                       color: globals.accountColorsData['TextdetailsColor']!=null
          //                                           ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
          //                                       fontWeight: FontWeight.normal,
          //                                       height: 0.8,
          //                                     ),
          //                                     maxLines: 1,
          //                                     textAlign: TextAlign.left,
          //                                   ),
          //                                 ],
          //                               ),
          //                             ),
          //                             Expanded(
          //                               flex: 5,
          //                               child: hasRegistration?(
          //                                   Row(
          //                                     crossAxisAlignment: CrossAxisAlignment.end,
          //                                     mainAxisAlignment: MainAxisAlignment.end,
          //                                     children: <Widget>[
          //                                       Container(
          //                                         decoration: SystemControls().mealButtonDecoration(),
          //                                         height: 40,
          //                                         width: 40,
          //                                         child: InkWell(
          //                                           onTap: ()async{
          //                                             if(UserToken == ""){
          //                                               ErrorDialogAlertGoTOLogin(context,
          //                                                   AppLocalizations.of(context).translate("mustLogin"),Lang);
          //                                             }
          //                                             else{
          //                                               var res = await updateFavsItem(Lang, UserToken,
          //                                                   _meals[index]['products_id']);
          //                                               if(res.status ==200){
          //                                                 SuccDialogAlertStaySameLayout(context, res.message);
          //                                                 if(_meals[index]['is_fav']==1){
          //                                                   _meals[index]['is_fav'] = 0;
          //                                                 }
          //                                                 else{
          //                                                   _meals[index]['is_fav'] = 1;
          //                                                 }
          //                                                 onFavSubmit(_meals);
          //                                               }
          //                                               else if(res.status == 401){
          //                                                 SystemControls().LogoutSetUserData(context, Lang);
          //                                                 ErrorDialogAlertBackHome(context, res.message);
          //                                               }
          //                                               else{
          //                                                 ErrorDialogAlert(context, res.message);
          //                                               }
          //                                             }
          //                                           },
          //                                           child: Row(
          //                                             mainAxisAlignment: MainAxisAlignment.center,
          //                                             children: <Widget>[
          //                                               _meals[index]['is_fav']==0?(
          //                                                   SystemControls().GetSVGImagesAsset(
          //                                                       "assets/Icons/heart.svg",
          //                                                       25,
          //                                                       null)
          //                                               ):(
          //                                                   SystemControls().GetSVGImagesAsset(
          //                                                       "assets/Icons/heart.svg",
          //                                                       25,
          //                                                       globals.accountColorsData['MainColor']!=null
          //                                                           ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz)
          //                                               ),
          //                                             ],
          //                                           ),
          //                                         ),
          //                                       ),
          //                                       Padding(padding: EdgeInsets.all(4)),
          //                                       Container(
          //                                         decoration: SystemControls().mealButtonDecoration(),
          //                                         //padding: EdgeInsets.only(right: 8,left: 8),
          //                                         height: 40,
          //                                         width: 40,
          //                                         child: InkWell(
          //                                           child: Row(
          //                                             mainAxisAlignment: MainAxisAlignment.center,
          //                                             children: <Widget>[
          //                                               //ToDo if _items[index][typeText+'available'] == null or != "1"
          //                                               //Show othr icon with line
          //                                               SystemControls().GetSVGImagesAsset(
          //                                                   "assets/Icons/cart.svg",
          //                                                   25,
          //                                                   Colors.black),
          //                                             ],
          //                                           ),
          //                                           onTap: ()async{
          //                                             if (_meals[index]['products_available']!=null
          //                                                 || _meals[index]['products_available']=="1") {
          //                                               print("add to cart");
          //                                               int sear = FindMealAtCart(
          //                                                   _meals[index]['products_id']);
          //                                               //TODO Remove it to add all items already at the cart
          //                                               if (true) { //sear == -1
          //                                                 dynamic AddMeal = CartMeal();
          //                                                 AddMeal.meals_id =
          //                                                 _meals[index]['products_id'];
          //                                                 AddMeal.meals_title =
          //                                                 _meals[index]['products_title'];
          //                                                 AddMeal.meals_img =
          //                                                 _meals[index]['products_img'];
          //                                                 AddMeal.meals_desc =
          //                                                 _meals[index]['products_desc'];
          //                                                 if (findOfferPrice) {
          //                                                   AddMeal.meals_price =
          //                                                   _meals[index]['products_price_after_sale'].toString();
          //                                                 }
          //                                                 else {
          //                                                   AddMeal.meals_price =
          //                                                   _meals[index]['products_price'].toString();
          //                                                 }
          //                                                 AddMeal
          //                                                     .follow_delivery_time =
          //                                                 _meals[index]['category']['categories_follow_delivery_time'];
          //                                                 AddMeal.type = "product";
          //                                                 AddMeal.count = 1;
          //
          //                                                 dynamic obj = AddMeal
          //                                                     .toJson();
          //                                                 _CartMeals.add(obj);
          //
          //                                                 onAddTocartSubmit(
          //                                                     _CartMeals);
          //
          //                                                 write_to_file(obj);
          //
          //                                                 SuccDialogAlertStaySameLayout(
          //                                                     context,
          //                                                     AppLocalizations.of(
          //                                                         context)
          //                                                         .translate(
          //                                                         'orderAddtoCart'));
          //                                               }
          //                                               else {
          //                                                 ErrorDialogAlert(context,
          //                                                     AppLocalizations.of(
          //                                                         context)
          //                                                         .translate(
          //                                                         'mealAleadyAdded'));
          //                                                 //TODO if want to increase meal count
          //                                                 //_CartMeals[sear]['count']++;
          //                                                 //await UpdateList(_CartMeals);
          //                                               }
          //                                             }
          //                                           },
          //                                         ),
          //                                       ),
          //                                     ],
          //                                   )
          //                               ):(
          //                                   Container()
          //                               ),
          //                             ),
          //                           ],
          //                         )
          //                     ):(
          //                         Container()
          //                     ),
          //                   ],
          //                 ),
          //               ),
          //               flex: 4,
          //             )
          //           ],
          //         ),
          //       ),
          //     ),
          //   ),
          // );
        },
      ),
    );
  }

  Future<void> showBottomSelectQuantity(BuildContext context,
      dynamic StartVal, int ItemIndex,amount)
  {
    List<dynamic> _choices = <dynamic>[];
    dynamic currentQuantity = StartVal;
    while(currentQuantity<=10){
      _choices.add(currentQuantity);
      currentQuantity +=StartVal;
    }
    print(_choices.length.toString());
    return showModalBottomSheet(
        context: context,
        isScrollControlled: false,
        builder: (context) {
          return ConstrainedBox(
            constraints: new BoxConstraints(
              minHeight: MediaQuery.of(context).size.height*0.2,
              maxHeight: MediaQuery.of(context).size.height*0.4,
            ),
            child: Container(
              color: Colors.white,
              child: Container(
                child: Stack(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.topCenter,
                      padding: EdgeInsets.all(15),
                      child: Text('selectQuantity'.tr().toString(),
                        style: TextStyle(
                          fontFamily: lang,
                          fontWeight: FontWeight.bold,
                          fontSize: SystemControls.font3,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                          height: 1.2,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      alignment: Alignment.topRight,
                      padding: EdgeInsets.all(0),
                      child: IconButton(
                        icon: Icon(Icons.close,color: Colors.black,),
                        onPressed: (){
                          Navigator.pop(context);
                        },),
                    ),
                    DraggableScrollableSheet(
                      initialChildSize: 1,
                      minChildSize: 0.8,
                      builder: (context, ScrollController scrollController) {
                        return Container(
                          color: Colors.white,
                          margin: EdgeInsets.only(top: 50),
                          child: ListView.builder(
                            controller: scrollController,
                            itemCount: _choices.length,
                            itemBuilder: (BuildContext context, int index) {
                              return ListTile(
                                title: RichText(
                                  textAlign: TextAlign.start,
                                  text: new TextSpan(
                                    children: <TextSpan>[
                                      TextSpan(
                                        text: _choices[index].toString(),
                                        style: TextStyle(
                                          fontSize: SystemControls.font3,
                                          fontFamily: lang,
                                          color: globals.accountColorsData['TextHeaderColor']!=null
                                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                          fontWeight: FontWeight.bold,
                                          height: 0.9,
                                        ),
                                      ),
                                      TextSpan(
                                        text: "  ",
                                        style: TextStyle(
                                          fontSize: SystemControls.font3,
                                          fontFamily: lang,
                                          color: globals.accountColorsData['TextHeaderColor']!=null
                                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                          fontWeight: FontWeight.bold,
                                          height: 0.9,
                                        ),
                                      ),
                                      TextSpan(
                                        text: products[ItemIndex].productsType=='weight'?
                                        'KG'.tr().toString()
                                            :'piece'.tr().toString(),
                                        style: TextStyle(
                                          fontSize: SystemControls.font3,
                                          fontFamily: lang,
                                          color: globals.accountColorsData['TextHeaderColor']!=null
                                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                          fontWeight: FontWeight.bold,
                                          height: 0.9,
                                        ),
                                      ),
                                    ],
                                  ),
                                  maxLines: 1,
                                ),
                                //title: Text(_choices[index].toString()),
                                onTap: (){
                                  //setState(() {
                                  amount = _choices[index];
                                  print(amount);
                                  onFavSubmit(products);
                                  Navigator.pop(context);

                                  //});*/
                                },
                              );
                            },
                          ),
                        );
                      },
                    ),
                  ],
                ),
                decoration: BoxDecoration(
                  color: Theme.of(context).canvasColor,
                  borderRadius: BorderRadius.only(
                    topLeft: const Radius.circular(12),
                    topRight: const Radius.circular(12),
                  ),
                ),
              ),
            ),
          );
          return DraggableScrollableSheet(
            initialChildSize: 1,
            minChildSize: 0.8,
            builder: (context, ScrollController scrollController) {
              return Container(
                color: Colors.blue[100],
                margin: EdgeInsets.only(top: 50),
                child: ListView.builder(
                  controller: scrollController,
                  itemCount: 25,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(title: Text('Item $index'));
                  },
                ),
              );
            },
          );
        });
  }
}