


import '../../Models/HomeModel/sharedModel.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cached_network_image/cached_network_image.dart';

import '../../screens/IteamDetails2.dart';
import '../../dataControl/dataModule.dart';
import '../../Controls.dart';

import '../../globals.dart' as globals;

class SliderDefaultHomeAdsNew extends StatefulWidget{
  final List<SharedOffer> _slider;
  final String Lang;
  String AppCurrency;
  List<dynamic> _CartMeals = <dynamic>[];
  int design_Control;
  String UserToken;
  bool hasRegistration;
  SliderDefaultHomeAdsNew(this._slider,this.Lang, this._CartMeals,
      this.AppCurrency,this.design_Control,this.UserToken,this.hasRegistration,);
  @override
  _SliderDefaultHomeAds createState() => _SliderDefaultHomeAds();
}

List<T> map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) {
    result.add(handler(i, list[i]));
  }
  return result;
}

class _SliderDefaultHomeAds extends State<SliderDefaultHomeAdsNew>{
  int _current = 0;
  Color _yelowColor = Color.fromRGBO(237, 212, 23, 1);
  SystemControls contro = SystemControls();

  Widget build(BuildContext context){
    if (widget._slider.length > 0) {
      return Column(
        children: <Widget>[
          CarouselSlider.builder(
            options: CarouselOptions(
              height: 190,
              autoPlayCurve: Curves.fastOutSlowIn,
              aspectRatio: 16 / 9,
              viewportFraction: 1.0,
              initialPage: 0,
              enableInfiniteScroll: true,
              reverse: false,
              autoPlay: true,
              autoPlayInterval: Duration(seconds: 6),
              autoPlayAnimationDuration: Duration(milliseconds: 600),
              scrollDirection: Axis.horizontal,
            ),
            itemCount: widget._slider.length,
            itemBuilder: (BuildContext context,int index,__) =>
                InkWell(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      //color: Colors.black26
                    ),
                    child:
                    Stack(
                      children: <Widget>[
                        CachedNetworkImage(
                          imageUrl:SystemControls().GetIFDeviceTablet()?
                          SystemControls().GetImgPath(
                              widget._slider[index].offersImgTab,"original")
                              :SystemControls().GetImgPath(
                              widget._slider[index].offersImgMobile,"original"),//offers_img_mobile
                          placeholder: (context, url) => Center(
                              child: SystemControls().circularProgress()),
                          errorWidget: (context, url, error) => Center(child: Icon(Icons.error),),
                          fit: BoxFit.cover,
                          width: MediaQuery.of(context).size.width,
                          height: 190,
                        ),

                        /*Center(child: SystemControls().circularProgress()),
                          Center(
                            child: Image.network(
                              SystemControls().GetImgPath(
                                  widget._slider[index]['offers_img_mobile'],"medium"),
                              fit: BoxFit.cover,
                              width: MediaQuery.of(context).size.width,
                            ),
                          ),
                          Center(
                            child: Image.network(
                              SystemControls().GetImgPath(
                                  widget._slider[index]['offers_img_mobile'],"original"),
                              fit: BoxFit.cover,
                              width: MediaQuery.of(context).size.width,
                            ),
                          ),*/
                      ],
                    ),
                    height: 190,
                  ),
                  onTap: (){
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) =>
                        //TODO Use new Item Details
                        ItemDetails(
                            widget._slider[index],
                            widget._CartMeals,
                            widget.Lang,
                            widget.AppCurrency,
                            widget.UserToken,
                            widget.design_Control,
                            true,
                            null,
                            widget.hasRegistration,
                            /// wrong
                            true,-2, widget._slider[index].offersDesc
                        )
                        ));
                  },
                ),
          ),
          //_sliderDots(),
        ],
      );
    }
    return Center();
  }

  LinearGradient UI_commonLinearGradient = LinearGradient(
    begin: FractionalOffset.topCenter,
    end: FractionalOffset.bottomCenter,
    colors: [
      Colors.transparent,
      Colors.black.withOpacity(0.4),
    ],
    stops: [
      0.0,
      1.0,
    ],
  );

  Widget _sliderContent(dynamic i) {
    return GestureDetector(
      onTap: () {

      },
      child: Container(
        padding: EdgeInsets.all(10),
        height: 190,
        decoration: BoxDecoration(gradient: UI_commonLinearGradient),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
              child: Text(
                'Jacket 30% OFF'.toUpperCase(),
                style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold,
                  shadows: <Shadow>[
                    Shadow(
                      offset: Offset(1.0, 1.0),
                      blurRadius: 1.0,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
            ),  //Title
          ],
        ),
      ),
    );
  }
}