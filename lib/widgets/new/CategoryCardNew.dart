import '../../Models/HomeModel/sharedModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../Controls.dart';
import '../../dataControl/dataModule.dart';
import '../../screens/home.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:cached_network_image/cached_network_image.dart';

import '../../globals.dart' as globals;
import 'package:easy_localization/easy_localization.dart';
TextStyle CategoryTextStyle(String Lang,Color SelectedColorText){
  return TextStyle(
    fontFamily: Lang,
    fontWeight: FontWeight.normal,
    fontSize: SystemControls.font3,
    height: 0.9,
    color: SelectedColorText,
  );
}
Widget CategoryCardItemNew(SharedCategory Category,int SelectedID, String Lang){
  Color CardBG;
  Color SelectedColorText;

  print("Category");
  print(Category);
  print("Category");

  if(SelectedID == Category.categoriesId){
    CardBG = globals.accountColorsData['MainColor']!=null
        ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz;
    SelectedColorText = globals.accountColorsData['TextOnMainColor']!=null
        ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz;
  }
  else{
    CardBG = Colors.white;
    SelectedColorText = SystemControls.TextHeaderColorz;
  }
  Key textWid;
  return Container(
    decoration: BoxDecoration(
        color: CardBG,
        borderRadius: BorderRadius.all(
            Radius.circular(
                SystemControls.RadiusCircValue/2)),
        border: Border.all(
            color: globals.accountColorsData['TextdetailsColor']!=null
                ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
            width: 1)
    ),
    margin: EdgeInsets.only(left: 5,right: 5),
    padding: EdgeInsets.only(right: 8, left: 8),
    child: Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.topCenter,
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 3),
                  alignment: Alignment.center,
                  //color: Colors.green,
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: 10,
                      maxHeight: SystemControls.CategoryImgSize,
                      maxWidth: SystemControls.CategoryImgSize,
                      minHeight: 10,
                    ),
                    child: CachedNetworkImage(
                      imageUrl: SystemControls()
                          .GetImgPath(
                          Category.categoriesImg,
                          "original"),
                      height: SystemControls.CategoryImgSize,
                      width: SystemControls.CategoryImgSize,
                    ),
                  ),
                ),
                /*
                Container(
                  margin: EdgeInsets.only(top: 3),
                  alignment: Alignment.center,
                  height: 60,
                  width: 60,
                  //color: Colors.green,
                  child: CachedNetworkImage(
                    imageUrl: SystemControls()
                        .GetImgPath(
                        Category['categories_img'],
                        "original"),
                    height: 60,
                  ),
                ),
                */
                //This text is wrong to be use but we add it to try
                // to make the category card with not fixed by get text width
                Text(
                  Category.categoriesTitle,
                  style: TextStyle(
                    fontFamily: Lang,
                    fontWeight: FontWeight.normal,
                    fontSize: SystemControls.font3,
                    height: 0.1,
                    color: Color.fromRGBO(0, 0, 0, 0),
                  ),
                  textAlign: TextAlign.center,
                  maxLines: 1,
                ),
              ],
            ),
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            height: 50,
            color: CardBG,
            alignment: Alignment.center,
            padding: EdgeInsets.only(bottom: 0,top: 0),
            child: ConstrainedBox(
              constraints: BoxConstraints(
                minWidth: 100,
              ),
              child: Text(
                Category.categoriesTitle,
                style: CategoryTextStyle(Lang,SelectedColorText),
                textAlign: TextAlign.center,

                maxLines: 1,
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
Widget OfferCardItemNew(BuildContext context, int SelectedID, String Lang){
  Color CardBG;
  Color SelectedColorText;
  if(SelectedID == -2){
    CardBG = globals.accountColorsData['MainColor']!=null
        ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz;
    SelectedColorText = globals.accountColorsData['TextOnMainColor']!=null
        ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz;
  }
  else{
    CardBG = Colors.white;
    SelectedColorText = SystemControls.TextHeaderColorz;
  }
  return Container(
      decoration: BoxDecoration(
          color: CardBG,
          borderRadius: BorderRadius.all(Radius.circular(
              SystemControls.RadiusCircValue/2)),
          border: Border.all(
              color: globals.accountColorsData['TextdetailsColor']!=null
                  ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
              width: 1)
      ),
      margin: EdgeInsets.only(left: 5,right: 5),
      padding: EdgeInsets.only(right: 15, left: 15,),
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 6),
                    height: SystemControls.CategoryImgSize,
                    width: SystemControls.CategoryImgSize,
                    child: Center(
                      child: SystemControls().GetSVGImagesAsset(
                          'assets/Icons/offer.svg', 70,
                          Colors.black),
                    ),
                  ),
                  //This text is wrong to be use but we add it to try
                  // to make the category card with not fixed by get text width
                  Text(

                    "offers".tr().toString(),
                    style: TextStyle(
                      fontFamily: Lang,
                      fontWeight: FontWeight.normal,
                      fontSize: SystemControls.font3,
                      height: 0.1,
                      color: Color.fromRGBO(0, 0, 0, 0),
                    ),
                    textAlign: TextAlign.center,
                    maxLines: 1,
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              color: CardBG,
              height: 50,
              width: 100,
              alignment: Alignment.center,
              padding: EdgeInsets.only(bottom: 10,top: 10),
              child: Text(

                "offers".tr().toString(),
                style: CategoryTextStyle(Lang,SelectedColorText),
                textAlign: TextAlign.center,
              ),
            ),
          )
        ],
      )
  );
}

Widget CategoryVerticalCardItem(dynamic Category,int SelectedID, String Lang){
  Color CardBG;
  Color SelectedColorText;
  if(SelectedID == Category['categories_id']){
    CardBG = globals.accountColorsData['MainColor']!=null
        ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz;
    SelectedColorText = globals.accountColorsData['TextOnMainColor']!=null
        ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz;
  }
  else{
    CardBG = Colors.white;
    SelectedColorText = SystemControls.TextHeaderColorz;
  }
  return Container(
      decoration: BoxDecoration(
        color: CardBG,
        borderRadius: BorderRadius.all(
            Radius.circular(0)),
      ),
      margin: EdgeInsets.all(5),
      padding: EdgeInsets.only(top: 10,bottom: 10),
      child: Column(
        children: <Widget>[
          Container(
            child:
            Stack(
              children: <Widget>[
                //Center(child: SystemControls().circularProgress()),
                CachedNetworkImage(
                  imageUrl: SystemControls()
                      .GetImgPath(
                      Category['categories_img'],
                      "original"),
                  height: 30,
                ),
              ],
            ),
          ),
          Padding(
              padding: EdgeInsets.all(5)),
          Container(
            child: new Center(
                child: new Text(
                  Category['categories_title'],
                  //list[position],
                  style: CategoryTextStyle(Lang,SelectedColorText),
                  textAlign: TextAlign
                      .center,
                  maxLines: 2,
                )
            ),
          ),
        ],
      )
  );
}
Widget OfferVerticalCardItem(BuildContext context, int SelectedID, String Lang){
  Color CardBG;
  Color SelectedColorText;
  if(SelectedID == -2){
    CardBG = globals.accountColorsData['MainColor']!=null
        ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz;
    SelectedColorText = globals.accountColorsData['TextOnMainColor']!=null
        ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz;
  }
  else{
    CardBG = Colors.white;
    SelectedColorText = SystemControls.TextHeaderColorz;
  }
  return Container(
      decoration: BoxDecoration(
        color: CardBG,
        borderRadius: BorderRadius.all(
            Radius.circular(0)),
      ),
      margin: EdgeInsets.all(5),
      padding: EdgeInsets.only(top: 10,bottom: 10),
      child: Column(
        children: <Widget>[
          SystemControls().GetSVGImagesAsset(
              'assets/Icons/offer.svg', 25,
              Colors.black),
          Padding(padding: EdgeInsets.all(5)),
          Container(
            child: new Center(
                child: new Text(
                  "offers".tr().toString(),
                  style: CategoryTextStyle(Lang,SelectedColorText),
                  textAlign: TextAlign.center,
                  maxLines: 2,
                )
            ),
          ),
        ],
      )
  );
}

Widget CategoryVerticalTabHeaderImg(int Rotation){
  return Container(
    margin: EdgeInsets.only(top: 5, bottom: 5),
    child: RotatedBox(
      quarterTurns: Rotation,
      child: Image.asset(
        "assets/categoriesTabHeaderImg.png",
        color: globals.accountColorsData['MainColor']!=null
            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
        fit: BoxFit.fitWidth,),
    ),
  );
}
Widget CategoryVerticalTabRowImg(){
  return Container(
    margin: EdgeInsets.only(top: 5, bottom: 5),
    child: Image.asset("assets/rowBottom.png",
      color: globals.accountColorsData['MainColor']!=null
          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
      fit: BoxFit.fitWidth,),
  );
}