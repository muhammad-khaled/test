import 'dart:ui';

import '../locale/share_preferences_con.dart';
import '../screens/privacy.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../Controls.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:easy_localization/easy_localization.dart';

import '../screens/home.dart';
import '../screens/opinion.dart';
import '../screens/aboutUs.dart';
import '../screens/login.dart';
import '../screens/favrs.dart';
import '../screens/updateUser.dart';
import '../screens/ordersList.dart';
import '../screens/updatePassword.dart';
import '../screens/myAddresses.dart';
import '../screens/Branches.dart';
import '../screens/ChooseLayout.dart';
import '../screens/categoryListNew.dart';
import '../dataControl/userModule.dart';
import '../widgets/UI_Alert_Widgets.dart';
import '../dataControl/dataModule.dart';
import '../main.dart';

import '../globals.dart' as globals;

class HomeDrawerOut extends StatefulWidget {
  BuildContext contextx;
  String Lang;
  String Token;
  String Currency;
  List<dynamic> _CartMeals;
  int design_Control;
  bool hasRegistration;
  dynamic appInfo;
  List<dynamic> categoriesz = <dynamic>[];
  HomeDrawerOut(
      this.contextx,
      this.Lang,
      this.Token,
      this.Currency,
      this._CartMeals,
      this.design_Control,
      this.hasRegistration,
      this.appInfo,
      this.categoriesz) {
    if (Token == "") {
      isLogging = false;
    }
  }
  bool isLogging = true;

  @override
  State<HomeDrawerOut> createState() => _HomeDrawerOutState();
}

class _HomeDrawerOutState extends State<HomeDrawerOut> with SharePreferenceData  {
  String userName;

  GetCustomerData() {
    SystemControls().getDataFromPreferences("name").then((String name) {

      setState(() {
        userName = name;
      });
    });
  }

  dividingLineContainer() {
    return Container(
      height: 1,
      color: globals.accountColorsData['TextdetailsColor'] != null
          ? Color(int.parse(globals.accountColorsData['TextdetailsColor']))
          : SystemControls.TextdetailsColorz,
      margin: EdgeInsets.only(right: 15, left: 15),
    );
  }

  dividingLineWhiteContainer() {
    return Container(
      height: 1,
      color: Colors.white,
      margin: EdgeInsets.only(right: 15, left: 15),
    );
  }

  TextStyle Text_Style() {
    return TextStyle(
      fontSize: SystemControls.font3,
      fontFamily: widget.Lang,
      color: globals.accountColorsData['TextHeaderColor'] != null
          ? Color(int.parse(globals.accountColorsData['TextHeaderColor']))
          : SystemControls.TextHeaderColorz,
    );
  }

  Color ItemBGColor() {
    return globals.accountColorsData['MainColorOpacity'] != null
        ? Color(int.parse(globals.accountColorsData['MainColorOpacity']))
        : SystemControls.MainColorOpacityz;
  }

  SetLoginArea(BuildContext context) {
    if (widget.isLogging == false) {
      return Column(
        children: <Widget>[
          Container(
            color: ItemBGColor(),
            child: ListTile(
              title: Text(
                "login".tr().toString(),
                style: Text_Style(),
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            login(widget.Lang, widget.design_Control)));
              },
            ),
          ), //Login
          dividingLineWhiteContainer(),
        ],
      );
    } else {
      return Column(
        children: <Widget>[
          Container(
            color: ItemBGColor(),
            child: ListTile(
              title: Text(
                "profile".tr().toString(),
                style: Text_Style(),
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => updateUser(
                            widget.Lang, widget.Token, widget.design_Control)));
              },
            ),
          ), //Logout
          dividingLineWhiteContainer(),
          /*Container(
            child: ListTile(
              title: Text(AppLocalizations.of(context).translate("changePassword"),
                style: Text_Style(),
              ),
              onTap: (){
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (BuildContext context) =>
                        updatePassword(Lang,Token,design_Control)
                    ));
              },
            ),
          ), //changePassword
          dividingLineContainer(),*/
        ],
      );
    }
    return Container();
  }

  SetLogoutArea(BuildContext context) {
    if (widget.isLogging == true) {
      return Column(
        children: <Widget>[
          Container(
            //color: ItemBGColor(),
            child: ListTile(
              title: Text(
                "logout".tr().toString(),
                style: Text_Style(),
              ),
              onTap: () async {
                var res = await logoutApi(widget.Token, widget.Lang);
                if (res.status == 200) {
                  print(res.message);
                }
                SystemControls().LogoutSetUserData(context, widget.Lang);
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => Home(widget.Lang)),
                    ModalRoute.withName('/'));
              },
            ),
          ), //Logout
          dividingLineContainer(),
        ],
      );
    }
    return Container();
  }

  Widget build(BuildContext context) {
    print("widget.Token");
    print(widget.Token);
    print(widget.Token);
    print("widget.Token");
    GetCustomerData();
    return Drawer(
      child: Container(
        //color: SystemControls.MainColor,
        decoration: BoxDecoration(
          color: SystemControls.AppbarBGColorz,
          image: DecorationImage(
            image: AssetImage("assets/MenuBGFinal.png"),
            //repeat: ImageRepeat.repeat,
            //fit: BoxFit.cover,
          ),
        ),
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 20, right: 40, left: 40),
              height: 150,
              child: Center(
                child: Image.asset(
                  widget.Lang == "ar" ? 'assets/logoNameSide.png':'assets/logoNameSideEn.png',
                  height: 80,
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                //color: Colors.white,
                decoration: BoxDecoration(
                  color: Colors.white,
                  image: DecorationImage(
                    image: AssetImage("assets/MenuBGFinal.png"),
                    //repeat: ImageRepeat.repeat
                    //fit: BoxFit.cover
                  ),
                ),
                child: ListView(
                  padding: EdgeInsets.zero,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                         userName != null ?    Container(
                          child: ListTile(
                          
                            title:  userName != null ? RichText(
                              text: TextSpan(
                                text:
                                    "${"hello".tr().toString()} ", //"home",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18,

                                ),
                                children: <TextSpan>[
                                  new TextSpan(
                                      text: ' $userName',
                                      style: new TextStyle(fontWeight: FontWeight.bold,fontSize: 17)),
                                ]
                              ),

                            ):Text(""),
                            onTap: () {
                            },
                          ),
                        ):Container(width: 0,height: 0,),
                        Container(
                          child: ListTile(
                            title: Text(
                             "home".tr().toString(), //"home",
                              style: Text_Style(),
                            ),
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          Home(widget.Lang)));
                            },
                          ),
                        ), //home
                        dividingLineWhiteContainer(),

                        Container(
                          child: ListTile(
                            title: Text(
                              "offers".tr().toString(), //"home",
                              style: Text_Style(),
                            ),
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          CategoryMealsListNew(
                                              widget._CartMeals,
                                              widget.categoriesz,
                                              widget.Lang,
                                              -2,
                                              widget.Token,
                                              widget.Currency,
                                              -2,
                                              widget.design_Control,
                                              widget.hasRegistration,
                                              widget.appInfo)));
                            },
                          ),
                        ), //home
                        dividingLineWhiteContainer(),

                        widget.hasRegistration
                            ? (SetLoginArea(context))
                            : (Container()),

                        //SetLoginArea(),
                        widget.hasRegistration
                            ? (Column(
                                children: <Widget>[
                                  Container(
                                    color: ItemBGColor(),
                                    child: ListTile(
                                      title: Text(
                                  "addresses".tr().toString(),
                                        style: Text_Style(),
                                      ),
                                      onTap: () {
                                        if (widget.isLogging == false) {
                                          ErrorDialogAlertGoTOLogin(
                                              context,
                                            "mustLogin".tr().toString(),
                                              widget.Lang);
                                        } else {
                                          Navigator.pop(context);
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (BuildContext
                                                          context) =>
                                                      MyAddresses(
                                                          widget.Lang,
                                                          widget.Token,
                                                          widget.Currency,
                                                          widget
                                                              .design_Control)));
                                        }
                                      },
                                    ),
                                  ), //my addresses
                                  dividingLineWhiteContainer(),

                                  Container(
                                    color: ItemBGColor(),
                                    child: ListTile(
                                      title: Text(
                                        "favorite".tr().toString(),
                                        style: Text_Style(),
                                      ),
                                      onTap: () {
                                        if (widget.isLogging == false) {
                                          ErrorDialogAlertGoTOLogin(
                                              context,
                                              "mustLogin".tr().toString(),
                                              widget.Lang);
                                        } else {
                                          Navigator.pop(context);
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (BuildContext
                                                          context) =>
                                                      favsLay(
                                                          widget._CartMeals,
                                                          widget.Lang,
                                                          widget.Token,
                                                          widget.Currency,
                                                          widget.design_Control,
                                                          widget
                                                              .hasRegistration)));
                                        }
                                      },
                                    ),
                                  ), //favorite
                                  dividingLineWhiteContainer(),

                                  Container(
                                    color: ItemBGColor(),
                                    child: ListTile(
                                      title: Text(
                                        "orders".tr().toString(),
                                        style: Text_Style(),
                                      ),
                                      onTap: () {
                                        if (widget.isLogging == false) {
                                          ErrorDialogAlertGoTOLogin(
                                              context,
                                              "mustLogin".tr().toString(),
                                              widget.Lang);
                                        } else {
                                          Navigator.pop(context);
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (BuildContext
                                                          context) =>
                                                      OrderList(
                                                          widget.Lang,
                                                          widget.Token,
                                                          widget.Currency,
                                                          widget
                                                              .design_Control)));
                                        }
                                      },
                                    ),
                                  ), //orders
                                  dividingLineWhiteContainer(),
                                ],
                              ))
                            : (Container()),

                        Container(
                          child: ListTile(
                            title: Text(
                          "opinion".tr().toString(),
                              style: Text_Style(),
                            ),
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          opinion(
                                              widget.Lang,
                                              widget.design_Control,
                                              widget.Token)));
                            },
                          ),
                        ), //opinion
                        dividingLineContainer(),

                        Container(
                          child: ListTile(
                            title: Text(
                              "aboutUs".tr().toString(),
                              style: Text_Style(),
                            ),
                            onTap: () {
                              Navigator.pop(context);

                              print(widget.appInfo['infos_about']);
                              print(widget.Lang);
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          AboutUs(widget.appInfo, widget.Lang,
                                              widget.design_Control)));
                            },
                          ),
                        ), //aboutUs
                        dividingLineContainer(),
                        Container(
                          child: ListTile(
                            title: Text(
                             "privacyPolicy".tr().toString(),
                              style: Text_Style(),
                            ),
                            onTap: () {
                              Navigator.pop(context);

                              print(widget.appInfo['infos_privacy_policy']);
                              print(widget.Lang);
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          PrivacyPolicy(
                                              widget.appInfo,
                                              widget.Lang,
                                              widget.design_Control)));
                            },
                          ),
                        ), //aboutUs
                        dividingLineContainer(),

                        globals.numOfLang > 1
                            ? (Container(
                                child: ListTile(
                                  title: Text(
                                   "changeLanguage".tr().toString(),
                                    style: Text_Style(),
                                  ),
                                  onTap: () {
                                    Navigator.pop(context);
                                    print("on click button 22 ");
                                    SystemControls.width = 45;
                                    if (widget.Lang == "en") {
                                      Center(
                                        child:
                                            SystemControls().circularProgress(),
                                      );

                                      SystemControls()
                                          .storeDataToPreferences("lang", "ar")
                                          .then((value) {
                                        //appLanguage.changeLanguage(Locale("ar"));
                                        // AppLocalizations.of(context).load();
                                        EasyLocalization.of(context).setLocale(Locale('ar', 'EG'));
                                        print('\nSaving Done $value....\n');
                                        Navigator.pushAndRemoveUntil(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => MyApp()),
                                            ModalRoute.withName('/'));
                                      });
                                    } else {
                                      Center(
                                        child:
                                            SystemControls().circularProgress(),
                                      );
                                      //AppLocalizations(Locale('ar', ''));
                                      SystemControls()
                                          .storeDataToPreferences("lang", "en")
                                          .then((value) {
                                        EasyLocalization.of(context).setLocale(Locale('en', 'EN'));
                                        print('\nSaving Done $value....\n');
                                        Navigator.pushAndRemoveUntil(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => MyApp()),
                                            ModalRoute.withName('/'));
                                      });
                                    }
                                    /*
                            Navigator.pushAndRemoveUntil(context,
                                MaterialPageRoute(builder: (BuildContext context) => MyApp()),
                                ModalRoute.withName('/'));*/
                                  },
                                ),
                              ))
                            : (Container()), //changeLanguage
                        globals.numOfLang > 1
                            ? (dividingLineContainer())
                            : (Container()),

                        widget.hasRegistration
                            ? (SetLogoutArea(context))
                            : (Container()),

                        SystemControls.branchesValid
                            ? (Column(
                                children: <Widget>[
                                  Container(
                                    child: ListTile(
                                      title: Text(
                                        "branches".tr().toString(),
                                        style: Text_Style(),
                                      ),
                                      onTap: () {
                                        Navigator.pop(context);
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (BuildContext
                                                        context) =>
                                                    BranchesLayout(
                                                        widget.Lang,
                                                        widget.Token,
                                                        widget
                                                            .design_Control)));
                                      },
                                    ),
                                  ),
                                  dividingLineContainer(),
                                ],
                              ))
                            : (Container()),
                        SystemControls.branchesValid
                            ? (Column(
                                children: <Widget>[
                                  Container(
                                    child: ListTile(
                                      title: Text(
                                        "Chosser",
                                        style: Text_Style(),
                                      ),
                                      onTap: () {
                                        Navigator.pop(context);
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (BuildContext
                                                        context) =>
                                                    ChooseLayout(
                                                        widget.Lang,
                                                        widget
                                                            .design_Control)));
                                      },
                                    ),
                                  ),
                                  dividingLineContainer(),
                                ],
                              ))
                            : (Container()),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget HomeDrawerOutzzz(
    BuildContext context,
    String Lang,
    String Token,
    String Currency,
    List<dynamic> _CartMeals,
    int design_Control,
    bool hasRegistration,
    dynamic appInfo) {
  bool isLogging = true;
  if (Token == "") {
    isLogging = false;
  }
  dividingLineContainer() {
    return Container(
      height: 1,
      color: globals.accountColorsData['TextdetailsColor'] != null
          ? Color(int.parse(globals.accountColorsData['TextdetailsColor']))
          : SystemControls.TextdetailsColorz,
      margin: EdgeInsets.only(right: 15, left: 15),
    );
  }

  dividingLineWhiteContainer() {
    return Container(
      height: 1,
      color: Colors.white,
      margin: EdgeInsets.only(right: 15, left: 15),
    );
  }

  TextStyle Text_Style() {
    return TextStyle(
      fontSize: SystemControls.font3,
      fontFamily: Lang,
      color: globals.accountColorsData['TextHeaderColor'] != null
          ? Color(int.parse(globals.accountColorsData['TextHeaderColor']))
          : SystemControls.TextHeaderColorz,
    );
  }

  Color ItemBGColor() {
    return globals.accountColorsData['MainColorOpacity'] != null
        ? Color(int.parse(globals.accountColorsData['MainColorOpacity']))
        : SystemControls.MainColorOpacityz;
  }

  SetLoginArea() {
    if (isLogging == false) {
      return Column(
        children: <Widget>[
          Container(
            color: ItemBGColor(),
            child: ListTile(
              title: Text(
                "login".tr().toString(),
                style: Text_Style(),
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            login(Lang, design_Control)));
              },
            ),
          ), //Login
          dividingLineWhiteContainer(),
        ],
      );
    } else {
      return Column(
        children: <Widget>[
          Container(
            color: ItemBGColor(),
            child: ListTile(
              title: Text(
                "profile".tr().toString(),
                style: Text_Style(),
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            updateUser(Lang, Token, design_Control)));
              },
            ),
          ), //Logout
          dividingLineWhiteContainer(),
          /*Container(
            child: ListTile(
              title: Text(AppLocalizations.of(context).translate("changePassword"),
                style: Text_Style(),
              ),
              onTap: (){
                Navigator.pop(context);
                Navigator.push(context,
                    MaterialPageRoute(builder: (BuildContext context) =>
                        updatePassword(Lang,Token,design_Control)
                    ));
              },
            ),
          ), //changePassword
          dividingLineContainer(),*/
        ],
      );
    }
    return Container();
  }

  SetLogoutArea() {
    if (isLogging == true) {
      return Column(
        children: <Widget>[
          Container(
            //color: ItemBGColor(),
            child: ListTile(
              title: Text(
             "logout".tr().toString(),
                style: Text_Style(),
              ),
              onTap: () async {
                var res = await logoutApi(Token, Lang);
                if (res.status == 200) {
                  print(res.message);
                }
                SystemControls().LogoutSetUserData(context, Lang);
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => Home(Lang)),
                    ModalRoute.withName('/'));
              },
            ),
          ), //Logout
          dividingLineContainer(),
        ],
      );
    }
    return Container();
  }

  //var appLanguage = Provider.of<AppLanguage>(context);

  return Drawer(
    child: Container(
      //color: SystemControls.MainColor,
      decoration: BoxDecoration(
        color: Colors.white,
        /*image: DecorationImage(
          image: AssetImage("assets/MenuBGFinal.png"),
          repeat: ImageRepeat.repeat,
          //fit: BoxFit.cover,
        ),
        */
      ),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 20, right: 40, left: 40),
            height: 150,
            child: Center(
              child: Image.asset(
                Lang == "ar" ? 'assets/logoNameSide.png':'assets/logoNameSideEn.png',
                height: 80,
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              //color: Colors.white,
              decoration: BoxDecoration(
                color: Colors.white,
                image: DecorationImage(
                  image: AssetImage("assets/MenuBGFinal.png"),
                  //repeat: ImageRepeat.repeat
                  //fit: BoxFit.cover
                ),
              ),
              child: ListView(
                padding: EdgeInsets.zero,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        child: ListTile(
                          title: Text(
                         "home".tr().toString(), //"home",
                            style: Text_Style(),
                          ),
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        Home(Lang)));
                          },
                        ),
                      ), //home
                      dividingLineWhiteContainer(),

                      hasRegistration ? (SetLoginArea()) : (Container()),

                      //SetLoginArea(),
                      hasRegistration
                          ? (Column(
                              children: <Widget>[
                                Container(
                                  color: ItemBGColor(),
                                  child: ListTile(
                                    title: Text(
                                "addresses".tr().toString(),
                                      style: Text_Style(),
                                    ),
                                    onTap: () {
                                      if (isLogging == false) {
                                        ErrorDialogAlertGoTOLogin(
                                            context,
                                            "mustLogin".tr().toString(),
                                            Lang);
                                      } else {
                                        Navigator.pop(context);
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder:
                                                    (BuildContext context) =>
                                                        MyAddresses(
                                                            Lang,
                                                            Token,
                                                            Currency,
                                                            design_Control)));
                                      }
                                    },
                                  ),
                                ), //my addresses
                                dividingLineWhiteContainer(),

                                Container(
                                  color: ItemBGColor(),
                                  child: ListTile(
                                    title: Text(
                                    "favorite".tr().toString(),
                                      style: Text_Style(),
                                    ),
                                    onTap: () {
                                      if (isLogging == false) {
                                        ErrorDialogAlertGoTOLogin(
                                            context,
                                          "mustLogin".tr().toString(),
                                            Lang);
                                      } else {
                                        Navigator.pop(context);
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder:
                                                    (BuildContext context) =>
                                                        favsLay(
                                                            _CartMeals,
                                                            Lang,
                                                            Token,
                                                            Currency,
                                                            design_Control,
                                                            hasRegistration)));
                                      }
                                    },
                                  ),
                                ), //favorite
                                dividingLineWhiteContainer(),

                                Container(
                                  color: ItemBGColor(),
                                  child: ListTile(
                                    title: Text(
                                     "orders".tr().toString(),
                                      style: Text_Style(),
                                    ),
                                    onTap: () {
                                      if (isLogging == false) {
                                        ErrorDialogAlertGoTOLogin(
                                            context,
                                           "mustLogin".tr().toString(),
                                            Lang);
                                      } else {
                                        Navigator.pop(context);
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder:
                                                    (BuildContext context) =>
                                                        OrderList(
                                                            Lang,
                                                            Token,
                                                            Currency,
                                                            design_Control)));
                                      }
                                    },
                                  ),
                                ), //orders
                                dividingLineWhiteContainer(),
                              ],
                            ))
                          : (Container()),

                      Container(
                        child: ListTile(
                          title: Text(
                         "opinion".tr().toString(),
                            style: Text_Style(),
                          ),
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        opinion(Lang, design_Control, Token)));
                          },
                        ),
                      ), //opinion
                      dividingLineContainer(),

                      Container(
                        child: ListTile(
                          title: Text(
                            "aboutUs".tr().toString(),
                            style: Text_Style(),
                          ),
                          onTap: () {
                            Navigator.pop(context);

                            print(appInfo['infos_about']);
                            print(Lang);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) => AboutUs(
                                        appInfo, Lang, design_Control)));
                          },
                        ),
                      ), //aboutUs
                      dividingLineContainer(),

                      globals.numOfLang > 1
                          ? (Container(
                              child: ListTile(
                                title: Text(
                                  "changeLanguage".tr().toString(),
                                  style: Text_Style(),
                                ),
                                onTap: () {
                                  Navigator.pop(context);
                                  print("on click button 22 ");
                                  SystemControls.width = 45;
                                  if (Lang == "en") {
                                    Center(
                                      child:
                                          SystemControls().circularProgress(),
                                    );

                                    SystemControls()
                                        .storeDataToPreferences("lang", "ar")
                                        .then((value) {
                                      //appLanguage.changeLanguage(Locale("ar"));
                                      EasyLocalization.of(context).setLocale(Locale('ar', 'EG'));
                                      print('\nSaving Done $value....\n');
                                      Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => MyApp()),
                                          ModalRoute.withName('/'));
                                    });
                                  } else {
                                    Center(
                                      child:
                                          SystemControls().circularProgress(),
                                    );
                                    //AppLocalizations(Locale('ar', ''));
                                    SystemControls()
                                        .storeDataToPreferences("lang", "en")
                                        .then((value) {
                                      EasyLocalization.of(context).setLocale(Locale('en', 'EN'));
                                      print('\nSaving Done $value....\n');
                                      Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => MyApp()),
                                          ModalRoute.withName('/'));
                                    });
                                  }
                                  /*
                            Navigator.pushAndRemoveUntil(context,
                                MaterialPageRoute(builder: (BuildContext context) => MyApp()),
                                ModalRoute.withName('/'));*/
                                },
                              ),
                            ))
                          : (Container()), //changeLanguage
                      globals.numOfLang > 1
                          ? (dividingLineContainer())
                          : (Container()),

                      hasRegistration ? (SetLogoutArea()) : (Container()),

                      SystemControls.branchesValid
                          ? (Column(
                              children: <Widget>[
                                Container(
                                  child: ListTile(
                                    title: Text(
                                      "branches".tr().toString(),
                                      style: Text_Style(),
                                    ),
                                    onTap: () {
                                      Navigator.pop(context);
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  BranchesLayout(Lang, Token,
                                                      design_Control)));
                                    },
                                  ),
                                ),
                                dividingLineContainer(),
                              ],
                            ))
                          : (Container()),
                      SystemControls.branchesValid
                          ? (Column(
                              children: <Widget>[
                                Container(
                                  child: ListTile(
                                    title: Text(
                                      "Chosser",
                                      style: Text_Style(),
                                    ),
                                    onTap: () {
                                      Navigator.pop(context);
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  ChooseLayout(
                                                      Lang, design_Control)));
                                    },
                                  ),
                                ),
                                dividingLineContainer(),
                              ],
                            ))
                          : (Container()),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    ),
  );
}
/*
//Currency || Token || _CartMeals
Widget drawerWidge(BuildContext context, dynamic appInfo, String Lang, bool hasRegistration,
    bool isLogging, String Token, String Currency, List<dynamic> _CartMeals,
    int design_Control){



  final SystemControls storage = SystemControls();
  return Drawer(
    child: Container(
      //color: SystemControls.MainColor,
      decoration: BoxDecoration(
        color: SystemControls.MainColor,
        image: DecorationImage(
          image: AssetImage("assets/AppBGFinal.png"),
          repeat: ImageRepeat.repeat,
          //fit: BoxFit.cover,
        ),
      ),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 20),
            height: 150,
            child: Center(
              child: Image.asset(Lang =="ar"? 'assets/logo.png':"assets/logoEn.png",height: 80,),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              //color: Colors.white,
              decoration: BoxDecoration(color: Colors.white,
                image: DecorationImage(
                    image: AssetImage("assets/AppBGFinal.png"),
                    repeat: ImageRepeat.repeat
                  //fit: BoxFit.cover
                ),
              ),
              child: ListView(
                padding: EdgeInsets.zero,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        child: ListTile(
                          title: Text(AppLocalizations.of(context).translate("home"),//"home",
                            style: Text_Style(),
                          ),
                          onTap: (){
                            Navigator.pop(context);
                            Navigator.push(context,
                                MaterialPageRoute(builder: (BuildContext context) =>
                                    Home(Lang)
                                ));
                          },
                        ),
                      ), //home
                      dividingLineContainer(),

                      hasRegistration?(
                          SetLoginArea()
                      ):(
                          Container()
                      ),

                      //SetLoginArea(),
                      hasRegistration?(
                          Column(
                            children: <Widget>[
                              Container(
                                child: ListTile(
                                  title: Text(AppLocalizations.of(context).translate("addresses"),
                                    style: Text_Style(),
                                  ),
                                  onTap: (){
                                    if(isLogging == false){
                                      ErrorDialogAlert(context,
                                          AppLocalizations.of(context).translate("mustLogin"));
                                    }
                                    else{
                                      Navigator.pop(context);
                                      Navigator.push(context,
                                          MaterialPageRoute(builder: (BuildContext context) =>
                                              MyAddresses(Lang, Token, Currency, design_Control)
                                          ));
                                    }
                                  },
                                ),
                              ), //my addresses
                              dividingLineContainer(),

                              Container(
                                child: ListTile(
                                  title: Text(AppLocalizations.of(context).translate("favorite"),
                                    style: Text_Style(),
                                  ),
                                  onTap: (){
                                    if(isLogging == false){
                                      ErrorDialogAlert(context,
                                          AppLocalizations.of(context).translate("mustLogin"));
                                    }
                                    else{
                                      Navigator.pop(context);
                                      Navigator.push(context,
                                          MaterialPageRoute(builder: (BuildContext context) =>
                                              favsLay(_CartMeals, Lang, Token,
                                                  Currency,design_Control,
                                                  hasRegistration)
                                          ));
                                    }
                                  },
                                ),
                              ), //favorite
                              dividingLineContainer(),

                              Container(
                                child: ListTile(
                                  title: Text(AppLocalizations.of(context).translate("orders"),
                                    style: Text_Style(),
                                  ),
                                  onTap: (){
                                    if(isLogging == false){
                                      ErrorDialogAlert(context,
                                          AppLocalizations.of(context).translate("mustLogin"));
                                    }
                                    else{
                                      Navigator.pop(context);
                                      Navigator.push(context,
                                          MaterialPageRoute(builder: (BuildContext context) =>
                                              OrderList(Lang, Token, Currency,design_Control)
                                          ));
                                    }

                                  },
                                ),
                              ), //orders
                              dividingLineContainer(),
                            ],
                          )
                      ):(
                          Container()
                      ),

                      Container(
                        child: ListTile(
                          title: Text(AppLocalizations.of(context).translate("opinion"),
                            style: Text_Style(),
                          ),
                          onTap: (){
                            Navigator.pop(context);
                            Navigator.push(context,
                                MaterialPageRoute(builder: (BuildContext context) =>
                                    opinion(Lang,design_Control)
                                ));
                          },
                        ),
                      ), //opinion
                      dividingLineContainer(),

                      Container(
                        child: ListTile(
                          title: Text(AppLocalizations.of(context).translate("aboutUs"),
                            style: Text_Style(),
                          ),
                          onTap: (){
                            Navigator.pop(context);

                            print(appInfo['infos_about']);
                            print(Lang);
                            Navigator.push(context,
                                MaterialPageRoute(builder: (BuildContext context) =>
                                    aboutUs(appInfo,Lang,design_Control)
                                ));
                          },
                        ),
                      ), //aboutUs
                      dividingLineContainer(),

                      Container(
                        child: ListTile(
                          title: Text(AppLocalizations.of(context).translate("changeLanguage"),
                            style: Text_Style(),
                          ),
                          onTap: (){
                            Navigator.pop(context);
                            print("on click button 22 ");
                            SystemControls.width = 45;
                            if(Lang == "en"){
                              Center(
                                child: SystemControls().circularProgress(),
                              );

                              storage.storeDataToPreferences("lang", "ar").then((value){
                                print('\nSaving Done $value....\n');
                                Navigator.pushAndRemoveUntil(context,
                                    MaterialPageRoute(builder: (context) => MyApp()),
                                    ModalRoute.withName('/'));
                              });
                            }
                            else {
                              Center(
                                child: SystemControls().circularProgress(),
                              );
                              //AppLocalizations(Locale('ar', ''));
                              storage.storeDataToPreferences("lang", "en").then((value){
                                print('\nSaving Done $value....\n');
                                Navigator.pushAndRemoveUntil(context,
                                    MaterialPageRoute(builder: (context) => MyApp()),
                                    ModalRoute.withName('/'));
                              });
                            }/*
                            Navigator.pushAndRemoveUntil(context,
                                MaterialPageRoute(builder: (BuildContext context) => MyApp()),
                                ModalRoute.withName('/'));*/
                          },
                        ),
                      ), //changeLanguage
                      dividingLineContainer(),

                      hasRegistration?(
                          SetLogoutArea()
                      ):(
                          Container()
                      ),

                      SystemControls.branchesValid?(
                          Column(
                            children: <Widget>[
                              Container(
                                child: ListTile(
                                  title: Text(AppLocalizations.of(context).translate("branches"),
                                    style: Text_Style(),
                                  ),
                                  onTap: (){
                                    Navigator.pop(context);
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (BuildContext context) =>
                                            BranchesLayout(Lang,Token,design_Control)
                                        ));
                                  },
                                ),
                              ),
                              dividingLineContainer(),
                            ],
                          )
                      ):(
                          Container()
                      ),
                      SystemControls.branchesValid?(
                          Column(
                            children: <Widget>[
                              Container(
                                child: ListTile(
                                  title: Text("Chosser",
                                    style: Text_Style(),
                                  ),
                                  onTap: (){
                                    Navigator.pop(context);
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (BuildContext context) =>
                                            ChooseLayout(Lang,design_Control)
                                        ));
                                  },
                                ),
                              ),
                              dividingLineContainer(),
                            ],
                          )
                      ):(
                          Container()
                      ),


                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    ),
  );

}*/
