import '../../provider/Drawer/DrawerProvider.dart';

import '../../dataControl/cartModule.dart';
import '../../provider/productsInCartProvider.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import '../../screens/HomePageScreenTheamTwo/HomePageScreenTheamTwo.dart';
import '../../screens/HomeTheamOne/HomePageScreenTheamOne.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import '../Controls.dart';

import '../screens/home.dart';
import '../screens/login.dart';
import '../screens/splash.dart';
import '../screens/cart1Items.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:android_intent/android_intent.dart';

import '../globals.dart' as globals;

import 'package:android_intent/android_intent.dart';
import 'package:geolocator/geolocator.dart';
import 'package:easy_localization/easy_localization.dart';
Future<void> SuccDialogAlertBackHome(BuildContext context, String Lang, String mes) {

  return showDialog<void>(
    context: context,
    barrierColor: Colors.white12,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      Future<CartList> localCart = read_from_file();

      Timer(Duration(seconds: 2), () async{
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("lang", Lang);
        Navigator.of(context
        ).pop();
        switch(SystemControls.theme){

          case "1":
            Navigator.of(context).popUntil((route) => route.isFirst);
                Navigator.pushReplacement(context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => HomePageScreenTheamOne(),)

            );
            break; case "2":
          Navigator.of(context).popUntil((route) => route.isFirst);


              Navigator.pushReplacement(context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => HomePageScreenTheamTwo(),)

            );
            break;
        }
      });

      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
        ),
        title: Text(mes,
          style: TextStyle(
              fontFamily: Lang,
              fontWeight: FontWeight.bold,
              fontSize: SystemControls.font3,
              color: globals.accountColorsData['TextHeaderColor']!=null
                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
          ),
          textAlign: TextAlign.center,
        ),
        contentPadding: EdgeInsets.only(top: 40,bottom: 0),
        content: ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
            bottomRight: Radius.circular(SystemControls.RadiusCircValue),
          ),
          child: Container(
            height: 80,
            margin: EdgeInsets.only(bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Consumer<CartStateProvider>(
                  builder: (context, cart, _) {
                    return FutureBuilder<CartList>(
                      future: localCart,
                      builder: (context, Cartsnap) {
                        if (Cartsnap.connectionState ==
                            ConnectionState.done &&
                            Cartsnap.hasData) {
                          cart.setCartList(Cartsnap.data.Cmeals);

                          CartStateProvider cartState =
                          Provider.of<CartStateProvider>(context,
                              listen: false);
                          if (cartState.productsCount == "0" &&
                              cart.cartList.length != 0) {
                            cartState.setCurrentProductsCount(
                                cart.cartList.length.toString());
                          }
                          return Container();
                        }
                        return Container();
                      },
                    );
                  },
                ),
                Center(
                  child: Icon(Icons.verified_user,size: 80,
                    color: globals.accountColorsData['MainColor']!=null
                        ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,),
                )
              ],
            ),
          ),
        ),
      );
    },
  );
}
Future<void> SuccDialogAlertLogin(BuildContext context, String Lang, String mes) {

  return showDialog<void>(
    context: context,
    barrierColor: Colors.white12,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      Future<CartList> localCart = read_from_file();

      Timer(Duration(seconds: 2), () async{
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("lang", Lang);
        Navigator.of(context
        ).pop();
        switch(SystemControls.theme){

          case "1":
            Navigator.of(context).popUntil((route) => route.isFirst);
                Navigator.pushReplacement(context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => HomePageScreenTheamOne(),)

            );
            break; case "2":
          Navigator.of(context).popUntil((route) => route.isFirst);


              Navigator.pushReplacement(context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => HomePageScreenTheamTwo(),)

            );
            break;
        }
      });

      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
        ),
        title: Text(mes,
          style: TextStyle(
              fontFamily: Lang,
              fontWeight: FontWeight.bold,
              fontSize: SystemControls.font3,
              color: globals.accountColorsData['TextHeaderColor']!=null
                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
          ),
          textAlign: TextAlign.center,
        ),
        contentPadding: EdgeInsets.only(top: 40,bottom: 0),
        content: ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
            bottomRight: Radius.circular(SystemControls.RadiusCircValue),
          ),
          child: Container(
            height: 80,
            margin: EdgeInsets.only(bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Consumer<CartStateProvider>(
                  builder: (context, cart, _) {
                    return FutureBuilder<CartList>(
                      future: localCart,
                      builder: (context, Cartsnap) {
                        if (Cartsnap.connectionState ==
                            ConnectionState.done &&
                            Cartsnap.hasData) {
                          cart.setCartList(Cartsnap.data.Cmeals);

                          CartStateProvider cartState =
                          Provider.of<CartStateProvider>(context,
                              listen: false);
                          if (cartState.productsCount == "0" &&
                              cart.cartList.length != 0) {
                            cartState.setCurrentProductsCount(
                                cart.cartList.length.toString());
                          }
                          return Container();
                        }
                        return Container();
                      },
                    );
                  },
                ),
                Center(
                  child: Icon(Icons.verified_user,size: 80,
                    color: globals.accountColorsData['MainColor']!=null
                        ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,),
                )
              ],
            ),
          ),
        ),
      );
    },
  );
}

Future<void> SuccDialogAlertGoCart(BuildContext contextt,String mes, String Lang,
    String UserToken, String AppCurrency, int design_Control) {
  Future<CartList> localCart = read_from_file();

  return showDialog<void>(
    context: contextt,
    barrierColor: Colors.white12,

    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {

      Timer(Duration(seconds: 2), () async{
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("lang", Lang);
        Navigator.of(context).pop();
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (BuildContext context) =>
                CartLayout(Lang, UserToken, AppCurrency, design_Control)
            ),
        );
      });
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
        ),
        title: Text(mes,
          style: TextStyle(
              fontFamily: Lang,
              fontWeight: FontWeight.bold,
              fontSize: SystemControls.font3,
              color: globals.accountColorsData['TextHeaderColor']!=null
                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
          ),
          textAlign: TextAlign.center,
        ),
        contentPadding: EdgeInsets.only(top: 40,bottom: 0),
        content: ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
            bottomRight: Radius.circular(SystemControls.RadiusCircValue),
          ),
          child: Container(
            height: 80,
            margin: EdgeInsets.only(bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Consumer<CartStateProvider>(
                  builder: (context, cart, _) {
                    return FutureBuilder<CartList>(
                      future: localCart,
                      builder: (context, Cartsnap) {
                        if (Cartsnap.connectionState ==
                            ConnectionState.done &&
                            Cartsnap.hasData) {
                          cart.setCartList(Cartsnap.data.Cmeals);

                          CartStateProvider cartState =
                          Provider.of<CartStateProvider>(context,
                              listen: false);
                          if (cartState.productsCount == "0" &&
                              cart.cartList.length != 0) {
                            cartState.setCurrentProductsCount(
                                cart.cartList.length.toString());
                          }
                          return Container();
                        }
                        return Container();
                      },
                    );
                  },
                ),
                Center(
                  child: Icon(Icons.verified_user,size: 80,
                    color: globals.accountColorsData['MainColor']!=null
                        ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,),
                )
              ],
            ),
          ),
        ),
      );
    },
  );
}


Future<void> youNeedToSelectChoiceDialog(BuildContext context) {
  return showDialog<void>(
    context: context,
    barrierColor: Colors.white12,

    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      Timer(Duration(seconds: 2), () async{
        Navigator.pop(context);

      });
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
        ),
        title: Text("youNeedToSelectChoice".tr().toString(),
          style: TextStyle(
              fontFamily: Lang,
              fontWeight: FontWeight.bold,
              fontSize: SystemControls.font3,
              color: globals.accountColorsData['TextHeaderColor']!=null
                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
          ),
          textAlign: TextAlign.center,
        ),
        contentPadding: EdgeInsets.only(top: 40,bottom: 0),
        content: ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
            bottomRight: Radius.circular(SystemControls.RadiusCircValue),
          ),
          child: Container(
            height: 80,
            margin: EdgeInsets.only(bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Icon(Icons.error_outline,size: 80,
                    color: globals.accountColorsData['MainColor']!=null
                        ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,),
                )
              ],
            ),
          ),
        ),
      );
    },
  );
}

Future<void> couponNotValid(BuildContext context,String message) {
  return showDialog<void>(
    context: context,
    barrierColor: Colors.white12,

    barrierDismissible: true, // user must tap button!
    builder: (BuildContext context) {
      // Timer(Duration(seconds: 2), () async{
      //   Navigator.pop(context);
      //
      // });
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
        ),
        title: Text("$message",
          style: TextStyle(
              fontFamily: Lang,
              fontWeight: FontWeight.bold,
              fontSize: SystemControls.font3,
              color: globals.accountColorsData['TextHeaderColor']!=null
                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
          ),
          textAlign: TextAlign.center,
        ),
        contentPadding: EdgeInsets.only(top: 40,bottom: 0),
        content: ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
            bottomRight: Radius.circular(SystemControls.RadiusCircValue),
          ),
          child: Container(
            height: 80,
            margin: EdgeInsets.only(bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Icon(Icons.error_outline,size: 80,
                    color: globals.accountColorsData['MainColor']!=null
                        ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,),
                )
              ],
            ),
          ),
        ),
      );
    },
  );
}
Future<void> passwordWrong(BuildContext context,String message) {
  return showDialog<void>(
    context: context,
    barrierColor: Colors.white12,

    barrierDismissible: true, // user must tap button!
    builder: (BuildContext context) {
      Timer(Duration(seconds: 1), () async{
        Navigator.pop(context);

      });
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
        ),
        title: Text("$message",
          style: TextStyle(
              fontFamily: Lang,
              fontWeight: FontWeight.bold,
              fontSize: SystemControls.font3,
              color: globals.accountColorsData['TextHeaderColor']!=null
                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
          ),
          textAlign: TextAlign.center,
        ),
        contentPadding: EdgeInsets.only(top: 40,bottom: 0),
        content: ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
            bottomRight: Radius.circular(SystemControls.RadiusCircValue),
          ),
          child: Container(
            height: 80,
            margin: EdgeInsets.only(bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Icon(Icons.error_outline,size: 80,
                    color: globals.accountColorsData['MainColor']!=null
                        ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,),
                )
              ],
            ),
          ),
        ),
      );
    },
  );
}


Future<void> SuccDialogAlertStaySameLayout(BuildContext context, String mes,Key key) {
  Future<CartList> localCart = read_from_file();
  Timer _timer;

  return showDialog<void>(

    context: context,
    barrierDismissible: true,

    // user must tap button!
    builder: (BuildContext context) {
      _timer = Timer(Duration(seconds: 1), () {
        Navigator.of(context, rootNavigator: true).pop();
      });
      return AlertDialog(


        key: key,
        titlePadding: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
        ),
        title: Row(
          children: [
            IconButton(onPressed: (){
              Navigator.of(context, rootNavigator: true).pop();

            }, icon: Icon(Icons.close)),

            Spacer(),
          ],
        ),
        contentPadding: EdgeInsets.only(top: 40,bottom: 0),
        content: ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
            bottomRight: Radius.circular(SystemControls.RadiusCircValue),
          ),
          child: Container(
            height: 150,
            margin: EdgeInsets.only(bottom: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[

                Container(
                  padding: EdgeInsets.only(left: 15,right: 15),

                  child: Text(mes,
                    maxLines: 4,
                    style: TextStyle(

                        fontFamily: Lang,
                        fontWeight: FontWeight.bold,
                        fontSize: SystemControls.font3,
                        color: globals.accountColorsData['TextHeaderColor']!=null
                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                Spacer(),
                Consumer<CartStateProvider>(
                  builder: (contextt, cart, _) {
                    return FutureBuilder<CartList>(
                      future: localCart,
                      builder: (contextt, Cartsnap) {
                        if (Cartsnap.connectionState ==
                            ConnectionState.done &&
                            Cartsnap.hasData) {
                          cart.setCartList(Cartsnap.data.Cmeals);

                          CartStateProvider cartState =
                          Provider.of<CartStateProvider>(contextt,
                              listen: false);
                          if (cartState.productsCount == "0" &&
                              cart.cartList.length != 0) {
                            cartState.setCurrentProductsCount(
                                cart.cartList.length.toString());
                          }
                          return Container();
                        }
                        return Container();
                      },
                    );
                  },
                ),
                Center(
                  child: Icon(Icons.verified_user,size: 80,
                    color: globals.accountColorsData['MainColor']!=null
                        ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,),
                )
              ],
            ),
          ),
        ),
      );
    },
  ).then((value){
    if (_timer.isActive) {
      _timer.cancel();
    }
  });
}


Future<void> SuccDialogAlertCoupon(
    {BuildContext context, var mes, String type, String appCurrency}) {
  Future<CartList> localCart = read_from_file();

  return showDialog<void>(
    context: context,
    barrierDismissible: true, // user must tap button!
    builder: (BuildContext context) {
      // Timer(Duration(seconds: 2), ()=>
      //     // Navigator.pop(context)
      // );
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
        ),
        title: Text("couponSuccess".tr().toString(),
          style: TextStyle(
              fontFamily: Lang,
              fontWeight: FontWeight.bold,
              fontSize: SystemControls.font3,
              color: globals.accountColorsData['TextHeaderColor']!=null
                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
          ),
          textAlign: TextAlign.center,
        ),
        contentPadding: EdgeInsets.only(top: 40,bottom: 0),
        content: ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
            bottomRight: Radius.circular(SystemControls.RadiusCircValue),
          ),
          child: Container(
            height: 20.h,
            width: MediaQuery.of(context).size.width/1.2,
            margin: EdgeInsets.only(bottom: 10),
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text( "theCouponDiscount".tr().toString(),
                      style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.bold,
                          fontSize: SystemControls.font3,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Text(type!="rate" ? mes +" "+ appCurrency:mes+" %",
                      style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.bold,
                          fontSize: SystemControls.font3,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Consumer<CartStateProvider>(
                      builder: (contextt, cart, _) {
                        return FutureBuilder<CartList>(
                          future: localCart,
                          builder: (contextt, Cartsnap) {
                            if (Cartsnap.connectionState ==
                                ConnectionState.done &&
                                Cartsnap.hasData) {
                              cart.setCartList(Cartsnap.data.Cmeals);

                              CartStateProvider cartState =
                              Provider.of<CartStateProvider>(contextt,
                                  listen: false);
                              if (cartState.productsCount == "0" &&
                                  cart.cartList.length != 0) {
                                cartState.setCurrentProductsCount(
                                    cart.cartList.length.toString());
                              }
                              return Container();
                            }
                            return Container();
                          },
                        );
                      },
                    ),
                    Center(
                      child: Icon(Icons.verified_user,size: 80,
                        color: globals.accountColorsData['MainColor']!=null
                            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}


Future<void> ErrorDialogAlert(BuildContext contextt, String mes) {
  return showDialog<void>(
    context: contextt,
    barrierDismissible: true, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
        ),
        title: Text(mes,
          style: TextStyle(
              fontFamily: Lang,
              fontWeight: FontWeight.bold,
              fontSize: SystemControls.font3,
              color: globals.accountColorsData['TextHeaderColor']!=null
                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
          ),
          textAlign: TextAlign.center,
        ),
        contentPadding: EdgeInsets.only(top: 40,bottom: 0),
        content: ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
            bottomRight: Radius.circular(SystemControls.RadiusCircValue),
          ),
          child: Container(
            height: 40,
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                      //borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: FlatButton(
                      child: Text("ok".tr().toString(),
                        style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font3,
                            color: globals.accountColorsData['TextOnMainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
                        ),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                        // Navigator.of(context).pop();
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}
Future<void> ErrorDialogAlertGoTOLogin(BuildContext contextt,String mes,String lang) {
  return showDialog<void>(
    context: contextt,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
        ),
        title: Text(mes,
          style: TextStyle(
              fontFamily: Lang,
              fontWeight: FontWeight.bold,
              fontSize: SystemControls.font3,
              color: globals.accountColorsData['TextHeaderColor']!=null
                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
          ),
          textAlign: TextAlign.center,
        ),
        contentPadding: EdgeInsets.only(top: 40,bottom: 0),
        content: ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
            bottomRight: Radius.circular(SystemControls.RadiusCircValue),
          ),
          child: Container(
            height: 40,
         child : Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                      border: Border(
                        top: BorderSide(color: globals.accountColorsData['TextHeaderColor']!=null
                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz),
                      ),
                    ),
                    child: FlatButton(
                      child: Text("cancel".tr().toString(),
                        style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font3,
                            color: globals.accountColorsData['TextOnMainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
                        ),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ),
                Container(
                  height: 40,
                  width: 1,
                  color: Colors.black,
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                      border: Border(
                        top: BorderSide(color: globals.accountColorsData['TextHeaderColor']!=null
                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz),
                      ),
                    ),
                    child: FlatButton(
                      child: Text("login".tr().toString(),
                        style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font3,
                            color: globals.accountColorsData['TextOnMainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
                        ),
                      ),
                      onPressed: ()async {
                        SharedPreferences prefs = await SharedPreferences.getInstance();
                        prefs.setString("lang", Lang);
                        Navigator.of(context).pop();
                        Navigator.push(contextt,
                            MaterialPageRoute(builder: (BuildContext context) =>
                                login(Lang,SystemControls.designControl)
                            ));
                      },
                    ),
                  ),
                ),


              ],
            )
          ),
        ),
      );
    },
  );
}
Future<void> ErrorDialogAlertBackHome(BuildContext context, String mes) {
  print('BackHome BAckHome');
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
        ),
        title: Text(mes,
          style: TextStyle(
              fontFamily: Lang,
              fontWeight: FontWeight.bold,
              fontSize: SystemControls.font3,
              color: globals.accountColorsData['TextHeaderColor']!=null
                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
          ),
          textAlign: TextAlign.center,
        ),
        contentPadding: EdgeInsets.only(top: 40,bottom: 0),
        content: ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
            bottomRight: Radius.circular(SystemControls.RadiusCircValue),
          ),
          child: Container(
            height: 40,
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                      //borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: FlatButton(
                      child: Text("ok".tr().toString(),
                        style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font3,
                            color: globals.accountColorsData['TextOnMainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
                        ),
                      ),

                      onPressed: () {
                        print('cccccccccccccccccccc');
                        switch(SystemControls.theme){

                          case "1":
                            Navigator.of(context).popUntil((route) => route.isFirst);
                            Navigator.pushReplacement(context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) => HomePageScreenTheamOne(),)

                            );
                            break; case "2":
                          Navigator.of(context).popUntil((route) => route.isFirst);


                          Navigator.pushReplacement(context,
                              MaterialPageRoute(
                                builder: (BuildContext context) => HomePageScreenTheamTwo(),)

                          );
                          break;
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}
Future<void> ErrorDialogAlertBackCart(BuildContext context, String mes) {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
        ),
        title: Text(mes,
          style: TextStyle(
              fontFamily: Lang,
              fontWeight: FontWeight.bold,
              fontSize: SystemControls.font3,
              color: globals.accountColorsData['TextHeaderColor']!=null
                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
          ),
          textAlign: TextAlign.center,
        ),
        contentPadding: EdgeInsets.only(top: 40,bottom: 0),
        content: ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
            bottomRight: Radius.circular(SystemControls.RadiusCircValue),
          ),
          child: Container(
            height: 40,
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                      //borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: FlatButton(
                      child: Text("ok".tr().toString(),
                        style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font3,
                            color: globals.accountColorsData['TextOnMainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
                        ),
                      ),
                      onPressed: () {
                        print('ffffffffhhhhhhhhh');
                        Navigator.pop(context);
                            Navigator.of(context).popUntil((route) => route.isFirst);
                        print('ffffffffhhhhhhhhh');

                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}
CheckGpsAlert(BuildContext contextt ) async {
  print("\n\n\n\n\n_checkGps ... open out\n\n\n\n");
  if (!(await Geolocator().isLocationServiceEnabled())) {
    if (Theme.of(contextt).platform == TargetPlatform.android) {
  return showDialog<void>(
    context: contextt,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
        ),
        title: Text("gpsCheckTitle".tr().toString()+
            "\n"+ "gpsOpenMes".tr().toString(),
          style: TextStyle(
              fontFamily: Lang,
              fontWeight: FontWeight.bold,
              fontSize: SystemControls.font3,
              color: globals.accountColorsData['TextHeaderColor']!=null
                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
          ),
          textAlign: TextAlign.center,),
        content: ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
            bottomRight: Radius.circular(SystemControls.RadiusCircValue),
          ),
          child: Container(
            height: 40,
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                      border: Border(
                        top: BorderSide(color: globals.accountColorsData['TextHeaderColor']!=null
                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz),
                      ),
                      //borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: FlatButton(
                      child: Text("cancel".tr().toString(),
                        style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font3,
                            color: globals.accountColorsData['TextOnMainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
                        ),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ),
                Container(
                  height: 40,
                  width: 1,
                  color: Colors.black,
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                      border: Border(
                        top: BorderSide(color: globals.accountColorsData['TextHeaderColor']!=null
                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz),
                      ),
                    ),
                    child: FlatButton(
                      child: Text("ok".tr().toString(),
                        style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font3,
                            color: globals.accountColorsData['TextOnMainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
                        ),
                      ),
                      onPressed: () {
                        final AndroidIntent intent = AndroidIntent(
                            action: 'android.settings.LOCATION_SOURCE_SETTINGS');
                        intent.launch();
                        Navigator.of(context, rootNavigator: true).pop();
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );}}
}


Future<void> deleteDialogAlert(BuildContext contextt, String mes,Function ok,String lang) {
  return showDialog<void>(
    context: contextt,
    barrierDismissible: true, // user must tap button!
    builder: (BuildContext context) {
      Provider.of<DrawerProvider>(context,listen: false).fetchFavoriteProduct(contextt);

      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
        ),
        title: Text("$mes".tr().toString(),
          style: TextStyle(
              fontFamily: Lang,
              fontWeight: FontWeight.bold,
              fontSize: SystemControls.font3,
              color: globals.accountColorsData['TextHeaderColor']!=null
                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
          ),
          textAlign: TextAlign.center,
        ),
        contentPadding: EdgeInsets.only(top: 40,bottom: 0),
        content: ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
            bottomRight: Radius.circular(SystemControls.RadiusCircValue),
          ),
          child: Container(
            height: 40,
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                      //borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: TextButton(
                      child: Text("cancel".tr().toString(),
                        style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font3,
                            color: globals.accountColorsData['TextOnMainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
                        ),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                        // Navigator.of(context).pop();
                      },
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                      //borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: TextButton(
                      child: Text("ok".tr().toString(),
                        style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font3,
                            color: globals.accountColorsData['TextOnMainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
                        ),
                      ),
                      onPressed: () {
                        ok();
                        Navigator.of(context).pop();
                        // Navigator.of(context).pop();
                      },
                    ),
                  ),
                ),

              ],
            ),
          ),
        ),
      );
    },
  );
}