//
//
//
//
// // To parse this JSON data, do
// //
// //     final initalApiModel = initalApiModelFromJson(jsonString);
//
// import 'dart:convert';
//
// InitalApiModel initalApiModelFromJson(String str) => InitalApiModel.fromJson(json.decode(str));
//
// String initalApiModelToJson(InitalApiModel data) => json.encode(data.toJson());
//
// class InitalApiModel {
//   InitalApiModel({
//     this.status,
//     this.message,
//     this.errors,
//     this.data,
//   });
//
//   int status;
//   String message;
//   dynamic errors;
//   Data data;
//
//   factory InitalApiModel.fromJson(Map<String, dynamic> json) => InitalApiModel(
//     status: json["status"],
//     message: json["message"],
//     errors: json["errors"],
//     data: Data.fromJson(json["data"]),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "status": status,
//     "message": message,
//     "errors": errors,
//     "data": data.toJson(),
//   };
// }
//
// class Data {
//   Data({
//     this.account,
//     this.info,
//     this.setting,
//     this.langs,
//     this.defaultLang,
//     this.hasRegistration,
//     this.paymentMethods,
//   });
//
//   Account account;
//   Info info;
//   Setting setting;
//   List<Lang> langs;
//   String defaultLang;
//   bool hasRegistration;
//   List<PaymentMethod> paymentMethods;
//
//   factory Data.fromJson(Map<String, dynamic> json) => Data(
//     account: Account.fromJson(json["account"]),
//     info: Info.fromJson(json["info"]),
//     setting: Setting.fromJson(json["setting"]),
//     langs: List<Lang>.from(json["langs"].map((x) => Lang.fromJson(x))),
//     defaultLang: json["defaultLang"],
//     hasRegistration: json["hasRegistration"],
//     paymentMethods: List<PaymentMethod>.from(json["paymentMethods"].map((x) => PaymentMethod.fromJson(x))),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "account": account.toJson(),
//     "info": info.toJson(),
//     "setting": setting.toJson(),
//     "langs": List<dynamic>.from(langs.map((x) => x.toJson())),
//     "defaultLang": defaultLang,
//     "hasRegistration": hasRegistration,
//     "paymentMethods": List<dynamic>.from(paymentMethods.map((x) => x.toJson())),
//   };
// }
//
// class Account {
//   Account({
//     this.accountsId,
//     this.accountsName,
//     this.accountsSlug,
//     this.accountsImgDashboard,
//     this.accountsImgRtl,
//     this.accountsImgLtr,
//     this.accountsImgHeader,
//     this.accountsImgApp,
//     this.accountsShowBranches,
//     this.accountsShowAreas,
//     this.accountsShowCoupons,
//     this.accountsSingleMenu,
//     this.accountsCountry,
//     this.accountsCountryCode,
//     this.accountsMultipleCountries,
//     this.accountsCountryMobileDigitsCount,
//     this.accountsTimezone,
//     this.accountsCountryLatLong,
//     this.accountsShowCalories,
//     this.accountsShowProductType,
//     this.accountsFcmKey,
//     this.accountsFcmApiKey,
//     this.accountsFcmAuthDomain,
//     this.accountsFcmDatabaseUrl,
//     this.accountsFcmProjectId,
//     this.accountsFcmStorageBucket,
//     this.accountsFcmMessagingSenderId,
//     this.accountsFcmAppId,
//     this.accountsFcmMeasurementId,
//     this.accountsAndroidUrl,
//     this.accountsIosUrl,
//     this.accountsProductUrl,
//     this.accountsNumberDecimalDigits,
//     this.accountsStatus,
//     this.accountsPosition,
//     this.accountsWebServiceToken,
//     this.accountsCreatedAt,
//     this.accountsUpdatedAt,
//     this.colors,
//     this.productsName,
//     this.langs,
//     this.translations,
//   });
//
//   int accountsId;
//   String accountsName;
//   String accountsSlug;
//   String accountsImgDashboard;
//   String accountsImgRtl;
//   String accountsImgLtr;
//   String accountsImgHeader;
//   dynamic accountsImgApp;
//   String accountsShowBranches;
//   String accountsShowAreas;
//   String accountsShowCoupons;
//   String accountsSingleMenu;
//   String accountsCountry;
//   String accountsCountryCode;
//   String accountsMultipleCountries;
//   int accountsCountryMobileDigitsCount;
//   String accountsTimezone;
//   String accountsCountryLatLong;
//   String accountsShowCalories;
//   String accountsShowProductType;
//   String accountsFcmKey;
//   String accountsFcmApiKey;
//   String accountsFcmAuthDomain;
//   String accountsFcmDatabaseUrl;
//   String accountsFcmProjectId;
//   String accountsFcmStorageBucket;
//   String accountsFcmMessagingSenderId;
//   String accountsFcmAppId;
//   dynamic accountsFcmMeasurementId;
//   dynamic accountsAndroidUrl;
//   dynamic accountsIosUrl;
//   String accountsProductUrl;
//   int accountsNumberDecimalDigits;
//   String accountsStatus;
//   int accountsPosition;
//   dynamic accountsWebServiceToken;
//   DateTime accountsCreatedAt;
//   DateTime accountsUpdatedAt;
//   Colors colors;
//   String productsName;
//   List<Lang> langs;
//   List<AccountTranslation> translations;
//
//   factory Account.fromJson(Map<String, dynamic> json) => Account(
//     accountsId: json["accounts_id"],
//     accountsName: json["accounts_name"],
//     accountsSlug: json["accounts_slug"],
//     accountsImgDashboard: json["accounts_img_dashboard"],
//     accountsImgRtl: json["accounts_img_rtl"],
//     accountsImgLtr: json["accounts_img_ltr"],
//     accountsImgHeader: json["accounts_img_header"],
//     accountsImgApp: json["accounts_img_app"],
//     accountsShowBranches: json["accounts_show_branches"],
//     accountsShowAreas: json["accounts_show_areas"],
//     accountsShowCoupons: json["accounts_show_coupons"],
//     accountsSingleMenu: json["accounts_single_menu"],
//     accountsCountry: json["accounts_country"],
//     accountsCountryCode: json["accounts_country_code"],
//     accountsMultipleCountries: json["accounts_multiple_countries"],
//     accountsCountryMobileDigitsCount: json["accounts_country_mobile_digits_count"],
//     accountsTimezone: json["accounts_timezone"],
//     accountsCountryLatLong: json["accounts_country_lat_long"],
//     accountsShowCalories: json["accounts_show_calories"],
//     accountsShowProductType: json["accounts_show_product_type"],
//     accountsFcmKey: json["accounts_fcm_key"],
//     accountsFcmApiKey: json["accounts_fcm_apiKey"],
//     accountsFcmAuthDomain: json["accounts_fcm_authDomain"],
//     accountsFcmDatabaseUrl: json["accounts_fcm_databaseURL"],
//     accountsFcmProjectId: json["accounts_fcm_projectId"],
//     accountsFcmStorageBucket: json["accounts_fcm_storageBucket"],
//     accountsFcmMessagingSenderId: json["accounts_fcm_messagingSenderId"],
//     accountsFcmAppId: json["accounts_fcm_appId"],
//     accountsFcmMeasurementId: json["accounts_fcm_measurementId"],
//     accountsAndroidUrl: json["accounts_android_url"],
//     accountsIosUrl: json["accounts_ios_url"],
//     accountsProductUrl: json["accounts_product_url"],
//     accountsNumberDecimalDigits: json["accounts_number_decimal_digits"],
//     accountsStatus: json["accounts_status"],
//     accountsPosition: json["accounts_position"],
//     accountsWebServiceToken: json["accounts_web_service_token"],
//     accountsCreatedAt: DateTime.parse(json["accounts_created_at"]),
//     accountsUpdatedAt: DateTime.parse(json["accounts_updated_at"]),
//     colors: json["colors"] == null ? null : Colors.fromJson(json["colors"]),
//     productsName: json["products_name"],
//     langs: json["langs"] == null ? null : List<Lang>.from(json["langs"].map((x) => Lang.fromJson(x))),
//     translations: List<AccountTranslation>.from(json["translations"].map((x) => AccountTranslation.fromJson(x))),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "accounts_id": accountsId,
//     "accounts_name": accountsName,
//     "accounts_slug": accountsSlug,
//     "accounts_img_dashboard": accountsImgDashboard,
//     "accounts_img_rtl": accountsImgRtl,
//     "accounts_img_ltr": accountsImgLtr,
//     "accounts_img_header": accountsImgHeader,
//     "accounts_img_app": accountsImgApp,
//     "accounts_show_branches": accountsShowBranches,
//     "accounts_show_areas": accountsShowAreas,
//     "accounts_show_coupons": accountsShowCoupons,
//     "accounts_single_menu": accountsSingleMenu,
//     "accounts_country": accountsCountry,
//     "accounts_country_code": accountsCountryCode,
//     "accounts_multiple_countries": accountsMultipleCountries,
//     "accounts_country_mobile_digits_count": accountsCountryMobileDigitsCount,
//     "accounts_timezone": accountsTimezone,
//     "accounts_country_lat_long": accountsCountryLatLong,
//     "accounts_show_calories": accountsShowCalories,
//     "accounts_show_product_type": accountsShowProductType,
//     "accounts_fcm_key": accountsFcmKey,
//     "accounts_fcm_apiKey": accountsFcmApiKey,
//     "accounts_fcm_authDomain": accountsFcmAuthDomain,
//     "accounts_fcm_databaseURL": accountsFcmDatabaseUrl,
//     "accounts_fcm_projectId": accountsFcmProjectId,
//     "accounts_fcm_storageBucket": accountsFcmStorageBucket,
//     "accounts_fcm_messagingSenderId": accountsFcmMessagingSenderId,
//     "accounts_fcm_appId": accountsFcmAppId,
//     "accounts_fcm_measurementId": accountsFcmMeasurementId,
//     "accounts_android_url": accountsAndroidUrl,
//     "accounts_ios_url": accountsIosUrl,
//     "accounts_product_url": accountsProductUrl,
//     "accounts_number_decimal_digits": accountsNumberDecimalDigits,
//     "accounts_status": accountsStatus,
//     "accounts_position": accountsPosition,
//     "accounts_web_service_token": accountsWebServiceToken,
//     "accounts_created_at": accountsCreatedAt.toIso8601String(),
//     "accounts_updated_at": accountsUpdatedAt.toIso8601String(),
//     "colors": colors == null ? null : colors.toJson(),
//     "products_name": productsName,
//     "langs": langs == null ? null : List<dynamic>.from(langs.map((x) => x.toJson())),
//     "translations": List<dynamic>.from(translations.map((x) => x.toJson())),
//   };
// }
//
// class Colors {
//   Colors({
//     this.mainColor,
//     this.appbarBgColor,
//     this.searchBgColor,
//     this.bottomNavigationBarBgColor,
//     this.bgColor,
//     this.textHeaderColor,
//     this.textdetailsColor,
//     this.borderColor,
//     this.navigBarBorderColor,
//     this.textOnMainColor,
//     this.color10,
//     this.productsName,
//     this.translations,
//   });
//
//   String mainColor;
//   String appbarBgColor;
//   String searchBgColor;
//   String bottomNavigationBarBgColor;
//   String bgColor;
//   String textHeaderColor;
//   String textdetailsColor;
//   String borderColor;
//   String navigBarBorderColor;
//   String textOnMainColor;
//   String color10;
//   dynamic productsName;
//   List<dynamic> translations;
//
//   factory Colors.fromJson(Map<String, dynamic> json) => Colors(
//     mainColor: json["MainColor"],
//     appbarBgColor: json["AppbarBGColor"],
//     searchBgColor: json["SearchBGColor"],
//     bottomNavigationBarBgColor: json["bottomNavigationBarBGColor"],
//     bgColor: json["BGColor"],
//     textHeaderColor: json["TextHeaderColor"],
//     textdetailsColor: json["TextdetailsColor"],
//     borderColor: json["BorderColor"],
//     navigBarBorderColor: json["NavigBarBorderColor"],
//     textOnMainColor: json["TextOnMainColor"],
//     color10: json["color10"],
//     productsName: json["products_name"],
//     translations: List<dynamic>.from(json["translations"].map((x) => x)),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "MainColor": mainColor,
//     "AppbarBGColor": appbarBgColor,
//     "SearchBGColor": searchBgColor,
//     "bottomNavigationBarBGColor": bottomNavigationBarBgColor,
//     "BGColor": bgColor,
//     "TextHeaderColor": textHeaderColor,
//     "TextdetailsColor": textdetailsColor,
//     "BorderColor": borderColor,
//     "NavigBarBorderColor": navigBarBorderColor,
//     "TextOnMainColor": textOnMainColor,
//     "color10": color10,
//     "products_name": productsName,
//     "translations": List<dynamic>.from(translations.map((x) => x)),
//   };
// }
//
// class Lang {
//   Lang({
//     this.languagesId,
//     this.name,
//     this.locale,
//     this.dir,
//     this.status,
//     this.position,
//     this.pivot,
//   });
//
//   int languagesId;
//   String name;
//   String locale;
//   String dir;
//   int status;
//   int position;
//   Pivot pivot;
//
//   factory Lang.fromJson(Map<String, dynamic> json) => Lang(
//     languagesId: json["languages_id"],
//     name: json["name"],
//     locale: json["locale"],
//     dir: json["dir"],
//     status: json["status"],
//     position: json["position"],
//     pivot: Pivot.fromJson(json["pivot"]),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "languages_id": languagesId,
//     "name": name,
//     "locale": locale,
//     "dir": dir,
//     "status": status,
//     "position": position,
//     "pivot": pivot.toJson(),
//   };
// }
//
// class Pivot {
//   Pivot({
//     this.fkAccountsId,
//     this.fkLanguagesId,
//   });
//
//   int fkAccountsId;
//   int fkLanguagesId;
//
//   factory Pivot.fromJson(Map<String, dynamic> json) => Pivot(
//     fkAccountsId: json["FK_accounts_id"],
//     fkLanguagesId: json["FK_languages_id"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "FK_accounts_id": fkAccountsId,
//     "FK_languages_id": fkLanguagesId,
//   };
// }
//
// class AccountTranslation {
//   AccountTranslation({
//     this.accountsTransId,
//     this.accountsId,
//     this.locale,
//     this.productsName,
//   });
//
//   int accountsTransId;
//   int accountsId;
//   String locale;
//   String productsName;
//
//   factory AccountTranslation.fromJson(Map<String, dynamic> json) => AccountTranslation(
//     accountsTransId: json["accounts_trans_id"],
//     accountsId: json["accounts_id"],
//     locale: json["locale"],
//     productsName: json["products_name"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "accounts_trans_id": accountsTransId,
//     "accounts_id": accountsId,
//     "locale": locale,
//     "products_name": productsName,
//   };
// }
//
// class Info {
//   Info({
//     this.infosId,
//     this.fkAccountsId,
//     this.infosPhone1,
//     this.infosPhone2,
//     this.infosPhone3,
//     this.infosGps,
//     this.infosAdvantages,
//     this.infosFacebook,
//     this.infosTwitter,
//     this.infosYoutube,
//     this.infosInstagram,
//     this.infosSnapchat,
//     this.infosWhatsapp,
//     this.infosFoursquare,
//     this.infosCreatedAt,
//     this.infosUpdatedAt,
//     this.infosAbout,
//     this.infosPrivacyPolicy,
//     this.trans,
//     this.translations,
//   });
//
//   int infosId;
//   int fkAccountsId;
//   String infosPhone1;
//   String infosPhone2;
//   String infosPhone3;
//   String infosGps;
//   dynamic infosAdvantages;
//   String infosFacebook;
//   dynamic infosTwitter;
//   dynamic infosYoutube;
//   dynamic infosInstagram;
//   dynamic infosSnapchat;
//   String infosWhatsapp;
//   dynamic infosFoursquare;
//   DateTime infosCreatedAt;
//   DateTime infosUpdatedAt;
//   String infosAbout;
//   String infosPrivacyPolicy;
//   List<InfoTran> trans;
//   List<InfoTran> translations;
//
//   factory Info.fromJson(Map<String, dynamic> json) => Info(
//     infosId: json["infos_id"],
//     fkAccountsId: json["FK_accounts_id"],
//     infosPhone1: json["infos_phone1"],
//     infosPhone2: json["infos_phone2"],
//     infosPhone3: json["infos_phone3"],
//     infosGps: json["infos_gps"],
//     infosAdvantages: json["infos_advantages"],
//     infosFacebook: json["infos_facebook"],
//     infosTwitter: json["infos_twitter"],
//     infosYoutube: json["infos_youtube"],
//     infosInstagram: json["infos_instagram"],
//     infosSnapchat: json["infos_snapchat"],
//     infosWhatsapp: json["infos_whatsapp"],
//     infosFoursquare: json["infos_foursquare"],
//     infosCreatedAt: DateTime.parse(json["infos_created_at"]),
//     infosUpdatedAt: DateTime.parse(json["infos_updated_at"]),
//     infosAbout: json["infos_about"],
//     infosPrivacyPolicy: json["infos_privacy_policy"],
//     trans: List<InfoTran>.from(json["trans"].map((x) => InfoTran.fromJson(x))),
//     translations: List<InfoTran>.from(json["translations"].map((x) => InfoTran.fromJson(x))),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "infos_id": infosId,
//     "FK_accounts_id": fkAccountsId,
//     "infos_phone1": infosPhone1,
//     "infos_phone2": infosPhone2,
//     "infos_phone3": infosPhone3,
//     "infos_gps": infosGps,
//     "infos_advantages": infosAdvantages,
//     "infos_facebook": infosFacebook,
//     "infos_twitter": infosTwitter,
//     "infos_youtube": infosYoutube,
//     "infos_instagram": infosInstagram,
//     "infos_snapchat": infosSnapchat,
//     "infos_whatsapp": infosWhatsapp,
//     "infos_foursquare": infosFoursquare,
//     "infos_created_at": infosCreatedAt.toIso8601String(),
//     "infos_updated_at": infosUpdatedAt.toIso8601String(),
//     "infos_about": infosAbout,
//     "infos_privacy_policy": infosPrivacyPolicy,
//     "trans": List<dynamic>.from(trans.map((x) => x.toJson())),
//     "translations": List<dynamic>.from(translations.map((x) => x.toJson())),
//   };
// }
//
// class InfoTran {
//   InfoTran({
//     this.infosTransId,
//     this.infosId,
//     this.locale,
//     this.infosAbout,
//     this.infosPrivacyPolicy,
//   });
//
//   int infosTransId;
//   int infosId;
//   String locale;
//   String infosAbout;
//   String infosPrivacyPolicy;
//
//   factory InfoTran.fromJson(Map<String, dynamic> json) => InfoTran(
//     infosTransId: json["infos_trans_id"],
//     infosId: json["infos_id"],
//     locale: json["locale"],
//     infosAbout: json["infos_about"],
//     infosPrivacyPolicy: json["infos_privacy_policy"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "infos_trans_id": infosTransId,
//     "infos_id": infosId,
//     "locale": locale,
//     "infos_about": infosAbout,
//     "infos_privacy_policy": infosPrivacyPolicy,
//   };
// }
//
// class PaymentMethod {
//   PaymentMethod({
//     this.paymentsId,
//     this.paymentsSlug,
//     this.paymentsName,
//     this.translations,
//   });
//
//   int paymentsId;
//   String paymentsSlug;
//   String paymentsName;
//   List<PaymentMethodTranslation> translations;
//
//   factory PaymentMethod.fromJson(Map<String, dynamic> json) => PaymentMethod(
//     paymentsId: json["payments_id"],
//     paymentsSlug: json["payments_slug"],
//     paymentsName: json["payments_name"],
//     translations: List<PaymentMethodTranslation>.from(json["translations"].map((x) => PaymentMethodTranslation.fromJson(x))),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "payments_id": paymentsId,
//     "payments_slug": paymentsSlug,
//     "payments_name": paymentsName,
//     "translations": List<dynamic>.from(translations.map((x) => x.toJson())),
//   };
// }
//
// class PaymentMethodTranslation {
//   PaymentMethodTranslation({
//     this.paymentsTransId,
//     this.paymentsId,
//     this.locale,
//     this.paymentsName,
//   });
//
//   int paymentsTransId;
//   int paymentsId;
//   String locale;
//   String paymentsName;
//
//   factory PaymentMethodTranslation.fromJson(Map<String, dynamic> json) => PaymentMethodTranslation(
//     paymentsTransId: json["payments_trans_id"],
//     paymentsId: json["payments_id"],
//     locale: json["locale"],
//     paymentsName: json["payments_name"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "payments_trans_id": paymentsTransId,
//     "payments_id": paymentsId,
//     "locale": locale,
//     "payments_name": paymentsName,
//   };
// }
//
// class Setting {
//   Setting({
//     this.settingsId,
//     this.fkAccountsId,
//     this.settingsDefaultLanguage,
//     this.settingsPaginationCount,
//     this.settingsPaginationCountDashboard,
//     this.settingsDeliveryTime,
//     this.settingsDeliveryCost,
//     this.settingsMinOrder,
//     this.settingsCreatedAt,
//     this.settingsUpdatedAt,
//     this.settingsDeliveryCostNew,
//     this.settingsCurrency,
//     this.trans,
//     this.account,
//     this.translations,
//   });
//
//   int settingsId;
//   int fkAccountsId;
//   String settingsDefaultLanguage;
//   int settingsPaginationCount;
//   int settingsPaginationCountDashboard;
//   int settingsDeliveryTime;
//   int settingsDeliveryCost;
//   int settingsMinOrder;
//   DateTime settingsCreatedAt;
//   DateTime settingsUpdatedAt;
//   String settingsDeliveryCostNew;
//   String settingsCurrency;
//   List<SettingTran> trans;
//   Account account;
//   List<SettingTran> translations;
//
//   factory Setting.fromJson(Map<String, dynamic> json) => Setting(
//     settingsId: json["settings_id"],
//     fkAccountsId: json["FK_accounts_id"],
//     settingsDefaultLanguage: json["settings_default_language"],
//     settingsPaginationCount: json["settings_pagination_count"],
//     settingsPaginationCountDashboard: json["settings_pagination_count_dashboard"],
//     settingsDeliveryTime: json["settings_delivery_time"],
//     settingsDeliveryCost: json["settings_delivery_cost"],
//     settingsMinOrder: json["settings_min_order"],
//     settingsCreatedAt: DateTime.parse(json["settings_created_at"]),
//     settingsUpdatedAt: DateTime.parse(json["settings_updated_at"]),
//     settingsDeliveryCostNew: json["settings_delivery_cost_new"],
//     settingsCurrency: json["settings_currency"],
//     trans: List<SettingTran>.from(json["trans"].map((x) => SettingTran.fromJson(x))),
//     account: Account.fromJson(json["account"]),
//     translations: List<SettingTran>.from(json["translations"].map((x) => SettingTran.fromJson(x))),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "settings_id": settingsId,
//     "FK_accounts_id": fkAccountsId,
//     "settings_default_language": settingsDefaultLanguage,
//     "settings_pagination_count": settingsPaginationCount,
//     "settings_pagination_count_dashboard": settingsPaginationCountDashboard,
//     "settings_delivery_time": settingsDeliveryTime,
//     "settings_delivery_cost": settingsDeliveryCost,
//     "settings_min_order": settingsMinOrder,
//     "settings_created_at": settingsCreatedAt.toIso8601String(),
//     "settings_updated_at": settingsUpdatedAt.toIso8601String(),
//     "settings_delivery_cost_new": settingsDeliveryCostNew,
//     "settings_currency": settingsCurrency,
//     "trans": List<dynamic>.from(trans.map((x) => x.toJson())),
//     "account": account.toJson(),
//     "translations": List<dynamic>.from(translations.map((x) => x.toJson())),
//   };
// }
//
// class SettingTran {
//   SettingTran({
//     this.settingsTransId,
//     this.settingsId,
//     this.locale,
//     this.settingsCurrency,
//   });
//
//   int settingsTransId;
//   int settingsId;
//   String locale;
//   String settingsCurrency;
//
//   factory SettingTran.fromJson(Map<String, dynamic> json) => SettingTran(
//     settingsTransId: json["settings_trans_id"],
//     settingsId: json["settings_id"],
//     locale: json["locale"],
//     settingsCurrency: json["settings_currency"],
//   );
//
//   Map<String, dynamic> toJson() => {
//     "settings_trans_id": settingsTransId,
//     "settings_id": settingsId,
//     "locale": locale,
//     "settings_currency": settingsCurrency,
//   };
// }
