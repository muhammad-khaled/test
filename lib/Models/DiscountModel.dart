// To parse this JSON data, do
//
//     final discountDetails = discountDetailsFromJson(jsonString);

import 'dart:convert';

import 'HomeModel/sharedModel.dart';

DiscountDetailsModel discountDetailsFromJson(String str) => DiscountDetailsModel.fromJson(json.decode(str));

String discountDetailsToJson(DiscountDetailsModel data) => json.encode(data.toJson());

class DiscountDetailsModel {
  DiscountDetailsModel({
    this.status,
    this.message,
    this.errors,
    this.data,
  });

  int status;
  String message;
  dynamic errors;
  Data data;

  factory DiscountDetailsModel.fromJson(Map<String, dynamic> json) => DiscountDetailsModel(
    status: json["status"],
    message: json["message"],
    errors: json["errors"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "errors": errors,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    this.products,
    this.pagination,
    this.min,
    this.max,
  });

  List<ProductShared> products;
  Pagination pagination;
  String min;
  String max;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    products: List<ProductShared>.from(json["products"].map((x) => ProductShared.fromJson(x))),
    pagination: Pagination.fromJson(json["pagination"]),
    min: json["min"],
    max: json["max"],
  );

  Map<String, dynamic> toJson() => {
    "products": List<dynamic>.from(products.map((x) => x.toJson())),
    "pagination": pagination.toJson(),
    "min": min,
    "max": max,
  };
}









