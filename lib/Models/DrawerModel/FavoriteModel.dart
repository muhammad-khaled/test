// To parse this JSON data, do
//
//     final favoriteModel = favoriteModelFromJson(jsonString);

import 'dart:convert';

import '../../Models/HomeModel/sharedModel.dart';

FavoriteModel favoriteModelFromJson(String str) => FavoriteModel.fromJson(json.decode(str));

String favoriteModelToJson(FavoriteModel data) => json.encode(data.toJson());

class FavoriteModel {
  FavoriteModel({
    this.status,
    this.message,
    this.errors,
    this.data,
  });

  int status;
  String message;
  dynamic errors;
  Data data;

  factory FavoriteModel.fromJson(Map<String, dynamic> json) => FavoriteModel(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    errors: json["errors"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "errors": errors,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.favs,
  });

  List<ProductShared> favs;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    favs: json["favs"] == null ? null : List<ProductShared>.from(json["favs"].map((x) => ProductShared.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "favs": favs == null ? null : List<dynamic>.from(favs.map((x) => x.toJson())),
  };
}





class Category {
  Category({
    this.categoriesId,
    this.categoriesCode,
    this.fkAccountsId,
    this.categoriesParentId,
    this.categoriesImg,
    this.categoriesFollowDeliveryTime,
    this.categoriesDiscountRate,
    this.categoriesStatus,
    this.categoriesPosition,
    this.categoriesCreatedAt,
    this.categoriesUpdatedAt,
    this.lft,
    this.rgt,
    this.depth,
    this.categoriesTitle,
    this.categoriesDesc,
    this.translations,
  });

  int categoriesId;
  String categoriesCode;
  int fkAccountsId;
  dynamic categoriesParentId;
  String categoriesImg;
  String categoriesFollowDeliveryTime;
  dynamic categoriesDiscountRate;
  String categoriesStatus;
  int categoriesPosition;
  DateTime categoriesCreatedAt;
  DateTime categoriesUpdatedAt;
  int lft;
  int rgt;
  int depth;
  String categoriesTitle;
  dynamic categoriesDesc;
  List<CategoryTranslation> translations;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    categoriesId: json["categories_id"] == null ? null : json["categories_id"],
    categoriesCode: json["categories_code"] == null ? null : json["categories_code"],
    fkAccountsId: json["FK_accounts_id"] == null ? null : json["FK_accounts_id"],
    categoriesParentId: json["categories_parent_id"],
    categoriesImg: json["categories_img"] == null ? null : json["categories_img"],
    categoriesFollowDeliveryTime: json["categories_follow_delivery_time"] == null ? null : json["categories_follow_delivery_time"],
    categoriesDiscountRate: json["categories_discount_rate"],
    categoriesStatus: json["categories_status"] == null ? null : json["categories_status"],
    categoriesPosition: json["categories_position"] == null ? null : json["categories_position"],
    categoriesCreatedAt: json["categories_created_at"] == null ? null : DateTime.parse(json["categories_created_at"]),
    categoriesUpdatedAt: json["categories_updated_at"] == null ? null : DateTime.parse(json["categories_updated_at"]),
    lft: json["lft"] == null ? null : json["lft"],
    rgt: json["rgt"] == null ? null : json["rgt"],
    depth: json["depth"] == null ? null : json["depth"],
    categoriesTitle: json["categories_title"] == null ? null : json["categories_title"],
    categoriesDesc: json["categories_desc"],
    translations: json["translations"] == null ? null : List<CategoryTranslation>.from(json["translations"].map((x) => CategoryTranslation.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "categories_id": categoriesId == null ? null : categoriesId,
    "categories_code": categoriesCode == null ? null : categoriesCode,
    "FK_accounts_id": fkAccountsId == null ? null : fkAccountsId,
    "categories_parent_id": categoriesParentId,
    "categories_img": categoriesImg == null ? null : categoriesImg,
    "categories_follow_delivery_time": categoriesFollowDeliveryTime == null ? null : categoriesFollowDeliveryTime,
    "categories_discount_rate": categoriesDiscountRate,
    "categories_status": categoriesStatus == null ? null : categoriesStatus,
    "categories_position": categoriesPosition == null ? null : categoriesPosition,
    "categories_created_at": categoriesCreatedAt == null ? null : categoriesCreatedAt.toIso8601String(),
    "categories_updated_at": categoriesUpdatedAt == null ? null : categoriesUpdatedAt.toIso8601String(),
    "lft": lft == null ? null : lft,
    "rgt": rgt == null ? null : rgt,
    "depth": depth == null ? null : depth,
    "categories_title": categoriesTitle == null ? null : categoriesTitle,
    "categories_desc": categoriesDesc,
    "translations": translations == null ? null : List<dynamic>.from(translations.map((x) => x.toJson())),
  };
}




class FavPivot {
  FavPivot({
    this.fkCustomersId,
    this.fkProductsId,
  });

  int fkCustomersId;
  int fkProductsId;

  factory FavPivot.fromJson(Map<String, dynamic> json) => FavPivot(
    fkCustomersId: json["FK_customers_id"] == null ? null : json["FK_customers_id"],
    fkProductsId: json["FK_products_id"] == null ? null : json["FK_products_id"],
  );

  Map<String, dynamic> toJson() => {
    "FK_customers_id": fkCustomersId == null ? null : fkCustomersId,
    "FK_products_id": fkProductsId == null ? null : fkProductsId,
  };
}

class FavTranslation {
  FavTranslation({
    this.productsTransId,
    this.productsId,
    this.locale,
    this.productsTitle,
    this.productsDesc,
  });

  int productsTransId;
  int productsId;
  String locale;
  String productsTitle;
  String productsDesc;

  factory FavTranslation.fromJson(Map<String, dynamic> json) => FavTranslation(
    productsTransId: json["products_trans_id"] == null ? null : json["products_trans_id"],
    productsId: json["products_id"] == null ? null : json["products_id"],
    locale: json["locale"] == null ? null : json["locale"],
    productsTitle: json["products_title"] == null ? null : json["products_title"],
    productsDesc: json["products_desc"] == null ? null : json["products_desc"],
  );

  Map<String, dynamic> toJson() => {
    "products_trans_id": productsTransId == null ? null : productsTransId,
    "products_id": productsId == null ? null : productsId,
    "locale": locale == null ? null : locale,
    "products_title": productsTitle == null ? null : productsTitle,
    "products_desc": productsDesc == null ? null : productsDesc,
  };
}
