// To parse this JSON data, do
//
//     final applicationHomeModel = applicationHomeModelFromJson(jsonString);

import 'dart:convert';

import '../../Models/HomeModel/sharedModel.dart';

ApplicationHomeModel applicationHomeModelFromJson(String str) => ApplicationHomeModel.fromJson(json.decode(str));

String applicationHomeModelToJson(ApplicationHomeModel data) => json.encode(data.toJson());

class ApplicationHomeModel {
  ApplicationHomeModel({
    this.status,
    this.message,
    this.errors,
    this.data,
  });

  int status;
  String message;
  dynamic errors;
  Data data;

  factory ApplicationHomeModel.fromJson(Map<String, dynamic> json) => ApplicationHomeModel(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    errors: json["errors"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "errors": errors,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.homeProducts,
    this.categories,
    this.discountProducts,
    this.bestSales,
    this.offers,
    this.advertisements,
  });

  List<ProductShared> homeProducts;
  List<SharedCategory> categories;
  List<ProductShared> discountProducts;
  List<ProductShared> bestSales;
  List<SharedOffer> offers;
  Advertisements advertisements;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    homeProducts: json["homeProducts"] == null ? null : List<ProductShared>.from(json["homeProducts"].map((x) => ProductShared.fromJson(x))),
    categories: json["categories"] == null ? null : List<SharedCategory>.from(json["categories"].map((x) => SharedCategory.fromJson(x))),
    discountProducts: json["discountProducts"] == null ? null : List<ProductShared>.from(json["discountProducts"].map((x) => ProductShared.fromJson(x))),
    bestSales: json["bestSales"] == null ? null : List<ProductShared>.from(json["bestSales"].map((x) => ProductShared.fromJson(x))),
    offers: json["offers"] == null ? null : List<SharedOffer>.from(json["offers"].map((x) => SharedOffer.fromJson(x))),
    advertisements: json["advertisements"] == null ? null : Advertisements.fromJson(json["advertisements"]),
  );

  Map<String, dynamic> toJson() => {
    "homeProducts": homeProducts == null ? null : List<dynamic>.from(homeProducts.map((x) => x.toJson())),
    "categories": categories == null ? null : List<dynamic>.from(categories.map((x) => x.toJson())),
    "discountProducts": discountProducts == null ? null : List<dynamic>.from(discountProducts.map((x) => x.toJson())),
    "bestSales": bestSales == null ? null : List<dynamic>.from(bestSales.map((x) => x.toJson())),
    "offers": offers == null ? null : List<dynamic>.from(offers.map((x) => x.toJson())),
    "advertisements": advertisements == null ? null : advertisements.toJson(),
  };
}

class Advertisements {
  Advertisements({
    this.startPage,
    this.middlePage,
    this.endPage,
    this.sectionalBanners,
  });

  List<EndPage> startPage;
  List<EndPage> middlePage;
  List<EndPage> endPage;
  List<EndPage> sectionalBanners;

  factory Advertisements.fromJson(Map<String, dynamic> json) => Advertisements(
    startPage: json["start_page"] == null ? null : List<EndPage>.from(json["start_page"].map((x) => EndPage.fromJson(x))),
    middlePage: json["middle_page"] == null ? null : List<EndPage>.from(json["middle_page"].map((x) => EndPage.fromJson(x))),
    endPage: json["end_page"] == null ? null : List<EndPage>.from(json["end_page"].map((x) => EndPage.fromJson(x))),
    sectionalBanners: json["sectional_banners"] == null ? null : List<EndPage>.from(json["sectional_banners"].map((x) => EndPage.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "start_page": startPage == null ? null : List<dynamic>.from(startPage.map((x) => x.toJson())),
    "middle_page": middlePage == null ? null : List<dynamic>.from(middlePage.map((x) => x.toJson())),
    "end_page": endPage == null ? null : List<dynamic>.from(endPage.map((x) => x.toJson())),
    "sectional_banners": sectionalBanners == null ? null : List<dynamic>.from(sectionalBanners.map((x) => x.toJson())),
  };
}

class EndPage {
  EndPage({
    this.advertisementsId,
    this.fkAccountsId,
    this.advertisementsName,
    this.advertisementsColor,
    this.advertisementsStatus,
    this.advertisementsViewPage,
    this.advertisementsFrom,
    this.advertisementsTo,
    this.advertisementsType,
    this.advertisementsValue,
    this.advertisementsPosition,
    this.advertisementsCreatedAt,
    this.advertisementsUpdatedAt,
    this.advertisementsText,
    this.advertisementsImg,
    this.advertisementsMobileImg,
    this.advertisementsUrl,
    this.translations,
  });

  int advertisementsId;
  int fkAccountsId;
  String advertisementsName;
  dynamic advertisementsColor;
  String advertisementsStatus;
  String advertisementsViewPage;
  DateTime advertisementsFrom;
  DateTime advertisementsTo;
  String advertisementsType;
  String advertisementsValue;
  int advertisementsPosition;
  DateTime advertisementsCreatedAt;
  DateTime advertisementsUpdatedAt;
  dynamic advertisementsText;
  String advertisementsImg;
  String advertisementsMobileImg;
  dynamic advertisementsUrl;
  List<EndPageTranslation> translations;

  factory EndPage.fromJson(Map<String, dynamic> json) => EndPage(
    advertisementsId: json["advertisements_id"] == null ? null : json["advertisements_id"],
    fkAccountsId: json["FK_accounts_id"] == null ? null : json["FK_accounts_id"],
    advertisementsName: json["advertisements_name"] == null ? null : json["advertisements_name"],
    advertisementsColor: json["advertisements_color"],
    advertisementsStatus: json["advertisements_status"] == null ? null : json["advertisements_status"],
    advertisementsViewPage: json["advertisements_view_page"] == null ? null : json["advertisements_view_page"],
    advertisementsFrom: json["advertisements_from"] == null ? null : DateTime.parse(json["advertisements_from"]),
    advertisementsTo: json["advertisements_to"] == null ? null : DateTime.parse(json["advertisements_to"]),
    advertisementsType: json["advertisements_type"] == null ? null : json["advertisements_type"],
    advertisementsValue: json["advertisements_value"] == null ? null : json["advertisements_value"],
    advertisementsPosition: json["advertisements_position"] == null ? null : json["advertisements_position"],
    advertisementsCreatedAt: json["advertisements_created_at"] == null ? null : DateTime.parse(json["advertisements_created_at"]),
    advertisementsUpdatedAt: json["advertisements_updated_at"] == null ? null : DateTime.parse(json["advertisements_updated_at"]),
    advertisementsText: json["advertisements_text"],
    advertisementsImg: json["advertisements_img"] == null ? null : json["advertisements_img"],
    advertisementsMobileImg: json["advertisements_mobile_img"] == null ? null : json["advertisements_mobile_img"],
    advertisementsUrl: json["advertisements_url"],
    translations: json["translations"] == null ? null : List<EndPageTranslation>.from(json["translations"].map((x) => EndPageTranslation.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "advertisements_id": advertisementsId == null ? null : advertisementsId,
    "FK_accounts_id": fkAccountsId == null ? null : fkAccountsId,
    "advertisements_name": advertisementsName == null ? null : advertisementsName,
    "advertisements_color": advertisementsColor,
    "advertisements_status": advertisementsStatus == null ? null : advertisementsStatus,
    "advertisements_view_page": advertisementsViewPage == null ? null : advertisementsViewPage,
    "advertisements_from": advertisementsFrom == null ? null : "${advertisementsFrom.year.toString().padLeft(4, '0')}-${advertisementsFrom.month.toString().padLeft(2, '0')}-${advertisementsFrom.day.toString().padLeft(2, '0')}",
    "advertisements_to": advertisementsTo == null ? null : "${advertisementsTo.year.toString().padLeft(4, '0')}-${advertisementsTo.month.toString().padLeft(2, '0')}-${advertisementsTo.day.toString().padLeft(2, '0')}",
    "advertisements_type": advertisementsType == null ? null : advertisementsType,
    "advertisements_value": advertisementsValue == null ? null : advertisementsValue,
    "advertisements_position": advertisementsPosition == null ? null : advertisementsPosition,
    "advertisements_created_at": advertisementsCreatedAt == null ? null : advertisementsCreatedAt.toIso8601String(),
    "advertisements_updated_at": advertisementsUpdatedAt == null ? null : advertisementsUpdatedAt.toIso8601String(),
    "advertisements_text": advertisementsText,
    "advertisements_img": advertisementsImg == null ? null : advertisementsImg,
    "advertisements_mobile_img": advertisementsMobileImg == null ? null : advertisementsMobileImg,
    "advertisements_url": advertisementsUrl,
    "translations": translations == null ? null : List<dynamic>.from(translations.map((x) => x.toJson())),
  };
}

class EndPageTranslation {
  EndPageTranslation({
    this.advertisementsTransId,
    this.advertisementsId,
    this.locale,
    this.advertisementsImg,
    this.advertisementsMobileImg,
    this.advertisementsText,
    this.advertisementsUrl,
  });

  int advertisementsTransId;
  int advertisementsId;
  Locale locale;
  String advertisementsImg;
  String advertisementsMobileImg;
  dynamic advertisementsText;
  dynamic advertisementsUrl;

  factory EndPageTranslation.fromJson(Map<String, dynamic> json) => EndPageTranslation(
    advertisementsTransId: json["advertisements_trans_id"] == null ? null : json["advertisements_trans_id"],
    advertisementsId: json["advertisements_id"] == null ? null : json["advertisements_id"],
    locale: json["locale"] == null ? null : localeValues.map[json["locale"]],
    advertisementsImg: json["advertisements_img"] == null ? null : json["advertisements_img"],
    advertisementsMobileImg: json["advertisements_mobile_img"] == null ? null : json["advertisements_mobile_img"],
    advertisementsText: json["advertisements_text"],
    advertisementsUrl: json["advertisements_url"],
  );

  Map<String, dynamic> toJson() => {
    "advertisements_trans_id": advertisementsTransId == null ? null : advertisementsTransId,
    "advertisements_id": advertisementsId == null ? null : advertisementsId,
    "locale": locale == null ? null : localeValues.reverse[locale],
    "advertisements_img": advertisementsImg == null ? null : advertisementsImg,
    "advertisements_mobile_img": advertisementsMobileImg == null ? null : advertisementsMobileImg,
    "advertisements_text": advertisementsText,
    "advertisements_url": advertisementsUrl,
  };
}

class SuggestionTranslation {
  SuggestionTranslation({
    this.productsTransId,
    this.productsId,
    this.locale,
    this.productsTitle,
    this.productsDesc,
  });

  int productsTransId;
  int productsId;
  Locale locale;
  String productsTitle;
  String productsDesc;

  factory SuggestionTranslation.fromJson(Map<String, dynamic> json) => SuggestionTranslation(
    productsTransId: json["products_trans_id"] == null ? null : json["products_trans_id"],
    productsId: json["products_id"] == null ? null : json["products_id"],
    locale: json["locale"] == null ? null : localeValues.map[json["locale"]],
    productsTitle: json["products_title"] == null ? null : json["products_title"],
    productsDesc: json["products_desc"] == null ? null : json["products_desc"],
  );

  Map<String, dynamic> toJson() => {
    "products_trans_id": productsTransId == null ? null : productsTransId,
    "products_id": productsId == null ? null : productsId,
    "locale": locale == null ? null : localeValues.reverse[locale],
    "products_title": productsTitle == null ? null : productsTitle,
    "products_desc": productsDesc == null ? null : productsDesc,
  };
}



class Suggestion {
  Suggestion({
    this.productsId,
    this.fkAccountsId,
    this.fkCategoriesId,
    this.productsCode,
    this.productsImg,
    this.productsPrice,
    this.productsPriceAfterSale,
    this.productsCalories,
    this.productsVideo,
    this.productsType,
    this.productsLowestWeightAvailable,
    this.productsHome,
    this.productsAvailable,
    this.productsStatus,
    this.productsPosition,
    this.productsCreatedAt,
    this.productsUpdatedAt,
    this.isFav,
    this.categorySeries,
    this.productsTitle,
    this.productsDesc,
    this.pivot,
    this.choices,
    this.images,
    this.suggestions,
    this.category,
    this.account,
    this.translations,
  });

  int productsId;
  int fkAccountsId;
  int fkCategoriesId;
  String productsCode;
  String productsImg;
  String productsPrice;
  String productsPriceAfterSale;
  bool productsCalories;
  dynamic productsVideo;
  String productsType;
  dynamic productsLowestWeightAvailable;
  String productsHome;
  String productsAvailable;
  String productsStatus;
  int productsPosition;
  DateTime productsCreatedAt;
  DateTime productsUpdatedAt;
  int isFav;
  List<SharedCategory> categorySeries;
  String productsTitle;
  String productsDesc;
  SuggestionPivot pivot;
  List<Choice> choices;
  List<dynamic> images;
  List<Suggestion> suggestions;
  SharedCategory category;
  Account account;
  List<SuggestionTranslation> translations;

  factory Suggestion.fromJson(Map<String, dynamic> json) => Suggestion(
    productsId: json["products_id"] == null ? null : json["products_id"],
    fkAccountsId: json["FK_accounts_id"] == null ? null : json["FK_accounts_id"],
    fkCategoriesId: json["FK_categories_id"] == null ? null : json["FK_categories_id"],
    productsCode: json["products_code"] == null ? null : json["products_code"],
    productsImg: json["products_img"] == null ? null : json["products_img"],
    productsPrice: json["products_price"] == null ? null : json["products_price"],
    productsPriceAfterSale: json["products_price_after_sale"] == null ? null : json["products_price_after_sale"],
    productsCalories: json["products_calories"] == null ? null : json["products_calories"],
    productsVideo: json["products_video"],
    productsType: json["products_type"] == null ? null : json["products_type"],
    productsLowestWeightAvailable: json["products_lowest_weight_available"],
    productsHome: json["products_home"] == null ? null : json["products_home"],
    productsAvailable: json["products_available"] == null ? null : json["products_available"],
    productsStatus: json["products_status"] == null ? null : json["products_status"],
    productsPosition: json["products_position"] == null ? null : json["products_position"],
    productsCreatedAt: json["products_created_at"] == null ? null : DateTime.parse(json["products_created_at"]),
    productsUpdatedAt: json["products_updated_at"] == null ? null : DateTime.parse(json["products_updated_at"]),
    isFav: json["is_fav"] == null ? null : json["is_fav"],
    categorySeries: json["category_series"] == null ? null : List<SharedCategory>.from(json["category_series"].map((x) => SharedCategory.fromJson(x))),
    productsTitle: json["products_title"] == null ? null : json["products_title"],
    productsDesc: json["products_desc"] == null ? null : json["products_desc"],
    pivot: json["pivot"] == null ? null : SuggestionPivot.fromJson(json["pivot"]),
    choices: json["choices"] == null ? null : List<Choice>.from(json["choices"].map((x) => Choice.fromJson(x))),
    images: json["images"] == null ? null : List<dynamic>.from(json["images"].map((x) => x)),
    suggestions: json["suggestions"] == null ? null : List<Suggestion>.from(json["suggestions"].map((x) => Suggestion.fromJson(x))),
    category: json["category"] == null ? null : SharedCategory.fromJson(json["category"]),
    account: json["account"] == null ? null : Account.fromJson(json["account"]),
    translations: json["translations"] == null ? null : List<SuggestionTranslation>.from(json["translations"].map((x) => SuggestionTranslation.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "products_id": productsId == null ? null : productsId,
    "FK_accounts_id": fkAccountsId == null ? null : fkAccountsId,
    "FK_categories_id": fkCategoriesId == null ? null : fkCategoriesId,
    "products_code": productsCode == null ? null : productsCode,
    "products_img": productsImg == null ? null : productsImg,
    "products_price": productsPrice == null ? null : productsPrice,
    "products_price_after_sale": productsPriceAfterSale == null ? null : productsPriceAfterSale,
    "products_calories": productsCalories == null ? null : productsCalories,
    "products_video": productsVideo,
    "products_type": productsType == null ? null : productsType,
    "products_lowest_weight_available": productsLowestWeightAvailable,
    "products_home": productsHome == null ? null : productsHome,
    "products_available": productsAvailable == null ? null : productsAvailable,
    "products_status": productsStatus == null ? null : productsStatus,
    "products_position": productsPosition == null ? null : productsPosition,
    "products_created_at": productsCreatedAt == null ? null : productsCreatedAt.toIso8601String(),
    "products_updated_at": productsUpdatedAt == null ? null : productsUpdatedAt.toIso8601String(),
    "is_fav": isFav == null ? null : isFav,
    "category_series": categorySeries == null ? null : List<dynamic>.from(categorySeries.map((x) => x.toJson())),
    "products_title": productsTitle == null ? null : productsTitle,
    "products_desc": productsDesc == null ? null : productsDesc,
    "pivot": pivot == null ? null : pivot.toJson(),
    "choices": choices == null ? null : List<dynamic>.from(choices.map((x) => x.toJson())),
    "images": images == null ? null : List<dynamic>.from(images.map((x) => x)),
    "suggestions": suggestions == null ? null : List<dynamic>.from(suggestions.map((x) => x.toJson())),
    "category": category == null ? null : category.toJson(),
    "account": account == null ? null : account.toJson(),
    "translations": translations == null ? null : List<dynamic>.from(translations.map((x) => x.toJson())),
  };
}

class SuggestionPivot {
  SuggestionPivot({
    this.fkProductsId,
    this.fkSuggestedProductId,
  });

  int fkProductsId;
  int fkSuggestedProductId;

  factory SuggestionPivot.fromJson(Map<String, dynamic> json) => SuggestionPivot(
    fkProductsId: json["FK_products_id"] == null ? null : json["FK_products_id"],
    fkSuggestedProductId: json["FK_suggested_product_id"] == null ? null : json["FK_suggested_product_id"],
  );

  Map<String, dynamic> toJson() => {
    "FK_products_id": fkProductsId == null ? null : fkProductsId,
    "FK_suggested_product_id": fkSuggestedProductId == null ? null : fkSuggestedProductId,
  };
}






