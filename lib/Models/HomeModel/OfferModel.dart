


// To parse this JSON data, do
//
//     final offerModelModel = offerModelModelFromJson(jsonString);

import 'dart:convert';

import '../../Models/HomeModel/sharedModel.dart';

OfferModelModel offerModelModelFromJson(String str) => OfferModelModel.fromJson(json.decode(str));

String offerModelModelToJson(OfferModelModel data) => json.encode(data.toJson());

class OfferModelModel {
  OfferModelModel({
    this.status,
    this.message,
    this.errors,
    this.data,
  });

  int status;
  String message;
  dynamic errors;
  Data data;

  factory OfferModelModel.fromJson(Map<String, dynamic> json) => OfferModelModel(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    errors: json["errors"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "errors": errors,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.offers,
  });

  List<Offer> offers;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    offers: json["offers"] == null ? null : List<Offer>.from(json["offers"].map((x) => Offer.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "offers": offers == null ? null : List<dynamic>.from(offers.map((x) => x.toJson())),
  };
}

class Offer {
  Offer({
    this.offersId,
    this.offersCode,
    this.fkAccountsId,
    this.fkCategoriesId,
    this.offersImgMobile,
    this.offersImgTab,
    this.offersPrice,
    this.offersCalories,
    this.offersHome,
    this.offersStatus,
    this.offersPosition,
    this.offersCreatedAt,
    this.offersUpdatedAt,
    this.offersTitle,
    this.offersDesc,
    this.account,
    this.translations,
  });

  int offersId;
  String offersCode;
  int fkAccountsId;
  int fkCategoriesId;
  String offersImgMobile;
  String offersImgTab;
  String offersPrice;
  bool offersCalories;
  String offersHome;
  String offersStatus;
  int offersPosition;
  DateTime offersCreatedAt;
  DateTime offersUpdatedAt;
  String offersTitle;
  dynamic offersDesc;
  Account account;
  List<OfferTranslation> translations;

  factory Offer.fromJson(Map<String, dynamic> json) => Offer(
    offersId: json["offers_id"] == null ? null : json["offers_id"],
    offersCode: json["offers_code"] == null ? null : json["offers_code"],
    fkAccountsId: json["FK_accounts_id"] == null ? null : json["FK_accounts_id"],
    fkCategoriesId: json["FK_categories_id"] == null ? null : json["FK_categories_id"],
    offersImgMobile: json["offers_img_mobile"] == null ? null : json["offers_img_mobile"],
    offersImgTab: json["offers_img_tab"] == null ? null : json["offers_img_tab"],
    offersPrice: json["offers_price"] == null ? null : json["offers_price"],
    offersCalories: json["offers_calories"] == null ? null : json["offers_calories"],
    offersHome: json["offers_home"] == null ? null : json["offers_home"],
    offersStatus: json["offers_status"] == null ? null : json["offers_status"],
    offersPosition: json["offers_position"] == null ? null : json["offers_position"],
    offersCreatedAt: json["offers_created_at"] == null ? null : DateTime.parse(json["offers_created_at"]),
    offersUpdatedAt: json["offers_updated_at"] == null ? null : DateTime.parse(json["offers_updated_at"]),
    offersTitle: json["offers_title"] == null ? null : json["offers_title"],
    offersDesc: json["offers_desc"],
    account: json["account"] == null ? null : Account.fromJson(json["account"]),
    translations: json["translations"] == null ? null : List<OfferTranslation>.from(json["translations"].map((x) => OfferTranslation.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "offers_id": offersId == null ? null : offersId,
    "offers_code": offersCode == null ? null : offersCode,
    "FK_accounts_id": fkAccountsId == null ? null : fkAccountsId,
    "FK_categories_id": fkCategoriesId == null ? null : fkCategoriesId,
    "offers_img_mobile": offersImgMobile == null ? null : offersImgMobile,
    "offers_img_tab": offersImgTab == null ? null : offersImgTab,
    "offers_price": offersPrice == null ? null : offersPrice,
    "offers_calories": offersCalories == null ? null : offersCalories,
    "offers_home": offersHome == null ? null : offersHome,
    "offers_status": offersStatus == null ? null : offersStatus,
    "offers_position": offersPosition == null ? null : offersPosition,
    "offers_created_at": offersCreatedAt == null ? null : offersCreatedAt.toIso8601String(),
    "offers_updated_at": offersUpdatedAt == null ? null : offersUpdatedAt.toIso8601String(),
    "offers_title": offersTitle == null ? null : offersTitle,
    "offers_desc": offersDesc,
    "account": account == null ? null : account.toJson(),
    "translations": translations == null ? null : List<dynamic>.from(translations.map((x) => x.toJson())),
  };
}



class OfferTranslation {
  OfferTranslation({
    this.offersTransId,
    this.offersId,
    this.locale,
    this.offersTitle,
    this.offersDesc,
  });

  int offersTransId;
  int offersId;
  String locale;
  String offersTitle;
  dynamic offersDesc;

  factory OfferTranslation.fromJson(Map<String, dynamic> json) => OfferTranslation(
    offersTransId: json["offers_trans_id"] == null ? null : json["offers_trans_id"],
    offersId: json["offers_id"] == null ? null : json["offers_id"],
    locale: json["locale"] == null ? null : json["locale"],
    offersTitle: json["offers_title"] == null ? null : json["offers_title"],
    offersDesc: json["offers_desc"],
  );

  Map<String, dynamic> toJson() => {
    "offers_trans_id": offersTransId == null ? null : offersTransId,
    "offers_id": offersId == null ? null : offersId,
    "locale": locale == null ? null : locale,
    "offers_title": offersTitle == null ? null : offersTitle,
    "offers_desc": offersDesc,
  };
}
