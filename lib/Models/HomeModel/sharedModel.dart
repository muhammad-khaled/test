
class Account {
  Account({
    this.accountsId,
    this.accountsName,
    this.accountsSlug,
    this.accountsImgDashboard,
    this.accountsImgRtl,
    this.accountsImgLtr,
    this.accountsImgHeader,
    this.accountsImgApp,
    this.accountsShowBranches,
    this.accountsShowAreas,
    this.accountsShowCoupons,
    this.accountsSingleMenu,
    this.accountsCountry,
    this.accountsCountryCode,
    this.accountsMultipleCountries,
    this.accountsCountryMobileDigitsCount,
    this.accountsTimezone,
    this.accountsCountryLatLong,
    this.accountsShowCalories,
    this.accountsShowProductType,
    this.accountsFcmKey,
    this.accountsFcmApiKey,
    this.accountsFcmAuthDomain,
    this.accountsFcmDatabaseUrl,
    this.accountsFcmProjectId,
    this.accountsFcmStorageBucket,
    this.accountsFcmMessagingSenderId,
    this.accountsFcmAppId,
    this.accountsFcmMeasurementId,
    this.accountsAndroidUrl,
    this.accountsIosUrl,
    this.accountsProductUrl,
    this.accountsNumberDecimalDigits,
    this.accountsStatus,
    this.accountsPosition,
    this.accountsWebServiceToken,
    this.accountsCreatedAt,
    this.accountsUpdatedAt,
    this.productsName,
    this.translations,
  });

  int accountsId;
  String accountsName;
  String accountsSlug;
  String accountsImgDashboard;
  String accountsImgRtl;
  String accountsImgLtr;
  String accountsImgHeader;
  dynamic accountsImgApp;
  String accountsShowBranches;
  String accountsShowAreas;
  String accountsShowCoupons;
  String accountsSingleMenu;
  String accountsCountry;
  String accountsCountryCode;
  String accountsMultipleCountries;
  int accountsCountryMobileDigitsCount;
  String accountsTimezone;
  String accountsCountryLatLong;
  String accountsShowCalories;
  String accountsShowProductType;
  String accountsFcmKey;
  String accountsFcmApiKey;
  String accountsFcmAuthDomain;
  String accountsFcmDatabaseUrl;
  String accountsFcmProjectId;
  String accountsFcmStorageBucket;
  String accountsFcmMessagingSenderId;
  String accountsFcmAppId;
  dynamic accountsFcmMeasurementId;
  dynamic accountsAndroidUrl;
  dynamic accountsIosUrl;
  String accountsProductUrl;
  int accountsNumberDecimalDigits;
  String accountsStatus;
  int accountsPosition;
  dynamic accountsWebServiceToken;
  DateTime accountsCreatedAt;
  DateTime accountsUpdatedAt;
  String productsName;
  List<AccountTranslation> translations;
  factory Account.fromJson(Map<String, dynamic> json) => Account(
    accountsId: json["accounts_id"] == null ? null : json["accounts_id"],
    accountsName: json["accounts_name"] == null ? null :json["accounts_name"],
    accountsSlug: json["accounts_slug"] == null ? null : json["accounts_slug"],
    accountsImgDashboard: json["accounts_img_dashboard"] == null ? null : json["accounts_img_dashboard"],
    accountsImgRtl: json["accounts_img_rtl"] == null ? null : json["accounts_img_rtl"],
    accountsImgLtr: json["accounts_img_ltr"] == null ? null : json["accounts_img_ltr"],
    accountsImgHeader: json["accounts_img_header"] == null ? null : json["accounts_img_header"],
    accountsImgApp: json["accounts_img_app"],
    accountsShowBranches: json["accounts_show_branches"] == null ? null : json["accounts_show_branches"],
    accountsShowAreas: json["accounts_show_areas"] == null ? null : json["accounts_show_areas"],
    accountsShowCoupons: json["accounts_show_coupons"] == null ? null : json["accounts_show_coupons"],
    accountsSingleMenu: json["accounts_single_menu"] == null ? null : json["accounts_single_menu"],
    accountsCountry: json["accounts_country"] == null ? null : json["accounts_country"],
    accountsCountryCode: json["accounts_country_code"] == null ? null : json["accounts_country_code"],
    accountsMultipleCountries: json["accounts_multiple_countries"] == null ? null : json["accounts_multiple_countries"],
    accountsCountryMobileDigitsCount: json["accounts_country_mobile_digits_count"] == null ? null : json["accounts_country_mobile_digits_count"],
    accountsTimezone: json["accounts_timezone"] == null ? null : json["accounts_timezone"],
    accountsCountryLatLong: json["accounts_country_lat_long"] == null ? null : json["accounts_country_lat_long"],
    accountsShowCalories: json["accounts_show_calories"] == null ? null : json["accounts_show_calories"],
    accountsShowProductType: json["accounts_show_product_type"] == null ? null : json["accounts_show_product_type"],
    accountsFcmKey: json["accounts_fcm_key"] == null ? null : json["accounts_fcm_key"],
    accountsFcmApiKey: json["accounts_fcm_apiKey"] == null ? null : json["accounts_fcm_apiKey"],
    accountsFcmAuthDomain: json["accounts_fcm_authDomain"] == null ? null : json["accounts_fcm_authDomain"],
    accountsFcmDatabaseUrl: json["accounts_fcm_databaseURL"] == null ? null : json["accounts_fcm_databaseURL"],
    accountsFcmProjectId: json["accounts_fcm_projectId"] == null ? null : json["accounts_fcm_projectId"],
    accountsFcmStorageBucket: json["accounts_fcm_storageBucket"] == null ? null : json["accounts_fcm_storageBucket"],
    accountsFcmMessagingSenderId: json["accounts_fcm_messagingSenderId"] == null ? null : json["accounts_fcm_messagingSenderId"],
    accountsFcmAppId: json["accounts_fcm_appId"] == null ? null : json["accounts_fcm_appId"],
    accountsFcmMeasurementId: json["accounts_fcm_measurementId"],
    accountsAndroidUrl: json["accounts_android_url"],
    accountsIosUrl: json["accounts_ios_url"],
    accountsProductUrl: json["accounts_product_url"] == null ? null : json["accounts_product_url"],
    accountsNumberDecimalDigits: json["accounts_number_decimal_digits"] == null ? null : json["accounts_number_decimal_digits"],
    accountsStatus: json["accounts_status"] == null ? null : json["accounts_status"],
    accountsPosition: json["accounts_position"] == null ? null : json["accounts_position"],
    accountsWebServiceToken: json["accounts_web_service_token"],
    accountsCreatedAt: json["accounts_created_at"] == null ? null : DateTime.parse(json["accounts_created_at"]),
    accountsUpdatedAt: json["accounts_updated_at"] == null ? null : DateTime.parse(json["accounts_updated_at"]),
    productsName: json["products_name"] == null ? null : json["products_name"],
    translations: json["translations"] == null ? null : List<AccountTranslation>.from(json["translations"].map((x) => AccountTranslation.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "accounts_id": accountsId == null ? null : accountsId,
    "accounts_name": accountsName == null ? null : accountsName,
    "accounts_slug": accountsSlug == null ? null : accountsSlug,
    "accounts_img_dashboard": accountsImgDashboard == null ? null : accountsImgDashboard,
    "accounts_img_rtl": accountsImgRtl == null ? null : accountsImgRtl,
    "accounts_img_ltr": accountsImgLtr == null ? null : accountsImgLtr,
    "accounts_img_header": accountsImgHeader == null ? null : accountsImgHeader,
    "accounts_img_app": accountsImgApp,
    "accounts_show_branches": accountsShowBranches == null ? null : accountsShowBranches,
    "accounts_show_areas": accountsShowAreas == null ? null : accountsShowAreas,
    "accounts_show_coupons": accountsShowCoupons == null ? null : accountsShowCoupons,
    "accounts_single_menu": accountsSingleMenu == null ? null : accountsSingleMenu,
    "accounts_country": accountsCountry == null ? null : accountsCountry,
    "accounts_country_code": accountsCountryCode == null ? null : accountsCountryCode,
    "accounts_multiple_countries": accountsMultipleCountries == null ? null : accountsMultipleCountries,
    "accounts_country_mobile_digits_count": accountsCountryMobileDigitsCount == null ? null : accountsCountryMobileDigitsCount,
    "accounts_timezone": accountsTimezone == null ? null : accountsTimezone,
    "accounts_country_lat_long": accountsCountryLatLong == null ? null : accountsCountryLatLong,
    "accounts_show_calories": accountsShowCalories == null ? null : accountsShowCalories,
    "accounts_show_product_type": accountsShowProductType == null ? null : accountsShowProductType,
    "accounts_fcm_key": accountsFcmKey == null ? null : accountsFcmKey,
    "accounts_fcm_apiKey": accountsFcmApiKey == null ? null : accountsFcmApiKey,
    "accounts_fcm_authDomain": accountsFcmAuthDomain == null ? null : accountsFcmAuthDomain,
    "accounts_fcm_databaseURL": accountsFcmDatabaseUrl == null ? null : accountsFcmDatabaseUrl,
    "accounts_fcm_projectId": accountsFcmProjectId == null ? null : accountsFcmProjectId,
    "accounts_fcm_storageBucket": accountsFcmStorageBucket == null ? null : accountsFcmStorageBucket,
    "accounts_fcm_messagingSenderId": accountsFcmMessagingSenderId == null ? null : accountsFcmMessagingSenderId,
    "accounts_fcm_appId": accountsFcmAppId == null ? null : accountsFcmAppId,
    "accounts_fcm_measurementId": accountsFcmMeasurementId,
    "accounts_android_url": accountsAndroidUrl,
    "accounts_ios_url": accountsIosUrl,
    "accounts_product_url": accountsProductUrl == null ? null : accountsProductUrl,
    "accounts_number_decimal_digits": accountsNumberDecimalDigits == null ? null : accountsNumberDecimalDigits,
    "accounts_status": accountsStatus == null ? null : accountsStatus,
    "accounts_position": accountsPosition == null ? null : accountsPosition,
    "accounts_web_service_token": accountsWebServiceToken,
    "accounts_created_at": accountsCreatedAt == null ? null : accountsCreatedAt.toIso8601String(),
    "accounts_updated_at": accountsUpdatedAt == null ? null : accountsUpdatedAt.toIso8601String(),
    "products_name": productsName == null ? null : productsName,
    "translations": translations == null ? null : List<dynamic>.from(translations.map((x) => x.toJson())),
  };
}



class AccountTranslation {
  AccountTranslation({
    this.accountsTransId,
    this.accountsId,
    this.locale,
    this.productsName,
  });

  int accountsTransId;
  int accountsId;
  Locale locale;
  String productsName;

  factory AccountTranslation.fromJson(Map<String, dynamic> json) => AccountTranslation(
    accountsTransId: json["accounts_trans_id"] == null ? null : json["accounts_trans_id"],
    accountsId: json["accounts_id"] == null ? null : json["accounts_id"],
    locale: json["locale"] == null ? null : localeValues.map[json["locale"]],
    productsName: json["products_name"] == null ? null : json["products_name"],
  );

  Map<String, dynamic> toJson() => {
    "accounts_trans_id": accountsTransId == null ? null : accountsTransId,
    "accounts_id": accountsId == null ? null : accountsId,
    "locale": locale == null ? null : localeValues.reverse[locale],
    "products_name": productsName == null ? null : productsName,
  };
}
class SharedCategory {
  SharedCategory({
    this.categoriesId,
    this.categoriesCode,
    this.fkAccountsId,
    this.categoriesParentId,
    this.categoriesImg,
    this.categoriesBanner,
    this.categoriesFollowDeliveryTime,
    this.categoriesDiscountRate,
    this.categoriesStatus,
    this.categoriesPosition,
    this.categoriesCreatedAt,
    this.categoriesUpdatedAt,
    this.lft,
    this.rgt,
    this.depth,
    this.categoriesTitle,
    this.categoriesDesc,
    this.translations,
    this.subcategories
  });

  int categoriesId;
  String categoriesCode;
  int fkAccountsId;
  List<SharedCategory> subcategories;
  int categoriesParentId;
  String categoriesImg;
  dynamic categoriesBanner;
  String categoriesFollowDeliveryTime;
  int categoriesDiscountRate;
  String categoriesStatus;
  int categoriesPosition;
  DateTime categoriesCreatedAt;
  DateTime categoriesUpdatedAt;
  int lft;
  int rgt;
  int depth;
  String categoriesTitle;
  dynamic categoriesDesc;
  List<CategoryTranslation> translations;

  factory SharedCategory.fromJson(Map<String, dynamic> json) => SharedCategory(
    categoriesId: json["categories_id"] == null ? null : json["categories_id"],
    categoriesCode: json["categories_code"] == null ? null : json["categories_code"],
    fkAccountsId: json["FK_accounts_id"] == null ? null : json["FK_accounts_id"],
    categoriesParentId: json["categories_parent_id"] == null ? null : json["categories_parent_id"],
    categoriesImg: json["categories_img"] == null ? null : json["categories_img"],
    categoriesBanner: json["categories_banner"],
    categoriesFollowDeliveryTime: json["categories_follow_delivery_time"] == null ? null : json["categories_follow_delivery_time"],
    categoriesDiscountRate: json["categories_discount_rate"] == null ? null : json["categories_discount_rate"],
    categoriesStatus: json["categories_status"] == null ? null : json["categories_status"],
    categoriesPosition: json["categories_position"] == null ? null : json["categories_position"],
    categoriesCreatedAt: json["categories_created_at"] == null ? null : DateTime.parse(json["categories_created_at"]),
    categoriesUpdatedAt: json["categories_updated_at"] == null ? null : DateTime.parse(json["categories_updated_at"]),
    lft: json["lft"] == null ? null : json["lft"],
    rgt: json["rgt"] == null ? null : json["rgt"],
    depth: json["depth"] == null ? null : json["depth"],
    categoriesTitle: json["categories_title"] == null ? null : json["categories_title"],
    categoriesDesc: json["categories_desc"],
    subcategories: json["subcategories"] == null ? null : List<SharedCategory>.from(json["subcategories"].map((x) => SharedCategory.fromJson(x))),
    translations: json["translations"] == null ? null : List<CategoryTranslation>.from(json["translations"].map((x) => CategoryTranslation.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "categories_id": categoriesId == null ? null : categoriesId,
    "categories_code": categoriesCode == null ? null : categoriesCode,
    "FK_accounts_id": fkAccountsId == null ? null : fkAccountsId,
    "categories_parent_id": categoriesParentId == null ? null : categoriesParentId,
    "categories_img": categoriesImg == null ? null : categoriesImg,
    "categories_banner": categoriesBanner,
    "categories_follow_delivery_time": categoriesFollowDeliveryTime == null ? null : categoriesFollowDeliveryTime,
    "categories_discount_rate": categoriesDiscountRate == null ? null : categoriesDiscountRate,
    "categories_status": categoriesStatus == null ? null : categoriesStatus,
    "categories_position": categoriesPosition == null ? null : categoriesPosition,
    "categories_created_at": categoriesCreatedAt == null ? null : categoriesCreatedAt.toIso8601String(),
    "categories_updated_at": categoriesUpdatedAt == null ? null : categoriesUpdatedAt.toIso8601String(),
    "lft": lft == null ? null : lft,
    "rgt": rgt == null ? null : rgt,
    "depth": depth == null ? null : depth,
    "categories_title": categoriesTitle == null ? null : categoriesTitle,
    "categories_desc": categoriesDesc,
    "subcategories": subcategories == null ? null : List<dynamic>.from(subcategories.map((x) => x.toJson())),
    "translations": translations == null ? null : List<dynamic>.from(translations.map((x) => x.toJson())),
  };
}


class ImageShared {
  ImageShared({
    this.productsImagesId,
    this.productsId,
    this.productsImagesName,
  });

  int productsImagesId;
  int productsId;
  String productsImagesName;

  factory ImageShared.fromJson(Map<String, dynamic> json) => ImageShared(
    productsImagesId: json["products_images_id"] == null ? null : json["products_images_id"],
    productsId: json["products_id"] == null ? null : json["products_id"],
    productsImagesName: json["products_images_name"] == null ? null : json["products_images_name"],
  );

  Map<String, dynamic> toJson() => {
    "products_images_id": productsImagesId == null ? null : productsImagesId,
    "products_id": productsId == null ? null : productsId,
    "products_images_name": productsImagesName == null ? null : productsImagesName,
  };
}



class SharedOffer {
  SharedOffer({
    this.offersId,
    this.offersCode,
    this.fkAccountsId,
    this.fkCategoriesId,
    this.offersImgMobile,
    this.offersImgTab,
    this.offersPrice,
    this.offersCalories,
    this.offersHome,
    this.offersStatus,
    this.offersPosition,
    this.offersCreatedAt,
    this.offersUpdatedAt,
    this.offersTitle,
    this.offersDesc,
    this.account,
    this.translations,
  });

  int offersId;
  String offersCode;
  int fkAccountsId;
  int fkCategoriesId;
  String offersImgMobile;
  String offersImgTab;
  String offersPrice;
  bool offersCalories;
  String offersHome;
  String offersStatus;
  int offersPosition;
  DateTime offersCreatedAt;
  DateTime offersUpdatedAt;
  String offersTitle;
  String offersDesc;
  Account account;
  List<OfferTranslation> translations;

  factory SharedOffer.fromJson(Map<String, dynamic> json) => SharedOffer(
    offersId: json["offers_id"] == null ? null : json["offers_id"],
    offersCode: json["offers_code"] == null ? null : json["offers_code"],
    fkAccountsId: json["FK_accounts_id"] == null ? null : json["FK_accounts_id"],
    fkCategoriesId: json["FK_categories_id"] == null ? null : json["FK_categories_id"],
    offersImgMobile: json["offers_img_mobile"] == null ? null : json["offers_img_mobile"],
    offersImgTab: json["offers_img_tab"] == null ? null : json["offers_img_tab"],
    offersPrice: json["offers_price"] == null ? null : json["offers_price"],
    offersCalories: json["offers_calories"] == null ? null : json["offers_calories"],
    offersHome: json["offers_home"] == null ? null : json["offers_home"],
    offersStatus: json["offers_status"] == null ? null : json["offers_status"],
    offersPosition: json["offers_position"] == null ? null : json["offers_position"],
    offersCreatedAt: json["offers_created_at"] == null ? null : DateTime.parse(json["offers_created_at"]),
    offersUpdatedAt: json["offers_updated_at"] == null ? null : DateTime.parse(json["offers_updated_at"]),
    offersTitle: json["offers_title"] == null ? null : json["offers_title"],
    offersDesc: json["offers_desc"] == null ? null : json["offers_desc"],
    account: json["account"] == null ? null : Account.fromJson(json["account"]),
    translations: json["translations"] == null ? null : List<OfferTranslation>.from(json["translations"].map((x) => OfferTranslation.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "offers_id": offersId == null ? null : offersId,
    "offers_code": offersCode == null ? null : offersCode,
    "FK_accounts_id": fkAccountsId == null ? null : fkAccountsId,
    "FK_categories_id": fkCategoriesId == null ? null : fkCategoriesId,
    "offers_img_mobile": offersImgMobile == null ? null : offersImgMobile,
    "offers_img_tab": offersImgTab == null ? null : offersImgTab,
    "offers_price": offersPrice == null ? null : offersPrice,
    "offers_calories": offersCalories == null ? null : offersCalories,
    "offers_home": offersHome == null ? null : offersHome,
    "offers_status": offersStatus == null ? null : offersStatus,
    "offers_position": offersPosition == null ? null : offersPosition,
    "offers_created_at": offersCreatedAt == null ? null : offersCreatedAt.toIso8601String(),
    "offers_updated_at": offersUpdatedAt == null ? null : offersUpdatedAt.toIso8601String(),
    "offers_title": offersTitle == null ? null : offersTitle,
    "offers_desc": offersDesc == null ? null : offersDesc,
    "account": account == null ? null : account.toJson(),
    "translations": translations == null ? null : List<dynamic>.from(translations.map((x) => x.toJson())),
  };
}

class OfferTranslation {
  OfferTranslation({
    this.offersTransId,
    this.offersId,
    this.locale,
    this.offersTitle,
    this.offersDesc,
  });

  int offersTransId;
  int offersId;
  Locale locale;
  String offersTitle;
  String offersDesc;

  factory OfferTranslation.fromJson(Map<String, dynamic> json) => OfferTranslation(
    offersTransId: json["offers_trans_id"] == null ? null : json["offers_trans_id"],
    offersId: json["offers_id"] == null ? null : json["offers_id"],
    locale: json["locale"] == null ? null : localeValues.map[json["locale"]],
    offersTitle: json["offers_title"] == null ? null : json["offers_title"],
    offersDesc: json["offers_desc"] == null ? null : json["offers_desc"],
  );

  Map<String, dynamic> toJson() => {
    "offers_trans_id": offersTransId == null ? null : offersTransId,
    "offers_id": offersId == null ? null : offersId,
    "locale": locale == null ? null : localeValues.reverse[locale],
    "offers_title": offersTitle == null ? null : offersTitle,
    "offers_desc": offersDesc == null ? null : offersDesc,
  };
}


class Pagination {
  Pagination({
    this.currentPage,
    this.firstPageUrl,
    this.from,
    this.lastPage,
    this.lastPageUrl,
    this.nextPageUrl,
    this.path,
    this.perPage,
    this.prevPageUrl,
    this.to,
    this.total,
  });

  int currentPage;
  String firstPageUrl;
  int from;
  int lastPage;
  String lastPageUrl;
  dynamic nextPageUrl;
  String path;
  int perPage;
  dynamic prevPageUrl;
  int to;
  int total;

  factory Pagination.fromJson(Map<String, dynamic> json) => Pagination(
    currentPage: json["current_page"] == null ? null : json["current_page"],
    firstPageUrl: json["first_page_url"] == null ? null : json["first_page_url"],
    from: json["from"] == null ? null : json["from"],
    lastPage: json["last_page"] == null ? null : json["last_page"],
    lastPageUrl: json["last_page_url"] == null ? null : json["last_page_url"],
    nextPageUrl: json["next_page_url"],
    path: json["path"] == null ? null : json["path"],
    perPage: json["per_page"] == null ? null : json["per_page"],
    prevPageUrl: json["prev_page_url"],
    to: json["to"] == null ? null : json["to"],
    total: json["total"] == null ? null : json["total"],
  );

  Map<String, dynamic> toJson() => {
    "current_page": currentPage == null ? null : currentPage,
    "first_page_url": firstPageUrl == null ? null : firstPageUrl,
    "from": from == null ? null : from,
    "last_page": lastPage == null ? null : lastPage,
    "last_page_url": lastPageUrl == null ? null : lastPageUrl,
    "next_page_url": nextPageUrl,
    "path": path == null ? null : path,
    "per_page": perPage == null ? null : perPage,
    "prev_page_url": prevPageUrl,
    "to": to == null ? null : to,
    "total": total == null ? null : total,
  };
}


class Choice {
  Choice({
    this.choicesId,
    this.fkAccountsId,
    this.choicesParentId,
    this.choicesStatus,
    this.choicesPosition,
    this.choicesCreatedAt,
    this.choicesUpdatedAt,
    this.lft,
    this.rgt,
    this.depth,
    this.choicesTitle,
    this.pivot,
    this.translations,
  });

  int choicesId;
  int fkAccountsId;
  dynamic choicesParentId;
  String choicesStatus;
  int choicesPosition;
  DateTime choicesCreatedAt;
  DateTime choicesUpdatedAt;
  int lft;
  int rgt;
  int depth;
  String choicesTitle;
  ChoicePivot pivot;
  List<ChoiceTranslation> translations;

  factory Choice.fromJson(Map<String, dynamic> json) => Choice(
    choicesId: json["choices_id"] == null ? null : json["choices_id"],
    fkAccountsId: json["FK_accounts_id"] == null ? null : json["FK_accounts_id"],
    choicesParentId: json["choices_parent_id"],
    choicesStatus: json["choices_status"] == null ? null : json["choices_status"],
    choicesPosition: json["choices_position"] == null ? null : json["choices_position"],
    choicesCreatedAt: json["choices_created_at"] == null ? null : DateTime.parse(json["choices_created_at"]),
    choicesUpdatedAt: json["choices_updated_at"] == null ? null : DateTime.parse(json["choices_updated_at"]),
    lft: json["lft"] == null ? null : json["lft"],
    rgt: json["rgt"] == null ? null : json["rgt"],
    depth: json["depth"] == null ? null : json["depth"],
    choicesTitle: json["choices_title"] == null ? null : json["choices_title"],
    pivot: json["pivot"] == null ? null : ChoicePivot.fromJson(json["pivot"]),
    translations: json["translations"] == null ? null : List<ChoiceTranslation>.from(json["translations"].map((x) => ChoiceTranslation.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "choices_id": choicesId == null ? null : choicesId,
    "FK_accounts_id": fkAccountsId == null ? null : fkAccountsId,
    "choices_parent_id": choicesParentId,
    "choices_status": choicesStatus == null ? null : choicesStatus,
    "choices_position": choicesPosition == null ? null : choicesPosition,
    "choices_created_at": choicesCreatedAt == null ? null : choicesCreatedAt.toIso8601String(),
    "choices_updated_at": choicesUpdatedAt == null ? null : choicesUpdatedAt.toIso8601String(),
    "lft": lft == null ? null : lft,
    "rgt": rgt == null ? null : rgt,
    "depth": depth == null ? null : depth,
    "choices_title": choicesTitle == null ? null : choicesTitle,
    "pivot": pivot == null ? null : pivot.toJson(),
    "translations": translations == null ? null : List<dynamic>.from(translations.map((x) => x.toJson())),
  };
}

class ChoicePivot {
  ChoicePivot({
    this.fkProductsId,
    this.fkChoicesId,
  });

  int fkProductsId;
  int fkChoicesId;

  factory ChoicePivot.fromJson(Map<String, dynamic> json) => ChoicePivot(
    fkProductsId: json["FK_products_id"] == null ? null : json["FK_products_id"],
    fkChoicesId: json["FK_choices_id"] == null ? null : json["FK_choices_id"],
  );

  Map<String, dynamic> toJson() => {
    "FK_products_id": fkProductsId == null ? null : fkProductsId,
    "FK_choices_id": fkChoicesId == null ? null : fkChoicesId,
  };
}

class ChoiceTranslation {
  ChoiceTranslation({
    this.choicesTransId,
    this.choicesId,
    this.locale,
    this.choicesTitle,
  });

  int choicesTransId;
  int choicesId;
  Locale locale;
  String choicesTitle;

  factory ChoiceTranslation.fromJson(Map<String, dynamic> json) => ChoiceTranslation(
    choicesTransId: json["choices_trans_id"] == null ? null : json["choices_trans_id"],
    choicesId: json["choices_id"] == null ? null : json["choices_id"],
    locale: json["locale"] == null ? null : localeValues.map[json["locale"]],
    choicesTitle: json["choices_title"] == null ? null : json["choices_title"],
  );

  Map<String, dynamic> toJson() => {
    "choices_trans_id": choicesTransId == null ? null : choicesTransId,
    "choices_id": choicesId == null ? null : choicesId,
    "locale": locale == null ? null : localeValues.reverse[locale],
    "choices_title": choicesTitle == null ? null : choicesTitle,
  };
}

class ProductTranslation {
  ProductTranslation({
    this.productsTransId,
    this.productsId,
    this.locale,
    this.productsTitle,
    this.productsDesc,
  });

  int productsTransId;
  int productsId;
  Locale locale;
  String productsTitle;
  String productsDesc;

  factory ProductTranslation.fromJson(Map<String, dynamic> json) => ProductTranslation(
    productsTransId: json["products_trans_id"] == null ? null : json["products_trans_id"],
    productsId: json["products_id"] == null ? null : json["products_id"],
    locale: json["locale"] == null ? null : localeValues.map[json["locale"]],
    productsTitle: json["products_title"] == null ? null : json["products_title"],
    productsDesc: json["products_desc"] == null ? null : json["products_desc"],
  );

  Map<String, dynamic> toJson() => {
    "products_trans_id": productsTransId == null ? null : productsTransId,
    "products_id": productsId == null ? null : productsId,
    "locale": locale == null ? null : localeValues.reverse[locale],
    "products_title": productsTitle == null ? null : productsTitle,
    "products_desc": productsDesc == null ? null : productsDesc,
  };
}

class CategoryTranslation {
  CategoryTranslation({
    this.categoriesTransId,
    this.categoriesId,
    this.locale,
    this.categoriesTitle,
    this.categoriesDesc,
  });

  int categoriesTransId;
  int categoriesId;
  Locale locale;
  String categoriesTitle;
  dynamic categoriesDesc;

  factory CategoryTranslation.fromJson(Map<String, dynamic> json) => CategoryTranslation(
    categoriesTransId: json["categories_trans_id"] == null ? null : json["categories_trans_id"],
    categoriesId: json["categories_id"] == null ? null : json["categories_id"],
    locale: json["locale"] == null ? null : localeValues.map[json["locale"]],
    categoriesTitle: json["categories_title"] == null ? null : json["categories_title"],
    categoriesDesc: json["categories_desc"],
  );

  Map<String, dynamic> toJson() => {
    "categories_trans_id": categoriesTransId == null ? null : categoriesTransId,
    "categories_id": categoriesId == null ? null : categoriesId,
    "locale": locale == null ? null : localeValues.reverse[locale],
    "categories_title": categoriesTitle == null ? null :categoriesTitle,
    "categories_desc": categoriesDesc,
  };
}
enum Locale { AR, EN }

final localeValues = EnumValues({
  "ar": Locale.AR,
  "en": Locale.EN
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}





class ProductShared {
  ProductShared({
    this.productsId,
    this.fkAccountsId,
    this.fkCategoriesId,
    this.productsCode,
    this.productsImg,
    this.productsPrice,
    this.productsPriceAfterSale,
    this.productsCalories,
    this.productsVideo,
    this.productsType,
    this.productsLowestWeightAvailable,
    this.productsHome,
    this.productsAvailable,
    this.productsStatus,
    this.productsPosition,
    this.productsCreatedAt,
    this.productsUpdatedAt,
    this.isFav,
    this.categorySeries,
    this.productsTitle,
    this.productsDesc,
    this.category,
    this.suggestions,
    this.choices,
    this.images,
    this.account,
    this.translations,
  });

  int productsId;
  int fkAccountsId;
  int fkCategoriesId;
  String productsCode;
  String productsImg;
  String productsPrice;
  String productsPriceAfterSale;
  bool productsCalories;
  String productsVideo;
  String productsType;
  dynamic productsLowestWeightAvailable;
  String productsHome;
  String productsAvailable;
  String productsStatus;
  int productsPosition;
  DateTime productsCreatedAt;
  DateTime productsUpdatedAt;
  int isFav;
  List<SharedCategory> categorySeries;
  String productsTitle;
  String productsDesc;
  SharedCategory category;
  List<ProductShared> suggestions;
  List<Choice> choices;
  List<ImageShared> images;
  Account account;
  List<ProductTranslation> translations;

  factory ProductShared.fromJson(Map<String, dynamic> json) => ProductShared(
    productsId: json["products_id"] == null ? null : json["products_id"],
    fkAccountsId: json["FK_accounts_id"] == null ? null : json["FK_accounts_id"],
    fkCategoriesId: json["FK_categories_id"] == null ? null : json["FK_categories_id"],
    productsCode: json["products_code"] == null ? null : json["products_code"],
    productsImg: json["products_img"] == null ? null : json["products_img"],
    productsPrice: json["products_price"] == null ? null : json["products_price"],
    productsPriceAfterSale: json["products_price_after_sale"] == null ? null : json["products_price_after_sale"],
    productsCalories: json["products_calories"] == null ? null : json["products_calories"],
    productsVideo: json["products_video"] == null ? null : json["products_video"],
    productsType: json["products_type"] == null ? null : json["products_type"],
    productsLowestWeightAvailable: json["products_lowest_weight_available"],
    productsHome: json["products_home"] == null ? null : json["products_home"],
    productsAvailable: json["products_available"] == null ? null : json["products_available"],
    productsStatus: json["products_status"] == null ? null : json["products_status"],
    productsPosition: json["products_position"] == null ? null : json["products_position"],
    productsCreatedAt: json["products_created_at"] == null ? null : DateTime.parse(json["products_created_at"]),
    productsUpdatedAt: json["products_updated_at"] == null ? null : DateTime.parse(json["products_updated_at"]),
    isFav: json["is_fav"] == null ? null : json["is_fav"],
    categorySeries: json["category_series"] == null ? null : List<SharedCategory>.from(json["category_series"].map((x) => SharedCategory.fromJson(x))),
    productsTitle: json["products_title"] == null ? null : json["products_title"],
    productsDesc: json["products_desc"] == null ? null : json["products_desc"],
    category: json["category"] == null ? null : SharedCategory.fromJson(json["category"]),
    suggestions: json["suggestions"] == null ? null : List<ProductShared>.from(json["suggestions"].map((x) => ProductShared.fromJson(x))),
    choices: json["choices"] == null ? null : List<Choice>.from(json["choices"].map((x) => Choice.fromJson(x))),
    images: json["images"] == null ? null : List<ImageShared>.from(json["images"].map((x) => ImageShared.fromJson(x))),
    account: json["account"] == null ? null : Account.fromJson(json["account"]),
    translations: json["translations"] == null ? null : List<ProductTranslation>.from(json["translations"].map((x) => ProductTranslation.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "products_id": productsId == null ? null : productsId,
    "FK_accounts_id": fkAccountsId == null ? null : fkAccountsId,
    "FK_categories_id": fkCategoriesId == null ? null : fkCategoriesId,
    "products_code": productsCode == null ? null : productsCode,
    "products_img": productsImg == null ? null : productsImg,
    "products_price": productsPrice == null ? null : productsPrice,
    "products_price_after_sale": productsPriceAfterSale == null ? null : productsPriceAfterSale,
    "products_calories": productsCalories == null ? null : productsCalories,
    "products_video": productsVideo == null ? null : productsVideo,
    "products_type": productsType == null ? null : productsType,
    "products_lowest_weight_available": productsLowestWeightAvailable,
    "products_home": productsHome == null ? null : productsHome,
    "products_available": productsAvailable == null ? null : productsAvailable,
    "products_status": productsStatus == null ? null : productsStatus,
    "products_position": productsPosition == null ? null : productsPosition,
    "products_created_at": productsCreatedAt == null ? null : productsCreatedAt.toIso8601String(),
    "products_updated_at": productsUpdatedAt == null ? null : productsUpdatedAt.toIso8601String(),
    "is_fav": isFav == null ? null : isFav,
    "category_series": categorySeries == null ? null : List<dynamic>.from(categorySeries.map((x) => x.toJson())),
    "products_title": productsTitle == null ? null : productsTitle,
    "products_desc": productsDesc == null ? null : productsDesc,
    "category": category == null ? null : category.toJson(),
    "suggestions": suggestions == null ? null : List<dynamic>.from(suggestions.map((x) => x.toJson())),
    "choices": choices == null ? null : List<dynamic>.from(choices.map((x) => x.toJson())),
    "images": images == null ? null : List<dynamic>.from(images.map((x) => x.toJson())),
    "account": account == null ? null : account.toJson(),
    "translations": translations == null ? null : List<dynamic>.from(translations.map((x) => x.toJson())),
  };
}