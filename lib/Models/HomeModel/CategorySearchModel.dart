// To parse this JSON data, do
//
//     final categorySearchModel = categorySearchModelFromJson(jsonString);

import 'dart:convert';

import '../../Models/HomeModel/sharedModel.dart';

CategorySearchModel categorySearchModelFromJson(String str) => CategorySearchModel.fromJson(json.decode(str));

String categorySearchModelToJson(CategorySearchModel data) => json.encode(data.toJson());

class CategorySearchModel {
  CategorySearchModel({
    this.status,
    this.message,
    this.errors,
    this.data,
  });

  int status;
  String message;
  dynamic errors;
  Data data;

  factory CategorySearchModel.fromJson(Map<String, dynamic> json) => CategorySearchModel(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    errors: json["errors"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "errors": errors,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.categories,
  });

  List<SharedCategory> categories;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    categories: json["categories"] == null ? null : List<SharedCategory>.from(json["categories"].map((x) => SharedCategory.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "categories": categories == null ? null : List<dynamic>.from(categories.map((x) => x.toJson())),
  };
}


enum Locale { AR, EN }

final localeValues = EnumValues({
  "ar": Locale.AR,
  "en": Locale.EN
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
