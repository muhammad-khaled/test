// To parse this JSON data, do
//
//     final productByIdModel = productByIdModelFromJson(jsonString);

import 'dart:convert';

import '../../Models/HomeModel/sharedModel.dart';

ProductByIdModel productByIdModelFromJson(String str) => ProductByIdModel.fromJson(json.decode(str));

String productByIdModelToJson(ProductByIdModel data) => json.encode(data.toJson());

class ProductByIdModel {
  ProductByIdModel({
    this.status,
    this.message,
    this.errors,
    this.data,
  });

  int status;
  String message;
  dynamic errors;
  Data data;

  factory ProductByIdModel.fromJson(Map<String, dynamic> json) => ProductByIdModel(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    errors: json["errors"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "errors": errors,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.product,
  });

  ProductShared product;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    product: json["product"] == null ? null : ProductShared.fromJson(json["product"]),
  );

  Map<String, dynamic> toJson() => {
    "product": product == null ? null : product.toJson(),
  };
}

