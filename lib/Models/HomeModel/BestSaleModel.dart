// To parse this JSON data, do
//
//     final bestSaleModel = bestSaleModelFromJson(jsonString);

import 'dart:convert';

import '../../Models/HomeModel/sharedModel.dart';

BestSaleModel bestSaleModelFromJson(String str) => BestSaleModel.fromJson(json.decode(str));

String bestSaleModelToJson(BestSaleModel data) => json.encode(data.toJson());

class BestSaleModel {
  BestSaleModel({
    this.status,
    this.message,
    this.errors,
    this.data,
  });

  int status;
  String message;
  dynamic errors;
  Data data;

  factory BestSaleModel.fromJson(Map<String, dynamic> json) => BestSaleModel(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    errors: json["errors"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "errors": errors,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.bestSales,
    this.pagination,
  });

  List<ProductShared> bestSales;
  Pagination pagination;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    bestSales: json["bestSales"] == null ? null : List<ProductShared>.from(json["bestSales"].map((x) => ProductShared.fromJson(x))),
    pagination: json["pagination"] == null ? null : Pagination.fromJson(json["pagination"]),
  );

  Map<String, dynamic> toJson() => {
    "bestSales": bestSales == null ? null : List<dynamic>.from(bestSales.map((x) => x.toJson())),
    "pagination": pagination == null ? null : pagination.toJson(),
  };
}





