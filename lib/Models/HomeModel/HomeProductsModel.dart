// To parse this JSON data, do
//
//     final homeProductsModel = homeProductsModelFromJson(jsonString);

import 'dart:convert';

import '../../Models/HomeModel/sharedModel.dart';

HomeProductsModel homeProductsModelFromJson(String str) => HomeProductsModel.fromJson(json.decode(str));

String homeProductsModelToJson(HomeProductsModel data) => json.encode(data.toJson());

class HomeProductsModel {
  HomeProductsModel({
    this.status,
    this.message,
    this.errors,
    this.data,
  });

  int status;
  String message;
  dynamic errors;
  Data data;

  factory HomeProductsModel.fromJson(Map<String, dynamic> json) => HomeProductsModel(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    errors: json["errors"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "errors": errors,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.homeProducts,
    this.pagination,
    this.min,
    this.max,
  });

  List<ProductShared> homeProducts;
  Pagination pagination;
  String min;
  String max;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    homeProducts: json["products"] == null ? null : List<ProductShared>.from(json["products"].map((x) => ProductShared.fromJson(x))),
    pagination: json["pagination"] == null ? null : Pagination.fromJson(json["pagination"]),
    min: json["min"] == null ? null : json["min"],
    max: json["max"] == null ? null : json["max"],
  );

  Map<String, dynamic> toJson() => {
    "products": homeProducts == null ? null : List<dynamic>.from(homeProducts.map((x) => x.toJson())),
    "pagination": pagination == null ? null : pagination.toJson(),
    "min": min == null ? null : min,
    "max": max == null ? null : max,
  };
}



