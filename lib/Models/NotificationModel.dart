class NotificationModel {
  int status;
  String message;
  List errors;
  Data data;

  NotificationModel({this.status, this.message, this.errors, this.data});

  NotificationModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    errors = json['errors'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    data['errors'] = this.errors;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Notifications> notifications;
  Pagination pagination;

  Data({this.notifications, this.pagination});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['notifications'] != null) {
      notifications = new List<Notifications>();
      json['notifications'].forEach((v) {
        notifications.add(new Notifications.fromJson(v));
      });
    }
    pagination = json['pagination'] != null
        ? new Pagination.fromJson(json['pagination'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.notifications != null) {
      data['notifications'] =
          this.notifications.map((v) => v.toJson()).toList();
    }
    if (this.pagination != null) {
      data['pagination'] = this.pagination.toJson();
    }
    return data;
  }
}

class Notifications {
  int notificationsId;
  int fKAccountsId;
  String notificationsText;
  String notificationsType;
  String notificationsCreatedAt;
  String notificationsUpdatedAt;
  List<Customers> customers;

  Notifications(
      {this.notificationsId,
        this.fKAccountsId,
        this.notificationsText,
        this.notificationsType,
        this.notificationsCreatedAt,
        this.notificationsUpdatedAt,
        this.customers});

  Notifications.fromJson(Map<String, dynamic> json) {
    notificationsId = json['notifications_id'];
    fKAccountsId = json['FK_accounts_id'];
    notificationsText = json['notifications_text'];
    notificationsType = json['notifications_type'];
    notificationsCreatedAt = json['notifications_created_at'];
    notificationsUpdatedAt = json['notifications_updated_at'];
    if (json['customers'] != null) {
      customers = new List<Customers>();
      json['customers'].forEach((v) {
        customers.add(new Customers.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['notifications_id'] = this.notificationsId;
    data['FK_accounts_id'] = this.fKAccountsId;
    data['notifications_text'] = this.notificationsText;
    data['notifications_type'] = this.notificationsType;
    data['notifications_created_at'] = this.notificationsCreatedAt;
    data['notifications_updated_at'] = this.notificationsUpdatedAt;
    if (this.customers != null) {
      data['customers'] = this.customers.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Customers {
  int notificationsCustomersId;
  int notificationsId;
  int customersId;
  String readAt;

  Customers(
      {this.notificationsCustomersId,
        this.notificationsId,
        this.customersId,
        this.readAt});

  Customers.fromJson(Map<String, dynamic> json) {
    notificationsCustomersId = json['notifications_customers_id'];
    notificationsId = json['notifications_id'];
    customersId = json['customers_id'];
    readAt = json['read_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['notifications_customers_id'] = this.notificationsCustomersId;
    data['notifications_id'] = this.notificationsId;
    data['customers_id'] = this.customersId;
    data['read_at'] = this.readAt;
    return data;
  }
}

class Pagination {
  int currentPage;
  String firstPageUrl;
  int from;
  int lastPage;
  String lastPageUrl;
  String nextPageUrl;
  String path;
  int perPage;
  String prevPageUrl;
  int to;
  int total;

  Pagination(
      {this.currentPage,
        this.firstPageUrl,
        this.from,
        this.lastPage,
        this.lastPageUrl,
        this.nextPageUrl,
        this.path,
        this.perPage,
        this.prevPageUrl,
        this.to,
        this.total});

  Pagination.fromJson(Map<String, dynamic> json) {
    currentPage = json['current_page'];
    firstPageUrl = json['first_page_url'];
    from = json['from'];
    lastPage = json['last_page'];
    lastPageUrl = json['last_page_url'];
    nextPageUrl = json['next_page_url'];
    path = json['path'];
    perPage = json['per_page'];
    prevPageUrl = json['prev_page_url'];
    to = json['to'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = this.currentPage;
    data['first_page_url'] = this.firstPageUrl;
    data['from'] = this.from;
    data['last_page'] = this.lastPage;
    data['last_page_url'] = this.lastPageUrl;
    data['next_page_url'] = this.nextPageUrl;
    data['path'] = this.path;
    data['per_page'] = this.perPage;
    data['prev_page_url'] = this.prevPageUrl;
    data['to'] = this.to;
    data['total'] = this.total;
    return data;
  }
}