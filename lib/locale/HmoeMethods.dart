



import '../../provider/NotificaionProvider.dart';
import 'package:provider/provider.dart';

import '../../dataControl/cartModule.dart';
import '../../screens/orderDetails.dart';
import '../../widgets/UI_Alert_Widgets.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import '../Controls.dart';
import '../../globals.dart' as globals;

Future<void> handleNotification(
    {Map<dynamic, dynamic> message,
    bool dialog,
    int notificationType,
    BuildContext context,
    String token,
    String appCurrency}) async {
  var data = message['data'] ?? message;
  var notification = message['notification'];
  if (data['orderID'] == null) {
    print("zzzzz");
    if (notificationType == 2) {
      // OnClickNotificationButton();
    }
  } else {
    String orderID = data['orderID'];
    String orderStatus = data['orderStatus'];
    print("## firebasr xxxx: " +
        'notificationStatusMsg_$orderStatus'.tr().toString());
    print("## firebasr2 : " + orderID);
    String title = 'notificationTitle'.tr().toString();

    if (orderStatus != null) {
      title =
          title + "\n" + 'notificationStatusMsg_$orderStatus'.tr().toString();
    }
    //title = "Show Order Details";

    print("## firebasr2 out IF: " + orderID);
    if (orderID != null) {
      print("## firebasr2 on IF: " + orderID);
      confirmDialogAlert(context:context, title:title, orderId:orderID,token: token,appCurrency: appCurrency);
    }
    print("## firebasr2 after IF: " + orderID);

    /// [...]
  }
}

Future<void> confirmDialogAlert(
    {BuildContext context,
    String title,
    String orderId,
    String token,
    String appCurrency}) {
  print(title);
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius:
            BorderRadius.circular(SystemControls.RadiusCircValue)),
        title: Text(
          title,
          style: TextStyle(
              fontFamily:
              EasyLocalization.of(context).currentLocale.languageCode,
              fontWeight: FontWeight.bold,
              fontSize: SystemControls.font3,
              color: globals.accountColorsData['TextHeaderColor'] != null
                  ? Color(
                  int.parse(globals.accountColorsData['TextHeaderColor']))
                  : SystemControls.TextHeaderColorz),
          textAlign: TextAlign.center,
        ),
        contentPadding: EdgeInsets.only(top: 40, bottom: 0),
        content: ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
            bottomRight: Radius.circular(SystemControls.RadiusCircValue),
          ),
          child: Container(
            height: 40,
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor'] != null
                          ? Color(int.parse(
                          globals.accountColorsData['MainColor']))
                          : SystemControls.MainColorz,
                      border: Border(
                        top: BorderSide(color: Colors.black),
                      ),
                    ),
                    child: FlatButton(
                      child: Text(
                        "yes".tr().toString(),
                        style: TextStyle(
                            fontFamily: EasyLocalization.of(context).currentLocale.languageCode,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font3,
                            color: globals.accountColorsData[
                            'TextOnMainColor'] !=
                                null
                                ? Color(int.parse(globals
                                .accountColorsData['TextOnMainColor']))
                                : SystemControls.TextOnMainColorz),
                      ),
                      onPressed: () async {
                        //TODO
                        var res =
                        await getUserOrderById(EasyLocalization.of(context).currentLocale.languageCode, token, orderId);
                        if (res != null && res.status == 200) {
                          dynamic orderDetails = res.data['order'];
                          print("###### " + orderDetails.toString());
                          Navigator.of(context).pop();
                          //Navigator.of(context).pop();
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => OrderDetails(
                                      EasyLocalization.of(context)
                                          .currentLocale
                                          .languageCode,
                                      appCurrency,
                                      orderDetails,
                                      SystemControls.designControl,
                                      token,
                                      1)));
                        } else if (res.status == 401) {
                          print("Home 259 .... 401 cart");
                          SystemControls().LogoutSetUserData(context, EasyLocalization.of(context).currentLocale.languageCode);
                          ErrorDialogAlertBackHome(context, res.message);
                        }
                      },
                    ),
                  ),
                ),
                Container(
                  color: Colors.black,
                  width: 1,
                  height: 40,
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor'] != null
                          ? Color(int.parse(
                          globals.accountColorsData['MainColor']))
                          : SystemControls.MainColorz,
                      border: Border(
                        top: BorderSide(color: Colors.black),
                      ),
                      //borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: FlatButton(
                      child: Text(
                        "no".tr().toString(),
                        style: TextStyle(
                            fontFamily: EasyLocalization.of(context).currentLocale.languageCode,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font3,
                            color: globals.accountColorsData[
                            'TextOnMainColor'] !=
                                null
                                ? Color(int.parse(globals
                                .accountColorsData['TextOnMainColor']))
                                : SystemControls.TextOnMainColorz),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    },
  );
}
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
new FlutterLocalNotificationsPlugin();
AndroidNotificationChannel _channel = AndroidNotificationChannel("1","dd","dds");


Future displayNotificationAndroid(RemoteMessage message,BuildContext context) async {





  print('${ _channel.id}');
  // print('${message.notification.apple.sound}');



  print('message');
  Provider.of<NotificationProvider>(context,listen: false).getNum();
  Provider.of<NotificationProvider>(context,listen: false).fetchNotificationCounter(EasyLocalization.of(context).currentLocale.languageCode, "userToken", context);
  final sound ="notificationsound.mp3";
  var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
    _channel.id,
    'name3',
    'desc3',
    sound: RawResourceAndroidNotificationSound("${message.notification.android.sound}"),
    icon: "@mipmap/ic_launcher",
    largeIcon: DrawableResourceAndroidBitmap("@mipmap/ic_launcher"),
    enableLights: true,
    importance: Importance.max,
    priority: Priority.high,
    color: Colors.red,
    playSound: true,

    // sound: AndroidNotificationSound
  );
  var iOSPlatformChannelSpecifics = new IOSNotificationDetails(threadIdentifier: "thread_id",presentAlert: true,presentBadge: true,presentSound: true,badgeNumber: 2);
  var platformChannelSpecifics = new NotificationDetails(
      android: androidPlatformChannelSpecifics,
      iOS: iOSPlatformChannelSpecifics);
  await flutterLocalNotificationsPlugin.show(
    1,
    "${SystemControls.appName}",
    message.notification.body.toString(),

    platformChannelSpecifics,


    payload: message.data["click_action"].toString(),



  );
}
Future displayNotificationAndroidBackGround(RemoteMessage message) async {
  print('messageBack Ground');
  print('${message.notification.android.channelId}');

  print('messageBack Ground');
  // Provider.of<NotificationProvider>(context,listen: false).getNum();
  // Provider.of<NotificationProvider>(context,listen: false).fetchNotificationCounter(EasyLocalization.of(context).currentLocale.languageCode, "userToken", context);
  final sound ="notificationsound.mp3";
  var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
    _channel.id,
    'name3',
    'desc3',
    sound: RawResourceAndroidNotificationSound("${message.notification.android.sound}"),
    icon: "@mipmap/ic_launcher",
    largeIcon: DrawableResourceAndroidBitmap("@mipmap/ic_launcher"),
    enableLights: true,
    importance: Importance.max,
    priority: Priority.high,
    color: Colors.red,
    playSound: true,

    // sound: AndroidNotificationSound
  );
  var iOSPlatformChannelSpecifics = new IOSNotificationDetails(threadIdentifier: "thread_id",presentAlert: true,presentBadge: true,presentSound: true,badgeNumber: 2);
  var platformChannelSpecifics = new NotificationDetails(
      android: androidPlatformChannelSpecifics,
      iOS: iOSPlatformChannelSpecifics);
  await flutterLocalNotificationsPlugin.show(
    1,
    "${SystemControls.appName}",
    message.notification.body.toString(),

    platformChannelSpecifics,
    payload: message.data["click_action"].toString(),



  );
}

Future displayNotificationIos(Map<String, dynamic> message,BuildContext context) async {
  Provider.of<NotificationProvider>(context,listen: false).getNum();
  print("ios");
  var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
    _channel.id,
    'name',
    'desc',
    icon: "@mipmap/ic_launcher",
    largeIcon: DrawableResourceAndroidBitmap("@mipmap/ic_launcher"),
    enableLights: true,
    importance: Importance.max,
    priority: Priority.high,
    color: Colors.red,
    playSound: true,
  );
  var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
  var platformChannelSpecifics = new NotificationDetails(
      android: androidPlatformChannelSpecifics,
      iOS: iOSPlatformChannelSpecifics);
  await flutterLocalNotificationsPlugin.show(
    1,
    "${SystemControls.appName}",
    message['message'].toString(),
    platformChannelSpecifics,
    payload: message['payload'].toString(),
  );
}