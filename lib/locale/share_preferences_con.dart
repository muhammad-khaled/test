import 'package:shared_preferences/shared_preferences.dart';


class SharePreferenceData{



   final String isLogin = "ApiLanguage";
   getApiLanguage() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    bool language = _prefs.getBool('ApiLanguage') ?? false ;

    return language;
  }


  final String accountsShowAdvertisements = "accountsShowAdvertisements";
   getaccountsShowAdvertisements() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String accountsShowAdvertisements  = _prefs.getString('accountsShowAdvertisements') ?? "" ;

    return accountsShowAdvertisements;
  }

  final String accountsShowCoupons = "accounts_show_coupons";
   getAccountsShowCoupons() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String accountsShowAdvertisements  = _prefs.getString('accounts_show_coupons') ?? "" ;

    return accountsShowAdvertisements;
  }




  final String apHasRegistration = "apHasRegistration";
  Future<bool> getAppRegistration() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    bool language = _prefs.getBool('apHasRegistration') ?? false ;

    return language;
  }

  final String appInfo = "appInfo";
  Future<String> getAppInfo() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String language = _prefs.get('appInfo') ?? "" ;

    return language;
  }
   final String notificationCount = "notifcatonCount";
  Future<String> getLanguageCode() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //print('\n\nReading current language code....\n\n');
    String value = prefs.getString('lang');
    //print('\n\nReading data is about to be done..\n\n');

    //print('\n\n(lang:$value)\n\n');
    return value;
  }

  Future<bool> storeDataToPreferences(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('\nSaving current $key....\n');
    print('\n($key:$value)\n');
    bool isStored = await prefs.setString(key, value);
    print('\n$key is about to be stored..\n');
    return isStored;
  }
  Future<bool> storeIntDataToPreferences(String key, int value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('\nSaving current $key....\n');
    print('\n($key:$value)\n');
    bool isStored = await prefs.setInt(key, value);
    print('\n$key is about to be stored..\n');
    return isStored;
  }
  Future<bool> storeBoolDataToPreferences(String key, bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //print('\nSaving current $key....\n');
    //print('\n($key:$value)\n');
    bool isStored = await prefs.setBool(key, value);
    //print('\n$key is about to be stored..\n');
    return isStored;
  }
  Future<String> getDataFromPreferences(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //print('\n\nRestoring current data status....$key\n\n');
    String isStored = prefs.getString(key);
    //print('\n\nRsetoring status is about to be done..\n\n');
    return isStored;
  }

  Future<bool> getBoolDataFromPreferences(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //print('\n\nRestoring current data status....$key\n\n');
    bool isStored = prefs.getBool(key);
    //print('\n\nRsetoring status is about to be done..\n\n');
    return isStored;
  }

  Future<bool> getCurrentLoggingStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //print('\n\nRestoring current logging status....\n\n');
    bool isStored = prefs.getBool('isLoggedIn');
    //print('\n\nRsetoring status is about to be done..\n\n');

    return isStored;
  }
  Future<bool> setLoggingStatus(bool status) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //print('\n\nRestoring current logging status....\n\n');
    bool isLoggedIn = await prefs.setBool('isLoggedIn',status);
    //print('\n\nRsetoring status is about to be done..\n\n');

    //print('\n\n(isLoggedIn:$isLoggedIn)\n\n');
    return isLoggedIn ?? false;
  }



   getNotificationCount() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    int notification  = _prefs.getInt(notificationCount) ?? 0;

    return notification ;
  }
  clearUserData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.clear();
  }
}