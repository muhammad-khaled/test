library my_prj.globals;

double latitude = 0;
double longitude = 0;
String accounts_country_lat_long = "";
int acount_delivery_time = 0;
String device_token =  "";
var minOrderCost;
var deliveryCost;
String countryCode = "+966";
int numberDecimalDigits = 2;
int numOfLang = 1;
Map<String, String> accountColorsData = {};
List<dynamic> paymentMethods = <dynamic>[];
String show_branches= "0";
String show_areas= "0";
int phone_number_length = 11;
String accounts_multiple_countries = "";
String accounts_product_url = "";