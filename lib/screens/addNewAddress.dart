import 'package:android_intent/android_intent.dart';
import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';

import '../Controls.dart';



import '../screens/addNewAddressSubmit.dart';

import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';


import '../globals.dart' as globals;

class addNewAddress extends StatefulWidget{

  String Lang;
  String AppCurrency;
  String UserToken;
  int design_Control;
  addNewAddress(this.Lang, this.UserToken,this.AppCurrency,this.design_Control);

  _addNewAddress createState() =>
      _addNewAddress(this.Lang, this.UserToken,this.AppCurrency,
          this.design_Control);
}

class _addNewAddress extends State<addNewAddress>{
  String Lang;
  String AppCurrency;
  String UserToken;
  int design_Control;
  @override
  void initState() {
    super.initState();
    CheckGpsAlert(context);
    _getCurrentLocation();
  }
  _addNewAddress(this.Lang, this.UserToken,this.AppCurrency,
      this.design_Control);

@override
  void dispose() {
    // TODO: implement dispose
  CheckGpsAlert(context);

  super.dispose();
  }
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  GoogleMapController mapController;
  Position _currentPosition;
  LatLng _center = LatLng(45.521563, -122.677433);
  final Map<String, Marker> _markers = {};

  _getCurrentLocation() async{

    Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        //Add current location to selected var
        _center = LatLng(_currentPosition.latitude,_currentPosition.longitude);
        //Add selected location to markers List
        _markers.clear();
        final marker = Marker(
          markerId: MarkerId("curr_loc"),
          position: LatLng(_currentPosition.latitude, _currentPosition.longitude),
          infoWindow: InfoWindow(title: 'Your Location'),
        );
        _markers["Current Location"] = marker;
        //Move map camera to selected location or target
        mapController.animateCamera(CameraUpdate.newCameraPosition(
          CameraPosition(
            bearing: 0,
            target: _center,
            zoom: 15.0,
          ),
        ));
      });

    }).catchError((e) {
      print(e);
    });
  }
  _checkGps() async {
    print("_checkGps ... open ");
    if (!(await Geolocator().isLocationServiceEnabled())) {
      if (Theme.of(context).platform == TargetPlatform.android) {
        return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
              ),
              title: Text("gpsCheckTitle".tr().toString()+
                  "\n"+ "gpsOpenMes".tr().toString(),
                style: TextStyle(
                    fontFamily: Lang,
                    fontWeight: FontWeight.bold,
                    fontSize: SystemControls.font3,
                    color: globals.accountColorsData['TextHeaderColor']!=null
                        ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                ),
                textAlign: TextAlign.center,),
              content: ClipRRect(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
                  bottomRight: Radius.circular(SystemControls.RadiusCircValue),
                ),
                child: Container(
                  height: 40,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Container(
                          decoration: BoxDecoration(
                            color: globals.accountColorsData['MainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                            border: Border(
                              top: BorderSide(color: globals.accountColorsData['TextHeaderColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz),
                            ),
                          ),
                          child: FlatButton(
                            child: Text("ok".tr().toString(),
                              style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.normal,
                                  fontSize: SystemControls.font3,
                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                              ),
                            ),
                            onPressed: () {
                              final AndroidIntent intent = AndroidIntent(
                                  action: 'android.settings.LOCATION_SOURCE_SETTINGS');
                              intent.launch();
                              Navigator.of(context, rootNavigator: true).pop();
                            },
                          ),
                        ),
                      ),
                      Container(
                        height: 40,
                        width: 1,
                        color: Colors.black,
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          decoration: BoxDecoration(
                            color: globals.accountColorsData['MainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                            border: Border(
                              top: BorderSide(color: globals.accountColorsData['TextHeaderColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz),
                            ),
                            //borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: FlatButton(
                            child: Text("cancel".tr().toString(),
                              style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.normal,
                                  fontSize: SystemControls.font3,
                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                              ),
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                      ),


                    ],
                  ),
                ),
              ),
            );
          },
        );
      }
    }
  }

  void _onMapCreated(GoogleMapController controller) {
    // _checkGps();
    mapController = controller;
  }
  //OnTab map Function
  _mapTappedSetLoca(LatLng sellectedLocation) {
    print(sellectedLocation);
    _center = sellectedLocation;
    setState(() {
      //On select location add marker
      _markers.clear();
      final marker = Marker(
        markerId: MarkerId("curr_loc"),
        position: sellectedLocation,
        infoWindow: InfoWindow(title: 'Your Location'),
      );
      _markers["Current Location"] = marker;
    });

  }

  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    var _mapWidget = Expanded(
      flex: 1,
      child: Container(
        margin: EdgeInsets.only(top: 10,bottom: 10),
        height: mediaQuery.size.shortestSide-30,
        child: Center(
          child: GoogleMap(
            onTap: _mapTappedSetLoca,

            myLocationEnabled: true,
            onMapCreated: _onMapCreated,
            initialCameraPosition: CameraPosition(
              target: _center,
              zoom: 10.0,
            ),
            markers: _markers.values.toSet(),
          ),
        ),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text("addNewAddress".tr().toString()),
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
      ),
      body: Stack(
        //fit: StackFit.expand,
        children: <Widget>[
          LayoutBG(design_Control),
          Align(
            alignment: Alignment.center,
            child: Container(
              //width: mediaQuery.size.shortestSide,
              margin: EdgeInsets.only(top: 10,left: 10,right: 10,bottom: 10),
              child: Column(
                //shrinkWrap : true,
                //physics: ScrollPhysics(),
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                //padding: EdgeInsets.zero,
                children: <Widget>[
                  _mapWidget,

                  Container(
                    width: mediaQuery.size.width,
                    height: 50,
                    margin: EdgeInsets.only(top: 6,bottom: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                    ),
                    child: FlatButton(
                      onPressed: ()async{

                        Navigator.push(context,
                            MaterialPageRoute(builder: (BuildContext context) =>
                                addNewAddressSubmit(Lang,UserToken,AppCurrency,design_Control,_center)
                            ));
                      },
                      child: Text("next".tr().toString(),
                          style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font3,
                            color: globals.accountColorsData['TextOnMainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                            height: 1.2,
                          )
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
/*
          Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              Container(
                color: SystemControls.BGColor,
                alignment: Alignment.bottomCenter,
                height: 120,
                width: mediaQuery.size.width,
              ),
              Container(
                width: mediaQuery.size.width*0.9,
                margin: EdgeInsets.only(right: 10,left: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[

                  ],
                ),
              )
            ],
          ),*/
        ],
      ),
    );
  }
}