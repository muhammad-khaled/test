import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import '../Controls.dart';

import 'package:easy_localization/easy_localization.dart';

import '../dataControl/userModule.dart';
import '../screens/login.dart';
import '../screens/register.dart';

import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';
import '../widgets/UI_Widgets_InputField.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../locale/share_preferences_con.dart';
import 'package:quiver/async.dart';

import '../globals.dart' as globals;

class updatePhoneVerify extends StatefulWidget{
  String Lang;
  int design_Control;
  String phoneString;
  String countryCode;
  String UserToken;
  bool verified;
  updatePhoneVerify(this.Lang,this.UserToken,this.design_Control,
      this.phoneString,this.countryCode,this.verified);
  _registerVerifyPhoneNumber createState() =>
      _registerVerifyPhoneNumber(Lang, UserToken,design_Control,
          phoneString,countryCode);
}

class _registerVerifyPhoneNumber extends State<updatePhoneVerify> with SharePreferenceData{

  String Lang;
  int design_Control;
  String phoneString;
  String countryCode;
  String UserToken;
  bool verified;
  int CodeNumberOfIndex = 6;
  _registerVerifyPhoneNumber(this.Lang,this.UserToken,
      this.design_Control,this.phoneString,this.countryCode){
    for (var i = 0; i < CodeNumberOfIndex; i++) {
      _listFocusNode.add(new FocusNode());
      _TextFieldControllerCode.add(new TextEditingController());
    }
  }

  void initState() {
    super.initState();
    startTimer();
    _verifyPhoneNumber(context);
    GetCustomerData();
  }
  String email;
  String name;
  String gender;
  String birthday;
  GetCustomerData(){
    SystemControls().getDataFromPreferences("name").then((String Gname){
      name = Gname;
      SystemControls().getDataFromPreferences("email").then((String Gemail){
        email = Gemail;
        SystemControls().getDataFromPreferences("customers_gender").then((String Ggender){
          gender =Ggender;
          SystemControls().getDataFromPreferences("customers_birthday").then((String Gbirthday){
            birthday = Gbirthday;
          });
        });
      });
    });
  }
  _verifyPhoneNumber(BuildContext context) async {
    print("_verifyPhoneNumber "+phoneString);
    String phoneNumber = countryCode+phoneString;
    final FirebaseAuth _auth = await FirebaseAuth.instance;
    _auth.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        timeout: Duration(seconds: 40),
        verificationCompleted: (authCredential) => _verificationComplete(authCredential, context),
        // verificationFailed: (authException) => _verificationFailed(authException, context),
        codeAutoRetrievalTimeout: (verificationId) => _codeAutoRetrievalTimeout(verificationId,context),
        // called when the SMS code is sent
        codeSent: (verificationId, [code]) => _smsCodeSent(verificationId,[code],context));
  }
  /// will get an AuthCredential object that will help with logging into Firebase.
  _verificationComplete(AuthCredential authCredential, BuildContext context) {
    print("Success!!! UUID is: "+authCredential.toString());

    FirebaseAuth.instance.signInWithCredential(authCredential).then((authResult)
    async{
      print("Success!!! UUID is: " + authResult.user.uid);
      if(UserToken != null) {
        var get = await updateUserApi(UserToken, Lang, name,
            email, phoneString,birthday,gender,countryCode,verified);
        if (get == null) {
          ErrorDialogAlert(context,
              "respError".tr().toString());
        }
        else {
          dynamic cu = get.message;
          print(get.message);
          if (get.errors == null && get.status == 200) {
            storeDataToPreferences("customers_phone", phoneString);
            setLoggingStatus(true);
            SuccDialogAlertBackHome(context, Lang, get.message);
          }
          else if (get.status == 401) {
            SystemControls().LogoutSetUserData(context, Lang);
            ErrorDialogAlertBackHome(context, get.message);
          }
          else if (get.errors != null) {
            String ErrorMes = "";
            for (int i = 0; i < get.errors.length; i++) {
              ErrorMes += get.errors[i] + "\n";
            }
            ErrorDialogAlert(context, ErrorMes);
          }
          else {
            ErrorDialogAlert(context, get.message);
          }
        }
      }
      else{
        //TODO
      }
    });
  }

  _smsCodeSent(String verificationId, List<int> code,BuildContext context) {
    // set the verification code so that we can use it to log the user in
    print("## Code sent ...\n"+verificationId+"\n"+code.toString());
    GlobalVerificationId = verificationId;
  }

  _verificationFailed( BuildContext context) {
    // print("Exception!! message:" + authException.message.toString());
    // final snackBar = SnackBar(content: Text("Exception!! message:" + authException.message.toString()));
    // Scaffold.of(context).showSnackBar(snackBar);
  }

  _codeAutoRetrievalTimeout(String verificationId,BuildContext context) {
    // set the verification code so that we can use it to log the user in
    print("_codeAutoRetrievalTimeout: " + verificationId);
    setState(() {
      resendActive=true;
    });
  }

  bool resendActive=false;
  String GlobalVerificationId="";
  final List<FocusNode> _listFocusNode = <FocusNode>[];
  final List<TextEditingController> _TextFieldControllerCode = <TextEditingController>[];

  //TextEditingController TextFieldCodeIndex0 = TextEditingController();
  //final FocusNode _nameFocus = FocusNode();

  int _current = 0;
  void startTimer() {
    CountdownTimer countDownTimer = new CountdownTimer(
      new Duration(seconds: SystemControls.msgWaitingTine),
      new Duration(seconds: 1),
    );

    var sub = countDownTimer.listen(null);
    sub.onData((duration) {
      setState(() { _current = SystemControls.msgWaitingTine - duration.elapsed.inSeconds; });
    });

    sub.onDone(() {
      print("Done");
      resendActive=true;
      sub.cancel();
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text("editPhoneNumber".tr().toString()),
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
      ),
      body: Stack(
        children: <Widget>[
          LayoutBG(design_Control),
          Align(
            alignment: Alignment.center,
            child: Container(
              margin: EdgeInsets.all(20),
              width: MediaQuery.of(context).size.shortestSide,
              child: ListView(
                //mainAxisAlignment: MainAxisAlignment.start,
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("verificationEnterMsg1".tr().toString(),
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font2,
                              color: globals.accountColorsData['TextHeaderColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                          ),
                          textAlign: TextAlign.center,
                        ),
                        Text(_current.toString() + "second".tr().toString(),
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.bold,
                              fontSize: SystemControls.font2,
                              color: globals.accountColorsData['TextHeaderColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    //margin: EdgeInsets.only(top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("verificationEnterMsg2".tr().toString(),
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font2,
                              color: globals.accountColorsData['TextHeaderColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                          ),
                          textAlign: TextAlign.center,
                        ),
                        Text(countryCode+phoneString,
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font2,
                              color: globals.accountColorsData['TextHeaderColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
/// see after translation ///

               EasyLocalization.of(context).currentLocale.languageCode=="en"?   Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Container(
                          margin: EdgeInsets.only(
                            top: 15,
                            right: 10,
                            left: 10,
                          ),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            //maxLength: 1,
                            maxLines: 1,
                            maxLengthEnforced: true,
                            autocorrect: false,
                            controller: _TextFieldControllerCode[0],
                            focusNode: _listFocusNode[0],
                            decoration: InputFieldDecoration("", Lang),
                            keyboardType: TextInputType.phone,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                            onChanged: (String value){
                              if(value.length>=1 && value.isNotEmpty){
                                _listFocusNode[0].unfocus();
                                FocusScope.of(context).requestFocus(_listFocusNode[1]);
                              }
                            },
                          ),
                        ), //phone number
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          margin: EdgeInsets.only(
                            top: 15,
                            right: 10,
                            left: 10,
                          ),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            //maxLength: 1,
                            maxLines: 1,
                            maxLengthEnforced: true,
                            autocorrect: false,
                            controller: _TextFieldControllerCode[1],
                            focusNode: _listFocusNode[1],
                            decoration: InputFieldDecoration("", Lang),
                            keyboardType: TextInputType.phone,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                            onChanged: (String value){
                              if(value.length>=1 && value.isNotEmpty){
                                _listFocusNode[1].unfocus();
                                FocusScope.of(context).requestFocus(_listFocusNode[2]);
                              }
                            },
                          ),
                        ), //phone number
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          margin: EdgeInsets.only(
                            top: 15,
                            right: 10,
                            left: 10,
                          ),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            //maxLength: 1,
                            maxLines: 1,
                            maxLengthEnforced: true,
                            autocorrect: false,
                            controller: _TextFieldControllerCode[2],
                            focusNode: _listFocusNode[2],
                            decoration: InputFieldDecoration("", Lang),
                            keyboardType: TextInputType.phone,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                            onChanged: (String value){
                              if(value.length>=1 && value.isNotEmpty){
                                _listFocusNode[2].unfocus();
                                FocusScope.of(context).requestFocus(_listFocusNode[3]);
                              }
                            },
                          ),
                        ), //phone number
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          margin: EdgeInsets.only(
                            top: 15,
                            right: 10,
                            left: 10,
                          ),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            //maxLength: 1,
                            maxLines: 1,
                            maxLengthEnforced: true,
                            autocorrect: false,
                            controller: _TextFieldControllerCode[3],
                            focusNode: _listFocusNode[3],
                            decoration: InputFieldDecoration("", Lang),
                            keyboardType: TextInputType.phone,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                            onChanged: (String value){
                              if(value.length>=1 && value.isNotEmpty){
                                _listFocusNode[3].unfocus();
                                FocusScope.of(context).requestFocus(_listFocusNode[4]);
                              }
                            },
                          ),
                        ), //phone number
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          margin: EdgeInsets.only(
                            top: 15,
                            right: 10,
                            left: 10,
                          ),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            //maxLength: 1,
                            maxLines: 1,
                            maxLengthEnforced: true,
                            autocorrect: false,
                            controller: _TextFieldControllerCode[4],
                            focusNode: _listFocusNode[4],
                            decoration: InputFieldDecoration("", Lang),
                            keyboardType: TextInputType.phone,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                            onChanged: (String value){
                              if(value.length>=1 && value.isNotEmpty){
                                _listFocusNode[4].unfocus();
                                FocusScope.of(context).requestFocus(_listFocusNode[5]);
                              }
                            },
                          ),
                        ), //phone number
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          margin: EdgeInsets.only(
                            top: 15,
                            right: 10,
                            left: 10,
                          ),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            //maxLength: 1,
                            maxLines: 1,
                            maxLengthEnforced: true,
                            autocorrect: false,
                            controller: _TextFieldControllerCode[5],
                            focusNode: _listFocusNode[5],
                            decoration: InputFieldDecoration("", Lang),
                            keyboardType: TextInputType.phone,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                            onChanged: (String value){
                              if(value.length>=1 && value.isNotEmpty){
                                _listFocusNode[5].unfocus();
                                //FocusScope.of(context).requestFocus(_listFocusNode[1]);
                              }
                            },
                          ),
                        ), //phone number
                      ),
                    ],
                  ):
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Container(
                          margin: EdgeInsets.only(
                            top: 15,
                            right: 10,
                            left: 10,
                          ),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            //maxLength: 1,
                            maxLines: 1,
                            maxLengthEnforced: true,
                            autocorrect: false,
                            controller: _TextFieldControllerCode[5],
                            focusNode: _listFocusNode[5],
                            decoration: InputFieldDecoration("", Lang),
                            keyboardType: TextInputType.phone,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                            onChanged: (String value){
                              if(value.length>=1 && value.isNotEmpty){
                                _listFocusNode[5].unfocus();
                                //FocusScope.of(context).requestFocus(_listFocusNode[1]);
                              }
                            },
                          ),
                        ), //phone number
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          margin: EdgeInsets.only(
                            top: 15,
                            right: 10,
                            left: 10,
                          ),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            //maxLength: 1,
                            maxLines: 1,
                            maxLengthEnforced: true,
                            autocorrect: false,
                            controller: _TextFieldControllerCode[4],
                            focusNode: _listFocusNode[4],
                            decoration: InputFieldDecoration("", Lang),
                            keyboardType: TextInputType.phone,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                            onChanged: (String value){
                              if(value.length>=1 && value.isNotEmpty){
                                _listFocusNode[4].unfocus();
                                FocusScope.of(context).requestFocus(_listFocusNode[5]);
                              }
                            },
                          ),
                        ), //phone number
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          margin: EdgeInsets.only(
                            top: 15,
                            right: 10,
                            left: 10,
                          ),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            //maxLength: 1,
                            maxLines: 1,
                            maxLengthEnforced: true,
                            autocorrect: false,
                            controller: _TextFieldControllerCode[3],
                            focusNode: _listFocusNode[3],
                            decoration: InputFieldDecoration("", Lang),
                            keyboardType: TextInputType.phone,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                            onChanged: (String value){
                              if(value.length>=1 && value.isNotEmpty){
                                _listFocusNode[3].unfocus();
                                FocusScope.of(context).requestFocus(_listFocusNode[4]);
                              }
                            },
                          ),
                        ), //phone number
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          margin: EdgeInsets.only(
                            top: 15,
                            right: 10,
                            left: 10,
                          ),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            //maxLength: 1,
                            maxLines: 1,
                            maxLengthEnforced: true,
                            autocorrect: false,
                            controller: _TextFieldControllerCode[2],
                            focusNode: _listFocusNode[2],
                            decoration: InputFieldDecoration("", Lang),
                            keyboardType: TextInputType.phone,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                            onChanged: (String value){
                              if(value.length>=1 && value.isNotEmpty){
                                _listFocusNode[2].unfocus();
                                FocusScope.of(context).requestFocus(_listFocusNode[3]);
                              }
                            },
                          ),
                        ), //phone number
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          margin: EdgeInsets.only(
                            top: 15,
                            right: 10,
                            left: 10,
                          ),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            //maxLength: 1,
                            maxLines: 1,
                            maxLengthEnforced: true,
                            autocorrect: false,
                            controller: _TextFieldControllerCode[1],
                            focusNode: _listFocusNode[1],
                            decoration: InputFieldDecoration("", Lang),
                            keyboardType: TextInputType.phone,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                            onChanged: (String value){
                              if(value.length>=1 && value.isNotEmpty){
                                _listFocusNode[1].unfocus();
                                FocusScope.of(context).requestFocus(_listFocusNode[2]);
                              }
                            },
                          ),
                        ), //phone number
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          margin: EdgeInsets.only(
                            top: 15,
                            right: 10,
                            left: 10,
                          ),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            //maxLength: 1,
                            maxLines: 1,
                            maxLengthEnforced: true,
                            autocorrect: false,
                            controller: _TextFieldControllerCode[0],
                            focusNode: _listFocusNode[0],
                            decoration: InputFieldDecoration("", Lang),
                            keyboardType: TextInputType.phone,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                            onChanged: (String value){
                              if(value.length>=1 && value.isNotEmpty){
                                _listFocusNode[0].unfocus();
                                FocusScope.of(context).requestFocus(_listFocusNode[1]);
                              }
                            },
                          ),
                        ), //phone number
                      ),

                      //////






                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: Text("enterVerificationCode".tr().toString(),
                      style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font2,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: FlatButton(
                      onPressed: ()async{
                        //TODO
                        String SmsCode="";
                        if(_TextFieldControllerCode[0].text.length==1 &&
                            _TextFieldControllerCode[1].text.length==1 &&
                            _TextFieldControllerCode[2].text.length==1 &&
                            _TextFieldControllerCode[3].text.length==1 &&
                            _TextFieldControllerCode[4].text.length==1 &&
                            _TextFieldControllerCode[5].text.length==1 &&
                            GlobalVerificationId!=""
                        ){
                          for (var i = 0; i < CodeNumberOfIndex; i++) {
                            SmsCode +=_TextFieldControllerCode[i].text;
                          }

                          var _credential = PhoneAuthProvider.credential(
                              verificationId: GlobalVerificationId, smsCode: SmsCode.trim());
                          _verificationComplete(_credential,context);
                        }
                        else{
                          print("Error Error Error Error");
                        }

                      },
                      child: Center(
                        child: Text(
                          "verify".tr().toString(),
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font2,
                              color: globals.accountColorsData['TextOnMainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ), //Submit
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: Text("verificationCodeNotReceived".tr().toString(),
                      style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.bold,
                          fontSize: SystemControls.font2,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Container(
                          margin: EdgeInsets.only(top: 10),
                          decoration: BoxDecoration(
                            color: resendActive?(Colors.white):(Colors.black26),
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            border: null,
                          ),
                          child: FlatButton(
                            onPressed: (){
                              //TODO resend method
                              if(resendActive==true){
                                _verifyPhoneNumber(context);
                              }
                            },
                            child: Center(
                              child: Text(
                               "resentVerificationCode".tr().toString(),
                                style: TextStyle(
                                    fontFamily: Lang,
                                    fontWeight: FontWeight.normal,
                                    fontSize: SystemControls.font2,
                                    color: Colors.black
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                      ),
                      ),
                      Container(
                        width: 10,
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          margin: EdgeInsets.only(top: 10),
                        ),
                      ),
                    ],
                  ) //Back && Resend
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

}