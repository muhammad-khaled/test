import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:flutter/services.dart';

import '../dataControl/dataModule.dart';
import '../Controls.dart';


//imppp4
import '../dataControl/cartModule.dart';
import 'package:easy_localization/easy_localization.dart';
import '../dataControl/cartModule.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';
import '../widgets/UI_Widgets_InputField.dart';
import '../screens/myAddresses.dart';

import '../globals.dart' as globals;

class addNewAddressSubmit extends StatefulWidget{

  String Lang;
  String AppCurrency;
  String UserToken;
  int design_Control;
  LatLng center;
  addNewAddressSubmit(this.Lang, this.UserToken,this.AppCurrency,this.design_Control,this.center);

  _addNewAddressSubmit createState() =>
      _addNewAddressSubmit(this.Lang, this.UserToken,this.AppCurrency,
          this.design_Control,this.center);
}

class _addNewAddressSubmit extends State<addNewAddressSubmit>{
  String Lang;
  String AppCurrency;
  String UserToken;
  int design_Control;
  @override
  void initState() {
    super.initState();
  }
  _addNewAddressSubmit(this.Lang, this.UserToken,this.AppCurrency,
      this.design_Control,this._center){
    for(int i=0;i<SystemControls.addresPartsFormat(Lang).length;i++){
      TextEditingController addressTextField1 = TextEditingController();
      addressList.add(addressTextField1);
    }
    _getSelectedAddressText(_center.latitude,_center.longitude);
  }

  TextEditingController addressTitleTextField = TextEditingController();
  final FocusNode _addressFocus = FocusNode();
  final FocusNode _addressTitleFocus = FocusNode();

  bool setDefault = false;
  // final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  GoogleMapController mapController;
  Position _currentPosition;
  LatLng _center;
  final Map<String, Marker> _markers = {};

  //ConvertLocation cordenate toAddress text
  List<TextEditingController> addressList = <TextEditingController>[];
  String GetInputFieldValue(Address address, String key){
    if(key == "addressLine"){
      return address.addressLine;
    }
    else if(key == "adminArea"){
      return address.adminArea;
    }
    else if(key == "countryCode"){
      return address.countryCode;
    }
    else if(key == "countryName"){
      return address.countryName;
    }
    else if(key == "featureName"){
      return address.featureName;
    }
    else if(key == "locality"){
      return address.locality;
    }
    else if(key == "postalCode"){
      return address.postalCode;
    }
    else if(key == "subAdminArea"){
      return address.subAdminArea;
    }
    else if(key == "subLocality"){
      return address.subLocality;
    }
    else if(key == "subThoroughfare"){
      return address.subThoroughfare;
    }
    else if(key == "thoroughfare"){
      return address.thoroughfare;
    }
    else{
      return "";
    }

  }
  _getSelectedAddressText(var lat,var long) async{
    final coordinates = new Coordinates(lat, long);
    Geocoder.local.findAddressesFromCoordinates(coordinates).then((var address) {
      setState(() {
        print("Get address from locer");
        for(int i=0;i<SystemControls.addresPartsFormat(Lang).length;i++){
          addressList[i].text = GetInputFieldValue(address.first,SystemControls.addresGeoCodeKey()[i]);
        }
        print("\n\n\n\n done");
      });

    }).catchError((e) {
      print(e);
    });
  }


  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("addNewAddress".tr().toString()),
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
      ),
      body: Stack(
        //fit: StackFit.expand,
        children: <Widget>[
          LayoutBG(design_Control),
          Align(
            alignment: Alignment.center,
            child: Container(
              //width: mediaQuery.size.shortestSide,
                margin: EdgeInsets.only(top: 10,left: 10,right: 10,bottom: 10),
                child: ListView(
                  //shrinkWrap : true,
                  //physics: ScrollPhysics(),
                  //padding: EdgeInsets.zero,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: TextFormField(
                        controller: addressTitleTextField,
                        focusNode: _addressTitleFocus,
                        decoration: InputFieldDecoration("addressTitle".tr().toString(), Lang),
                        keyboardType: TextInputType.text,
                        onFieldSubmitted: (_){
                          _addressTitleFocus.unfocus();
                          FocusScope.of(context).requestFocus(_addressFocus);
                        },
                        style: new TextStyle(
                            fontFamily: Lang,
                            height: 1
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),

                      child: Row(
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Text("addressDefault".tr().toString(),
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.bold,
                                  fontSize: SystemControls.font3,
                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                  height: 1.2,
                                )
                            ),
                          ),
                          Container(
                            //flex: 1,
                            child: Switch(
                              value: setDefault,
                              onChanged: (value){
                                setState(() {
                                  setDefault = value;
                                });
                              },
                              activeTrackColor: globals.accountColorsData['MainColorOpacity']!=null?
                              Color(int.parse(globals.accountColorsData['MainColorOpacity'])) : SystemControls.MainColorOpacityz,
                              activeColor: globals.accountColorsData['MainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                            ),
                          )
                        ],
                      ),
                    ),


                    ListView.builder(
                        shrinkWrap : true,
                        physics: ScrollPhysics(),
                        scrollDirection: Axis.vertical,
                        itemCount: (SystemControls.addresPartsFormat(Lang).length/2).round(),
                        itemBuilder: (context, ListIndex){
                          int index = ListIndex * 2;
                          if(SystemControls.addresPartsFormat(Lang).length > 3) {
                            return Row(
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    margin: EdgeInsets.only(top: 10),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10)),
                                    ),
                                    child: TextFormField(
                                      controller: addressList[index],
                                      decoration: InputFieldDecoration(
                                          SystemControls.addresPartsFormat(
                                              Lang)[index], Lang),
                                      keyboardType: TextInputType.text,
                                      style: new TextStyle(
                                        fontFamily: Lang,
                                      ),
                                    ),
                                  ),
                                  flex: 1,
                                ),
                                Container(width: 5,),
                                Expanded(
                                  flex: 1,
                                  child: (index + 1) < SystemControls
                                      .addresPartsFormat(Lang)
                                      .length ? (
                                      Container(
                                        margin: EdgeInsets.only(top: 10),
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10)),
                                        ),
                                        child: TextFormField(
                                          controller: addressList[index + 1],
                                          decoration: InputFieldDecoration(
                                              SystemControls
                                                  .addresPartsFormat(
                                                  Lang)[index + 1], Lang),
                                          keyboardType: TextInputType.text,
                                          style: new TextStyle(
                                            fontFamily: Lang,
                                          ),
                                        ),
                                      )
                                  ) : (Container()),
                                )
                              ],
                            );
                          }
                          else {
                            return Column(
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(top: 10),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(10)),
                                  ),
                                  child: TextFormField(
                                    controller: addressList[index],
                                    decoration: InputFieldDecoration(
                                        SystemControls.addresPartsFormat(
                                            Lang)[index], Lang),
                                    keyboardType: TextInputType.text,
                                    style: new TextStyle(
                                      fontFamily: Lang,
                                    ),
                                  ),
                                ),
                                (index + 1) < SystemControls
                                    .addresPartsFormat(Lang)
                                    .length ? (
                                    Container(
                                      margin: EdgeInsets.only(top: 10),
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10)),
                                      ),
                                      child: TextFormField(
                                        controller: addressList[index + 1],
                                        decoration: InputFieldDecoration(
                                            SystemControls
                                                .addresPartsFormat(
                                                Lang)[index + 1], Lang),
                                        keyboardType: TextInputType.text,
                                        style: new TextStyle(
                                          fontFamily: Lang,
                                        ),
                                      ),
                                    )
                                ) : (Container()),
                              ],
                            );
                          }
                        }
                    ),

                    Container(
                      width: mediaQuery.size.width,
                      height: 50,
                      margin: EdgeInsets.only(top: 12,bottom: 10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        color: globals.accountColorsData['MainColor']!=null
                            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                      ),
                      child: FlatButton(
                        onPressed: ()async{
                          String gps = _center.latitude.toString()
                              + "," + _center.longitude.toString();
                          String fulladdress = "";
                          bool missFiled=false;
                          for(int i=0;i<SystemControls.addresPartsFormat(Lang).length;i++){
                            if(SystemControls.addresPartsFormat(Lang)[i].contains('*')
                                &&addressList[i].text.isEmpty){
                              missFiled = true;
                              break;
                            }
                            fulladdress += addressList[i].text +",";
                          }

                          String Default;
                          if(setDefault == false){
                            Default = "0";
                          }
                          else{
                            Default = "1";
                          }
                          if(addressTitleTextField.text.isNotEmpty && fulladdress!=""
                              && missFiled == false){
                            var res = await AddNewAddressApi(Lang, UserToken,
                                fulladdress, addressTitleTextField.text,
                                gps, Default);
                            if(res == null){
                              ErrorDialogAlert(context,
                                  "respError".tr().toString());
                            }
                            else{
                              if(res.errors == null && res.status ==200){
                                SuccDialogAlertBackHome(context, Lang,res.message);
                              }
                              else if(res.status==401){
                                SystemControls().LogoutSetUserData(context, Lang);
                                ErrorDialogAlertBackHome(context, res.message);
                              }
                              else{
                                String error="";
                                for(int i=0;i<res.errors.length;i++){
                                  error += res.errors[i] + "\n";
                                }
                                ErrorDialogAlert(context,error);
                              }
                            }

                          }
                          else{
                            ErrorDialogAlert(context,
                                "AddAllData".tr().toString());
                          }
                        },
                        child: Text("save".tr().toString(),
                            style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData['TextOnMainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                              height: 1.2,
                            )
                        ),
                      ),
                    ),
                  ],)
            ),
          ),
        ],
      ),
    );
  }
}