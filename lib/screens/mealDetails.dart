import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'dart:async';

import '../provider/productsInCartProvider.dart';
import '../Controls.dart';
import '../locale/app_localization.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

import '../widgets/mealSuggestionsList.dart';
import '../dataControl/cartModule.dart';
import '../screens/home.dart';
import '../dataControl/favsModule.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';

import '../dataControl/dataModule.dart';
import '../screens/cart1Items.dart';
import '../widgets/homeItemsList.dart';

import '../globals.dart' as globals;

class MealDetails extends StatefulWidget{
  dynamic _meal;
  String Lang;
  String AppCurrency;
  String UserToken;
  int design_Control;
  List<dynamic> _CartMeals = <dynamic>[];
  bool type_offer;
  String mealId;
  bool hasRegistration;
  MealDetails(this._meal, this._CartMeals, this.Lang, this.AppCurrency,
      this.UserToken,this.design_Control,this.type_offer,this.mealId,this.hasRegistration);

  mealDetails createState() => mealDetails(this._meal, this._CartMeals, this.Lang,
      this.AppCurrency,this.UserToken,this.design_Control,this.type_offer,
      this.mealId,this.hasRegistration);
}
class mealDetails extends State<MealDetails>
    with TickerProviderStateMixin{
  ScrollController _scrollControllerItem;
  int design_Control;
  dynamic _meal;
  String Lang;
  String AppCurrency;
  String UserToken;
  List<dynamic> _CartMeals = <dynamic>[];
  bool type_offer;
  String typeText;
  String mealId;

  Future<CartList> localCart = read_from_file();
  Future<ResponseApi> respInitialx;
  Future<ResponseProductApi> mealResp;
  mealDetails(this._meal, this._CartMeals, this.Lang, this.AppCurrency,
      this.UserToken, this.design_Control,this.type_offer,this.mealId,
      this.hasRegistration){
    //TOGET meall Details if meal not foun
    if(_meal==null){
      mealResp = getMealById(Lang,mealId);
    }
  }

  String CheckText(String get){
    if(get != null)
      return get;
    else
      return "";
  }

  CheckSuggestion(){
    if(_meal['suggestions'] != null){
      if(_meal['suggestions'].length > 0) {
        return Container(
          margin: EdgeInsets.only(right: 10, left: 10, top: 15),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("suggestions".tr().toString(),
                      //"Suggestions",
                      style: TextStyle(
                        fontFamily: Lang,
                        fontSize: SystemControls.font2,
                        color: globals.accountColorsData['TextHeaderColor']!=null
                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                        fontWeight: FontWeight.normal,
                        height: 1,
                      ),
                      maxLines: 1,
                      textAlign: TextAlign.left,
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      }
    }
    return Container();
  }
  FindMealAtCart(int mealId, String type){
    for(int i=0; i<_CartMeals.length; i++){
      if(_CartMeals[i]['products_id'] == mealId &&
          _CartMeals[i]['type'] == type){
        return i;
      }
    }
    return -1;
  }

  DefaultCacheManager manager = new DefaultCacheManager();
  //manager.removeFile(url)

  OnClickCartButton(){
    Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (BuildContext context) =>
            CartLayout(Lang,UserToken,AppCurrency,design_Control)
        ),(Route<dynamic> route) => true
    );
  }

  @override@override
  void dispose() {
    manager.removeFile(SystemControls().GetImgPath(imgURL,"original"));
    super.dispose();
  }

  AddSuggToCart(dynamic item,String typeText){
    print("Get Action ...666.");
    print("add to cart");
    String Img;
    int sear;
    String followDeliveryTime;
    if(typeText == 'offers_'){
      sear = FindMealAtCart(item[typeText+'id'],'offer');
      Img = item['offers_img_mobile'];
      followDeliveryTime = "0";
    }
    else{
      sear = FindMealAtCart(item[typeText+'id'],'product');
      Img = item[typeText+'img'];
      followDeliveryTime = item['category']['categories_follow_delivery_time'];
    }
    if(sear == -1) {
      dynamic AddMeal = CartMeal();
      AddMeal.meals_id =
          item[typeText+'id'];
      AddMeal.meals_title =
          item[typeText+'title'];
      AddMeal.meals_img = Img;
      AddMeal.meals_desc =
          item[typeText+'desc'];
      if(item[typeText+'price_after_sale'] != null&&
          item[typeText+'price_after_sale'] != ""){
        AddMeal.meals_price =
            item[typeText + 'price_after_sale'];
      }
      else {
        AddMeal.meals_price =
            item[typeText + 'price'];
      }
      AddMeal.follow_delivery_time = followDeliveryTime;
      if(type_offer){
        AddMeal.type = "offer";
      }
      else {
        AddMeal.type = "product";
      }
      AddMeal.count = 1;

      dynamic obj = AddMeal.toJson();
      write_to_file(obj);
      setState(() {
        _CartMeals.add(obj);
      });

      SuccDialogAlertStaySameLayout(context,
         'orderAddtoCart'.tr().toString(),ValueKey("sss"));
    }
    else{
      ErrorDialogAlert(context,
          'mealAleadyAdded'.tr().toString());
      //TODO if want to increase meal count
      //_CartMeals[sear]['count']++;
      //await UpdateList(_CartMeals);
    }
  }

  Widget RequestHasError(){
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text("respError".tr().toString(),
            style: TextStyle(
              fontFamily: Lang,
              fontWeight: FontWeight.normal,
              fontSize: SystemControls.font2,
              height: 1.2,
            ),
            textAlign: TextAlign.center,
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            width: 150,
            decoration: BoxDecoration(
              color: globals.accountColorsData['MainColor']!=null
                  ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
              borderRadius: BorderRadius.all(Radius.circular(
                  SystemControls.RadiusCircValue
              )),
            ),
            child: FlatButton(
              onPressed: () {
                setState(() {
                  mealResp = getMealById(Lang,mealId);
                });
              },
              child: Center(
                child: Text("reload".tr().toString(),
                    style: TextStyle(
                      fontFamily: Lang,
                      fontWeight: FontWeight.bold,
                      fontSize: SystemControls.font2,
                    )
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  bool hasRegistration;
  Widget build(BuildContext context) {
    print("_meal details fun ... \n\n\n");
    var cartState = Provider.of<CartStateProvider>(context,listen: false);
    //localCart = read_from_file();
    return FutureBuilder<CartList>(
        future: localCart,
        builder: (context,Cartsnap){
          if(Cartsnap.hasData){
            _CartMeals = Cartsnap.data.Cmeals;
            cartState.setCartList(Cartsnap.data.Cmeals);
          }
          return Scaffold(
            appBar: AppBar(
              title: AppBarTitleImg(Lang),
              centerTitle: true,
              backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
                  ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
              flexibleSpace: AppBarElementsflexibleSpace(design_Control),
              actions: <Widget>[
                AppBarCartButton(context, OnClickCartButton,
                    _CartMeals.length.toString(),hasRegistration),
                AppBarHomeIConButton(context,Lang,SystemControls().GetImgPath(imgURL,"original")),
              ],
            ),
            body: Stack(
              children: <Widget>[
                LayoutBG(design_Control),
                _meal==null?(
                    FutureBuilder<ResponseProductApi>(
                      future: mealResp,
                      builder: (context,mealSnap){
                        if(mealSnap.hasData){
                          _meal = mealSnap.data.data['product'];
                          return LayoutContains();
                        }
                        else if(mealSnap.hasError){
                          return RequestHasError();
                        }
                        return Center(
                          child: SystemControls().circularProgress(),
                        );
                      },
                    )
                ):(
                    LayoutContains()
                ),
              ],
            ),
          );
        }
    );
  }
  //String imgUrl;
  String imgURL;
  String CatName;
  String CatImgURL;
  String followDeliveryTime;
  ItemDataInit(){
    if(type_offer){
      typeText = 'offers_';
    }
    else{
      typeText = 'products_';
    }
    if(type_offer == true){
      imgURL = _meal['offers_img_mobile'].toString();
      //imgUrl = SystemControls().GetImgPath(_meal['offers_img_mobile'],"original");
    }
    else{
      imgURL = _meal[typeText+'img'].toString();
      //imgUrl = SystemControls().GetImgPath(_meal[typeText+'img'],"original");
    }

    if(_meal['category'] == null){
      CatName = "offers".tr().toString();
      CatImgURL = "null";
      followDeliveryTime = "0";
    }
    else{
      CatName = _meal['category']['categories_title'];
      CatImgURL = _meal['category']['categories_img'];
      followDeliveryTime = _meal['category']['categories_follow_delivery_time'];
    }
    if(_meal[typeText+'price_after_sale']!=null &&
        _meal[typeText+'price_after_sale']!=""){
      findOfferPrice = true;
    }
  }
  bool findOfferPrice = false;

  Future<ResponseApi> resCatList;
  bool selectionCat = false;
  onAddCartSubmit(List<dynamic> _cart){
    setState(() {
      _CartMeals = _cart;
    });
  }
  onSeaFavSubmit(List<dynamic> _mealsg){
    setState(() {
      // _meals = _mealsg;
      print("Add fav");
    });
  }

  Widget LayoutContains(){
    ItemDataInit();
    if(selectionCat){
      return FutureBuilder<ResponseApi>(
          future: resCatList,
          builder: (context, snap){
            if(snap.hasData){
              List<dynamic> _searchMeals = snap.data.data['products'];
              if (snap.connectionState == ConnectionState.waiting) {
                return Center(
                  child: SystemControls().circularProgress(),
                );
              }
              if(_searchMeals != null&&_searchMeals.length>0){
                return ListView(
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        InkWell(
                          onTap: (){
                            setState(() {
                              selectionCat = false;
                            });
                          },
                          child: Container(
                            margin: EdgeInsets.all(10),
                            child: Icon(Icons.clear,size: 30,),
                          ),
                        )
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top: SystemControls.branchesValid?(0):(10)),
                      child: HomeItemsList(_searchMeals, _CartMeals, Lang,
                          UserToken, AppCurrency,design_Control,hasRegistration,
                          onAddCartSubmit,onSeaFavSubmit),
                    ),
                  ],
                );
              }
              else{
                return Container(
                  child: Center(
                    child: Text('searchResultNull'.tr().toString(),
                      style: TextStyle(
                        fontFamily: Lang,
                        fontWeight: FontWeight.bold,
                        fontSize: SystemControls.font2,
                        color: globals.accountColorsData['TextHeaderColor']!=null
                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                      ),
                    ),
                  ),
                );
              }
            }
            return Center(
              child: SystemControls().circularProgress(),
            );
          }
      );
    }
    else {
      return Container(
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(bottom: 20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                    Radius.circular(SystemControls.RadiusCircValue*1)),
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 250,
                    child: InkWell(
                      //Mealdetails img to open full img screen
                      onTap: (){
                        _FullImgScreen(context, SystemControls().GetImgPath(imgURL,"original"));
                      },
                      child: Stack(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            alignment: Alignment.center,
                            //TODO use cacheImg
                            child: Stack(
                              children: <Widget>[
                                CachedNetworkImage(
                                  imageUrl: SystemControls().GetImgPath(imgURL,"medium"),
                                  placeholder: (context, url) => Center(child: SystemControls().circularProgress(),),
                                  errorWidget: (context, url, error) => Center(child: Icon(Icons.broken_image),),
                                  fit: BoxFit.contain,
                                  width: MediaQuery.of(context).size.width,
                                  height: 250,
                                ),
                                Hero(
                                  tag: 'imageHero',
                                  child: CachedNetworkImage(
                                    imageUrl: SystemControls().GetImgPath(imgURL,"original"),
                                    fit: BoxFit.contain,
                                    width: MediaQuery.of(context).size.width,
                                    height: 250,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Column(
                            children: <Widget>[
                              Container(
                                //height: 50,
                                alignment: Alignment.centerLeft,
                                child: null,
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 10,left: 10,top: 15),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(_meal[typeText+'title'].toString(),
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontSize: SystemControls.font2,
                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                  fontWeight: FontWeight.bold,
                                  height: 1,
                                ),
                                maxLines: 1,
                                textAlign: TextAlign.start,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 10,left: 10,top: 10),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 8,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              type_offer?(
                                  SystemControls().GetSVGImagesAsset(
                                      'assets/Icons/offer.svg', 25,
                                      Colors.black)
                              ):(
                                  CachedNetworkImage(
                                    imageUrl: SystemControls().GetImgPath(
                                        CatImgURL,"original"),
                                    height: 25,
                                  )
                              ),
                              Container(
                                height: 25,
                                child: _meal['category_series']!=null?ListView.builder(
                                  padding: EdgeInsets.zero,
                                  shrinkWrap : true,
                                  physics: ScrollPhysics(),
                                  scrollDirection: Axis.horizontal,
                                  itemCount: _meal['category_series'].length,
                                  itemBuilder: (context, index){
                                    if (_meal['category_series'][index]['categories_title']==null) {
                                      return Container();
                                    }
                                    return Center(
                                      child: Row(
                                        children: <Widget>[
                                          index!=0?(
                                              Text(" / ",
                                                style: TextStyle(
                                                  fontFamily: Lang,
                                                  fontSize: SystemControls.font4,
                                                  color: globals.accountColorsData['TextdetailsColor']!=null
                                                      ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                  fontWeight: FontWeight.normal,
                                                  height: 0.8,
                                                ),
                                                maxLines: 1,
                                                textAlign: TextAlign.center,
                                              )
                                          ):Container(),
                                          Padding(
                                            padding: EdgeInsets.only(right: 5,left: 5),
                                            child: InkWell(
                                              onTap: (){
                                                setState(() {
                                                  resCatList = CategoryListApi(Lang,
                                                      _meal['category_series'][index]['categories_id'],
                                                      UserToken,"1");
                                                  selectionCat = true;
                                                });
                                              },
                                              child: Text(
                                                _meal['category_series'][index]['categories_title'],
                                                style: TextStyle(
                                                  fontFamily: Lang,
                                                  fontSize: SystemControls.font3,
                                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                                  fontWeight: FontWeight.normal,
                                                  height: 0.8,
                                                ),
                                                maxLines: 1,
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    );
                                  },
                                )
                                    :(
                                    Text("  "+CatName,//"mix grill",
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontSize: SystemControls.font3,
                                        color: globals.accountColorsData['TextdetailsColor']!=null
                                            ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                        fontWeight: FontWeight.normal,
                                        height: 1,
                                      ),
                                      maxLines: 1,
                                      textAlign: TextAlign.start,)
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 3,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(_meal[typeText +'calories']==null||_meal[typeText +'calories']==false? ""
                                  :_meal[typeText+'calories'].toString()
                                  + "calories".tr().toString(),
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontSize: SystemControls.font3,
                                  color: globals.accountColorsData['TextdetailsColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                  fontWeight: FontWeight.normal,
                                  height: 0.8,
                                ),
                                maxLines: 1,
                                textAlign: TextAlign.end,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),


                  CheckText(_meal[typeText+'desc'])==""?(
                      Container()
                  ):(
                      Container(
                        margin: EdgeInsets.only(right: 10,left: 10,top: 15),
                        child: Row(
                          //mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(CheckText(_meal[typeText+'desc']),
                                    style: TextStyle(
                                      fontFamily: Lang,
                                      fontSize: SystemControls.font3,
                                      color: globals.accountColorsData['TextdetailsColor']!=null
                                          ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                      fontWeight: FontWeight.normal,
                                      //height: 1,
                                    ),
                                    //maxLines: 1,
                                    textAlign: TextAlign.start,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                  ),

                  _meal["choices"] != null && _meal["choices"].length > 0?
                  Container(
                    margin: EdgeInsets.only(right: 10,left: 10,top: 10),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text('size'.tr().toString(),
                                style: new TextStyle(
                                  fontSize: SystemControls.font3,
                                  fontFamily: Lang,
                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                  fontWeight: FontWeight.bold,
                                ),
                              )
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              InkWell(
                                child: Text(SelectedSize==""?
                               'selectSize'.tr().toString()
                                    :SelectedSize,
                                  style: new TextStyle(
                                    fontSize: SystemControls.font3,
                                    fontFamily: Lang,
                                    color: globals.accountColorsData['TextHeaderColor']!=null
                                        ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                    fontWeight: FontWeight.normal,
                                  ),
                                ),
                                onTap: (){
                                  showBottomSelectSize2(context,_meal["choices"]);
                                },
                              )
                            ],
                          ),
                        ),
                      ],
                    ), //Category title,
                  )
                      :Container(),

                  _meal[typeText+'price'].toString()=="0"||
                      _meal[typeText+'price']==null?(
                      Container()
                  ):(
                      Container(
                        margin: EdgeInsets.only(right: 10,left: 10,top: 10),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              flex: 3,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  RichText(
                                    textAlign: TextAlign.start,
                                    text: new TextSpan(
                                      children: <TextSpan>[
                                        findOfferPrice?(
                                            TextSpan(
                                              text: _meal[typeText+'price']==null? "n"
                                                  : _meal[typeText+'price'].toString()+" "+ AppCurrency,
                                              style: new TextStyle(
                                                fontSize: SystemControls.font3,
                                                fontFamily: Lang,
                                                color: globals.accountColorsData['TextdetailsColor']!=null
                                                    ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                fontWeight: FontWeight.normal,
                                                decoration: TextDecoration.lineThrough,
                                              ),
                                            )
                                        ):(TextSpan()
                                        ),
                                        findOfferPrice?(
                                            TextSpan(text: ' ',)
                                        ):(TextSpan()
                                        ),
                                        new TextSpan(
                                          text: findOfferPrice
                                              ? _meal[typeText+'price_after_sale'].toString()+" "+ AppCurrency
                                              : _meal[typeText+'price'].toString()+" "+ AppCurrency,
                                          style: TextStyle(
                                            fontSize: SystemControls.font2,
                                            fontFamily: Lang,
                                            color: globals.accountColorsData['TextHeaderColor']!=null
                                                ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Expanded(
                              flex: 4,
                              child: hasRegistration?(
                                  Row(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      type_offer?(
                                          Container()
                                      ):(
                                          Container(
                                            decoration: SystemControls().mealButtonDecoration(),
                                            height: 40,
                                            width: 40,
                                            child: InkWell(
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                  _meal['is_fav']==0?(
                                                      SystemControls().GetSVGImagesAsset(
                                                          "assets/Icons/heart.svg",
                                                          25,
                                                          null)
                                                  ):(
                                                      SystemControls().GetSVGImagesAsset(
                                                          "assets/Icons/heart.svg",
                                                          25,
                                                          globals.accountColorsData['MainColor']!=null
                                                              ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz)
                                                  ),
                                                ],
                                              ),
                                              onTap: ()async{
                                                if(UserToken == ""){
                                                  ErrorDialogAlertGoTOLogin(context,
                                                    "mustLogin".tr().toString(),Lang);
                                                }
                                                else{
                                                  var res = await updateFavsItem(Lang, UserToken,
                                                      _meal['products_id'],context);
                                                  if(res.status ==200){
                                                    print("get login info ++ "+ res.message);
                                                    SuccDialogAlertStaySameLayout(context, res.message,ValueKey("caategoryList1"));
                                                    setState(() {
                                                      if(_meal['is_fav']==1){
                                                        _meal['is_fav'] = 0;
                                                      }
                                                      else{
                                                        _meal['is_fav'] = 1;
                                                      }
                                                    });
                                                  }
                                                  else if(res.status == 401){
                                                    SystemControls().LogoutSetUserData(context, Lang);
                                                    ErrorDialogAlertBackHome(context, res.message);
                                                  }
                                                  else{
                                                    ErrorDialogAlert(context, res.message);
                                                  }
                                                }
                                              },
                                            ),
                                          )
                                      ),
                                      Padding(padding: EdgeInsets.all(8)),
                                      Container(
                                        decoration: SystemControls().mealButtonDecoration(),
                                        height: 40,
                                        width: 40,
                                        child: InkWell(
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              //ToDo if _items[index][typeText+'available'] == null or != "1"
                                              //Show othr icon with line
                                              SystemControls().GetSVGImagesAsset(
                                                  "assets/Icons/cart.svg",
                                                  25,
                                                  Colors.black),
                                            ],
                                          ),
                                          onTap: ()async{
                                            //TODo remove offer after get available from api
                                            if (type_offer||_meal[typeText+'available']!=null
                                                || _meal[typeText+'available']=="1") {
                                              print("add to cart");
                                              int sear;
                                              if (type_offer) {
                                                sear = FindMealAtCart(
                                                    _meal[typeText + 'id'],
                                                    'offer');
                                              }
                                              else {
                                                sear = FindMealAtCart(
                                                    _meal[typeText + 'id'],
                                                    'products');
                                              }
                                              //TODO Remove it to add all items already at the cart
                                              if (true) { //sear == -1
                                                dynamic AddMeal = CartMeal();
                                                AddMeal.meals_id =
                                                _meal[typeText + 'id'];
                                                AddMeal.meals_title =
                                                _meal[typeText + 'title'];
                                                AddMeal.meals_img = imgURL;
                                                AddMeal.meals_desc =
                                                _meal[typeText + 'desc'];
                                                if (findOfferPrice) {
                                                  AddMeal.meals_price =
                                                  _meal[typeText +
                                                      'price_after_sale'];
                                                } else {
                                                  AddMeal.meals_price =
                                                  _meal[typeText + 'price'];
                                                }
                                                AddMeal.follow_delivery_time =
                                                    followDeliveryTime;
                                                if (type_offer) {
                                                  AddMeal.type = "offer";
                                                }
                                                else {
                                                  AddMeal.type = "product";
                                                }
                                                AddMeal.count = 1;
                                                AddMeal.choice_id = SelectedSizeId;
                                                AddMeal.choice_text = SelectedSize;

                                                dynamic obj = AddMeal.toJson();
                                                write_to_file(obj);
                                                //setState(() {
                                                  _CartMeals.add(obj);
                                                //});

                                                CartStateProvider cartState = Provider.of<CartStateProvider>(context,listen: false);
                                                cartState.setCurrentProductsCount(_CartMeals.length.toString());

                                                SuccDialogAlertStaySameLayout(
                                                    context,

                                                        'orderAddtoCart'.tr().toString(),ValueKey("categoryLiast1"));
                                              }
                                              else {
                                                // ErrorDialogAlert(context,
                                                //     AppLocalizations.of(context)
                                                //         .translate(
                                                //         'mealAleadyAdded'));
                                                //TODO if want to increase meal count
                                                //_CartMeals[sear]['count']++;
                                                //await UpdateList(_CartMeals);
                                              }
                                            }
                                          },
                                        ),
                                      ),
                                    ],
                                  )
                              ):(
                                  Container()
                              ),
                            ),
                          ],
                        ), //Category title,
                      )
                  ),

                ],
              ),
            ),

            CheckSuggestion(),
            type_offer?(
                Container()
            ):(
                Container(
                  margin: EdgeInsets.only(bottom: 10),
                  height: 200,
                  child: SuggestionList(_meal['suggestions'],_CartMeals,Lang,
                      AppCurrency,UserToken,design_Control,AddSuggToCart,hasRegistration),
                )
            )
          ],
        ),
      );
    }
  }

  Future<void> _FullImgScreen(BuildContext context, String imgURL) {
    return showDialog<void>(
      context: context,
      //barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        AnimationController controller;
        Animation<double> animation;

        controller = AnimationController(
            duration: const Duration(milliseconds: 1000), vsync: this);
        animation = CurvedAnimation(parent: controller, curve: Curves.easeIn);
        controller.forward();

        return GestureDetector(
          child: Container(
            width: MediaQuery.of(context).size.width,
            color: Color.fromRGBO(0, 0, 0, 0.8),
            child:  Stack(
              children: <Widget>[
                GestureDetector(
                  child: Center(
                    child: Container(
                      child: FadeTransition(
                        opacity: animation,
                        child: CachedNetworkImage(
                          imageUrl:  imgURL,
                          placeholder: (context, url) => Center(child: SystemControls().circularProgress(),),
                          fit: BoxFit.contain,
                          width: MediaQuery.of(context).size.width,
                          fadeInCurve: Curves.bounceInOut,
                          fadeInDuration: const Duration(seconds: 2),
                        ),
                      ),
                    ),
                  ),
                  onTap: () {
                    //Navigator.pop(context);
                  },
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    margin: EdgeInsets.all(20),
                    child: Icon(Icons.cancel,color: Colors.white,size: 30,),
                  ),
                )
              ],
            ),
          ),
          onTap: () {
            Navigator.pop(context);
          },
        );
      },
    );
  }

  //Size Selection
  String SelectedSize = "";
  int SelectedSizeId;
  Future<void> showBottomSelectSize(BuildContext context, List<dynamic> _choices){
    return showModalBottomSheet(
        context: context,
        isScrollControlled: false,
        builder: (context) {
          return Wrap(
            children: <Widget>[
              Container(
                color: Color(0xFF737373),
                //height: 600,
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Text("Select Size",
                            style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.bold,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData['TextHeaderColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                              height: 1.2,
                            )
                        ),
                      ),
                      SizesList(),
                    ],
                  ),
                  decoration: BoxDecoration(
                    color: Theme.of(context).canvasColor,
                    borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(10),
                      topRight: const Radius.circular(10),
                    ),
                  ),
                ),
              )
            ],
          );
        });
  }
  Future<void> showBottomSelectSize2(BuildContext context, List<dynamic> _choices){

    return showModalBottomSheet(
        context: context,
        isScrollControlled: false,
        builder: (context) {
          return ConstrainedBox(
            constraints: new BoxConstraints(
              minHeight: MediaQuery.of(context).size.height*0.2,
              maxHeight: MediaQuery.of(context).size.height*0.4,
            ),
            child: Container(
              color: Color(0xFF737373),
              child: Container(
                child: Stack(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.topCenter,
                      padding: EdgeInsets.all(15),
                      child: Text('selectSize'.tr().toString(),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.bold,
                          fontSize: SystemControls.font3,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                          height: 1.2,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      alignment: Alignment.topRight,
                      padding: EdgeInsets.all(0),
                      child: IconButton(
                        icon: Icon(Icons.close,color: Colors.black,),
                        onPressed: (){
                          Navigator.pop(context);
                        },),
                    ),
                    DraggableScrollableSheet(
                      initialChildSize: 1,
                      minChildSize: 0.8,
                      builder: (context, ScrollController scrollController) {
                        return Container(
                          color: Colors.black12,
                          margin: EdgeInsets.only(top: 50),
                          child: ListView.builder(
                            controller: scrollController,
                            itemCount: _choices.length,
                            itemBuilder: (BuildContext context, int index) {
                              return ListTile(
                                title: Text(_choices[index]['choices_title']),
                                onTap: (){
                                  setState(() {
                                    SelectedSize = _choices[index]['choices_title'];
                                    SelectedSizeId = _choices[index]['choices_id'];
                                  });
                                  Navigator.pop(context);
                                },
                              );
                            },
                          ),
                        );
                      },
                    ),
                  ],
                ),
                decoration: BoxDecoration(
                  color: Theme.of(context).canvasColor,
                  borderRadius: BorderRadius.only(
                    topLeft: const Radius.circular(12),
                    topRight: const Radius.circular(12),
                  ),
                ),
              ),
            ),
          );
          return DraggableScrollableSheet(
            initialChildSize: 1,
            minChildSize: 0.8,
            builder: (context, ScrollController scrollController) {
              return Container(
                color: Colors.blue[100],
                margin: EdgeInsets.only(top: 50),
                child: ListView.builder(
                  controller: scrollController,
                  itemCount: 25,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(title: Text('Item $index'));
                  },
                ),
              );
            },
          );
        });
  }

  Widget SizesList(){
    return ListView(
      scrollDirection: Axis.vertical,
      padding: EdgeInsets.zero,
      shrinkWrap : true,
      physics: ScrollPhysics(),
      children: <Widget>[
        ListTile(
            leading: Icon(Icons.check_circle_outline),
            title: Text("Mediam"),
            onTap: (){
              Navigator.pop(context);
            }
        ),
        ListTile(
            leading: Icon(Icons.check_circle_outline),
            title: Text("Large"),
            onTap: (){
              Navigator.pop(context);
            }
        ),
        ListTile(
            leading: Icon(Icons.check_circle_outline),
            title: Text("X-Large"),
            onTap: (){
              Navigator.pop(context);
            }
        ),
        ListTile(
            leading: Icon(Icons.check_circle_outline),
            title: Text("XX-Large 0"),
            onTap: (){
              Navigator.pop(context);
            }
        ),
        ListTile(
            leading: Icon(Icons.check_circle_outline),
            title: Text("XX-Large 1"),
            onTap: (){
              Navigator.pop(context);
            }
        ),
        ListTile(
            leading: Icon(Icons.check_circle_outline),
            title: Text("XX-Large 2"),
            onTap: (){
              Navigator.pop(context);
            }
        ),
        ListTile(
            leading: Icon(Icons.check_circle_outline),
            title: Text("XX-Large 3"),
            onTap: (){
              Navigator.pop(context);
            }
        ),
        ListTile(
            leading: Icon(Icons.check_circle_outline),
            title: Text("XX-Large 4"),
            onTap: (){
              Navigator.pop(context);
            }
        ),
        ListTile(
            leading: Icon(Icons.check_circle_outline),
            title: Text("XX-Large 5"),
            onTap: (){
              Navigator.pop(context);
            }
        ),
        ListTile(
            leading: Icon(Icons.check_circle_outline),
            title: Text("XX-Large 6"),
            onTap: (){
              Navigator.pop(context);
            }
        ),
        ListTile(
            leading: Icon(Icons.check_circle_outline),
            title: Text("XX-Large 4"),
            onTap: (){
              Navigator.pop(context);
            }
        ),
        ListTile(
            leading: Icon(Icons.check_circle_outline),
            title: Text("XX-Large 5"),
            onTap: (){
              Navigator.pop(context);
            }
        ),
        ListTile(
            leading: Icon(Icons.check_circle_outline),
            title: Text("XX-Large 6"),
            onTap: (){
              Navigator.pop(context);
            }
        ),
      ],
    );
  }
}
