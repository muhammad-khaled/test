import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import '../Controls.dart';

import '../locale/app_localization.dart';
import '../dataControl/userModule.dart';
import 'package:easy_localization/easy_localization.dart';

import '../locale/share_preferences_con.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';
import '../widgets/UI_Widgets_InputField.dart';

import '../globals.dart' as globals;

class forgetPasswordSubmit extends StatelessWidget with SharePreferenceData{

  String Lang;
  String phone;
  String countryCode;
  int design_Control;
  forgetPasswordSubmit(this.Lang,this.phone,
      this.design_Control,this.countryCode);

  TextEditingController newPasswordField = TextEditingController();

  final FocusNode _newpasswordFocus = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("changePassword".tr().toString()),
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
      ),
      body: Stack(
        children: <Widget>[
          LayoutBG(design_Control),
          Align(
            alignment: Alignment.center,
            child: Container(
              margin: EdgeInsets.all(20),
              width: MediaQuery.of(context).size.shortestSide,
              child: ListView(
                //mainAxisAlignment: MainAxisAlignment.start,
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 40),
                    child: Text("addNewPassword".tr().toString(),//"Full name",
                      style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font2,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: TextFormField(
                      controller: newPasswordField,
                      focusNode: _newpasswordFocus,
                      decoration: InputFieldDecoration("newPassword".tr().toString(), Lang),
                      keyboardType: TextInputType.text,
                      onFieldSubmitted: (_){
                        _newpasswordFocus.unfocus();
                      },
                      style: new TextStyle(
                        fontFamily: Lang,
                      ),
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: FlatButton(
                      onPressed: () async{
                        if(newPasswordField.text.isEmpty){
                          print("empity");
                          ErrorDialogAlert(context,
                             "AddAllData".tr().toString());
                        }
                        else{
                          var get = await mobileForgotPasswordApi(Lang,phone,
                              newPasswordField.text,countryCode);
                          if (get == null) {
                            ErrorDialogAlert(context,
                                "respError".tr().toString());
                          }
                          else {
                            dynamic cu = get.message;
                            print(get.message);
                            if (get.status == 200) {
                              SuccDialogAlertBackHome(context, Lang, get.message);
                            }
                            else {
                              if (get.errors != null) {
                                String ErrorMes = "";
                                for (int i = 0; i < get.errors.length; i++) {
                                  ErrorMes += get.errors[i] + "\n";
                                }
                                ErrorDialogAlert(context, ErrorMes);
                              }
                              else {
                                ErrorDialogAlert(context, get.message);
                              }
                            }
                          }
                        }
                      },
                      child: Center(
                        child: Text(
                         "change".tr().toString(),
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font2,
                              color: globals.accountColorsData['TextOnMainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                          ),
                        ),
                      ),
                    ),
                  ), //Login
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}