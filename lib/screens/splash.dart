import 'dart:async';

import '../../screens/HomeTheamOne/HomePageScreenTheamOne.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'dart:io';

import '../dataControl/dataModule.dart';
import '../Controls.dart';
import 'package:easy_localization/easy_localization.dart';
import '../main.dart';

import '../screens/home.dart';
import '../screens/stopApp.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import '../globals.dart' as globals;
import 'HomePageScreenTheamTwo/HomePageScreenTheamTwo.dart';

//import 'package:android_intent/android_intent.dart';
//import 'package:geolocator/geolocator.dart';

String Lang;
class splash extends StatefulWidget {
  splash(this.lang);
  final String lang;
  _splash createState() => _splash();
}
// extension ColorExtension on String {
// toColor(String ops) {
//   var hexColor = this.replaceAll("#", "");
//   if (hexColor.length == 6) {
//     hexColor = ops + hexColor;
//   }
//   if (hexColor.length == 8) {
//     return Color(int.parse("0x$hexColor"));
//
//   }
// }
// }

class _splash extends State<splash>{

  final SystemControls storg = SystemControls();
  bool langFound;
  Future<ResponseApi> resp2;

  FirebaseMessaging _firebaseMessaging;

  void initState(){
    super.initState();
    //////////
    initConnectivity();
    // CheckGpsAlert(context);
    // _connectivitySubscription =
    //     _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
     Lang =widget.lang;
    // storg.getLanguageCode().then((String value){
    //   print("value");
    //   print(value);
    //   print("value");
    //   setState( (){
    //     if(value != null){
    //       langFound = true;
    //       print("At the spalsh Function ... Lang from preference ####"+ value);
    //       Lang = value;
    //     }
    //     else{
    //      langFound = false;
    //      Lang = EasyLocalization.of(context).currentLocale.languageCode;
    //     }
    //     //resp2 = InitialApi(Lang);
    //   });
    // });

    FirebaseMessaging.instance.getToken().then((token){
      print('\n\n(firebase_token2:$token)\n\n');
      globals.device_token = token;
      print('\n\n(device_token: '+globals.device_token+')\n\n');
    });

  }
  @override
  void dispose() {
    // _connectivitySubscription.cancel();
    super.dispose();
  }

  final SystemControls storage = SystemControls();


  bool waitFlag = false;

  String GetHexClorCode(String code,String ops){
    var hexColor = code.replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = ops + hexColor;
    }
    print(hexColor);
    if (hexColor.length == 8) {
      return "0x$hexColor";
    }
    else
      return "nnnnnnn";
  }
  SetGoloraFromApi(dynamic colors){
    if(colors != null) {
      Map<String, String> parametersMap = {
        'MainColor': GetHexClorCode(colors['MainColor'], 'FF'),
        'MainColorOpacity': GetHexClorCode(colors['MainColor'], '7F'),
        'AppbarBGColor': GetHexClorCode(colors['AppbarBGColor'], 'FF'),
        'SearchBGColor': GetHexClorCode(colors['SearchBGColor'], 'FF'),
        'bottomNavigationBarBGColor': GetHexClorCode(
            colors['bottomNavigationBarBGColor'], 'FF'),
        'BGColor': GetHexClorCode(colors['BGColor'], 'FF'),
        'TextHeaderColor': GetHexClorCode(colors['TextHeaderColor'], 'FF'),
        'TextdetailsColor': GetHexClorCode(colors['TextdetailsColor'], 'FF'),
        'BorderColor': GetHexClorCode(colors['BorderColor'], 'FF'),
        'NavigBarBorderColor': GetHexClorCode(
            colors['NavigBarBorderColor'], 'FF'),
        'TextOnMainColor': GetHexClorCode(colors['TextOnMainColor'], 'FF'),
      };
      globals.accountColorsData = parametersMap;
      print("globals globals color #######33");
    }
  }

  @override
  Widget build(BuildContext context) {
    print("_splash fun 1... \n\n\n");
    //globals.accounts_country_lat_long="2222";
    //if(_connectionStatus == "connected"){
      return FutureBuilder<ResponseApi>(
        future: resp2,
        builder: (context,snap){
          if (waitFlag==false && snap.connectionState == ConnectionState.waiting) {
            Timer(Duration(seconds: 30), (){
              print("wait done");
              /*setState(() {
                //waitFlag = true;
              });*/
            });
          }
          if(snap.hasData && snap.data.status != 401){
            print("_splash fun ... has Data\n\n\n");
            dynamic ReposData = snap.data.data;
            String defaultLang = ReposData['defaultLang'];
            dynamic _account = ReposData['account'];
            dynamic _colors = _account['colors'];
            globals.accounts_country_lat_long = _account['accounts_country_lat_long'];
            dynamic setting = ReposData['setting'];
            List<dynamic> lang = ReposData['langs'];
            bool hasRegistration = ReposData['hasRegistration'];
            String Currency = setting['settings_currency'].toString();
            //Get Payment methods from api
            if(ReposData['paymentMethods'] != null){
              globals.paymentMethods = ReposData['paymentMethods'];
              //print(globals.paymentMethods[1]['payments_name']);
            }
            ////Set Colors
            SetGoloraFromApi(_colors);
            //Get account delivery_time
            if(setting['settings_delivery_time'] != null){
              globals.acount_delivery_time = setting['settings_delivery_time'];
              storage.storeIntDataToPreferences
                ("acount_delivery_time", setting['settings_delivery_time']);
            }
            if(setting['settings_delivery_cost'] != null){
              globals.deliveryCost = setting['settings_delivery_cost'];
            }
            else{
              globals.deliveryCost = 0;
            }
            if(setting['settings_min_order'] != null){
              globals.minOrderCost = setting['settings_min_order'];
            }
            else{
              globals.minOrderCost = 0;
            }
            if(_account['accounts_number_decimal_digits'] != null){
              globals.numberDecimalDigits = _account['accounts_number_decimal_digits'];
            }
            if(_account['accounts_country_code'] != null){
              globals.countryCode = _account['accounts_country_code'];
            }
            if(_account['accounts_show_branches'] != null){
              globals.show_branches = _account['accounts_show_branches'];
            }
            if(_account['accounts_show_areas'] != null){
              globals.show_areas = _account['accounts_show_areas'];
            }
            if(_account['accounts_country_mobile_digits_count'] != null){
              globals.phone_number_length = _account['accounts_country_mobile_digits_count'];
            }
            if(_account['accounts_multiple_countries'] != null){
              globals.accounts_multiple_countries = _account['accounts_multiple_countries'];
            }
            if(_account['accounts_product_url'] != null){
              globals.accounts_product_url = _account['accounts_product_url'];
            }
            storg.storeDataToPreferences("currency", Currency);
            storg.storeBoolDataToPreferences("hasRegistration", hasRegistration);

            globals.numOfLang = lang.length;
            if(_account['accounts_status'] != "1"){
              Timer(Duration(seconds: 1), ()=>
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(
                        builder: (BuildContext context) => stopApp(_account['accounts_name'],Lang),)
                  )
              );
            }
            else{
              if(langFound == false){
                //print("can't find land ....#####");
                storg.storeDataToPreferences("lang", defaultLang).then((stord){
                  //print("splash try to store lang 1....######");
                  if(defaultLang == Lang){
                    switch(SystemControls.theme){
                      case "1":
                        Timer(Duration(seconds: 2), ()=>
                            Navigator.pushReplacement(context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) => HomePageScreenTheamOne(),)
                            )
                        );
                        break;
                        case "2":
                        Timer(Duration(seconds: 2), ()=>
                            Navigator.pushReplacement(context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) => HomePageScreenTheamTwo(),)
                            )
                        );
                        break;
                    }
                  }
                  else {
                    Timer(Duration(seconds: 0), () =>
                        Navigator.pushReplacement(context,
                            MaterialPageRoute(
                              builder: (BuildContext context) => MyApp(),)
                        )
                    );
                  }
                });
              }
              else{
                if(lang.length < 2){
                  print("have only one lang "+lang[0]['locale'].toString());
                  if(lang[0]['locale'] == Lang){
                    print("get it will ...");
                   switch(SystemControls.theme){
                     case "1":
                       Timer(Duration(seconds: 2), ()=>
                           Navigator.pushReplacement(context,
                               MaterialPageRoute(
                                 builder: (BuildContext context) => HomePageScreenTheamOne(),)
                           )
                       );
                       break;case "2":
                       Timer(Duration(seconds: 2), ()=>
                           Navigator.pushReplacement(context,
                               MaterialPageRoute(
                                 builder: (BuildContext context) => HomePageScreenTheamTwo(),)
                           )
                       );
                       break;
                   }
                  }
                  else{
                    storg.storeDataToPreferences("lang", defaultLang).then((stord){
                      print("splash try to store lang 1....######");
                      Timer(Duration(seconds: 0), ()=>
                          Navigator.pushReplacement(context,
                              MaterialPageRoute(
                                builder: (BuildContext context) => MyApp(),)
                          )
                      );
                    });
                  }
                }
                else{
                  switch(SystemControls.theme){
                    case "1":
                      Timer(Duration(seconds: 2), ()=>
                          Navigator.pushReplacement(context,
                              MaterialPageRoute(
                                builder: (BuildContext context) => HomePageScreenTheamOne(),)
                          )
                      );
                      break;     case "2":
                      Timer(Duration(seconds: 2), ()=>
                          Navigator.pushReplacement(context,
                              MaterialPageRoute(
                                builder: (BuildContext context) => HomePageScreenTheamTwo(),)
                          )
                      );
                      break;
                  }
                }
              }
            }
          }
          if(snap.hasData && snap.data.status == 401){
            return Scaffold(
              backgroundColor: globals.accountColorsData['BGColor']!=null
                  ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
              appBar: AppBar(
                title: Container(
                  child: Center(
                    child: Image.asset(Lang =="ar"? 'assets/logo.png':"assets/logoEn.png",
                      fit: BoxFit.fitHeight,
                      height: 50,
                    ),
                  ),
                ),
                backgroundColor: globals.accountColorsData['MainColor']!=null
                    ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
              ),
              body: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(snap.data.message,
                      style: TextStyle(
                        fontFamily: Lang,
                        fontWeight: FontWeight.normal,
                        fontSize: SystemControls.font2,
                        height: 1.2,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            );
          }
          else if(snap.hasError || waitFlag==true){
            print("_splash fun ... has Error\n\n\n");
            return Scaffold(
              backgroundColor: globals.accountColorsData['BGColor']!=null
                  ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
              appBar: AppBar(
                title: Container(
                  child: Center(
                    child: Image.asset(Lang =="ar"? 'assets/logo.png':"assets/logoEn.png",
                      fit: BoxFit.fitHeight,
                      height: 50,
                    ),
                  ),
                ),
                backgroundColor: globals.accountColorsData['MainColor']!=null
                    ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
              ),
              body: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("respError".tr().toString(),
                      style: TextStyle(
                        fontFamily: Lang,
                        fontWeight: FontWeight.normal,
                        fontSize: SystemControls.font2,
                        height: 1.2,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      width: 150,
                      decoration: BoxDecoration(
                        color: globals.accountColorsData['MainColor']!=null
                            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                        borderRadius: BorderRadius.all(Radius.circular(
                            SystemControls.RadiusCircValue
                        )),
                      ),
                      child: FlatButton(
                        onPressed: () {
                          print("Reload");
                          setState(() {
                            resp2 = InitialApi(Lang,context);
                            waitFlag = false;
                          });
                        },
                        child: Center(
                          child: Text("reload".tr().toString(),
                              style: TextStyle(
                                fontFamily: Lang,
                                fontWeight: FontWeight.bold,
                                fontSize: SystemControls.font2,
                              )
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
          return Scaffold(
            body: Stack(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    image: DecorationImage(
                        image: AssetImage("assets/SplashBG.png",),
                        //repeat: ImageRepeat.repeat
                      fit: BoxFit.cover
                    ),
                  ),
                  child: Center(
                    child: Image.asset(Lang =="ar"? 'assets/logo.png':"assets/logoEn.png",
                      fit: BoxFit.cover,
                      width: MediaQuery.of(context).size.width>600?(
                          300
                      ):(
                          MediaQuery.of(context).size.width * 0.5
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: ConnectionStatusBar(),
                ),
              ],
            ),
          );
        },
      );
  }

  ConnectionStatusBar(){
    final Color color = Colors.redAccent;
    final double width = double.maxFinite;
    final double height = 30;

    if(_connectionStatus == 'connected'){
      return Container();
    }
    else{
      return Container(
        child: SafeArea(
          bottom: false,
          child: Container(
            color: color,
            width: width,
            height: height,
            child: Container(
              margin: EdgeInsets.only(right: 5,left: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Please check your internet connection',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'en',
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font4,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      InkWell(
                        child: Text(
                          'Try Again..',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'en',
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font4,
                          ),
                        ),
                        onTap: (){
                          setState(() async{
                            initConnectivity();
                          });
                        },
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      );
    }
  }
  ///Internet Check area
  String _connectionStatus = 'connected';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }
  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = "connected";
              print("Set Connection test");
              resp2 = InitialApi(Lang,context);
              waitFlag = false;
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = "failed";
          });
        }
        break;
      case ConnectivityResult.mobile:
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = "connected";
              print("Set Connection test");
              resp2 = InitialApi(Lang,context);
              waitFlag = false;
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = "failed";
          });
        }
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "failed");
        break;
      default:
        setState(() => _connectionStatus = 'failed');
        break;
    }
  }
}