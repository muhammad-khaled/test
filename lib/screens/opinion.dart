import '../locale/share_preferences_con.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';

import '../Controls.dart';
import '../screens/home.dart';

import 'package:easy_localization/easy_localization.dart';
import '../dataControl/userModule.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';
import '../widgets/UI_Widgets_InputField.dart';

import '../globals.dart' as globals;

class opinion extends StatelessWidget with SharePreferenceData{
  String Lang;
  String token;
  int design_Control;
  opinion(this.Lang, this.design_Control,this.token){
    GetCustomerData();  }

  TextEditingController nameTextField = TextEditingController();
  TextEditingController phoneTextField = TextEditingController();
  TextEditingController messageTextField = TextEditingController();

  final FocusNode _nameFocus = FocusNode();
  final FocusNode _messageFocus = FocusNode();
  final FocusNode _phoneFocus = FocusNode();
  String _phoneNumber="";
  String _countryCode="";
  GetCustomerData(){
    SystemControls().getDataFromPreferences("name").then((String name){
      nameTextField.text = name;

        SystemControls().getDataFromPreferences("customers_phone").then((String phone){
          _phoneNumber = phone;
          SystemControls().getDataFromPreferences("customers_country_code").then((String code){
            _countryCode = code;
            phoneTextField.text = code+phone;


            print(phoneTextField.text);
        });
          /*
          SystemControls().getDataFromPreferences("customers_gender").then((String gender){
            print(gender + "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
            setState(() {
              if(gender=="male"){
                _isRadioSelected = 0;
              }
              else if(gender =="female"){
                _isRadioSelected = 1;
              }
            });

            SystemControls().getDataFromPreferences("customers_birthday").then((String birthday){
              print(birthday + "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
              var arr = birthday.split('-');
              if(arr.length==3) {
                birthYearField.text = arr[0];
                birthMonthField.text = arr[1];
                birthDayField.text = arr[2];
              }
            });
          });*/
        });

    });
  }
  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("opinion".tr().toString()),
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
      ),
      body: Stack(
        children: <Widget>[
          LayoutBG(design_Control),
          Align(
            alignment: Alignment.center,
            child: Container(
              width: mediaQuery.size.shortestSide,
              margin: EdgeInsets.all(10),
              child: ListView(
                //mainAxisAlignment: MainAxisAlignment.start,
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                token.isEmpty?  Container(
                    margin: EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: TextFormField(
                      controller: nameTextField,
                      focusNode: _nameFocus,
                      decoration: InputFieldDecoration("name".tr().toString(), Lang),
                      keyboardType: TextInputType.text,
                      onFieldSubmitted: (_){
                        _nameFocus.unfocus();
                        FocusScope.of(context).requestFocus(_phoneFocus);
                      },
                      style: new TextStyle(
                        fontFamily: Lang,
                      ),
                    ),
                  ):Container(height: 0,width: 0,),
                  token.isEmpty? Container(
                    margin: EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: TextFormField(
                      controller: phoneTextField,
                      focusNode: _phoneFocus,
                      decoration: InputFieldDecoration("phone".tr().toString(), Lang),
                      keyboardType: TextInputType.phone,
                      //maxLines: 5,
                      onFieldSubmitted: (_){
                        _phoneFocus.unfocus();
                        FocusScope.of(context).requestFocus(_messageFocus);
                      },
                      style: new TextStyle(
                        fontFamily: Lang,

                      ),
                    ),
                  ):Container(height: 0,width: 0,),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: TextFormField(
                      controller: messageTextField,
                      focusNode: _messageFocus,
                      decoration: InputFieldDecoration("complain".tr().toString(), Lang,


                    ),
                      keyboardType: TextInputType.multiline,
                      maxLines: 5,
                      onFieldSubmitted: (_){
                        _messageFocus.unfocus();
                      },
                      style: new TextStyle(
                        fontFamily: Lang,
                      ),
                    ),
                  ),

                  Container(
                      margin: EdgeInsets.only(top: 50),
                      child: Center(
                        child: Container(
                          height: 40,
                          width: 200,
                          decoration: BoxDecoration(
                            color: globals.accountColorsData['MainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: FlatButton(
                            onPressed: ()async{
                              if(nameTextField.text.isNotEmpty && phoneTextField.text.isNotEmpty
                                  && messageTextField.text.isNotEmpty){
                                _ConfirmDialogAlert(context,Lang);
                              }
                              else{
                                ErrorDialogAlert(context,
                                    "AddAllData".tr().toString());
                              }
                            },
                            child: Text("send".tr().toString(),
                              style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.normal,
                                  fontSize: SystemControls.font2,
                                  color: globals.accountColorsData['TextOnMainColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
                              ),
                            ),
                          ),
                        ),
                      )
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _ConfirmDialogAlert(BuildContext context,String lang) {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
          ),
          title: Text("confirmationMsg".tr().toString(),
            style: TextStyle(
                fontFamily: Lang,
                fontWeight: FontWeight.bold,
                fontSize: SystemControls.font3,
                color: globals.accountColorsData['TextHeaderColor']!=null
                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
            ),
            textAlign: TextAlign.center,
          ),
          contentPadding: EdgeInsets.only(top: 40,bottom: 0),
          content: ClipRRect(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
              bottomRight: Radius.circular(SystemControls.RadiusCircValue),
            ),
            child: Container(
              height: 40,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Container(
                      decoration: BoxDecoration(
                        color: globals.accountColorsData['MainColor']!=null
                            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                        border: Border(
                          top: BorderSide(color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz),
                        ),
                        //borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: FlatButton(
                        child: Text("cancel".tr().toString(),
                          style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font3,
                            color: globals.accountColorsData['TextOnMainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                          ),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ),
                  Container(
                    height: 40,
                    width: 1,
                    color: Colors.black,
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      decoration: BoxDecoration(
                        color: globals.accountColorsData['MainColor']!=null
                            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                        border: Border(
                          top: BorderSide(color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz),
                        ),
                      ),
                      child: FlatButton(
                        child: Text("send".tr().toString(),
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color:globals.accountColorsData['TextOnMainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                          ),
                        ),
                        onPressed: () async{
                          var res = await opinionApi(Lang, nameTextField.text,
                              phoneTextField.text, messageTextField.text,null);
                          if(res == null){
                            ErrorDialogAlert(context,
                                "respError".tr().toString());
                          }
                          else{
                            //print(res.message);
                            if(res.status != null){
                              SuccDialogAlertBackHome(context, Lang, res.message);
                              Timer(Duration(seconds: 2), (){
                               // Navigator.pop(context);
                              });
                            }
                            else{Navigator.pop(context);
                              ErrorDialogAlert(context,res.message);
                            }
                          }

                        },
                      ),
                    ),
                  ),


                ],
              ),
            ),
          ),
        );
      },
    );
  }
}