import '../../widgets/Drawer/ModefiedDrawerWidget.dart';

import '../../widgets/SharedWidget/OffersScreen.dart';

import '../../Models/HomeModel/OfferModel.dart';
import '../../Models/HomeModel/sharedModel.dart';
import '../../dataControl/cartModule.dart';
import '../../locale/HmoeMethods.dart';
import '../../provider/HomeProvider/HomeProvider.dart';
import '../../provider/NotificaionProvider.dart';
import '../../provider/HomeProvider/SearchProvider.dart';
import '../../provider/productsInCartProvider.dart';
import '../../widgets/Drawer/Drawer.dart';
import '../../widgets/SharedWidget/DisplayWidget.dart';
import '../../widgets/SharedWidget/ProgressIndecator.dart';
import '../../widgets/UI_Alert_Widgets.dart';
import '../../widgets/UI_Wigdets.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import '../../globals.dart' as globals;
import 'package:easy_localization/easy_localization.dart';
import '../../Controls.dart';
import '../../Models/HomeModel/applicationHomeModel.dart';
import '../../services/HomeRepo.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../cart1Items.dart';
import '../notificationLayoutList.dart';
import '../orderDetails.dart';
import '../splash.dart';
import '../SharedScreen/SearchScreen.dart';

class HomePageScreenTheamOne extends StatefulWidget {
  const HomePageScreenTheamOne({Key key}) : super(key: key);

  @override
  _HomePageScreenTheamOneState createState() => _HomePageScreenTheamOneState();
}

class _HomePageScreenTheamOneState extends State<HomePageScreenTheamOne> {
  ScrollController _scrollController;

  scrollListener() {
    var homeProvider = Provider.of<HomeProvider>(context, listen: false);

    switch (homeProvider.isCategory) {
      case -2:
        if (homeProvider.currentPage < homeProvider.lastPage &&
            _scrollController.position.pixels >=
                _scrollController.position.maxScrollExtent &&
            homeProvider.isCategory == -2) {
          print('HomePage');
          homeProvider.fetchHomeItems(context, homeProvider.currentPage++);
        }
        break;
      case 0:
        if (homeProvider.currentPage < homeProvider.lastPage &&
            _scrollController.position.pixels >=
                _scrollController.position.maxScrollExtent &&
            homeProvider.isCategory == 0) {
          if (homeProvider.currentPage < homeProvider.nextPage) {
            print('products');
            homeProvider.fetchProductById(
                context, homeProvider.categoryId, homeProvider.currentPage++);
          }
        }

        break;
      case -1:
        if (homeProvider.currentPage < homeProvider.lastPage &&
            _scrollController.position.pixels ==
                _scrollController.position.maxScrollExtent &&
            homeProvider.isCategory == -1) {
          print('offers');
          if (homeProvider.currentPage < homeProvider.nextPage) {
            homeProvider.fetchDiscountProducts(
                context, "${homeProvider.currentPage++}");
          }
        }
        break;
    }
  }



  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  new FlutterLocalNotificationsPlugin();


  String tokenData;
  @override
  void initState() {
    Provider.of<CartStateProvider>(context, listen: false).fetchCardNum();
    localCart = read_from_file();
    _scrollController = ScrollController();
    _scrollController.addListener(scrollListener);

    super.initState();
    log();
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            MacOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );

    WidgetsFlutterBinding.ensureInitialized();

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print(message.data);
      print(message.notification.body);
      print(message.notification.title);
      print(message.messageId);
      print(message.messageType);
      displayNotificationAndroid(message,context);
      // handleNotification(message: message.data,dialog: true,notificationType: 1,token: userToken,appCurrency: appCurrency,context: context);
    });

    // FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
    //   print('A new onMessageOpenedApp event was published!');
    //
    // });

    FirebaseMessaging.instance.getToken().then((String token) {
      assert(token != null);
      String _homeScreenText = "Waiting for token...";
      _homeScreenText = "Push Messaging token: $token";
      tokenData = token;
      print(_homeScreenText);
    });
    ///////////////////////
    // CheckGpsAlert(context);
  }

  final SystemControls storage = SystemControls();

  bool hasRegistration = false;
  dynamic appInitialInfo;
  String userToken = "";
  String appCurrency = "";
  bool isLogging;


  void log() {

    setState(() {
      storage.getAppRegistration().then((value) {
        if (value == true) {
          hasRegistration = true;
        } else {
          hasRegistration = false;
        }
      });
      storage.getAppInfo().then((value) {
        if (value != null) {
          appInitialInfo = value;
        } else {
          appInitialInfo = "";
        }
      });
      print("Home isLogging -> in**" + isLogging.toString());
      storage.getCurrentLoggingStatus().then((status) {
        if (status != null) {
          isLogging = status;
          print("Home isLogging ->" + isLogging.toString());
        } else {
          isLogging = false;
        }
        storage.getDataFromPreferences('api_token').then((token) {
          if (token != null) {
            userToken = token;
            print("Home token ->" + userToken.toString());
          } else {
            userToken = "";
          }
          storage.getDataFromPreferences('currency').then((currency) {
            appCurrency = currency;

            print("Home AppCurrency ->" + appCurrency);
          });
        });
      });
    });


  }


  onClickNotificationButton() {
    if (userToken == "") {
      ErrorDialogAlertGoTOLogin(context, "mustLogin".tr().toString(),
          EasyLocalization.of(context).currentLocale.languageCode);
    } else {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => NotificationList(
                  EasyLocalization.of(context).currentLocale.languageCode,
                  userToken,
                  appCurrency,
                  SystemControls.designControl)),
          (Route<dynamic> route) => true);
    }
  }

  onClickCartButton() {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => CartLayout(
                EasyLocalization.of(context).currentLocale.languageCode,
                userToken,
                appCurrency,
                SystemControls.designControl)),
        (Route<dynamic> route) => true);
  }

  final ImagePicker _picker = ImagePicker();
  void onImageButtonPressed(ImageSource source) async {
    try {
      final pickedFile = await _picker.getImage(
        source: source,
      );
      if (pickedFile.path != null) {
        CartStateProvider cartState =
            Provider.of<CartStateProvider>(context, listen: false);
        cartState.setImgFilePicker(pickedFile);
        String msg = 'AddPicToCartSuccMsg'.tr().toString();
        SuccDialogAlertStaySameLayout(context, msg,ValueKey("homeOne"));
      }
    } catch (e) {
      print("Image picker error : ");
      print(e);
    }
  }

  Future<void> showBottomSelectImgWidget({BuildContext context}) {
    return showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            color: Color(0xFF737373),
            height: 180,
            child: Container(
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Text("imgPickerHeader".tr().toString(),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.bold,
                          fontSize: SystemControls.font3,
                          color: globals.accountColorsData['TextHeaderColor'] !=
                                  null
                              ? Color(int.parse(
                                  globals.accountColorsData['TextHeaderColor']))
                              : SystemControls.TextHeaderColorz,
                          height: 1.2,
                        )),
                  ),
                  ListTile(
                      leading: Icon(Icons.photo_library),
                      title: Text("imgPickerGallery".tr().toString()),
                      onTap: () {
                        print('Select From gallery');
                        onImageButtonPressed(ImageSource.gallery);
                        Navigator.pop(context);
                      }),
                  ListTile(
                      leading: Icon(Icons.camera_alt),
                      title: Text("imgPickerCamera".tr().toString()),
                      onTap: () {
                        print('Take Photo With Camera');
                        onImageButtonPressed(ImageSource.camera);
                        Navigator.pop(context);
                      }),
                ],
              ),
              decoration: BoxDecoration(
                color: Theme.of(context).canvasColor,
                borderRadius: BorderRadius.only(
                  topLeft: const Radius.circular(10),
                  topRight: const Radius.circular(10),
                ),
              ),
            ),
          );
        });
  }

  List<SharedCategory> categories = [];
  Future<CartList> localCart = read_from_file();
  @override
  Widget build(BuildContext context) {
    localCart = read_from_file();
    GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
    toggleDrawer() async {
      if (scaffoldKey.currentState.isDrawerOpen) {
        scaffoldKey.currentState.openEndDrawer();
      } else {
        scaffoldKey.currentState.openEndDrawer();
      }
    }

    Provider.of<CartStateProvider>(context, listen: false).fetchCardNum();
    return Scaffold(
      backgroundColor: Colors.white,
      key: scaffoldKey,
      appBar: AppBar(
        title: AppBarTitleImg(
            EasyLocalization.of(context).currentLocale.languageCode),
        centerTitle: false,
        backgroundColor: AppBarBGColor(),
        flexibleSpace:
            AppBarElementsflexibleSpace(SystemControls.designControl),
        leading: Builder(
          builder: (context) =>
              DrawerIcon(context, SystemControls.designControl),
        ),
        actions: <Widget>[
          //CartButtonAppBar(),
          Container(
            height: 2.h,
            width: 10.w,
            child: InkWell(
              onTap: onClickNotificationButton,
              child: Stack(
                children: [
                  Container(
                    child: Center(
                      child: IconButton(
                        icon: SystemControls().GetSVGImagesAsset(
                            "assets/Icons/notification.svg", 25, null),
                        /*SystemControls().GetSVGImagesAsset(
                "assets/Icons/cart.svg",
                25,
                Colors.black),*/
                        //onPressed: onClickIcon,
                      ),
                    ),
                  ),
                  Consumer<CartStateProvider>(builder: (context, cart, _) {
                    return Container(
                      margin: EdgeInsets.only(top: 2),
                      width: 22,
                      height: 22,
                      decoration: BoxDecoration(
                        //color: Colors.white,
                        color: globals.accountColorsData['MainColor'] != null
                            ? Color(int.parse(
                                globals.accountColorsData['MainColor']))
                            : SystemControls.MainColorz,
                        borderRadius: BorderRadius.all(Radius.circular(11)),
                      ),
                      child: Center(child: Consumer<NotificationProvider>(
                        builder: (context, notification, _) {
                          return Text(
                            "${notification.counter == null ? "0" : notification.counter}",
                            style: TextStyle(
                              color: globals.accountColorsData[
                                          'TextOnMainColor'] !=
                                      null
                                  ? Color(int.parse(globals
                                      .accountColorsData['TextOnMainColor']))
                                  : SystemControls.TextOnMainColorz,
                              fontSize: SystemControls.font4,
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                          );
                        },
                      )),
                    );
                  }),
                ],
              ),
              // onTap: onClickIcon,
            ),
          ),
          Container(
              height: 2.h,
              width: 10.w,
              child: InkWell(
                child: Stack(
                  children: <Widget>[
                    Container(
                      child: Center(
                        child: IconButton(
                          icon: SystemControls().GetSVGImagesAsset(
                              "assets/Icons/cart.svg", 25, Colors.black),
                          //onPressed: onClickIcon,
                        ),
                      ),
                    ),
                    Consumer<CartStateProvider>(builder: (context, cart, _) {
                      return Container(
                        margin: EdgeInsets.only(top: 2),
                        width: 22,
                        height: 22,
                        decoration: BoxDecoration(
                          //color: Colors.white,
                          color: globals.accountColorsData['MainColor'] != null
                              ? Color(int.parse(
                                  globals.accountColorsData['MainColor']))
                              : SystemControls.MainColorz,
                          borderRadius: BorderRadius.all(Radius.circular(11)),
                        ),
                        child: Center(
                          child: Consumer<CartStateProvider>(
                            builder: (context, cart, _) {
                              return StreamBuilder<CartList>(
                                  stream: cart.streamCard,
                                  builder: (context, snap) {
                                    if (snap.hasData) {
                                      double x = 0;
                                      snap.data.Cmeals.forEach((element) {
                                        x = x + element["count"];
                                      });
                                      print('xxxxxxx:$x');
                                      return Text(
                                        "${x.round()}".replaceAll(".0", ""),
                                        style: TextStyle(
                                          color: globals.accountColorsData[
                                                      'TextOnMainColor'] !=
                                                  null
                                              ? Color(int.parse(
                                                  globals.accountColorsData[
                                                      'TextOnMainColor']))
                                              : SystemControls.TextOnMainColorz,
                                          fontSize: SystemControls.font4,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        textAlign: TextAlign.center,
                                      );
                                    } else {
                                      return Text(
                                        "0",
                                        style: TextStyle(
                                          color: globals.accountColorsData[
                                                      'TextOnMainColor'] !=
                                                  null
                                              ? Color(int.parse(
                                                  globals.accountColorsData[
                                                      'TextOnMainColor']))
                                              : SystemControls.TextOnMainColorz,
                                          fontSize: SystemControls.font4,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        textAlign: TextAlign.center,
                                      );
                                    }
                                  });
                            },
                          ),
                        ),
                      );
                    })
                  ],
                ),
                onTap: onClickCartButton,
              )),
          SizedBox(
            width: 2.w,
          ),

          Consumer2<SearchProvider, CartStateProvider>(
              builder: (context, search, cart, _) {
            return IconButton(
                onPressed: () {
                  search.controlSearchCategories(true);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SearchScreenNew(
                                cartList: cart.cartList,
                                appCurrency: appCurrency,
                                token: userToken,
                              )));
                },
                icon: Icon(
                  Icons.search,
                  size: 25,
                ));
          }),
        ],
      ),
      drawer: Consumer<CartStateProvider>(
        builder: (context, cart, _) {
          return Drawer(
            child: ModifiedDrawerWidget(
              token: userToken,
              hasRegistration: hasRegistration,
              isLogin: isLogging,
              currency: appCurrency,
              appInfo: appInitialInfo,
              drawer: toggleDrawer,
            ),
          );
        },
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          SystemControls.cartAddPhoto
              ? (Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FloatingActionButton(
                      onPressed: () {
                        showBottomSelectImgWidget(context: context);
                      },
                      heroTag: 'image0',
                      tooltip: 'Pick Image',
                      backgroundColor: globals.accountColorsData['MainColor'] !=
                              null
                          ? Color(
                              int.parse(globals.accountColorsData['MainColor']))
                          : SystemControls.MainColorz,
                      child: const Icon(
                        Icons.add_photo_alternate,
                        size: 35,
                      ),
                    ),
                    Container(
                      color: Colors.white,
                      //width: 80,
                      margin: EdgeInsets.only(top: 10),
                      child: Text(
                        'AddPicToCart'.tr().toString(),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font2,
                          height: 1.2,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    )
                  ],
                ))
              : (Container()),
        ],
      ),
      body: CustomScrollView(
        controller: _scrollController,
        physics: BouncingScrollPhysics(),
        slivers: [
          SliverToBoxAdapter(
            child: Consumer<CartStateProvider>(
              builder: (context, cart, _) {
                return FutureBuilder<CartList>(
                  future: localCart,
                  builder: (context, Cartsnap) {
                    if (Cartsnap.connectionState == ConnectionState.done &&
                        Cartsnap.hasData) {
                      cart.setCartList(Cartsnap.data.Cmeals);

                      CartStateProvider cartState =
                          Provider.of<CartStateProvider>(context,
                              listen: false);
                      if (cartState.productsCount == "0" &&
                          cart.cartList.length != 0) {
                        cartState.setCurrentProductsCount(
                            cart.cartList.length.toString());
                      }
                      return Consumer<CartStateProvider>(
                          builder: (context, cart, _) {
                        return Container();
                      });
                    }
                    return Container(
                      height: 0,
                      width: 0,
                      child: Center(),
                    );
                  },
                );
              },
            ),
          ),
          SliverToBoxAdapter(
            child: Consumer<HomeProvider>(
              builder: (context, homeProvider, _) {
                return homeProvider.selectedCategory == -2
                    ? FutureBuilder<OfferModelModel>(
                        future:
                            GetOfferApiRepo(context: context).getOfferApiData,
                        builder: (context, snap) {
                          if (snap.hasData) {
                            snap.data.data.offers.removeWhere(
                                (element) => element.offersPrice == "0");
                            return ListView.builder(
                              shrinkWrap: true,
                              physics: ScrollPhysics(),
                              itemCount: snap.data.data.offers.length,
                              itemBuilder: (context, index) {
                                return InkWell(
                                  onTap: (){
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=>OfferScreen(offer: snap.data.data.offers[index],token: userToken,appCurrency: appCurrency,)));
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Container(
                                      height: 20.h,
                                      width: 100.w,
                                      child: CachedNetworkImage(
                                          imageUrl: SystemControls().GetImgPath(
                                              snap.data.data.offers[index]
                                                  .offersImgMobile,
                                              "original"),
                                          fit: BoxFit.fill,
                                          height: 20.h,
                                          width: 100.w,
                                          errorWidget: (context, url, error) =>
                                              Icon(Icons.error)),
                                    ),
                                  ),
                                );
                              },
                            );
                          } else {
                            return Container(
                              height: 30.h,
                              child: Indicator(),
                            );
                          }
                        },
                      )
                    : Container(
                        height: 0,
                        width: 0,
                      );
              },
            ),
          ),
          SliverAppBar(
            floating: true,
            pinned: true,
            forceElevated: true,
            elevation: 0,
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(15.h),
              child: FutureBuilder<ApplicationHomeModel>(
                  future: ApplicationHomeResponseRepo(context: context)
                      .getHomeApplicationItems,
                  builder: (context, snap) {
                    if (snap.hasData) {
                      categories = snap.data.data.categories;
                      return Column(
                        children: [
                          Consumer<HomeProvider>(
                              builder: (context, homeProvider, _) {
                            return Container(
                              height: 15.h,
                              width: 100.w,
                              child: ListView(
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      homeProvider.productList.clear();
                                      // setState(() {});
                                      homeProvider.changeIndex(-1, -1);

                                      homeProvider.fetchDiscountProducts(
                                          context,
                                          "${homeProvider.currentPage}");
                                    },
                                    child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Container(
                                            padding: const EdgeInsets.only(
                                                top: 8.0, bottom: 8.0),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                    bottomLeft:
                                                        Radius.circular(10),
                                                    bottomRight:
                                                        Radius.circular(10)),
                                                border: Border.all(
                                                    color: homeProvider
                                                                .selectedCategory ==
                                                            -1
                                                        ? globals.accountColorsData[
                                                                    'MainColor'] !=
                                                                null
                                                            ? Color(int.parse(
                                                                globals.accountColorsData[
                                                                    'MainColor']))
                                                            : SystemControls
                                                                .MainColorz
                                                        : Colors.transparent,
                                                    width: 2)),
                                            child: Column(
                                              children: [
                                                SystemControls()
                                                    .GetSVGImagesAsset(
                                                        'assets/Icons/offer.svg',
                                                        6.h,
                                                        Colors.black),
                                                Spacer(),
                                                Container(
                                                    decoration: BoxDecoration(
                                                        border: Border(
                                                            top: BorderSide(
                                                                color: homeProvider
                                                                            .selectedCategory ==
                                                                        -1
                                                                    ? globals.accountColorsData['MainColor'] !=
                                                                            null
                                                                        ? Color(int.parse(globals.accountColorsData[
                                                                            'MainColor']))
                                                                        : SystemControls
                                                                            .MainColorz
                                                                    : Colors
                                                                        .transparent,
                                                                width: 2))),
                                                    width: 30.w,
                                                    height: 4.h,
                                                    child: Center(
                                                        child: Text(
                                                      "offers".tr().toString(),
                                                      style: TextStyle(
                                                          color: globals.accountColorsData[
                                                                      'MainColor'] !=
                                                                  null
                                                              ? Color(int.parse(
                                                                  globals.accountColorsData[
                                                                      'MainColor']))
                                                              : SystemControls
                                                                  .MainColorz),
                                                    ))),
                                              ],
                                            ))),
                                  ),
                                  ListView.builder(
                                      shrinkWrap: true,
                                      physics: ScrollPhysics(),
                                      scrollDirection: Axis.horizontal,
                                      itemCount:
                                          snap.data.data.categories.length,
                                      itemBuilder: (context, index) {
                                        return InkWell(
                                          onTap: () {
                                            homeProvider.productList.clear();
                                            homeProvider.changeIndex(index, 0);
                                            homeProvider.getCategoryId(snap
                                                .data
                                                .data
                                                .categories[index]
                                                .categoriesId);
                                            homeProvider.fetchProductById(
                                                context,
                                                homeProvider.categoryId,
                                                homeProvider.currentPage);
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.only(
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  10),
                                                          bottomRight:
                                                              Radius.circular(
                                                                  10)),
                                                  border: Border.all(
                                                      color: homeProvider
                                                                  .selectedCategory ==
                                                              index
                                                          ? globals.accountColorsData[
                                                                      'MainColor'] !=
                                                                  null
                                                              ? Color(int.parse(
                                                                  globals.accountColorsData[
                                                                      'MainColor']))
                                                              : SystemControls
                                                                  .MainColorz
                                                          : Colors.transparent,
                                                      width: 2)),
                                              child: Column(
                                                children: [
                                                  Spacer(),
                                                  Center(
                                                    child: CachedNetworkImage(
                                                      imageUrl: SystemControls()
                                                          .GetImgPath(
                                                              snap
                                                                  .data
                                                                  .data
                                                                  .categories[
                                                                      index]
                                                                  .categoriesImg
                                                                  .toString(),
                                                              "original"),
                                                      placeholder:
                                                          (context, url) =>
                                                              Center(
                                                        child: SystemControls()
                                                            .circularProgress(),
                                                      ),
                                                      errorWidget: (context,
                                                              url, error) =>
                                                          Center(
                                                        child: Icon(
                                                            Icons.broken_image),
                                                      ),
                                                      fit: BoxFit.contain,
                                                      width: 30.w,
                                                      height: 6.h,
                                                    ),
                                                  ),
                                                  Spacer(),
                                                  Container(
                                                      decoration: BoxDecoration(
                                                          border: Border(
                                                              top: BorderSide(
                                                                  color: homeProvider
                                                                              .selectedCategory ==
                                                                          index
                                                                      ? globals.accountColorsData['MainColor'] !=
                                                                              null
                                                                          ? Color(int.parse(globals.accountColorsData[
                                                                              'MainColor']))
                                                                          : SystemControls
                                                                              .MainColorz
                                                                      : Colors
                                                                          .transparent,
                                                                  width: 2))),
                                                      width: 30.w,
                                                      height: 5.h,
                                                      child: Center(
                                                          child: Text(
                                                        "${snap.data.data.categories[index].categoriesTitle}",
                                                        style: TextStyle(
                                                            color: globals.accountColorsData[
                                                                        'MainColor'] !=
                                                                    null
                                                                ? Color(int.parse(
                                                                    globals.accountColorsData[
                                                                        'MainColor']))
                                                                : SystemControls
                                                                    .MainColorz),
                                                      ))),
                                                ],
                                              ),
                                            ),
                                          ),
                                        );
                                      }),
                                ],
                              ),
                            );
                          })
                        ],
                      );
                    } else {
                      return Container(
                        height: 100.h,
                        child: Indicator(),
                      );
                    }
                  }),
            ),
            expandedHeight: 15.h,
          ),
          SliverToBoxAdapter(
            child: Consumer<CartStateProvider>(
              builder: (context, cart, _) {
                return DisplayProductWidget(
                  cartList: cart.cartList,
                  token: userToken,
                  appCurrency: appCurrency,
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
