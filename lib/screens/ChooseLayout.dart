import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import '../Controls.dart';

import 'package:easy_localization/easy_localization.dart';
import '../screens/forgetPassword.dart';
import '../screens/home.dart';

import '../dataControl/userModule.dart';
import '../locale/share_preferences_con.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';

import '../globals.dart' as globals;

class ChooseLayout extends StatelessWidget with SharePreferenceData{

  String Lang;
  int design_Control;
  ChooseLayout(this.Lang,this.design_Control);

  TextEditingController emailTextField = TextEditingController();
  TextEditingController passwordTextField = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: globals.accountColorsData['MainColor']!=null
                  ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
              image: DecorationImage(
                  image: AssetImage("assets/SplashBG.png",),
                  fit: BoxFit.cover
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: mediaQuery.size.height*0.2),
            height: 150,
            child: Center(
              child: Image.asset(Lang =="ar"? 'assets/logo.png':"assets/logoEn.png"),
            ),
          ),
          Container(
            margin: EdgeInsets.all(20),
            alignment: Alignment.bottomCenter,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 20),
                  height: 60,
                  decoration: BoxDecoration(
                    color: globals.accountColorsData['MainColor']!=null
                        ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                    borderRadius: BorderRadius.all(Radius.circular(
                      SystemControls.RadiusCircValue
                    )),
                  ),
                  child: FlatButton(
                    onPressed: ()async{},
                    child: Center(
                      child: Text(
                     "delivery".tr().toString(),
                        style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.bold,
                            fontSize: SystemControls.font2,
                            color: Colors.black
                        ),
                      ),
                    ),
                  ),
                ), //Delivery
                Container(
                  margin: EdgeInsets.only(top: 20),
                  height: 60,
                  decoration: BoxDecoration(
                    color: globals.accountColorsData['MainColor']!=null
                        ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                    borderRadius: BorderRadius.all(Radius.circular(
                        SystemControls.RadiusCircValue
                    )),
                  ),
                  child: FlatButton(
                    onPressed: ()async{},
                    child: Center(
                      child: Text(
                        "fromBranch".tr().toString(),
                        style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.bold,
                            fontSize: SystemControls.font2,
                            color: globals.accountColorsData['TextOnMainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
                        ),
                      ),
                    ),
                  ),
                ), //FromBranch
                Container(
                  margin: EdgeInsets.only(top: mediaQuery.size.height*0.1),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}