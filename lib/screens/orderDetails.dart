import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:sizer/sizer.dart';

import '../Controls.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';

import 'package:easy_localization/easy_localization.dart';
import '../dataControl/cartModule.dart';
import '../widgets/UI_Widgets_InputField.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../screens/home.dart';
import '../screens/cart1Items.dart';
import '../dataControl/userModule.dart';

import 'package:provider/provider.dart';
import '../provider/productsInCartProvider.dart';

import '../globals.dart' as globals;


class OrderDetails extends StatelessWidget {
  List<dynamic> _OrderItems = <dynamic>[];
  String Lang;
  String AppCurrency;
  dynamic order;
  int design_Control;
  String UserToken;
  List<dynamic> _Ordercontacts = <dynamic>[];

  int selectedTab = 0;
  OrderDetails(this.Lang, this.AppCurrency, this.order, this.design_Control,
      this.UserToken, this.selectedTab) {
    _OrderItems = order['items'];
    print(json.encode(order));
    _Ordercontacts = order['contacts'];
    showTime = GetTimerUICalcu();
    if (order['orders_follow_delivery_time'] == "0") {
      orderFollowDeliveryTime = false;
    } else {
      orderFollowDeliveryTime = true;
    }
  }
  //TODO Get it from Apis
  bool orderFollowDeliveryTime;
  bool timerNega = false;
  var showTime ;
  int passed ;
  GetTimerUICalcu() {
    var ZoneDifference = new DateTime.now().timeZoneOffset.inMinutes;
    var now = new DateTime.now();
    var createdTime = DateTime.parse(order['orders_created_at']);

    print("ZoneDifference");
    print("$ZoneDifference");
    print("ZoneDifference");
    print("createdTime.toLocal()");
    print(createdTime.toLocal().add(Duration(minutes: 10)));
    print("createdTime.toLocal()");
    var deliveryTime = globals.acount_delivery_time;
    Duration duration = now.difference(createdTime);
    print('deliveryTime');
    print('$deliveryTime');
    print('deliveryTime');
    print('duration');
    print('${duration.inMinutes}');
    print('duration');

    passed = deliveryTime - duration.inMinutes;
    int diffrence = duration.inMinutes - deliveryTime;
    //TODO check if order follow delivery time or not
    if (orderFollowDeliveryTime == false ||
        deliveryTime == 0 ||
        deliveryTime == null) {
      return diffrence;
    } else {
      int output = deliveryTime - duration.inMinutes;
      if (output <= 0) {
        //Case start order from x
        timerNega = true;
        return  DateFormat.jm().format(createdTime.toLocal().add(Duration(minutes: 10))) ;
      } else {
        return output;
      }
    }
  }

  Widget StatusIconUI(int Current) {
    int status = int.parse(order['orders_status']);
    if (status == 0) {
      return SystemControls()
          .GetSVGImagesAsset('assets/Icons/check_gray.svg', 40, null);
    } else if (Current <= status) {
      return SystemControls()
          .GetSVGImagesAsset('assets/Icons/check_green.svg', 40, null);
    } else {
      return SystemControls()
          .GetSVGImagesAsset('assets/Icons/check_gray.svg', 40, null);
    }
    /*switch(order['orders_status']){
        case "1":
          if(Current=="1"){
            return Icon(
              Icons.check_circle,
              size: 40,
              color: Color.fromRGBO(43,174,136, 1),
            );
          }
          break;
        case "2":
          if(Current=="2"||Current=="1"){
            return SystemControls().GetSVGImagesAsset(
                'assets/Icons/check_green.svg',
                40,
                null);
            return Icon(
              Icons.check_circle,
              size: 40,
              color: Color.fromRGBO(43,174,136, 1),
            );
          }
          break;
          case "2":
            //if(Current=="3"){
              return SystemControls().GetSVGImagesAsset(
                  'assets/Icons/check_green.svg',
                  40,
                  null);
            //}
            break;
            return Icon(
              Icons.check_circle,
              size: 40,
              color: Color.fromRGBO(43,174,136, 1),
            );
          case "1":
            //if(Current=="4"){
              return SystemControls().GetSVGImagesAsset(
                  'assets/Icons/check_green.svg',
                  40,
                  null);
            //}
            break;

            return Icon(
              Icons.check_circle,
              size: 40,
              color: Color.fromRGBO(43,174,136, 1),
            );
      }
      return SystemControls().GetSVGImagesAsset(
          'assets/Icons/check_gray.svg',
          40,
          null);*/
  }

  List<dynamic> _CartMeals = <dynamic>[];
  addOrderToCarty() {
    _CartMeals.clear();
    for (int i = 0; i < _OrderItems.length; i++) {
      dynamic AddMeal = CartMeal();
      if (_OrderItems[i]['FK_products_id'] == null) {
        dynamic offer = _OrderItems[i]['offer'];
        AddMeal.meals_id = offer['offers_id'];
        AddMeal.meals_title = offer['offers_title'];
        AddMeal.meals_img = offer['offers_img_mobile'];
        AddMeal.meals_desc = offer['offers_desc'];
        AddMeal.meals_price = offer['offers_price'].toString();
        AddMeal.follow_delivery_time = "1";
        AddMeal.type = "offer";
        AddMeal.count = _OrderItems[i]['orders_items_count'];
      } else {
        dynamic meal = _OrderItems[i]['product'];
        AddMeal.quantityType =
        meal['products_type']; //Product follow weight or num
        AddMeal.minQuantity = meal['products_lowest_weight_available'];

        AddMeal.meals_id = meal['products_id'];
        AddMeal.choice_text = "";
        AddMeal.choice_id =_OrderItems[i]["choice"] != null ? _OrderItems[i]["choice"]['choices_id']:null;
        AddMeal.meals_title = meal['products_title'];
        AddMeal.meals_img = meal['products_img'];
        AddMeal.meals_desc = meal['products_desc'];
        AddMeal.meals_price = meal['products_price'].toString();
        AddMeal.follow_delivery_time = order[
        'orders_follow_delivery_time']; //meal['category']['categories_follow_delivery_time'];
        AddMeal.type = "product";
        AddMeal.count = double.parse(_OrderItems[i]['orders_items_count']);
      }
      dynamic obj = AddMeal.toJson();
      _CartMeals.add(obj);
    }
  }

  Color notSelectColor = Color.fromRGBO(142, 142, 147, 1);
  Color SelectColor = Color.fromRGBO(118, 118, 120, 1);

  bool open = true;

  //TabController _tabController = new TabController(length: 2);

  Widget build(BuildContext context) {
    Timer(Duration(seconds: 2), () {
      if (order['orders_status'] == "4" &&
          _Ordercontacts.length <= 0 &&
          open == true) {
        open = false;
        _opinionAlert(context);
      }
    });

    _OrderItems.forEach((element) {
      print(element);
    });
    return WillPopScope(
      child: DefaultTabController(
        initialIndex: selectedTab,
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text("ordernum".tr().toString() +
                " " +
                order['orders_id'].toString()),
            backgroundColor: globals.accountColorsData['AppbarBGColor'] !=
                null
                ? Color(int.parse(globals.accountColorsData['AppbarBGColor']))
                : SystemControls.AppbarBGColorz,
            flexibleSpace: AppBarElementsflexibleSpace(design_Control),
            bottom: PreferredSize(
              preferredSize: const Size.fromHeight(50),
              child: Container(
                color: notSelectColor,
                child: TabBar(
                  unselectedLabelColor: Colors.white,
                  labelColor: globals.accountColorsData['MainColor'] != null
                      ? Color(
                      int.parse(globals.accountColorsData['MainColor']))
                      : SystemControls.MainColorz,
                  unselectedLabelStyle: TextStyle(
                    fontFamily: Lang,
                    fontSize: SystemControls.font3,
                    fontWeight: FontWeight.normal,
                    height: 1,
                  ),
                  labelStyle: TextStyle(
                    fontFamily: Lang,
                    fontSize: SystemControls.font2,
                    fontWeight: FontWeight.bold,
                    height: 1,
                  ),
                  indicatorSize: TabBarIndicatorSize.tab,
                  indicator: BoxDecoration(
                      color: SelectColor, //Selected
                      border: Border(
                        bottom: BorderSide(
                          color:
                          globals.accountColorsData['MainColor'] != null
                              ? Color(int.parse(
                              globals.accountColorsData['MainColor']))
                              : SystemControls.MainColorz,
                          width: 4.0,
                        ),
                      )),
                  tabs: <Widget>[
                    Tab(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text("orderSummary".tr().toString()),
                      ),
                    ),
                    Tab(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text("orserStatus".tr().toString()),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          body: TabBarView(
            children: <Widget>[
              OrderSummaryLayout(context),
              OrderStatusLayout(context),
            ],
          ),
        ),
      ),
      onWillPop: () {
        print("button back meal details");
        Navigator.pop(context, true);
      },
    );
  }

  //TODO need to read about it more
  double textFactor = 1.1;
  Widget OrderSummaryLayout(BuildContext context) {

    _OrderItems.forEach((element) {
      print(element);
    });
    final mediaQuery = MediaQuery.of(context);
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        LayoutBG(design_Control),
        Container(
          margin: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 150),
          child: ListView(
            children: <Widget>[
              SystemControls.cartAddPhoto
                  ? Container(
                margin: EdgeInsets.only(top: 0),
                child: Text(
                    'AddPicToCart'.tr().toString(),
                    style: TextStyle(
                      fontFamily: Lang,
                      fontWeight: FontWeight.bold,
                      fontSize: SystemControls.font3,
                      color:
                      globals.accountColorsData['TextHeaderColor'] !=
                          null
                          ? Color(int.parse(globals
                          .accountColorsData['TextHeaderColor']))
                          : SystemControls.TextHeaderColorz,
                      height: 1.2,
                    )),
              )
                  : Container(),
              SystemControls.cartAddPhoto
                  ? (order['orders_img'] != null
                  ? Container(
                //width: 120,
                height: 120,
                margin: EdgeInsets.only(top: 10),
                child: CachedNetworkImage(
                  imageUrl: SystemControls()
                      .GetImgPath(order['orders_img'], "medium"),
                  placeholder: (context, url) => Center(
                    child: SystemControls().circularProgress(),
                  ),
                  errorWidget: (context, url, error) => Center(
                    child: Icon(Icons.broken_image),
                  ),
                  fit: BoxFit.contain,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                ), /*
                              child: Image.network(
                                SystemControls().GetImgPath(item_Img,"medium"),
                                fit: BoxFit.contain,alignment: Alignment.center,),*/
              )
                  : Container(
                padding: EdgeInsets.all(10),
                child: Center(
                  child: Text(
                    'emptyListOrderDetails'.tr().toString(),
                    style: TextStyle(
                      fontFamily: Lang,
                      fontWeight: FontWeight.normal,
                      fontSize: SystemControls.font2,
                      height: 1.2,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ))
                  : Container(),
              Container(
                padding: EdgeInsets.only(bottom: 10, top: 10),
                child: Text(
                    "cartProductsTitle".tr().toString(),
                    style: TextStyle(
                      fontFamily: Lang,
                      fontWeight: FontWeight.bold,
                      fontSize: SystemControls.font3,
                      color:
                      globals.accountColorsData['TextHeaderColor'] != null
                          ? Color(int.parse(
                          globals.accountColorsData['TextHeaderColor']))
                          : SystemControls.TextHeaderColorz,
                      height: 1.2,
                    )),
              ),
              _OrderItems.length > 0
                  ? ListView.builder(
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  itemCount: _OrderItems.length,
                  itemBuilder: (context, index) {
                    String item_Img;
                    String item_title;
                    if (_OrderItems[index]['FK_products_id'] == null) {
                      dynamic offer = _OrderItems[index]['offer'];
                      item_Img = offer['offers_img_mobile'];
                      item_title = offer['offers_title'];
                    } else {
                      dynamic meal = _OrderItems[index]['product'];
                      item_Img = meal['products_img'];
                      item_title = meal['products_title'];
                    }
                    return InkWell(
                      child: Container(
                        height: 100,
                        decoration: BoxDecoration(
                          color: Colors
                              .white, //Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.all(Radius.circular(
                              SystemControls.RadiusCircValue)),
                        ),
                        margin: EdgeInsets.all(5),
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(
                              SystemControls.RadiusCircValue)),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: Container(
                                  child: CachedNetworkImage(
                                    imageUrl: SystemControls().GetImgPath(
                                        item_Img.toString(), "medium"),
                                    placeholder: (context, url) => Center(
                                      child: SystemControls()
                                          .circularProgress(),
                                    ),
                                    errorWidget: (context, url, error) =>
                                        Center(
                                          child: Icon(Icons.broken_image),
                                        ),
                                    fit: BoxFit.contain,
                                    width:
                                    MediaQuery.of(context).size.width,
                                    height:
                                    MediaQuery.of(context).size.height,
                                  ),
                                  /*
                              child: Image.network(
                                SystemControls().GetImgPath(item_Img,"medium"),
                                fit: BoxFit.contain,alignment: Alignment.center,),*/
                                ),
                              ),
                              Padding(padding: EdgeInsets.all(5)),
                              Expanded(
                                flex: 2,
                                child: Container(
                                    child: Column(
                                      mainAxisAlignment:
                                      MainAxisAlignment.center,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          item_title,
                                          style: TextStyle(
                                            fontFamily: Lang,
                                            fontWeight: FontWeight.bold,
                                            fontSize: SystemControls.font4,
                                            height: 1.2,
                                          ),
                                          textAlign: TextAlign.start,
                                          maxLines: 2,
                                        ),
                                        Padding(
                                            padding: EdgeInsets.only(top: 5)),
                                        Row(
                                          children: [
                                            Text(
                                              _OrderItems[index]
                                              ['orders_items_price'] +
                                                  " " +
                                                  AppCurrency,
                                              style: TextStyle(
                                                fontFamily: Lang,
                                                fontWeight: FontWeight.normal,
                                                fontSize: SystemControls.font4,
                                                height: 1.2,
                                              ),
                                              textAlign: TextAlign.center,
                                              maxLines: 2,
                                            ),
                                            SizedBox(width: 20,),
                                            _OrderItems[index]
                                            ['orders_items_price_before_sale']!= null ? Text(
                                              "${double.parse("${ _OrderItems[index]
                                              ['orders_items_price_before_sale'] }")}"+
                                                  " " +
                                                  AppCurrency,
                                              style: TextStyle(
                                                color: Colors.grey,
                                                decoration: TextDecoration.lineThrough,
                                                fontFamily: Lang,
                                                fontWeight: FontWeight.normal,
                                                fontSize: SystemControls.font4,
                                                height: 1.2,
                                              ),
                                              textAlign: TextAlign.center,
                                              maxLines: 2,
                                            ):Container(),
                                          ],
                                        ),
                                        Padding(
                                            padding: EdgeInsets.only(top: 5)),
                                        _OrderItems[index]
                                        ['product'] != null ?    RichText(
                                          textAlign: TextAlign.start,
                                          text: new TextSpan(
                                            children: <TextSpan>[
                                              TextSpan(
                                                text: _OrderItems[index]
                                                ['product']
                                                ['products_type'] ==
                                                    'weight'
                                                    ?
                                                    "itemsCountWeight".tr().toString()
                                                    :
                                                    "itemsCount".tr().toString(),
                                                style: TextStyle(
                                                  color: globals.accountColorsData[
                                                  'TextHeaderColor'] !=
                                                      null
                                                      ? Color(int.parse(globals
                                                      .accountColorsData[
                                                  'TextHeaderColor']))
                                                      : SystemControls
                                                      .TextHeaderColorz,
                                                  fontFamily: Lang,
                                                  fontWeight: FontWeight.normal,
                                                  fontSize:
                                                  SystemControls.font4,
                                                  height: 1.2,
                                                ),
                                              ),
                                              TextSpan(
                                                text: "  ",
                                                style: TextStyle(
                                                  fontFamily: Lang,
                                                  fontWeight: FontWeight.normal,
                                                  height: 1.2,
                                                ),
                                              ),
                                              _OrderItems[index][
                                              'customer_request'] !=
                                                  null
                                                  ? TextSpan(
                                                text: _OrderItems[index]
                                                ['product']
                                                [
                                                'products_type'] ==
                                                    'weight'
                                                    ? _OrderItems[index]
                                                ['customer_request']
                                                [
                                                'orders_items_changes_count']
                                                    .toString() +
                                                    "**"
                                                    : (double.parse(_OrderItems[
                                                index]
                                                ['customer_request']
                                                ['orders_items_changes_count']))
                                                    .toInt()
                                                    .toString(),
                                                //text: _OrderItems[index]['customer_request']['orders_items_changes_count'].toString(),
                                                style: TextStyle(
                                                  color: globals.accountColorsData[
                                                  'TextHeaderColor'] !=
                                                      null
                                                      ? Color(int.parse(globals
                                                      .accountColorsData[
                                                  'TextHeaderColor']))
                                                      : SystemControls
                                                      .TextHeaderColorz,
                                                  fontFamily: Lang,
                                                  fontWeight:
                                                  FontWeight.normal,
                                                  fontSize: SystemControls
                                                      .font4,
                                                  height: 1.2,
                                                  decoration:
                                                  TextDecoration
                                                      .lineThrough,
                                                ),
                                              )
                                                  : TextSpan(
                                                text: "",
                                                style: TextStyle(
                                                  fontFamily: Lang,
                                                  fontWeight:
                                                  FontWeight.normal,
                                                  fontSize: SystemControls
                                                      .font4,
                                                  height: 1.2,
                                                ),
                                              ),
                                              TextSpan(
                                                text: _OrderItems[index]
                                                ['product']
                                                ['products_type'] ==
                                                    'weight'
                                                    ? "  " +
                                                    _OrderItems[index][
                                                    'orders_items_count']
                                                        .toString() +
                                                    "**"
                                                    : "  " +
                                                    (double.parse(_OrderItems[
                                                    index][
                                                    'orders_items_count']))
                                                        .toInt()
                                                        .toString(),
                                                //text: "  "+_OrderItems[index]['orders_items_count'].toString(),
                                                style: TextStyle(
                                                  color: globals.accountColorsData[
                                                  'TextHeaderColor'] !=
                                                      null
                                                      ? Color(int.parse(globals
                                                      .accountColorsData[
                                                  'TextHeaderColor']))
                                                      : SystemControls
                                                      .TextHeaderColorz,
                                                  fontFamily: Lang,
                                                  fontWeight: FontWeight.normal,
                                                  fontSize:
                                                  SystemControls.font4,
                                                  height: 1.2,
                                                  decoration:
                                                  TextDecoration.none,
                                                ),
                                              ),

                                            ],
                                          ),
                                          maxLines: 1,
                                        ):Container(),
                                        SizedBox(height: 2,),
                                        _OrderItems[index]["choice"]!= null ?    Text(
                                            "${"size".tr().toString()}:  ${_OrderItems[index]["choice"][
                                            'choices_title']}",
                                          style: TextStyle(
                                            color: globals.accountColorsData[
                                            'TextHeaderColor'] !=
                                                null
                                                ? Color(int.parse(globals
                                                .accountColorsData[
                                            'TextHeaderColor']))
                                                : SystemControls
                                                .TextHeaderColorz,
                                            fontFamily: Lang,
                                            fontWeight: FontWeight.normal,
                                            fontSize:
                                            SystemControls.font4,
                                            height: 1.2,
                                            decoration:
                                            TextDecoration.none,
                                          ),
                                        ): Container(),
                                        SizedBox( height: 3,),



                                      ],
                                    ),),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  })
                  : (Container(
                padding: EdgeInsets.all(20),
                child: Center(
                  child: Text(
                    'emptyListOrderDetails'.tr().toString(),
                    style: TextStyle(
                      fontFamily: Lang,
                      fontWeight: FontWeight.normal,
                      fontSize: SystemControls.font2,
                      height: 1.2,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              )),

              SizedBox(height: 10.h,)
            ],
          ),
        ),
        Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Container(
              color: globals.accountColorsData['BGColor'] != null
                  ? Color(int.parse(globals.accountColorsData['BGColor']))
                  : SystemControls.BGColorz,
              alignment: Alignment.bottomCenter,
              height: 200,

              width: mediaQuery.size.width,
            ),
            Container(
              width: mediaQuery.size.width * 0.9,
              margin: EdgeInsets.only(right: 10, left: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    height: 1,
                    color: globals.accountColorsData['TextdetailsColor'] != null
                        ? Color(int.parse(
                        globals.accountColorsData['TextdetailsColor']))
                        : SystemControls.TextdetailsColorz,
                  ),
                  Padding(padding: EdgeInsets.all(3)),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Text("itemsPrice".tr().toString(),
                            style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData[
                              'TextHeaderColor'] !=
                                  null
                                  ? Color(int.parse(globals
                                  .accountColorsData['TextHeaderColor']))
                                  : SystemControls.TextHeaderColorz,
                              height: 1.2,
                            )),
                      ),
                      Expanded(
                        flex: 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Text(
                                order['orders_price'].toString() +
                                    " " +
                                    AppCurrency,
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.normal,
                                  fontSize: SystemControls.font3,
                                  color: globals.accountColorsData[
                                  'TextHeaderColor'] !=
                                      null
                                      ? Color(int.parse(
                                      globals.accountColorsData[
                                      'TextHeaderColor']))
                                      : SystemControls.TextHeaderColorz,
                                  height: 1.2,
                                ))
                          ],
                        ),
                      )
                    ],
                  ),
                  order['coupons_value'] != null ?   Padding(padding: EdgeInsets.all(3)):Container(),
                  order['coupons_value'] != null ?     Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Text("theCouponDiscount".tr().toString(),
                            style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData[
                              'TextHeaderColor'] !=
                                  null
                                  ? Color(int.parse(globals
                                  .accountColorsData['TextHeaderColor']))
                                  : SystemControls.TextHeaderColorz,
                              height: 1.2,
                            )),
                      ),
                      Expanded(
                        flex: 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            order['coupons_type'] =="rate" ? Text(
                                order['coupons_value'].toString() +
                                    "  %",
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.normal,
                                  fontSize: SystemControls.font3,
                                  color: globals.accountColorsData[
                                  'TextHeaderColor'] !=
                                      null
                                      ? Color(int.parse(
                                      globals.accountColorsData[
                                      'TextHeaderColor']))
                                      : SystemControls.TextHeaderColorz,
                                  height: 1.2,
                                )):   Text(
                                order['coupons_value'].toString() +
                                    " " +
                                    AppCurrency,
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.normal,
                                  fontSize: SystemControls.font3,
                                  color: globals.accountColorsData[
                                  'TextHeaderColor'] !=
                                      null
                                      ? Color(int.parse(
                                      globals.accountColorsData[
                                      'TextHeaderColor']))
                                      : SystemControls.TextHeaderColorz,
                                  height: 1.2,
                                ))
                          ],
                        ),
                      )
                    ],
                  ):Container(),
                  Padding(padding: EdgeInsets.all(3)),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Text(
                            "deliveryFees".tr().toString(),
                            style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData[
                              'TextHeaderColor'] !=
                                  null
                                  ? Color(int.parse(globals
                                  .accountColorsData['TextHeaderColor']))
                                  : SystemControls.TextHeaderColorz,
                              height: 1.2,
                            )),
                      ),
                      Expanded(
                        flex: 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Text(
                                order['orders_delivery_cost'].toString() +
                                    " " +
                                    AppCurrency,
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.normal,
                                  fontSize: SystemControls.font3,
                                  color: globals.accountColorsData[
                                  'TextHeaderColor'] !=
                                      null
                                      ? Color(int.parse(
                                      globals.accountColorsData[
                                      'TextHeaderColor']))
                                      : SystemControls.TextHeaderColorz,
                                  height: 1.2,
                                ))
                          ],
                        ),
                      )
                    ],
                  ),
                  Container(
                    height: 1,
                    color: globals.accountColorsData['TextdetailsColor'] != null
                        ? Color(int.parse(
                        globals.accountColorsData['TextdetailsColor']))
                        : SystemControls.TextdetailsColorz,
                  ),
                  Padding(padding: EdgeInsets.all(3)),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Text(
                          "TotalPrice".tr().toString(),
                            style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData[
                              'TextHeaderColor'] !=
                                  null
                                  ? Color(int.parse(globals
                                  .accountColorsData['TextHeaderColor']))
                                  : SystemControls.TextHeaderColorz,
                              height: 1.2,
                            )),
                      ),
                      Expanded(
                        flex: 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Text(
                                order['orders_final_cost'].toString() +
                                    " " +
                                    AppCurrency,
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.normal,
                                  fontSize: SystemControls.font3,
                                  color: globals.accountColorsData[
                                  'TextHeaderColor'] !=
                                      null
                                      ? Color(int.parse(
                                      globals.accountColorsData[
                                      'TextHeaderColor']))
                                      : SystemControls.TextHeaderColorz,
                                  height: 1.2,
                                ))
                          ],
                        ),
                      )
                    ],
                  ),
                  Padding(padding: EdgeInsets.all(6)),
                  Container(
                    width: mediaQuery.size.width,
                    height: 50,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        color: globals.accountColorsData['MainColor'] != null
                            ? Color(int.parse(
                            globals.accountColorsData['MainColor']))
                            : SystemControls.MainColorz),
                    child: FlatButton(
                      onPressed: () async {
                        _ConfirmDialogAlert(context);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.repeat,
                            color:
                            globals.accountColorsData['TextOnMainColor'] !=
                                null
                                ? Color(int.parse(globals
                                .accountColorsData['TextOnMainColor']))
                                : SystemControls.TextOnMainColorz,
                            size: 20,
                          ),
                          Padding(padding: EdgeInsets.all(5)),
                          Text("reorder".tr().toString(),
                              style: TextStyle(
                                fontFamily: Lang,
                                fontWeight: FontWeight.normal,
                                fontSize: SystemControls.font3,
                                color: globals.accountColorsData[
                                'TextOnMainColor'] !=
                                    null
                                    ? Color(int.parse(globals
                                    .accountColorsData['TextOnMainColor']))
                                    : SystemControls.TextOnMainColorz,
                                height: 1,
                              )),
                        ],
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.all(10)),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget OrderStatusLayout(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        LayoutBG(design_Control),
        Container(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              SystemControls.branchesValid
                  ? (Container(
                margin: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 7,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Jeddah Branch",
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontSize:
                                  SystemControls.font2 * textFactor,
                                  color: globals.accountColorsData[
                                  'TextHeaderColor'] !=
                                      null
                                      ? Color(int.parse(
                                      globals.accountColorsData[
                                      'TextHeaderColor']))
                                      : SystemControls.TextHeaderColorz,
                                  fontWeight: FontWeight.bold,
                                  height: 1,
                                ),
                                maxLines: 1,
                                textAlign: TextAlign.start,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Padding(padding: EdgeInsets.all(4)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.location_on,
                          color: Colors.black,
                          size: 25,
                        ),
                        Padding(padding: EdgeInsets.all(2)),
                        Text(
                          "Address, Address, Address",
                          style: TextStyle(
                            fontFamily: Lang,
                            fontSize: SystemControls.font3 * textFactor,
                            color: globals.accountColorsData[
                            'TextdetailsColor'] !=
                                null
                                ? Color(int.parse(
                                globals.accountColorsData[
                                'TextdetailsColor']))
                                : SystemControls.TextdetailsColorz,
                            fontWeight: FontWeight.normal,
                            height: 0.9,
                          ),
                          maxLines: 1,
                          textAlign: TextAlign.start,
                        ),
                      ],
                    ),
                  ],
                ),
              ))
                  : (Container(
                height: 20,
              )),
              order['orders_status'] == "4"
                  ? (Container())
                  : (Container(
                margin: EdgeInsets.only(right: 20, left: 20, bottom: 20),
                padding: EdgeInsets.all(20),
                //height: 80,
                width: mediaQuery.size.width,
                decoration: BoxDecoration(
                    color: Color.fromRGBO(254, 236, 236, 1),
                    borderRadius: BorderRadius.all(
                        Radius.circular(SystemControls.RadiusCircValue)),
                    border: Border.all(
                        color: Color.fromRGBO(252, 210, 210, 1),
                        width: 2)),
                child: Center(
                  child: Text(
                    order['orders_status'] == "0"
                        ?"orderAlreadyCanceled".tr().toString()
                        : "cancelOrderWarning".tr().toString(),
                    style: TextStyle(
                      fontFamily: Lang,
                      fontSize: SystemControls.font3 * textFactor,
                      color: Color.fromRGBO(230, 111, 125, 1),
                      fontWeight: FontWeight.normal,
                      height: 1,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              )), //warning

              order["orders_status"]!="0" ?     Container(
                margin: EdgeInsets.only(bottom: 20),
                padding: EdgeInsets.all(20),
                color: Color.fromRGBO(254, 247, 219, 1),
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        order['orders_status'] == "4"
                            ? ("orderTimeAreaDone".tr().toString())
                            : (!orderFollowDeliveryTime
                            ? ("orderTimeAreaWait".tr().toString())
                            : (timerNega
                            ? ("orderTimeStartfrom".tr().toString())
                            :"orderTimeAreaWait".tr().toString())),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontSize: SystemControls.font3 * textFactor,
                          color: globals.accountColorsData['TextHeaderColor'] !=
                              null
                              ? Color(int.parse(
                              globals.accountColorsData['TextHeaderColor']))
                              : SystemControls.TextHeaderColorz,
                          fontWeight: FontWeight.bold,
                          height: 1,
                        ),
                        maxLines: 1,
                        textAlign: TextAlign.start,
                      ),
                      Padding(padding: EdgeInsets.all(5)),
                      orderFollowDeliveryTime || order['orders_status'] == "4"
                          ? (Align(
                        alignment: Alignment.center,
                        child: Container(
                          child: ClipRRect(
                            borderRadius:
                            BorderRadius.all(Radius.circular(50)),
                            child: Container(
                              width: 150,
                              height: 150,
                              color: globals.accountColorsData[
                              'BorderColor'] !=
                                  null
                                  ? Color(int.parse(globals
                                  .accountColorsData['BorderColor']))
                                  : SystemControls.BorderColorz,
                              padding: EdgeInsets.all(2),
                              child: ClipRRect(
                                borderRadius:
                                BorderRadius.all(Radius.circular(50)),
                                child: Container(
                                  alignment: Alignment.center,
                                  color: Colors.white,
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                    MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        order['orders_status'] == "4"
                                            ? (
                                            //if order deliverd show order total time
                                            order['orders_time_taken']
                                                .toString())
                                            : showTime.toString(),
                                        style: TextStyle(
                                          fontFamily: Lang,
                                          fontSize: SystemControls.font3 *
                                              textFactor,
                                          color: globals.accountColorsData[
                                          'TextHeaderColor'] !=
                                              null
                                              ? Color(int.parse(globals
                                              .accountColorsData[
                                          'TextHeaderColor']))
                                              : SystemControls
                                              .TextHeaderColorz,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        maxLines: 1,
                                        textAlign: TextAlign.start,
                                      ),
                                      passed >=0   ?Text(
                                        "Min",
                                        style: TextStyle(
                                          fontFamily: Lang,
                                          fontSize: SystemControls.font3 *
                                              textFactor,
                                          color: globals.accountColorsData[
                                          'TextHeaderColor'] !=
                                              null
                                              ? Color(int.parse(globals
                                              .accountColorsData[
                                          'TextHeaderColor']))
                                              : SystemControls
                                              .TextHeaderColorz,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        maxLines: 1,
                                        textAlign: TextAlign.start,
                                      ):Container(),
                                    ],
                                  ) ,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ))
                          : (Padding(
                        padding: EdgeInsets.only(left: 10, right: 10),
                        child: Text(
                          "orderTimeRestaurantMess".tr().toString(),
                          style: TextStyle(
                            fontFamily: Lang,
                            fontSize: SystemControls.font3 * textFactor,
                            color: globals.accountColorsData[
                            'TextHeaderColor'] !=
                                null
                                ? Color(int.parse(
                                globals.accountColorsData[
                                'TextHeaderColor']))
                                : SystemControls.TextHeaderColorz,
                            fontWeight: FontWeight.normal,
                            height: 1,
                          ),
                          maxLines: 3,
                          textAlign: TextAlign.center,
                        ),
                      )),
                    ],
                  ),
                ),
              ):Container(), //timer
              Container(
                margin: EdgeInsets.all(40),
                //height: 370,
                child: Stack(
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                            width: 2,
                            height: 300,
                            margin: EdgeInsets.only(
                              //bottom: 60,
                                top: 30,
                                right: 20,
                                left: 20),
                            color: Colors.black54 //SystemControls.BorderColor,
                        ),
                        /*
                        Container(
                            width: 2,
                            height: 72,
                            margin: EdgeInsets.only(
                              //bottom: 60,
                                top: 30,
                                right: 20,
                                left: 20
                            ),
                            color: Colors.black54//SystemControls.BorderColor,
                        ),
                        Container(
                            width: 2,
                            height: 72,
                            margin: EdgeInsets.only(
                              //bottom: 60,
                                top: 30,
                                right: 20,
                                left: 20
                            ),
                            color: Colors.black54//SystemControls.BorderColor,
                        ),*/
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            StatusIconUI(1),
                            Padding(
                              padding: EdgeInsets.all(10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "orderNew".tr().toString(),
                                    style: TextStyle(
                                      fontFamily: Lang,
                                      fontSize:
                                      SystemControls.font3 * textFactor,
                                      color: globals.accountColorsData[
                                      'TextHeaderColor'] !=
                                          null
                                          ? Color(int.parse(
                                          globals.accountColorsData[
                                          'TextHeaderColor']))
                                          : SystemControls.TextHeaderColorz,
                                      fontWeight: FontWeight.bold,
                                      height: 1,
                                    ),
                                    maxLines: 1,
                                    textAlign: TextAlign.start,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(5),
                                  ),
                                  Text(
                                    "orderNewHint".tr().toString(),
                                    style: TextStyle(
                                      fontFamily: Lang,
                                      fontSize:
                                      SystemControls.font3 * textFactor,
                                      color: globals.accountColorsData[
                                      'TextdetailsColor'] !=
                                          null
                                          ? Color(int.parse(
                                          globals.accountColorsData[
                                          'TextdetailsColor']))
                                          : SystemControls.TextdetailsColorz,
                                      fontWeight: FontWeight.normal,
                                      height: 1,
                                    ),
                                    maxLines: 1,
                                    textAlign: TextAlign.start,
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(padding: EdgeInsets.all(20)),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            StatusIconUI(2),
                            Padding(
                              padding: EdgeInsets.all(10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "orderConfirm".tr().toString(),
                                    style: TextStyle(
                                      fontFamily: Lang,
                                      fontSize:
                                      SystemControls.font3 * textFactor,
                                      color: globals.accountColorsData[
                                      'TextHeaderColor'] !=
                                          null
                                          ? Color(int.parse(
                                          globals.accountColorsData[
                                          'TextHeaderColor']))
                                          : SystemControls.TextHeaderColorz,
                                      fontWeight: FontWeight.bold,
                                      height: 1,
                                    ),
                                    maxLines: 1,
                                    textAlign: TextAlign.start,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(5),
                                  ),
                                  Text(
                                 "orderConfirmHint".tr().toString(),
                                    style: TextStyle(
                                      fontFamily: Lang,
                                      fontSize:
                                      SystemControls.font3 * textFactor,
                                      color: globals.accountColorsData[
                                      'TextdetailsColor'] !=
                                          null
                                          ? Color(int.parse(
                                          globals.accountColorsData[
                                          'TextdetailsColor']))
                                          : SystemControls.TextdetailsColorz,
                                      fontWeight: FontWeight.normal,
                                      height: 1,
                                    ),
                                    maxLines: 1,
                                    textAlign: TextAlign.start,
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(padding: EdgeInsets.all(20)),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            StatusIconUI(3),
                            Padding(
                              padding: EdgeInsets.all(10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "orderOnWay".tr().toString(),
                                    style: TextStyle(
                                      fontFamily: Lang,
                                      fontSize:
                                      SystemControls.font3 * textFactor,
                                      color: globals.accountColorsData[
                                      'TextHeaderColor'] !=
                                          null
                                          ? Color(int.parse(
                                          globals.accountColorsData[
                                          'TextHeaderColor']))
                                          : SystemControls.TextHeaderColorz,
                                      fontWeight: FontWeight.bold,
                                      height: 1,
                                    ),
                                    maxLines: 1,
                                    textAlign: TextAlign.start,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(5),
                                  ),
                                  Text(
                                    "orderOnWayHint".tr().toString(),
                                    style: TextStyle(
                                      fontFamily: Lang,
                                      fontSize:
                                      SystemControls.font3 * textFactor,
                                      color: globals.accountColorsData[
                                      'TextdetailsColor'] !=
                                          null
                                          ? Color(int.parse(
                                          globals.accountColorsData[
                                          'TextdetailsColor']))
                                          : SystemControls.TextdetailsColorz,
                                      fontWeight: FontWeight.normal,
                                      height: 1,
                                    ),
                                    maxLines: 1,
                                    textAlign: TextAlign.start,
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding:
                              EdgeInsetsDirectional.fromSTEB(5, 0, 0, 10),
                              child: Icon(
                                Icons.motorcycle,
                                size: 40,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                        Padding(padding: EdgeInsets.all(20)),
                        Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            StatusIconUI(4),
                            Padding(
                              padding: EdgeInsets.all(10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "orderDone".tr().toString(),
                                    style: TextStyle(
                                      fontFamily: Lang,
                                      fontSize:
                                      SystemControls.font3 * textFactor,
                                      color: globals.accountColorsData[
                                      'TextHeaderColor'] !=
                                          null
                                          ? Color(int.parse(
                                          globals.accountColorsData[
                                          'TextHeaderColor']))
                                          : SystemControls.TextHeaderColorz,
                                      fontWeight: FontWeight.bold,
                                      height: 1,
                                    ),
                                    maxLines: 1,
                                    textAlign: TextAlign.start,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(5),
                                  ),
                                  Text(
                                    "orderDoneHint".tr().toString(),
                                    style: TextStyle(
                                      fontFamily: Lang,
                                      fontSize:
                                      SystemControls.font3 * textFactor,
                                      color: globals.accountColorsData[
                                      'TextdetailsColor'] !=
                                          null
                                          ? Color(int.parse(
                                          globals.accountColorsData[
                                          'TextdetailsColor']))
                                          : SystemControls.TextdetailsColorz,
                                      fontWeight: FontWeight.normal,
                                      height: 1,
                                    ),
                                    maxLines: 1,
                                    textAlign: TextAlign.start,
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    )
                  ],
                ),
              ), //order current status

              order['orders_status'] == "4" || order['orders_status'] == "0"
                  ? (Container())
                  : Container(
                  height: 50,
                  margin: EdgeInsets.only(right: 40, left: 40, bottom: 40),
                  child: MaterialButton(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    onPressed: () async {
                      //_opinionAlert(context);

                      deleteDialogAlert(context, "deleteAddress", () async {
                        if (order['orders_status'] == "0") {
                          ErrorDialogAlert(
                              context,
                              "orderAlreadyCanceled".tr().toString());
                        } else if (order['orders_status'] != "1") {
                          ErrorDialogAlert(
                              context,
                             "cancelOrderWarning".tr().toString());
                        } else {
                          var res = await cancelOrder(Lang, UserToken,
                              order['orders_id'].toString());
                          if (res != null && res.status == 200) {
                            SuccDialogAlertBackHome(
                                context, Lang, res.message);
                            var res2 = await getUserOrderById(Lang,
                                UserToken, order['orders_id'].toString());
                            if (res2 != null && res2.status == 200) {
                              dynamic orderDetails = res2.data['order'];
                              print("###### " + orderDetails.toString());
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => OrderDetails(
                                          Lang,
                                          AppCurrency,
                                          orderDetails,
                                          design_Control,
                                          UserToken,
                                          0)));
                            } else if (res2.status == 401) {
                              SystemControls()
                                  .LogoutSetUserData(context, Lang);
                              ErrorDialogAlertBackHome(
                                  context, res2.message);
                            }
                          } else if (res.status == 401) {
                            SystemControls()
                                .LogoutSetUserData(context, Lang);
                            ErrorDialogAlertBackHome(context, res.message);
                          }
                        }
                      }, Lang);
                    },
                    child: Text(
                      "cancelOrderAction".tr().toString(),
                      style: TextStyle(
                        fontFamily: Lang,
                        fontSize: SystemControls.font3 * textFactor,
                        color: globals.accountColorsData['TextOnMainColor']!=null
                            ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz

                        ,fontWeight: FontWeight.bold,
                        height: 1,
                      ),
                    ),
                    color:  globals.accountColorsData['MainColor']!=null
                        ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                  )
              ), //Cancel order

              _Ordercontacts.length > 0
                  ? (Container(
                margin: EdgeInsets.only(right: 20, left: 20, bottom: 20),
                padding: EdgeInsets.all(5),
                //height: 80,
                width: mediaQuery.size.width,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(
                        Radius.circular(SystemControls.RadiusCircValue)),
                    border: Border.all(
                        color: globals.accountColorsData['BorderColor'] !=
                            null
                            ? Color(int.parse(
                            globals.accountColorsData['BorderColor']))
                            : SystemControls.BorderColorz,
                        width: 2)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Center(
                      child: Text(
                      'opinion'.tr().toString(),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontSize: SystemControls.font2,
                          color: globals.accountColorsData[
                          'TextHeaderColor'] !=
                              null
                              ? Color(int.parse(globals
                              .accountColorsData['TextHeaderColor']))
                              : SystemControls.TextHeaderColorz,
                          fontWeight: FontWeight.bold,
                          //height: 1,
                        ),
                        //maxLines: 1,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5),
                      child: Text(
                        _Ordercontacts[0]['contacts_message'].toString(),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontSize: SystemControls.font3,
                          color: globals.accountColorsData[
                          'TextHeaderColor'] !=
                              null
                              ? Color(int.parse(globals
                              .accountColorsData['TextHeaderColor']))
                              : SystemControls.TextHeaderColorz,
                          fontWeight: FontWeight.normal,
                          //height: 1,
                        ),
                        //maxLines: 1,
                        textAlign: TextAlign.start,
                      ),
                    ),
                  ],
                ),
              ))
                  : (Container()), //User opinion
            ],
          ),
        ),
      ],
    );
  }

  Future<void> _ConfirmDialogAlert(BuildContext context) {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius:
              BorderRadius.circular(SystemControls.RadiusCircValue)),
          title: Text(
            "reorderConfirmMSG".tr().toString(),
            style: TextStyle(
                fontFamily: Lang,
                fontWeight: FontWeight.bold,
                fontSize: SystemControls.font3,
                color: globals.accountColorsData['TextHeaderColor'] != null
                    ? Color(
                    int.parse(globals.accountColorsData['TextHeaderColor']))
                    : SystemControls.TextHeaderColorz),
            textAlign: TextAlign.center,
          ),
          contentPadding: EdgeInsets.only(top: 40, bottom: 0),
          content: ClipRRect(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
              bottomRight: Radius.circular(SystemControls.RadiusCircValue),
            ),
            child: Container(
              height: 40,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Container(
                      decoration: BoxDecoration(
                        color: globals.accountColorsData['MainColor'] != null
                            ? Color(int.parse(
                            globals.accountColorsData['MainColor']))
                            : SystemControls.MainColorz,
                        border: Border(
                          top: BorderSide(color: Colors.black),
                        ),
                        //borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: FlatButton(
                        child: Text(
                          "cancel".tr().toString(),
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData[
                              'TextOnMainColor'] !=
                                  null
                                  ? Color(int.parse(globals
                                  .accountColorsData['TextOnMainColor']))
                                  : SystemControls.TextOnMainColorz),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ),
              Container(
                color: Colors.black,
                width: 1,
                height: 40,
              ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      decoration: BoxDecoration(
                        color: globals.accountColorsData['MainColor'] != null
                            ? Color(int.parse(
                            globals.accountColorsData['MainColor']))
                            : SystemControls.MainColorz,
                        border: Border(
                          top: BorderSide(color: Colors.black),
                        ),
                      ),
                      child: FlatButton(
                        child: Text(
                          "orderBut".tr().toString(),
                          style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font3,
                            color:
                            globals.accountColorsData['TextOnMainColor'] !=
                                null
                                ? Color(int.parse(globals
                                .accountColorsData['TextOnMainColor']))
                                : SystemControls.TextOnMainColorz,
                          ),
                        ),
                        onPressed: () async {
                          await clear_cart_file();
                          addOrderToCarty();
                          UpdateList(_CartMeals);

                          CartStateProvider cartState =
                          Provider.of<CartStateProvider>(context,
                              listen: false);
                          cartState.setCurrentProductsCount(
                              _CartMeals.length.toString());
                          if (order['orders_img'] != null) {
                            cartState.setCartImgName(order['orders_img']);
                          }
                          Navigator.of(context).pop();
                          SuccDialogAlertGoCart(
                              context,
                              'orderAddtoCart'.tr().toString(),
                              Lang,
                              UserToken,
                              AppCurrency,
                              design_Control);
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Future<void> _opinionAlert(BuildContext context) {
    TextEditingController messageTextField = TextEditingController();

    final FocusNode _messageFocus = FocusNode();

    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius:
              BorderRadius.circular(SystemControls.RadiusCircValue)),
          contentPadding: EdgeInsets.zero,
          titlePadding: EdgeInsets.all(5),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 5, right: 5),
                child:
                Text('opinion'.tr().toString()),
              ),
              Container(
                margin: EdgeInsets.all(0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    alignment: Alignment(0, -1),
                    height: 50,
                    width: 50,
                    child: Icon(
                      Icons.cancel,
                      color: Colors.black,
                      size: 30,
                    ),
                  ),
                ),
              )
            ],
          ),
          content: Container(
            height: 300,
            child: Center(
              child: Container(
                width: MediaQuery.of(context).size.shortestSide,
                //height: 100,
                margin: EdgeInsets.all(10),
                child: ListView(
                  //mainAxisAlignment: MainAxisAlignment.start,
                  //crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: TextField(
                        controller: messageTextField,
                        focusNode: _messageFocus,
                        decoration: InputFieldDecoration(
                        "complain".tr().toString(),
                            Lang),
                        keyboardType: TextInputType.multiline,
                        maxLines: 5,
                        style: new TextStyle(
                          fontFamily: Lang,
                        ),
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 50),
                        child: Center(
                          child: Container(
                            height: 40,
                            width: 200,
                            decoration: BoxDecoration(
                              color: globals.accountColorsData['MainColor'] !=
                                  null
                                  ? Color(int.parse(
                                  globals.accountColorsData['MainColor']))
                                  : SystemControls.MainColorz,
                              borderRadius:
                              BorderRadius.all(Radius.circular(10)),
                            ),
                            child: FlatButton(
                              onPressed: () async {
                                if (messageTextField.text.isNotEmpty) {
                                  var res = await opinionApi(
                                      Lang,
                                      "00",
                                      "0",
                                      messageTextField.text,
                                      order['orders_id']);
                                  if (res == null) {
                                    ErrorDialogAlert(
                                        context,
                                        "respError".tr().toString());
                                  } else {
                                    //print(res.message);
                                    if (res.status != null) {
                                      SuccDialogAlertBackHome(
                                          context, Lang, res.message);
                                      Timer(Duration(seconds: 2), () {
                                        // Navigator.pop(context);
                                      });
                                    } else {
                                      Navigator.pop(context);
                                      ErrorDialogAlert(context, res.message);
                                    }
                                  }
                                } else {
                                  ErrorDialogAlert(
                                      context, "AddAllData".tr().toString());
                                }
                              },
                              child: Text(
                               "send".tr().toString(),
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.normal,
                                  fontSize: SystemControls.font2,
                                  color: globals.accountColorsData[
                                  'TextOnMainColor'] !=
                                      null
                                      ? Color(int.parse(
                                      globals.accountColorsData[
                                      'TextOnMainColor']))
                                      : SystemControls.TextOnMainColorz,
                                ),
                              ),
                            ),
                          ),
                        ))
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
/*
class OrderAddOpinion extends StatelessWidget{
  String Lang;
  int design_Control;
  OrderAddOpinion(this.Lang,this.design_Control);

  TextEditingController nameTextField = TextEditingController();
  TextEditingController phoneTextField = TextEditingController();
  TextEditingController messageTextField = TextEditingController();

  final FocusNode _nameFocus = FocusNode();
  final FocusNode _messageFocus = FocusNode();
  final FocusNode _phoneFocus = FocusNode();

  Widget build(BuildContext context){
    return Directionality(
      textDirection: GetDirection(Lang),
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            LayoutBG(design_Control),
            Align(
              alignment: Alignment.center,
              child: Container(
                width: MediaQuery.of(context).size.shortestSide,
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.only(top: 15),
                child: ListView(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: TextFormField(
                        controller: nameTextField,
                        focusNode: _nameFocus,
                        decoration: InputFieldDecoration(AppLocalizations.of(context).translate("name"), Lang),
                        keyboardType: TextInputType.text,
                        onFieldSubmitted: (_){
                          _nameFocus.unfocus();
                          FocusScope.of(context).requestFocus(_phoneFocus);
                        },
                        style: new TextStyle(
                          fontFamily: Lang,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: TextFormField(
                        controller: phoneTextField,
                        focusNode: _phoneFocus,
                        decoration: InputFieldDecoration(AppLocalizations.of(context).translate("phone"), Lang),
                        keyboardType: TextInputType.phone,
                        //maxLines: 5,
                        onFieldSubmitted: (_){
                          _phoneFocus.unfocus();
                          FocusScope.of(context).requestFocus(_messageFocus);
                        },
                        style: new TextStyle(
                          fontFamily: Lang,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: TextFormField(
                        controller: messageTextField,
                        focusNode: _messageFocus,
                        decoration: InputFieldDecoration(AppLocalizations.of(context).translate("complain"), Lang),
                        keyboardType: TextInputType.multiline,
                        maxLines: 5,
                        onFieldSubmitted: (_){
                          _messageFocus.unfocus();
                        },
                        style: new TextStyle(
                          fontFamily: Lang,
                        ),
                      ),
                    ),

                    Container(
                        margin: EdgeInsets.only(top: 50),
                        child: Center(
                          child: Container(
                            height: 40,
                            width: 200,
                            decoration: BoxDecoration(
                              color: globals.accountColorsData['MainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                            ),
                            child: FlatButton(
                              onPressed: ()async{
                                if(nameTextField.text.isNotEmpty && phoneTextField.text.isNotEmpty
                                    && messageTextField.text.isNotEmpty){
                                  //TODO
                                  //_ConfirmDialogAlert(context);
                                }
                                else{
                                  ErrorDialogAlert(context,"Must Add all fields");
                                }
                              },
                              child: Text(AppLocalizations.of(context).translate("send"),
                                style: TextStyle(
                                    fontFamily: Lang,
                                    fontWeight: FontWeight.normal,
                                    fontSize: SystemControls.font2,
                                    color: Colors.black
                                ),
                              ),
                            ),
                          ),
                        )
                    )
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.topRight,
              child: Container(
                margin: EdgeInsets.all(20),
                child: GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Container(
                    child: Icon(Icons.cancel,
                      color: Colors.white,
                      size: 30,
                    ),
                  ),
                ),
              ),
            ), //bACK
          ],
        ),
      ),
    );
  }

  Future<void> _ConfirmDialogAlert(BuildContext context) {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
          ),
          title: Text(AppLocalizations.of(context).translate("confirmationMsg"),
            style: TextStyle(
                fontFamily: Lang,
                fontWeight: FontWeight.bold,
                fontSize: SystemControls.font3,
                color: globals.accountColorsData['TextHeaderColor']!=null
                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
            ),
            textAlign: TextAlign.center,
          ),
          contentPadding: EdgeInsets.only(top: 40,bottom: 0),
          content: ClipRRect(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
              bottomRight: Radius.circular(SystemControls.RadiusCircValue),
            ),
            child: Container(
              height: 40,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Container(
                      decoration: BoxDecoration(
                        color: globals.accountColorsData['MainColor']!=null
                            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                        border: Border(
                          top: BorderSide(color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz),
                        ),
                      ),
                      child: FlatButton(
                        child: Text(AppLocalizations.of(context).translate("send"),
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData['TextHeaderColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                          ),
                        ),
                        onPressed: () async{
                          /*var res = await opinionApi(Lang, nameTextField.text,
                              phoneTextField.text, messageTextField.text);
                          if(res == null){
                            ErrorDialogAlert(context,
                                AppLocalizations.of(context).translate("respError"));
                          }
                          else{
                            //print(res.message);
                            if(res.status != null){
                              SuccDialogAlertBackHome(context, Lang, res.message);
                              Timer(Duration(seconds: 2), (){
                                // Navigator.pop(context);
                              });
                            }
                            else{Navigator.pop(context);
                            ErrorDialogAlert(context,res.message);
                            }
                          }

                           */
                        },
                      ),
                    ),
                  ),
                  Container(
                    height: 40,
                    width: 1,
                    color: Colors.black,
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      decoration: BoxDecoration(
                        color: globals.accountColorsData['MainColor']!=null
                            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                        border: Border(
                          top: BorderSide(color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz),
                        ),
                        //borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: FlatButton(
                        child: Text(AppLocalizations.of(context).translate("cancel"),
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData['TextHeaderColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                          ),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
*/
