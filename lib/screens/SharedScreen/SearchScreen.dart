
import '../../Models/HomeModel/sharedModel.dart';
import '../../widgets/SharedWidget/NoItemScreen.dart';

import '../../Models/HomeModel/CategoryDetailModel.dart';
import '../../Models/HomeModel/CategorySearchModel.dart';
import '../../provider/HomeProvider/SearchProvider.dart';
import '../../services/HomeRepo.dart';
import '../../widgets/SharedWidget/ProductItmsList.dart';
import '../../widgets/SharedWidget/ProgressIndecator.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../Controls.dart';
import '../../globals.dart' as globals;
import 'package:easy_localization/easy_localization.dart';
import 'package:sizer/sizer.dart';

class SearchScreenNew extends StatefulWidget {
  SearchScreenNew({Key key, this.cartList,this.appCurrency,this.token}) : super(key: key);
  List<dynamic> cartList;
  String appCurrency;
  String token;

  @override
  _SearchScreenNewState createState() => _SearchScreenNewState();
}

class _SearchScreenNewState extends State<SearchScreenNew> {
  ScrollController scrollController;
  ScrollController scrollControllerTitle;

  scrollListener() {
    var searchProvider = Provider.of<SearchProvider>(context, listen: false);

    print(scrollController.position.pixels);
       switch(searchProvider.search){
         case 0:
           if (searchProvider.currentPage < searchProvider.lastPage &&
               scrollController.position.pixels >=
                   scrollController.position.maxScrollExtent &&
               searchProvider.search == 0) {
             if (searchProvider.currentPage < searchProvider.nextPage) {
               print('products');
               searchProvider.fetchProductById(
                   context, searchProvider.categoryId, searchProvider.currentPage++);
             }
           }
           break;
           case 1:
           if (searchProvider.currentPage < searchProvider.lastPage &&
               scrollController.position.pixels >=
                   scrollController.position.maxScrollExtent &&
               searchProvider.search == 0) {
             if (searchProvider.currentPage < searchProvider.nextPage) {
               print('products');
               searchProvider.fetchProductByTitle(
                   context, searchProvider.searchController.text, searchProvider.currentPage++);
             }
           }
           break;
       }


  }
  scrollListenerTitle() {
    var searchProvider = Provider.of<SearchProvider>(context, listen: false);

    print(scrollControllerTitle.position.pixels);
       switch(searchProvider.search){

           case 1:
           if (searchProvider.currentPage < searchProvider.lastPage &&
               scrollControllerTitle.position.pixels >=
                   scrollControllerTitle.position.maxScrollExtent &&
               searchProvider.search == 1) {
             if (searchProvider.currentPage < searchProvider.nextPage) {
               print('products');
               searchProvider.fetchProductByTitle(
                   context, searchProvider.searchController.text, searchProvider.currentPage++);
             }
           }
           break;
       }


  }

  @override
  void initState() {
    // TODO: implement initState
    scrollController = ScrollController();
    scrollControllerTitle = ScrollController();
    scrollController.addListener(scrollListener);
    scrollControllerTitle.addListener(scrollListenerTitle);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(10.h),
          child: AppBar(
            automaticallyImplyLeading: false, // hides leading widget
            flexibleSpace: Column(
              children: [
                Spacer(),
                SearchTextWidget(),
              ],
            ),
          )),
      body: SingleChildScrollView(
physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            Consumer<SearchProvider>(builder: (context, search, _) {
              return Container(

                child: search.showSearchCategories == true
                    ? FutureBuilder<CategorySearchModel>(
                        future:
                            GetCategorySearchTreeRepo(context: context)
                                .getCategorySearch,
                        builder: (context, snap) {
                          if (snap.hasData) {
                            return Container(

                              width: MediaQuery.of(context).size.width,
                              child: ListView.builder(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                scrollDirection: Axis.vertical,
                                itemCount:
                                    snap.data.data.categories.length,
                                itemBuilder: (context, index) {
                                  print(
                                      "snap.data.data.categories[index].depth");
                                  print(snap
                                      .data.data.categories[index].depth);
                                  return Container(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [

                                        Theme(
                                          data: Theme.of(context).copyWith(accentColor: Colors.black),

                                          child: ExpansionTile(
                                            iconColor: Colors.black,
                                              childrenPadding: EdgeInsets.only(left: 10),
                                            title: Text(
                                            "${snap.data.data.categories[index].categoriesTitle}",
                                            style: TextStyle(
                                                fontWeight:
                                                FontWeight.bold,color: Colors.black,fontSize: 14.5),

                                          ),children: [
                                            InkWell(
                                              onTap: (){
                                                search.searchWithCatID(context,snap
                                                    .data.data.categories[index].categoriesId);
                                              },
                                              child: Container(

                                                padding: EasyLocalization.of(context).currentLocale.languageCode =="en" ?EdgeInsets.only(left: 30):EdgeInsets.only(right: 30),
                                                width: MediaQuery.of(context).size.width-100  ,

                                                child: Text(
                                                  "viewAll".tr().toString(),
                                                  style: TextStyle(
                                                      fontWeight:
                                                      FontWeight.bold,fontSize: 14),
                                                ),
                                              ),
                                            ),


                                            Container(
                                              padding:EasyLocalization.of(context).currentLocale.languageCode =="en" ? const EdgeInsets.only(left: 60):const EdgeInsets.only(right: 60),
                                              child: CategoryBuilder(subcategories: snap.data.data.categories[index].subcategories,style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 14),),
                                            )


                                          ], leading: CircleAvatar(
                                              backgroundColor: Colors.black.withOpacity(.07),
                                              child: CachedNetworkImage(
                                                imageUrl: SystemControls()
                                                    .GetImgPath(snap.data.data.categories[index].categoriesImg, "original"),
                                                placeholder: (context, url) => Center(
                                                  child: SystemControls().circularProgress(),
                                                ),
                                                errorWidget: (context, url, error) => Center(
                                                  child: Icon(Icons.broken_image),
                                                ),
                                                fit: BoxFit.contain,
                                                height: 12.h,
                                              )),),
                                        ),

                                      ],
                                    ),
                                  );
                                },
                              ),
                            );
                          } else {
                            return Container(
                              height: 50.h,
                              width: MediaQuery.of(context).size.width,
                              child: Center(child: Indicator()),
                            );
                          }
                        },
                      )
                    : search.search ==1 ? StreamBuilder<CategoriesDetailModel>(
                    stream: search.streamProductByTitle,
                    builder: (context,snap){
                        if(snap.hasData){
                          if (snap.data.data.pagination.lastPage ==
                              search.currentPage) {
                            search.showLoader = false;

                          }
                          search.lastPage = snap.data.data.pagination.lastPage;
                          search.currentPage = snap.data.data.pagination.currentPage;

                          return snap.data.data.products.length ==0? NoItemWidget(): Container(

                              height: MediaQuery.of(context).size.height-115,
                              width: MediaQuery.of(context).size.width,
                              child: SingleChildScrollView(
                                controller: scrollControllerTitle,
                                child: Column(
                                  children: [
                                    ProductsGridView(products: search.productSearchByTitleList,cartList: widget.cartList,appCurrency: widget.appCurrency,search: true,token: widget.token,),
                                  ],
                                ),
                              ));
                        }else if(snap.connectionState == ConnectionState.waiting) {
                          return Container(
                            height: 50.h,
                            width: MediaQuery.of(context).size.width,
                            child: Center(child: Indicator()),
                          );
                        }else{
                          return Container();
                        }

                }):StreamBuilder<CategoriesDetailModel>(
                    stream: search.streamProductById,
                    builder: (context,snap){
                      if(snap.hasData){
                        if (snap.data.data.pagination.lastPage ==
                            search.currentPage) {
                          search.showLoader = false;

                        }
                        search.lastPage = snap.data.data.pagination.lastPage;
                        search.currentPage = snap.data.data.pagination.currentPage;
                        print('ffffffffffffffff');
                        print(search.lastPage);
                        print(search.currentPage);
                        print("home.lastPage${search.lastPage}");
                        return snap.data.data.products.length ==0? NoItemWidget(): Container(

                            height: MediaQuery.of(context).size.height-115,
                            width: MediaQuery.of(context).size.width,
                            child: SingleChildScrollView(
                              controller: scrollController,
                              child: Column(
                                children: [
                                  ProductsGridView(products: search.productSearchByIdList,cartList: widget.cartList,appCurrency: widget.appCurrency,search: true,token: widget.token,),
                                ],
                              ),
                            ));
                      }else if(snap.connectionState == ConnectionState.waiting) {
                        return Container(
                          height: 50.h,
                          width: MediaQuery.of(context).size.width,
                          child: Center(child: Indicator()),
                        );
                      }else{
                        return Container();
                      }

                    }),
              );
            })
          ],
        ),
      ),
    );
  }


}

class SearchTextWidget extends StatelessWidget {
  SearchTextWidget({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Consumer<SearchProvider>(builder: (context,search,_){
      return Container(
        height: 80,
        // decoration: BoxDecoration(
        //   color: globals.accountColorsData['SearchBGColor'] != null
        //       ? Color(int.parse(globals.accountColorsData['SearchBGColor']))
        //       : SystemControls.SearchBGColorz,
        //
        // ),
        padding: EdgeInsets.all(5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
               Consumer<SearchProvider>(builder: (context,search,_){
                 return  IconButton(
                   icon: Icon(Icons.close, color: Colors.black),
                   onPressed: () {
                       search.searchController.clear();
                     Navigator.pop(context);
                   },
                 );
               },),
                InkWell(
                  child: Container(
                    height: 40,
                    width: 40,
                    margin: EdgeInsetsDirectional.fromSTEB(10, 0, 0, 0),
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor'] != null
                          ? Color(
                          int.parse(globals.accountColorsData['MainColor']))
                          : SystemControls.MainColorz,
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          blurRadius:
                          2.0, // has the effect of softening the shadow
                          spreadRadius:
                          0.5, // has the effect of extending the shadow
                          offset: Offset(
                            2, // horizontal, move right 10
                            3.0, // vertical, move down 10
                          ),
                        )
                      ],
                    ),
                    child: RotatedBox(
                      quarterTurns: 2,
                      child: Icon(
                        Icons.send,
                        color:
                        globals.accountColorsData['TextOnMainColor'] != null
                            ? Color(int.parse(
                            globals.accountColorsData['TextOnMainColor']))
                            : SystemControls.TextOnMainColorz,
                        size: 20,
                      ),
                    ),
                  ),
                  onTap: () {
                    if(FocusScope.of(context).isFirstFocus) {
                      FocusScope.of(context).requestFocus(new FocusNode());
                    }
                    search.searchWithTitle(context, search.searchController.text);
                  },
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Container(
                    height: 50,
                    decoration: BoxDecoration(
                      //color: Colors.white,
                        borderRadius: BorderRadius.all(
                            Radius.circular(SystemControls.RadiusCircValue)),
                      ),
                    child: TextField(
                      onTap: (){
                        search.controlSearchCategories(true);
                        print('true sssssss');
                        },
                      controller: search.searchController,
                      cursorColor: Colors.black,
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                        hintText: "searchText".tr().toString(),
                        helperStyle: TextStyle(color: globals
                            .accountColorsData[
                        'BorderColor'] !=
                            null
                            ? Color(int.parse(globals
                            .accountColorsData[
                        'BorderColor']))
                            : SystemControls.BorderColorz),
                        errorMaxLines: 1,
                        errorStyle: TextStyle(
                            fontSize: 0, height: 0),
                        isDense: false,
                        prefixIcon: Icon(Icons.search,
                            color: globals
                                .accountColorsData[
                            'BorderColor'] !=
                                null
                                ? Color(int.parse(globals
                                .accountColorsData[
                            'BorderColor']))
                                : SystemControls.BorderColorz),
                        filled: true,
                        contentPadding: EdgeInsets.all(3),
                        fillColor: globals
                            .accountColorsData[
                        'BorderColor'] !=
                            null
                            ? Color(int.parse(globals
                            .accountColorsData[
                        'BorderColor']))
                            .withOpacity(.2)
                            : SystemControls.BorderColorz,
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: globals
                                    .accountColorsData[
                                'BorderColor'] !=
                                    null
                                    ? Color(int.parse(globals
                                    .accountColorsData[
                                'BorderColor']))
                                    .withOpacity(.2)
                                    : SystemControls.BorderColorz,
                                width: 1),
                            borderRadius: BorderRadius.all(
                                Radius.circular(8))),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: globals
                                    .accountColorsData[
                                'BorderColor'] !=
                                    null
                                    ? Color(int.parse(globals
                                    .accountColorsData[
                                'BorderColor']))
                                    .withOpacity(.2)
                                    : SystemControls.BorderColorz,
                                width: 1),
                            borderRadius: BorderRadius.all(
                                Radius.circular(8))),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: globals
                                    .accountColorsData[
                                'BorderColor'] !=
                                    null
                                    ? Color(int.parse(globals
                                    .accountColorsData[
                                'BorderColor']))
                                    .withOpacity(.2)
                                    : SystemControls.BorderColorz,
                                width: 1),
                            borderRadius: BorderRadius.all(
                                Radius.circular(8))),
                        errorBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: globals
                                    .accountColorsData[
                                'BorderColor'] !=
                                    null
                                    ? Color(int.parse(globals
                                    .accountColorsData[
                                'BorderColor']))
                                    .withOpacity(.2)
                                    : SystemControls.BorderColorz,
                                width: 1),
                            borderRadius: BorderRadius.all(
                                Radius.circular(8))),
                        focusedErrorBorder:
                        OutlineInputBorder(
                            borderSide: BorderSide(
                                color: globals
                                    .accountColorsData[
                                'BorderColor'] !=
                                    null
                                    ? Color(int.parse(globals
                                    .accountColorsData[
                                'BorderColor']))
                                    .withOpacity(.2)
                                    : SystemControls.BorderColorz,
                                width: 1),
                            borderRadius:
                            BorderRadius.all(
                                Radius.circular(
                                    8))),
                      ),
                      onSubmitted: (v) {
                        /// search
                      },
                      //onChanged: widget.onSearchQueryChanged,
                    ),
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
              ],
            ),
          ],
        ),
      );
    },);
  }
}
class CategoryBuilder extends StatelessWidget {
  CategoryBuilder({Key key,this.subcategories,this.token,this.appCureency,this.style}) : super(key: key);
  List<SharedCategory> subcategories;
  final String token;
  final String appCureency;
  final TextStyle style;
  @override
  Widget build(BuildContext context) {
    return  Consumer<SearchProvider>(builder: (context,search,_){
      return ListView.builder(
        itemCount: subcategories.length,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context,index){
          return Column(
            children: [

              Theme(
                data: ThemeData(
                    dividerColor: Colors.transparent

                ),


                child: ExpansionTile(

                  iconColor: Colors.black,
                  initiallyExpanded: subcategories[index].subcategories.length >1 ? false : false,
                  title: Text("${subcategories[index].categoriesTitle}",style: style,),
                  children:[

                    Container(
                        padding:EasyLocalization.of(context).currentLocale.languageCode =="en" ? EdgeInsets.only(left: 16):EdgeInsets.only(right: 16),
                        width: MediaQuery.of(context).size.width,
                        height: EasyLocalization.of(context).currentLocale.languageCode =="en" ? 2.h:3.h,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [

                            InkWell(
                              onTap: (){

                                search.searchWithCatID(context,subcategories[index].categoriesId);
                              },
                              child: Container(

                                  height:EasyLocalization.of(context).currentLocale.languageCode =="en" ? 2.h:3.h,
                                  width: MediaQuery.of(context).size.width,
                                  child: Text("viewAll".tr().toString(),)),),
                            Spacer(),


                          ],
                        )),
                    Padding(
                      padding:EasyLocalization.of(context).currentLocale.languageCode =="en" ? const EdgeInsets.only(left: 8.0,bottom: 5):const EdgeInsets.only(right: 8.0,bottom: 5),
                      child: Column(

                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: List.generate(subcategories[index].subcategories.length, (secondIndex) => Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            InkWell(
                              onTap: (){


                                search.searchWithCatID(context,subcategories[index]
                                    .subcategories[secondIndex].categoriesId);
                                },
                              child: Container(
                                    padding:EasyLocalization.of(context).currentLocale.languageCode =="en" ? EdgeInsets.only(left: 10):EdgeInsets.only(right: 10),
                                  width: MediaQuery.of(context).size.width,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(

                                        child: Padding(
                                          padding: const EdgeInsets.only(top: 10.0),
                                          child: Text("${subcategories[index].subcategories[secondIndex].categoriesTitle}"),
                                        ),
                                      ),
                                    ],
                                  )),
                            ),




                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: CategoryBuilder(subcategories: subcategories[index].subcategories[secondIndex].subcategories,appCureency: appCureency,token: token,style: TextStyle(color: Colors.black,fontWeight: FontWeight.normal),),
                            )
                          ],
                        )),
                      ),
                    )
                  ],


                ),
              ),

            ],
          );
        },
      );
    });
  }
}