






  import '../../Models/HomeModel/ProductByIdModel.dart';
import 'package:sizer/sizer.dart';
import '../../services/HomeRepo.dart';
import '../../widgets/SharedWidget/ProductDeatilsScreen.dart';
import '../../widgets/SharedWidget/ProgressIndecator.dart';
import 'package:flutter/material.dart';

class GetProductByIDScreen extends StatelessWidget {
    const GetProductByIDScreen({Key key,this.id,this.token,this.appCurrency,this.cartList}) : super(key: key);
  final int id;
    final String token;
    final String appCurrency;
    final List<dynamic> cartList;
    @override
    Widget build(BuildContext context) {
      return FutureBuilder<ProductByIdModel>(
          future: GetProductByIdRepo(context: context,id: id).getProductById,
          builder: (context,snap){

           if(snap.hasData){
             return ProductItemsDetailsScreen(product: snap.data.data.product,appCurrency: appCurrency,cartList: cartList,token: token,);
           }else{
             return Container(
               color: Colors.white,
               height: 100.h,
               child: Indicator(),
             );
           }

      });
    }
  }
