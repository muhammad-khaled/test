import '../../Repo/CouponValidateRepo.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../dataControl/dataModule.dart';
import '../Controls.dart';

import 'dart:async';
import 'package:provider/provider.dart';
import '../provider/productsInCartProvider.dart';
import 'package:sizer/sizer.dart';

import 'package:easy_localization/easy_localization.dart';
import '../dataControl/cartModule.dart';
import '../screens/cart2Order.dart';
import '../screens/cart1-1SelectBranch.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:android_intent/android_intent.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/foundation.dart';
import 'dart:io';
import '../globals.dart' as globals;

class CartLayout extends StatefulWidget{
  String Lang;
  String AppCurrency;
  String UserToken;
  int design_Control;
  CartLayout(this.Lang, this.UserToken,this.AppCurrency,this.design_Control);

  _cartLayout createState() => _cartLayout(this.Lang, this.UserToken,
      this.AppCurrency,this.design_Control);

}
class _cartLayout extends State<CartLayout>{


  //End of section
  final Future<CartList> localCart = read_from_file();
  List<dynamic> _mealsCart = <dynamic>[];
  String Lang;
  String AppCurrency;
  String UserToken;
  int design_Control;
  final SystemControls storage = SystemControls();
  String hasCoupon ;
   getPerifrance()async{
     storage.getAccountsShowCoupons().then((value) {

         hasCoupon = value;
         print("hasCoupon");
         print(value);
         print("hasCoupon");
    });
   }


   ScrollController _controller ;
  // KeyboardVisibilityNotification _keyboardVisibility = new KeyboardVisibilityNotification();
  // int _keyboardVisibilitySubscriberId;
  // bool _keyboardState;

  void initState() {
    super.initState();
    // _keyboardState = _keyboardVisibility.isKeyboardVisible;
    //
    // _keyboardVisibilitySubscriberId = _keyboardVisibility.addNewListener(
    //   onChange: (bool visible) {
    //     setState(() {
    //       _keyboardState = visible;
    //     });
    //   },
    // );


    _controller = ScrollController();
    _getCurrentLocation();
    CheckGpsAlert(context);
    getPerifrance();
  }

  Future<ResponseApi> respAdds;
  List<dynamic> _addreses = <dynamic>[];

  TextEditingController notesTextField = TextEditingController();
  _cartLayout(this.Lang, this.UserToken,
      this.AppCurrency,this.design_Control){
    respAdds = getUserAddressesApi(Lang,UserToken);
    print("Get frtom global file  ***** "
        +"\n  **  lat " +globals.latitude.toString()
        +"\n  **  lon "+ globals.longitude.toString());
    latitude = globals.latitude;
    longitude = globals.longitude;

  }
  double latitude=0, longitude=0;

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  _getCurrentLocation() async{
    print("get location");
    Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        Position _currentPosition = position;
        latitude = position.latitude;
        longitude =position.longitude;
        globals.latitude = latitude;
        globals.longitude = longitude;
        print("Position .... " + latitude.toString() + longitude.toString());
        print("Position .... " + _currentPosition.toString());
      });
    }).catchError((e) {
      print(e);
    });
  }

  int findIfItemOnList(dynamic item, List<dynamic> _list){
    int output=-1;
    if(_list.length != 0){
      //TODO Check ingredients list too
      for(int i=0; i<_list.length; i++){
        if(_list[i]['meals_id']== item['meals_id']
            && _list[i]['type']== item['type']
            && _list[i]['choice']== item['choice']){
          print("Find #### "+_list[i]['meals_id'].toString());
          output = i;
          break;
        }
      }
    }
    return output;
  }
  bool hide = true;
  void OrganizeCartData(List<dynamic> newListall){
    List<dynamic> newList = <dynamic>[];
    print("in fun ... "+newListall.length.toString());
    newList.add(newListall[0]);
    for(int i=1; i<newListall.length; i++){
      //bool findObj = newList.contains(newListall[i]);
      int findObj=findIfItemOnList(newListall[i],newList);
      print("Get Find #### "+findObj.toString());
      if(findObj<0){
        newList.add(newListall[i]);
      }
      else{
        dynamic quantity = newListall[i]['count'];
        newList[findObj]['count'] += quantity;
      }
    }
    //_mealsCart.clear();
    _mealsCart.clear();
    _mealsCart = newList;
    print("new list --->\nnum of items" + newList.length.toString());
    print("\nnew list\n\n" + newList.toString()+"\n\n");

    CartStateProvider cartState = Provider.of<CartStateProvider>(context,listen: false);
    cartState.setCurrentProductsCount(_mealsCart.length.toString());
    cartState.fetchCardNum();
  }

  bool firstLoad = true;
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    // _keyboardVisibilitySubscriberId = _keyboardVisibility.addNewListener(
    //   onChange: (bool visible) {
    //
    //       _keyboardState = visible;
    //
    //   },
    // );
    print('_keyboardState');
    // print(_keyboardState);

    // final appLocalizations = AppLocalizations.of(context);
    CartStateProvider cartState = Provider.of<CartStateProvider>(context,listen: false);
    _imageFile = cartState.imageFile;
    _imgReordername = cartState.cartImgName == null ? "": cartState.cartImgName;

    print('keyboardIsOpened');
    print(MediaQuery.of(context).viewInsets.horizontal);
    print('keyboardIsOpened');
    print("Position in*** .... " + latitude.toString() +"/"+ longitude.toString());
    return Scaffold(

      appBar: AppBar(
        title: Text("cart".tr().toString()),
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
      ),
      body: FutureBuilder<CartList>(
          future: localCart,
          builder: (context, snap){
            if(snap.hasData || _imageFile != null || _imgReordername != ""){
              //int total_items = 0;
              double total_price = 0;
              if(firstLoad == true && snap.data != null){
                _mealsCart = snap.data.Cmeals;



                if(_mealsCart.length>0) {
                  print("num of items new list1 " +
                      _mealsCart.length.toString());

                  //Sort with  meal id
                  _mealsCart.sort((a, b) {
                    return a['meals_id'].compareTo(b['meals_id']);
                  });
                  //Sort with type --> offer or meal can use same type
                  _mealsCart.sort((a, b) {
                    return a['type'].compareTo(b['type']);
                  });

                  print("num of items new list2 " +
                      _mealsCart.length.toString());
                  /*for (int i = 0; i < _mealsCart.length; i++) {
                    total_items = total_items + _mealsCart[i]['count'];
                    total_price = total_price +
                        (double.parse(_mealsCart[i]['meals_price']) *
                            _mealsCart[i]['count']);
                  }*/
                  print("num of items new list3 " +
                      _mealsCart.length.toString());
                  OrganizeCartData(_mealsCart);
                  //newListall.clear();
                }
                firstLoad = false;

              }
              if(_mealsCart.length>0) {
                for (int i = 0; i < _mealsCart.length; i++) {
                  //total_items = total_items + _mealsCart[i]['count'];
                  total_price = total_price +
                      (double.parse(_mealsCart[i]['meals_price']) *
                          double.parse("${_mealsCart[i]['count']}"));
                }
                print("num of items" + _mealsCart.length.toString());
                cartState.setCartList(snap.data.Cmeals);
                /*return Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    LayoutBG(design_Control),
                    Container(
                      margin: EdgeInsets.only(
                          top: 10, left: 10, right: 10, bottom: 160),
                      child: ListView(
                        scrollDirection: Axis.vertical,
                        children: <Widget>[
                          ImagPickerWidget(),
                          Padding(
                            padding: EdgeInsets.only(top: 10),
                            child: ListView.builder(
                                scrollDirection: Axis.vertical,
                                shrinkWrap: true,
                                physics: ScrollPhysics(),
                                itemCount: _mealsCart.length,
                                itemBuilder: (context, index) {
                                  return InkWell(
                                    child: Container(
                                        height: 100,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          //Theme.of(context).primaryColor,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(SystemControls
                                                  .RadiusCircValue)),
                                        ),
                                        margin: EdgeInsets.all(5),
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(SystemControls
                                                  .RadiusCircValue)),
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment
                                                .stretch,
                                            mainAxisAlignment: MainAxisAlignment
                                                .center,
                                            children: <Widget>[
                                              Expanded(
                                                flex: 1,
                                                child: Container(
                                                  child: ClipRRect(
                                                    borderRadius: BorderRadius
                                                        .all(Radius.circular(
                                                        SystemControls
                                                            .RadiusCircValue)),
                                                    child: CachedNetworkImage(
                                                      imageUrl: SystemControls().GetImgPath(_mealsCart[index]['meals_img'].toString(),"medium"),
                                                      placeholder: (context, url) => Center(child: SystemControls().circularProgress(),),
                                                      errorWidget: (context, url, error) => Center(child: Icon(Icons.broken_image),),
                                                      fit: BoxFit.contain,
                                                      width: MediaQuery.of(context).size.width,
                                                      height: MediaQuery.of(context).size.height,
                                                    ),/*
                                              child: Image.network(
                                                SystemControls().GetImgPath(
                                                    _mealsCart[index]['meals_img'],
                                                    "medium"),
                                                fit: BoxFit.contain,),*/
                                                  ),
                                                ),
                                              ),
                                              Padding(padding: EdgeInsets.all(5)),
                                              Expanded(
                                                flex: 2,
                                                child: Container(
                                                    margin: EdgeInsets.only(
                                                        top: 5),
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment
                                                          .center,
                                                      crossAxisAlignment: CrossAxisAlignment
                                                          .end,
                                                      children: <Widget>[
                                                        Container(
                                                          child: Row(
                                                            children: <Widget>[
                                                              Expanded(
                                                                flex: 2,
                                                                child: Container(
                                                                    child: Column(
                                                                      mainAxisAlignment: MainAxisAlignment
                                                                          .center,
                                                                      crossAxisAlignment: CrossAxisAlignment
                                                                          .start,
                                                                      children: <
                                                                          Widget>[
                                                                        Text(
                                                                          _mealsCart[index]['meals_title'],
                                                                          style: TextStyle(
                                                                            fontFamily: Lang,
                                                                            fontWeight: FontWeight
                                                                                .normal,
                                                                            fontSize: SystemControls
                                                                                .font4,
                                                                            height: 1.0,
                                                                          ),
                                                                          textAlign: TextAlign
                                                                              .start,
                                                                          maxLines: 1,
                                                                        ),
                                                                        Padding(
                                                                            padding: EdgeInsets
                                                                                .only(
                                                                                top: 5)),
                                                                        Text(
                                                                          _mealsCart[index]['meals_price'] +
                                                                              " " +
                                                                              AppCurrency,
                                                                          style: TextStyle(
                                                                            fontFamily: Lang,
                                                                            fontWeight: FontWeight
                                                                                .normal,
                                                                            fontSize: SystemControls
                                                                                .font4,
                                                                            height: 1.2,
                                                                          ),
                                                                          textAlign: TextAlign
                                                                              .center,
                                                                          maxLines: 2,
                                                                        )
                                                                      ],
                                                                    )
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Container(
                                                          child: Row(
                                                            mainAxisAlignment: MainAxisAlignment
                                                                .spaceBetween,
                                                            crossAxisAlignment: CrossAxisAlignment
                                                                .center,
                                                            children: <Widget>[
                                                              Row(
                                                                mainAxisAlignment: MainAxisAlignment
                                                                    .start,
                                                                crossAxisAlignment: CrossAxisAlignment
                                                                    .center,
                                                                children: <
                                                                    Widget>[
                                                                  InkWell(
                                                                    child: Container(
                                                                      width: 30,
                                                                      height: 30,
                                                                      decoration: BoxDecoration(
                                                                        color: globals.accountColorsData['BGColor']!=null
                                                                            ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
                                                                        borderRadius: BorderRadius
                                                                            .all(
                                                                            Radius
                                                                                .circular(
                                                                                SystemControls
                                                                                    .RadiusCircValue /
                                                                                    2)),
                                                                      ),
                                                                      child: Center(
                                                                        child: Icon(
                                                                            Icons.remove,
                                                                            color: Colors
                                                                                .black //SystemControls.MainColor,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    onTap: () {
                                                                      print(
                                                                          "on click button dicres quantity ");
                                                                      if (_mealsCart[index]['count'] >
                                                                          1) {
                                                                        setState(() {
                                                                          _mealsCart[index]['count']--;
                                                                          UpdateList(
                                                                              _mealsCart);
                                                                        });
                                                                      }
                                                                    },
                                                                  ),
                                                                  Padding(
                                                                    padding: EdgeInsets
                                                                        .only(
                                                                        right: 15,
                                                                        left: 15),
                                                                    child: Text(
                                                                      _mealsCart[index]['count']
                                                                          .toString(),
                                                                      style: TextStyle(
                                                                        fontFamily: Lang,
                                                                        fontWeight: FontWeight
                                                                            .normal,
                                                                        fontSize: SystemControls
                                                                            .font3,
                                                                        height: 1.2,
                                                                      ),
                                                                      textAlign: TextAlign
                                                                          .center,
                                                                      maxLines: 2,
                                                                    ),
                                                                  ),
                                                                  InkWell(
                                                                    child: Container(
                                                                      width: 30,
                                                                      height: 30,
                                                                      decoration: BoxDecoration(
                                                                        color: globals.accountColorsData['BGColor']!=null
                                                                            ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
                                                                        borderRadius: BorderRadius
                                                                            .all(
                                                                            Radius
                                                                                .circular(
                                                                                SystemControls
                                                                                    .RadiusCircValue /
                                                                                    2)),
                                                                      ),
                                                                      child: Center(
                                                                        child: Icon(
                                                                            Icons
                                                                                .add,
                                                                            color: Colors
                                                                                .black //SystemControls.MainColor,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    onTap: () {
                                                                      print(
                                                                          "on click button incr quantity ");
                                                                      setState(() {
                                                                        _mealsCart[index]['count']++;
                                                                        UpdateList(
                                                                            _mealsCart);
                                                                      });
                                                                    },
                                                                  ),
                                                                ],
                                                              ),
                                                              Expanded(
                                                                flex: 1,
                                                                child: Row(
                                                                  mainAxisAlignment: MainAxisAlignment
                                                                      .end,
                                                                  crossAxisAlignment: CrossAxisAlignment
                                                                      .end,
                                                                  children: <
                                                                      Widget>[
                                                                    IconButton(
                                                                        icon: Icon(
                                                                          Icons.delete_outline,),
                                                                        onPressed: () {
                                                                          print(
                                                                              "on delete from cart.");
                                                                          setState(() {
                                                                            _mealsCart.removeAt(index);
                                                                            UpdateList(_mealsCart);

                                                                            CartState cartState = Provider.of<CartState>(context);
                                                                            cartState.setCurrentProductsCount(_mealsCart.length.toString());
                                                                          });
                                                                        }),
                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                    ),
                                  );
                                }
                            ),
                          ),
                        ],
                      ),
                    ),
                    FutureBuilder<responceFe>(
                        future: respAdds,
                        builder: (context, snap) {
                          if (snap.hasData) {
                            if (snap.data.status == 200) {
                              dynamic dataa = snap.data.data;
                              _addreses = dataa['addresses'];
                            }
                            else if (snap.data.status == 401) {
                              print("address .... 401 cart");
                              SystemControls().LogoutSetUserData(
                                  context, Lang);
                              //ErrorDialogAlertBackHome(context, snap.data.message);
                            }
                            else {
                              _addreses = [];
                            }
                            return Container();
                          }
                          if (snap.hasError) {
                            _addreses = [];
                          }
                          return Center(
                            child: SystemControls().circularProgress(),
                          );
                        }
                    ),
                    Stack(
                      alignment: Alignment.bottomCenter,
                      children: <Widget>[
                        Container(
                          color: globals.accountColorsData['BGColor']!=null
                              ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
                          alignment: Alignment.bottomCenter,
                          height: 160,
                          width: MediaQuery
                              .of(context)
                              .size
                              .width,
                        ),
                        Container(
                          width: MediaQuery
                              .of(context)
                              .size
                              .width * 0.9,
                          margin: EdgeInsets.only(right: 10, left: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              /*Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Text(AppLocalizations.of(context).translate("mealsCount"),
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.normal,
                                        fontSize: SystemControls.font3,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
        ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                        height: 1.2,
                                      )
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Text(_mealsCart.length.toString(),
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.normal,
                                        fontSize: SystemControls.font3,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
        ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                        height: 1.2,
                                      )
                                  ),
                                )
                              ],
                            ),
                            Padding(padding: EdgeInsets.all(3)),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Text(AppLocalizations.of(context).translate("itemsCount"),
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.normal,
                                        fontSize: SystemControls.font3,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
        ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                        height: 1.2,
                                      )
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Text(total_items.toString(),
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.normal,
                                        fontSize: SystemControls.font3,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
        ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                        height: 1.2,
                                      )
                                  ),
                                )
                              ],
                            ),*/
                              Container(
                                height: 1,
                                color: globals.accountColorsData['TextdetailsColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                              ),
                              Padding(padding: EdgeInsets.all(3)),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(AppLocalizations.of(context)
                                        .translate("itemsPrice"),
                                        style: TextStyle(
                                          fontFamily: Lang,
                                          fontWeight: FontWeight.normal,
                                          fontSize: SystemControls.font3,
                                          color: globals.accountColorsData['TextHeaderColor']!=null
                                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                          height: 1.2,
                                        )
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment
                                          .end,
                                      crossAxisAlignment: CrossAxisAlignment
                                          .end,
                                      children: <Widget>[
                                        Text(
                                            total_price.toStringAsFixed(globals.numberDecimalDigits) +
                                                " " + AppCurrency,
                                            style: TextStyle(
                                              fontFamily: Lang,
                                              fontWeight: FontWeight.normal,
                                              fontSize: SystemControls
                                                  .font3,
                                              color: globals.accountColorsData['TextHeaderColor']!=null
                                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                              height: 1.2,
                                            )
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),

                              Padding(padding: EdgeInsets.all(3)),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(AppLocalizations.of(context)
                                        .translate("deliveryFees"),
                                        style: TextStyle(
                                          fontFamily: Lang,
                                          fontWeight: FontWeight.normal,
                                          fontSize: SystemControls.font3,
                                          color: globals.accountColorsData['TextHeaderColor']!=null
                                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                          height: 1.2,
                                        )
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment
                                          .end,
                                      crossAxisAlignment: CrossAxisAlignment
                                          .end,
                                      children: <Widget>[
                                        Text(globals.deliveryCost
                                            .toString() + " " + AppCurrency,
                                            style: TextStyle(
                                              fontFamily: Lang,
                                              fontWeight: FontWeight.normal,
                                              fontSize: SystemControls
                                                  .font3,
                                              color: globals.accountColorsData['TextHeaderColor']!=null
                                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                              height: 1.2,
                                            )
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),

                              Container(
                                height: 1,
                                color: globals.accountColorsData['TextdetailsColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                              ),
                              Padding(padding: EdgeInsets.all(3)),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(AppLocalizations.of(context)
                                        .translate("TotalPrice"),
                                        style: TextStyle(
                                          fontFamily: Lang,
                                          fontWeight: FontWeight.normal,
                                          fontSize: SystemControls.font3,
                                          color: globals.accountColorsData['TextHeaderColor']!=null
                                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                          height: 1.2,
                                        )
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment
                                          .end,
                                      crossAxisAlignment: CrossAxisAlignment
                                          .end,
                                      children: <Widget>[
                                        Text((total_price +
                                            globals.deliveryCost)
                                            .toStringAsFixed(globals.numberDecimalDigits) + " " +
                                            AppCurrency,
                                            style: TextStyle(
                                              fontFamily: Lang,
                                              fontWeight: FontWeight.normal,
                                              fontSize: SystemControls
                                                  .font3,
                                              color: globals.accountColorsData['TextHeaderColor']!=null
                                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                              height: 1.2,
                                            )
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),

                              Padding(padding: EdgeInsets.all(6)),
                              Container(
                                width: MediaQuery
                                    .of(context)
                                    .size
                                    .width,
                                height: 50,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(10)),
                                    color: globals.accountColorsData['MainColor']!=null
                                        ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz
                                ),
                                child: FlatButton(
                                  onPressed: () {
                                    if (UserToken == "") {
                                      ErrorDialogAlertGoTOLogin(context,
                                          AppLocalizations.of(context)
                                              .translate("mustLogin"));
                                    }
                                    else if (globals.minOrderCost >
                                        total_price) {
                                      String WarningMessage =
                                          AppLocalizations.of(context)
                                              .translate(
                                              "minOrderCostWarningMessage")
                                              + globals.minOrderCost
                                              .toString()
                                              + " " + AppCurrency;
                                      ErrorDialogAlert(
                                          context, WarningMessage);
                                    }
                                    else {
                                      Navigator.pushReplacement(context,
                                          MaterialPageRoute(
                                              builder: (
                                                  BuildContext context) =>
                                                  CartOrderLayout(
                                                      Lang,
                                                      UserToken,
                                                      AppCurrency,
                                                      _mealsCart,
                                                      design_Control,
                                                      _addreses,
                                                      latitude,
                                                      longitude)
                                          )
                                      );
                                    }
                                  },
                                  child: Text(AppLocalizations.of(context)
                                      .translate("orderBut"),
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.normal,
                                        fontSize: SystemControls.font3,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
                                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                        height: 1.2,
                                      )
                                  ),
                                ),
                              ),
                              Padding(padding: EdgeInsets.all(10)),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                );*/
              }
              /*
              else{//Empty
                return Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    LayoutBG(design_Control),
                    Container(
                      margin: EdgeInsets.only(
                          top: 10, left: 10, right: 10, bottom: 160),
                      child: ListView(
                          scrollDirection: Axis.vertical,
                        children: <Widget>[
                          ImagPickerWidget(),
                          Padding(
                            padding: EdgeInsets.all(20),
                            child: Center(
                              child: Text(AppLocalizations.of(context).translate('emptyListcart')+" Normal get",
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.normal,
                                  fontSize: SystemControls.font2,
                                  height: 1.2,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    FutureBuilder<responceFe>(
                        future: respAdds,
                        builder: (context, snap) {
                          if (snap.hasData) {
                            if (snap.data.status == 200) {
                              dynamic dataa = snap.data.data;
                              _addreses = dataa['addresses'];
                            }
                            else if (snap.data.status == 401) {
                              print("address .... 401 cart");
                              SystemControls().LogoutSetUserData(
                                  context, Lang);
                              //ErrorDialogAlertBackHome(context, snap.data.message);
                            }
                            else {
                              _addreses = [];
                            }
                            return Container();
                          }
                          if (snap.hasError) {
                            _addreses = [];
                          }
                          return Center(
                            child: SystemControls().circularProgress(),
                          );
                        }
                    ),
                    Stack(
                      alignment: Alignment.bottomCenter,
                      children: <Widget>[
                        Container(
                          color: globals.accountColorsData['BGColor']!=null
                              ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
                          alignment: Alignment.bottomCenter,
                          height: 160,
                          width: MediaQuery
                              .of(context)
                              .size
                              .width,
                        ),
                        Container(
                          width: MediaQuery
                              .of(context)
                              .size
                              .width * 0.9,
                          margin: EdgeInsets.only(right: 10, left: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              /*Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Text(AppLocalizations.of(context).translate("mealsCount"),
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.normal,
                                        fontSize: SystemControls.font3,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
        ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                        height: 1.2,
                                      )
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Text(_mealsCart.length.toString(),
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.normal,
                                        fontSize: SystemControls.font3,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
        ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                        height: 1.2,
                                      )
                                  ),
                                )
                              ],
                            ),
                            Padding(padding: EdgeInsets.all(3)),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Text(AppLocalizations.of(context).translate("itemsCount"),
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.normal,
                                        fontSize: SystemControls.font3,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
        ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                        height: 1.2,
                                      )
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Text(total_items.toString(),
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.normal,
                                        fontSize: SystemControls.font3,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
        ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                        height: 1.2,
                                      )
                                  ),
                                )
                              ],
                            ),*/
                              Container(
                                height: 1,
                                color: globals.accountColorsData['TextdetailsColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                              ),
                              Padding(padding: EdgeInsets.all(3)),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(AppLocalizations.of(context)
                                        .translate("itemsPrice"),
                                        style: TextStyle(
                                          fontFamily: Lang,
                                          fontWeight: FontWeight.normal,
                                          fontSize: SystemControls.font3,
                                          color: globals.accountColorsData['TextHeaderColor']!=null
                                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                          height: 1.2,
                                        )
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment
                                          .end,
                                      crossAxisAlignment: CrossAxisAlignment
                                          .end,
                                      children: <Widget>[
                                        Text(
                                            total_price.toStringAsFixed(globals.numberDecimalDigits) +
                                                " " + AppCurrency,
                                            style: TextStyle(
                                              fontFamily: Lang,
                                              fontWeight: FontWeight.normal,
                                              fontSize: SystemControls
                                                  .font3,
                                              color: globals.accountColorsData['TextHeaderColor']!=null
                                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                              height: 1.2,
                                            )
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),

                              Padding(padding: EdgeInsets.all(3)),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(AppLocalizations.of(context)
                                        .translate("deliveryFees"),
                                        style: TextStyle(
                                          fontFamily: Lang,
                                          fontWeight: FontWeight.normal,
                                          fontSize: SystemControls.font3,
                                          color: globals.accountColorsData['TextHeaderColor']!=null
                                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                          height: 1.2,
                                        )
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment
                                          .end,
                                      crossAxisAlignment: CrossAxisAlignment
                                          .end,
                                      children: <Widget>[
                                        Text(globals.deliveryCost
                                            .toString() + " " + AppCurrency,
                                            style: TextStyle(
                                              fontFamily: Lang,
                                              fontWeight: FontWeight.normal,
                                              fontSize: SystemControls
                                                  .font3,
                                              color: globals.accountColorsData['TextHeaderColor']!=null
                                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                              height: 1.2,
                                            )
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),

                              Container(
                                height: 1,
                                color: globals.accountColorsData['TextdetailsColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                              ),
                              Padding(padding: EdgeInsets.all(3)),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(AppLocalizations.of(context)
                                        .translate("TotalPrice"),
                                        style: TextStyle(
                                          fontFamily: Lang,
                                          fontWeight: FontWeight.normal,
                                          fontSize: SystemControls.font3,
                                          color: globals.accountColorsData['TextHeaderColor']!=null
                                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                          height: 1.2,
                                        )
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment
                                          .end,
                                      crossAxisAlignment: CrossAxisAlignment
                                          .end,
                                      children: <Widget>[
                                        Text((total_price +
                                            globals.deliveryCost)
                                            .toStringAsFixed(globals.numberDecimalDigits) + " " +
                                            AppCurrency,
                                            style: TextStyle(
                                              fontFamily: Lang,
                                              fontWeight: FontWeight.normal,
                                              fontSize: SystemControls
                                                  .font3,
                                              color: globals.accountColorsData['TextHeaderColor']!=null
                                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                              height: 1.2,
                                            )
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),

                              Padding(padding: EdgeInsets.all(6)),
                              Container(
                                width: MediaQuery
                                    .of(context)
                                    .size
                                    .width,
                                height: 50,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(10)),
                                    color: globals.accountColorsData['MainColor']!=null
                                        ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz
                                ),
                                child: FlatButton(
                                  onPressed: () {
                                    if (UserToken == "") {
                                      ErrorDialogAlertGoTOLogin(context,
                                          AppLocalizations.of(context)
                                              .translate("mustLogin"));
                                    }
                                    else if (globals.minOrderCost >
                                        total_price) {
                                      String WarningMessage =
                                          AppLocalizations.of(context)
                                              .translate(
                                              "minOrderCostWarningMessage")
                                              + globals.minOrderCost
                                              .toString()
                                              + " " + AppCurrency;
                                      ErrorDialogAlert(
                                          context, WarningMessage);
                                    }
                                    else {
                                      Navigator.pushReplacement(context,
                                          MaterialPageRoute(
                                              builder: (
                                                  BuildContext context) =>
                                                  CartOrderLayout(
                                                      Lang,
                                                      UserToken,
                                                      AppCurrency,
                                                      _mealsCart,
                                                      design_Control,
                                                      _addreses,
                                                      latitude,
                                                      longitude)
                                          )
                                      );
                                    }
                                  },
                                  child: Text(AppLocalizations.of(context)
                                      .translate("orderBut"),
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.normal,
                                        fontSize: SystemControls.font3,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
                                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                        height: 1.2,
                                      )
                                  ),
                                ),
                              ),
                              Padding(padding: EdgeInsets.all(10)),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                );
              }*/
              return Stack(
                children: <Widget>[
                  LayoutBG(design_Control),
                   Container(
                    margin:  EdgeInsets.only(right: 20, left: 20, top: 10,
                        bottom: (globals.show_branches=="1"||globals.show_areas=="1")?120:160
                    ),

                    width: MediaQuery.of(context).size.width * 0.9,
                    child: ListView(
                      scrollDirection: Axis.vertical,

                      controller: _controller,
                      children: <Widget>[
                        ImagPickerWidget(),
                        Container(
                          padding: EdgeInsets.only(top: 0),
                          width: MediaQuery.of(context).size.width,
                          child: Text("cartProductsTitle".tr().toString(),
                              style: TextStyle(
                                fontFamily: Lang,
                                fontWeight: FontWeight.bold,
                                fontSize: SystemControls.font3,
                                color: globals.accountColorsData['TextHeaderColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                height: 1.2,
                              )
                          ),
                        ),
                        _mealsCart.length>0?
                        Padding(
                          padding: EdgeInsets.only(top: 10),
                          child: ListView.builder(
                            padding: EdgeInsets.zero,
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              physics: ScrollPhysics(),
                              itemCount: _mealsCart.length,
                              itemBuilder: (context, index) {
                                return InkWell(
                                  child: Container(
                                      height: 100,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        //Theme.of(context).primaryColor,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(SystemControls
                                                .RadiusCircValue)),
                                      ),
                                      margin: EdgeInsets.only(top: 5,bottom: 5),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(SystemControls
                                                .RadiusCircValue)),
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment
                                              .stretch,
                                          mainAxisAlignment: MainAxisAlignment
                                              .center,
                                          children: <Widget>[
                                            Expanded(
                                              flex: 1,
                                              child: Container(
                                                child: ClipRRect(
                                                  borderRadius: BorderRadius
                                                      .all(Radius.circular(
                                                      SystemControls
                                                          .RadiusCircValue)),
                                                  child: CachedNetworkImage(
                                                    imageUrl: SystemControls().GetImgPath(_mealsCart[index]['meals_img'].toString(),"medium"),
                                                    placeholder: (context, url) => Center(child: SystemControls().circularProgress(),),
                                                    errorWidget: (context, url, error) => Center(child: Icon(Icons.broken_image),),
                                                    fit: BoxFit.contain,
                                                    width: MediaQuery.of(context).size.width,
                                                    height: MediaQuery.of(context).size.height,
                                                  ),
                                                  /*
                                              child: Image.network(
                                                SystemControls().GetImgPath(
                                                    _mealsCart[index]['meals_img'],
                                                    "medium"),
                                                fit: BoxFit.contain,),*/
                                                ),
                                              ),
                                            ),
                                            Padding(padding: EdgeInsets.all(5)),
                                            Expanded(
                                              flex: 2,
                                              child: Container(
                                                  margin: EdgeInsets.only(
                                                      top: 5),
                                                  child: Column(
                                                    mainAxisAlignment: MainAxisAlignment
                                                        .center,
                                                    crossAxisAlignment: CrossAxisAlignment
                                                        .end,
                                                    children: <Widget>[
                                                      Container(
                                                        child: Row(
                                                          crossAxisAlignment: CrossAxisAlignment
                                                              .end,
                                                          children: <Widget>[
                                                            Expanded(
                                                              flex: 2,
                                                              child: Container(
                                                                  child: Column(
                                                                    mainAxisAlignment: MainAxisAlignment
                                                                        .center,
                                                                    crossAxisAlignment: CrossAxisAlignment
                                                                        .start,
                                                                    children: <
                                                                        Widget>[
                                                                      Text(
                                                                        _mealsCart[index]['meals_title'],
                                                                        style: TextStyle(
                                                                          fontFamily: Lang,
                                                                          fontWeight: FontWeight
                                                                              .normal,
                                                                          fontSize: SystemControls
                                                                              .font4,
                                                                          height: 1.0,
                                                                        ),
                                                                        textAlign: TextAlign
                                                                            .start,
                                                                        maxLines: 1,
                                                                      ),
                                                                      Padding(
                                                                          padding: EdgeInsets
                                                                              .only(
                                                                              top: 5)),
                                                                      Row(
                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                                        children: [
                                                                          Text(
                                                                            _mealsCart[index]['meals_price'] +
                                                                                " " +
                                                                                AppCurrency +
                                                                                "   " ,
                                                                            style: TextStyle(
                                                                              fontFamily: Lang,
                                                                              fontWeight: FontWeight
                                                                                  .normal,
                                                                              fontSize: SystemControls
                                                                                  .font4,
                                                                              height: 1.2,
                                                                            ),
                                                                            textAlign: TextAlign
                                                                                .start,
                                                                            maxLines: 1,
                                                                          ),
                                                                          Text(_mealsCart[index]['products_type']=='weight'?
                                                                                'priceForWeight'.tr().toString()
                                                                                    :'priceForCount'.tr().toString(),
                                                                            style: TextStyle(
                                                                              fontFamily: Lang,
                                                                              fontWeight: FontWeight
                                                                                  .normal,
                                                                              fontSize: SystemControls
                                                                                  .font4,
                                                                              height: 1.2,
                                                                            ),
                                                                            textAlign: TextAlign
                                                                                .start,
                                                                            maxLines: 1,
                                                                          )


                                                                        ],
                                                                      ),
                                                                    ],
                                                                  )
                                                              ),
                                                            ),
                                                            _mealsCart[index]['choice'] != null?
                                                            Container(
                                                              padding: EdgeInsets.only(right: 10,left: 10),
                                                                child: Column(
                                                                  mainAxisAlignment: MainAxisAlignment
                                                                      .center,
                                                                  crossAxisAlignment: CrossAxisAlignment
                                                                      .end,
                                                                  children: <Widget>[
                                                                    Text(
                                                                      'size'.tr().toString()+
                                                                          " "+
                                                                      _mealsCart[index]['choice_text'],
                                                                      style: TextStyle(
                                                                        fontFamily: Lang,
                                                                        fontWeight: FontWeight
                                                                            .normal,
                                                                        fontSize: SystemControls
                                                                            .font4,
                                                                        height: 1.0,
                                                                      ),
                                                                      textAlign: TextAlign
                                                                          .start,
                                                                      maxLines: 2,
                                                                    ),
                                                                  ],
                                                                )
                                                            )
                                                                : Container(),
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment
                                                              .spaceBetween,
                                                          crossAxisAlignment: CrossAxisAlignment
                                                              .center,
                                                          children: <Widget>[
                                                            Row(
                                                              mainAxisAlignment: MainAxisAlignment.start,
                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                              children: <Widget>[
                                                                InkWell(
                                                                  child: Container(
                                                                    width: 30,
                                                                    height: 30,
                                                                    decoration: BoxDecoration(
                                                                      color: globals.accountColorsData['BGColor']!=null
                                                                          ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
                                                                      borderRadius: BorderRadius.all(
                                                                          Radius
                                                                              .circular(
                                                                              SystemControls
                                                                                  .RadiusCircValue /
                                                                                  2)),
                                                                    ),
                                                                    child: Center(
                                                                      child: Icon(
                                                                          Icons.remove,
                                                                          color: Colors
                                                                              .black //SystemControls.MainColor,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  onTap: () {
                                                                    print(
                                                                        "on click button dicres quantity ");
                                                                      setState(() {
                                                                        if(_mealsCart[index]['products_type']=='weight'
                                                                            &&_mealsCart[index]['count']
                                                                            > double.parse(_mealsCart[index]['products_lowest_weight_available']))
                                                                        {
                                                                          _mealsCart[index]['count']
                                                                          -=  double.parse(_mealsCart[index]['products_lowest_weight_available']);
                                                                        }
                                                                        else if (_mealsCart[index]['count'] > 1){
                                                                          _mealsCart[index]['count']--;
                                                                        }
                                                                        UpdateList(
                                                                            _mealsCart);
                                                                      });
                                                                  },
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets
                                                                      .only(
                                                                      right: 15,
                                                                      left: 15),
                                                                  child: Text(_mealsCart[index]['products_type']=='weight'?
                                                                      _mealsCart[index]['count'].toString()
                                                                      :(double.parse("${_mealsCart[index]['count']}").round()).toString(),
                                                                    style: TextStyle(
                                                                      fontFamily: Lang,
                                                                      fontWeight: FontWeight
                                                                          .normal,
                                                                      fontSize: SystemControls
                                                                          .font3,
                                                                      height: 1.2,
                                                                    ),
                                                                    textAlign: TextAlign
                                                                        .center,
                                                                    maxLines: 2,
                                                                  ),
                                                                ),
                                                                InkWell(
                                                                  child: Container(
                                                                    width: 30,
                                                                    height: 30,
                                                                    decoration: BoxDecoration(
                                                                      color: globals.accountColorsData['BGColor']!=null
                                                                          ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
                                                                      borderRadius: BorderRadius
                                                                          .all(
                                                                          Radius
                                                                              .circular(
                                                                              SystemControls
                                                                                  .RadiusCircValue /
                                                                                  2)),
                                                                    ),
                                                                    child: Center(
                                                                      child: Icon(
                                                                          Icons
                                                                              .add,
                                                                          color: Colors
                                                                              .black //SystemControls.MainColor,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  onTap: () {
                                                                    print(
                                                                        "on click button incr quantity ");
                                                                    setState(() {
                                                                      if(_mealsCart[index]['products_type']=='weight'){
                                                                        _mealsCart[index]['count']
                                                                            +=  double.parse(_mealsCart[index]['products_lowest_weight_available']);
                                                                      }
                                                                      else {
                                                                        _mealsCart[index]['count']++;
                                                                      }
                                                                      UpdateList(
                                                                          _mealsCart);
                                                                    });
                                                                  },
                                                                ),
                                                              ],
                                                            ),
                                                            Expanded(
                                                              flex: 1,
                                                              child: Row(
                                                                mainAxisAlignment: MainAxisAlignment
                                                                    .end,
                                                                crossAxisAlignment: CrossAxisAlignment
                                                                    .end,
                                                                children: <
                                                                    Widget>[
                                                                  IconButton(
                                                                      icon: Icon(
                                                                        Icons.delete_outline,),
                                                                      onPressed: () {
                                                                        print(
                                                                            "on delete from cart.");
                                                                        setState(() {
                                                                          _mealsCart.removeAt(index);
                                                                          UpdateList(_mealsCart);


                                                                          cartState.setCurrentProductsCount(_mealsCart.length.toString());
                                                                        });
                                                                      }),
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  )
                                              ),
                                            )
                                          ],
                                        ),
                                      )
                                  ),
                                );
                              }
                          ),
                        ):
                        Padding(
                          padding: EdgeInsets.all(20),
                          child: Center(
                            child: Text('emptyListcart'.tr().toString(),
                              style: TextStyle(
                                fontFamily: Lang,
                                fontWeight: FontWeight.normal,
                                fontSize: SystemControls.font2,
                                height: 1.2,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),

                        _mealsCart.length>0||
                            _imageFile != null ||
                            _imgReordername != ""?
                        Container(
                          margin: EdgeInsets.only(top: 10,bottom: 10),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            scrollPadding:  EdgeInsets.only(bottom: 185),
                            keyboardType: TextInputType.text,
                            controller: notesTextField,
                            onTap: (){
                              setState(() {
                                _controller.animateTo(
                                  _controller.position.maxScrollExtent,
                                  duration: Duration(seconds: 2),
                                  curve: Curves.fastOutSlowIn,
                                );
                              });
                            },

                            //decoration: InputFieldDecoration(appLocalizations.translate("orderAddressNotes"), Lang),
                            decoration: InputDecoration(

                              labelText: "orderIteamsNotes".tr().toString(),
                              labelStyle: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.normal,
                                  fontSize: SystemControls.font3,
                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                              ),
                              fillColor: Colors.white,
                              contentPadding: EdgeInsets.only(right: 10,left: 10,top: 16,bottom: 0),
                              border: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                                borderSide: new BorderSide(),
                              ),
                              enabledBorder: const OutlineInputBorder(
                                borderSide: const BorderSide(color: Colors.black, width: 0.0),
                              ),
                              alignLabelWithHint: true,
                            ),
                            maxLines: 3,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),

                          ),
                        )

                        :Container(),
                        SizedBox(height:14.h ,)
                  //ِItems note
                      ],
                    ),
                  ),

                  FutureBuilder<ResponseApi>(
                      future: respAdds,
                      builder: (context, snap) {
                        if (snap.hasData) {
                          if (snap.data.status == 200) {
                            dynamic dataa = snap.data.data;
                            _addreses = dataa['addresses'];
                          }
                          else {
                            _addreses = [];
                          }
                          return Container();
                        }
                        if (snap.hasError) {
                          _addreses = [];
                        }
                        return Center(
                          child: SystemControls().circularProgress(),
                        );
                      }
                  ),

                 Stack(
                    alignment: Alignment.bottomCenter,
                    children: <Widget>[
                      Container(
                        color: globals.accountColorsData['BGColor']!=null
                            ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
                        //color: Colors.cyan,
                        alignment: Alignment.bottomCenter,
                        height: (globals.show_branches=="1"||globals.show_areas=="1")?25.h:    hasCoupon =="1"?  33.h:25,
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        margin: EdgeInsets.only(right: 10, left: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[


                            SizedBox(height: 1.h,),
                            hasCoupon =="1"?         Container(
                              width: MediaQuery.of(context).size.width,
                              color: Colors.white,
                              padding: const EdgeInsets.only(left: 0.0,right: 0),

                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [

                                  Container(
                                    color: Colors.white,
                                    width: MediaQuery.of(context).size.width/1.3,
                                    child: TextField(
                                      cursorColor: Colors.black,
                                      controller: cartState.couponController,
                                      decoration: InputDecoration(
                                        prefixText: "    ",
                                        hintText: "coupon".tr().toString(),
                                        errorMaxLines: 1,
                                        errorStyle: TextStyle(
                                            fontSize: 0, height: 0),
                                        isDense: false,
                                        // prefixIcon: Icon(Icons.search,
                                        //     color: Colors.black),
                                        filled: true,
                                        contentPadding: EdgeInsets.all(3),
                                        fillColor: globals
                                            .accountColorsData[
                                        'BorderColor'] !=
                                            null
                                            ? Color(int.parse(globals
                                            .accountColorsData[
                                        'BorderColor']))
                                            .withOpacity(.2)
                                            : SystemControls.BorderColorz,
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: globals
                                                    .accountColorsData[
                                                'BorderColor'] !=
                                                    null
                                                    ? Color(int.parse(globals
                                                    .accountColorsData[
                                                'BorderColor']))
                                                    .withOpacity(.2)
                                                    : SystemControls.BorderColorz,
                                                width: 1),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8))),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: globals
                                                    .accountColorsData[
                                                'BorderColor'] !=
                                                    null
                                                    ? Color(int.parse(globals
                                                    .accountColorsData[
                                                'BorderColor']))
                                                    .withOpacity(.2)
                                                    : SystemControls.BorderColorz,
                                                width: 1),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8))),
                                        focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: globals
                                                    .accountColorsData[
                                                'BorderColor'] !=
                                                    null
                                                    ? Color(int.parse(globals
                                                    .accountColorsData[
                                                'BorderColor']))
                                                    .withOpacity(.2)
                                                    : SystemControls.BorderColorz,
                                                width: 1),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8))),
                                        errorBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: globals
                                                    .accountColorsData[
                                                'BorderColor'] !=
                                                    null
                                                    ? Color(int.parse(globals
                                                    .accountColorsData[
                                                'BorderColor']))
                                                    .withOpacity(.2)
                                                    : SystemControls.BorderColorz,
                                                width: 1),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(8))),
                                        focusedErrorBorder:
                                        OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: globals
                                                    .accountColorsData[
                                                'BorderColor'] !=
                                                    null
                                                    ? Color(int.parse(globals
                                                    .accountColorsData[
                                                'BorderColor']))
                                                    .withOpacity(.2)
                                                    : SystemControls.BorderColorz,
                                                width: 1),
                                            borderRadius:
                                            BorderRadius.all(
                                                Radius.circular(
                                                    12))),
                                      ),
                                    ),
                                  ),

                                  Spacer(),
                                  InkWell(
                                    child: Container(
                                      height: 40,
                                      width: 40,
                                      margin: EdgeInsetsDirectional.fromSTEB(
                                          10, 0, 0, 0),
                                      decoration: BoxDecoration(
                                        color: globals.accountColorsData[
                                        'MainColor'] !=
                                            null
                                            ? Color(int.parse(
                                            globals.accountColorsData[
                                            'MainColor']))
                                            : SystemControls.MainColorz,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey,
                                            blurRadius:
                                            2.0, // has the effect of softening the shadow
                                            spreadRadius:
                                            0.5, // has the effect of extending the shadow
                                            offset: Offset(
                                              2, // horizontal, move right 10
                                              3.0, // vertical, move down 10
                                            ),
                                          )
                                        ],
                                      ),
                                      child: Center(
                                        child: Icon(
                                          Icons.send,
                                          color: globals.accountColorsData[
                                          'TextOnMainColor'] !=
                                              null
                                              ? Color(int.parse(
                                              globals.accountColorsData[
                                              'TextOnMainColor']))
                                              : SystemControls.TextOnMainColorz,
                                          size: 20,
                                        ),
                                      ),
                                    ),
                                    onTap: () {
                                      if(FocusScope.of(context).isFirstFocus) {
                                        FocusScope.of(context).requestFocus(new FocusNode());
                                      }
                                      if (UserToken == "") {
                                        ErrorDialogAlertGoTOLogin(context,
                                            "mustLogin".tr().toString(),Lang);
                                      }else{
                                        ValidateCouponREpo(context: context,couponCode: cartState.couponController.value.text,appCurrency: widget.AppCurrency,totalPrice: "$total_price").validateCouponFun;
                                      }
                                    },
                                  ),
                                  Spacer(),
                                ],
                              ),
                            ):Container(),
                            SizedBox(height:     hasCoupon =="1"? 1.5.h:0,),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child:   Text("itemsPrice".tr().toString(),
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.normal,
                                        fontSize: SystemControls.font3,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
                                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                        height: 1.2,
                                      )
                                  ),
                                ),
                                Consumer<CartStateProvider>(builder: (context,cart,_){
                                  return Text(

                                          "$total_price " + AppCurrency,
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.normal,
                                        fontSize: SystemControls
                                            .font3,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
                                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                        height: 1.2,
                                      )
                                  );
                                })

                              ],
                            ),
                            SizedBox(height: 1.5.h,),
                            hasCoupon =="1"?         Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child:   Text("theCouponDiscount".tr().toString(),
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.normal,
                                        fontSize: SystemControls.font3,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
                                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                        height: 1.2,
                                      )
                                  ),
                                ),
                                Consumer<CartStateProvider>(builder: (context,cart,_){
                                  return Text(
                                      (cart.couponType !="" ? cart.couponType=="value"? cart.couponValue+" "+ widget.AppCurrency: cart.couponValue+" %":"0"),
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.normal,
                                        fontSize: SystemControls
                                            .font3,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
                                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                        height: 1.2,
                                      )
                                  );
                                })

                              ],
                            ): Container(),
                            SizedBox(height:     hasCoupon =="1"?  1.5.h:0,),

                            (globals.show_branches=="1" ||globals.show_areas=="1")?
                            Container()
                                :Column(
                              children: [
                                Padding(padding: EdgeInsets.all(3)),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 1,
                                      child: Text("deliveryFees".tr().toString(),
                                          style: TextStyle(
                                            fontFamily: Lang,
                                            fontWeight: FontWeight.normal,
                                            fontSize: SystemControls.font3,
                                            color: globals.accountColorsData['TextHeaderColor']!=null
                                                ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                            height: 1.2,
                                          )
                                      ),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment
                                            .end,
                                        crossAxisAlignment: CrossAxisAlignment
                                            .end,
                                        children: <Widget>[
                                          Text(globals.deliveryCost.toStringAsFixed(globals.numberDecimalDigits) + " " + AppCurrency,
                                              style: TextStyle(
                                                fontFamily: Lang,
                                                fontWeight: FontWeight.normal,
                                                fontSize: SystemControls
                                                    .font3,
                                                color: globals.accountColorsData['TextHeaderColor']!=null
                                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                                height: 1.2,
                                              )
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(height: 1.5.h,),

                                Container(
                                  height: 1,
                                  color: globals.accountColorsData['TextdetailsColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                ),

                                SizedBox(height: 1.5.h,),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 1,
                                      child: Text("TotalPrice".tr().toString(),
                                          style: TextStyle(
                                            fontFamily: Lang,
                                            fontWeight: FontWeight.normal,
                                            fontSize: SystemControls.font3,
                                            color: globals.accountColorsData['TextHeaderColor']!=null
                                                ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                            height: 1.2,
                                          )
                                      ),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment
                                            .end,
                                        crossAxisAlignment: CrossAxisAlignment
                                            .end,
                                        children: <Widget>[
                                          Consumer<CartStateProvider>(builder: (context,cart,_){
                                            return  Text(( (cart.couponType !="" ? cart.couponType=="value"?(total_price - double.parse(cart.couponValue))+globals.deliveryCost: (total_price-((total_price * double.parse(cart.couponValue))/100))+globals.deliveryCost:total_price +

                                                globals.deliveryCost)<=0?0:cart.couponType !="" ? cart.couponType=="value"?(total_price - double.parse(cart.couponValue))+globals.deliveryCost: (total_price-((total_price * double.parse(cart.couponValue))/100))+globals.deliveryCost:total_price +

                                                globals.deliveryCost).toStringAsFixed(globals.numberDecimalDigits) + " " +
                                                AppCurrency,
                                                style: TextStyle(
                                                  fontFamily: Lang,
                                                  fontWeight: FontWeight.normal,
                                                  fontSize: SystemControls
                                                      .font3,
                                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                                  height: 1.2,
                                                )
                                            );
                                          })
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),

                            SizedBox(height: 1.5.h,),

                            Container(
                              width: MediaQuery
                                  .of(context)
                                  .size
                                  .width,
                              height: 50,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(10)),
                                  color: globals.accountColorsData['MainColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz
                              ),
                              child: FlatButton(
                                onPressed: () {
                                  if (UserToken == "") {
                                    ErrorDialogAlertGoTOLogin(context,
                                        "mustLogin".tr().toString(),Lang);
                                  }
                                  else if (globals.minOrderCost > total_price
                                      && _imageFile == null && _imgReordername == "") {
                                    String WarningMessage =
                                        "minOrderCostWarningMessage".tr().toString()
                                            + globals.minOrderCost.toString()
                                            + " " + AppCurrency;
                                    ErrorDialogAlert(context, WarningMessage);
                                  }
                                  else if(_mealsCart.length <=0
                                      && _imageFile == null && _imgReordername == ""){
                                    print("empty");
                                  }
                                  else {
                                    if(globals.show_branches=="1"||globals.show_areas=="1"){
                                      Navigator.push(context,
                                          MaterialPageRoute(
                                              builder: (
                                                  BuildContext context) =>
                                                  CartSelectBranchLayout(
                                                      Lang,
                                                      UserToken,
                                                      AppCurrency,
                                                      _mealsCart,
                                                      design_Control,
                                                      _addreses,
                                                      latitude,
                                                      longitude,
                                                      notesTextField.text)
                                          )
                                      );
                                    }
                                    else {
                                      Navigator.push(context,
                                          MaterialPageRoute(
                                              builder: (
                                                  BuildContext context) =>
                                                  CartOrderLayout(
                                                      Lang,
                                                      UserToken,
                                                      AppCurrency,
                                                      _mealsCart,
                                                      design_Control,
                                                      _addreses,
                                                      latitude,
                                                      longitude,
                                                      notesTextField.text,0)
                                          )
                                      );
                                    }
                                  }
                                },
                                child: Text("orderBut".tr().toString(),
                                    style: TextStyle(
                                      fontFamily: Lang,
                                      fontWeight: FontWeight.normal,
                                      fontSize: SystemControls.font3,
                                      color: globals.accountColorsData['TextOnMainColor']!=null
                                          ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                                      height: 1.2,
                                    )
                                ),
                              ),
                            ),
                            Padding(padding: EdgeInsets.all(3)),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              );
            }
            else if(snap.hasError ||snap.connectionState == ConnectionState.done){
              return Container(
                margin: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 160),
                child: Column(
                  children: <Widget>[
                    ImagPickerWidget(),

                    Container(
                      padding: EdgeInsets.only(top: 0),
                      width: MediaQuery.of(context).size.width,
                      child: Text("cartProductsTitle".tr().toString(),
                          style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.bold,
                            fontSize: SystemControls.font3,
                            color: globals.accountColorsData['TextHeaderColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                            height: 1.2,
                          )
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(20),
                      child: Text('emptyListcart'.tr().toString(),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font2,
                          height: 1.2,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    )
                  ],
                ),
              );
            }
            return Center(
              child: SystemControls().circularProgress(),
            );
          }
      ),
    );
  }

  ImagPickerWidget(){
    if(SystemControls.cartAddPhoto){
      /*return Container(
          height: 100,
          decoration: BoxDecoration(
            color: Colors.white,
            //Theme.of(context).primaryColor,
            borderRadius: BorderRadius.all(
                Radius.circular(SystemControls
                    .RadiusCircValue)),
          ),
          margin: EdgeInsets.only(top: 5,bottom: 5),
          child: ClipRRect(
            borderRadius: BorderRadius.all(
                Radius.circular(SystemControls
                    .RadiusCircValue)),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    child: ClipRRect(
                      borderRadius: BorderRadius
                          .all(Radius.circular(
                          SystemControls
                              .RadiusCircValue)),
                      child: Center(
                        child: !kIsWeb && defaultTargetPlatform == TargetPlatform.android
                            ? FutureBuilder<void>(
                          future: retrieveLostData(_picker),
                          builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
                            switch (snapshot.connectionState) {
                              case ConnectionState.done:
                                return Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      child: _previewImage(_imageFile),
                                    ),
                                  ],
                                );
                              default:
                                if (snapshot.hasError) {
                                  return Text(
                                    'Pick image/video error: ${snapshot.error}}',
                                    textAlign: TextAlign.center,
                                  );
                                } else {
                                  return Text(
                                    'Pick image not pick yet',
                                    textAlign: TextAlign.center,
                                  );
                                }
                            }
                          },
                        )
                            : (_previewImage(_imageFile)),
                      ),
                    ),
                  ),
                ),
                Padding(padding: EdgeInsets.all(5)),
                Expanded(
                  flex: 2,
                  child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            child: IconButton(
                                icon: Icon(_imageFile==null ?
                                Icons.add_photo_alternate:Icons.replay,),
                                onPressed: () {
                                  //reload img
                                  showBottomSelectImgWidget(context: context);
                                }),
                          ),
                          Container(
                            child: IconButton(
                                icon: Icon(
                                  Icons.delete_outline,),
                                onPressed: () {
                                  setState(() {
                                    _imageFile = null;
                                    CartState cartState = Provider.of<CartState>(context);
                                    cartState.setImgFilePicker(_imageFile);
                                  });
                                }),
                          ),
                        ],
                      )
                  ),
                )
              ],
            ),
          )
      );*/
      return Container(
        margin: EdgeInsets.only(top: 10, bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Text('AddPicToCart'.tr().toString(),
                  style: TextStyle(
                    fontFamily: Lang,
                    fontWeight: FontWeight.bold,
                    fontSize: SystemControls.font3,
                    color: globals.accountColorsData['TextHeaderColor']!=null
                        ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                    height: 1.2,
                  )
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 10)),
            _imageFile==null && _imgReordername == ""?
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 120,
                    height: 120,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                            Radius.circular(60)),
                        color: globals.accountColorsData['MainColor']!=null
                            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz
                    ),
                    child: FlatButton(
                      onPressed: () {
                        showBottomSelectImgWidget(context: context);
                      },
                      child: Icon(Icons.add_photo_alternate,
                        size: 50,color: Colors.white,),
                    ),
                  )
                ],
              ),
            ):
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  InkWell(
                    onTap: (){
                      _FullImgScreen(context);
                    },
                    child: Container(
                      height: 120,
                      child: ClipRRect(
                        borderRadius: BorderRadius
                            .all(Radius.circular(
                            SystemControls
                                .RadiusCircValue)),
                        child: Center(
                          child: !kIsWeb && defaultTargetPlatform == TargetPlatform.android
                              ? FutureBuilder<void>(
                            future: retrieveLostData(_picker),
                            builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
                              switch (snapshot.connectionState) {
                                case ConnectionState.done:
                                  return Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        child: _previewImage(_imageFile),
                                      ),
                                    ],
                                  );
                                default:
                                  if (snapshot.hasError) {
                                    return Text(
                                      'Pick image/video error: ${snapshot.error}}',
                                      textAlign: TextAlign.center,
                                    );
                                  } else {
                                    return Text(
                                      'Pick image not pick yet',
                                      textAlign: TextAlign.center,
                                    );
                                  }
                              }
                            },
                          )
                              : (_previewImage(_imageFile)),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),
                    child: IconButton(
                        icon: Icon(
                          Icons.delete_outline,),
                        onPressed: () {
                          setState(() {
                            _imageFile = null;
                            _imgReordername = "";
                            CartStateProvider cartState = Provider.of<CartStateProvider>(context,listen: false);
                            cartState.setImgFilePicker(_imageFile);
                            cartState.setCartImgName("");
                          });
                        }),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    }
    else{
      return Container();
    }
  }
  ////////////////////////////////Img picker part
//TODO
  PickedFile _imageFile;
  String _imgReordername;
  final ImagePicker _picker = ImagePicker();

  Future<void> showBottomSelectImgWidget({BuildContext context}){
    return showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            color: Color(0xFF737373),
            height: 180,
            child: Container(
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Text("imgPickerHeader".tr().toString(),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.bold,
                          fontSize: SystemControls.font3,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                          height: 1.2,
                        )
                    ),
                  ),
                  ListTile(
                      leading: Icon(Icons.photo_library),
                      title: Text("imgPickerGallery".tr().toString()),
                      onTap: (){
                        print('Select From gallery');
                        _onImageButtonPressed(ImageSource.gallery);
                        Navigator.pop(context);
                      }
                  ),
                  ListTile(
                      leading: Icon(Icons.camera_alt),
                      title: Text("imgPickerCamera".tr().toString()),
                      onTap: (){
                        print('Take Photo With Camera');
                        _onImageButtonPressed(ImageSource.camera);
                        Navigator.pop(context);
                      }
                  ),
                ],
              ),
              decoration: BoxDecoration(
                color: Theme.of(context).canvasColor,
                borderRadius: BorderRadius.only(
                  topLeft: const Radius.circular(10),
                  topRight: const Radius.circular(10),
                ),
              ),
            ),
          );
        });
  }

  void _onImageButtonPressed(ImageSource source) async {
            try {
              final pickedFile = await _picker.getImage(
                source: source,
              );
              setState(() {
                if(pickedFile.path != null){
                _imageFile = pickedFile;
                }
              });
              CartStateProvider cartState = Provider.of<CartStateProvider>(context,listen: false);
              cartState.setImgFilePicker(_imageFile);

            } catch (e) {
              print("Image picker error : ");
              print(e);
            }
  }

  Widget _previewImage(PickedFile _imageFile) {
    if(_imgReordername != ""){
      return CachedNetworkImage(
        imageUrl: SystemControls().GetImgPath(_imgReordername,"medium"),
        placeholder: (context, url) => Center(child: SystemControls().circularProgress(),),
        errorWidget: (context, url, error) => Center(child: Icon(Icons.broken_image),),
        fit: BoxFit.contain,
      );
    }
    else if (_imageFile != null) {
      if (kIsWeb) {
        // Why network?
        // See https://pub.dev/packages/image_picker#getting-ready-for-the-web-platform
        return Image.network(_imageFile.path);
      } else {
        return Image.file(File(_imageFile.path),fit: BoxFit.contain,);
      }
    }
    else {
      return const Text(
        '',
        textAlign: TextAlign.center,
      );
    }
  }

  Future<void> retrieveLostData(ImagePicker _picker) async {
    final LostData response = await _picker.getLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
        setState(() {
          _imageFile = response.file;
        });
    } else {
      print(response.exception.code);
    }
  }


  Future<void> _FullImgScreen(BuildContext context) {
    return showDialog<void>(
      context: context,
      //barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {

        return GestureDetector(
          child: Container(
            width: MediaQuery.of(context).size.width,
            color: Color.fromRGBO(0, 0, 0, 0.8),
            child:  Stack(
              children: <Widget>[
                GestureDetector(
                  child: Container(
                    child: ClipRRect(
                      borderRadius: BorderRadius
                          .all(Radius.circular(
                          SystemControls
                              .RadiusCircValue)),
                      child: Center(
                        child: !kIsWeb && defaultTargetPlatform == TargetPlatform.android
                            ? FutureBuilder<void>(
                          future: retrieveLostData(_picker),
                          builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
                            switch (snapshot.connectionState) {
                              case ConnectionState.done:
                                return Container(
                                  child: _previewImage(_imageFile),
                                );
                              default:
                                if (snapshot.hasError) {
                                  return Text(
                                    'Pick image/video error: ${snapshot.error}}',
                                    textAlign: TextAlign.center,
                                  );
                                } else {
                                  return Text(
                                    'Pick image not pick yet',
                                    textAlign: TextAlign.center,
                                  );
                                }
                            }
                          },
                        )
                            : (_previewImage(_imageFile)),
                      ),
                    ),
                  ),
                  onTap: () {
                    //Navigator.pop(context);
                  },
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    margin: EdgeInsets.all(20),
                    child: Icon(Icons.cancel,color: Colors.white,size: 30,),
                  ),
                )
              ],
            ),
          ),
          onTap: () {
            Navigator.pop(context);
          },
        );
      },
    );
  }

}
