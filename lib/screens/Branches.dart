import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'dart:convert';
import 'package:easy_localization/easy_localization.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';

import '../dataControl/dataModule.dart';
import '../Controls.dart';

//imppp4
import '../dataControl/cartModule.dart';
import '../screens/home.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Widgets_InputField.dart';

import '../globals.dart' as globals;

class BranchesLayout extends StatefulWidget{

  String Lang;
  String AppCurrency;
  String UserToken;
  int design_Control;
  BranchesLayout(this.Lang, this.UserToken,this.design_Control);

  _BranchesLayout createState() =>
      _BranchesLayout(this.Lang, this.UserToken,this.design_Control);
}

class _BranchesLayout extends State<BranchesLayout>{
  Future<ResponseApi> respAdds;
  String Lang;
  String UserToken;
  int design_Control;
  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
  }
  _BranchesLayout(this.Lang, this.UserToken,this.design_Control){
    respAdds = getUserAddressesApi(Lang,UserToken);
  }

  TextEditingController branchTextField = TextEditingController();
  String userLocationCity = "Not get";
  GoogleMapController mapController;
  Position _currentPosition;
  LatLng _center = LatLng(45.521563, -122.677433);
  final Map<String, Marker> _markers = {};

  _getCurrentLocation() async{
    Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        //To Get address text
        _getAddressFromLatLng();
        //Add current location to selected var
        _center = LatLng(_currentPosition.latitude,_currentPosition.longitude);
        //Add selected location to markers List
        _markers.clear();
        final marker = Marker(
          markerId: MarkerId("curr_loc"),
          position: LatLng(_currentPosition.latitude, _currentPosition.longitude),
          infoWindow: InfoWindow(title: 'Your Location'),
        );
        _markers["Current Location"] = marker;
        //Move map camera to selected location or target
        mapController.animateCamera(CameraUpdate.newCameraPosition(
          CameraPosition(
            bearing: 0,
            target: _center,
            zoom: 15.0,
          ),
        ));
      });

    }).catchError((e) {
      print(e);
    });
  }
  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await Geolocator().placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        userLocationCity =
            "${place.locality}, ${place.postalCode}, ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }
  int _selectedIndex = 0;

  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Scaffold(

      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          LayoutBG(design_Control),
          Container(
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 10,bottom: 10),
                  height: mediaQuery.size.height*0.4,
                  child: Center(
                    child: GoogleMap(
                      //onTap: _mapTappedSetLoca,
                      myLocationEnabled: true,
                      onMapCreated: _onMapCreated,
                      initialCameraPosition: CameraPosition(
                        target: _center,
                        zoom: 15.0,
                      ),
                      markers: _markers.values.toSet(),
                    ),
                  ),
                ),
              ],
            ),
          ), //Maps
          Container(
            margin: EdgeInsets.only(
                top: mediaQuery.size.height*0.4 - 100,
                left: 15,right: 15,bottom: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 180,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                          Radius.circular(SystemControls.RadiusCircValue)),
                      color: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz
                  ),
                  child: Container(
                    margin: EdgeInsets.all(20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(252, 234, 107, 1),
                            borderRadius: BorderRadius.all(
                                Radius.circular(SystemControls.RadiusCircValue)),
                          ),
                          padding: EdgeInsets.all(5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsetsDirectional.fromSTEB(20, 0, 0, 0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      "branchesChoose".tr().toString(),
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.bold,
                                        fontSize: SystemControls.font2,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
                                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                      ),
                                    ),
                                    Text(userLocationCity,
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.normal,
                                        fontSize: SystemControls.font3,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
                                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              CupertinoButton(
                                  child: RotatedBox(
                                    quarterTurns: -1,
                                    child: Icon(
                                      Icons.arrow_back_ios,
                                      color: Colors.black,
                                    ),
                                  ),//Icon(Icons.arrow_back_ios),
                                  onPressed: () {

                                  }),
                            ],
                          ),
                        ), //DropDown_Container
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(
                                Radius.circular(SystemControls.RadiusCircValue)),
                          ),
                          child: TextFormField(
                            controller: branchTextField,
                            decoration: InputFieldDecoration("branchSearch".tr().toString(), Lang),
                            keyboardType: TextInputType.text,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ), //Selection
              ],
            ),
          ), //Selection Area
          Container(
            margin: EdgeInsets.only(
                top: mediaQuery.size.height*0.4 + 90,
                left: 15,right: 15,bottom: 10),
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 10,top: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("50 "+
                          "branch".tr().toString(),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontSize: SystemControls.font2,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                          fontWeight: FontWeight.bold,
                          height: 1,
                        ),
                        maxLines: 1,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                ),
                ListView.builder(
                  shrinkWrap : true,
                  physics: ScrollPhysics(),
                  padding: EdgeInsets.zero,
                  itemCount: 5,
                  itemBuilder: (context,index){
                    return InkWell(
                      child: Container(
                        //height: 120,
                        margin: EdgeInsets.only(bottom: 15),
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(
                              Radius.circular(
                                  SystemControls.RadiusCircValue)),
                          child: Container(
                            color: Colors.white,
                            padding: EdgeInsets.all(20),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Expanded(
                                      flex: 7,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text("Jeddah Branch",
                                            style: TextStyle(
                                              fontFamily: Lang,
                                              fontSize: SystemControls.font2,
                                              color: globals.accountColorsData['TextHeaderColor']!=null
                                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                              fontWeight: FontWeight.bold,
                                              height: 1,
                                            ),
                                            maxLines: 1,
                                            textAlign: TextAlign.start,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(padding: EdgeInsets.all(4)),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(
                                      Icons.location_on,
                                      color: globals.accountColorsData['BGColor']!=null
                                          ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
                                      size: 25,
                                    ),
                                    Padding(padding: EdgeInsets.all(2)),
                                    Text("Address, Address, Address",
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontSize: SystemControls.font3,
                                        color: globals.accountColorsData['TextdetailsColor']!=null
                                            ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                        fontWeight: FontWeight.normal,
                                        height: 0.9,
                                      ),
                                      maxLines: 1,
                                      textAlign: TextAlign.start,
                                    ),
                                  ],
                                ),
                                Padding(padding: EdgeInsets.all(4)),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 5,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            Icons.phone,
                                            color: globals.accountColorsData['BGColor']!=null
                                                ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
                                            size: 25,
                                          ),
                                          Padding(padding: EdgeInsets.all(2)),
                                          Text("92007519",
                                            style: TextStyle(
                                              fontFamily: Lang,
                                              fontSize: SystemControls.font3,
                                              color: globals.accountColorsData['TextdetailsColor']!=null
                                                  ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                              fontWeight: FontWeight.normal,
                                              height: 0.9,
                                            ),
                                            maxLines: 1,
                                            textAlign: TextAlign.start,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                      flex: 5,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(
                                            Icons.access_time,
                                            color: globals.accountColorsData['BGColor']!=null
                                                ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
                                            size: 25,
                                          ),
                                          Padding(padding: EdgeInsets.all(2)),
                                          Text("2020-20-20 15:45:12",
                                            style: TextStyle(
                                              fontFamily: Lang,
                                              fontSize: SystemControls.font3,
                                              color: globals.accountColorsData['TextdetailsColor']!=null
                                                  ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                              fontWeight: FontWeight.normal,
                                              height: 0.9,
                                            ),
                                            maxLines: 1,
                                            textAlign: TextAlign.start,
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      onTap: () {},
                    );
                  },
                ),
              ],
            ),
          ), //List of Branches
        ],
      ),
    );
  }
}