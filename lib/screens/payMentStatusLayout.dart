import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import '../Controls.dart';

import 'package:easy_localization/easy_localization.dart';
import '../screens/register.dart';
import '../screens/forgetPassword.dart';
import '../screens/home.dart';
import '../screens/registerGetPhoneNumber.dart';

import '../dataControl/userModule.dart';
import '../locale/share_preferences_con.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';
import '../widgets/UI_Widgets_InputField.dart';
import '../globals.dart' as globals;
import 'HomePageScreenTheamTwo/HomePageScreenTheamTwo.dart';
import 'HomeTheamOne/HomePageScreenTheamOne.dart';

class paymentStatues extends StatelessWidget with SharePreferenceData{

  String Lang;
  int Statues;
  String statusMessage;
  paymentStatues(this.Lang,this.Statues,this.statusMessage);
  @override
  Widget build(BuildContext context) {
    print('paymentStatues');
    print(Statues);
    print('paymentStatuesddd');
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: globals.accountColorsData['BGColor']!=null
                  ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
              image: DecorationImage(
                  image: AssetImage("assets/SplashBG.png",),
                  fit: BoxFit.cover
              ),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Container(
              margin: EdgeInsets.all(20),
              width: MediaQuery.of(context).size.shortestSide,
              child: ListView(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 70),
                    height: 150,
                    child: Center(
                      child: Image.asset(Lang =="ar"? 'assets/logo.png':"assets/logoEn.png"),
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 60),
                    child: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color:Statues==0 ? Colors.red: Colors.green,
                        border: Border.all(color: Statues==0 ? Colors.red: Colors.green,)
                      ),
                      child: Center(
                        child: Statues==0?(
                            Icon(Icons.close,
                              color: Colors.white,
                              size: 70,
                            )
                        ):(
                            Icon(Icons.check_circle,
                              color: Colors.white,
                              size: 70,
                            )
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 15),
                    alignment: Alignment.center,
                    child: Text(statusMessage,

                      //"Full name",
                      style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.bold,
                          fontSize: SystemControls.font2,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 90),
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    height: 50,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                            Radius.circular(10)),
                        color: globals.accountColorsData['MainColor']!=null
                            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz
                    ),
                    child: FlatButton(
                      onPressed: () {
                        switch(SystemControls.theme){

                          case "1":
                            Navigator.of(context).popUntil((route) => route.isFirst);
                            Navigator.pushReplacement(context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) => HomePageScreenTheamOne(),)

                            );
                            break; case "2":
                          Navigator.of(context).popUntil((route) => route.isFirst);


                          Navigator.pushReplacement(context,
                              MaterialPageRoute(
                                builder: (BuildContext context) => HomePageScreenTheamTwo(),)

                          );
                          break;
                        }
                      },
                      child: Text("home".tr().toString(),
                        style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.bold,
                            fontSize: SystemControls.font2,
                            color: globals.accountColorsData['TextOnMainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}