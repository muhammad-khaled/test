import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import '../Controls.dart';

import 'package:easy_localization/easy_localization.dart';
import '../dataControl/userModule.dart';
import '../screens/home.dart';
import '../screens/updatePassword.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';
import '../widgets/UI_Widgets_InputField.dart';

import '../locale/share_preferences_con.dart';
import '../screens/updatePhoneNumber.dart';

import '../globals.dart' as globals;

class updateUser extends StatefulWidget{
  String Lang;
  String UserToken;
  int design_Control;
  updateUser(this.Lang,this.UserToken,this.design_Control);
  _updateUser createState() => _updateUser(
      this.UserToken,this.design_Control);
}
class _updateUser extends State<updateUser> with SharePreferenceData{

  String UserToken;
  int design_Control;
  _updateUser(this.UserToken,this.design_Control){
    GetCustomerData();
  }

  TextEditingController nameTextField = TextEditingController();
  TextEditingController emailTextField = TextEditingController();
  TextEditingController phoneTextField = TextEditingController();


  TextEditingController birthDayField = TextEditingController();
  TextEditingController birthMonthField = TextEditingController();
  TextEditingController birthYearField = TextEditingController();

  final FocusNode _nameFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _phoneFocus = FocusNode();
  bool verified ;
  String _phoneNumber="";
  String _countryCode="";
  GetCustomerData(){
    SystemControls().getDataFromPreferences("name").then((String name){
      nameTextField.text = name;
      SystemControls().getDataFromPreferences("email").then((String email){
        emailTextField.text = email;
        SystemControls().getDataFromPreferences("customers_phone").then((String phone){
          _phoneNumber = phone;
          SystemControls().getDataFromPreferences("customers_country_code").then((String code){
            _countryCode = code;
            phoneTextField.text = code+phone;
          });
          SystemControls().getDataFromPreferences("email_verified").then(( value){
            print('email_verified');
            print(value);
            verified = value =="0" ?false :true;
            print('email_verified');
          });

          SystemControls().getDataFromPreferences("customers_gender").then((String gender){
            print(gender + "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
            setState(() {
              if(gender=="male"){
                _isRadioSelected = 0;
              }
              else if(gender =="female"){
                _isRadioSelected = 1;
              }
            });

            SystemControls().getDataFromPreferences("customers_birthday").then((String birthday){
              print(birthday + "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
              var arr = birthday.split('-');
              if(arr.length==3) {
                birthYearField.text = arr[0];
                birthMonthField.text = arr[1];
                birthDayField.text = arr[2];
              }
            });
          });
        });
      });
    });
  }
  int _isRadioSelected = -1;
  void _handleRadioValueChange(int value) {
    setState(() {
      _isRadioSelected = value;
    });
  }


  @override
  Widget build(BuildContext context) {
    print("widget.Lang");
    print(widget.Lang);
    print(widget.Lang);

    //nameTextField.text = "name";
    return Scaffold(
      appBar: AppBar(
        title: Text("profile".tr().toString()),
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
      ),
      body: Stack(
        children: <Widget>[
          LayoutBG(design_Control),
          Align(
            alignment: Alignment.center,
            child: Container(
              margin: EdgeInsets.all(20),
              width: MediaQuery.of(context).size.shortestSide,
              alignment: Alignment.center,
              child: ListView(
                //mainAxisAlignment: MainAxisAlignment.start,
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 0,bottom: 20),
                    child: Text("profile".tr().toString(),
                      style: TextStyle(
                          fontFamily: widget.Lang,
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font2,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                      ),
                    ),
                  ),

                  SystemControls.designControl==2?(
                      Container()
                  ):Row(
                    children: <Widget>[
                      Expanded(
                        flex: 3,
                        child: Container(
                          //margin: EdgeInsets.only(top: 0,bottom: 20),
                          child: Text("changePassword".tr().toString(),//"Full name",
                            style: TextStyle(
                                fontFamily: widget.Lang,
                                fontWeight: FontWeight.bold,
                                fontSize: SystemControls.font2,
                                color: globals.accountColorsData['TextHeaderColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                            ),
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.all(5)),
                      Expanded(
                        flex: 2,
                        child: Container(
                          margin: EdgeInsets.only(top: 10),
                          decoration: BoxDecoration(
                            color: globals.accountColorsData['MainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: FlatButton(
                            onPressed: () async{
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (BuildContext context) =>
                                      updatePassword(widget.Lang,UserToken,design_Control)
                                  ));
                            },
                            child: Center(
                              child: Text(
                               "change".tr().toString(),
                                style: TextStyle(
                                    fontFamily: widget.Lang,
                                    fontWeight: FontWeight.normal,
                                    fontSize: SystemControls.font2,
                                    color: globals.accountColorsData['TextOnMainColor']!=null
                                        ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 15),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: TextFormField(
                      controller: nameTextField,
                      focusNode: _nameFocus,
                      decoration: InputFieldDecoration("name".tr().toString(), widget.Lang),
                      keyboardType: TextInputType.text,
                      onFieldSubmitted: (_){
                        _nameFocus.unfocus();
                        FocusScope.of(context).requestFocus(_emailFocus);
                      },
                      style: new TextStyle(
                        fontFamily: widget.Lang,
                      ),
                    ),
                  ), //name
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: TextFormField(
                      controller: emailTextField,
                      focusNode: _emailFocus,
                      decoration: InputFieldDecoration("email".tr().toString(), widget.Lang),
                      keyboardType: TextInputType.text,
                      onFieldSubmitted: (_){
                        _emailFocus.unfocus();
                        //FocusScope.of(context).requestFocus(_phoneFocus);
                      },
                      style: new TextStyle(
                        fontFamily: widget.Lang,
                      ),
                    ),
                  ),

          verified == false?  Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(0.0),
              child: Row(
                children: <Widget>[
                  new Checkbox(value: verified,
                      activeColor:   globals.accountColorsData['MainColor'] != null
                          ? Color(int.parse(
                          globals.accountColorsData['MainColor']))
                          : SystemControls.MainColorz,
                      onChanged:(bool newValue){
                        setState(() {
                          verified = newValue;
                        });

                      }),
              Text('verified'.tr().toString()) ,Spacer()
                ],
              ),
            ):Container(),
                  //email
                  /*Container(
                    margin: EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: TextFormField(
                      enabled: false,
                      controller: phoneTextField,
                      focusNode: _phoneFocus,
                      decoration: InputFieldDecoration(AppLocalizations.of(context).translate("phone"), Lang),
                      keyboardType: TextInputType.text,
                      onFieldSubmitted: (_){
                        _phoneFocus.unfocus();
                      },
                      style: new TextStyle(
                        fontFamily: Lang,
                      ),
                    ),
                  ), //phone
                  */
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 3,
                        child: Container(
                          margin: EdgeInsets.only(top: 10),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            enabled: false,
                            controller: phoneTextField,
                            focusNode: _phoneFocus,
                            decoration: InputFieldDecoration("phone".tr().toString(), widget.Lang),
                            keyboardType: TextInputType.text,
                            onFieldSubmitted: (_){
                              _phoneFocus.unfocus();
                            },
                            style: new TextStyle(
                              fontFamily: widget.Lang,
                            ),
                          ),
                        ), //phone
                      ),
                      Padding(padding: EdgeInsets.all(5)),
                      Expanded(
                        flex: 2,
                        child: Container(
                          margin: EdgeInsets.only(top: 10),
                          decoration: BoxDecoration(
                            color: globals.accountColorsData['MainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: FlatButton(
                            onPressed: () async{
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (BuildContext context) =>
                                  updatePhoneNumber(
                                      widget.Lang,UserToken,design_Control,verified)
                                  ));
                            },
                            child: Center(
                              child: Text(
                               "change".tr().toString(),
                                style: TextStyle(
                                    fontFamily: widget.Lang,
                                    fontWeight: FontWeight.normal,
                                    fontSize: SystemControls.font2,
                                    color: globals.accountColorsData['TextOnMainColor']!=null
                                        ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),


                  Container(
                    margin: EdgeInsets.only(top: 15),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(
                              bottom: 20,
                            ),
                            child: Text("birthday".tr().toString()+"\n"
                                + "option".tr().toString(),
                              style: TextStyle(
                                  fontFamily: widget.Lang,
                                  fontWeight: FontWeight.bold,
                                  fontSize: SystemControls.font2,
                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            right: 10,
                            left: 10,
                          ),
                          width: 40,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            maxLength: 2,
                            maxLines: 1,
                            maxLengthEnforced: true,
                            autocorrect: false,
                            controller: birthDayField,
                            //focusNode: _listFocusNode[3],
                            decoration: InputFieldDecoration("day".tr().toString(), widget.Lang),
                            keyboardType: TextInputType.number,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontFamily: widget.Lang,
                            ),
                          ),
                        ), //DD
                        Container(
                          margin: EdgeInsets.only(
                            bottom: 20,
                          ),
                          child: Text("-",
                            style: TextStyle(
                                fontFamily: widget.Lang,
                                fontWeight: FontWeight.bold,
                                fontSize: SystemControls.font2,
                                color: globals.accountColorsData['TextHeaderColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            right: 10,
                            left: 10,
                          ),
                          width: 45,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            maxLength: 2,
                            maxLines: 1,
                            maxLengthEnforced: true,
                            autocorrect: false,
                            controller: birthMonthField,
                            //focusNode: _listFocusNode[3],
                            decoration: InputFieldDecoration("month".tr().toString(), widget.Lang),
                            keyboardType: TextInputType.number,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontFamily: widget.Lang,
                            ),
                          ),
                        ), //MM
                        Container(
                          margin: EdgeInsets.only(
                            bottom: 20,
                          ),
                          child: Text("-",
                            style: TextStyle(
                                fontFamily: widget.Lang,
                                fontWeight: FontWeight.bold,
                                fontSize: SystemControls.font2,
                                color: globals.accountColorsData['TextHeaderColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            right: 10,
                            left: 10,
                          ),
                          width: 70,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            maxLength: 4,
                            maxLines: 1,
                            maxLengthEnforced: true,
                            autocorrect: false,
                            controller: birthYearField,
                            //focusNode: _listFocusNode[3],
                            decoration: InputFieldDecoration("year".tr().toString(), widget.Lang),
                            keyboardType: TextInputType.number,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontFamily: widget.Lang,
                            ),
                          ),
                        ), //YYYY
                      ],
                    ),
                  ), //date of birthday
                  Container(
                    margin: EdgeInsets.only(top: 15),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            child: Text("gender".tr().toString()+"\n"
                                + "option".tr().toString(),
                              style: TextStyle(
                                  fontFamily: widget.Lang,
                                  fontWeight: FontWeight.bold,
                                  fontSize: SystemControls.font2,
                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                              ),
                            ),
                          ),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                new Radio(
                                  value: 0,
                                  activeColor: globals.accountColorsData['MainColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                                  groupValue: _isRadioSelected,
                                  onChanged: _handleRadioValueChange,
                                ),
                                new Text("male".tr().toString(),
                                  style: new TextStyle(fontSize: 16.0),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                new Radio(
                                  value: 1,
                                  activeColor: globals.accountColorsData['MainColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                                  groupValue: _isRadioSelected,
                                  onChanged: _handleRadioValueChange,
                                ),
                                new Text("female".tr().toString(),
                                  style: new TextStyle(fontSize: 16.0),
                                ),
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ), //Gender

                  Container(
                    margin: EdgeInsets.only(top: 30),
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: FlatButton(
                      onPressed: () async{
                        print(widget.Lang);
                        print(widget.Lang);
                        SystemControls()
                            .storeDataToPreferences("lang", "${widget.Lang}");
                        String gender = " ";
                        if(_isRadioSelected == 0){
                          gender = "male";
                        }
                        else if(_isRadioSelected == 1){
                          gender = "female";
                        }
                        String birthDate="";
                        if(birthDayField.text.isNotEmpty && birthMonthField.text.isNotEmpty &&
                            birthYearField.text.isNotEmpty){
                          birthDate = birthYearField.text +"-"+birthMonthField.text
                              + "-" + birthDayField.text;
                        }
                        if(nameTextField.text.isEmpty || phoneTextField.text.isEmpty){
                          print("empity");
                          ErrorDialogAlert(context,
                             "AddAllData".tr().toString());
                        }
                        else{
                          print(nameTextField.text);
                          var get = await updateUserApi(UserToken, widget.Lang, nameTextField.text,
                              emailTextField.text, _phoneNumber,birthDate,gender,_countryCode,verified);

                          if(get == null){
                            ErrorDialogAlert(context,
                               "respError".tr().toString());
                          }
                          else{
                            dynamic cu = get.message;
                            print(get.message);
                            if(get.errors == null && get.status == 200){
                              SystemControls().storeDataToPreferences("lang", "${widget.Lang}");
                              storeDataToPreferences("name",nameTextField.text);
                              storeDataToPreferences("email",emailTextField.text);
                              storeDataToPreferences("customers_phone",_phoneNumber);
                              storeDataToPreferences("customers_country_code",_countryCode);
                              storeDataToPreferences("customers_birthday",birthDate);
                              storeDataToPreferences("customers_gender",gender);
                              setLoggingStatus(true);
                              SuccDialogAlertBackHome(context, widget.Lang, get.message);
                            }
                            else if(get.status == 401){
                              SystemControls().LogoutSetUserData(context, widget.Lang);
                              ErrorDialogAlertBackHome(context, get.message);
                            }
                            else if(get.errors != null){
                              String ErrorMes="";
                              for(int i=0;i<get.errors.length;i++){
                                ErrorMes += get.errors[i]+"\n";
                              }
                              ErrorDialogAlert(context, ErrorMes);
                            }
                            else{
                              ErrorDialogAlert(context, get.message);
                            }
                          }

                        }
                      },
                      child: Center(
                        child: Text(
                          "change".tr().toString(),
                          style: TextStyle(
                              fontFamily: widget.Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font2,
                              color: globals.accountColorsData['TextOnMainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
                          ),
                        ),
                      ),
                    ),
                  ), //change
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}