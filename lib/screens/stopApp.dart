import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../Controls.dart';

import '../globals.dart' as globals;

String Title;
String Lang;
class stopApp extends StatefulWidget {

  stopApp(String title,String lang){
    Title = title;
    Lang = lang;
  }
  _stopApp createState() => _stopApp();
}

class _stopApp extends State<stopApp>{

  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Container(
          child: Center(
            child: Image.asset(Lang =="ar"? 'assets/logo.png':"assets/logoEn.png",
              fit: BoxFit.fitHeight,
              height: 50,
            ),
          ),
        ),
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
      ),
      body: Center(
        child: Container(
          margin: EdgeInsets.all(30),
          child: Text("This application doesn't work now.\n Please try again later.",
            style: TextStyle(
            fontFamily: "en",
            fontWeight: FontWeight.normal,
            fontSize: SystemControls.font2,
            height: 1.2,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }

}