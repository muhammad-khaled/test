import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';

import '../dataControl/dataModule.dart';
import '../Controls.dart';

import 'package:easy_localization/easy_localization.dart';
//imppp4
import '../dataControl/cartModule.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';
import '../widgets/UI_Widgets_InputField.dart';
import '../screens/myAddresses.dart';
import '../screens/editAddressSubmit.dart';

import '../globals.dart' as globals;

class editAddress extends StatefulWidget{

  String Lang;
  String AppCurrency;
  String UserToken;
  int design_Control;
  dynamic addressDet;
  editAddress(this.Lang, this.UserToken,this.AppCurrency,
      this.design_Control,this.addressDet);

  _editAddress createState() =>
      _editAddress(this.Lang, this.UserToken,this.AppCurrency,
          this.design_Control,this.addressDet);
}

class _editAddress extends State<editAddress>{
  String Lang;
  String AppCurrency;
  String UserToken;
  int design_Control;
  dynamic addressDet;

  double latitude, longitude;
  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
  }
  _editAddress(this.Lang, this.UserToken,this.AppCurrency,
      this.design_Control, this.addressDet){

    String gps = addressDet['customer_addresses_gps'];
    var arr = gps.split(',');
    latitude = double.parse(arr[0]);
    longitude = double.parse(arr[1]);
  }

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  GoogleMapController mapController;
  LatLng _center = LatLng(45.521563, -122.677433);
  final Map<String, Marker> _markers = {};

  _getCurrentLocation() async{
    setState(() {
      _center = LatLng(latitude,longitude);
      //Add selected location to markers List
      _markers.clear();
      final marker = Marker(
        markerId: MarkerId("curr_loc"),
        position: LatLng(latitude,longitude),
        infoWindow: InfoWindow(title: 'Selected Location'),
      );
      _markers["Selected Location"] = marker;
      //Move map camera to selected location or target
      mapController.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(
          bearing: 0,
          target: _center,
          zoom: 15.0,
        ),
      ));
    });
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }
  //OnTab map Function
  _mapTappedSetLoca(LatLng sellectedLocation) {
    print(sellectedLocation);
    _center = sellectedLocation;
    setState(() {
      //On select location add marker
      _markers.clear();
      final marker = Marker(
        markerId: MarkerId("curr_loc"),
        position: sellectedLocation,
        infoWindow: InfoWindow(title: 'Your Location'),
      );
      _markers["Current Location"] = marker;
    });

  }

  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    var _mapWidget = Expanded(
      flex: 1,
      child: Container(
        margin: EdgeInsets.only(top: 10,bottom: 10),
        //height: mediaQuery.size.shortestSide-30,
        child: Center(
          child: GoogleMap(
            onTap: _mapTappedSetLoca,
            myLocationEnabled: true,
            onMapCreated: _onMapCreated,
            initialCameraPosition: CameraPosition(
              target: _center,
              zoom: 15.0,
            ),
            markers: _markers.values.toSet(),
          ),
        ),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text("editAddress".tr().toString()),
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
      ),
      body: Stack(
        //fit: StackFit.expand,
        children: <Widget>[
          LayoutBG(design_Control),
          Align(
            alignment: Alignment.center,
            child: Container(
              //width: mediaQuery.size.shortestSide,
              //height: mediaQuery.size.height,
              margin: EdgeInsets.only(top: 10,left: 10,right: 10,bottom: 10),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Column(
                      //shrinkWrap : true,
                      //physics: NeverScrollableScrollPhysics(),
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      //padding: EdgeInsets.zero,
                      children: <Widget>[
                        _mapWidget,

                        Padding(padding: EdgeInsets.all(6)),
                        Container(
                          width: mediaQuery.size.width,
                          height: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                              color: globals.accountColorsData['MainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz
                          ),
                          child: FlatButton(
                            onPressed: ()async{
                              String gps = _center.latitude.toString()
                                  + "," + _center.longitude.toString();
                              addressDet['customer_addresses_gps'] = gps;

                              print("Location before ** ########## "+ addressDet['customer_addresses_gps']);
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (BuildContext context) =>
                                      editAddressSubmit(Lang,UserToken,AppCurrency,design_Control,addressDet)
                                  ));
                            },
                            child: Text("next".tr().toString(),
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.normal,
                                  fontSize: SystemControls.font3,
                                  color: globals.accountColorsData['TextOnMainColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                                  height: 1.2,
                                )
                            ),
                          ),
                        ),
                        Padding(padding: EdgeInsets.all(10)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
/*
          Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              Container(
                color: SystemControls.BGColor,
                alignment: Alignment.bottomCenter,
                height: 120,
                width: mediaQuery.size.width,
              ),
              Container(
                width: mediaQuery.size.width*0.9,
                margin: EdgeInsets.only(right: 10,left: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(padding: EdgeInsets.all(6)),
                    Container(
                      width: mediaQuery.size.width,
                      height: 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: SystemControls.MainColor
                      ),
                      child: FlatButton(
                        onPressed: ()async{
                          String gps = _center.latitude.toString()
                              + "," + _center.longitude.toString();
                          String Default;
                          if(setDefault == false){
                            Default = "0";
                          }
                          else{
                            Default = "1";
                          }
                          if(addressTitleTextField.text.isNotEmpty &&
                              addressTextField.text.isNotEmpty){
                            var res = await editAddressApi(Lang, UserToken,
                                addressTextField.text, addressTitleTextField.text,
                                gps, Default,addressDet['customer_addresses_id']);
                            if(res == null){
                              ErrorDialogAlert(context,
                                  appLocalizations.translate("respError"));
                            }
                            else{
                              if(res.errors == null && res.status ==200){
                                SuccDialogAlertStaySameLayout(context, res.message);
                                Timer(Duration(seconds: 3), ()=>
                                    Navigator.pushReplacement(context,
                                        MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              MyAddresses(Lang,UserToken,
                                                  AppCurrency,design_Control),)
                                    )
                                );
                              }
                              else{
                                String error="";
                                for(int i=0;i<res.errors.length;i++){
                                  error += res.errors[i] + "\n";
                                }
                                ErrorDialogAlert(context,error);
                              }
                            }

                          }
                          else{
                            ErrorDialogAlert(context,
                                appLocalizations.translate("AddAllData"));
                          }
                        },
                        child: Text(appLocalizations.translate("save"),
                            style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color: SystemControls.TextHeaderColor,
                              height: 1.2,
                            )
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.all(10)),
                  ],
                ),
              )
            ],
          ),*/
        ],
      ),
    );
  }
}