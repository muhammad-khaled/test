import 'package:flutter/material.dart';
import '../Controls.dart';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'dart:async';

import '../locale/app_localization.dart';
import 'package:easy_localization/easy_localization.dart';
import '../widgets/homeItemsList.dart';
import '../dataControl/favsModule.dart';
import '../dataControl/dataModule.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';

import '../globals.dart' as globals;

//import 'package:cached_network_image/cached_network_image.dart';


class favsLay extends StatefulWidget{

  String Lang;
  String Token;
  List<dynamic> _CartMeals = <dynamic>[];
  String AppCurrency;
  int design_Control;
  bool hasRegistration;
  favsLay(this._CartMeals, this.Lang, this.Token, this.AppCurrency,
      this.design_Control,this.hasRegistration);

  _favsLay createState() =>
      _favsLay(this._CartMeals, this.Lang, this.Token, this.AppCurrency,
          this.design_Control,this.hasRegistration);
}

class _favsLay extends State<favsLay>{
  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);

  }
  @override
  void dispose() {
    super.dispose();
    _connectivitySubscription.cancel();
  }

  Future<ResponseApi> resp;
  String Lang;
  String Token;
  List<dynamic> _CartMeals = <dynamic>[];
  String AppCurrency;
  int design_Control;
  bool hasRegistration;
  _favsLay(this._CartMeals, this.Lang, this.Token, this.AppCurrency,
      this.design_Control,this.hasRegistration){
    resp = getFavsList(Lang,Token);
  }

  String CheckText(String get){
    if(get != null)
      return get;
    else
      return " ";
  }

  FindMealAtCart(int mealId){
    for(int i=0; i<_CartMeals.length; i++){
      if(_CartMeals[i]['meals_id'] == mealId && _CartMeals[i]['type'] == "offer"){
        return i;
      }
    }
    return -1;
  }

  onAddCartSubmit(List<dynamic> _cart){
    setState(() {
      _CartMeals = _cart;
    });
  }
  onFavSubmit(List<dynamic> _mealsg){
    setState(() {
      _meals = _mealsg;
      print("Add fav");
    });
  }
  ScrollController _scrollControllerItem;
  List<dynamic> _meals = <dynamic>[];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("favorite".tr().toString()),
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
        actions: <Widget>[
          SystemControls.designControl==1 ?
              AppBarHomeIConButton(context,Lang,"")
              :Container(),
        ],
      ),
      body:Stack(
        children: <Widget>[
          LayoutBG(design_Control),
          FutureBuilder<ResponseApi>(
            future: resp,
            builder: (context, snap){
              if(snap.hasData){
                if(snap.data.status==200) {
                  _meals = snap.data.data['favs'];
                  if(_meals.length<=0){
                    return Container(
                      child: Center(
                        child: Text('emptyListFav'.tr().toString()),
                      ),
                    );
                  }
                  return Stack(
                    children: <Widget>[
                      Container(
                          child: HomeItemsList(_meals, _CartMeals, Lang,
                              Token, AppCurrency, design_Control,hasRegistration,
                              onAddCartSubmit,onFavSubmit)
                      ),
                    ],
                  );
                }
                else if(snap.data.status==401){
                  SystemControls().LogoutSetUserData(context, Lang);
                  Timer(Duration(seconds: 1), ()=>
                      ErrorDialogAlertBackHome(context, snap.data.message)
                  );
                  return Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(snap.data.message,
                          style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font2,
                            height: 1.2,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  );
                }
                return Container();
              }
              else if(snap.hasError){
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("respError".tr().toString(),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font2,
                          height: 1.2,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 150,
                        decoration: BoxDecoration(
                          color: globals.accountColorsData['MainColor']!=null
                              ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                          borderRadius: BorderRadius.all(Radius.circular(
                              SystemControls.RadiusCircValue
                          )),
                        ),
                        child: FlatButton(
                          onPressed: () {
                            print("Reload");
                            setState(() {
                              resp = getFavsList(Lang,Token);
                            });
                          },
                          child: Center(
                            child: Text("reload".tr().toString(),
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.bold,
                                  fontSize: SystemControls.font2,
                                )
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              }
              return Center(
                child: SystemControls().circularProgress(),
              );
            },
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: ConnectionStatusBar(),
          ),
        ],
      ),
    );
  }

  //////////////
  ConnectionStatusBar(){
    final Color color = Colors.redAccent;
    final double width = double.maxFinite;
    final double height = 30;

    if(_connectionStatus == 'connected'){
      return Container();
    }
    else{
      return Container(
        child: SafeArea(
          bottom: false,
          child: Container(
            color: color,
            width: width,
            height: height,
            child: Container(
              margin: EdgeInsets.only(right: 5,left: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      InkWell(
                        child: Text(
                          'Try Again..',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'en',
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font4,
                          ),
                        ),
                        onTap: (){
                          setState(() async{
                            initConnectivity();
                          });
                        },
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Please check your internet connection',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'en',
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font4,
                        ),
                      ),
                    ],
                  ),

                ],
              ),
            ),
          ),
        ),
      );
    }
  }
  ///Internet Check area
  String _connectionStatus = 'connected';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }
  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = "connected";
              resp = getFavsList(Lang,Token);
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = "failed";
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState((){
          _connectionStatus = "connected";
          resp = getFavsList(Lang,Token);
        });
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "failed");
        break;
      default:
        setState(() => _connectionStatus = 'failed');
        break;
    }
  }
}