import '../../provider/HomeProvider/HomeProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geocoder/geocoder.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';

import 'package:provider/provider.dart';
import '../globals.dart';
import '../provider/productsInCartProvider.dart';

import '../dataControl/dataModule.dart';
import '../Controls.dart';

import '../locale/app_localization.dart';
//imppp4
import '../dataControl/cartModule.dart';
import 'package:easy_localization/easy_localization.dart';
import '../screens/web_view_page.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';
import '../widgets/UI_Widgets_InputField.dart';

import '../globals.dart' as globals;

import 'package:image_picker/image_picker.dart';
import 'package:flutter/foundation.dart';
class CartCheckOutLayout extends StatefulWidget{

  List<dynamic> _mealsCart = <dynamic>[];
  String Lang;
  String AppCurrency;
  String UserToken;
  int design_Control;
  LatLng _center;
  bool newAddressflag;
  String selectedAddressFull;
  String itemsNote;
  int selectedAreaId;
  CartCheckOutLayout(this.Lang,this.UserToken,this.AppCurrency,this._mealsCart,
      this.design_Control,this._center,this.newAddressflag,
      this.selectedAddressFull,this.itemsNote,this.selectedAreaId);

  _CartCheckOutLayout createState() =>
      _CartCheckOutLayout(this.Lang, this.UserToken,this.AppCurrency,
          this._mealsCart,this.design_Control,this._center,
          this.newAddressflag,this.selectedAddressFull,this.itemsNote);
}

class _CartCheckOutLayout extends State<CartCheckOutLayout>{
  final Future<CartList> localCart = read_from_file();
  List<dynamic> _mealsCart = <dynamic>[];
  Future<ResponseApi> respAdds;
  List<dynamic> _addreses = <dynamic>[];
  String Lang;
  String AppCurrency;
  String UserToken;
  int design_Control;
  bool newAddressflag;
  String selectedAddressFull;
  String itemsNote;


  @override
  void initState() {
    super.initState();

    CreateItemsList();
  }

  _CartCheckOutLayout(this.Lang, this.UserToken,this.AppCurrency,
      this._mealsCart,this.design_Control,this._center,
      this.newAddressflag,this.selectedAddressFull,this.itemsNote){
    if(_center.latitude==0||_center.longitude==0){
      String gps = globals.accounts_country_lat_long;
      var arr = gps.split(',');
      print("accounts_country_lat_long\n\n\n"+arr[0]+"\n\n\n"+arr[1]);
      //camZomeValu = 7;
      _center = LatLng(double.parse(arr[0]), double.parse(arr[1]));
    }
    for(int i=0;i<SystemControls.addresPartsFormat(Lang).length;i++){
      TextEditingController addressTextField1 = TextEditingController();
      addressList.add(addressTextField1);
    }
    if(false) {
      _getSelectedAddressText(_center.latitude, _center.longitude);
    }
    else{
      var addressArr = selectedAddressFull.split(',');
      for(int i=0; i<addressArr.length;i++){
        if(i<SystemControls.addresPartsFormat(Lang).length){
          addressList[i].text = addressArr[i];
        }
        else{
          break;
        }
      }
    }
  }

  int total_items = 0;
  double total_price = 0;

  List <dynamic> items = <dynamic>[];
  //var Listjson;

  var Listjson0, Listjson1;
  List <dynamic> items0 = <dynamic>[], items1 = <dynamic>[];
  bool hasProduct = false;
  String image;
  String image2;
  bool show = true;
  CreateItemsList(){
    print("paymentMethods.length");
    print(paymentMethods.length);
    print(globals.paymentMethods);
    print("paymentMethods.length");
    image =Provider.of<CartStateProvider>(context,listen: false).myCartImgName == null ? "":Provider.of<CartStateProvider>(context,listen: false).myCartImgName;
    image2 =Provider.of<CartStateProvider>(context,listen: false).cartImgName== null ? "":Provider.of<CartStateProvider>(context,listen: false).cartImgName;

    print(Provider.of<CartStateProvider>(context,listen: false).cartImgName);
    _mealsCart.forEach((element) {
      print('element:$element');
      if(element["meals_img"]!= null){
        show = false;
      }
      if(element["type"]!="offer"){

      }
    });
    for(int i=0; i<_mealsCart.length; i++){
      Map<String, dynamic> item = {
        "id": _mealsCart[i]['meals_id'],
        "type": _mealsCart[i]['type'],
        "count": _mealsCart[i]['count'],
        "choice": _mealsCart[i]['choice'],
      };
      if(_mealsCart[i]['follow_delivery_time']=="0"&& _mealsCart[i]['type'] != "offer"){
        print('not follow follow');
        items0.add(item);
      }
      else{
        items1.add(item);
      }
      if(hasProduct == false){
        items1.forEach((element) {
          if(element["type"] != "offer"){
            hasProduct =true;
          }
        });
      }
      //Calcu total price and count items
      //total_items = total_items + _mealsCart[i]['count'];
      total_price = total_price +
          (double.parse(_mealsCart[i]['meals_price']) * double.parse("${_mealsCart[i]['count']}"));
    }

    if(hasProduct == false){
      print(hasProduct);
      print(hasProduct);
      items1.forEach((element) {
        Map<String, dynamic> item = {
          "id": element['id'],
          "type": element['type'],
          "count": element['count'],
          "choice": element['choice'],
        };

        items0.add(item);
      });
      items1.clear();
    }



    Listjson0 = json.encode(items0);
    print("Listjson0 ->"+Listjson0);
    Listjson1 = json.encode(items1);
    print("Listjson1 ->"+Listjson1);
  }
  List<TextEditingController> addressList = <TextEditingController>[];

  TextEditingController notesTextField = TextEditingController();
  final FocusNode _notesFocus = FocusNode();

  LatLng _center;

  //ConvertLocation cordenate toAddress text
  String GetInputFieldValue(Address address, String key){
    if(key == "addressLine"){
      return address.addressLine;
    }
    else if(key == "adminArea"){
      return address.adminArea;
    }
    else if(key == "countryCode"){
      return address.countryCode;
    }
    else if(key == "countryName"){
      return address.countryName;
    }
    else if(key == "featureName"){
      return address.featureName;
    }
    else if(key == "locality"){
      return address.locality;
    }
    else if(key == "postalCode"){
      return address.postalCode;
    }
    else if(key == "subAdminArea"){
      return address.subAdminArea;
    }
    else if(key == "subLocality"){
      return address.subLocality;
    }
    else if(key == "subThoroughfare"){
      return address.subThoroughfare;
    }
    else if(key == "thoroughfare"){
      return address.thoroughfare;
    }
    else{
      return "";
    }

  }
  _getSelectedAddressText(var lat,var long) async{
    final coordinates = new Coordinates(lat, long);
    Geocoder.local.findAddressesFromCoordinates(coordinates).then((var address) {
      setState(() {
        print("Get address from locer");
        for(int i=0;i<SystemControls.addresPartsFormat(Lang).length;i++){
          addressList[i].text = GetInputFieldValue(address.first,SystemControls.addresGeoCodeKey()[i]);
        }
        print("\n\n\n\n done");
      });

    }).catchError((e) {
      print(e);
    });
  }

  int _isRadioSelected = 0;
  void _handleRadioValueChange(int value) {
    setState(() {
      _isRadioSelected = value;
    });
  }
  File imagePicked = File("");

  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    CartStateProvider cartState = Provider.of<CartStateProvider>(context,listen: false);
    PickedFile _imageFile = cartState.imageFile;


    print('cart imageeee');
    // print(cartState.imageFile.path);
    print('cart imageeee');
    if(_imageFile != null){
      imagePicked = File(_imageFile.path);
    }
    print('cart imageeee');
    print(imagePicked);
    print('cart imageeee');
    return Scaffold(
      appBar: AppBar(
        title: Text("cart".tr().toString()),
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          LayoutBG(design_Control),
          Align(
            alignment: Alignment.center,
            child: Container(
              //width: mediaQuery.size.shortestSide,
              margin: EdgeInsets.only(top: 10,left: 10,right: 10,bottom: 0),
              child: ListView(
                //mainAxisAlignment: MainAxisAlignment.start,
                //crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  ListView.builder(
                      shrinkWrap : true,
                      physics: ScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      itemCount: (SystemControls.addresPartsFormat(Lang).length~/2)+1,
                      itemBuilder: (context, ListIndex){
                        int index = ListIndex*2;
                        if(SystemControls.addresPartsFormat(Lang).length > 3) {
                          return Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.only(top: 10),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(10)),
                                  ),
                                  child: TextFormField(
                                    controller: addressList[index],
                                    decoration: InputFieldDecoration(
                                        SystemControls.addresPartsFormat(
                                            Lang)[index], Lang),
                                    keyboardType: TextInputType.text,
                                    style: new TextStyle(
                                      fontFamily: Lang,
                                    ),
                                  ),
                                ),
                                flex: 1,
                              ),
                              Container(width: 5,),
                              Expanded(
                                flex: 1,
                                child: (index + 1) < SystemControls
                                    .addresPartsFormat(Lang)
                                    .length ? (
                                    Container(
                                      margin: EdgeInsets.only(top: 10),
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10)),
                                      ),
                                      child: TextFormField(
                                        controller: addressList[index + 1],
                                        decoration: InputFieldDecoration(
                                            SystemControls.addresPartsFormat(
                                                Lang)[index + 1], Lang),
                                        keyboardType: TextInputType.text,
                                        style: new TextStyle(
                                          fontFamily: Lang,
                                        ),
                                      ),
                                    )
                                ) : (Container()),
                              )
                            ],
                          );
                        }
                        else{
                          return Column(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(10)),
                                ),
                                child: TextFormField(
                                  enabled: false,
                                  controller: addressList[index],
                                  decoration: InputFieldDecoration(
                                      SystemControls.addresPartsFormat(
                                          Lang)[index], Lang),
                                  keyboardType: TextInputType.text,
                                  style: new TextStyle(
                                    fontFamily: Lang,
                                  ),
                                ),
                              ),
                              Container(padding: EdgeInsets.all(5),),
                              (index + 1) < SystemControls
                                  .addresPartsFormat(Lang)
                                  .length ? (
                                  Container(
                                    margin: EdgeInsets.only(top: 10),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10)),
                                    ),
                                    child: TextFormField(
                                      controller: addressList[index + 1],
                                      decoration: InputFieldDecoration(
                                          SystemControls.addresPartsFormat(
                                              Lang)[index + 1], Lang),
                                      keyboardType: TextInputType.text,
                                      style: new TextStyle(
                                        fontFamily: Lang,
                                      ),
                                    ),
                                  )
                              ) : (Container()),
                            ],
                          );
                        }
                        return Container(
                          margin: EdgeInsets.only(top: 10),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            controller: addressList[index],
                            decoration: InputFieldDecoration(SystemControls.addresPartsFormat(Lang)[index], Lang),
                            keyboardType: TextInputType.text,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                          ),
                        );
                      }
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: TextFormField(
                      controller: notesTextField,
                      focusNode: _notesFocus,
                      //decoration: InputFieldDecoration(appLocalizations.translate("orderAddressNotes"), Lang),
                      decoration: InputDecoration(
                        labelText: "orderAddressNotes".tr().toString(),
                        labelStyle: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font3,
                            color: globals.accountColorsData['TextHeaderColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                        ),
                        fillColor: Colors.white,
                        contentPadding: EdgeInsets.only(right: 10,left: 10,top: 16,bottom: 0),
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                        enabledBorder: const OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.black, width: 0.0),
                        ),
                        alignLabelWithHint: true,
                      ),
                      keyboardType: TextInputType.multiline,
                      maxLines: 3,
                      onFieldSubmitted: (_){
                        _notesFocus.unfocus();
                      },
                      style: new TextStyle(
                        fontFamily: Lang,
                      ),

                    ),
                  ),//ِAddress note

                  globals.paymentMethods.length>=1?   Padding(
                    padding: EdgeInsets.all(10),
                    child: Text("paymentWay".tr().toString(),
                      style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.bold,
                          fontSize: SystemControls.font2,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                      ),
                    ),
                  ):Container(),
/// see after modification //
                  globals.paymentMethods != null?(

                      ((items0.length>0 && items1.length >0 ) || _imageFile != null ||  image2.isNotEmpty || image.isNotEmpty)? ListView.builder(
                          shrinkWrap : true,
                          physics: ScrollPhysics(),
                          scrollDirection: Axis.vertical,
                          itemCount: 1,
                          itemBuilder: (context, index){
                            print('----****----');
                                print(_imageFile);
                            print(image);
                            print(image2);
                            print('----****----');
                                return Row(
                              children: <Widget>[
                                new Radio(
                                  value: index,
                                  activeColor: globals.accountColorsData['MainColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                                  groupValue: _isRadioSelected,
                                  onChanged: _handleRadioValueChange,
                                ),
                                new Text(globals.paymentMethods[index]['payments_name'].toString(),
                                  style: new TextStyle(fontSize: 16.0),
                                ),
                              ],
                            );
                          }
                      ) :  ListView.builder(
                          shrinkWrap : true,
                          physics: ScrollPhysics(),
                          scrollDirection: Axis.vertical,
                          itemCount: globals.paymentMethods.length,
                          itemBuilder: (context, index){
                            print('----****----');
                            print(_imageFile);
                            print(image);
                            print(image2);
                            print('----****----');
                            return Row(
                              children: <Widget>[
                                new Radio(
                                  value: index,
                                  activeColor: globals.accountColorsData['MainColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                                  groupValue: _isRadioSelected,
                                  onChanged: _handleRadioValueChange,
                                ),
                                new Text(globals.paymentMethods[index]['payments_name'].toString(),
                                  style: new TextStyle(fontSize: 16.0),
                                ),
                              ],
                            );
                          }
                      )

                  ): ListView.builder(
                      shrinkWrap : true,
                      physics: ScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      itemCount: SystemControls.paymentWayFun(Lang).length,
                      itemBuilder: (context, index){
                        return Row(
                          children: <Widget>[
                            new Radio(
                              value: index,
                              activeColor: globals.accountColorsData['MainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                              groupValue: _isRadioSelected,
                              onChanged: _handleRadioValueChange,
                            ),
                            new Text(SystemControls.paymentWayFun(Lang)[index].toString(),
                              style: new TextStyle(fontSize: 16.0),
                            ),
                          ],
                        );
                      }
                  ),


                  Container(
                    height: 1,
                    color: globals.accountColorsData['TextdetailsColor']!=null
                        ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                  ),
                  Padding(padding: EdgeInsets.all(6)),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Text("TotalPrice".tr().toString(),
                            style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData['TextHeaderColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                              height: 1.2,
                            )
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child:  Consumer<CartStateProvider>(builder: (context,cart,_){
                          return  Text(
                              ((cart.couponType !="" ? cart.couponType=="value"?(total_price - double.parse(cart.couponValue)): (total_price-((total_price * double.parse(cart.couponValue))/100)) :total_price)<=0?0:cart.couponType !="" ? cart.couponType=="value"?(total_price - double.parse(cart.couponValue))+globals.deliveryCost: (total_price-((total_price * double.parse(cart.couponValue))/100))+globals.deliveryCost :total_price+globals.deliveryCost).toStringAsFixed(globals.numberDecimalDigits) +
                                  " " + AppCurrency,
                              style: TextStyle(
                                fontFamily: Lang,
                                fontWeight: FontWeight.normal,
                                fontSize: SystemControls
                                    .font3,
                                color: globals.accountColorsData['TextHeaderColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                height: 1.2,
                              )
                          );
                        }),
                      )
                    ],
                  ),
                  Padding(padding: EdgeInsets.all(6)),
                  Container(
                    width: mediaQuery.size.width,
                    height: 50,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        color: globals.accountColorsData['MainColor']!=null
                            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz
                    ),
                    child: FlatButton(
                      onPressed: ()async{
                        _ConfirmDialogAlert(Lang,context,_imageFile);
                        /*
                          String gps = _center.latitude.toString()
                              + "," + _center.longitude.toString();
                          String fulladdress = addressTextField5.text +" "
                              + addressTextField4.text +" "
                              + addressTextField3.text +" "
                              + addressTextField2.text +" "
                              + addressTextField1.text;
                          print(gps);
                          if(fulladdress==null || fulladdress ==""){
                            ErrorDialogAlert(context,
                                appLocalizations.translate("mustaddAlladdress"));
                          }
                          else if(UserToken == ""){
                            ErrorDialogAlert(context,
                                appLocalizations.translate("mustLogin"));
                          }
                          else{
                            var res0, res1;
                            if(items0.length>=1) {
                              res0 = await CreateOrder(Lang, UserToken,
                                  fulladdress, note,
                                  items0, gps,"0");
                            }
                            if(items1.length>=1) {
                              res1 = await CreateOrder(Lang, UserToken,
                                  fulladdress, note,
                                  items1, gps,"1");
                            }

                            if(res0 == null){
                              ErrorDialogAlert(context,
                                  appLocalizations.translate("respError"));
                            }
                            else if(res0.status == 401){
                              SystemControls().LogoutSetUserData(context, Lang);
                              ErrorDialogAlertBackHome(context, res0.message);
                            }
                            else{
                              print(res0.message);
                              if(res0.errors == null && res0.status == 200){
                                await clear_cart_file();
                                CartState cartState = Provider.of<CartState>(context);
                                cartState.setCurrentProductsCount(0.toString());

                                SuccDialogAlertBackHome(context, Lang,res0.message);
                              }
                              else{
                                ErrorDialogAlert(context,res0.message);
                              }
                            }

                            if(res1 == null){
                              ErrorDialogAlert(context,
                                  appLocalizations.translate("respError"));
                            }
                            else if(res1.status == 401){
                              SystemControls().LogoutSetUserData(context, Lang);
                              ErrorDialogAlertBackHome(context, res1.message);
                            }
                            else{
                              print(res1.message);
                              if(res1.errors == null && res1.status == 200){
                                await clear_cart_file();
                                CartState cartState = Provider.of<CartState>(context);
                                cartState.setCurrentProductsCount(0.toString());

                                SuccDialogAlertBackHome(context, Lang,res1.message);
                              }
                              else{
                                ErrorDialogAlert(context,res1.message);
                              }
                            }
                          }
                           */
                      },
                      child: Text("confirmOrder".tr().toString(),
                          style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font3,
                            color: globals.accountColorsData['TextOnMainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                            height: 1.2,
                          )
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.all(6)),
                ],
              ),
            ),
          ),
/*
          Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              Container(
                color: globals.accountColorsData['BGColor']!=null
                    ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
                alignment: Alignment.bottomCenter,
                height: 120,
                width: mediaQuery.size.width,
              ),
              Container(
                width: mediaQuery.size.shortestSide,
                margin: EdgeInsets.only(right: 10,left: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      height: 1,
                      color: globals.accountColorsData['TextdetailsColor']!=null
                          ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                    ),
                    Padding(padding: EdgeInsets.all(6)),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Text(appLocalizations.translate("TotalPrice"),
                              style: TextStyle(
                                fontFamily: Lang,
                                fontWeight: FontWeight.normal,
                                fontSize: SystemControls.font3,
                                color: globals.accountColorsData['TextHeaderColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                height: 1.2,
                              )
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Text((total_price+globals.deliveryCost).toStringAsFixed(globals.numberDecimalDigits)
                              +" "+AppCurrency,
                            style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData['TextHeaderColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                              height: 1.2,
                            ),
                          ),
                        )
                      ],
                    ),
                    Padding(padding: EdgeInsets.all(6)),
                    Container(
                      width: mediaQuery.size.width,
                      height: 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: globals.accountColorsData['MainColor']!=null
                              ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz
                      ),
                      child: FlatButton(
                        onPressed: ()async{
                            _ConfirmDialogAlert(context);
                          /*
                          String gps = _center.latitude.toString()
                              + "," + _center.longitude.toString();
                          String fulladdress = addressTextField5.text +" "
                              + addressTextField4.text +" "
                              + addressTextField3.text +" "
                              + addressTextField2.text +" "
                              + addressTextField1.text;
                          print(gps);
                          if(fulladdress==null || fulladdress ==""){
                            ErrorDialogAlert(context,
                                appLocalizations.translate("mustaddAlladdress"));
                          }
                          else if(UserToken == ""){
                            ErrorDialogAlert(context,
                                appLocalizations.translate("mustLogin"));
                          }
                          else{
                            var res0, res1;
                            if(items0.length>=1) {
                              res0 = await CreateOrder(Lang, UserToken,
                                  fulladdress, note,
                                  items0, gps,"0");
                            }
                            if(items1.length>=1) {
                              res1 = await CreateOrder(Lang, UserToken,
                                  fulladdress, note,
                                  items1, gps,"1");
                            }

                            if(res0 == null){
                              ErrorDialogAlert(context,
                                  appLocalizations.translate("respError"));
                            }
                            else if(res0.status == 401){
                              SystemControls().LogoutSetUserData(context, Lang);
                              ErrorDialogAlertBackHome(context, res0.message);
                            }
                            else{
                              print(res0.message);
                              if(res0.errors == null && res0.status == 200){
                                await clear_cart_file();
                                CartState cartState = Provider.of<CartState>(context);
                                cartState.setCurrentProductsCount(0.toString());

                                SuccDialogAlertBackHome(context, Lang,res0.message);
                              }
                              else{
                                ErrorDialogAlert(context,res0.message);
                              }
                            }

                            if(res1 == null){
                              ErrorDialogAlert(context,
                                  appLocalizations.translate("respError"));
                            }
                            else if(res1.status == 401){
                              SystemControls().LogoutSetUserData(context, Lang);
                              ErrorDialogAlertBackHome(context, res1.message);
                            }
                            else{
                              print(res1.message);
                              if(res1.errors == null && res1.status == 200){
                                await clear_cart_file();
                                CartState cartState = Provider.of<CartState>(context);
                                cartState.setCurrentProductsCount(0.toString());

                                SuccDialogAlertBackHome(context, Lang,res1.message);
                              }
                              else{
                                ErrorDialogAlert(context,res1.message);
                              }
                            }
                          }
                           */
                        },
                        child: Text(appLocalizations.translate("confirmOrder"),
                            style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData['TextHeaderColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                              height: 1.2,
                            )
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.all(10)),
                  ],
                ),
              )
            ],
          ),*/
        ],
      ),
    );
  }

  Future<void> _ConfirmDialogAlert(String lang,BuildContext context,PickedFile imageFile) {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
          ),
          title: Text("confirmationMsg".tr().toString(),
            style: TextStyle(
                fontFamily: Lang,
                fontWeight: FontWeight.bold,
                fontSize: SystemControls.font3,
                color: globals.accountColorsData['TextHeaderColor']!=null
                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
            ),
            textAlign: TextAlign.center,
          ),
          contentPadding: EdgeInsets.only(top: 40,bottom: 0),
          content: ClipRRect(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
              bottomRight: Radius.circular(SystemControls.RadiusCircValue),
            ),
            child: Container(
              height: 40,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Container(
                      decoration: BoxDecoration(
                        color: globals.accountColorsData['MainColor']!=null
                            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                        border: Border(
                          top: BorderSide(color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz),
                        ),
                        //borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: FlatButton(
                        child: Text("cancel".tr().toString(),
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData['TextOnMainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
                          ),
                        ),
                        onPressed: () {

                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ),
                  Container(
                    height: 40,
                    width: 1,
                    color: Colors.black,
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      decoration: BoxDecoration(
                        color: globals.accountColorsData['MainColor']!=null
                            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                        border: Border(
                          top: BorderSide(color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz),
                        ),
                      ),
                      child: FlatButton(
                        child: Text("send".tr().toString(),
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData['TextOnMainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
                          ),
                        ),
                        onPressed: () async{
                          StartLoad(context,imageFile);
                        },
                      ),
                    ),
                  ),


                ],
              ),
            ),
          ),
        );
      },
    );
  }

  StartLoad(BuildContext context,  PickedFile imageFile)async{

    String gps = _center.latitude.toString()
        + "," + _center.longitude.toString();
    String fulladdress = "";
    bool missFiled=false;
    String missedField="";
    for(int i=0;i<SystemControls.addresPartsFormat(Lang).length;i++){
      if(SystemControls.addresPartsFormat(Lang)[i].contains('*')
          &&addressList[i].text.isEmpty){
        missFiled = true;
        missedField = "("+SystemControls.addresPartsFormat(Lang)[i]+")";
        break;
      }
      fulladdress += addressList[i].text +",";
    }
    print(fulladdress);
    if(fulladdress==null || fulladdress =="" || missFiled == true){
      if(missFiled){
        Navigator.of(context).pop();
        ErrorDialogAlert(context,
           "mustaddAlladdress2".tr().toString()+missedField);
      }
      else {
        Navigator.of(context).pop();
        ErrorDialogAlert(context,
           "mustaddAlladdress".tr().toString());
      }
    }
    else if(UserToken == ""){
      Navigator.of(context).pop();
      ErrorDialogAlert(context,
          "mustLogin".tr().toString());
      Navigator.of(context).pop();
    }
    else{
      _StartLoadDialogAlert(context);
      if(newAddressflag == true) {
        print("Add new address ####");
        var res = await AddNewAddressApi(Lang, UserToken,
            fulladdress, fulladdress.substring(0, 5),
            gps, "0");
      }
      //SetPaymet way
      int paymentMethod = null;
      if(globals.paymentMethods.length >0
          && globals.paymentMethods[_isRadioSelected]['payments_id'] != null){
        paymentMethod = globals.paymentMethods[_isRadioSelected]['payments_id'];
      }
      //Set file function
      CartStateProvider cartState = Provider.of<CartStateProvider>(context,listen: false);
      print('Api image');
      print(cartState.imageFile);
      print('Api image');
      String orderImgName = cartState.myCartImgName;
      File imagePicked = File("");
      if(imageFile != null){
        imagePicked = File(imageFile.path);

      }
      print('Api image');
      print(imagePicked);
      print('Api image');
      var res0, res1;
      if(items1.length>=1) {
        print('first');

        res1 = await CreateOrder(Lang, UserToken,
            fulladdress, notesTextField.text,
            items1, gps,"1",paymentMethod,imagePicked,
            orderImgName,itemsNote,widget.selectedAreaId,context).whenComplete(()
        {imagePicked = null;
        Provider.of<CartStateProvider>(context,listen: false).cartImgName = null;
        cartState.fetchCardNum();
        cartState.cartList.clear();
        Provider.of<HomeProvider>(context,listen: false).changeCurrentHomeWidgetIndex(0, context);

        });
      }
      if(items0.length>=1) {

        print('second');
        res0 = await CreateOrder(Lang, UserToken,
            fulladdress, notesTextField.text,
            items0, gps,"0",paymentMethod,imagePicked,
            orderImgName,itemsNote,widget.selectedAreaId,context).whenComplete(()  {imagePicked = null;

        Provider.of<CartStateProvider>(context,listen: false).cartImgName = null;
        cartState.fetchCardNum();
        Provider.of<HomeProvider>(context,listen: false).changeCurrentHomeWidgetIndex(0, context);

        cartState.cartList.clear();
        });
      }
      if(items0.length ==0&& items1.length ==0 && imagePicked !=null) {
        print('second');
        print(imagePicked);
        print('second');
        res0 = await CreateOrder(Lang, UserToken,
            fulladdress, notesTextField.text,
            items0, gps,"0",paymentMethod,imagePicked,
            orderImgName,itemsNote,widget.selectedAreaId,context).whenComplete(() {imagePicked = null;
        Provider.of<CartStateProvider>(context,listen: false).cartImgName = null;
        cartState.fetchCardNum();
        cartState.cartList.clear();
        Provider.of<HomeProvider>(context,listen: false).changeCurrentHomeWidgetIndex(0, context);

        });
      }
      //  if(items1.length>=1){
      //    print('final');
      //
      //    res0 = await CreateOrder(Lang, UserToken,
      //       fulladdress, notesTextField.text,
      //       items0, gps,"1",paymentMethod,imagePicked,
      //       orderImgName,itemsNote,widget.selectedAreaId).whenComplete(() => imagePicked = null);
      // }


      if(res0 == null){
        // ErrorDialogAlert(context,
        //     appLocalizations.translate("respError"));
      }
      else if(res0.status == 401){
        SystemControls().LogoutSetUserData(context, Lang);
        ErrorDialogAlertBackHome(context, res0.message);
      }
      else{
        print(res0.message);
        if(res0.errors == null && res0.status == 200){
          await clear_cart_file();
          CartStateProvider cartState = Provider.of<CartStateProvider>(context,listen: false);
          cartState.setCurrentProductsCount(0.toString());
          cartState.setImgFilePicker(null);
          cartState.setCartImgName("");

          print(res0.data['url']);
          if(res0.data['url'] != null){
            SuccDialogAlertStaySameLayout(context, res0.message,ValueKey("checkOut1"));
            String url = res0.data['url'];
            if(!url.contains('https')){
              print("res0 url * "+url);
              url = "https"+url.substring(4);
            }
            print("res0 url ** "+url);
            Navigator.pop(context);
            Navigator.push(context,
                MaterialPageRoute(builder: (BuildContext context) =>
                    WebViewPage(
                      //url: 'http://www.nmait.net/polaris/ar/api/paymentOrder?accountId=4&orderId=402&payments_id=3',
                      url: url,
                      token: UserToken,
                      lang: Lang,
                      //onPageLoadingFinishCallback: null,
                    )
                ));

          }
          else {
            SuccDialogAlertBackHome(
                context, Lang, res0.message);
          }
        }
        else{
          print("res0.errors.toString()");
          print(res0.errors.toString());
          ErrorDialogAlertBackCart(context,res0);
        }
      }
      print("res1");
      print(res1);
      print("res1");

      if(res1 == null && items1.length>=1){
        // ErrorDialogAlert(context,
        //     appLocalizations.translate("respError"));
      }
      else if(res1.status == 401){
        SystemControls().LogoutSetUserData(context, Lang);
        ErrorDialogAlertBackHome(context, res1.message);
        Navigator.pop(context);
      }
      else{
        print(res1.message);
        if(res1.errors == null && res1.status == 200){
          await clear_cart_file();
          CartStateProvider cartState = Provider.of<CartStateProvider>(context,listen: false);
          cartState.setCurrentProductsCount(0.toString());
          cartState.setImgFilePicker(null);

          if(res1.data['url'] != null){
            SuccDialogAlertStaySameLayout(context, res1.message,ValueKey("checkOut2"));
            String url = res1.data['url'];
            if(!url.contains('https')){
              print("res1 url * "+url);
              url = "https"+url.substring(4);
            }
            print("res1 url ** "+url);

            Navigator.pop(context);
            Navigator.push(context,
                MaterialPageRoute(builder: (BuildContext context) =>
                    WebViewPage(
                      url: url,
                      token: UserToken,
                      lang: Lang,
                      //onPageLoadingFinishCallback: null,
                    )
                ));

          }
          else {
            SuccDialogAlertBackHome(
                context, Lang, res1.message);
          }
        }
        else if(res1.errors != null){
          ErrorDialogAlertBackCart(context,res1.message);
        }
      }
    }
  }
  Future<void> _StartLoadDialogAlert(BuildContext context) {
    //final appLocalizations = AppLocalizations.of(context);
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return Center(
          child: SystemControls().circularProgress(),
        );
      },
    );
  }
}
