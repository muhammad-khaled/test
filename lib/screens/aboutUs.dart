import 'package:flutter/material.dart';
import '../Controls.dart';
import '../widgets/UI_Wigdets.dart';
import '../dataControl/dataModule.dart';
import '../screens/home.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:easy_localization/easy_localization.dart';
import '../globals.dart' as globals;

class AboutUs extends StatelessWidget{
  dynamic _info;
  String lang;
  int designControl;
  AboutUs(this._info,this.lang,this.designControl);

  launchWithURL(String url) async {
    //const url = 'https://flutter.dev';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }


  launchWithURLWhats(String url) async {
    //const url = 'https://flutter.dev';
    if (await canLaunch("whatsapp://send?$url")) {

      await canLaunch("whatsapp://send?phone=$url") ? launch("whatsapp://send?phone=$url"): print('');

    } else {
      throw 'Could not launch $url';
    }
  }
  launchURL() async {
    const url = 'tel:+1 555 010 999';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  GetSocialMedialIcons(String url, String imagePath){
    if(url != null){
      return Container(
          width: 40,
          height: 40,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(25)),
          ),
          margin: EdgeInsetsDirectional.fromSTEB(0, 0, 3,0),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(25)),
            child: InkWell(
              child: Image.asset(imagePath,fit: BoxFit.cover,),
              onTap: (){
                //TODO Add open link or call
                launchWithURL(url);
                //launchURL()
              },
            ),
          )
      );
    }
    else{
      return Container();
    }
  }
  GetSocialMedialIconsSVG(String url, String imagePath){
    if(url != null){
      return Container(
          width: 40,
          height: 40,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(25)),
          ),
          margin: EdgeInsetsDirectional.fromSTEB(0, 0, 3,0),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(25)),
            child: InkWell(
              child: SystemControls().GetSVGImagesAsset(
                  imagePath, 20,null),
              onTap: (){
                //TODO Add open link or call
                launchWithURL(url);
                //launchURL()
              },
            ),
          )
      );
    }
    else{
      return Container();
    }
  }
  GetSocialMedialIconsSVGWhats(String url, String imagePath){
    if(url != null){
      return Container(
          width: 40,
          height: 40,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(25)),
          ),
          margin: EdgeInsetsDirectional.fromSTEB(0, 0, 3,0),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(25)),
            child: InkWell(
              child: SystemControls().GetSVGImagesAsset(
                  imagePath, 20,null),
              onTap: (){
                print(url);
                print(url);
                //TODO Add open link or call
                launchWithURLWhats(url);
                //launchURL()
              },
            ),
          )
      );
    }
    else{
      return Container();
    }
  }

  GetPhoneNum(String phone){
    if(phone != null){
      return Container(
        margin: EdgeInsets.only(top: 5),
        child: InkWell(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[

              Text(phone,
                style: TextStyle(
                    fontFamily: lang,
                    fontWeight: FontWeight.normal,
                    fontSize: SystemControls.font3,
                    color: Colors.black
                ),
              ),
              Padding(padding: EdgeInsets.all(4)),
              Icon(Icons.phone,size: 25,),

            ],
          ),
          onTap: (){
            String URL = 'tel://'+phone;
            //TODO Add open link or call
            launchWithURL(URL);
            //launchURL();
          },
        ),
      );
    }
    else{
      return Container();
    }
  }

  GetLocationIcon(String location, String imagePath){
    if(location != null){
      String url = "https://www.google.com/maps/search/?api=1&"
          "query="+location;
      return Container(
          width: 40,
          height: 40,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(25)),
          ),
          margin: EdgeInsetsDirectional.fromSTEB(0, 0, 3,0),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(25)),
            child: InkWell(
              child: Image.asset(imagePath,fit: BoxFit.cover,),
              onTap: (){
                //TODO Add open link or call
                launchWithURL(url);
                //launchURL()
              },
            ),
          )
      );
    }
    else{
      return Container();
    }

  }
  GetLocationIconSVG(String location, String imagePath){
    if(location != null){
      String url = "https://www.google.com/maps/search/?api=1&"
          "query="+location;
      return Container(
          width: 40,
          height: 40,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(25)),
          ),
          margin: EdgeInsetsDirectional.fromSTEB(0, 0, 3,0),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(25)),
            child: InkWell(
              child: SystemControls().GetSVGImagesAsset(
                  imagePath, 20,null),
              onTap: (){
                //TODO Add open link or call
                launchWithURL(url);
                //launchURL()
              },
            ),
          )
      );
    }
    else{
      return Container();
    }

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body:
        Center(
          child: SafeArea(
            child: CustomScrollView(
              physics: BouncingScrollPhysics(),

              slivers: <Widget>[
                SliverAppBar(
                  pinned: true,
                  floating: true,
                  expandedHeight: MediaQuery.of(context).size.height * 0.3,
                  backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
                      ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
                  title: Text("aboutUs".tr().toString()),
                  flexibleSpace: Stack(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * 0.3,
                        child: AppBarElementsflexibleSpace(designControl),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Center(
                          child: Container(
                            //width: MediaQuery.of(context).size.width*0.5,
                            padding: EdgeInsets.all(20),
                            margin: EdgeInsets.only(bottom: 5,top: 50),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(0)),
                            ),
                            child: Image.asset(lang =="ar"? 'assets/logo.png':"assets/logoEn.png",fit: BoxFit.contain,),
                          ),
                        ),
                      )
                    ],
                  ),

                ),
                SliverList(
                  delegate: SliverChildListDelegate([
                    Container(
                        margin: EdgeInsets.only(top: 10),
                        child: Center(
                          //TODO get Version
                            child: Text(SystemControls.Version(lang),
                              style: TextStyle(
                                  fontFamily: lang,
                                  fontWeight: FontWeight.normal,
                                  fontSize: SystemControls.font3,
                                  color: Colors.black
                              ),
                            )
                        )
                    ),
                    Container(
                      margin: EdgeInsets.all(10),
                      child: Text(_info['infos_about']==null?"":_info['infos_about'],
                        style: TextStyle(
                            fontFamily: lang,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font3,
                            color: Colors.black
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      child:Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          GetLocationIconSVG(_info['infos_gps'],'assets/socialIcons/LocationIc.svg'),
                          GetSocialMedialIconsSVG(_info['infos_facebook'],'assets/socialIcons/facebook.svg'),
                          GetSocialMedialIconsSVG(_info['infos_twitter'],'assets/socialIcons/twitter.svg'),
                          GetSocialMedialIconsSVG(_info['infos_youtube'],'assets/socialIcons/youtube.svg'),
                          GetSocialMedialIconsSVG(_info['infos_instagram'],'assets/socialIcons/instagramicon.svg'),
                          GetSocialMedialIconsSVG(_info['infos_snapchat'],'assets/socialIcons/snapchat.svg'),
                          GetSocialMedialIconsSVGWhats(_info['infos_whatsapp'],'assets/socialIcons/whatsIcon.svg'),
                          GetSocialMedialIconsSVG(_info['infos_foursquare'],'assets/socialIcons/foursquare.svg'),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 25),
                      child: Column(
                        children: <Widget>[
                          GetPhoneNum(_info['infos_phone1']),
                          GetPhoneNum(_info['infos_phone2']),
                          GetPhoneNum(_info['infos_phone3']),
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 15,bottom: 10),
                        child: InkWell(
                          onTap: (){
                            String url = "https://www.namaait.com/"+lang;
                            launchWithURL(url);
                          },
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text('developedBy'.tr().toString(),
                                style: TextStyle(
                                    fontFamily: lang,
                                    fontWeight: FontWeight.bold,
                                    fontSize: SystemControls.font4,
                                    color: Colors.black
                                ),
                              ),
                              Padding(padding: EdgeInsets.all(5)),
                              Container(
                                child: Center(
                                  child: InkWell(
                                    child: Image.asset('assets/namaa-'+lang+'.png',
                                      width: 75,//MediaQuery.of(context).size.width*0.3,
                                      fit: BoxFit.cover,),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                    ),
                  ]),
                ),
              ],
            ),
          ),
        )
    );
  }

}