import 'package:flutter/material.dart';
import '../Controls.dart';
import 'dart:async';

import 'package:easy_localization/easy_localization.dart';

import '../screens/opinion.dart';
import '../main.dart';
import '../screens/aboutUs.dart';
import '../screens/privacy.dart';
import '../globals.dart' as globals;

Widget MoreOptions(BuildContext context, dynamic info,String Lang,
    bool hasRegistration, bool isLogging, String Token, String Currency,
    List<dynamic> _CartMeals, int design_Control){

  TextStyle textStyleGeneral(){
    return TextStyle(
        fontFamily: Lang,
        fontWeight: FontWeight.bold,
        fontSize: SystemControls.font3,
        color: globals.accountColorsData['TextHeaderColor']!=null
            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
    );
  }
  return Stack(
    children: <Widget>[

      Container(
        child: Column(
          children: <Widget>[
            InkWell(
              onTap: (){
                print(info['infos_about']);
                Navigator.push(context,
                    MaterialPageRoute(builder: (BuildContext context) =>
                        AboutUs(info,Lang,design_Control)
                    ));
              },
              child: Container(
                margin: EdgeInsets.only(bottom: 3),
                color: Colors.white,
                height: 60,
                padding: EdgeInsets.only(right: 20,left: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("aboutUs".tr().toString(),
                      style: textStyleGeneral(),
                      textAlign: TextAlign.start,
                    ),
                    Icon(Icons.arrow_forward_ios,size: 16,color: globals.accountColorsData['NavigBarBorderColor']!=null
                        ? Color(int.parse(globals.accountColorsData['NavigBarBorderColor'])) : SystemControls.NavigBarBorderColorz,)
                  ],
                ),
              ),
            ),
            /*InkWell(
              onTap: (){

              },
              child: Container(
                margin: EdgeInsets.only(bottom: 3),
                color: Colors.white,
                height: 60,
                padding: EdgeInsets.only(right: 20,left: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(AppLocalizations.of(context).translate("branches"),
                      style: textStyleGeneral(),
                      textAlign: TextAlign.start,
                    ),
                    Icon(Icons.arrow_forward_ios,size: 16,color: globals.accountColorsData['NavigBarBorderColor']!=null
                      ? Color(int.parse(globals.accountColorsData['NavigBarBorderColor'])) : SystemControls.NavigBarBorderColorz,)
                  ],
                ),
              ),
            ),*/
            InkWell(
              onTap: (){
                print(info['infos_privacy_policy']);
                Navigator.push(context,
                    MaterialPageRoute(builder: (BuildContext context) =>
                        PrivacyPolicy(info,Lang,design_Control)
                    ));
              },
              child: Container(
                margin: EdgeInsets.only(bottom: 3),
                color: Colors.white,
                height: 60,
                padding: EdgeInsets.only(right: 20,left: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("policy".tr().toString(),
                      style: textStyleGeneral(),
                      textAlign: TextAlign.start,
                    ),
                    Icon(Icons.arrow_forward_ios,size: 16,color: globals.accountColorsData['NavigBarBorderColor']!=null
                        ? Color(int.parse(globals.accountColorsData['NavigBarBorderColor'])) : SystemControls.NavigBarBorderColorz,)
                  ],
                ),
              ),
            ), //policy
            InkWell(
              onTap: (){
                Navigator.push(context,
                    MaterialPageRoute(builder: (BuildContext context) =>
                        opinion(Lang,design_Control,Token)
                    ));
              },
              child: Container(
                margin: EdgeInsets.only(bottom: 3),
                color: Colors.white,
                height: 60,
                padding: EdgeInsets.only(right: 20,left: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("opinion".tr().toString(),
                      style: textStyleGeneral(),
                      textAlign: TextAlign.start,
                    ),
                    Icon(Icons.arrow_forward_ios,size: 16,color: globals.accountColorsData['NavigBarBorderColor']!=null
                        ? Color(int.parse(globals.accountColorsData['NavigBarBorderColor'])) : SystemControls.NavigBarBorderColorz,)
                  ],
                ),
              ),
            ), //opinion
            globals.numOfLang>1?(
                InkWell(
              onTap: (){
                if(Lang == "en"){
                  Center(
                    child: SystemControls().circularProgress(),
                  );

                  SystemControls().storeDataToPreferences("lang", "ar").then((value){
                    //appLanguage.changeLanguage(Locale("ar"));
                    EasyLocalization.of(context).setLocale(Locale('ar','EG'));
                    print('\nSaving Done $value....\n');
                    Navigator.pushAndRemoveUntil(context,
                        MaterialPageRoute(builder: (BuildContext context) => MyApp()),
                        ModalRoute.withName('/'));
                  });

                }
                else {
                  Center(
                    child: SystemControls().circularProgress(),
                  );
                  //AppLocalizations(Locale('ar', ''));
                  SystemControls().storeDataToPreferences("lang", "en").then((value){
                   EasyLocalization.of(context).setLocale(Locale('en','EN'));
                    print('\nSaving Done $value....\n');
                    Navigator.pushAndRemoveUntil(context,
                        MaterialPageRoute(builder: (BuildContext context) => MyApp()),
                        ModalRoute.withName('/'));
                  });
                }
                Navigator.pushAndRemoveUntil(context,
                    MaterialPageRoute(builder: (BuildContext context) => MyApp()),
                    ModalRoute.withName('/'));
              },
              child: Container(
                margin: EdgeInsets.only(bottom: 3),
                color: Colors.white,
                height: 60,
                padding: EdgeInsets.only(right: 20,left: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("changeLanguage".tr().toString(),
                      style: textStyleGeneral(),
                      textAlign: TextAlign.start,
                    ),
                    Icon(Icons.arrow_forward_ios,size: 16,color: globals.accountColorsData['NavigBarBorderColor']!=null
                        ? Color(int.parse(globals.accountColorsData['NavigBarBorderColor'])) : SystemControls.NavigBarBorderColorz,)
                  ],
                ),
              ),
            )
            ):(Container()), //change language
          ],
        ),
      ),
    ],
  );
}