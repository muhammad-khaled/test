import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';
import '../dataControl/dataModule.dart';
import '../Controls.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';
import '../widgets/UI_Alert_Widgets.dart';
import 'dart:io';
import 'dart:async';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import '../widgets/homeItemsList.dart';
import '../widgets/categoriesList.dart';
import '../widgets/drawerWidge.dart';
import '../screens/cart1Items.dart';
import '../screens/categoryListNew.dart';
import '../dataControl/cartModule.dart';
import '../widgets/UI_Widgets_CategoriesList.dart';

import '../locale/app_localization.dart';
import '../widgets/adsSliderNew.dart';

import '../screens/offersListNew.dart';
import '../screens/AccountHome.dart';
import '../screens/MoreHome.dart';
import '../screens/orderDetails.dart';
import '../screens/notificationLayoutList.dart';

import '../widgets/categoriesListVertical.dart';
import 'package:easy_localization/easy_localization.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Widgets_InputField.dart';
import '../widgets/SearchPainter.dart';
import '../widgets/SearchAppBar.dart';
import '../provider/productsInCartProvider.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:android_intent/android_intent.dart';
import 'package:geolocator/geolocator.dart';

import 'package:image_picker/image_picker.dart';
import 'package:flutter/foundation.dart';

import 'package:firebase_messaging/firebase_messaging.dart';

import '../globals.dart' as globals;

class Home extends StatefulWidget {
  String Lang;
  Home(this.Lang);
  _home createState() => _home(Lang);
}

class _home extends State<Home> with SingleTickerProviderStateMixin{
  //TODO get it from APIs
  int design_Control = SystemControls.designControl;
  _home(this.Lang);
  String Lang;





  // Future<void> _ConfirmDialogAlert(BuildContext context,String title,String orderId) {
  //   print(title);
  //   return showDialog<void>(
  //     context: context,
  //     barrierDismissible: false, // user must tap button!
  //     builder: (BuildContext context) {
  //       return AlertDialog(
  //         shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
  //         ),
  //         title: Text(title,
  //           style: TextStyle(
  //               fontFamily: Lang,
  //               fontWeight: FontWeight.bold,
  //               fontSize: SystemControls.font3,
  //               color: globals.accountColorsData['TextHeaderColor']!=null
  //                   ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
  //           ),
  //           textAlign: TextAlign.center,
  //         ),
  //         contentPadding: EdgeInsets.only(top: 40,bottom: 0),
  //         content: ClipRRect(
  //           borderRadius: BorderRadius.only(
  //             bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
  //             bottomRight: Radius.circular(SystemControls.RadiusCircValue),
  //           ),
  //           child: Container(
  //             height: 40,
  //             child: Row(
  //               children: <Widget>[
  //                 Expanded(
  //                   flex: 1,
  //                   child: Container(
  //                     decoration: BoxDecoration(
  //                       color: globals.accountColorsData['MainColor']!=null
  //                           ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
  //                       border: Border(
  //                         top: BorderSide(color: Colors.black),
  //                       ),
  //                     ),
  //                     child: FlatButton(
  //                       child: Text("yes".tr().toString(),
  //                         style: TextStyle(
  //                             fontFamily: Lang,
  //                             fontWeight: FontWeight.normal,
  //                             fontSize: SystemControls.font3,
  //                             color: globals.accountColorsData['TextOnMainColor']!=null
  //                                 ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
  //                         ),
  //                       ),
  //                       onPressed: () async{
  //                         //TODO
  //                         var res = await getUserOrderById(Lang,UserToken,orderId);
  //                         if(res !=null && res.status == 200){
  //                           dynamic orderDetails = res.data['order'];
  //                           print("###### " + orderDetails.toString());
  //                           Navigator.of(context).pop();
  //                           //Navigator.of(context).pop();
  //                           Navigator.push(context,
  //                               MaterialPageRoute(
  //                                   builder: (context) =>
  //                                       OrderDetails(
  //                                           Lang, AppCurrency,
  //                                           orderDetails,
  //                                           design_Control,UserToken,1)
  //                               ));
  //                         }
  //                         else if(res.status==401){
  //                           print("Home 259 .... 401 cart");
  //                           SystemControls().LogoutSetUserData(context, Lang);
  //                           ErrorDialogAlertBackHome(context, res.message);
  //                         }
  //                       },
  //                     ),
  //                   ),
  //                 ),
  //                 Container(
  //                   color: Colors.black,
  //                   width: 1,
  //                   height: 40,
  //                 ),
  //                 Expanded(
  //                   flex: 1,
  //                   child: Container(
  //                     decoration: BoxDecoration(
  //                       color: globals.accountColorsData['MainColor']!=null
  //                           ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
  //                       border: Border(
  //                         top: BorderSide(color: Colors.black),
  //                       ),
  //                       //borderRadius: BorderRadius.all(Radius.circular(10)),
  //                     ),
  //                     child: FlatButton(
  //                       child: Text("no".tr().toString(),
  //                         style: TextStyle(
  //                             fontFamily: Lang,
  //                             fontWeight: FontWeight.normal,
  //                             fontSize: SystemControls.font3,
  //                             color: globals.accountColorsData['TextOnMainColor']!=null
  //                                 ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
  //                         ),
  //                       ),
  //                       onPressed: () {
  //                         Navigator.of(context).pop();
  //                       },
  //                     ),
  //                   ),
  //                 ),
  //               ],
  //             ),
  //           ),
  //         ),
  //       );
  //     },
  //   );
  // }

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  new FlutterLocalNotificationsPlugin();
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();

  }
  FirebaseMessaging _firebaseMessaging ;

  String tokenData;
  @override
  void initState() {


    _searchScrollController = ScrollController();
    _searchScrollController.addListener(_searchScrollListener);
    _homeScrollController = ScrollController();
    _homeScrollController.addListener(_homeScrollListener);
    super.initState();

    // flutterLocalNotificationsPlugin
    //     .resolvePlatformSpecificImplementation<
    //     MacOSFlutterLocalNotificationsPlugin>()
    //     ?.requestPermissions(
    //   alert: true,
    //   badge: true,
    //   sound: true,
    // );
    //
    // WidgetsFlutterBinding.ensureInitialized();
    //
    //
    //
    // FirebaseMessaging.onMessage.listen((RemoteMessage message) {
    //   print(message.data);
    //   print(message.notification.body);
    //   print(message.notification.title);
    //   print(message.messageId);
    //   print(message.messageType);
    //   // displayNotificationAndroid(message);
    //   // _handleNotification(message.data,true,1);
    //
    // });

    // FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
    //   print('A new onMessageOpenedApp event was published!');
    //   // Navigator.pushNamed(context, '/message',
    //   //     arguments: MessageArguments(message, true));
    // });

/////////////////////////////////
//     _firebaseMessaging.configure(
//       onMessage: (Map<String, dynamic> message) async{
//         displayNotificationIos(message);
//
//         _handleNotification(message,true,1);
//         print('on message $message \n\n');
//       },
//       onResume: (Map<String, dynamic> message)async {
//         displayNotificationIos(message);
//
//         _handleNotification(message,true,2);
//         print('on resume $message \n\n');
//       },
//       onLaunch: (Map<String, dynamic> message)async {
//         displayNotificationIos(message);
//
//         _handleNotification(message,true,3);
//         print('on launch $message \n\n');
//       },
//     );
    // _firebaseMessaging.requestNotificationPermissions(
    //     const IosNotificationSettings(
    //         sound: true, badge: true, alert: true, provisional: true));
    // _firebaseMessaging.onIosSettingsRegistered
    //     .listen((IosNotificationSettings settings) {
    //   print("Settings registered: $settings");
    // });
    FirebaseMessaging.instance.getToken().then((String token) {
      assert(token != null);
      String _homeScreenText = "Waiting for token...";
      _homeScreenText = "Push Messaging token: $token";
      tokenData =token;
      print(_homeScreenText);
    });
    ///////////////////////
    //Check if gps opened in this device or not
    // CheckGpsAlert(context);

    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);


    //Search
    _controller = AnimationController(vsync: this,
        duration: Duration(milliseconds: Lang=="ar"?0:500));
    _animation = Tween(begin: 0.0, end: 1.0).animate(_controller);
    _controller.addStatusListener(animationStatusListener);

  }
  @override
  void dispose() {
    super.dispose();
    _connectivitySubscription.cancel();
  }
  // Future displayNotificationAndroid(RemoteMessage message) async {
  //
  //
  //
  //   var androidPlatformChannelSpecifics = new  AndroidNotificationDetails('1', 'name', 'desc',
  //     icon: "@mipmap/ic_launcher",
  //     largeIcon: DrawableResourceAndroidBitmap("@mipmap/ic_launcher"),
  //     enableLights: true,
  //     importance: Importance.max,
  //
  //     priority: Priority.high,
  //     color: Colors.red,
  //     playSound: true,
  //
  //
  //   );
  //   var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
  //   var platformChannelSpecifics = new NotificationDetails(android: androidPlatformChannelSpecifics,iOS: iOSPlatformChannelSpecifics);
  //   await flutterLocalNotificationsPlugin.show(
  //     0,
  //     "Ahmed Gharib Pharmacy",
  //
  //     message.notification.body.toString(),
  //     platformChannelSpecifics,
  //     payload: message.data["click_action"].toString(),
  //   );
  // }
  // Future displayNotificationIos(Map<String, dynamic> message) async {
  //
  //   var androidPlatformChannelSpecifics = new  AndroidNotificationDetails('1', 'name', 'desc',
  //     icon: "@mipmap/ic_launcher",
  //     largeIcon: DrawableResourceAndroidBitmap("@mipmap/ic_launcher"),
  //     enableLights: true,
  //     importance: Importance.max,
  //
  //     priority: Priority.high,
  //     color: Colors.red,
  //     playSound: true,
  //
  //
  //   );
  //   var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
  //   var platformChannelSpecifics = new NotificationDetails(android: androidPlatformChannelSpecifics,iOS: iOSPlatformChannelSpecifics);
  //   await flutterLocalNotificationsPlugin.show(
  //     0,
  //     "Adam Pharmacy",
  //
  //     message['message'].toString(),
  //     platformChannelSpecifics,
  //     payload: message['payload'].toString(),
  //   );
  // }
  // Future displayNotification(Map<String, dynamic> message) async {
  //       if(message['notification']["title"]=="Message"||message['notification']["title"]=="الرسالة"){
  //        Provider.of<NotificationProvider>(context,listen: false).getNum();
  //       }
  //     var androidPlatformChannelSpecifics = new  AndroidNotificationDetails('1', 'name', 'desc',
  //       icon: "@mipmap/ic_launcher",
  //       largeIcon: DrawableResourceAndroidBitmap("@mipmap/ic_launcher"),
  //       enableLights: true,
  //       importance: Importance.max,
  //
  //       priority: Priority.high,
  //       color: Colors.red,
  //       playSound: true,
  //
  //
  //     );
  //     var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
  //     var platformChannelSpecifics = new NotificationDetails(android: androidPlatformChannelSpecifics,iOS: iOSPlatformChannelSpecifics);
  //     await flutterLocalNotificationsPlugin.show(
  //       0,
  //       "Ahmed Gharib Pharmacy",
  //
  //       message['notification']['body'].toString(),
  //       platformChannelSpecifics,
  //       payload: message['payload'].toString(),
  //     );
  // }
  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: ' + payload);
    }
  }
  // void logggg(){
  //   setState(() {
  //     print("Home isLogging -> in**" + isLogging.toString());
  //     storage.getCurrentLoggingStatus().then((stause){
  //       if(stause != null) {
  //         isLogging = stause;
  //         print("Home isLogging ->" + isLogging.toString());
  //       }
  //       else{
  //         isLogging = false;
  //       }
  //       storg.getDataFromPreferences('api_token').then((token){
  //         if(token != null) {
  //           UserToken = token;
  //           print("Home token ->" + UserToken.toString());
  //         }
  //         else{
  //           UserToken = "";
  //         }
  //         storg.getDataFromPreferences('currency').then((token){
  //           AppCurrency = token;
  //
  //           print("Home AppCurrency ->" + AppCurrency);
  //         });
  //       });
  //     });
  //
  //   });
  // }

  final SystemControls storg = SystemControls();

  Future<ResponseApi> resp;// = fetchList(Lang);
  Future<ResponseApi> respHomItems;// = fetchList(Lang);
  Future<ResponseApi> respInitial;// = InitialApi(Lang);
  Future<ResponseApi> searchCaegories;
  //To relaod when open Home page again
  Future<CartList> localCart = read_from_file();


  final SystemControls storage = SystemControls();

  bool hasRegistration = false;
  dynamic AppInitialInfo;
  String UserToken = "";
  String AppCurrency = "";
  List<dynamic> _cartList = <dynamic>[];
  List<dynamic> _meals = <dynamic>[];
  List<dynamic> _categoriesz = <dynamic>[];
  List<dynamic> _offers = <dynamic>[];

  Widget RequestHasError(){
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text("respError".tr().toString(),
            style: TextStyle(
              fontFamily: Lang,
              fontWeight: FontWeight.normal,
              fontSize: SystemControls.font2,
              height: 1.2,
            ),
            textAlign: TextAlign.center,
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            width: 150,
            decoration: BoxDecoration(
              color: globals.accountColorsData['MainColor']!=null
                  ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
              borderRadius: BorderRadius.all(Radius.circular(
                  SystemControls.RadiusCircValue
              )),
            ),
            child: FlatButton(
              onPressed: () {
                print("Reload");
                setState(() {
                  resp = fetchList(Lang,UserToken);
                  respHomItems = homeItemsGet(Lang, UserToken, homeReqPageindex.toString());
                  searchCaegories = getSearchCategories(Lang);
                  respInitial = InitialApi(Lang,context);
                  searchCaegories = getSearchCategories(Lang);
                  localCart = read_from_file();
                });
              },
              child: Center(
                child: Text("reload".tr().toString(),
                    style: TextStyle(
                      fontFamily: Lang,
                      fontWeight: FontWeight.bold,
                      fontSize: SystemControls.font2,
                    )
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  Widget SearchCategryRecer(List<dynamic> _subCategories){
    return ListView.builder(
        padding: EdgeInsets.zero,
        shrinkWrap : true,
        physics: ScrollPhysics(),
        scrollDirection: Axis.vertical,
        itemCount: _subCategories.length,
        itemBuilder: (context, index){
          return Column(
            children: <Widget>[
              InkWell(
                onTap: (){

                  onSearchSelectCat(_subCategories[index]['categories_id'],_subCategories[index]['categories_title']);
                },
                child: Container(
                  //margin: EdgeInsets.only(right: 10,left: 10),
                  padding: EdgeInsets.only(right: 15,left: 15),
                  height: 30,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        _subCategories[index]['categories_title'],
                        //list[position],
                        style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font3,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                          height: 1.2,
                        ),
                        textAlign: TextAlign.center,
                        maxLines: 2,
                      ),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 13,
                        color: globals.accountColorsData['NavigBarBorderColor']!=null
                            ? Color(int.parse(globals.accountColorsData['NavigBarBorderColor'])) : SystemControls.NavigBarBorderColorz,)
                    ],
                  ),
                ),
              ),
              _subCategories[index]['subcategories'].length>=1?(
                  Container(
                    padding: EdgeInsetsDirectional.fromSTEB(40, 0, 0, 0),
                    child: SearchCategryRecer(_subCategories[index]['subcategories']),
                  )
              ):(Container()),
            ],
          );
        }
    );
  }

  ///Img picker part
  final ImagePicker _picker = ImagePicker();
  void _onImageButtonPressed(ImageSource source) async {
    try {
      final pickedFile = await _picker.getImage(
        source: source,
      );
      if(pickedFile.path != null){
        CartStateProvider cartState = Provider.of<CartStateProvider>(context,listen: false);
        cartState.setImgFilePicker(pickedFile);
        String msg = 'AddPicToCartSuccMsg'.tr().toString();
        // SuccDialogAlertStaySameLayout(context,msg);
      }

    } catch (e) {
      print("Image picker error : ");
      print(e);
    }
  }
  Future<void> showBottomSelectImgWidget({BuildContext context}){
    return showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            color: Color(0xFF737373),
            height: 180,
            child: Container(
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Text("imgPickerHeader".tr().toString(),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.bold,
                          fontSize: SystemControls.font3,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                          height: 1.2,
                        )
                    ),
                  ),
                  ListTile(
                      leading: Icon(Icons.photo_library),
                      title: Text("imgPickerGallery".tr().toString()),
                      onTap: (){
                        print('Select From gallery');
                        _onImageButtonPressed(ImageSource.gallery);
                        Navigator.pop(context);
                      }
                  ),
                  ListTile(
                      leading: Icon(Icons.camera_alt),
                      title: Text("imgPickerCamera".tr().toString()),
                      onTap: (){
                        print('Take Photo With Camera');
                        _onImageButtonPressed(ImageSource.camera);
                        Navigator.pop(context);
                      }
                  ),
                ],
              ),
              decoration: BoxDecoration(
                color: Theme.of(context).canvasColor,
                borderRadius: BorderRadius.only(
                  topLeft: const Radius.circular(10),
                  topRight: const Radius.circular(10),
                ),
              ),
            ),
          );
        });
  }
  ///
  Widget build(BuildContext context) {
    //_meals = <dynamic>[];
    //_categoriesz = <dynamic>[];
    _offers = <dynamic>[];

    localCart = read_from_file();

    final mediaQuery = MediaQuery.of(context);

    return WillPopScope(
      onWillPop: ()async{
        if(!Navigator.canPop(context)){
          return showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text("ConfirmExit".tr().toString()),
                  content: Text("Areyousureyouwanttoexit?".tr().toString()),
                  actions: <Widget>[
                    FlatButton(
                      child: Text("yes".tr().toString()),
                      onPressed: () {
                        SystemNavigator.pop();
                      },
                    ),
                    FlatButton(
                      child: Text("no".tr().toString()),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    )
                  ],
                );
              }
          );
        }else{
          Navigator.pop(context);
          print('caaaaaaaaannnnnana');
        }
        return Future.value(true);

      },
      child: FutureBuilder<CartList>(
        future: localCart,
        builder: (context,Cartsnap){
          if(Cartsnap.connectionState == ConnectionState.done && Cartsnap.hasData){
            _cartList = Cartsnap.data.Cmeals;
            CartStateProvider cartState = Provider.of<CartStateProvider>(context,listen: false);
            if(cartState.productsCount=="0" && _cartList.length != 0){
              cartState.setCurrentProductsCount(_cartList.length.toString());
            }
          }
          return FutureBuilder<ResponseApi>(
              future: respInitial,
              builder: (context, snap){
                if(snap.hasData){
                  AppInitialInfo = snap.data.data['info'];
                  hasRegistration = snap.data.data['hasRegistration'];
                  return _Body();
                }
                else if(snap.hasError){
                  return RequestHasError();
                }
                return Stack(
                  children: <Widget>[
                    LayoutBG(design_Control),
                    Center(
                        child: SystemControls().circularProgress()
                    ),
                  ],
                );
              }
          );

        },
      ),
    );
  }

  bool isLogging;

  double CatStorageKey = 0;
  ScrollController _scrollControllerItem;
  Widget _Body(){

    final mediaQuery = MediaQuery.of(context);
    print("\n\n\n"+"HomeBody !!!!!!!");
    return FutureBuilder<ResponseApi>(
      future: resp,
      builder: (context, snapshot){
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Stack(
            children: <Widget>[
              LayoutBG(design_Control),
              Center(
                  child: SystemControls().circularProgress()
              ),
            ],
          );
        }
        if(snapshot.hasData){
          dynamic RepData = snapshot.data.data;
          //_meals = RepData['homeProducts'];//snapshot.data.homeMeals;
          _categoriesz = RepData['categories'];//snapshot.data.categories;
          print("\n\n\n\n\n "+_categoriesz.length.toString()+"\n\n\n\n\n");


          print('Offfffeeeerrrrssss');
          print(RepData['offers']);
          print('Offfffeeeerrrrssss');
          _offers = RepData['offers'];
          print(_offers.length);

          //snapshot.data.offers;
          return SafeArea(
            bottom: true,
            top: false,
            child: Scaffold(
              appBar: HomeAppBar(),
              bottomNavigationBar: design_Control==1?(
                  SizedBox(height: 1,)
              ):(
                  SizedBox(
                    height: 62,
                    child: Column(
                      children: <Widget>[
                        /*Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Container(
                                height: 1,
                                color: globals.accountColorsData['NavigBarBorderColor']!=null
                      ? Color(int.parse(globals.accountColorsData['NavigBarBorderColor'])) : SystemControls.NavigBarBorderColorz,
                              ),
                            ),
                          ],
                        ),*/
                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Container(
                                height: 3,
                                color: navigBarindex==0
                                    ? (globals.accountColorsData['MainColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz):Colors.white,
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                height: 3,
                                color: navigBarindex==1
                                    ? (globals.accountColorsData['MainColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz):Colors.white,
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                height: 3,
                                color: navigBarindex==2
                                    ? (globals.accountColorsData['MainColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz):Colors.white,
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                height: 3,
                                color: navigBarindex==3
                                    ? (globals.accountColorsData['MainColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz):Colors.white,
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                height: 3,
                                color: navigBarindex==4
                                    ? (globals.accountColorsData['MainColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz):Colors.white,
                              ),
                            ),
                          ],
                        ),
                        menu()
                      ],
                    ),
                  )
              ),
              body: Stack(
                children: <Widget>[
                  LayoutBG(design_Control),
                  navigatorBody(_selectedIndex),
                  AnimatedBuilder(
                    animation: _animation,
                    builder: (context, child) {
                      return CustomPaint(
                        painter: MyPainter(
                          containerHeight: 75,
                          center: Offset(0, 0),
                          radius: _animation.value * MediaQuery.of(context).size.width,
                          context: context,
                        ),
                      );
                    },
                  ),
                  isInSearchMode ? (
                      SearchBar(
                        onCancelSearch: cancelSearch,
                        onSearchQuerySubmit: onSearchSubmit,
                      )
                  ) : (
                      Container()
                  ),
                  isInSearchMode&&showSearchCat ?(
                      Container(
                        margin: EdgeInsets.only(top: 80),
                        //height: 500,
                        color: Colors.white,
                        child: FutureBuilder<ResponseApi>(
                          future: searchCaegories,
                          builder: (context, snap){
                            List<dynamic> _categories = <dynamic>[];
                            if(snap.hasData){
                              _categories = snap.data.data['categories'];
                              return ListView.builder(
                                scrollDirection: Axis.vertical,
                                itemCount: _categories.length,
                                itemBuilder: (context, index){
                                  if(_categories==null){
                                    return Container();
                                  }
                                  return Column(
                                    children: <Widget>[
                                      InkWell(
                                        onTap: (){
                                          onSearchSelectCat(_categories[index]['categories_id'],_categories[index]['categories_title']);
                                        },
                                        child: Container(
                                          //margin: EdgeInsets.only(right: 10,left: 10),
                                          padding: EdgeInsets.only(right: 15,left: 15),
                                          height: 40,
                                          color: Colors.white,
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                _categories[index]['categories_title'],
                                                //list[position],
                                                style: TextStyle(
                                                  fontFamily: Lang,
                                                  fontWeight: FontWeight.normal,
                                                  fontSize: SystemControls.font3,
                                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                                  height: 1.2,
                                                ),
                                                textAlign: TextAlign.center,
                                                maxLines: 2,
                                              ),
                                              Icon(
                                                Icons.arrow_forward_ios,
                                                size: 16,
                                                color: globals.accountColorsData['NavigBarBorderColor']!=null
                                                    ? Color(int.parse(globals.accountColorsData['NavigBarBorderColor'])) : SystemControls.NavigBarBorderColorz,)
                                            ],
                                          ),
                                        ),
                                      ),
                                      _categories[index]['subcategories'].length>=1?(
                                          Container(
                                            padding: EdgeInsetsDirectional.fromSTEB(40, 0, 0, 0),
                                            child: SearchCategryRecer(_categories[index]['subcategories']),
                                          )
                                      ):(Container()),
                                    ],
                                  );
                                },
                              );
                            }
                            else if(snap.hasError){
                              return RequestHasError();
                            }
                            return Center(
                              child: SystemControls().circularProgress(),
                            );
                          },
                        ),
                      )
                  ):(Container()),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: ConnectionStatusBar(),
                  ),
                ],
              ),
              floatingActionButton: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  SystemControls.cartAddPhoto?(
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          FloatingActionButton(
                            onPressed: () {
                              showBottomSelectImgWidget(context: context);
                            },
                            heroTag: 'image0',
                            tooltip: 'Pick Image',
                            backgroundColor: globals.accountColorsData['MainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['MainColor']))
                                : SystemControls.MainColorz,
                            child: const Icon(Icons.add_photo_alternate,size: 35,),
                          ),
                          Container(
                            color: Colors.white,
                            //width: 80,
                            margin: EdgeInsets.only(top: 10),
                            child: Text('AddPicToCart'.tr().toString(),
                              style: TextStyle(
                                fontFamily: Lang,
                                fontWeight: FontWeight.normal,
                                fontSize: SystemControls.font2,
                                height: 1.2,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          )
                        ],
                      )
                  ):(Container()),
                ],
              ),
              drawer: design_Control==1?(
                  HomeDrawerOut(context, Lang, UserToken,
                      AppCurrency,_cartList,design_Control,
                      hasRegistration,AppInitialInfo,_categoriesz)
              ):(
                  null
              ),
            ),
          );
          return navigatorBody(_selectedIndex);//
        }
        else if(snapshot.hasError){
          print(snapshot.error);
          return RequestHasError();
        }else if(!snapshot.hasData){
          return Center(
            child: CircularProgressIndicator(),
          );
        }else{


          return Stack(
            children: <Widget>[
              LayoutBG(design_Control),
              Center(
                  child: SystemControls().circularProgress()
              ),
            ],
          );
        }
      },
    );
  }

  TextEditingController _searchFieldController = TextEditingController();
  Widget HomeAppBar(){
    print("\n\n\n"+"HomeAppBar !!!!!!!");
    if(SystemControls.branchesValid){
      //return CustomAppBar();
      return AppBar(
        title: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Jeddah Branch"),
            ],
          ),
        ),
        centerTitle: false,
        backgroundColor: AppBarBGColor(),
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
        leading: Builder(
          builder: (context)=> DrawerIcon(context,design_Control),
        ),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(125),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsetsDirectional.fromSTEB(72, 0, 10, 0),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.location_on,
                          color: Colors.black,
                          size: 20,
                        ),
                        Padding(padding: EdgeInsets.all(2)),
                        Text("Al Hamadaniyyah, Jeddah 23747, Sauid",
                          style: TextStyle(
                            fontFamily: Lang,
                            fontSize: SystemControls.font2,
                            color: globals.accountColorsData['TextHeaderColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                            fontWeight: FontWeight.normal,
                            height: 1,
                          ),
                          maxLines: 1,
                          textAlign: TextAlign.start,
                        )
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.phone,
                                color: Colors.black,
                                size: 20,
                              ),
                              Padding(padding: EdgeInsets.all(2)),
                              Text("92007519",
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontSize: SystemControls.font2,
                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                  fontWeight: FontWeight.normal,
                                  height: 1,
                                ),
                                maxLines: 1,
                                textAlign: TextAlign.start,
                              )
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 5,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.access_time,
                                color: Colors.black,
                                size: 20,
                              ),
                              Padding(padding: EdgeInsets.all(2)),
                              Text("2020-20-20 15:45:12 AM",
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontSize: SystemControls.font2,
                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                  fontWeight: FontWeight.normal,
                                  height: 1,
                                ),
                                maxLines: 1,
                                textAlign: TextAlign.start,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                height: 50,
                margin: EdgeInsets.all(20),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(
                              Radius.circular(SystemControls.RadiusCircValue)),
                        ),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: TextFormField(
                                controller: _searchFieldController,
                                decoration: InputFieldDecoration("search".tr().toString(), Lang),
                                keyboardType: TextInputType.text,
                                style: new TextStyle(
                                  fontFamily: Lang,
                                ),
                              ),
                            ),
                            InkWell(
                              child: Container(
                                height: 50,
                                width: 50,
                                margin: EdgeInsetsDirectional.fromSTEB(10, 0, 0, 0),
                                child: Icon(Icons.send,
                                    color: Colors.black),
                              ),
                              onTap: (){
                                onSearchSubmit(_searchFieldController.text);
                              },
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsetsDirectional.fromSTEB(10, 0, 0, 0),
                      height: MediaQuery.of(context).size.height,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                            Radius.circular(SystemControls.RadiusCircValue)
                        ),
                      ),
                      child: AppBarCartButton(context, OnClickCartButton,
                          _cartList.length.toString(),hasRegistration),
                    )
                  ],
                ),
              ),

            ],
          ),
        ),
      );
    }
    else{
      return AppBar(
        title: AppBarTitleImg(Lang),
        centerTitle: false,
        backgroundColor: AppBarBGColor(),
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
        leading: Builder(
          builder: (context)=> DrawerIcon(context,design_Control),
        ),
        actions: <Widget>[
          //CartButtonAppBar(),
          AppBarNotificationButton(context, OnClickNotificationButton,
              hasRegistration,Lang,UserToken),
          AppBarCartButton(context, OnClickCartButton,
              _cartList.length.toString(),hasRegistration),
          isInSearchMode ? (
              HomeButtonAppBar(design_Control)
          ) : (
              GestureDetector(
                child: IconButton(
                  icon: SystemControls().GetSVGImagesAsset(
                      "assets/Icons/search.svg",
                      25,
                      Colors.black),
                ),
                onTapUp: onSearchTapUp,)
          ),
        ],
      );
    }
  }
  OnClickNotificationButton(){
    if(UserToken == ""){
      ErrorDialogAlertGoTOLogin(context,
          "mustLogin".tr().toString(),Lang);
    }
    else{

      Navigator.pushAndRemoveUntil(context,
          MaterialPageRoute(builder: (BuildContext context) =>
              NotificationList(Lang, UserToken, AppCurrency,design_Control)
          ),(Route<dynamic> route) => true
      );
    }
  }
  OnClickCartButton(){
    Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (BuildContext context) =>
            CartLayout(Lang,UserToken,AppCurrency,design_Control)
        ),(Route<dynamic> route) => true
    );
  }
  HomeButtonAppBar(int DesignControl){
    return IconButton(
        icon: SystemControls().GetSVGImagesAsset(
            "assets/Icons/search.svg",
            25,
            Colors.black),
        onPressed: (){
          print("on click button main bar ");
          cancelSearch();
        }
    );

  }

  ///Search App bar
  AnimationController _controller;
  Animation _animation;
  bool isInSearchMode = false;
  bool showSearchCat = true;
  String searchText = "";
  int searchCatId = 0;
  String searchCatName = "";
  animationStatusListener(AnimationStatus animationStatus) {
    if (animationStatus == AnimationStatus.completed) {
      setState(() {
        isInSearchMode = true;
      });
    }
  }
  void onSearchTapUp(TapUpDetails details) {
    _controller.forward();
  }
  cancelSearch() {
    setState(() {
      isInSearchMode = false;
      showSearchCat = true;
      _selectedIndex = 0;
      navigBarindex = 0;
      searchCatName = "";
    });
    _controller.reverse();
  }
  onSearchSubmit(String query){
    print("Submit This text to get search result $query");
    setState(() {
      StartLoad = false;
      stopRequest = true;
      showSearchCat = true;
      currentPageindex=0;
      maxPageIndex=1;

      isInSearchMode = false;
      searchCatId = 0;
      _selectedIndex = 10;

      searchText = query;
      searchCatName = "";
      _searchMeals.clear();
      resSearch = SearchWithtitleApi(Lang, query, UserToken,currentPageindex.toString());
    });
    _controller.reverse();
  }
  onSearchSelectCat(int CatID, String catName){
    print("Submit This text to get search result with catID $CatID");
    setState(() {
      StartLoad = false;
      stopRequest = true;
      showSearchCat = true;
      currentPageindex=0;
      maxPageIndex=1;

      resSearch = CategoryListApi(Lang, CatID, UserToken,"1");
      _searchMeals.clear();
      searchCatId = CatID;
      searchCatName = catName;
      _selectedIndex = 10;

      isInSearchMode = false;
    });
    _controller.reverse();
  }

  ScrollController _searchScrollController;
  int currentPageindex=0;
  int maxPageIndex=1;
  bool StartLoad = false;
  bool stopRequest = true;
  String nextUrl = "";
  _searchScrollListener() {
    if( nextUrl !=null){
      String num = nextUrl.split("=").last;
      if(_searchScrollController.position.pixels==_searchScrollController.position.maxScrollExtent
          && currentPageindex < maxPageIndex){
        currentPageindex +=1;

        setState(() {
          print("load new page ..... \n\n"+num);
          StartLoad = true;
          stopRequest = true;
          if(showSearchCat == false) {

            resSearch = SearchWithtitleApi(
                Lang, searchText, UserToken, num);

          }
          else {
            resSearch = SearchWithtitleApi(
                Lang, searchText, UserToken, num);          }
        });
      }
      else if(currentPageindex == maxPageIndex && StartLoad==true) {
        setState(() {
          StartLoad = false;
        });
      } }

  }
  //////////////
  int homeReqPageindex=0;
  int homeReqmaxPageIndex=1;
  bool homeStartLoad = false;
  bool homestopRequest = false;
  ScrollController _homeScrollController;
  _homeScrollListener() {
    if(_selectedIndex == 0) {
      //print(_homeScrollController.position.pixels);
      if(_homeScrollController.position.pixels==_homeScrollController.position.maxScrollExtent
          && homeReqPageindex < homeReqmaxPageIndex&& homestopRequest == false){
        //homeReqPageindex;
        setState(() {
          print("load new page ..... \n\n"+homeReqPageindex.toString());
          homeStartLoad = true;
          homestopRequest = true;
          respHomItems = homeItemsGet(Lang, UserToken, (homeReqPageindex+1).toString());
        });
      }
      else if(homeReqPageindex == homeReqmaxPageIndex){
        setState(() {
          print("load new page 2222 ..... ");
          homeStartLoad = false;
        });
      }
      else{
        print("load new page final ..... ");
      }
    }
  }
  //////////////
  onAddCartSubmit(List<dynamic> _cart){
    setState(() {
      _cartList = _cart;

    });
  }
  onSeaFavSubmit(List<dynamic> _mealsg){
    setState(() {
      // _meals = _mealsg;
      print("Add  function callback to use -> setState");
    });
  }
  //////////////
  ConnectionStatusBar(){
    final Color color = Colors.redAccent;
    final double width = double.maxFinite;
    final double height = 30;

    if(_connectionStatus == 'connected'){
      return Container();
    }
    else{
      return Directionality(
        // textDirection: TextDirection.ltr,
        child: Container(
          child: SafeArea(
            bottom: false,
            child: Container(
              color: color,
              width: width,
              height: height,
              child: Container(
                margin: EdgeInsets.only(right: 5,left: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Please check your internet connection',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'en',
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font4,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        InkWell(
                          child: Text(
                            'Try Again..',
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'en',
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font4,
                            ),
                          ),
                          onTap: (){
                            setState(() async{
                              initConnectivity();
                            });
                          },
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    }
  }
  ///Internet Check area
  String _connectionStatus = 'connected';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }
  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = "connected";
              resp = fetchList(Lang,UserToken);
              respHomItems = homeItemsGet(Lang, UserToken, homeReqPageindex.toString());
              searchCaegories = getSearchCategories(Lang);
              respInitial = InitialApi(Lang,context);
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = "failed";
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState((){
          _connectionStatus = "connected";
          resp = fetchList(Lang,UserToken);
          respHomItems = homeItemsGet(Lang, UserToken, homeReqPageindex.toString());
          searchCaegories = getSearchCategories(Lang);
          respInitial = InitialApi(Lang,context);
        });
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "failed");
        break;
      default:
        setState(() => _connectionStatus = 'failed');
        break;
    }
  }

  Widget menu() {
    if(design_Control == 1){
      return Container();
    }
    else{
      return BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items:[
          BottomNavigationBarItem(
            icon: SystemControls().GetSVGImagesAsset(
                'assets/Icons/home.svg', 23,
                null),
            activeIcon: SystemControls().GetSVGImagesAsset(
                'assets/Icons/home.svg', 25,
                Colors.black),
            title: Text('home'.tr().toString()),

          ),
          BottomNavigationBarItem(
            icon: SystemControls().GetSVGImagesAsset(
                'assets/Icons/menu.svg', 23,
                null),
            activeIcon: SystemControls().GetSVGImagesAsset(
                'assets/Icons/menu.svg', 25,
                Colors.black),
            title: Text('seactions'.tr().toString()),
          ),
          BottomNavigationBarItem(
            icon: SystemControls().GetSVGImagesAsset(
                'assets/Icons/offer.svg', 23,
                null),
            activeIcon: SystemControls().GetSVGImagesAsset(
                'assets/Icons/offer.svg', 25,
                Colors.black),
            title: Text('offers'.tr().toString()),
          ),
          BottomNavigationBarItem(
            icon: SystemControls().GetSVGImagesAsset(
                'assets/Icons/account.svg', 23,
                null),
            activeIcon: SystemControls().GetSVGImagesAsset(
                'assets/Icons/account.svg', 25,
                Colors.black),
            title: Text('account'.tr().toString()),
          ),
          BottomNavigationBarItem(
            icon: SystemControls().GetSVGImagesAsset(
                'assets/Icons/more.svg', 23,
                null),
            activeIcon: SystemControls().GetSVGImagesAsset(
                'assets/Icons/more.svg', 25,
                Colors.black),
            title: Text('more'.tr().toString()),
          ),
        ],
        currentIndex: navigBarindex,
        selectedItemColor: Colors.black,
        unselectedItemColor: Color.fromRGBO(142, 142, 147, 1),
        onTap: _onItemTapped,
      );
    }
  }
  Future<ResponseApi> resSearch;

  List<dynamic> _searchMeals =  <dynamic>[];
  int SelectedCatHome=-1;

  bool loadFinalone = true;
  Widget navigatorBody(int pageIndex){
    // The equivalent of the "smallestWidth" qualifier on Android.
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    print("shortestSide ******"+shortestSide.toString());
    final bool useMobileLayout = shortestSide < 550;

    if(pageIndex == 10){
      print("Search page");
      return FutureBuilder<ResponseApi>(
          future: resSearch,
          builder: (context, snap){
            if(snap.hasData){
              if (snap.connectionState == ConnectionState.waiting && _searchMeals.isEmpty) {
                return Center(
                  child: SystemControls().circularProgress(),
                );
              }
              else if(snap.hasData){
                if(snap.connectionState == ConnectionState.done) {
                  print("Add data Add data *************************** ");
                  print("current "+currentPageindex.toString());
                  print("max     "+maxPageIndex.toString());
                  print("stopRequest     "+stopRequest.toString());

                  if(currentPageindex <= maxPageIndex&& stopRequest==true) {
                    print("Add new items   .... ");
                    _searchMeals += snap.data.data['products'].toSet().toList();
                    stopRequest = false;
                    StartLoad = false;
                  }
                  currentPageindex = snap.data.data['pagination']['current_page'];
                  nextUrl = snap.data.data['pagination']['next_page_url'];
                  maxPageIndex = snap.data.data['pagination']['last_page'];
                }
                print(currentPageindex);
                if(_searchMeals.length>0){
                  return Container(
                    margin: EdgeInsets.only(top: SystemControls.branchesValid?(0):(10)),
                    child: ListView(
                      controller: _searchScrollController,
                      children: <Widget>[
                        Container(
                          alignment: Alignment.center,
                          height: 50,
                          child: Text('searchResult'.tr().toString()
                              +(searchCatName !="" ?searchCatName:searchText)),
                        ),
                        HomeItemsList(_searchMeals, _cartList, Lang,
                            UserToken, AppCurrency,design_Control,hasRegistration,
                            onAddCartSubmit,onSeaFavSubmit),
                        StartLoad? Container(
                          padding: EdgeInsets.all(20),
                          child: Center(
                            child: SystemControls().circularProgress(),
                          ),
                        )
                            :Container(
                          padding: EdgeInsets.all(20),
                          child: (currentPageindex<maxPageIndex)?
                          InkWell(
                            onTap: (){
                              if(_selectedIndex == 10) {//search
                                if(currentPageindex < maxPageIndex){
                                  currentPageindex +=1;
                                  setState(() {
                                    print("load new page ..... \n\n"+currentPageindex.toString());
                                    StartLoad = true;
                                    stopRequest = true;
                                    if(showSearchCat == false) {
                                      resSearch = SearchWithtitleApi(
                                          Lang, searchText, UserToken, currentPageindex.toString());
                                    }
                                    else {
                                      resSearch = CategoryListApi(Lang, searchCatId, UserToken,currentPageindex.toString());
                                    }
                                  });
                                }
                                else if(currentPageindex == maxPageIndex && StartLoad==true){
                                  setState(() {
                                    StartLoad = false;
                                  });
                                }
                              }
                            },
                            child: Center(
                              child: Text('loadMore'.tr().toString(),
                                style: TextStyle(
                                  fontSize: SystemControls.font3,
                                  fontFamily: Lang,
                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                  fontWeight: FontWeight.bold,
                                  height: 0.9,
                                ),
                              ),
                            ),
                          )
                              :Container(),
                        ),
                      ],
                    ),
                  );
                }
                else{
                  return Container(
                    child: Center(
                      child: Text('searchResultNull'.tr().toString(),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.bold,
                          fontSize: SystemControls.font2,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                        ),
                      ),
                    ),
                  );
                }
              }
            }
            return Center(
              child: SystemControls().circularProgress(),
            );
          }
      );
    }
    switch(pageIndex){
      case 0: //Home
      //TODO clean it like cat layout
        return Stack(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                design_Control==1&&useMobileLayout==false&&false?(
                    Container(
                      width: 130,
                      child: CategoryListVertical(
                          _categoriesz, -1, _cartList, Lang, UserToken, AppCurrency,
                          _offers, CatStorageKey, design_Control,
                          hasRegistration,AppInitialInfo),
                    )
                ):(
                    Container()
                ),
                Flexible(
                  flex: 1,
                  child: CustomScrollView(
                    controller: _homeScrollController,
                    slivers: <Widget>[
                      useMobileLayout==false||design_Control!=1||true?(
                          SliverList(delegate: SliverChildListDelegate([]))
                      ):(
                          SliverAppBar(
                              pinned: true,
                              expandedHeight: 120,
                              floating: true,
                              backgroundColor: Color.fromRGBO(251, 224, 2, 0),
                              leading: new Container(),
                              flexibleSpace: Container(
                                margin: EdgeInsets.only(bottom: 5),
                                child: CategoryList(
                                    _categoriesz, -1, _cartList, Lang, UserToken, AppCurrency,
                                    _offers, CatStorageKey, design_Control,
                                    hasRegistration,AppInitialInfo),
                              )
                          )
                      ),
                      SliverList(
                        delegate: SliverChildListDelegate([
                          Container(
                            width: MediaQuery.of(context).size.width,
                            child: SliderDefaultHomeAds(
                              _offers, Lang, _cartList, AppCurrency,design_Control,
                              UserToken,hasRegistration,),
                          ),

                          design_Control>0?Container(
                            margin: EdgeInsets.all(5),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Icon(
                                  Icons.apps,
                                  color: globals.accountColorsData['MainColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['MainColor']))
                                      : SystemControls.MainColorz,
                                  size: 25,),
                                Padding(
                                  padding: EdgeInsets.only(right: 5,left: 5),
                                  child: Text("homeCatesListTitle".tr().toString(),
                                    style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.bold,
                                        fontSize: SystemControls.font2,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
                                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                              :Container(),
                          design_Control>0?(
                              Container(
                                margin: EdgeInsets.all(5),
                                height: 150,
                                child: ListView(
                                  padding: EdgeInsets.zero,
                                  shrinkWrap : true,
                                  physics: ScrollPhysics(),
                                  scrollDirection: Axis.horizontal,
                                  children: <Widget>[
                                    design_Control==1?(
                                        InkWell(
                                          child: OfferCardItem(context, -1,Lang),
                                          onTap: (){
                                            Navigator.pushReplacement(context,
                                                _createRoute(-2));
                                          },
                                        )
                                    ):(
                                        Container()
                                    ),
                                    ListView.builder(
                                        padding: EdgeInsets.zero,
                                        shrinkWrap : true,
                                        physics: ScrollPhysics(),
                                        scrollDirection: Axis.horizontal,
                                        itemCount: _categoriesz.length,
                                        itemBuilder: (context, index) {
                                          if (_categoriesz[index]['categories_parent_id']!=null) {
                                            return Container();
                                          }
                                          else {
                                            return InkWell(
                                              child: CategoryCardItem(
                                                  _categoriesz[index], -1, Lang),
                                              onTap: () {
                                                if(design_Control==1) {
                                                  Navigator.pushReplacement(context,
                                                      _createRoute(
                                                          _categoriesz[index]['categories_id'])
                                                  );
                                                }
                                                else {
                                                  setState(() {
                                                    print("cat id from card " +
                                                        _categoriesz[index]['categories_id']
                                                            .toString());
                                                    SelectedCatHome =
                                                    _categoriesz[index]['categories_id'];
                                                  });
                                                  _onItemTapped(1);
                                                }
                                              },
                                            );
                                          }
                                        }
                                    ),
                                  ],
                                ),
                                /*child: CategoryList(
                                _categoriesz, -1, _cartList, Lang, UserToken, AppCurrency,
                                _offers, CatStorageKey, design_Control,
                                hasRegistration,AppInitialInfo),*/
                              )
                          ):(Container()),

                          Container(
                            margin: EdgeInsets.all(5),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Icon(
                                  Icons.star,
                                  color: globals.accountColorsData['MainColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['MainColor']))
                                      : SystemControls.MainColorz,
                                  size: 25,),
                                Padding(
                                  padding: EdgeInsets.only(right: 5,left: 5),
                                  child: Text("homeProductsTitle".tr().toString(),
                                    style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.bold,
                                        fontSize: SystemControls.font2,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
                                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            child: FutureBuilder<ResponseApi>(
                              future: respHomItems,
                              builder: (context, snap){
                                if (snap.connectionState == ConnectionState.waiting && _meals.isEmpty) {
                                  return Center(
                                    child: SystemControls().circularProgress(),
                                  );
                                }
                                else if(snap.hasData){
                                  // && snap.connectionState == ConnectionState.done
                                  homeReqmaxPageIndex = snap.data.data['pagination']['last_page'];
                                  if(snap.connectionState == ConnectionState.done
                                      && snap.data.data['pagination']['current_page'] != homeReqPageindex) {
                                    homeReqPageindex = snap.data.data['pagination']['current_page'];
                                    print("pagination after deone "+homeReqPageindex.toString());

                                    homestopRequest = false;
                                    homeStartLoad = false;
                                    _meals += snap.data.data['products'];
                                    print("pagination " + _meals.length.toString());


                                  }
                                  return HomeItemsList(_meals, _cartList, Lang,
                                      UserToken, AppCurrency,design_Control,hasRegistration,
                                      onAddCartSubmit,onSeaFavSubmit);
                                  /*else {
                                    return HomeItemsList(
                                        _meals,
                                        _cartList,
                                        Lang,
                                        UserToken,
                                        AppCurrency,
                                        design_Control,
                                        hasRegistration,
                                        onAddCartSubmit,
                                        onSeaFavSubmit);
                                  }*/
                                }
/*
                                else if(_meals.isNotEmpty){
                                  return HomeItemsList(_meals, _cartList, Lang,
                                      UserToken, AppCurrency,design_Control,hasRegistration,
                                      onAddCartSubmit,onSeaFavSubmit);
                                }*/

                                return Center(
                                  child: SystemControls().circularProgress(),
                                );
                              },
                            ),
                            /*child: HomeItemsList(_meals, _cartList, Lang,
                                UserToken, AppCurrency,design_Control,hasRegistration,
                                onAddCartSubmit,onSeaFavSubmit),*/
                          ),
                          homeStartLoad? Container(
                            padding: EdgeInsets.all(20),
                            child: Center(
                              child: SystemControls().circularProgress(),
                            ),
                          )
                              :Container(
                            padding: EdgeInsets.all(20),
                            child: (homeReqPageindex<homeReqmaxPageIndex)?
                            InkWell(
                              onTap: (){
                                if(_selectedIndex == 0) {
                                  //print(_homeScrollController.position.pixels);
                                  if(homeReqPageindex < homeReqmaxPageIndex&& homestopRequest == false){
                                    //homeReqPageindex;
                                    setState(() {
                                      print("load new page ..... \n\n"+homeReqPageindex.toString());
                                      homeStartLoad = true;
                                      homestopRequest = true;
                                      respHomItems = homeItemsGet(Lang, UserToken, (homeReqPageindex+1).toString());
                                    });
                                  }
                                  else if(homeReqPageindex == homeReqmaxPageIndex && homeStartLoad==true){
                                    setState(() {
                                      homeStartLoad = false;
                                    });
                                  }
                                }
                              },
                              child: Center(
                                child: Text('loadMore'.tr().toString(),
                                  style: TextStyle(
                                    fontSize: SystemControls.font3,
                                    fontFamily: Lang,
                                    color: globals.accountColorsData['TextHeaderColor']!=null
                                        ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                    fontWeight: FontWeight.bold,
                                    height: 0.9,
                                  ),
                                ),
                              ),
                            )
                                :Container(),
                          ),
                        ]),
                      ),
                    ],
                  ),
                ),
              ],
            )
          ],
        );
        break;
      case 1: //Menu
        print("select 1111 ########@@@@@@@@@@@@ "+SelectedCatHome.toString());
        if(SelectedCatHome == -1){
          SelectedCatHome = _categoriesz[0]['categories_id'];
        }
        return CategoryMealsListNew(
            _cartList,_categoriesz,Lang,0,UserToken, AppCurrency,
            SelectedCatHome,design_Control,
            hasRegistration,AppInitialInfo);
        break;
      case 2: //Offers
        return offerListPage(Lang,UserToken,-2,
            _cartList,_categoriesz,
            AppCurrency,design_Control,
            hasRegistration,AppInitialInfo);
        break;
      case 3: //Account
        return MyAccount(context, AppInitialInfo, Lang, hasRegistration, isLogging,
            UserToken,AppCurrency,_cartList,design_Control);
        break;
      case 4: //More
        return MoreOptions(context, AppInitialInfo, Lang, hasRegistration, isLogging,
            UserToken,AppCurrency,_cartList,design_Control);
        break;
    }
  }

  void _onItemTapped(int index) {
    setState(() {
      if(index < 3) {
        resp = fetchList(Lang, UserToken);
        if(index==0){
          SelectedCatHome = _categoriesz[0]['categories_id'];

          _meals.clear();
          homeReqPageindex = 0;
          respHomItems = homeItemsGet(Lang, UserToken, homeReqPageindex.toString());
        }
      }
      isInSearchMode = false;
      showSearchCat = true;
      _controller.reverse();

      _selectedIndex = index;
      navigBarindex = index;
    });
  }
  int _selectedIndex = 0;
  int navigBarindex = 0;


//TODO REad it to change show
  Route _createRoute(int index) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) =>
          CategoryMealsListNew(_cartList, _categoriesz, Lang,0, UserToken,
              AppCurrency, index,design_Control,hasRegistration,AppInitialInfo),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset.zero;
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }
/*
  onSelectCategoryx(int SelectrdId){
    Navigator.pushReplacement(context,
        _createRoute(SelectrdId)
    );
  }
  Route _createRoute(int index) {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) =>
          CategoryMealsListNew(_cartList, _categoriesz, Lang, UserToken,
              AppCurrency, index, _offers,design_Control,hasRegistration),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset.zero;
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }*/

}