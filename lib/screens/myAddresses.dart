import 'package:flutter/material.dart';
import '../Controls.dart';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import '../screens/home.dart';
import '../dataControl/dataModule.dart';
import '../dataControl/cartModule.dart';
import '../screens/orderDetails.dart';
import '../screens/addNewAddress.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';
import '../screens/editAddress.dart';

import 'package:android_intent/android_intent.dart';
import 'package:geolocator/geolocator.dart';

import '../globals.dart' as globals;

class MyAddresses extends StatefulWidget{
  String Lang;
  String Token;
  String AppCurrency;
  int design_Control;

  MyAddresses(this.Lang, this.Token, this.AppCurrency,this.design_Control);

  _MyAddresses createState() =>
      _MyAddresses(this.Lang, this.Token, this.AppCurrency,this.design_Control);

}
class _MyAddresses extends State<MyAddresses>{
  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);

  }
  @override
  void dispose() {
    super.dispose();
    CheckGpsAlert(context);

    _connectivitySubscription.cancel();
  }
  _checkGps() async {
    print("_checkGps ... open ");
    if (!(await Geolocator().isLocationServiceEnabled())) {
      if (Theme.of(context).platform == TargetPlatform.android) {
        return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(SystemControls.RadiusCircValue)
              ),
              title: Text("gpsCheckTitle".tr().toString()+
                  "\n"+ "gpsOpenMes".tr().toString(),
                style: TextStyle(
                    fontFamily: Lang,
                    fontWeight: FontWeight.bold,
                    fontSize: SystemControls.font3,
                    color: globals.accountColorsData['TextHeaderColor']!=null
                        ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                ),
                textAlign: TextAlign.center,),
              content: ClipRRect(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
                  bottomRight: Radius.circular(SystemControls.RadiusCircValue),
                ),
                child: Container(
                  height: 40,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Container(
                          decoration: BoxDecoration(
                            color: globals.accountColorsData['MainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                            border: Border(
                              top: BorderSide(color: globals.accountColorsData['TextHeaderColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz),
                            ),
                          ),
                          child: FlatButton(
                            child: Text("ok".tr().toString(),
                              style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.normal,
                                  fontSize: SystemControls.font3,
                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                              ),
                            ),
                            onPressed: () {
                              final AndroidIntent intent = AndroidIntent(
                                  action: 'android.settings.LOCATION_SOURCE_SETTINGS');
                              intent.launch();
                              Navigator.of(context, rootNavigator: true).pop();
                            },
                          ),
                        ),
                      ),
                      Container(
                        height: 40,
                        width: 1,
                        color: Colors.black,
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          decoration: BoxDecoration(
                            color: globals.accountColorsData['MainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                            border: Border(
                              top: BorderSide(color: globals.accountColorsData['TextHeaderColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz),
                            ),
                            //borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: FlatButton(
                            child: Text("cancel".tr().toString(),
                              style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.normal,
                                  fontSize: SystemControls.font3,
                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                              ),
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                      ),


                    ],
                  ),
                ),
              ),
            );
          },
        );
      }
    }
  }

  Future<ResponseApi> respAdds;
  String Lang;
  String Token;
  String AppCurrency;
  int design_Control;
  _MyAddresses(this.Lang, this.Token, this.AppCurrency,
      this.design_Control){
    respAdds = getUserAddressesApi(Lang,Token);
  }

  String CheckText(String get){
    if(get != null)
      return get;
    else
      return " ";
  }

  List<dynamic> _addreses = <dynamic>[];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(//TODO
        title: Text("myAddresses".tr().toString()),
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
        actions: <Widget>[
          /*SystemControls.designControl==1?
              AppBarHomeIConButton(context,Lang,'')
              :Container(),*/
        ],
      ),
      body: Stack(
        children: <Widget>[
          LayoutBG(design_Control),
          FutureBuilder<ResponseApi>(
            future: respAdds,
            builder: (context, snap){
              if(snap.hasData){
                if(snap.data.status==200) {
                  dynamic dataa = snap.data.data;
                  _addreses = dataa['addresses'];

                  return Container(
                    child: Align(
                      alignment: Alignment.center,
                      child: Container(
                        margin: EdgeInsets.all(5),
                        width: MediaQuery.of(context).size.shortestSide,
                        child: ListView(
                          children: <Widget>[
                            _addreses.length==0?(
                                Container(
                                  padding: EdgeInsets.all(25),
                                  child: Center(
                                    child: Text('emptyListMyAddresses'.tr().toString()),
                                  ),
                                )
                            ):(
                                ListView.builder(
                                shrinkWrap: true,
                                physics: ScrollPhysics(),
                                scrollDirection: Axis.vertical,
                                itemCount: _addreses.length,
                                itemBuilder: (context, index) {
                                  return InkWell(
                                    child: Container(
                                      height: 100,
                                      margin: EdgeInsets.all(5),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(SystemControls
                                                .RadiusCircValue)),
                                        child: Container(
                                          color: Colors.white,
                                          padding: EdgeInsets.all(10),
                                          child: Column(
                                            children: <Widget>[
                                              Padding(
                                                  padding: EdgeInsets.all(4)),
                                              Row(
                                                mainAxisSize: MainAxisSize.max,
                                                mainAxisAlignment: MainAxisAlignment
                                                    .center,
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 7,
                                                    child: Column(
                                                      crossAxisAlignment: CrossAxisAlignment
                                                          .start,
                                                      mainAxisAlignment: MainAxisAlignment
                                                          .center,
                                                      children: <Widget>[
                                                        Text(
                                                          _addreses[index]['customer_addresses_name'],
                                                          //"Address title",
                                                          style: TextStyle(
                                                            fontFamily: Lang,
                                                            fontSize: SystemControls
                                                                .font2,
                                                            color: globals.accountColorsData['TextHeaderColor']!=null
                                                                ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                                            fontWeight: FontWeight
                                                                .normal,
                                                            height: 1,
                                                          ),
                                                          maxLines: 1,
                                                          textAlign: TextAlign
                                                              .start,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 3,
                                                    child: Row(
                                                      crossAxisAlignment: CrossAxisAlignment
                                                          .end,
                                                      mainAxisAlignment: MainAxisAlignment
                                                          .end,
                                                      children: <Widget>[
                                                        /*Text(AppLocalizations.of(context).translate("change"),
                                                      style: TextStyle(
                                                        fontFamily: Lang,
                                                        fontSize: SystemControls.font3,
                                                        color: SystemControls.TextdetailsColor,
                                                        fontWeight: FontWeight.normal,
                                                        height: 0.8,
                                                      ),
                                                      maxLines: 1,
                                                      textAlign: TextAlign.end,
                                                    ),*/
                                                        InkWell(
                                                          onTap: () async {

                                                            deleteDialogAlert(context,"deleteAddress",()async{
                                                              var res = await removeAddressApi(
                                                                  Lang, Token,
                                                                  _addreses[index]['customer_addresses_id']);
                                                              if (res != null) {
                                                                if (res.status ==
                                                                    200) {
                                                                  setState(() {
                                                                    _addreses
                                                                        .removeAt(
                                                                        index);
                                                                  });
                                                                }
                                                              }
                                                            },Lang);
                                                          },
                                                          child: Icon(Icons
                                                              .delete_outline,
                                                            color: Colors
                                                                .black,),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Padding(
                                                  padding: EdgeInsets.all(5)),
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: <Widget>[
                                                  Icon(
                                                    Icons.location_on,
                                                    color: globals.accountColorsData['BorderColor']!=null
                                                        ? Color(int.parse(globals.accountColorsData['BorderColor'])) : SystemControls.BorderColorz,
                                                    size: 30,
                                                  ),
                                                  Padding(
                                                      padding: EdgeInsets.all(
                                                          2)),
                                                  Expanded(
                                                    child: Text(
                                                      _addreses[index]['customer_addresses_address'],
                                                      style: TextStyle(
                                                        fontFamily: Lang,
                                                        fontSize: SystemControls
                                                            .font3,
                                                        color: globals.accountColorsData['TextdetailsColor']!=null
                                                            ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                        fontWeight: FontWeight
                                                            .normal,
                                                        height: 0.9,
                                                      ),
                                                      maxLines: 1,
                                                      textAlign: TextAlign.start,
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    onTap: () {
                                      Navigator.pushReplacement(context,
                                          MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                editAddress(
                                                    Lang, Token, AppCurrency,
                                                    design_Control,
                                                    _addreses[index]),
                                          ));
                                    },
                                  );
                                }
                            )
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 20, right: 10,
                                  left: 10, bottom: 20),
                              decoration: BoxDecoration(
                                color: globals.accountColorsData['MainColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                                borderRadius: BorderRadius.all(
                                    Radius.circular(10)),
                              ),
                              child: FlatButton(
                                onPressed: () {
                                  Navigator.pushReplacement(context,
                                      MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            addNewAddress(
                                                Lang, Token, AppCurrency,
                                                design_Control),
                                      ));
                                },
                                child: Center(
                                  child: Text(

                                        "addNewAddress".tr().toString(),
                                    style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.normal,
                                        fontSize: SystemControls.font2,
                                        color: globals.accountColorsData['TextOnMainColor']!=null
                                            ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                }
                else if(snap.data.status==401){
                  SystemControls().LogoutSetUserData(context, Lang);
                  Timer(Duration(seconds: 1), ()=>
                      ErrorDialogAlertBackHome(context, snap.data.message)
                  );
                  return Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(snap.data.message,
                          style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font2,
                            height: 1.2,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  );
                }
                return Container();
              }
              else if(snap.hasError){
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("respError".tr().toString(),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font2,
                          height: 1.2,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 150,
                        decoration: BoxDecoration(
                          color: globals.accountColorsData['MainColor']!=null
                              ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                          borderRadius: BorderRadius.all(Radius.circular(
                            SystemControls.RadiusCircValue
                          )),
                        ),
                        child: FlatButton(
                          onPressed: () {
                            print("Reload");
                            setState(() {
                              respAdds = getUserAddressesApi(Lang,Token);
                            });
                          },
                          child: Center(
                            child: Text("reload".tr().toString(),
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.bold,
                                  fontSize: SystemControls.font2,
                                )
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              }

              return Center(
                child: SystemControls().circularProgress(),
              );
            },
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: ConnectionStatusBar(),
          ),
        ],
      ),

    );
  }

  //////////////
  ConnectionStatusBar(){
    final Color color = Colors.redAccent;
    final double width = double.maxFinite;
    final double height = 30;

    if(_connectionStatus == 'connected'){
      return Container();
    }
    else{
      return Container(
        child: SafeArea(
          bottom: false,
          child: Container(
            color: color,
            width: width,
            height: height,
            child: Container(
              margin: EdgeInsets.only(right: 5,left: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Please check your internet connection',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'en',
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font4,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      InkWell(
                        child: Text(
                          'Try Again..',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'en',
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font4,
                          ),
                        ),
                        onTap: (){
                          setState(() async{
                            initConnectivity();
                          });
                        },
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      );
    }
  }
  ///Internet Check area
  String _connectionStatus = 'connected';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }
  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = "connected";
              respAdds = getUserAddressesApi(Lang,Token);
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = "failed";
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState((){
          _connectionStatus = "connected";
          respAdds = getUserAddressesApi(Lang,Token);
        });
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "failed");
        break;
      default:
        setState(() => _connectionStatus = 'failed');
        break;
    }
  }
}