import '../../provider/productsInCartProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geocoder/geocoder.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';
import '../Controls.dart';

import '../locale/app_localization.dart';
//imppp4
import '../dataControl/cartModule.dart';
import '../screens/home.dart';
import '../screens/cart3CheckOut.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';
import '../widgets/UI_Widgets_InputField.dart';

import '../globals.dart' as globals;

class CartOrderLayout extends StatefulWidget{

  List<dynamic> _mealsCart = <dynamic>[];
  String Lang;
  String AppCurrency;
  String UserToken;
  int design_Control;
  List<dynamic> _addreses = <dynamic>[];
  String itemsNote;
  LatLng _center;
  int selectedAreaId;
  CartOrderLayout(this.Lang,this.UserToken,this.AppCurrency,this._mealsCart,
      this.design_Control,this._addreses,double latit, double longit,
      this.itemsNote,this.selectedAreaId){
    _center = LatLng(latit, longit);
  }

  _cartOrder createState() =>
      _cartOrder(this.Lang, this.UserToken,this.AppCurrency, 
          this._mealsCart,this.design_Control,this._addreses,
          this._center,this.itemsNote);
}

class _cartOrder extends State<CartOrderLayout>{
  final Future<CartList> localCart = read_from_file();
  List<dynamic> _mealsCart = <dynamic>[];
  //Future<responceFe> respAdds;
  List<dynamic> _addreses = <dynamic>[];
  String Lang;
  String AppCurrency;
  String UserToken;
  int design_Control;
  String itemsNote;
  TextEditingController addressTextField1 = TextEditingController();

  @override
  void initState() {
    super.initState();

    CreateItemsList();
  }
  CreateItemsList(){
    for(int i=0; i<_mealsCart.length; i++){
      //Calcu total price and count items
      //total_items = total_items + _mealsCart[i]['count'];
      total_price = total_price +
          (double.parse(_mealsCart[i]['meals_price']) * double.parse("${_mealsCart[i]['count']}"));
    }
  }

  _cartOrder(this.Lang, this.UserToken,this.AppCurrency,
      this._mealsCart,this.design_Control,this._addreses,
      this._center,this.itemsNote){
    for(int i=0;i<SystemControls.addresPartsFormat(Lang).length;i++){
      addressList.add(addressTextField1);
    }

    if(_addreses != null && _addreses.length>0){
      String gps = _addreses[0]['customer_addresses_gps'];
      var arr = gps.split(',');
      _getaddresLocation(
          double.parse(arr[0]),
          double.parse(arr[1]));
      //camZomeValu = 15;
      _center = LatLng(double.parse(arr[0]), double.parse(arr[1]));

      //Show Address
      var addressArr = _addreses[0]['customer_addresses_address'].split(',');
      for(int i=0; i<addressArr.length;i++){
        if(i<SystemControls.addresPartsFormat(Lang).length){
          addressList[i].text = addressArr[i];
        }
        else{
          break;
        }
      }
    }
    else {
      newAddresFlag = true;
      if(_center.latitude==0||_center.longitude==0){
        String gps = globals.accounts_country_lat_long;
        var arr = gps.split(',');
        _getaddresLocation(
            double.parse(arr[0]),
            double.parse(arr[1]));
        print("accounts_country_lat_long\n\n\n"+arr[0]+"\n\n\n"+arr[1]);
        //camZomeValu = 7;
        _center = LatLng(double.parse(arr[0]), double.parse(arr[1]));
      }
      _getSelectedAddressText(_center.latitude,_center.longitude);
    }
    _markers.clear();
    final marker = Marker(
      markerId: MarkerId("selected"),
      position: _center,//LatLng(latitude,longitude),
      infoWindow: InfoWindow(title: 'Selected Location'),
    );
    _markers["Selected Location"] = marker;
  }

  double camZomeValu=15;
  int total_items = 0;
  double total_price = 0;

  GoogleMapController mapController;
  LatLng _center;
  final Map<String, Marker> _markers = {};

  _getaddresLocation(double latitude, double longitude) async{
    setState(() {
      _center = LatLng(latitude,longitude);
      //Add selected location to markers List
      _markers.clear();
      final marker = Marker(
        markerId: MarkerId("curr_loc"),
        position: LatLng(latitude,longitude),
        infoWindow: InfoWindow(title: 'Selected Location'),
      );
      _markers["Selected Location"] = marker;
      //Move map camera to selected location or target
      mapController.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(
          bearing: 0,
          target: _center,
          zoom: 15.0,
        ),
      ));
    });
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }
  //OnTab map Function
  _mapTappedSetLoca(LatLng sellectedLocation) {
    print(sellectedLocation);
    if(newAddresFlag) {
      _center = sellectedLocation;
      _getSelectedAddressText(_center.latitude,_center.longitude);
      setState(() {
        //On select location add marker
        _markers.clear();
        final marker = Marker(
          markerId: MarkerId("curr_loc"),
          position: sellectedLocation,
          infoWindow: InfoWindow(title: 'Your Location'),
        );
        _markers["Current Location"] = marker;
      });
    }

  }
  int _selectedIndex = 0;
  bool newAddresFlag = false;

  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    var _mapWidget = Expanded(
      flex: 1,
      child: Container(
        margin: EdgeInsets.only(top: 10,bottom: 10),
        //height: mediaQuery.size.width*0.7,
        child: Center(
          child: GoogleMap(
            onTap: _mapTappedSetLoca,
            myLocationEnabled: true,
            onMapCreated: _onMapCreated,
            initialCameraPosition: CameraPosition(
              target: _center,
              zoom: 15,
            ),
            markers: _markers.values.toSet(),
            scrollGesturesEnabled: newAddresFlag,
          ),
        ),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text("cart".tr().toString()),
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          LayoutBG(design_Control),
          Align(
            alignment: Alignment.center,
            child: Container(
              //width: mediaQuery.size.shortestSide,
              margin: EdgeInsets.only(top: 10,left: 10,right: 10,bottom: 120),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Text("selectAddressTitleCart".tr().toString(),
                            style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.bold,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData['TextHeaderColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                            )
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    height: 60,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        _addreses.length>0?(
                            Expanded(
                              flex: 1,
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                ),
                                padding: EdgeInsets.only(right: 5,left: 5),
                                child: CupertinoButton(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Text(
                                          _selectedIndex<_addreses.length ? (
                                              _addreses[_selectedIndex]['customer_addresses_name']
                                          ): (
                                              "addNewAddress".tr().toString()
                                          ),
                                          style: TextStyle(
                                            fontFamily: Lang,
                                            fontWeight: FontWeight.normal,
                                            fontSize: SystemControls.font3,
                                            color: globals.accountColorsData['TextHeaderColor']!=null
                                                ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                          ),
                                        ),
                                        //Text(appLocalizations.translate("selectOtherAddress")),
                                        Icon(Icons.keyboard_arrow_down,
                                          color: Colors.black,
                                          size: 15,),
                                      ],
                                    ),
                                    onPressed: () {
                                      showModalBottomSheet(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return Container(
                                              height: 300.0,
                                              decoration: BoxDecoration(
                                                color: Theme.of(context).canvasColor,
                                                borderRadius: BorderRadius.only(
                                                  topLeft: const Radius.circular(12),
                                                  topRight: const Radius.circular(12),
                                                ),
                                              ),
                                              child: Stack(
                                                children: <Widget>[
                                                  Container(
                                                    alignment: Alignment.topCenter,
                                                    padding: EdgeInsets.all(15),
                                                    child: Text('selectAddress'.tr().toString(),
                                                      style: TextStyle(
                                                        fontFamily: Lang,
                                                        fontWeight: FontWeight.bold,
                                                        fontSize: SystemControls.font3,
                                                        color: globals.accountColorsData['TextHeaderColor']!=null
                                                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                                        height: 1.2,
                                                      ),
                                                      textAlign: TextAlign.center,
                                                    ),
                                                  ),

                                                  DraggableScrollableSheet(
                                                    initialChildSize: 1,
                                                    minChildSize: 0.8,
                                                    builder: (context, ScrollController scrollController) {
                                                      return Container(
                                                        color: Colors.black12,
                                                        margin: EdgeInsets.only(top: 50),
                                                        child: ListView.builder(
                                                          controller: scrollController,
                                                          itemCount: _addreses.length,
                                                          itemBuilder: (BuildContext context, int index) {
                                                            return ListTile(
                                                              title: index !=_addreses.length?
                                                              Text(_addreses[index]['customer_addresses_name'])
                                                                  :Text("addNewAddress".tr().toString()),
                                                              onTap: (){
                                                                setState(() {
                                                                  newAddresFlag = false;
                                                                  _selectedIndex = index;
                                                                  if(index == _addreses.length){
                                                                    //selectedAddressFull = "";
                                                                    _markers.clear();
                                                                  }
                                                                  else {
                                                                    String gps = _addreses[index]['customer_addresses_gps'];
                                                                    var arr = gps.split(',');
                                                                    _getaddresLocation(
                                                                        double.parse(arr[0]),
                                                                        double.parse(arr[1]));
                                                                    setState(() {
                                                                      var addressArr = _addreses[index]['customer_addresses_address'].split(',');
                                                                      for(int i=0; i<addressArr.length;i++){
                                                                        if(i<SystemControls.addresPartsFormat(Lang).length){
                                                                          addressList[i].text = addressArr[i];
                                                                        }
                                                                        else{
                                                                          break;
                                                                        }
                                                                      }
                                                                    });

                                                                  }
                                                                  Navigator.pop(context);
                                                                });
                                                              },
                                                            );
                                                          },
                                                        ),
                                                      );
                                                    },
                                                  ),
                                                ],
                                              ),
                                            );
                                          });
                                    }),
                              ),
                            )
                        ):(
                            Container()
                        ),
                        SizedBox(width: 8,),
                        InkWell(
                          onTap: (){
                            setState(() {
                              //selectedAddressFull = "";
                              newAddresFlag = true;
                              addressList.clear();
                              addressTextField1.text="";
                              addressList.add(addressTextField1);
                              _markers.clear();
                            });
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              color: globals.accountColorsData['MainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                            ),
                            padding: EdgeInsets.only(right: 5,left: 5),
                            width: 120,
                            height: 50,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("addNewAddress".tr().toString(),
                                    style: TextStyle(
                                      fontFamily: Lang,
                                      fontWeight: FontWeight.normal,
                                      fontSize: SystemControls.font3,
                                      color: globals.accountColorsData['TextOnMainColor']!=null
                                          ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                                      height: 1.2,
                                    )
                                ),
                                SizedBox(width: 8,),
                                Icon(Icons.add_circle_outline,
                                  size: 25,
                                  color: Colors.white,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  ListView.builder(
                      shrinkWrap : true,
                      physics: ScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      itemCount: (SystemControls.addresPartsFormat(Lang).length~/2)+1,
                      itemBuilder: (context, ListIndex){
                        int index = ListIndex*2;
                        if(SystemControls.addresPartsFormat(Lang).length > 3) {
                          return Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  margin: EdgeInsets.only(top: 10),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(10)),
                                  ),
                                  child: TextFormField(
                                    controller: addressList[index],
                                    decoration: InputFieldDecoration(
                                        SystemControls.addresPartsFormat(
                                            Lang)[index], Lang),
                                    keyboardType: TextInputType.text,
                                    style: new TextStyle(
                                      fontFamily: Lang,
                                    ),
                                  ),
                                ),
                                flex: 1,
                              ),
                              Container(width: 5,),
                              Expanded(
                                flex: 1,
                                child: (index + 1) < SystemControls
                                    .addresPartsFormat(Lang)
                                    .length ? (
                                    Container(
                                      margin: EdgeInsets.only(top: 10),
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10)),
                                      ),
                                      child: TextFormField(
                                        controller: addressList[index + 1],
                                        decoration: InputFieldDecoration(
                                            SystemControls.addresPartsFormat(
                                                Lang)[index + 1], Lang),
                                        keyboardType: TextInputType.text,
                                        style: new TextStyle(
                                          fontFamily: Lang,
                                        ),
                                      ),
                                    )
                                ) : (Container()),
                              )
                            ],
                          );
                        }
                        else{
                          return Column(
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(10)),
                                ),
                                child: TextFormField(
                                  enabled: false,

                                  controller: addressList[index],
                                  decoration: InputFieldDecoration(
                                      SystemControls.addresPartsFormat(
                                          Lang)[index], Lang),
                                  keyboardType: TextInputType.text,
                                  style: new TextStyle(
                                    fontFamily: Lang,
                                  ),
                                ),
                              ),
                              Container(padding: EdgeInsets.all(5),),
                              (index + 1) < SystemControls
                                  .addresPartsFormat(Lang)
                                  .length ? (
                                  Container(
                                    margin: EdgeInsets.only(top: 10),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10)),
                                    ),
                                    child: TextFormField(
                                      controller: addressList[index + 1],
                                      decoration: InputFieldDecoration(
                                          SystemControls.addresPartsFormat(
                                              Lang)[index + 1], Lang),
                                      keyboardType: TextInputType.text,
                                      style: new TextStyle(
                                        fontFamily: Lang,
                                      ),
                                    ),
                                  )
                              ) : (Container()),
                            ],
                          );
                        }
                        return Container(
                          margin: EdgeInsets.only(top: 10),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            controller: addressList[index],
                            decoration: InputFieldDecoration(SystemControls.addresPartsFormat(Lang)[index], Lang),
                            keyboardType: TextInputType.text,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                          ),
                        );
                      }
                  ),
                  _mapWidget,
                ],
              ),
            ),
          ),

          Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              Container(
                color: globals.accountColorsData['BGColor']!=null
                    ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
                alignment: Alignment.bottomCenter,
                height: 120,
                width: mediaQuery.size.width,
              ),
              Container(
                width: mediaQuery.size.shortestSide,
                margin: EdgeInsets.only(right: 10,left: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      height: 1,
                      color: globals.accountColorsData['TextdetailsColor']!=null
                          ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                    ),
                    Padding(padding: EdgeInsets.all(6)),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Text("TotalPrice".tr().toString(),
                              style: TextStyle(
                                fontFamily: Lang,
                                fontWeight: FontWeight.normal,
                                fontSize: SystemControls.font3,
                                color: globals.accountColorsData['TextHeaderColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                height: 1.2,
                              )
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Consumer<CartStateProvider>(builder: (context,cart,_){
                            return  Text(
                                ((cart.couponType !="" ? cart.couponType=="value"?(total_price - double.parse(cart.couponValue)): (total_price-((total_price * double.parse(cart.couponValue))/100)) :total_price)<=0?0:cart.couponType !="" ? cart.couponType=="value"?(total_price - double.parse(cart.couponValue))+globals.deliveryCost: (total_price-((total_price * double.parse(cart.couponValue))/100)) +globals.deliveryCost:total_price+globals.deliveryCost).toStringAsFixed(globals.numberDecimalDigits) +
                                    " " + AppCurrency,
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.normal,
                                  fontSize: SystemControls
                                      .font3,
                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                  height: 1.2,
                                )
                            );
                          }),
                        )
                      ],
                    ),
                    Padding(padding: EdgeInsets.all(6)),
                    Container(
                      width: mediaQuery.size.width,
                      height: 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: globals.accountColorsData['MainColor']!=null
                              ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                      ),
                      child: FlatButton(
                        onPressed: ()async{
                          if(UserToken == ""){
                            ErrorDialogAlert(context,
                              "mustLogin".tr().toString());
                          }
                          else{
                            //bool newAddressflag = false;
                            String selectedAddressFull = "";
                            /*if(_selectedIndex == _addreses.length){
                              newAddressflag = true;
                            }
                            else{
                              selectedAddressFull = _addreses[_selectedIndex]['customer_addresses_address'];
                            }*/
                            for(int i=0;i<SystemControls.addresPartsFormat(Lang).length;i++){

                              selectedAddressFull += addressList[i].text +",";
                            }
                          selectedAddressFull.length >1 ?  Navigator.push(context,
                                MaterialPageRoute(
                                    builder: (
                                        BuildContext context) =>

                                        CartCheckOutLayout(
                                            Lang, UserToken,
                                            AppCurrency,
                                            _mealsCart,
                                            design_Control,
                                            _center,
                                            newAddresFlag,
                                            selectedAddressFull,
                                            itemsNote,widget.selectedAreaId)
                                )
                            )     :   ErrorDialogAlert(context,
                            "mustaddAlladdress".tr().toString());
                          }
                        },
                        child: Text("confirmOrder".tr().toString(),
                            style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData['TextOnMainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                              height: 1.2,
                            )
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.all(10)),
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  List<TextEditingController> addressList = <TextEditingController>[];
  //ConvertLocation cordenate toAddress text
  String GetInputFieldValue(Address address, String key){
    if(key == "addressLine"){
      return address.addressLine;
    }
    else if(key == "adminArea"){
      return address.adminArea;
    }
    else if(key == "countryCode"){
      return address.countryCode;
    }
    else if(key == "countryName"){
      return address.countryName;
    }
    else if(key == "featureName"){
      return address.featureName;
    }
    else if(key == "locality"){
      return address.locality;
    }
    else if(key == "postalCode"){
      return address.postalCode;
    }
    else if(key == "subAdminArea"){
      return address.subAdminArea;
    }
    else if(key == "subLocality"){
      return address.subLocality;
    }
    else if(key == "subThoroughfare"){
      return address.subThoroughfare;
    }
    else if(key == "thoroughfare"){
      return address.thoroughfare;
    }
    else{
      return "";
    }

  }
  _getSelectedAddressText(var lat,var long) async{
    final coordinates = new Coordinates(lat, long);
    Geocoder.local.findAddressesFromCoordinates(coordinates).then((var address) {
      setState(() {
        print("Get address from locer");
        for(int i=0;i<SystemControls.addresPartsFormat(Lang).length;i++){
          addressList[i].text = GetInputFieldValue(address.first,SystemControls.addresGeoCodeKey()[i]);
        }
        print("\n\n\n\n done");
      });

    }).catchError((e) {
      print(e);
    });
  }

}