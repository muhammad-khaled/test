import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'package:country_code_picker/country_code_picker.dart';
import '../Controls.dart';

import 'package:easy_localization/easy_localization.dart';

import '../dataControl/userModule.dart';
import '../screens/login.dart';
import '../screens/registerVerifyPhoneNumber.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';
import '../widgets/UI_Widgets_InputField.dart';
import '../globals.dart' as globals;

class registerEnterPhoneNumber extends StatelessWidget{

  String Lang;
  int design_Control;
  registerEnterPhoneNumber(this.Lang,this.design_Control);

  TextEditingController phoneTextField = TextEditingController();

  String countryCode = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: globals.accountColorsData['BGColor']!=null
                  ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
              image: DecorationImage(
                  image: AssetImage("assets/SplashBG.png",),
                  //repeat: ImageRepeat.repeat
                  fit: BoxFit.cover
              ),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Container(
              margin: EdgeInsets.all(20),
              width: MediaQuery.of(context).size.shortestSide,
              child: ListView(
                //mainAxisAlignment: MainAxisAlignment.start,
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[

                  Container(
                    margin: EdgeInsets.only(top: 50,bottom: 10),
                    height: 150,
                    child: Center(
                      child: Image.asset( Lang =="ar"? 'assets/logo.png':"assets/logoEn.png"),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Text("registerNewAccount".tr().toString(),
                      style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.bold,
                          fontSize: SystemControls.font2,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Text("signupPhoneNumberMsg".tr().toString(),
                      style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font2,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),

                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        alignment: Alignment.center,
                        width: 90,
                        height: 50,
                        decoration: BoxDecoration(
                            color: Colors.grey.shade400,
                            borderRadius: Lang =='en'? BorderRadius.only(topLeft: Radius.circular(8),bottomLeft: Radius.circular(8)):BorderRadius.only(topRight: Radius.circular(8),bottomRight: Radius.circular(8))

                        ),
                        margin: EdgeInsets.only(top: 15,right: 0),
                        child: globals.accounts_multiple_countries == "0" ?
                        Text(globals.countryCode,
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.bold,
                              fontSize: SystemControls.font2,
                              color: globals.accountColorsData['TextHeaderColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                          ),
                          textAlign: TextAlign.center,
                        )
                            :CountryCodePicker(
                          onChanged: (code){
                            countryCode = code.dialCode;
                            print(countryCode);
                          },
                          // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                          initialSelection: 'EG',
                          // optional. Shows only country name and flag
                          showCountryOnly: false,
                          // optional. Shows only country name and flag when popup is closed.
                          showOnlyCountryWhenClosed: false,
                          // optional. aligns the flag and the Text left
                          alignLeft: false,
                          //Get the country information relevant to the initial selection
                          onInit: (code) {
                            countryCode = code.dialCode;
                            print("onInit "+countryCode);
                          },
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          margin: EdgeInsets.only(top: 15),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            controller: phoneTextField,
                            decoration: InputFieldDecorationPhone("phone".tr().toString(), Lang),
                            keyboardType: TextInputType.phone,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                          ),
                        ), //phone number
                      )
                    ],
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: FlatButton(
                      onPressed: ()async{
                        bool wrongNumber = false;
                        print('thsssssssssss');
                        print(wrongNumber);
                        if(globals.accounts_multiple_countries == "0"
                            && phoneTextField.text.length!=globals.phone_number_length)
                          {
                            wrongNumber = true;
                            print(globals.phone_number_length);
                            print(phoneTextField.text.length);
                          print(wrongNumber);
                        }
                        else if(globals.accounts_multiple_countries == "1"
                            && phoneTextField.text.length < globals.phone_number_length)
                        {
                          wrongNumber = true;

                        }

                        if(phoneTextField.text.isNotEmpty && !wrongNumber
                            && phoneTextField.text.substring(0,1)!='0'){
                          if(globals.accounts_multiple_countries == "0") {
                            countryCode = globals.countryCode;
                          }
                          //TODO Use SMS verify method
                          var get = await checkPhoneNumber(Lang,phoneTextField.text,countryCode);
                          if(get.status==200&&get.message!=null){
                            ErrorDialogAlert(context,get.message.toString());
                          }
                          else{
                            Navigator.push(context,
                                MaterialPageRoute(builder: (BuildContext context) =>
                                //register(Lang,design_Control)
                                registerVerifyPhoneNumber(
                                    Lang, design_Control,phoneTextField.text,countryCode)
                                ));
                          }

                        }
                        else if(phoneTextField.text.isNotEmpty
                            && phoneTextField.text.substring(0,1)=='0')
                        {
                          ErrorDialogAlert(context,
                             "phoneNotStartWith0".tr().toString());
                        }
                        else if(phoneTextField.text.isNotEmpty && wrongNumber)
                        {
                          ErrorDialogAlert(context,
                             "enterTruePhone".tr().toString());
                        }
                        else{
                          ErrorDialogAlert(context,
                           "AddAllData".tr().toString());
                          print('vvvvv');
                        }
                      },
                      child: Center(
                        child: Text(
                          "phoneNumberverifyButton".tr().toString(),
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font2,
                              color: globals.accountColorsData['TextOnMainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                          ),
                        ),
                      ),
                    ),
                  ), //Send email

                  Container(
                    margin: EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      border: null,
                    ),
                    child: FlatButton(
                      onPressed: (){
                        Navigator.pop(context);
                      },
                      child: Center(
                        child: Text(
                          "back".tr().toString(),
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font2,
                              color: Colors.black
                          ),
                        ),
                      ),
                    ),
                  ), //back
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

}