import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import '../Controls.dart';

import 'package:easy_localization/easy_localization.dart';
import '../dataControl/userModule.dart';
import '../screens/home.dart';
import '../locale/share_preferences_con.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';
import '../widgets/UI_Widgets_InputField.dart';

import '../globals.dart' as globals;

class register extends StatefulWidget{
  String Lang;
  int design_Control;
  String phoneString;
  String countryCode;
  register(this.Lang, this.design_Control,this.phoneString,this.countryCode);

  _register createState() => _register(this.Lang, this.design_Control,
      this.phoneString,this.countryCode);
}
class _register extends State<register> with SharePreferenceData{

  String Lang;
  int design_Control;
  String phoneString;
  String countryCode;
  _register(this.Lang, this.design_Control,this.phoneString,this.countryCode){
    phoneTextField.text = this.countryCode+this.phoneString;
  }

  TextEditingController nameTextField = TextEditingController();
  TextEditingController emailTextField = TextEditingController();
  TextEditingController passwoedTextField = TextEditingController();
  TextEditingController phoneTextField = TextEditingController();


  TextEditingController birthDayField = TextEditingController();
  TextEditingController birthMonthField = TextEditingController();
  TextEditingController birthYearField = TextEditingController();

  final FocusNode _nameFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _phoneFocus = FocusNode();

  int _isRadioSelected = -1;
  void _handleRadioValueChange(int value) {
    setState(() {
      _isRadioSelected = value;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("signup".tr().toString()),
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
      ),
      body: Stack(
        children: <Widget>[
          LayoutBG(design_Control),
          Align(
            alignment: Alignment.center,
            child: Container(
              margin: EdgeInsets.all(20),
              width: MediaQuery.of(context).size.shortestSide,
              child: ListView(
                //mainAxisAlignment: MainAxisAlignment.start,
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 40),
                    child: Text("signup".tr().toString(),//"Full name",
                      style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font2,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                      ),
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      //color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: TextFormField(
                      enabled: false,
                      controller: phoneTextField,
                      focusNode: _phoneFocus,
                      decoration: InputFieldDecoration("phone".tr().toString(), Lang),
                      keyboardType: TextInputType.text,
                      //maxLines: 5,
                      onFieldSubmitted: (_){
                        _phoneFocus.unfocus();
                        //FocusScope.of(context).requestFocus(_passwordFocus);
                      },
                      style: new TextStyle(
                        fontFamily: Lang,
                      ),
                      // textDirection: TextDirection.ltr,
                      textAlign: Lang=='ar'?TextAlign.end:TextAlign.start,
                    ),
                  ), //phone
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: TextFormField(
                      controller: nameTextField,
                      focusNode: _nameFocus,
                      decoration: InputFieldDecoration("name".tr().toString(), Lang),
                      keyboardType: TextInputType.text,
                      //maxLines: 5,
                      onFieldSubmitted: (_){
                        _nameFocus.unfocus();
                        FocusScope.of(context).requestFocus(_passwordFocus);
                      },
                      style: new TextStyle(
                        fontFamily: Lang,
                      ),
                    ),
                  ), //name
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: TextFormField(
                      controller: passwoedTextField,
                      focusNode: _passwordFocus,
                      decoration: InputFieldDecoration("password".tr().toString(), Lang),
                      keyboardType: TextInputType.text,
                      //maxLines: 5,
                      onFieldSubmitted: (_){
                        _passwordFocus.unfocus();
                        FocusScope.of(context).requestFocus(_emailFocus);
                      },
                      style: new TextStyle(
                        fontFamily: Lang,
                      ),
                    ),
                  ), //password
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: TextFormField(
                      controller: emailTextField,
                      focusNode: _emailFocus,
                      decoration: InputFieldDecoration(
                          "email".tr().toString()
                              +"option".tr().toString(), Lang),
                      keyboardType: TextInputType.emailAddress,
                      //maxLines: 5,
                      onFieldSubmitted: (_){
                        _emailFocus.unfocus();
                      },
                      style: new TextStyle(
                        fontFamily: Lang,
                      ),
                    ),
                  ), //email

                  Container(
                    margin: EdgeInsets.only(top: 15),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.only(
                              bottom: 20,
                            ),
                            child: Text("birthday".tr().toString()+"\n"
                                + "option".tr().toString(),
                              style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.normal,
                                  fontSize: SystemControls.font2,
                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                              ),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            right: 10,
                            left: 10,
                          ),
                          width: 40,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            maxLength: 2,
                            maxLines: 1,
                            maxLengthEnforced: true,
                            autocorrect: false,
                            controller: birthDayField,
                            //focusNode: _listFocusNode[3],
                            decoration: InputFieldDecoration("dd", Lang),
                            keyboardType: TextInputType.number,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                          ),
                        ), //DD
                        Container(
                          margin: EdgeInsets.only(
                            bottom: 20,
                          ),
                          child: Text("-",
                            style: TextStyle(
                                fontFamily: Lang,
                                fontWeight: FontWeight.bold,
                                fontSize: SystemControls.font2,
                                color: globals.accountColorsData['TextHeaderColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            right: 10,
                            left: 10,
                          ),
                          width: 45,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            maxLength: 2,
                            maxLines: 1,
                            maxLengthEnforced: true,
                            autocorrect: false,
                            controller: birthMonthField,
                            //focusNode: _listFocusNode[3],
                            decoration: InputFieldDecoration("MM", Lang),
                            keyboardType: TextInputType.number,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                          ),
                        ), //MM
                        Container(
                          margin: EdgeInsets.only(
                            bottom: 20,
                          ),
                          child: Text("-",
                            style: TextStyle(
                                fontFamily: Lang,
                                fontWeight: FontWeight.bold,
                                fontSize: SystemControls.font2,
                                color: globals.accountColorsData['TextHeaderColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(
                            right: 10,
                            left: 10,
                          ),
                          width: 70,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            maxLength: 4,
                            maxLines: 1,
                            maxLengthEnforced: true,
                            autocorrect: false,
                            controller: birthYearField,
                            //focusNode: _listFocusNode[3],
                            decoration: InputFieldDecoration("YYYY", Lang),
                            keyboardType: TextInputType.number,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                          ),
                        ), //YYYY
                      ],
                    ),
                  ), //date of birthday
                  Container(
                    margin: EdgeInsets.only(top: 15),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            child: Text("gender".tr().toString()+"\n"
                                + "option".tr().toString(),
                              style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.normal,
                                  fontSize: SystemControls.font2,
                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                              ),
                            ),
                          ),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                new Radio(
                                  value: 0,
                                  activeColor: globals.accountColorsData['MainColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                                  groupValue: _isRadioSelected,
                                  onChanged: _handleRadioValueChange,
                                ),
                                new Text("male".tr().toString(),
                                  style: new TextStyle(fontSize: 16.0),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                new Radio(
                                  value: 1,
                                  activeColor: globals.accountColorsData['MainColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                                  groupValue: _isRadioSelected,
                                  onChanged: _handleRadioValueChange,
                                ),
                                new Text("female".tr().toString(),
                                  style: new TextStyle(fontSize: 16.0),
                                ),
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ), //Gender

                  Container(
                    margin: EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: FlatButton(
                      onPressed: () async{
                        //Email is option now
                        if(emailTextField.text.isEmpty){
                          emailTextField.text="";
                        }
                        String gender = " ";
                        if(_isRadioSelected == 0){
                          gender = "male";
                        }
                        else if(_isRadioSelected == 1){
                          gender = "female";
                        }
                        String birthDate="";
                        if(birthDayField.text.isNotEmpty && birthMonthField.text.isNotEmpty &&
                            birthYearField.text.isNotEmpty){
                          birthDate = birthYearField.text +"-"+birthMonthField.text
                              + "-" + birthDayField.text;
                        }
                        print("send to ser " + birthDate);
                        print("send to ser " + gender);

                        if(nameTextField.text.isEmpty ||
                            phoneTextField.text.isEmpty || passwoedTextField.text.isEmpty){
                          print("empity");
                          ErrorDialogAlert(context,
                              "AddAllData".tr().toString());
                        }
                        else{
                          var get = await registerApi(Lang, nameTextField.text,
                              emailTextField.text, phoneString, passwoedTextField.text,
                              birthDate,gender,countryCode);
                          if(get == null){
                            ErrorDialogAlert(context,
                               "respError".tr().toString());
                          }
                          else{
                            if(get.status == 200){
                              dynamic user_data = get.data['customer'];
                              dynamic user_token = get.data['token'];
                              if(user_data['customers_status'] == "0"){
                                ErrorDialogAlert(context,get.message);
                              }
                              else  if(user_token != null){
                                storeDataToPreferences(
                                    "api_token", user_token['access_token']);
                                storeDataToPreferences(
                                    "name", user_data['customers_name']);
                                storeDataToPreferences(
                                    "email", user_data['customers_email']);
                                storeDataToPreferences("customers_phone",
                                    user_data['customers_phone']);
                                storeDataToPreferences("customers_country_code",
                                    user_data['customers_country_code']);

                                storeDataToPreferences("customers_birthday",
                                    user_data['customers_birthday']);
                                storeDataToPreferences("customers_gender",
                                    user_data['customers_gender']);
                                setLoggingStatus(true);

                                SuccDialogAlertBackHome(context, Lang,get.message);

                              }
                            }
                            else{
                              String error= "";
                              for(int i=0;i<get.errors.length;i++){
                                error += get.errors[i] +"\n";
                              }
                              ErrorDialogAlert(context,error);
                            }
                          }
                        }
                      },
                      child: Center(
                        child: Text(
                          "signup".tr().toString(),
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font2,
                              color: globals.accountColorsData['TextOnMainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

}