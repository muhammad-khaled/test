
import '../provider/DiscountProvider.dart';
import '../services/GetDiscountApi.dart';
import '../widgets/ToolTipWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'dart:math' as math;
import '../Controls.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:easy_localization/easy_localization.dart';
import '../provider/productsInCartProvider.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'dart:async';

import '../locale/app_localization.dart';
import '../screens/home.dart';
import '../widgets/homeItemsList.dart';
import '../dataControl/dataModule.dart';
import '../widgets/drawerWidge.dart';

import '../screens/cart1Items.dart';
import '../screens/IteamDetails2.dart';
import '../screens/notificationLayoutList.dart';

import '../dataControl/favsModule.dart';
import '../dataControl/cartModule.dart';

import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';
import '../widgets/UI_Widgets_CategoriesList.dart';

import '../widgets/mealSuggestionsList.dart';
import '../dataControl/cartModule.dart';
import 'package:cached_network_image/cached_network_image.dart';

import '../widgets/SearchPainter.dart';
import '../widgets/SearchAppBar.dart';

import '../globals.dart' as globals;
//import 'package:cached_network_image/cached_network_image.dart';


class CategoryMealsListNew extends StatefulWidget{
  int id =0;
  String Lang;
  String Token;
  int currentSelectedCatID;
  List<dynamic> _CartMeals = <dynamic>[];
  List<dynamic> _categories = <dynamic>[];
  String AppCurrency;
  int design_Control;
  bool hasRegistration;
  dynamic appInfo;
  CategoryMealsListNew(this._CartMeals, this._categories, this.Lang,this.id, this.Token,
      this.AppCurrency, this.currentSelectedCatID,
      this.design_Control,this.hasRegistration,this.appInfo);
  _categoriesMealsListNew createState() =>
      _categoriesMealsListNew(
          this._CartMeals,
          this._categories,
          this.Lang,

          this.Token,
          this.AppCurrency,
          this.currentSelectedCatID,
          this.design_Control,
          this.hasRegistration,
          this.appInfo,
          this.id,
      );
}
class _categoriesMealsListNew extends State<CategoryMealsListNew>
    with SingleTickerProviderStateMixin{



  Future<ResponseApi> respHomItems;
  @override
  void initState() {
   Provider.of<DiscountProvider>(context,listen: false).fetchNotification(Lang, Token, context);
    _searchScrollController = ScrollController();
    _searchScrollController.addListener(_searchScrollListener);
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);

    //Search
    _controller = AnimationController(
        vsync: this,
        duration: Duration(milliseconds: Lang=="ar"?0:500));
    _animation = Tween(begin: 0.0, end: 1.0).animate(_controller);
    _controller.addStatusListener(animationStatusListener);

  }
  int homeReqPageindex=0;
  int homeReqmaxPageIndex=1;
  bool homeStartLoad = false;
  bool homestopRequest = false;
  ScrollController _homeScrollController;
  List<dynamic> _meals = <dynamic>[];

  @override
  void dispose() {
    super.dispose();
    _connectivitySubscription.cancel();
  }

  Future<ResponseApi> _categoryMeals;
  Future<ResponseApi> searchCaegories;
  List<dynamic> _categories = <dynamic>[];

  Future<CartList> localCart = read_from_file();

  String Lang;
  String Token;
  int id = 0;
  int selectedCatID;
  List<dynamic> _CartMeals = <dynamic>[];
  String AppCurrency;
  bool hasRegistration;
  dynamic appInfo;

  int design_Control;

  Function(List<dynamic>) onAddTocartSubmit;
  Function(List<dynamic>) onFavSubmit;

  _categoriesMealsListNew(List<dynamic> _CartMeals, this._categories, this.Lang,
      this.Token, this.AppCurrency, this.selectedCatID,
      this.design_Control,this.hasRegistration,this.appInfo,this.id){

    /*if(selectedCatID == -2){
      _categoryMeals = getOffersListApi(Lang);
      print("init function Category list new ........ -2");
    }
    else {
      _categoryMeals = CategoryListApi(Lang, selectedCatID);
      print("init function Category list new ........ CatID");
    }*/

  }

  String CheckText(String get){
    if(get != null)
      return get;
    else
      return " ";
  }

  FindMealAtCart(int mealId, String type){
    for(int i=0; i<_CartMeals.length; i++){
      if(_CartMeals[i]['products_id'] == mealId &&
          _CartMeals[i]['type'] == type){
        return i;
      }
    }
    return -1;
  }

  Color CatBGColor(int param){
    if(param == selectedCatID){
      return globals.accountColorsData['MainColor']!=null
          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz;
    }
    else
      return Colors.white;
  }

  onAddCartSubmit(List<dynamic> _cart){
    setState(() {
      _CartMeals = _cart;
    });
  }

  onSeaFavSubmit(List<dynamic> _meals){
    setState(() {
      _searchMeals = _meals;
      print("Add fav");
    });
  }


  CheckDesignType(BuildContext context){
    if(design_Control == 1){
      return AppBar(
        title: AppBarTitleImg(Lang),
        centerTitle: false,
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
        leading: Builder(
          builder: (context)=> DrawerIcon(context,design_Control),
        ),
        actions: <Widget>[
          AppBarNotificationButton(context, OnClickNotificationButton,
              hasRegistration,Lang,Token),
          AppBarCartButton(context, OnClickCartButton,
              _CartMeals.length.toString(),hasRegistration),
          isInSearchMode?
          IconButton(
              icon: SystemControls().GetSVGImagesAsset(
                  "assets/Icons/search.svg",
                  25,
                  Colors.black),
              onPressed: (){
                print("on click button main bar ");
                cancelSearch();
              }
          ):
          GestureDetector(
            child: IconButton(
              icon: SystemControls().GetSVGImagesAsset(
                  "assets/Icons/search.svg",
                  25,
                  Colors.black),
            ),
            onTapUp: onSearchTapUp,
          ),
        ],
      );
    }
    return null;
  }
  OnClickNotificationButton(){
    if(Token == ""){
      ErrorDialogAlertGoTOLogin(context,
          "mustLogin".tr().toString(),Lang);
    }
    else{

      Navigator.pushAndRemoveUntil(context,
          MaterialPageRoute(builder: (BuildContext context) =>
              NotificationList(Lang, Token, AppCurrency,design_Control)
          ),(Route<dynamic> route) => true
      );
    }
  }
  OnClickCartButton(){
    Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (BuildContext context) =>
            CartLayout(Lang,Token,AppCurrency,design_Control)
        ),(Route<dynamic> route) => true
    );
  }


  Widget SearchCategryRecer(List<dynamic> _subCategories){
    return ListView.builder(
        padding: EdgeInsets.zero,
        shrinkWrap : true,
        physics: ScrollPhysics(),
        scrollDirection: Axis.vertical,
        itemCount: _subCategories.length,
        itemBuilder: (context, index){
          return Column(
            children: <Widget>[
              InkWell(
                onTap: (){
                  onSearchSelectCat(_subCategories[index]['categories_id'],_subCategories[index]['categories_title']);
                },
                child: Container(
                  //margin: EdgeInsets.only(right: 10,left: 10),
                  padding: EdgeInsets.only(right: 15,left: 15),
                  height: 30,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        _subCategories[index]['categories_title'],
                        //list[position],
                        style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font3,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                          height: 1.2,
                        ),
                        textAlign: TextAlign.center,
                        maxLines: 2,
                      ),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 13,
                        color: globals.accountColorsData['NavigBarBorderColor']!=null
                            ? Color(int.parse(globals.accountColorsData['NavigBarBorderColor'])) : SystemControls.NavigBarBorderColorz,)
                    ],
                  ),
                ),
              ),
              _subCategories[index]['subcategories'].length>=1?(
                  Container(
                    padding: EdgeInsetsDirectional.fromSTEB(40, 0, 0, 0),
                    child: SearchCategryRecer(_subCategories[index]['subcategories']),
                  )
              ):(Container()),
            ],
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    var cartState = Provider.of<CartStateProvider>(context,listen: false);

    respHomItems = getDiscountApiresponce(Lang,context);
    localCart = read_from_file();
    _CartMeals = <dynamic>[];
    return FutureBuilder<CartList>(
        future: localCart,
        builder: (context,Cartsnap){
          if(Cartsnap.hasData){
            _CartMeals = Cartsnap.data.Cmeals;
            cartState.setCartList(Cartsnap.data.Cmeals);
          }
          return Scaffold(
            appBar: CheckDesignType(context),
            body: Stack(
              children: <Widget>[
                LayoutBG(design_Control),
                BodyCollectionFun(),
                AnimatedBuilder(
                  animation: _animation,
                  builder: (context, child) {
                    return CustomPaint(
                      painter: MyPainter(
                        containerHeight: 75,
                        center: Offset(0, 0),
                        radius: _animation.value * MediaQuery.of(context).size.width,
                        context: context,
                      ),
                    );
                  },
                ),
                isInSearchMode ? (
                    SearchBar(
                      onCancelSearch: cancelSearch,
                      onSearchQuerySubmit: onSearchSubmit,
                    )
                ) : (
                    Container()
                ),
                isInSearchMode&&showSearchCat ?(
                    Container(
                      margin: EdgeInsets.only(top: 80),
                      //height: 500,
                      color: Colors.white,
                      child: FutureBuilder<ResponseApi>(
                        future: searchCaegories,
                        builder: (context, snap){
                          List<dynamic> _categories = <dynamic>[];
                          if(snap.hasData){
                            _categories = snap.data.data['categories'];
                            return ListView.builder(
                              scrollDirection: Axis.vertical,
                              itemCount: _categories.length,
                              itemBuilder: (context, index){
                                if(_categories==null){
                                  return Container();
                                }
                                return Column(
                                  children: <Widget>[
                                    InkWell(
                                      onTap: (){
                                        onSearchSelectCat(_categories[index]['categories_id'],_categories[index]['categories_title']);
                                      },
                                      child: Container(
                                        //margin: EdgeInsets.only(right: 10,left: 10),
                                        padding: EdgeInsets.only(right: 15,left: 15),
                                        height: 40,
                                        color: Colors.white,
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Text(
                                              _categories[index]['categories_title'],
                                              //list[position],
                                              style: TextStyle(
                                                fontFamily: Lang,
                                                fontWeight: FontWeight.normal,
                                                fontSize: SystemControls.font3,
                                                color: globals.accountColorsData['TextHeaderColor']!=null
                                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                                height: 1.2,
                                              ),
                                              textAlign: TextAlign.center,
                                              maxLines: 2,
                                            ),
                                            Icon(
                                              Icons.arrow_forward_ios,
                                              size: 16,
                                              color: globals.accountColorsData['NavigBarBorderColor']!=null
                                                  ? Color(int.parse(globals.accountColorsData['NavigBarBorderColor'])) : SystemControls.NavigBarBorderColorz,)
                                          ],
                                        ),
                                      ),
                                    ),
                                    _categories[index]['subcategories'].length>=1?(
                                        Container(
                                          padding: EdgeInsetsDirectional.fromSTEB(40, 0, 0, 0),
                                          child: SearchCategryRecer(_categories[index]['subcategories']),
                                        )
                                    ):(Container()),
                                  ],
                                );
                              },
                            );
                          }
                          return Center(
                            child: SystemControls().circularProgress(),
                          );
                        },
                      ),
                    )
                ):(Container()),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: ConnectionStatusBar(),
                ),
              ],
            ),
            drawer: HomeDrawerOut(context, Lang, Token,AppCurrency,
                _CartMeals,design_Control,hasRegistration,appInfo,_categories),
          );
        },
    );
  }

  List<dynamic> _searchMeals = <dynamic>[];

  ScrollController _scrollController;
  _scrollListener() {
    if(selectedCatID != -2) {
      //print(_scrollController.position.pixels);
      //print(_scrollController.position.maxScrollExtent);

      if(_scrollController.position.pixels==_scrollController.position.maxScrollExtent
          && currentPageindex < maxPageIndex&& stopRequest == false){
        setState(() {
          //print("load new page ..... \n\n"+currentPageindex.toString());
          StartLoad = true;
          stopRequest = true;
          _categoryMeals = CategoryListApi(Lang, selectedCatID,Token,(currentPageindex+1).toString());
        });
      }
      else if(currentPageindex == maxPageIndex && StartLoad==true){
        setState(() {
          StartLoad = false;
        });
      }
    }else if(selectedCatID == -2){

      if(_scrollController.position.pixels==_scrollController.position.maxScrollExtent
          && homeReqPageindex < homeReqmaxPageIndex){
        setState(() {
          // print("load new page ..... \n\n"+nextPage);
          StartLoad = true;
          stopRequest = true;
     respHomItems = getDiscountApiresponceNext( context,nextPage,(homeReqPageindex+1).toString());
        });
      }
      else if(homeReqPageindex == homeReqmaxPageIndex && StartLoad==true){
        setState(() {
          StartLoad = false;
        });
      }
    }
  }
    String nextPage;
  int currentPageindex=0;
  int maxPageIndex=1;
  bool StartLoad = false;
  bool stopRequest = true;
  List<dynamic> _itemsList = <dynamic>[];
  List<dynamic> dataList = <dynamic>[];
  // List<dynamic> _itemsList = <dynamic>[];/
  Future<ResponseApi> resSearch;

  bool loadFinalone = true;

  final ImagePicker _picker = ImagePicker();

  void _onImageButtonPressed(ImageSource source) async {
    try {
      final pickedFile = await _picker.getImage(
        source: source,
      );
      if(pickedFile.path != null){
        CartStateProvider cartState = Provider.of<CartStateProvider>(context,listen: false);
        cartState.setImgFilePicker(pickedFile);
        String msg = 'AddPicToCartSuccMsg'.tr().toString();
        SuccDialogAlertStaySameLayout(context,msg,ValueKey("categoryList1"));
      }

    } catch (e) {
      print("Image picker error : ");
      print(e);
    }
  }
  Future<void> showBottomSelectImgWidget({BuildContext context}){
    return showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            color: Color(0xFF737373),
            height: 180,
            child: Container(
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Text("imgPickerHeader".tr().toString(),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.bold,
                          fontSize: SystemControls.font3,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                          height: 1.2,
                        )
                    ),
                  ),
                  ListTile(
                      leading: Icon(Icons.photo_library),
                      title: Text("imgPickerGallery".tr().toString()),
                      onTap: (){
                        print('Select From gallery');
                        _onImageButtonPressed(ImageSource.gallery);
                        Navigator.pop(context);
                      }
                  ),
                  ListTile(
                      leading: Icon(Icons.camera_alt),
                      title: Text("imgPickerCamera".tr().toString()),
                      onTap: (){
                        print('Take Photo With Camera');
                        _onImageButtonPressed(ImageSource.camera);
                        Navigator.pop(context);
                      }
                  ),
                ],
              ),
              decoration: BoxDecoration(
                color: Theme.of(context).canvasColor,
                borderRadius: BorderRadius.only(
                  topLeft: const Radius.circular(10),
                  topRight: const Radius.circular(10),
                ),
              ),
            ),
          );
        });
  }
  BodyCollectionFun(){
    final mediaQuery = MediaQuery.of(context);
    // final appLocalizations = AppLocalizations.of(context);
    double CategoriTabWidth=130;
    if(_selectedIndex == 10){//Search list
      return FutureBuilder<ResponseApi>(
          future: resSearch,
          builder: (context, snap){
            if(snap.hasData){
              if (snap.connectionState == ConnectionState.waiting && _searchMeals.isEmpty) {
                return Center(
                  child: SystemControls().circularProgress(),
                );
              }
              else if(snap.hasData){
                if(snap.connectionState == ConnectionState.done) {
                  print("Add data Add data *************************** ");
                  _searchMeals += snap.data.data['products'];
                  if(currentPageindex <= maxPageIndex && stopRequest == true) {
                    _searchMeals += snap.data.data['products'];
                    stopRequest = false;
                    StartLoad = false;
                  }
                  currentPageindex = snap.data.data['pagination']['current_page'];
                  maxPageIndex = snap.data.data['pagination']['last_page'];
                }
                print(currentPageindex);
                if(_searchMeals.length>0){
                  return Container(
                    margin: EdgeInsets.only(top: 20),
                    child: ListView(
                      controller: _searchScrollController,
                      children: <Widget>[
                        Container(
                          alignment: Alignment.center,
                          height: 50,
                          child: Text('searchResult'.tr().toString()
                              +(searchCatName !="" ?searchCatName:searchText)),
                        ),
                        HomeItemsList(_searchMeals,_CartMeals, Lang,
                            Token, AppCurrency,design_Control,hasRegistration,
                            onAddCartSubmit,onSeaFavSubmit),
                        StartLoad? Container(
                          padding: EdgeInsets.all(20),
                          child: Center(
                            child: SystemControls().circularProgress(),
                          ),
                        )
                            :Container(
                          padding: EdgeInsets.all(20),
                          child: (currentPageindex<maxPageIndex)?
                          InkWell(
                            onTap: (){
                              if(_selectedIndex == 10) {//search
                                if(currentPageindex < maxPageIndex){
                                  currentPageindex +=1;
                                  setState(() {
                                    print("load new page ..... \n\n"+currentPageindex.toString());
                                    StartLoad = true;
                                    stopRequest = true;
                                    if(showSearchCat == false) {
                                      resSearch = SearchWithtitleApi(
                                          Lang, searchText, Token, currentPageindex.toString());
                                    }
                                    else {
                                      resSearch = CategoryListApi(Lang, searchCatId, Token,currentPageindex.toString());
                                    }
                                  });
                                }
                                else if(currentPageindex == maxPageIndex && StartLoad==true){
                                  setState(() {
                                    StartLoad = false;
                                  });
                                }
                              }
                            },
                            child: Center(
                              child: Text('loadMore'.tr().toString(),
                                style: TextStyle(
                                  fontSize: SystemControls.font3,
                                  fontFamily: Lang,
                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                  fontWeight: FontWeight.bold,
                                  height: 0.9,
                                ),
                              ),
                            ),
                          )
                              :Container(),
                        ),
                      ],
                    ),
                  );
                }
                else{
                  return Container(
                    child: Center(
                      child: Text('searchResultNull'.tr().toString(),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.bold,
                          fontSize: SystemControls.font2,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                        ),
                      ),
                    ),
                  );
                }
              }
            }
            return Center(
              child: SystemControls().circularProgress(),
            );
          }
      );
      return FutureBuilder<ResponseApi>(
          future: resSearch,
          builder: (context, snap){
            if (snap.connectionState == ConnectionState.waiting) {
              return Center(
                child: SystemControls().circularProgress(),
              );
            }
            if(snap.hasData){
              _searchMeals = snap.data.data['products'];
              if(_searchMeals != null && _searchMeals.length>0){
                return Container(
                  margin: EdgeInsets.only(top: 80),
                  child: HomeItemsList(_searchMeals, _CartMeals, Lang,
                      Token, AppCurrency,design_Control,hasRegistration,
                      onAddCartSubmit,onSeaFavSubmit),
                );
              }
              else{
                return Container(
                  child: Center(
                    child: Text('searchResultNull'.tr().toString(),//,
                      style: TextStyle(
                        fontFamily: Lang,
                        fontWeight: FontWeight.bold,
                        fontSize: SystemControls.font2,
                        color: globals.accountColorsData['TextHeaderColor']!=null
                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                      ),
                    ),
                  ),
                );
              }
            }
            return Center(
              child: SystemControls().circularProgress(),
            );
          }
      );
    }
    else {
      var shortestSide = mediaQuery.size.shortestSide;
      final bool useMobileLayout = shortestSide < 550;
      final bool offersListControlNavigDesi = design_Control==2&&selectedCatID==-2;
      bool noData = false;
      return Scaffold(
        floatingActionButton: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            SystemControls.cartAddPhoto?(
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FloatingActionButton(
                      onPressed: () {
                        showBottomSelectImgWidget(context: context);
                      },
                      heroTag: 'image0',
                      tooltip: 'Pick Image',
                      backgroundColor: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor']))
                          : SystemControls.MainColorz,
                      child: const Icon(Icons.add_photo_alternate,size: 35,),
                    ),
                    Container(
                      color: Colors.white,
                      //width: 80,
                      margin: EdgeInsets.only(top: 10),
                      child: Text('AddPicToCart'.tr().toString(),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font2,
                          height: 1.2,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    )
                  ],
                )
            ):(Container()),
          ],
        ),
        body: Stack(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                design_Control ==2 ||
                offersListControlNavigDesi || useMobileLayout == true || true?(
                    Container()
                ):(
                    Container(
                      width: CategoriTabWidth,
                      child: Container(
                        margin: EdgeInsets.only(right: 5, left: 5, top: 5),
                        color: Colors.white,
                        child: ListView(
                            shrinkWrap: true,
                            physics: ScrollPhysics(),
                            scrollDirection: Axis.vertical,
                            children: [
                              CategoryVerticalTabHeaderImg(0),
                              design_Control==1 ?(
                                  Column(
                                    children: <Widget>[
                                      InkWell(
                                        child: OfferVerticalCardItem(context,selectedCatID,Lang),
                                        onTap: () {
                                          _onItemTapped(-2);
                                        },
                                      ),
                                      CategoryVerticalTabRowImg(),
                                    ],
                                  )
                              ):(
                                  Container()
                              ),
                              ListView.builder(
                                  shrinkWrap: true,
                                  physics: ScrollPhysics(),
                                  scrollDirection: Axis.vertical,
                                  itemCount: _categories.length,
                                  itemBuilder: (context, index) {
                                    if (_categories[index]['categories_parent_id']!=null) {
                                      return Container();
                                    }
                                    else {
                                      return Column(
                                        children: <Widget>[
                                          InkWell(
                                            child: CategoryVerticalCardItem(
                                                _categories[index], selectedCatID,
                                                Lang),
                                            onTap: () {
                                              _onItemTapped(
                                                  _categories[index]['categories_id']);
                                            },
                                          ),
                                          index == _categories.length - 1 ? (
                                              Container()
                                          ) : (
                                              CategoryVerticalTabRowImg()
                                          ),
                                        ],
                                      );
                                    }
                                  }
                              ),
                              CategoryVerticalTabHeaderImg(10),
                            ]
                        ),
                      ),
                    )
                ),
                Flexible(
                  flex: 1,
                  child: CustomScrollView(
                    controller: _scrollController,
                    slivers: <Widget>[
                      //useMobileLayout==false ||
                      offersListControlNavigDesi?(
                          SliverList(delegate: SliverChildListDelegate([]))
                      ):(//Categories list at top
                          SliverAppBar(
                            pinned: true,
                            expandedHeight: 150,
                            floating: true,
                            backgroundColor: Color.fromRGBO(251, 224, 2, 0),
                            leading: new Container(),
                            flexibleSpace: Container(
                              //height: 85,
                              margin: EdgeInsets.only(bottom: 5),
                              color: Colors.white,
                              child: Padding(
                                padding: const EdgeInsets.only(bottom: 2),
                                child: ListView(
                                    padding: EdgeInsets.zero,
                                    scrollDirection: Axis.horizontal,
                                    children: [
                                      design_Control==1 ?(
                                          InkWell(

                                            child: OfferCardItem(context, selectedCatID,Lang),
                                            onTap: () {
                                              _meals.clear();
                                              Provider.of<DiscountProvider>(context,listen: false).fetchNotification(Lang, Token, context);

                                              _onItemTapped(-2);
                                            },
                                          )
                                      ):(
                                          Container()
                                      ),
                                      ListView.builder(

                                          padding: EdgeInsets.zero,
                                          shrinkWrap: true,
                                          physics: ScrollPhysics(),
                                          scrollDirection: Axis.horizontal,
                                          itemCount: _categories.length,
                                          itemBuilder: (context, index) {
                                            if (_categories[index]['categories_parent_id']!=null) {
                                              return Container();
                                            }
                                            else {
                                              return InkWell(
                                                child: CategoryCardItem(
                                                    _categories[index],
                                                    selectedCatID, Lang),
                                                onTap: () {
                                                  _onItemTapped(

                                                      _categories[index]['categories_id']);
                                                },
                                              );
                                            }
                                          }
                                      ),
                                    ]
                                ),
                              ),
                            ),

                          )
                      ),
                      SliverList(
                        delegate: SliverChildListDelegate([
                          Container(
                            child: FutureBuilder<ResponseApi>(
                                future: _categoryMeals,
                                builder: (context, AsyncSnapshot<ResponseApi>snap) {
                                  if (snap.connectionState == ConnectionState.waiting && _itemsList.isEmpty) {
                                    return Center(
                                      child: SystemControls().circularProgress(),
                                    );
                                  }
                                   if (snap.hasData ) {
                                     if((snap.data.data["offers"] != null &&snap.data.data['offers'].length>=1 &&snap.data.data["products"] == null) ||(snap.data.data["products"] != null &&snap.data.data["products"].length>=1&&snap.data.data['offers'] == null)){
                                    noData = false;

                                    bool type_offer;
                                    String typeText;
                                    if (selectedCatID == -2) {
                                      _itemsList = snap.data.data['offers'];
                                      _itemsList.removeWhere((element) => element["offers_price"] == "0");
                                      print(_itemsList);
                                      type_offer = true;
                                      typeText= "offers_";
                                    }
                                    else {
                                      maxPageIndex = snap.data.data['pagination']['last_page'];
                                      print(currentPageindex);
                                      if(snap.connectionState == ConnectionState.done
                                          && snap.data.data['pagination']['current_page'] != currentPageindex) {
                                        currentPageindex = snap.data.data['pagination']['current_page'];
                                        print(currentPageindex);
                                        _itemsList += snap.data.data['products'];
                                        stopRequest = false;
                                      }
                                      type_offer = false;
                                      typeText= "products_";
                                    }
                                    //int count = (((mediaQuery.size.width-10)/shortestSideCount)).round();
                                    int count = (((MediaQuery.of(context).size.width-10)/380)).round();
                                    //-2 means  offer
                                    if (selectedCatID != -2) {
                                      count *= SystemControls.MinNumberOfItemsOnRow;
                                    }
                                    int deviceShortestSide = mediaQuery.size.shortestSide.round();
                                    int cardHei;
                                    int imgCardRatio;
                                    if(deviceShortestSide > 415){
                                      cardHei = 300;
                                      imgCardRatio =5;
                                    }
                                    else {
                                      if(SystemControls.cardViewItemNumber==2){
                                        //cardHei = 280;
                                        cardHei = 225;
                                      }
                                      else{
                                        cardHei = 250;
                                      }
                                      //imgCardRatio=7;
                                      imgCardRatio=5;
                                    }

                                    print('Offers Offers');
                                    _itemsList.forEach((element) {
                                      print(element["offers_price"]);
                                      });
                                    print('Offers Offers');

                                      return    Container(
                                      margin: EdgeInsets.all(5),
                                      child: GridView.builder(
                                        //controller: _scrollController,
                                        shrinkWrap : true,
                                        physics: ScrollPhysics(),
                                        itemCount: _itemsList.length,
                                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: count,
                                          childAspectRatio:// 1,
                                          (((mediaQuery.size.width-(10*count))/count) /cardHei),
                                        ),
                                        itemBuilder: (BuildContext, int index){

                                          String imgURL;
                                          String CatImgURL;

                                          if(type_offer == true){
                                            imgURL = _itemsList[index]['offers_img_mobile'].toString();
                                          }
                                          else{
                                            imgURL = _itemsList[index]['products_img'].toString();
                                          }

                                          String CatName1,CatName2;
                                          if(_itemsList[index]['category'] == null){
                                            CatName1 = "offers".tr().toString();
                                            CatName2 = "";
                                            CatImgURL = "null";
                                          }
                                          else{
                                            CatImgURL = _itemsList[index]['category']['categories_img'];
                                            if(_itemsList[index]['category_series'] != null){
                                              List<dynamic> _category_series = _itemsList[index]['category_series'];
                                              CatName1 = _itemsList[index]['category_series'][0]['categories_title'];
                                              if(_category_series.length>1){
                                                CatName2 = _itemsList[index]['category_series'][1]['categories_title'];
                                              }
                                              else{
                                                CatName2 = "";
                                              }
                                            }
                                          }
                                          bool findOfferPrice = false;
                                          if(_itemsList[index][typeText+'price_after_sale']!=null&&
                                              _itemsList[index][typeText+'price_after_sale']!=""){
                                            findOfferPrice = true;
                                          }
                                          return InkWell(
                                            onTap: (){
                                              print('myIndex: $index');
                                              Navigator.push(context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                      //TODO Use new Item Details
                                                      ItemDetails(
                                                          _itemsList[index],
                                                          _CartMeals,
                                                          Lang,
                                                          AppCurrency,
                                                          Token,
                                                          design_Control,
                                                          type_offer,
                                                          null,
                                                          hasRegistration,true,selectedCatID, _itemsList[index]["products_available"])
                                                  ));
                                            },
                                            child: Container(
                                              margin: EdgeInsets.all(3),
                                              padding: EdgeInsets.all(0),
                                              decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.all(Radius.circular(SystemControls.RadiusCircValue)),
                                                  color: Colors.white,
                                                  border: Border.all(
                                                    width: 1,
                                                    color: globals.accountColorsData['BorderColor']!=null
                                                        ? Color(int.parse(globals.accountColorsData['BorderColor'])) : SystemControls.BorderColorz,
                                                  )
                                              ),
                                              child: ClipRRect(
                                                borderRadius: BorderRadius.all(Radius.circular(SystemControls.RadiusCircValue)),
                                                child: Container(
                                                  child: Column(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: <Widget>[
                                                      Expanded(
                                                        child: Stack(
                                                          children: <Widget>[
                                                            Container(
                                                              alignment: Alignment.center,
                                                              child: CachedNetworkImage(
                                                                imageUrl: SystemControls().GetImgPath(imgURL,"medium"),
                                                                placeholder: (context, url) => Center(child: SystemControls().circularProgress(),),
                                                                errorWidget: (context, url, error) => Center(child: Icon(Icons.broken_image),),
                                                                fit: type_offer?
                                                                BoxFit.cover:
                                                                BoxFit.contain,
                                                                width: MediaQuery.of(context).size.width,
                                                                height: MediaQuery.of(context).size.height,
                                                              ),
                                                            ),
                                                            type_offer
                                                                ? Container():Container(
                                                              height: imgCardRatio*6.0,
                                                              width: cardHei.toDouble(),
                                                              child: Row(
                                                                children: [
                                                                  Spacer(),
                                                                  _itemsList[index]['products_price_after_sale'].toString().isNotEmpty ?  Container(
                                                                    width: cardHei.toDouble()/4,

                                                                    height: imgCardRatio*6.0,
                                                                    decoration: BoxDecoration(
                                                                        color:  globals.accountColorsData['MainColor']!=null
                                                                            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                                                                        borderRadius: Lang =="ar"? BorderRadius.only(topLeft: Radius.circular(5),bottomRight: Radius.circular(15)):BorderRadius.only(topRight: Radius.circular(5),bottomLeft: Radius.circular(15))
                                                                    ),
                                                                    child: Center(child: Text("${((1-(double.parse("${_itemsList[index]['products_price_after_sale']}")/double.parse("${_itemsList[index]['products_price']}")))*100).toInt()}%", style: TextStyle(
                                                                      fontSize: SystemControls.font3,
                                                                      fontFamily: Lang,
                                                                      color: globals.accountColorsData['TextOnMainColor']!=null
                                                                          ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                                                                      fontWeight: FontWeight.bold,
                                                                      height: 0.9,
                                                                    ),),),
                                                                  )  :Container()
                                                                ],
                                                              ),

                                                            ),
                                                            Column(
                                                              children: <Widget>[
                                                                hasRegistration? Row(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                  children: <Widget>[
                                                                    type_offer?(
                                                                        Container()
                                                                    ):(
                                                                        Container(
                                                                          padding: EdgeInsets.all(8),
                                                                          decoration: SystemControls().mealButtonDecoration(),
                                                                          child: InkWell(
                                                                            onTap: ()async{
                                                                              if(Token == ""){
                                                                                ErrorDialogAlertGoTOLogin(context,
                                                                                    "mustLogin".tr().toString(),Lang);
                                                                              }
                                                                              else{
                                                                                var res = await updateFavsItem(
                                                                                    Lang, Token,_itemsList[index][typeText+'id'],context);
                                                                                if(res.status ==200){
                                                                                  print("get login info ++ "+ res.message);
                                                                                  SuccDialogAlertStaySameLayout(context, res.message,ValueKey("categoryList2"));
                                                                                  setState(() {
                                                                                    if(_itemsList[index]['is_fav']==1){
                                                                                      _itemsList[index]['is_fav'] = 0;
                                                                                    }
                                                                                    else{
                                                                                      _itemsList[index]['is_fav'] = 1;
                                                                                    }
                                                                                  });
                                                                                }
                                                                                else if(res.status == 401){
                                                                                  SystemControls().LogoutSetUserData(context, Lang);
                                                                                  ErrorDialogAlertBackHome(context, res.message);
                                                                                }
                                                                                else{
                                                                                  ErrorDialogAlert(context, res.message);
                                                                                }
                                                                              }
                                                                            },//onClickFavButton,
                                                                            child: Row(
                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                              children: <Widget>[
                                                                                _itemsList[index]['is_fav']==0?(
                                                                                    SystemControls().GetSVGImagesAsset(
                                                                                        "assets/Icons/heart.svg",
                                                                                        18,
                                                                                        null)
                                                                                ):(
                                                                                    SystemControls().GetSVGImagesAsset(
                                                                                        "assets/Icons/heart.svg",
                                                                                        18,
                                                                                        globals.accountColorsData['MainColor']!=null
                                                                                            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz)
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        )
                                                                    ),
                                                                  ],
                                                                )
                                                                    :Container(),
                                                              ],
                                                            )
                                                          ],
                                                        ),
                                                        flex: _itemsList[index][typeText+'price']=="0"||
                                                            _itemsList[index][typeText+'price']==null? 9:imgCardRatio,
                                                      ),
                                                      Expanded(
                                                        child: Container(
                                                          margin: EdgeInsets.only(top: 0,right: 5,left: 5),
                                                          child: Column(
                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: <Widget>[
                                                              Padding(padding: EdgeInsets.all(2)),
                                                              Row(
                                                                mainAxisSize: MainAxisSize.max,
                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                children: <Widget>[
                                                                  Expanded(
                                                                    flex: 7,
                                                                    child: Column(
                                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                      children: <Widget>[
                                                                        RichText(
                                                                          textAlign: TextAlign.start,
                                                                          text: new TextSpan(
                                                                            children: <TextSpan>[
                                                                              TextSpan(
                                                                                text: _itemsList[index][typeText+'price'].toString()=='0'
                                                                                    ||_itemsList[index][typeText+'price'].toString()==''?(""):(
                                                                                    findOfferPrice
                                                                                        ? _itemsList[index][typeText+'price_after_sale'].toString()+" "+ AppCurrency + "  "
                                                                                        : _itemsList[index][typeText+'price'].toString()+" "+ AppCurrency+"  "
                                                                                ),
                                                                                style: TextStyle(
                                                                                  fontSize: SystemControls.font3,
                                                                                  fontFamily: Lang,
                                                                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                                                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                                                                  fontWeight: FontWeight.bold,
                                                                                  height: 0.9,
                                                                                ),
                                                                              ),
                                                                              findOfferPrice?(
                                                                                  TextSpan(text: ' ',)
                                                                              ):(TextSpan()
                                                                              ),
                                                                              findOfferPrice?(
                                                                                  TextSpan(
                                                                                    text: _itemsList[index][typeText+'price']==null? ""
                                                                                        : _itemsList[index][typeText+'price'].toString()+" "+ AppCurrency,
                                                                                    style: new TextStyle(
                                                                                      fontSize: SystemControls.font4,
                                                                                      fontFamily: Lang,
                                                                                      color: globals.accountColorsData['TextdetailsColor']!=null
                                                                                          ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                                                      fontWeight: FontWeight.normal,
                                                                                      height: 0.9,
                                                                                      decoration: TextDecoration.lineThrough,
                                                                                    ),
                                                                                  )
                                                                              ):(TextSpan()
                                                                              ),
                                                                              TextSpan(
                                                                                text: _itemsList[index]['products_type']=='weight'?
                                                                                'priceForWeight'.tr().toString()
                                                                                    :'priceForCount'.tr().toString(),
                                                                                style: TextStyle(
                                                                                  fontSize: SystemControls.font3,
                                                                                  fontFamily: Lang,
                                                                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                                                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                                                                  fontWeight: FontWeight.bold,
                                                                                  height: 0.9,
                                                                                ),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                          maxLines: 1,
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ],
                                                              ), //price
                                                              //Padding(padding: EdgeInsets.all(2)),
                                                              Padding(padding: EdgeInsets.all(2)),
                                                              Row(
                                                                mainAxisSize: MainAxisSize.max,
                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                children: <Widget>[
                                                                  Expanded(
                                                                    flex: 7,
                                                                    child: Column(
                                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                                      mainAxisAlignment: MainAxisAlignment.center,

                                                                      children: <Widget>[
                                                                        Container(
                                                                          //height: 38,
                                                                          child: Text(_itemsList[index][typeText+'title'].toString(),//"mix grill",
                                                                            style: TextStyle(
                                                                              fontFamily: Lang,
                                                                              fontSize: SystemControls.font4,
                                                                              color: globals.accountColorsData['TextdetailsColor']!=null
                                                                                  ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                                              fontWeight: FontWeight.normal,
                                                                              height: 1.2,
                                                                            ),
                                                                            maxLines: 2,
                                                                            textAlign: TextAlign.start,
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ],
                                                              ), //meal title/name
                                                              Padding(padding: EdgeInsets.all(3)),
                                                              _itemsList[index][typeText+'price'].toString()=="0"||
                                                                  _itemsList[index][typeText+'price']==null?(
                                                                  Container()
                                                              ):
                                                              Row(
                                                                mainAxisSize: MainAxisSize.max,
                                                                mainAxisAlignment: MainAxisAlignment.center,
                                                                children: <Widget>[
                                                                  _itemsList[index]["choices"] != null && _itemsList[index]["choices"].length > 0?
                                                                  Container():
                                                                  Expanded(
                                                                    flex: 4,
                                                                    child: InkWell(
                                                                      onTap: (){
                                                                        dynamic startVal;
                                                                        if(_itemsList[index]['products_type'] != "weight") { //Product follow weight or num
                                                                          startVal = 1;
                                                                        }
                                                                        else{
                                                                          startVal = double.parse(_itemsList[index]['products_lowest_weight_available']);
                                                                        }
                                                                        showBottomSelectQuantity(context,startVal,index);
                                                                      },
                                                                      child: Container(
                                                                        padding: EdgeInsets.only(right: 5,left: 5),
                                                                        height: 30,
                                                                        decoration: BoxDecoration(
                                                                          borderRadius: BorderRadius.all(
                                                                            Radius.circular(SystemControls.RadiusCircValue/2),),
                                                                          border: Border.all(color:globals.accountColorsData['TextdetailsColor']!=null
                                                                              ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                                              width: 1),
                                                                        ),
                                                                        //color: Colors.cyan,
                                                                        child: Row(
                                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                          children: [
                                                                            RichText(
                                                                              textAlign: TextAlign.start,
                                                                              text: new TextSpan(
                                                                                children: <TextSpan>[
                                                                                  TextSpan(
                                                                                    text: _itemsList[index]['quantity']==null
                                                                                        ?"1":_itemsList[index]['quantity'].toString(),
                                                                                    style: TextStyle(
                                                                                      fontFamily: Lang,
                                                                                      fontSize: SystemControls.font4,
                                                                                      color: globals.accountColorsData['TextdetailsColor']!=null
                                                                                          ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                                                      fontWeight: FontWeight.normal,
                                                                                      height: 1.2,
                                                                                    ),
                                                                                  ),
                                                                                  TextSpan(
                                                                                    text: "  ",
                                                                                    style: TextStyle(
                                                                                      fontSize: SystemControls.font3,
                                                                                      fontFamily: Lang,
                                                                                      color: globals.accountColorsData['TextHeaderColor']!=null
                                                                                          ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                                                                      fontWeight: FontWeight.bold,
                                                                                      height: 0.9,
                                                                                    ),
                                                                                  ),
                                                                                  TextSpan(
                                                                                    text: _itemsList[index]['products_type']=='weight'?
                                                                                    'KG'.tr().toString()
                                                                                        :'piece'.tr().toString(),
                                                                                    style: TextStyle(
                                                                                      fontFamily: Lang,
                                                                                      fontSize: SystemControls.font4,
                                                                                      color: globals.accountColorsData['TextdetailsColor']!=null
                                                                                          ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                                                      fontWeight: FontWeight.normal,
                                                                                      height: 1.2,
                                                                                    ),
                                                                                  ),
                                                                                ],
                                                                              ),
                                                                              maxLines: 1,
                                                                            ),
                                                                            Icon(
                                                                              Icons.keyboard_arrow_down,
                                                                              color: Colors.black,),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  SizedBox(width: 10,),
                                                                  _itemsList[index]["products_available"] !="0"?  Expanded(
                                                                    flex: 3,
                                                                    child: hasRegistration?(
                                                                        //Remove Add to cart button if price zero or null
                                                                        _itemsList[index][typeText+'price'].toString()=="0"||
                                                                            _itemsList[index][typeText+'price']==null?(
                                                                            Container()
                                                                        ):(
                                                                            Container(
                                                                              padding: EdgeInsets.only(right: 10,left: 10),
                                                                              decoration: BoxDecoration(
                                                                                borderRadius: BorderRadius.all(
                                                                                  Radius.circular(SystemControls.RadiusCircValue/2),),
                                                                                border: Border.all(color:globals.accountColorsData['TextdetailsColor']!=null
                                                                                    ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,width: 1),
                                                                              ),
                                                                              height: 30,
                                                                              child: InkWell(
                                                                                  child: Row(
                                                                                    mainAxisSize: MainAxisSize.max,
                                                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                                                    children: <Widget>[
                                                                                      Text(_itemsList[index]["choices"] != null && _itemsList[index]["choices"].length > 0?
                                                                                      "showDetails".tr().toString()
                                                                                          : "",//AppLocalizations.of(context).translate("addToCart"),
                                                                                        style: TextStyle(
                                                                                          fontFamily: Lang,
                                                                                          fontSize: SystemControls.font3,
                                                                                          color: globals.accountColorsData['TextHeaderColor']!=null
                                                                                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                                                                          fontWeight: FontWeight.normal,
                                                                                          height: 1,
                                                                                        ),
                                                                                        maxLines: 1,
                                                                                        textAlign: TextAlign.start,
                                                                                      ),
                                                                                      _itemsList[index]["choices"] != null && _itemsList[index]["choices"].length > 0?
                                                                                      Container()
                                                                                          :Icon(Icons.add_shopping_cart,color: Colors.black,size: 18,),
                                                                                    ],
                                                                                  ),
                                                                                  onTap: () {

                                                                                    if(_itemsList[index]["choices"] != null && _itemsList[index]["choices"].length > 0){
                                                                                      Navigator.push(context,
                                                                                          MaterialPageRoute(
                                                                                              builder: (context) =>
                                                                                              //TODO Use new Item Details
                                                                                              ItemDetails(
                                                                                                  _itemsList[index],
                                                                                                  _CartMeals,
                                                                                                  Lang,
                                                                                                  AppCurrency,
                                                                                                  Token,

                                                                                                  design_Control,
                                                                                                  type_offer,

                                                                                                  null,
                                                                                                  hasRegistration,false,id,_itemsList[index]["products_available"])
                                                                                          ));
                                                                                    }
                                                                                    else if (type_offer//_items[index][typeText+'price']!="0"
                                                                                        || _itemsList[index][typeText+'available']=="1")
                                                                                    {
                                                                                      setState(() {
                                                                                        print(
                                                                                            "add to cart");
                                                                                        int sear;
                                                                                        if (type_offer) {
                                                                                          sear = FindMealAtCart(_itemsList[index][typeText+'id'],
                                                                                              'offer');
                                                                                        }
                                                                                        else {
                                                                                          sear = FindMealAtCart(
                                                                                              _itemsList[index][typeText +
                                                                                                  'id'],
                                                                                              'product');
                                                                                        }
                                                                                        //TODO
                                                                                        if (true) { //sear == -1
                                                                                          dynamic AddMeal = CartMeal();
                                                                                          AddMeal.meals_id =
                                                                                          _itemsList[index][typeText + 'id'];
                                                                                          AddMeal.meals_title =
                                                                                          _itemsList[index][typeText + 'title'];
                                                                                          AddMeal.meals_img = imgURL;
                                                                                          AddMeal.meals_desc =
                                                                                          _itemsList[index][typeText + 'desc'];
                                                                                          if (findOfferPrice) {
                                                                                            AddMeal.meals_price =
                                                                                                _itemsList[index][typeText+'price_after_sale'].toString();
                                                                                          }
                                                                                          else {
                                                                                            AddMeal.meals_price =
                                                                                                _itemsList[index][typeText + 'price'].toString();
                                                                                          }
                                                                                          if (type_offer) {
                                                                                            AddMeal.type = "offer";
                                                                                            AddMeal.follow_delivery_time = "1";
                                                                                          }
                                                                                          else {
                                                                                            AddMeal.quantityType =
                                                                                            _itemsList[index]['products_type'];//Product follow weight or num
                                                                                            AddMeal.minQuantity =
                                                                                            _itemsList[index]['products_lowest_weight_available'];//Product follow weight or num
                                                                                            AddMeal.follow_delivery_time =
                                                                                            _itemsList[index]['category']['categories_follow_delivery_time'];
                                                                                            AddMeal.type = "product";
                                                                                          }
                                                                                          if(_itemsList[index]['quantity'] == null){
                                                                                            _itemsList[index]['quantity'] = 1;
                                                                                          }
                                                                                          AddMeal.count = _itemsList[index]['quantity'];

                                                                                          dynamic obj = AddMeal.toJson();
                                                                                          write_to_file(obj);
                                                                                          //setState(() {
                                                                                          _CartMeals.add(obj);
                                                                                          //});

                                                                                          CartStateProvider cartState = Provider.of<CartStateProvider>(context,listen: false);
                                                                                          int x = int.parse(cartState.productsCount);
                                                                                          x++;
                                                                                          cartState.setCurrentProductsCount(x.toString());

                                                                                          SuccDialogAlertStaySameLayout(
                                                                                              context,

                                                                                                  'orderAddtoCart'.tr().toString(),ValueKey("categoryList3"));
                                                                                        }
                                                                                        else {
                                                                                          // ErrorDialogAlert(
                                                                                          //     context,
                                                                                          //     AppLocalizations
                                                                                          //         .of(
                                                                                          //         context)
                                                                                          //         .translate(
                                                                                          //         'mealAleadyAdded'));
                                                                                          //TODO if want to increase meal count
                                                                                          //_CartMeals[sear]['count']++;
                                                                                          //await UpdateList(_CartMeals);
                                                                                        }
                                                                                      });
                                                                                    } //onClickAddToCart(context,_item,type_offer),
                                                                                  }
                                                                              ),
                                                                            )
                                                                        )
                                                                    ):(
                                                                        Container()
                                                                    ),
                                                                  ) : Expanded(
                                                                    flex: 3,
                                                                    child: hasRegistration?  MyTooltip(
                                                                      message: "productNotExist".tr().toString(),
                                                                      child: Container(
                                                                        padding: EdgeInsets.only(right: 10,left: 10),
                                                                        decoration: BoxDecoration(
                                                                          color: Colors.grey[100*3],
                                                                          borderRadius: BorderRadius.all(
                                                                            Radius.circular(SystemControls.RadiusCircValue/2),),
                                                                          border: Border.all(color:globals.accountColorsData['TextdetailsColor']!=null
                                                                              ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                                              width: 1),
                                                                        ),
                                                                        //padding: EdgeInsets.only(right: 8,left: 8),
                                                                        height: 30,
                                                                        child: Icon(Icons.add_shopping_cart),

                                                                      ),
                                                                    ):Container(),
                                                                  ),
                                                                ],
                                                              ),
                                                              Padding(padding: EdgeInsets.all(3)),
                                                            ],
                                                          ),
                                                        ),
                                                        flex: 4,
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          );
                                          // return InkWell(
                                          //   onTap: (){
                                          //     Navigator.push(context,
                                          //         MaterialPageRoute(
                                          //             builder: (context) =>
                                          //             //TODO Use new Item Details
                                          //             ItemDetails(
                                          //                 _itemsList[index],
                                          //                 _CartMeals,
                                          //                 Lang,
                                          //                 AppCurrency,
                                          //                 Token,
                                          //                 design_Control,
                                          //                 type_offer,
                                          //                 null,
                                          //                 hasRegistration,false,id,_itemsList[index]["products_available"])
                                          //         ));
                                          //   },
                                          //   child: Container(
                                          //     margin: EdgeInsets.all(3),
                                          //     padding: EdgeInsets.all(0),
                                          //     decoration: BoxDecoration(
                                          //         borderRadius: BorderRadius.all(Radius.circular(SystemControls.RadiusCircValue)),
                                          //         color: Colors.white,
                                          //         border: Border.all(
                                          //           width: 1,
                                          //           color: globals.accountColorsData['BorderColor']!=null
                                          //               ? Color(int.parse(globals.accountColorsData['BorderColor'])) : SystemControls.BorderColorz,
                                          //         )
                                          //     ),
                                          //     child: ClipRRect(
                                          //       borderRadius: BorderRadius.all(Radius.circular(SystemControls.RadiusCircValue)),
                                          //       child: Container(
                                          //         child: Column(
                                          //           mainAxisAlignment: MainAxisAlignment.start,
                                          //           children: <Widget>[
                                          //             Expanded(
                                          //               child: Stack(
                                          //                 children: <Widget>[
                                          //                   Container(
                                          //                     //width: MediaQuery.of(context).size.width,
                                          //                     alignment: Alignment.center,
                                          //                     child: CachedNetworkImage(
                                          //                       imageUrl: SystemControls().GetImgPath(imgURL,"medium"),
                                          //                       placeholder: (context, url) => Center(child: SystemControls().circularProgress(),),
                                          //                       errorWidget: (context, url, error) => Center(child: Icon(Icons.error),),
                                          //                       fit: BoxFit.contain,
                                          //                       width: MediaQuery.of(context).size.width,
                                          //                       height: MediaQuery.of(context).size.height,
                                          //                     ),
                                          //                   ),
                                          //                   Column(
                                          //                     children: <Widget>[
                                          //                       _itemsList[index][typeText+'price']=="0"||
                                          //                           _itemsList[index][typeText+'price']==null?(
                                          //                           Container()
                                          //                       ):(
                                          //                           Container(
                                          //                             alignment: Alignment.centerLeft,
                                          //                             child: Container(
                                          //                               //height: 50,
                                          //                               padding: EdgeInsets.all(4),
                                          //                               decoration: BoxDecoration(
                                          //                                 color: globals.accountColorsData['MainColor']!=null
                                          //                                     ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,//Colors.green,
                                          //                                 borderRadius: BorderRadius.only(
                                          //                                   bottomRight: Radius.circular(SystemControls.RadiusCircValue),),
                                          //                               ),
                                          //                               child: RichText(
                                          //                                 textAlign: TextAlign.start,
                                          //                                 text: new TextSpan(
                                          //                                   children: <TextSpan>[
                                          //                                     findOfferPrice?(
                                          //                                         TextSpan(
                                          //                                           text: _itemsList[index][typeText+'price']==null? ""
                                          //                                               : _itemsList[index][typeText+'price']+" "+ AppCurrency,
                                          //                                           style: new TextStyle(
                                          //                                             fontSize: SystemControls.font5,
                                          //                                             fontFamily: Lang,
                                          //                                             color: globals.accountColorsData['TextdetailsColor']!=null
                                          //                                                 ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                          //                                             fontWeight: FontWeight.normal,
                                          //                                             decoration: TextDecoration.lineThrough,
                                          //                                           ),
                                          //                                         )
                                          //                                     ):(TextSpan()
                                          //                                     ),
                                          //                                     findOfferPrice?(
                                          //                                         TextSpan(text: '\n',)
                                          //                                     ):(TextSpan()
                                          //                                     ),
                                          //                                     new TextSpan(
                                          //                                       text: findOfferPrice
                                          //                                           ? _itemsList[index][typeText+'price_after_sale']+" "+ AppCurrency
                                          //                                           : _itemsList[index][typeText+'price']+" "+ AppCurrency,
                                          //                                       style: TextStyle(
                                          //                                         fontSize: SystemControls.font4,
                                          //                                         fontFamily: Lang,
                                          //                                         color: globals.accountColorsData['TextHeaderColor']!=null
                                          //                                             ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                          //                                         fontWeight: FontWeight.bold,
                                          //                                       ),
                                          //                                     ),
                                          //                                   ],
                                          //                                 ),
                                          //                               ),
                                          //                             ),
                                          //                           )
                                          //                       ),
                                          //                     ],
                                          //                   )
                                          //                 ],
                                          //               ),
                                          //               flex: imgCardRatio,
                                          //             ),
                                          //             Expanded(
                                          //               child: Container(
                                          //                 margin: EdgeInsets.only(top: 0,right: 5,left: 5),
                                          //                 child: Column(
                                          //                   mainAxisAlignment: MainAxisAlignment.start,
                                          //                   crossAxisAlignment: CrossAxisAlignment.start,
                                          //                   children: <Widget>[
                                          //                     Padding(padding: EdgeInsets.all(4)),
                                          //                     Row(
                                          //                       mainAxisSize: MainAxisSize.max,
                                          //                       mainAxisAlignment: MainAxisAlignment.center,
                                          //                       children: <Widget>[
                                          //                         Expanded(
                                          //                           flex: 7,
                                          //                           child: Column(
                                          //                             crossAxisAlignment: CrossAxisAlignment.start,
                                          //                             mainAxisAlignment: MainAxisAlignment.center,
                                          //                             children: <Widget>[
                                          //                               Text(_itemsList[index][typeText+'title'].toString(),//"mix grill",
                                          //                                 style: TextStyle(
                                          //                                   fontFamily: Lang,
                                          //                                   fontSize: SystemControls.font3,
                                          //                                   color: globals.accountColorsData['TextHeaderColor']!=null
                                          //                                       ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                          //                                   fontWeight: FontWeight.normal,
                                          //                                   height: 1,
                                          //                                 ),
                                          //                                 maxLines: 1,
                                          //                                 textAlign: TextAlign.start,
                                          //                               ),
                                          //                             ],
                                          //                           ),
                                          //                         ),
                                          //                         SystemControls.cardViewItemNumber==1?(
                                          //                             Expanded(
                                          //                               flex: 3,
                                          //                               child: Column(
                                          //                                 crossAxisAlignment: CrossAxisAlignment.end,
                                          //                                 mainAxisAlignment: MainAxisAlignment.center,
                                          //                                 children: <Widget>[
                                          //                                   Text(_itemsList[index][typeText+'calories'].toString()=='null'? ""
                                          //                                       :_itemsList[index][typeText+'calories'].toString()
                                          //                                       + "calories".tr().toString(),
                                          //                                     style: TextStyle(
                                          //                                       fontFamily: Lang,
                                          //                                       fontSize: SystemControls.font4,
                                          //                                       color: globals.accountColorsData['TextdetailsColor']!=null
                                          //                                           ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                          //                                       fontWeight: FontWeight.normal,
                                          //                                       height: 0.8,
                                          //                                     ),
                                          //                                     maxLines: 1,
                                          //                                     textAlign: TextAlign.start,
                                          //                                   ),
                                          //                                 ],
                                          //                               ),
                                          //                             )
                                          //                         ):(
                                          //                             Container()
                                          //                         ),
                                          //                       ],
                                          //                     ), //Meal title & cal count
                                          //                     Padding(padding: EdgeInsets.all(2)),
                                          //                     SystemControls.cardViewItemNumber==1?(
                                          //                         Padding(padding: EdgeInsets.all(4))
                                          //                     ):(
                                          //                         Container()
                                          //                     ),
                                          //                     Row(
                                          //                       mainAxisSize: MainAxisSize.max,
                                          //                       mainAxisAlignment: MainAxisAlignment.center,
                                          //                       children: <Widget>[
                                          //                         Expanded(
                                          //                           flex: 5,
                                          //                           child: Row(
                                          //                             crossAxisAlignment: CrossAxisAlignment.center,
                                          //                             mainAxisAlignment: MainAxisAlignment.start,
                                          //                             children: <Widget>[
                                          //                               SystemControls.cardViewItemNumber==1?(
                                          //                                   type_offer?(
                                          //                                       SystemControls().GetSVGImagesAsset(
                                          //                                           'assets/Icons/offer.svg', 25,
                                          //                                           Colors.black)
                                          //                                   ):(
                                          //                                       CachedNetworkImage(
                                          //                                         imageUrl: SystemControls().GetImgPath(
                                          //                                             CatImgURL,"original"),
                                          //                                         height: 25,
                                          //                                       )
                                          //                                   )
                                          //                               ):(Container()),
                                          //
                                          //                               SystemControls.cardViewItemNumber==1?(
                                          //                                   Padding(padding: EdgeInsets.all(4))
                                          //                               ):(Container()),
                                          //                               InkWell(
                                          //                                 onTap: (){},
                                          //                                 child: Text(CatName1,
                                          //                                   style: TextStyle(
                                          //                                     fontFamily: Lang,
                                          //                                     fontSize: SystemControls.font4,
                                          //                                     color: globals.accountColorsData['TextdetailsColor']!=null
                                          //                                         ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                          //                                     fontWeight: FontWeight.normal,
                                          //                                     height: 0.8,
                                          //                                   ),
                                          //                                   maxLines: 1,
                                          //                                   textAlign: TextAlign.start,
                                          //                                 ),
                                          //                               ),
                                          //                               CatName2!=""?(
                                          //                                   Padding(
                                          //                                     padding: EdgeInsets.all(4),
                                          //                                     child: Text("/",
                                          //                                       style: TextStyle(
                                          //                                         fontFamily: Lang,
                                          //                                         fontSize: SystemControls.font4,
                                          //                                         color: globals.accountColorsData['TextdetailsColor']!=null
                                          //                                             ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                          //                                         fontWeight: FontWeight.normal,
                                          //                                         height: 0.8,
                                          //                                       ),
                                          //                                       maxLines: 1,
                                          //                                       textAlign: TextAlign.start,
                                          //                                     ),
                                          //                                   )
                                          //                               ):(Container()),
                                          //                               InkWell(
                                          //                                 onTap: (){},
                                          //                                 child: Text(CatName2,
                                          //                                   style: TextStyle(
                                          //                                     fontFamily: Lang,
                                          //                                     fontSize: SystemControls.font4,
                                          //                                     color: globals.accountColorsData['TextdetailsColor']!=null
                                          //                                         ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                          //                                     fontWeight: FontWeight.normal,
                                          //                                     height: 0.8,
                                          //                                   ),
                                          //                                   maxLines: 1,
                                          //                                   textAlign: TextAlign.start,
                                          //                                 ),
                                          //                               ),
                                          //                             ],
                                          //                           ),
                                          //                         ),
                                          //                         SystemControls.cardViewItemNumber==1?(
                                          //                             Expanded(
                                          //                               flex: 6,
                                          //                               child: Row(
                                          //                                 crossAxisAlignment: CrossAxisAlignment.end,
                                          //                                 mainAxisAlignment: MainAxisAlignment.end,
                                          //                                 children: <Widget>[
                                          //                                   type_offer?(
                                          //                                       Container()
                                          //                                   ):(
                                          //                                       Container(
                                          //                                         decoration: SystemControls().mealButtonDecoration(),
                                          //                                         height: 40,
                                          //                                         width: 40,
                                          //                                         child: InkWell(
                                          //                                           onTap: ()async{
                                          //                                             if(Token == ""){
                                          //                                               ErrorDialogAlertGoTOLogin(context,
                                          //                                                   "mustLogin".tr().toString(),Lang);
                                          //                                             }
                                          //                                             else{
                                          //                                               var res = await updateFavsItem(
                                          //                                                   Lang, Token,_itemsList[index][typeText+'id']);
                                          //                                               if(res.status ==200){
                                          //                                                 print("get login info ++ "+ res.message);
                                          //                                                 SuccDialogAlertStaySameLayout(context, res.message);
                                          //                                                 setState(() {
                                          //                                                   if(_itemsList[index]['is_fav']==1){
                                          //                                                     _itemsList[index]['is_fav'] = 0;
                                          //                                                   }
                                          //                                                   else{
                                          //                                                     _itemsList[index]['is_fav'] = 1;
                                          //                                                   }
                                          //                                                 });
                                          //                                               }
                                          //                                               else if(res.status == 401){
                                          //                                                 SystemControls().LogoutSetUserData(context, Lang);
                                          //                                                 ErrorDialogAlertBackHome(context, res.message);
                                          //                                               }
                                          //                                               else{
                                          //                                                 ErrorDialogAlert(context, res.message);
                                          //                                               }
                                          //                                             }
                                          //                                           },//onClickFavButton,
                                          //                                           child: Row(
                                          //                                             mainAxisAlignment: MainAxisAlignment.center,
                                          //                                             children: <Widget>[
                                          //                                               _itemsList[index]['is_fav']==0?(
                                          //                                                   SystemControls().GetSVGImagesAsset(
                                          //                                                       "assets/Icons/heart.svg",
                                          //                                                       25,
                                          //                                                       null)
                                          //                                               ):(
                                          //                                                   SystemControls().GetSVGImagesAsset(
                                          //                                                       "assets/Icons/heart.svg",
                                          //                                                       25,
                                          //                                                       globals.accountColorsData['MainColor']!=null
                                          //                                                           ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz)
                                          //                                               ),
                                          //                                             ],
                                          //                                           ),
                                          //                                         ),
                                          //                                       )
                                          //                                   ),
                                          //                                   Padding(padding: EdgeInsets.all(4)),
                                          //                                   //Remove Add to cart button if price zero or null
                                          //                                   _itemsList[index][typeText+'price']=="0"||
                                          //                                       _itemsList[index][typeText+'price']==null?(
                                          //                                       Container()
                                          //                                   ):(
                                          //                                       Container(
                                          //                                         decoration: SystemControls().mealButtonDecoration(),
                                          //                                         height: 40,
                                          //                                         width: 40,
                                          //                                         child: InkWell(
                                          //                                             child: Row(
                                          //                                               mainAxisAlignment: MainAxisAlignment.center,
                                          //                                               children: <Widget>[
                                          //                                                 //ToDo if _items[index][typeText+'available'] == null or != "1"
                                          //                                                 //Show othr icon with line
                                          //                                                 SystemControls().GetSVGImagesAsset(
                                          //                                                     "assets/Icons/cart.svg",
                                          //                                                     25,
                                          //                                                     Colors.black),
                                          //                                               ],
                                          //                                             ),
                                          //                                             onTap: () {
                                          //                                               //TODo remove offer after get available from api
                                          //                                               if (type_offer || _itemsList[index][typeText+'available']!=null
                                          //                                                   || _itemsList[index][typeText+'available']=="1") {
                                          //                                                 print("out add to cart  out");
                                          //                                                 setState(() {
                                          //                                                   print(
                                          //                                                       "add to cart");
                                          //                                                   int sear;
                                          //                                                   if (type_offer) {
                                          //                                                     sear =
                                          //                                                         FindMealAtCart(
                                          //                                                             _itemsList[index][typeText +
                                          //                                                                 'id'],
                                          //                                                             'offer');
                                          //                                                   }
                                          //                                                   else {
                                          //                                                     sear =
                                          //                                                         FindMealAtCart(
                                          //                                                             _itemsList[index][typeText +
                                          //                                                                 'id'],
                                          //                                                             'product');
                                          //                                                   }
                                          //                                                   //TODO
                                          //                                                   if (true) { //sear == -1
                                          //                                                     dynamic AddMeal = CartMeal();
                                          //                                                     AddMeal
                                          //                                                         .meals_id =
                                          //                                                     _itemsList[index][typeText +
                                          //                                                         'id'];
                                          //                                                     AddMeal
                                          //                                                         .meals_title =
                                          //                                                     _itemsList[index][typeText +
                                          //                                                         'title'];
                                          //                                                     AddMeal
                                          //                                                         .meals_img =
                                          //                                                         imgURL;
                                          //                                                     AddMeal
                                          //                                                         .meals_desc =
                                          //                                                     _itemsList[index][typeText +
                                          //                                                         'desc'];
                                          //                                                     if (findOfferPrice) {
                                          //                                                       AddMeal
                                          //                                                           .meals_price =
                                          //                                                           _itemsList[index][typeText +
                                          //                                                               'price_after_sale'].toString();
                                          //                                                     }
                                          //                                                     else {
                                          //                                                       AddMeal
                                          //                                                           .meals_price =
                                          //                                                           _itemsList[index][typeText +
                                          //                                                               'price'].toString();
                                          //                                                     }
                                          //                                                     if (type_offer) {
                                          //                                                       AddMeal
                                          //                                                           .type =
                                          //                                                       "offer";
                                          //                                                       AddMeal
                                          //                                                           .follow_delivery_time =
                                          //                                                       "1";
                                          //                                                     }
                                          //                                                     else {
                                          //                                                       AddMeal
                                          //                                                           .follow_delivery_time =
                                          //                                                       _itemsList[index]['category']['categories_follow_delivery_time'];
                                          //                                                       AddMeal
                                          //                                                           .type =
                                          //                                                       "product";
                                          //                                                     }
                                          //                                                     AddMeal
                                          //                                                         .count =
                                          //                                                     1;
                                          //
                                          //                                                     dynamic obj = AddMeal
                                          //                                                         .toJson();
                                          //                                                     write_to_file(
                                          //                                                         obj);
                                          //                                                     setState(() {
                                          //                                                       _CartMeals
                                          //                                                           .add(
                                          //                                                           obj);
                                          //                                                     });
                                          //
                                          //                                                     SuccDialogAlertStaySameLayout(
                                          //                                                         context,
                                          //                                                         AppLocalizations
                                          //                                                             .of(
                                          //                                                             context)
                                          //                                                             .translate(
                                          //                                                             'orderAddtoCart'));
                                          //                                                   }
                                          //                                                   else {
                                          //                                                     ErrorDialogAlert(
                                          //                                                         context,
                                          //                                                         AppLocalizations
                                          //                                                             .of(
                                          //                                                             context)
                                          //                                                             .translate(
                                          //                                                             'mealAleadyAdded'));
                                          //                                                     //TODO if want to increase meal count
                                          //                                                     //_CartMeals[sear]['count']++;
                                          //                                                     //await UpdateList(_CartMeals);
                                          //                                                   }
                                          //                                                 });
                                          //                                               } //onClickAddToCart(context,_item,type_offer),
                                          //                                             }
                                          //                                         ),
                                          //                                       )
                                          //                                   ),
                                          //                                 ],
                                          //                               ),
                                          //                             )
                                          //                         ):(
                                          //                             Container()
                                          //                         ),
                                          //                       ],
                                          //                     ), //Category title
                                          //
                                          //                     SystemControls.cardViewItemNumber==2?(
                                          //                         Padding(padding: EdgeInsets.all(4))
                                          //                     ):(
                                          //                         Container()
                                          //                     ),
                                          //                     SystemControls.cardViewItemNumber==2?(
                                          //                         Row(
                                          //                           mainAxisSize: MainAxisSize.max,
                                          //                           mainAxisAlignment: MainAxisAlignment.center,
                                          //                           children: <Widget>[
                                          //                             Expanded(
                                          //                               flex: 3,
                                          //                               child: Column(
                                          //                                 crossAxisAlignment: CrossAxisAlignment.start,
                                          //                                 mainAxisAlignment: MainAxisAlignment.center,
                                          //                                 children: <Widget>[
                                          //                                   Text(_itemsList[index][typeText+'calories'].toString()=='null'? ""
                                          //                                       :_itemsList[index][typeText+'calories'].toString()
                                          //                                       + "calories".tr().toString(),
                                          //                                     style: TextStyle(
                                          //                                       fontFamily: Lang,
                                          //                                       fontSize: SystemControls.font4,
                                          //                                       color: globals.accountColorsData['TextdetailsColor']!=null
                                          //                                           ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                          //                                       fontWeight: FontWeight.normal,
                                          //                                       height: 0.8,
                                          //                                     ),
                                          //                                     maxLines: 1,
                                          //                                     textAlign: TextAlign.start,
                                          //                                   ),
                                          //                                 ],
                                          //                               ),
                                          //                             ),
                                          //                             Expanded(
                                          //                               flex: 5,
                                          //                               child: Row(
                                          //                                 crossAxisAlignment: CrossAxisAlignment.end,
                                          //                                 mainAxisAlignment: MainAxisAlignment.end,
                                          //                                 children: <Widget>[
                                          //                                   type_offer?(
                                          //                                       Container()
                                          //                                   ):(
                                          //                                       Container(
                                          //                                         decoration: SystemControls().mealButtonDecoration(),
                                          //                                         height: 40,
                                          //                                         width: 40,
                                          //                                         child: InkWell(
                                          //                                           onTap: ()async{
                                          //                                             if(Token == ""){
                                          //                                               ErrorDialogAlertGoTOLogin(context,
                                          //                                                   "mustLogin".tr().toString(),Lang);
                                          //                                             }
                                          //                                             else{
                                          //                                               var res = await updateFavsItem(
                                          //                                                   Lang, Token,_itemsList[index][typeText+'id']);
                                          //                                               if(res.status ==200){
                                          //                                                 print("get login info ++ "+ res.message);
                                          //                                                 SuccDialogAlertStaySameLayout(context, res.message);
                                          //                                                 setState(() {
                                          //                                                   if(_itemsList[index]['is_fav']==1){
                                          //                                                     _itemsList[index]['is_fav'] = 0;
                                          //                                                   }
                                          //                                                   else{
                                          //                                                     _itemsList[index]['is_fav'] = 1;
                                          //                                                   }
                                          //                                                 });
                                          //                                               }
                                          //                                               else if(res.status == 401){
                                          //                                                 SystemControls().LogoutSetUserData(context, Lang);
                                          //                                                 ErrorDialogAlertBackHome(context, res.message);
                                          //                                               }
                                          //                                               else{
                                          //                                                 ErrorDialogAlert(context, res.message);
                                          //                                               }
                                          //                                             }
                                          //                                           },//onClickFavButton,
                                          //                                           child: Row(
                                          //                                             mainAxisAlignment: MainAxisAlignment.center,
                                          //                                             children: <Widget>[
                                          //                                               _itemsList[index]['is_fav']==0?(
                                          //                                                   SystemControls().GetSVGImagesAsset(
                                          //                                                       "assets/Icons/heart.svg",
                                          //                                                       25,
                                          //                                                       null)
                                          //                                               ):(
                                          //                                                   SystemControls().GetSVGImagesAsset(
                                          //                                                       "assets/Icons/heart.svg",
                                          //                                                       25,
                                          //                                                       globals.accountColorsData['MainColor']!=null
                                          //                                                           ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz)
                                          //                                               ),
                                          //                                             ],
                                          //                                           ),
                                          //                                         ),
                                          //                                       )
                                          //                                   ),
                                          //                                   Padding(padding: EdgeInsets.all(4)),
                                          //                                   //Remove Add to cart button if price zero or null
                                          //                                   _itemsList[index][typeText+'price']=="0"||
                                          //                                       _itemsList[index][typeText+'price']==null?(
                                          //                                       Container()
                                          //                                   ):(
                                          //                                       Container(
                                          //                                         decoration: SystemControls().mealButtonDecoration(),
                                          //                                         height: 40,
                                          //                                         width: 40,
                                          //                                         child: InkWell(
                                          //                                             child: Row(
                                          //                                               mainAxisAlignment: MainAxisAlignment.center,
                                          //                                               children: <Widget>[
                                          //                                                 //ToDo if _items[index][typeText+'available'] == null or != "1"
                                          //                                                 //Show othr icon with line
                                          //                                                 SystemControls().GetSVGImagesAsset(
                                          //                                                     "assets/Icons/cart.svg",
                                          //                                                     25,
                                          //                                                     Colors.black),
                                          //                                               ],
                                          //                                             ),
                                          //                                             onTap: () {
                                          //                                               if (type_offer//_items[index][typeText+'price']!="0"
                                          //                                                   || _itemsList[index][typeText+'available']=="1")
                                          //                                               {
                                          //                                                 setState(() {
                                          //                                                   print(
                                          //                                                       "add to cart");
                                          //                                                   int sear;
                                          //                                                   if (type_offer) {
                                          //                                                     sear =
                                          //                                                         FindMealAtCart(
                                          //                                                             _itemsList[index][typeText +
                                          //                                                                 'id'],
                                          //                                                             'offer');
                                          //                                                   }
                                          //                                                   else {
                                          //                                                     sear =
                                          //                                                         FindMealAtCart(
                                          //                                                             _itemsList[index][typeText +
                                          //                                                                 'id'],
                                          //                                                             'product');
                                          //                                                   }
                                          //                                                   //TODO
                                          //                                                   if (true) { //sear == -1
                                          //                                                     dynamic AddMeal = CartMeal();
                                          //                                                     AddMeal
                                          //                                                         .meals_id =
                                          //                                                     _itemsList[index][typeText +
                                          //                                                         'id'];
                                          //                                                     AddMeal
                                          //                                                         .meals_title =
                                          //                                                     _itemsList[index][typeText +
                                          //                                                         'title'];
                                          //                                                     AddMeal
                                          //                                                         .meals_img =
                                          //                                                         imgURL;
                                          //                                                     AddMeal
                                          //                                                         .meals_desc =
                                          //                                                     _itemsList[index][typeText +
                                          //                                                         'desc'];
                                          //                                                     if (findOfferPrice) {
                                          //                                                       AddMeal
                                          //                                                           .meals_price =
                                          //                                                           _itemsList[index][typeText +
                                          //                                                               'price_after_sale'].toString();
                                          //                                                     }
                                          //                                                     else {
                                          //                                                       AddMeal
                                          //                                                           .meals_price =
                                          //                                                           _itemsList[index][typeText +
                                          //                                                               'price'].toString();
                                          //                                                     }
                                          //                                                     if (type_offer) {
                                          //                                                       AddMeal
                                          //                                                           .type =
                                          //                                                       "offer";
                                          //                                                       AddMeal
                                          //                                                           .follow_delivery_time =
                                          //                                                       "1";
                                          //                                                     }
                                          //                                                     else {
                                          //                                                       AddMeal
                                          //                                                           .follow_delivery_time =
                                          //                                                       _itemsList[index]['category']['categories_follow_delivery_time'];
                                          //                                                       AddMeal
                                          //                                                           .type =
                                          //                                                       "product";
                                          //                                                     }
                                          //                                                     AddMeal
                                          //                                                         .count =
                                          //                                                     1;
                                          //
                                          //                                                     dynamic obj = AddMeal
                                          //                                                         .toJson();
                                          //                                                     write_to_file(
                                          //                                                         obj);
                                          //                                                     setState(() {
                                          //                                                       _CartMeals
                                          //                                                           .add(
                                          //                                                           obj);
                                          //                                                     });
                                          //
                                          //                                                     SuccDialogAlertStaySameLayout(
                                          //                                                         context,
                                          //                                                         AppLocalizations
                                          //                                                             .of(
                                          //                                                             context)
                                          //                                                             .translate(
                                          //                                                             'orderAddtoCart'));
                                          //                                                   }
                                          //                                                   else {
                                          //                                                     ErrorDialogAlert(
                                          //                                                         context,
                                          //                                                         AppLocalizations
                                          //                                                             .of(
                                          //                                                             context)
                                          //                                                             .translate(
                                          //                                                             'mealAleadyAdded'));
                                          //                                                     //TODO if want to increase meal count
                                          //                                                     //_CartMeals[sear]['count']++;
                                          //                                                     //await UpdateList(_CartMeals);
                                          //                                                   }
                                          //                                                 });
                                          //                                               } //onClickAddToCart(context,_item,type_offer),
                                          //                                             }
                                          //                                         ),
                                          //                                       )
                                          //                                   ),
                                          //                                 ],
                                          //                               ),
                                          //                             ),
                                          //                           ],
                                          //                         )
                                          //                     ):(
                                          //                         Container()
                                          //                     ),
                                          //                   ],
                                          //                 ),
                                          //               ),
                                          //               flex: 4,
                                          //             )
                                          //           ],
                                          //         ),
                                          //       ),
                                          //     ),
                                          //   ),
                                          // );
                                        },
                                      ),
                                    );
                                  }}
                                   if (snap.hasData ) {

                                    if((snap.data.data["products"] != null &&snap.data.data["products"].length<1 )||(snap.data.data["offers"] != null && snap.data.data["offers"].length<1)){
                                      noData = true;
                                    return Container(
                                      height: 200,
                                      child: Center(
                                        child: Text('noItems'.tr().toString(),
                                          style: TextStyle(
                                            fontSize: SystemControls.font3,
                                            fontFamily: Lang,
                                            color: globals.accountColorsData['TextHeaderColor']!=null
                                                ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                            fontWeight: FontWeight.bold,
                                            height: 0.9,
                                          ),
                                        ),
                                      ),
                                    );
                                  }}
                                  else if (snap.hasError) {
                                    return Center(
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text("respError".tr().toString(),
                                            style: TextStyle(
                                              fontFamily: Lang,
                                              fontWeight: FontWeight.normal,
                                              fontSize: SystemControls.font2,
                                              height: 1.2,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 10),
                                            width: 150,
                                            decoration: BoxDecoration(
                                              color: globals.accountColorsData['MainColor']!=null
                                                  ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                                              borderRadius: BorderRadius.all(Radius.circular(
                                                  SystemControls.RadiusCircValue
                                              )),
                                            ),
                                            child: FlatButton(
                                              onPressed: () {
                                                print("Reload");
                                                setState(() {
                                                  _itemsList.clear();
                                                  StartLoad = false;
                                                  stopRequest = false;
                                                  if(selectedCatID == -2){
                                                    _categoryMeals = getOffersListApi(Lang);
                                                  }
                                                  else {
                                                    _categoryMeals = CategoryListApi(Lang, selectedCatID,Token,currentPageindex.toString());
                                                  }
                                                });
                                              },
                                              child: Center(
                                                child: Text("reload".tr().toString(),
                                                    style: TextStyle(
                                                      fontFamily: Lang,
                                                      fontWeight: FontWeight.bold,
                                                      fontSize: SystemControls.font2,
                                                    )
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  }
                                  return Center(
                                    child: SystemControls().circularProgress(),
                                  );
                                }
                            ),
                          ),
                       selectedCatID ==-2 ?   Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text('discount'.tr().toString(),
                              style: TextStyle(
                                fontSize: SystemControls.font3,
                                fontFamily: Lang,
                                color: globals.accountColorsData['TextHeaderColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                fontWeight: FontWeight.bold,
                                height: 0.9,
                              ),
                            ),
                          ):Container(),
                          selectedCatID ==-2 ?  Container(
                            width: MediaQuery.of(context).size.width,
                            child: Consumer<DiscountProvider>(
                              builder: (context,discount,_){
                                return StreamBuilder<ResponseApi>(
                                  stream: discount.streamDiscount,
                                  builder: (context, snap){
                                    if (snap.connectionState == ConnectionState.waiting && _meals.isEmpty) {
                                      return Center(
                                        child: SystemControls().circularProgress(),
                                      );
                                    }
                                    else if(snap.hasData){
                                      // && snap.connectionState == ConnectionState.done


                                      homeReqmaxPageIndex = snap.data.data['pagination']['last_page'];


                                        if( snap.data.data['pagination']['current_page'] != homeReqPageindex) {

                                          print("Paggggggges");
                                          print(homeReqmaxPageIndex);
                                          print(homeReqPageindex);
                                          print(snap.data.data['pagination']['current_page']);
                                          print("Paggggggges");



                                        homeReqPageindex = snap.data.data['pagination']['current_page'];
                                        print("pagination after deone "+homeReqPageindex.toString());


                                        _meals += snap.data.data['products'];
                                        print("pagination " + _meals.length.toString());

                                        _meals.toSet().toList();
                                      }
                                      _meals.toSet().toList();
                                      return HomeItemsList(_meals, _CartMeals, Lang,
                                          Token, AppCurrency,design_Control,hasRegistration,
                                          onAddCartSubmit,onSeaFavSubmit);
                                      /*else {
                                    return HomeItemsList(
                                        _meals,
                                        _cartList,
                                        Lang,
                                        UserToken,
                                        AppCurrency,
                                        design_Control,
                                        hasRegistration,
                                        onAddCartSubmit,
                                        onSeaFavSubmit);
                                  }*/
                                    }
/*
                                else if(_meals.isNotEmpty){
                                  return HomeItemsList(_meals, _cartList, Lang,
                                      UserToken, AppCurrency,design_Control,hasRegistration,
                                      onAddCartSubmit,onSeaFavSubmit);
                                }*/

                                    return Container();
                                  },
                                );
                              },
                            )
                            /*child: HomeItemsList(_meals, _cartList, Lang,
                                UserToken, AppCurrency,design_Control,hasRegistration,
                                onAddCartSubmit,onSeaFavSubmit),*/
                          ):Container(),
                          SizedBox(height: 15,),

                          (StartLoad)? Container(
                            padding: EdgeInsets.all(20),
                            child: Center(
                              child: SystemControls().circularProgress(),
                            ),
                          )
                              : noData ==true ? Container(
                              padding: EdgeInsets.all(20),
                              child: (currentPageindex<maxPageIndex)?
                              InkWell(
                                onTap: (){
                                  if(selectedCatID != -2) {

                                    if(currentPageindex < maxPageIndex&& stopRequest == false){
                                      setState(() {
                                        //print("load new page ..... \n\n"+currentPageindex.toString());
                                        StartLoad = true;
                                        stopRequest = true;
                                        _categoryMeals = CategoryListApi(Lang, selectedCatID,Token,(currentPageindex+1).toString());
                                      });
                                    }
                                    else if(currentPageindex == maxPageIndex && StartLoad==true){
                                      setState(() {
                                        StartLoad = false;
                                      });
                                    }
                                  }
                                },
                                child: Center(
                                  child:  Text('loadMore'.tr().toString(),
                                    style: TextStyle(
                                      fontSize: SystemControls.font3,
                                      fontFamily: Lang,
                                      color: globals.accountColorsData['TextHeaderColor']!=null
                                          ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                      fontWeight: FontWeight.bold,
                                      height: 0.9,
                                    ),
                                  )
                                ),
                              )
                                  :Container(),
                          ):Container(),
                        ]),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }
  }
  final ValueNotifier<double> notifier = ValueNotifier(0);
  double latestValue = 0;
  double marg = 60;

  Future<void> showBottomSelectQuantity(BuildContext context,
      dynamic StartVal, int ItemIndex)
  {
    List<dynamic> _choices = <dynamic>[];
    dynamic currentQuantity = StartVal;
    while(currentQuantity<=10){
      _choices.add(currentQuantity);
      currentQuantity +=StartVal;
    }
    print(_choices.length.toString());
    return showModalBottomSheet(
        context: context,
        isScrollControlled: false,
        builder: (context) {
          return ConstrainedBox(
            constraints: new BoxConstraints(
              minHeight: MediaQuery.of(context).size.height*0.2,
              maxHeight: MediaQuery.of(context).size.height*0.4,
            ),
            child: Container(
              color: Colors.white,
              child: Container(
                child: Stack(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.topCenter,
                      padding: EdgeInsets.all(15),
                      child: Text('selectQuantity'.tr().toString(),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.bold,
                          fontSize: SystemControls.font3,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                          height: 1.2,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      alignment: Alignment.topRight,
                      padding: EdgeInsets.all(0),
                      child: IconButton(
                        icon: Icon(Icons.close,color: Colors.black,),
                        onPressed: (){
                          Navigator.pop(context);
                        },),
                    ),
                    DraggableScrollableSheet(
                      initialChildSize: 1,
                      minChildSize: 0.8,
                      builder: (context, ScrollController scrollController) {
                        return Container(
                          color: Colors.white,
                          margin: EdgeInsets.only(top: 50),
                          child: ListView.builder(
                            controller: scrollController,
                            itemCount: _choices.length,
                            itemBuilder: (BuildContext context, int index) {
                              return ListTile(
                                title: RichText(
                                  textAlign: TextAlign.start,
                                  text: new TextSpan(
                                    children: <TextSpan>[
                                      TextSpan(
                                        text: _choices[index].toString(),
                                        style: TextStyle(
                                          fontSize: SystemControls.font3,
                                          fontFamily: Lang,
                                          color: globals.accountColorsData['TextHeaderColor']!=null
                                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                          fontWeight: FontWeight.bold,
                                          height: 0.9,
                                        ),
                                      ),
                                      TextSpan(
                                        text: "  ",
                                        style: TextStyle(
                                          fontSize: SystemControls.font3,
                                          fontFamily: Lang,
                                          color: globals.accountColorsData['TextHeaderColor']!=null
                                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                          fontWeight: FontWeight.bold,
                                          height: 0.9,
                                        ),
                                      ),
                                      TextSpan(
                                        text: _itemsList[ItemIndex]['products_type']=='weight'?
                                        'KG'.tr().toString()
                                            :'piece'.tr().toString(),
                                        style: TextStyle(
                                          fontSize: SystemControls.font3,
                                          fontFamily: Lang,
                                          color: globals.accountColorsData['TextHeaderColor']!=null
                                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                          fontWeight: FontWeight.bold,
                                          height: 0.9,
                                        ),
                                      ),
                                    ],
                                  ),
                                  maxLines: 1,
                                ),
                                onTap: (){
                                  setState(() {
                                    _itemsList[ItemIndex]['quantity'] = _choices[index];
                                    print(_itemsList[ItemIndex]['quantity'].toString());
                                    Navigator.pop(context);

                                  });
                                },
                              );
                            },
                          ),
                        );
                      },
                    ),
                  ],
                ),
                decoration: BoxDecoration(
                  color: Theme.of(context).canvasColor,
                  borderRadius: BorderRadius.only(
                    topLeft: const Radius.circular(12),
                    topRight: const Radius.circular(12),
                  ),
                ),
              ),
            ),
          );
          return DraggableScrollableSheet(
            initialChildSize: 1,
            minChildSize: 0.8,
            builder: (context, ScrollController scrollController) {
              return Container(
                color: Colors.blue[100],
                margin: EdgeInsets.only(top: 50),
                child: ListView.builder(
                  controller: scrollController,
                  itemCount: 25,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(title: Text('Item $index'));
                  },
                ),
              );
            },
          );
        });
  }

  void _onItemTapped(int index) {
    setState(() {
      _itemsList.clear();
      StartLoad = false;
      stopRequest =false;
      currentPageindex=0;
      selectedCatID = index;
      print(index);
      print(index);
      //-2 means offer any other number is category id
      if(selectedCatID == -2){
        _categoryMeals = getOffersListApi(Lang);
      }
      else {
        _categoryMeals = CategoryListApi(Lang, selectedCatID,Token,currentPageindex.toString());
      }
    });
  }

  OnCardItemAddToCart(BuildContext context,dynamic _meal,bool type_offer){
    setState(() {
      print("from fun Add to cart ??");
      int sear;
      String typeText;
      String imgURL;
      if(type_offer){
        typeText = "offers_";
        sear = FindMealAtCart(_meal[typeText+'_id'],'offer');
        imgURL = _meal['offers_img_mobile'];
      }
      else{
        typeText = "meals_";
        sear = FindMealAtCart(_meal[typeText+'_id'],'meal');
        imgURL = _meal[typeText+'img'];
      }
      //TODO Remove it to add all items already at the cart
      if(true) {//sear == -1
        dynamic AddMeal = CartMeal();
        AddMeal.meals_id =
        _meal[typeText+'_id'];
        AddMeal.meals_title =
        _meal[typeText+'title'];
        AddMeal.meals_img = imgURL;
        AddMeal.meals_desc =
        _meal[typeText+'desc'];
        AddMeal.meals_price =
        _meal[typeText+'price'].toString();
        if(type_offer){
          AddMeal.follow_delivery_time = "1";
          AddMeal.type = "offer";
        }
        else {
          AddMeal.follow_delivery_time =
          _meal['category']['categories_follow_delivery_time'];
          AddMeal.type = "meal";
        }
        AddMeal.count = 1;

        AddMeal.quantityType =
        _meal['products_type'];//Product follow weight or num
        AddMeal.minQuantity =
        _meal['products_lowest_weight_available'];


        dynamic obj = AddMeal.toJson();
        write_to_file(obj);
        _CartMeals.add(obj);

        SuccDialogAlertStaySameLayout(context,
           'orderAddtoCart'.tr().toString(),ValueKey("categoryList5"));
      }
      else{
      //   ErrorDialogAlert(context,
      //       AppLocalizations.of(context).translate('mealAleadyAdded'));
      //   //TODO if want to increase meal count
      //   //_CartMeals[sear]['count']++;
      //   //await UpdateList(_CartMeals);
      }
    }
      );
  }

  OnCardItemClickFavButton(BuildContext context, int itemId)async{
    if(Token == ""){
      ErrorDialogAlertGoTOLogin(context,
          "mustLogin".tr().toString(),Lang);
    }
    else{
      var res = await updateFavsItem(Lang, Token,itemId,context);
      if(res.status ==200){
        print("get login info ++ "+ res.message);
        SuccDialogAlertStaySameLayout(context, res.message,ValueKey("categoryList4"));
      }
      else if(res.status == 401){
        SystemControls().LogoutSetUserData(context, Lang);
        ErrorDialogAlertBackHome(context, res.message);
      }
      else{
        ErrorDialogAlert(context, res.message);
      }
    }
  }

  void updateText(int homeInd) {
    setState(() {
      _selectedIndex = homeInd;
    });
  }
  ///Search App bar
  AnimationController _controller;
  Animation _animation;
  bool isInSearchMode = false;
  bool showSearchCat = true;
  String searchText = "";
  int searchCatId = 0;
  String searchCatName = "";
  int _selectedIndex = 0;
  ScrollController _searchScrollController;
  _searchScrollListener() {
    if(_selectedIndex == 10) {//search
      if(_searchScrollController.position.pixels==_searchScrollController.position.maxScrollExtent
          && currentPageindex < maxPageIndex){
        currentPageindex +=1;
        setState(() {
          print("load new page ..... \n\n"+currentPageindex.toString());
          StartLoad = true;
          stopRequest = true;
          if(showSearchCat == false) {
            resSearch = SearchWithtitleApi(
                Lang, searchText, Token, currentPageindex.toString());
          }
          else {
            resSearch = CategoryListApi(Lang, searchCatId, Token,currentPageindex.toString());
          }
        });
      }
      else if(currentPageindex == maxPageIndex && StartLoad==true){
        setState(() {
          StartLoad = false;
        });
      }
    }
  }

  animationStatusListener(AnimationStatus animationStatus) {
    if (animationStatus == AnimationStatus.completed) {
      setState(() {
        isInSearchMode = true;
      });
    }
  }
  void onSearchTapUp(TapUpDetails details) {
    _controller.forward();
  }
  cancelSearch() {
    setState(() {
      StartLoad = false;
      stopRequest = true;

      isInSearchMode = false;
      showSearchCat = true;
      searchCatId = 0;
      _searchMeals.clear();
      _selectedIndex = 0;
      currentPageindex = 0;
      searchCatName = "";
      maxPageIndex=1;
    });
    _controller.reverse();
  }
  onSearchSubmit(String query){
    print("Submit This text to get search result $query");
    ///Set state change
    ///_selectedIndex value fore search 10
    ///if you want to change it go to  navigatorBody() fun and change it to
    setState(() {
      StartLoad = false;
      stopRequest = true;
      //StartLoad = false;
      currentPageindex = 0;
      maxPageIndex=1;
      showSearchCat = true;
      isInSearchMode = false;
      searchCatId = 0;

      _searchMeals.clear();
      searchText = query;
      searchCatName = "";
      _selectedIndex = 10;

      resSearch = SearchWithtitleApi(Lang, searchText, Token, "1");
    });
  }
  onSearchSelectCat(int CatID, String catNam){
    print("Submit This text to get search result with catID $CatID");
    setState(() {
      //StartLoad = false;
      currentPageindex = 0;
      maxPageIndex=1;
      showSearchCat = true;
      searchCatName = catNam;
      //resSearch = CategoryListApi(Lang, CatID, UserToken,"1");
      _searchMeals.clear();
      searchCatId = CatID;
      _selectedIndex = 10;

      isInSearchMode = false;
      resSearch = CategoryListApi(Lang, searchCatId, Token,"1");
    });
    _controller.reverse();
  }
  //////////////
  ///Internet Check area
  ConnectionStatusBar(){
    final Color color = Colors.redAccent;
    final double width = double.maxFinite;
    final double height = 30;

    if(_connectionStatus == 'connected'){
      return Container();
    }
    else{
      return Container(
        margin: EdgeInsets.only(bottom: 50),
        child: SafeArea(
          bottom: false,
          child: Container(
            color: color,
            width: width,
            height: height,
            child: Container(
              margin: EdgeInsets.only(right: 5,left: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Please check your internet connection',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'en',
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font4,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      InkWell(
                        child: Text(
                          'Try Again..',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'en',
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font4,
                          ),
                        ),
                        onTap: (){
                          setState(() async{
                            initConnectivity();
                          });
                        },
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      );
    }
  }
  String _connectionStatus = 'connected';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }
  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = "connected";
              _itemsList.clear();
              StartLoad = false;
              searchCaegories = getSearchCategories(Lang);
              if(selectedCatID == -2){
                _categoryMeals = getOffersListApi(Lang);
              }
              else {
                _categoryMeals = CategoryListApi(Lang, selectedCatID,Token,currentPageindex.toString());
              }
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = "failed";
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState((){
          _connectionStatus = "connected";
          _itemsList.clear();
          StartLoad = false;
          searchCaegories = getSearchCategories(Lang);
          if(selectedCatID == -2){
            _categoryMeals = getOffersListApi(Lang);
          }
          else {
            _categoryMeals = CategoryListApi(Lang, selectedCatID,Token,currentPageindex.toString());
          }
        });
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "failed");
        break;
      default:
        setState(() => _connectionStatus = 'failed');
        break;
    }
  }
}