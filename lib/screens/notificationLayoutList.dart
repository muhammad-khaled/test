import '../Models/NotificationModel.dart';
import '../Repo/GetNotification.dart';
import '../Repo/ReadNotificationRepo.dart';
import '../provider/NotificaionProvider.dart';
import 'package:flutter/material.dart';
import '../Controls.dart';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import '../dataControl/dataModule.dart';
import '../dataControl/userModule.dart';
import '../dataControl/cartModule.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';

import 'package:provider/provider.dart';
import '../provider/productsInCartProvider.dart';

import '../globals.dart' as globals;

class NotificationList extends StatefulWidget{
  String Lang;
  String Token;
  String AppCurrency;
  int design_Control;

  NotificationList(this.Lang, this.Token, this.AppCurrency,this.design_Control);

  _NotificationList createState() =>
      _NotificationList(this.Lang, this.Token, this.AppCurrency,this.design_Control);

}
class _NotificationList extends State<NotificationList>{

  ScrollController scrollController;
  @override
  void initState() {
    super.initState();
    print('sttttaaaaaarrrrrtttt');
    Provider.of<NotificationProvider>(context,listen: false).getNotificationList.drain();
    var notificationProvider = Provider.of<NotificationProvider>(context,listen: false);

    notificationProvider.fetchNotification(Lang, Token, context);

    // initConnectivity();
    // _connectivitySubscription =
    //     _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);

    scrollController = new ScrollController()..addListener(_scrollListener);


  }
  @override
  void didChangeDependencies() {

    // TODO: implement didChangeDependencies
    Future<String> future;
    Timer(Duration(milliseconds: 100), (){  future = ReadNotificationRepo(lang: Lang,token: Token).readNotificationRepo;

    Provider.of<NotificationProvider>(context,listen: false).fetchNotificationCounter(EasyLocalization.of(context).currentLocale.languageCode, "userToken", context);

    Provider.of<NotificationProvider>(context,listen: false).resetCounteer();

    });

    super.didChangeDependencies();
  }
  @override
  void dispose() {
    // _connectivitySubscription.cancel();
    scrollController.removeListener(_scrollListener);
    print('clllosses stream vvv');
    super.dispose();


  }
  Future<NotificationModel> loadMore;

  void _scrollListener() {
    var notificationProvider = Provider.of<NotificationProvider>(context,listen: false);

    print('Loading');
    print(scrollController.position.extentAfter);
    NotificationModel notificationModel = NotificationModel();
    if (scrollController.position.pixels == scrollController.position.maxScrollExtent) {
        loadMore = notificationProvider.urlLoadMore != null ? NotificationLoadRepo(url: "${notificationProvider.urlLoadMore}&accountId=${SystemControls.AccountId}",token: Token,context: context).loadMoreNotification: null;
    }
  }

  Future<ResponseApi> respNe;
  String Lang;
  String Token;
  String AppCurrency;
  int design_Control;
  _NotificationList(this.Lang, this.Token, this.AppCurrency,this.design_Control){
    respNe = getUserNotification(Lang,Token);
  }

  @override
  Widget build(BuildContext context) {
    var notificationProvider = Provider.of<NotificationProvider>(context,listen: false);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(icon: Icon(Icons.arrow_back_rounded),color: Colors.black,onPressed: () {
          // Provider.of<NotificationProvider>(context,listen: false).getNotificationList.close();

          Navigator.pop(context);
        },),
        title: Text("notification".tr().toString()),
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
        actions: <Widget>[
          SystemControls.designControl==1?
              AppBarHomeIConButton(context,Lang,"")
              :Container(),
        ],
      ),
      body: Stack(
        children: <Widget>[
          LayoutBG(design_Control),
          StreamBuilder<List<NotificationModel>>(
            stream: notificationProvider.streamNotification,
            builder: (context, snap){
              if (snap.connectionState == ConnectionState.waiting) {
                print('');
                return Center(
                  child: SystemControls().circularProgress(),
                );
              }
              if(snap.hasData){

                List<Notifications> list= [];
                 List<Notifications> mainList =[];
                if(list.length < snap.data.length){
                  list.clear();
                snap.data.forEach((element) {
                  element.data.notifications.forEach((notifications) {
                    list.add(notifications);

                  });

                   list.toSet().toList();

                  });}

                list.toSet().toList().forEach((element) {
                  mainList.add(element);
                });

                print(snap.data.length);
                return Stack(
                    children: <Widget>[
                      Container(
                        child: Container(
                          margin: EdgeInsets.all(10),
                          child: ListView.builder(
                            controller: scrollController,
                              scrollDirection: Axis.vertical,
                              itemCount: snap.data.length,
                              itemBuilder: (context, index) {

                                return InkWell(
                                  child: Container(

                                    height: 80,
                                    margin: EdgeInsets.all(10),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(SystemControls
                                              .RadiusCircValue)),
                                      child: Container(
                                        color: mainList[index].customers[0].readAt == null?   globals.accountColorsData['MainColor']!=null
                                            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz:Colors.grey.withOpacity(.5),
                                        child: Column(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 7,
                                              child: Container(
                                                margin: EdgeInsets.all(14),
                                                alignment: Alignment.center,
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Row(
                                                      mainAxisSize: MainAxisSize
                                                          .max,
                                                      mainAxisAlignment: MainAxisAlignment
                                                          .center,
                                                      children: <Widget>[
                                                        Expanded(
                                                          flex: 7,
                                                          child: Column(
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            children: <Widget>[
                                                              Text(mainList[index].notificationsText,
                                                                style: TextStyle(
                                                                  fontFamily: Lang,
                                                                  fontSize: SystemControls
                                                                      .font2,
                                                                  color: mainList[index].customers[0].readAt == null?  globals.accountColorsData['TextOnMainColor']!=null
                                                                      ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextHeaderColorz:globals.accountColorsData['TextHeaderColor']!=null
                                                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                                                  fontWeight: FontWeight
                                                                      .normal,
                                                                  height: 1,
                                                                ),
                                                                maxLines: 1,
                                                                textAlign: TextAlign
                                                                    .start,
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Padding(
                                                        padding: EdgeInsets
                                                            .all(5)),
                                                    Row(
                                                      mainAxisSize: MainAxisSize
                                                          .max,
                                                      mainAxisAlignment: MainAxisAlignment
                                                          .center,
                                                      children: <Widget>[
                                                        Expanded(
                                                          flex: 7,
                                                          child: Column(
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            children: <Widget>[
                                                              Text(mainList[index].notificationsCreatedAt,
                                                                style: TextStyle(
                                                                  fontFamily: Lang,
                                                                  fontSize: SystemControls
                                                                      .font4,
                                                                  color: mainList[index].customers[0].readAt == null?  globals.accountColorsData['TextOnMainColor']!=null
                                                                      ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextHeaderColorz:globals.accountColorsData['TextHeaderColor']!=null
                                                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,

                                                                  fontWeight: FontWeight
                                                                      .normal,
                                                                  height: 1,
                                                                ),
                                                                maxLines: 1,
                                                                textAlign: TextAlign
                                                                    .start,
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  onTap: () {

                                  },
                                );
                              }
                          ),
                        ),
                      ),
                    ],
                  );
                }

              else if(snap.hasError){
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("respError".tr().toString(),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font2,
                          height: 1.2,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 150,
                        decoration: BoxDecoration(
                          color: globals.accountColorsData['MainColor']!=null
                              ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                          borderRadius: BorderRadius.all(Radius.circular(
                              SystemControls.RadiusCircValue
                          )),
                        ),
                        child: FlatButton(
                          onPressed: () {
                            print("Reload");
                            setState(() {
                              respNe = getUserNotification(Lang,Token);
                            });
                          },
                          child: Center(
                            child: Text("reload".tr().toString(),
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.bold,
                                  fontSize: SystemControls.font2,
                                )
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              }
              return Center(
                child: SystemControls().circularProgress(),
              );
            },
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: ConnectionStatusBar(),
          ),
        ],
      ),
    );
  }

  //////////////
  ConnectionStatusBar(){
    final Color color = Colors.redAccent;
    final double width = double.maxFinite;
    final double height = 30;

    if(_connectionStatus == 'connected'){
      return Container();
    }
    else{
      return Container(
        child: SafeArea(
          bottom: false,
          child: Container(
            color: color,
            width: width,
            height: height,
            child: Container(
              margin: EdgeInsets.only(right: 5,left: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Please check your internet connection',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'en',
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font4,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      InkWell(
                        child: Text(
                          'Try Again..',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'en',
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font4,
                          ),
                        ),
                        onTap: (){
                          setState(() async{
                            initConnectivity();
                          });
                        },
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      );
    }
  }
  ///Internet Check area
  String _connectionStatus = 'connected';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }
  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = "connected";
              respNe = getUserNotification(Lang,Token);
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = "failed";
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState((){
          _connectionStatus = "connected";
          respNe = getUserNotification(Lang,Token);
        });
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "failed");
        break;
      default:
        setState(() => _connectionStatus = 'failed');
        break;
    }
  }

  ///////////////
  List<dynamic> _CartMeals = <dynamic>[];
  addOrderToCarty(List<dynamic> _OrderItems){
    _CartMeals.clear();
    for(int i=0;i<_OrderItems.length;i++){
      dynamic AddMeal = CartMeal();
      if(_OrderItems[i]['FK_products_id'] == null){
        dynamic offer = _OrderItems[i]['offer'];
        AddMeal.meals_id = offer['offers_id'];
        AddMeal.meals_title = offer['offers_title'];
        AddMeal.meals_img = offer['offers_img_mobile'];
        AddMeal.meals_desc = offer['offers_desc'];
        AddMeal.meals_price = offer['offers_price'];
        AddMeal.follow_delivery_time = "1";
        AddMeal.type = "offer";
        AddMeal.count = _OrderItems[i]['orders_items_count'];
      }
      else{
        dynamic meal = _OrderItems[i]['product'];
        AddMeal.quantityType = meal['products_type'];//Product follow weight or num
        AddMeal.minQuantity = meal['products_lowest_weight_available'];

        AddMeal.meals_id = meal['products_id'];
        AddMeal.meals_title = meal['products_title'];
        AddMeal.meals_img = meal['products_img'];
        AddMeal.meals_desc = meal['products_desc'];
        AddMeal.meals_price = meal['products_price'];
        AddMeal.follow_delivery_time =
            meal['category']['categories_follow_delivery_time'];
        AddMeal.type = "product";
        AddMeal.count = double.parse(_OrderItems[i]['orders_items_count']);
      }
      dynamic obj = AddMeal.toJson();
      _CartMeals.add(obj);
    }
  }

  Future<void> _ConfirmDialogAlert(BuildContext context,List<dynamic> _OrderItems,String imgName) {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(
                  SystemControls.RadiusCircValue)
          ),
          title: Text("reorderConfirmMSG".tr().toString(),
            style: TextStyle(
                fontFamily: Lang,
                fontWeight: FontWeight.bold,
                fontSize: SystemControls.font3,
                color: globals.accountColorsData['TextHeaderColor']!=null
                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
            ),
            textAlign: TextAlign.center,
          ),
          contentPadding: EdgeInsets.only(top: 40,bottom: 0),
          content: ClipRRect(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
              bottomRight: Radius.circular(SystemControls.RadiusCircValue),
            ),
            child: Container(
              height: 40,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Container(
                      decoration: BoxDecoration(
                        color: globals.accountColorsData['MainColor']!=null
                            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                        border: Border(
                          top: BorderSide(color: Colors.black),
                        ),
                      ),
                      child: FlatButton(
                        child: Text("orderBut".tr().toString(),
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData['TextOnMainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                          ),
                        ),
                        onPressed: () async{
                          await clear_cart_file();
                          addOrderToCarty(_OrderItems);
                          UpdateList(_CartMeals);

                          CartStateProvider cartState = Provider.of<CartStateProvider>(context,listen: false);
                          cartState.setCurrentProductsCount(_CartMeals.length.toString());
                          cartState.setCurrentProductsCount(_CartMeals.length.toString());
                          if(imgName != null) {
                            cartState.setCartImgName(imgName);
                          }

                          Navigator.of(context).pop();

                          SuccDialogAlertGoCart(this.context,
                              'orderAddtoCart'.tr().toString(),
                              Lang,Token,AppCurrency,design_Control);

                        },
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.black,
                    width: 1,
                    height: 40,
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      decoration: BoxDecoration(
                        color: globals.accountColorsData['MainColor']!=null
                            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                        border: Border(
                          top: BorderSide(color: Colors.black),
                        ),
                        //borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: FlatButton(
                        child: Text("cancel".tr().toString(),
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData['TextOnMainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                          ),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}