import '../locale/share_preferences_con.dart';
import 'package:flutter/material.dart';
import '../Controls.dart';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import '../screens/home.dart';
import '../widgets/homeItemsList.dart';
import '../dataControl/favsModule.dart';
import '../dataControl/dataModule.dart';
import '../dataControl/cartModule.dart';
import '../screens/orderDetails.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';
import 'package:sizer/sizer.dart';
import 'package:provider/provider.dart';
import '../provider/productsInCartProvider.dart';

import '../globals.dart' as globals;

class OrderList extends StatefulWidget{
  String Lang;
  String Token;
  String AppCurrency;
  int design_Control;

  OrderList(this.Lang, this.Token, this.AppCurrency,this.design_Control);

  _orderList createState() =>
      _orderList(this.Lang, this.Token, this.AppCurrency,this.design_Control);

}
class _orderList extends State<OrderList> with SharePreferenceData{
  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);

  }
  @override
  void dispose() {
    super.dispose();
    _connectivitySubscription.cancel();
  }

  Future<ResponseApi> resp;
  String Lang;
  String Token;
  String AppCurrency;
  int design_Control;
  _orderList(this.Lang, this.Token, this.AppCurrency,this.design_Control){
    resp = getUserOrder(Lang,Token);
  }

  String CheckText(String get){
    if(get != null)
      return get;
    else
      return " ";
  }
  String OrderStatusTex(String indStatus){
    if(indStatus == "0"){
      return "orderCancel".tr().toString();
    }
    else if(indStatus=="1"){
      return "orderNew".tr().toString();
    }
    else if(indStatus=="2"){
      return "orderConfirm".tr().toString();
    }
    else if(indStatus=="3"){
      return "orderOnWay".tr().toString();
    }
    else if(indStatus=="4"){
      return "orderDone".tr().toString();
    }
    return "";
  }
  Color OrderStatusColor(String indStatus){
    if(indStatus == "0"){
      return Colors.red;
    }
    else if(indStatus=="1"){
      return Colors.green;
    }
    else if(indStatus=="2"){
      return Colors.green;
    }
    else if(indStatus=="3"){
      return Colors.green;
    }
    else if(indStatus=="4"){
      return Colors.blue;
    }
    return globals.accountColorsData['TextdetailsColor']!=null
        ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz;
  }

  _getRequests()async{
    print("on back to order list 000000000");
    resp = getUserOrder(Lang,Token);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("orders".tr().toString()),
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
        actions: <Widget>[
          SystemControls.designControl==1?
              AppBarHomeIConButton(context,Lang,"")
              :Container(),
        ],
      ),
      body: Stack(
        children: <Widget>[
          LayoutBG(design_Control),
          FutureBuilder<ResponseApi>(
            future: resp,
            builder: (context, snap){
              if (snap.connectionState == ConnectionState.waiting) {
                return Center(
                  child: SystemControls().circularProgress(),
                );
              }
              if(snap.hasData){
                if(snap.data.status==200) {
                  List<dynamic> _orders = snap.data.data['orders'];
                  return _orders.length ==0 ? Container(
                    height: 50.h,
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                      child: Text("noItemOrder".tr().toString(),style: TextStyle(fontWeight: FontWeight.bold,color:  globals.accountColorsData[
                      'MainColor'] !=
                          null
                          ? Color(int.parse(
                          globals.accountColorsData[
                          'MainColor']))
                          : SystemControls.MainColorz,fontSize: 14.sp),textAlign: TextAlign.center,),
                    ),
                  ): Stack(
                    children: <Widget>[
                      Container(
                        child: Container(
                          margin: EdgeInsets.all(10),
                          child: ListView.builder(
                              scrollDirection: Axis.vertical,
                              itemCount: _orders.length,
                              itemBuilder: (context, index) {
                                var ZoneDifference = new DateTime.now().timeZoneOffset.inHours;
                                print("\n\n\n"+_orders[index]['orders_created_at']);
                                var createdTime = DateTime.parse(_orders[index]['orders_created_at']);
                                createdTime.add(Duration(hours: ZoneDifference));
                                var newTime = createdTime.add(Duration(hours: ZoneDifference));
                                print(newTime.toString());
                                print(createdTime.toString()+"\n\n\n\n");
                                return InkWell(
                                  child: Container(
                                    height: 180,
                                    margin: EdgeInsets.all(10),
                                     decoration: BoxDecoration(
                                       borderRadius: BorderRadius.circular(10),
                                       border: Border.all(color:  globals.accountColorsData['MainColorOpacity']!=null
                                ? Color(int.parse(globals.accountColorsData['MainColorOpacity'])) : SystemControls.MainColorOpacityz,

                                )),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(SystemControls
                                              .RadiusCircValue)),
                                      child: Container(
                                        color: Colors.white,
                                        child: Column(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 7,
                                              child: Container(
                                                margin: EdgeInsets.all(14),
                                                alignment: Alignment.center,
                                                child: Column(
                                                  children: <Widget>[
                                                    Row(
                                                      mainAxisSize: MainAxisSize
                                                          .max,
                                                      mainAxisAlignment: MainAxisAlignment
                                                          .center,
                                                      children: <Widget>[
                                                        Expanded(
                                                          flex: 7,
                                                          child: Column(
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            children: <Widget>[
                                                              Row(
                                                                mainAxisAlignment: MainAxisAlignment
                                                                    .start,
                                                                crossAxisAlignment: CrossAxisAlignment
                                                                    .center,
                                                                children: <Widget>[
                                                                  Icon(
                                                                    Icons.access_time,
                                                                    // color: globals.accountColorsData['TextdetailsColor']!=null
                                                                    //     ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                                    color: Colors.blueGrey,
                                                                    size: 20,
                                                                  ),
                                                                  Padding(
                                                                      padding: EdgeInsets
                                                                          .all(2)),
                                                                  Text(newTime.toString().substring(0,19),
                                                                    style: TextStyle(
                                                                      fontFamily: Lang,
                                                                      fontSize: SystemControls
                                                                          .font4,
                                                                      color: globals.accountColorsData['TextdetailsColor']!=null
                                                                          ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                                      fontWeight: FontWeight
                                                                          .normal,
                                                                      height: 0.9,
                                                                    ),
                                                                    maxLines: 1,
                                                                    textAlign: TextAlign
                                                                        .start,
                                                                  ),
                                                                ],
                                                              ),

                                                            ],
                                                          ),
                                                        ),
                                                        RichText(
                                                          text: new TextSpan(
                                                            text: '${"paymentStatus".tr().toString()}: ',
                                                            style: TextStyle(
                                                              fontFamily: Lang,
                                                              fontSize: SystemControls
                                                                  .font4,
                                                               color: Colors.black,
                                                              fontWeight: FontWeight
                                                                  .normal,
                                                              height: 0.9,
                                                            ),
                                                            children: <TextSpan>[
                                                              new TextSpan(
                                                                  text: '${_orders[index]['orders_payment_status']=="3" ?"waiting".tr().toString():_orders[index]['orders_payment_status']=="2"?"success".tr().toString():"failed".tr().toString()}',
                                                                  style: TextStyle(
                                                                    fontFamily: Lang,
                                                                    fontSize: SystemControls
                                                                        .font4,
                                                                    color: _orders[index]['orders_payment_status']=="3" ?Colors.blue:_orders[index]['orders_payment_status']=="2"?Colors.green:Colors.red,
                                                                    fontWeight: FontWeight
                                                                        .bold,
                                                                    height: 0.9,
                                                                  ),

                                                              )],
                                                          ),
                                                        ),

                                                      ],
                                                    ),
                                                    Padding(
                                                        padding: EdgeInsets
                                                            .all(10)),
                                                    Row(
                                                      children: [
                                                        Text("ordernum".tr().toString() + " " + _orders[index]['orders_id'].toString(),
                                                          style: TextStyle(
                                                            fontFamily: Lang,
                                                            fontSize: SystemControls
                                                                .font3,
                                                            color: globals.accountColorsData['TextHeaderColor']!=null
                                                                ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                                            fontWeight: FontWeight
                                                                .normal,
                                                            height: 1,
                                                          ),
                                                          maxLines: 1,
                                                          textAlign: TextAlign
                                                              .start,
                                                        ),
                                                      ],
                                                    ),
                                                    Padding(
                                                        padding: EdgeInsets
                                                            .all(10)),
                                                    Row(
                                                      mainAxisSize: MainAxisSize
                                                          .max,
                                                      mainAxisAlignment: MainAxisAlignment
                                                          .center,
                                                      children: <Widget>[
                                                        Expanded(
                                                          flex: 5,
                                                          child: Column(
                                                            crossAxisAlignment: CrossAxisAlignment
                                                                .start,
                                                            mainAxisAlignment: MainAxisAlignment
                                                                .center,
                                                            children: <Widget>[
                                                              Text(
                                                                OrderStatusTex(
                                                                    _orders[index]['orders_status']),
                                                                style: TextStyle(
                                                                  fontFamily: Lang,
                                                                  fontSize: SystemControls
                                                                      .font4,
                                                                  color: OrderStatusColor(
                                                                      _orders[index]['orders_status']),
                                                                  fontWeight: FontWeight
                                                                      .normal,
                                                                  height: 0.9,
                                                                ),
                                                                maxLines: 1,
                                                                textAlign: TextAlign
                                                                    .start,
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Expanded(
                                                          flex: 5,
                                                          child: Column(
                                                            crossAxisAlignment: CrossAxisAlignment
                                                                .end,
                                                            mainAxisAlignment: MainAxisAlignment
                                                                .center,
                                                            children: <
                                                                Widget>[
                                                              Text(
                                                                _orders[index]['orders_final_cost'] +
                                                                    " " +
                                                                    AppCurrency,
                                                                style: TextStyle(
                                                                  fontFamily: Lang,
                                                                  fontSize: SystemControls
                                                                      .font4,
                                                                  color: globals.accountColorsData['TextdetailsColor']!=null
                                                                      ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                                                                  fontWeight: FontWeight
                                                                      .normal,
                                                                  height: 0.9,
                                                                ),
                                                                maxLines: 1,
                                                                textAlign: TextAlign
                                                                    .start,
                                                              ),
                                                            ],
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),

                                            Expanded(
                                              flex: 3,
                                              child: Container(
                                                //margin: EdgeInsets.only(top: 5),
                                                color: globals.accountColorsData['MainColorOpacity']!=null
                                                    ? Color(int.parse(globals.accountColorsData['MainColorOpacity'])) : SystemControls.MainColorOpacityz,
                                                child: InkWell(
                                                  onTap: () async {
                                                    SystemControls().storeDataToPreferences("lang", "$Lang");

                                                    _ConfirmDialogAlert(
                                                        context,
                                                        _orders[index]['items'],
                                                        _orders[index]['orders_img'],Lang);
                                                  },
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment
                                                        .center,
                                                    children: <Widget>[
                                                      Icon(
                                                        Icons.repeat,
                                                        color: globals.accountColorsData['TextOnMainColor']!=null
                                                            ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                                                        size: 20,
                                                      ),
                                                      Padding(
                                                          padding: EdgeInsets
                                                              .all(5)),
                                                      Text(

                                                            "reorder".tr().toString(),
                                                        style: TextStyle(
                                                          fontFamily: Lang,
                                                          fontWeight: FontWeight.normal,
                                                          fontSize: SystemControls.font3,
                                                          color: globals.accountColorsData['TextOnMainColor']!=null
                                                              ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                                                          height: 1,
                                                        ),
                                                        maxLines: 1,
                                                        textAlign: TextAlign
                                                            .start,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),

                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  onTap: () {
                                    Navigator.push(context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                OrderDetails(
                                                    Lang, AppCurrency,
                                                    _orders[index],
                                                    design_Control,
                                                    Token,0)
                                        )).then(
                                            (val)=>val?_getRequests():null
                                    );
                                  },
                                );
                              }
                          ),
                        ),
                      ),
                    ],
                  );
                }
                else if(snap.data.status==401){
                  SystemControls().LogoutSetUserData(context, Lang);
                  Timer(Duration(seconds: 1), ()=>
                      ErrorDialogAlertBackHome(context, snap.data.message)
                  );
                  return Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(snap.data.message,
                          style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font2,
                            height: 1.2,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  );
                }
                return Container();
              }
              else if(snap.hasError){
                print(snap.error);
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("respError".tr().toString(),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font2,
                          height: 1.2,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        width: 150,
                        decoration: BoxDecoration(
                          color: globals.accountColorsData['MainColor']!=null
                              ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                          borderRadius: BorderRadius.all(Radius.circular(
                              SystemControls.RadiusCircValue
                          )),
                        ),
                        child: FlatButton(
                          onPressed: () {
                            print("    Reload");
                            setState(() {
                              resp = getUserOrder(Lang,Token);
                            });
                          },
                          child: Center(
                            child: Text("reload.tr().toString()",
                                style: TextStyle(
                                  fontFamily: Lang,
                                  fontWeight: FontWeight.bold,
                                  fontSize: SystemControls.font2,
                                )
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              }
              return Center(
                child: SystemControls().circularProgress(),
              );
            },
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: ConnectionStatusBar(),
          ),
        ],
      ),
    );
  }

  //////////////
  ConnectionStatusBar(){
    final Color color = Colors.redAccent;
    final double width = double.maxFinite;
    final double height = 30;

    if(_connectionStatus == 'connected'){
      return Container();
    }
    else{
      return Container(
        child: SafeArea(
          bottom: false,
          child: Container(
            color: color,
            width: width,
            height: height,
            child: Container(
              margin: EdgeInsets.only(right: 5,left: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Please check your internet connection',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'en',
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font4,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      InkWell(
                        child: Text(
                          'Try Again..',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'en',
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font4,
                          ),
                        ),
                        onTap: (){
                          setState(() async{
                            initConnectivity();
                          });
                        },
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      );
    }
  }
  ///Internet Check area
  String _connectionStatus = 'connected';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }
  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        try {
          final result = await InternetAddress.lookup('google.com');
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            setState(() {
              _connectionStatus = "connected";
              resp = getUserOrder(Lang,Token);
            });
          }
        } on SocketException catch (_) {
          setState(() {
            _connectionStatus = "failed";
          });
        }
        break;
      case ConnectivityResult.mobile:
        setState((){
          _connectionStatus = "connected";
          resp = getUserOrder(Lang,Token);
        });
        break;
      case ConnectivityResult.none:
        setState(() => _connectionStatus = "failed");
        break;
      default:
        setState(() => _connectionStatus = 'failed');
        break;
    }
  }

  ///////////////
  List<dynamic> _CartMeals = <dynamic>[];
  addOrderToCarty(List<dynamic> _OrderItems){
    print('*****222*****');
    // _OrderItems.forEach((element) {
    //   print(element[0]);
    // });
    print(_OrderItems[0]["choice"]['choices_id']);
    print('*****222*****');

    _CartMeals.clear();
    for(int i=0;i<_OrderItems.length;i++){
      dynamic AddMeal = CartMeal();
      if(_OrderItems[i]['FK_products_id'] == null){
        dynamic offer = _OrderItems[i]['offer'];
        AddMeal.meals_id = offer['offers_id'];
        AddMeal.meals_title = offer['offers_title'];
        AddMeal.meals_img = offer['offers_img_mobile'];
        AddMeal.meals_desc = offer['offers_desc'];
        AddMeal.meals_price = offer['offers_price'];
        AddMeal.follow_delivery_time = "1";
        AddMeal.type = "offer";
        AddMeal.count = _OrderItems[i]['orders_items_count'];
      }
      else{
        dynamic meal = _OrderItems[i]['product'];
        AddMeal.quantityType = meal['products_type'];//Product follow weight or num
        AddMeal.minQuantity = meal['products_lowest_weight_available'];

        AddMeal.meals_id = meal['products_id'];
        AddMeal.choice_text = "";
        AddMeal.choice_id = _OrderItems[i]["choice"] != null? _OrderItems[i]["choice"]['choices_id']:null;
        AddMeal.meals_title = meal['products_title'];
        AddMeal.meals_img = meal['products_img'];
        AddMeal.meals_desc = meal['products_desc'];
        AddMeal.meals_price = meal['products_price'];
        AddMeal.follow_delivery_time =
            meal['category']['categories_follow_delivery_time'];
        AddMeal.type = "product";
        AddMeal.count = double.parse(_OrderItems[i]['orders_items_count']);
      }
      dynamic obj = AddMeal.toJson();
      _CartMeals.add(obj);
    }
  }

  Future<void> _ConfirmDialogAlert(BuildContext contextt,List<dynamic> _OrderItems,String imgName,String lang) {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(
                  SystemControls.RadiusCircValue)
          ),
          title: Text("reorderConfirmMSG".tr().toString(),
            style: TextStyle(
                fontFamily: Lang,
                fontWeight: FontWeight.bold,
                fontSize: SystemControls.font3,
                color: globals.accountColorsData['TextHeaderColor']!=null
                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
            ),
            textAlign: TextAlign.center,
          ),
          contentPadding: EdgeInsets.only(top: 40,bottom: 0),
          content: ClipRRect(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(SystemControls.RadiusCircValue),
              bottomRight: Radius.circular(SystemControls.RadiusCircValue),
            ),
            child: Container(
              height: 40,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Container(
                      decoration: BoxDecoration(
                        color: globals.accountColorsData['MainColor']!=null
                            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                        border: Border(
                          top: BorderSide(color: Colors.black),
                        ),
                        //borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: FlatButton(
                        child: Text("cancel".tr().toString(),
                          style: TextStyle(
                            fontFamily: Lang,
                            fontWeight: FontWeight.normal,
                            fontSize: SystemControls.font3,
                            color: globals.accountColorsData['TextOnMainColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                          ),
                        ),
                        onPressed: () {
                          SystemControls().storeDataToPreferences("lang", "$lang");

                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.black,
                    width: 1,
                    height: 40,
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      decoration: BoxDecoration(
                        color: globals.accountColorsData['MainColor']!=null
                            ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                        border: Border(
                          top: BorderSide(color: Colors.black),
                        ),
                      ),
                      child: FlatButton(
                        child: Text("orderBut".tr().toString(),
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData['TextOnMainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                          ),
                        ),
                        onPressed: () async{
                          SystemControls().storeDataToPreferences("lang", "$lang");

                          await clear_cart_file();
                          addOrderToCarty(_OrderItems);
                          UpdateList(_CartMeals);

                          CartStateProvider cartState = Provider.of<CartStateProvider>(context,listen: false);
                          cartState.setCurrentProductsCount(_CartMeals.length.toString());
                          cartState.setCurrentProductsCount(_CartMeals.length.toString());
                          if(imgName != null) {
                            cartState.setCartImgName(imgName);
                          }

                          Navigator.of(context).pop();

                          SuccDialogAlertGoCart(this.context,
                              'orderAddtoCart'.tr().toString(),
                              Lang,Token,AppCurrency,design_Control);

                        },
                      ),
                    ),
                  ),


                ],
              ),
            ),
          ),
        );
      },
    );
  }
}