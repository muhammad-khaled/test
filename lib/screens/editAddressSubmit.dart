import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';

import 'package:easy_localization/easy_localization.dart';
import '../Controls.dart';

import '../locale/app_localization.dart';
//imppp4
import '../dataControl/cartModule.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';
import '../widgets/UI_Widgets_InputField.dart';
import '../screens/myAddresses.dart';

import '../globals.dart' as globals;

class editAddressSubmit extends StatefulWidget{

  String Lang;
  String AppCurrency;
  String UserToken;
  int design_Control;
  dynamic addressDet;
  editAddressSubmit(this.Lang, this.UserToken,this.AppCurrency,
      this.design_Control,this.addressDet);

  _editAddressSubmit createState() =>
      _editAddressSubmit(this.Lang, this.UserToken,this.AppCurrency,
          this.design_Control,this.addressDet);
}

class _editAddressSubmit extends State<editAddressSubmit>{
  String Lang;
  String AppCurrency;
  String UserToken;
  int design_Control;
  dynamic addressDet;

  double latitude, longitude;
  @override
  void initState() {
    super.initState();
  }
  String gps;
  _editAddressSubmit(this.Lang, this.UserToken,this.AppCurrency,
      this.design_Control, this.addressDet){

    print("Location before ########## 2"+ addressDet['customer_addresses_gps']);

    addressTitleTextField.text = addressDet['customer_addresses_name'];
    if(addressDet['customer_addresses_default'] == "1"){
      setDefault = true;
    }
    gps = addressDet['customer_addresses_gps'];
    var arr = gps.split(',');
    latitude = double.parse(arr[0]);
    longitude = double.parse(arr[1]);
    ///Address
    for(int i=0;i<SystemControls.addresPartsFormat(Lang).length;i++){
      TextEditingController addressTextField1 = TextEditingController();
      addressList.add(addressTextField1);
    }
    String Fulladdress= addressDet['customer_addresses_address'];
    var addressArr = Fulladdress.split(',');
    for(int i=0; i<addressArr.length;i++){
      if(i<SystemControls.addresPartsFormat(Lang).length){
        addressList[i].text = addressArr[i];
      }
      else{
        break;
      }
    }
  }

  List<TextEditingController> addressList = <TextEditingController>[];

  TextEditingController addressTitleTextField = TextEditingController();
  final FocusNode _addressFocus = FocusNode();
  final FocusNode _addressTitleFocus = FocusNode();

  bool setDefault = false;

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  GoogleMapController mapController;


  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("editAddress".tr().toString()),
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
      ),
      body: Stack(
        //fit: StackFit.expand,
        children: <Widget>[
          LayoutBG(design_Control),
          Align(
            alignment: Alignment.center,
            child: Container(
              //width: mediaQuery.size.shortestSide,
              //height: mediaQuery.size.height,
              margin: EdgeInsets.only(top: 10,left: 10,right: 10,bottom: 10),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: ListView(
                      //shrinkWrap : true,
                      //physics: NeverScrollableScrollPhysics(),
                      //padding: EdgeInsets.zero,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            controller: addressTitleTextField,
                            focusNode: _addressTitleFocus,
                            decoration: InputFieldDecoration("addressTitle".tr().toString(), Lang),
                            keyboardType: TextInputType.text,
                            onFieldSubmitted: (_){
                              _addressTitleFocus.unfocus();
                              FocusScope.of(context).requestFocus(_addressFocus);
                            },
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),

                          child: Row(
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: Text("addressDefault".tr().toString(),
                                    style: TextStyle(
                                      fontFamily: Lang,
                                      fontWeight: FontWeight.normal,
                                      fontSize: SystemControls.font3,
                                      color: globals.accountColorsData['TextHeaderColor']!=null
                                          ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                      height: 1.2,
                                    )
                                ),
                              ),
                              Container(
                                //flex: 1,
                                child: Switch(
                                  value: setDefault,
                                  onChanged: (value){
                                    setState(() {
                                      setDefault = value;
                                    });
                                  },
                                  activeTrackColor: globals.accountColorsData['MainColorOpacity']!=null?
                                  Color(int.parse(globals.accountColorsData['MainColorOpacity'])) : SystemControls.MainColorOpacityz,
                                  activeColor: globals.accountColorsData['MainColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                                ),
                              )
                            ],
                          ),
                        ),

                        ListView.builder(
                            shrinkWrap : true,
                            physics: ScrollPhysics(),
                            scrollDirection: Axis.vertical,
                            itemCount: (SystemControls.addresPartsFormat(Lang).length/2).round(),
                            itemBuilder: (context, ListIndex){
                              int index = ListIndex*2;
                              if(SystemControls.addresPartsFormat(Lang).length > 3) {
                                return Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        margin: EdgeInsets.only(top: 10),
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10)),
                                        ),
                                        child: TextFormField(
                                          controller: addressList[index],
                                          decoration: InputFieldDecoration(
                                              SystemControls.addresPartsFormat(
                                                  Lang)[index], Lang),
                                          keyboardType: TextInputType.text,
                                          style: new TextStyle(
                                            fontFamily: Lang,
                                          ),
                                        ),
                                      ),
                                      flex: 1,
                                    ),
                                    Container(width: 5,),
                                    Expanded(
                                      flex: 1,
                                      child: (index + 1) < SystemControls
                                          .addresPartsFormat(Lang)
                                          .length ? (
                                          Container(
                                            margin: EdgeInsets.only(top: 10),
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10)),
                                            ),
                                            child: TextFormField(
                                              controller: addressList[index + 1],
                                              decoration: InputFieldDecoration(
                                                  SystemControls
                                                      .addresPartsFormat(
                                                      Lang)[index + 1], Lang),
                                              keyboardType: TextInputType.text,
                                              style: new TextStyle(
                                                fontFamily: Lang,
                                              ),
                                            ),
                                          )
                                      ) : (Container()),
                                    )
                                  ],
                                );
                              }
                              else {
                                return Column(
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.only(top: 10),
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10)),
                                      ),
                                      child: TextFormField(
                                        controller: addressList[index],
                                        decoration: InputFieldDecoration(
                                            SystemControls.addresPartsFormat(
                                                Lang)[index], Lang),
                                        keyboardType: TextInputType.text,
                                        style: new TextStyle(
                                          fontFamily: Lang,
                                        ),
                                      ),
                                    ),
                                    (index + 1) < SystemControls
                                        .addresPartsFormat(Lang)
                                        .length ? (
                                        Container(
                                          margin: EdgeInsets.only(top: 10),
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10)),
                                          ),
                                          child: TextFormField(
                                            controller: addressList[index + 1],
                                            decoration: InputFieldDecoration(
                                                SystemControls
                                                    .addresPartsFormat(
                                                    Lang)[index + 1], Lang),
                                            keyboardType: TextInputType.text,
                                            style: new TextStyle(
                                              fontFamily: Lang,
                                            ),
                                          ),
                                        )
                                    ) : (Container()),
                                  ],
                                );
                              }
                              return Container(
                                margin: EdgeInsets.only(top: 10),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                ),
                                child: TextFormField(
                                  controller: addressList[index],
                                  decoration: InputFieldDecoration(SystemControls.addresPartsFormat(Lang)[index], Lang),
                                  keyboardType: TextInputType.text,
                                  style: new TextStyle(
                                    fontFamily: Lang,
                                  ),
                                ),
                              );
                            }
                        ),

                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Container(
                            width: mediaQuery.size.width,
                            height: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                color: globals.accountColorsData['MainColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz
                            ),
                            child: FlatButton(
                              onPressed: ()async{
                                /*
                                String gps = _center.latitude.toString()
                                    + "," + _center.longitude.toString();*/
                                String Default;
                                if(setDefault == false){
                                  Default = "0";
                                }
                                else{
                                  Default = "1";
                                }

                                String fulladdress = "";
                                bool missFiled=false;
                                for(int i=0;i<SystemControls.addresPartsFormat(Lang).length;i++){
                                  if(SystemControls.addresPartsFormat(Lang)[i].contains('*')
                                      &&addressList[i].text.isEmpty){
                                    missFiled = true;
                                    break;
                                  }
                                  fulladdress += addressList[i].text +",";
                                }

                                if(addressTitleTextField.text.isNotEmpty &&
                                    fulladdress != "" && missFiled == false){
                                  var res = await editAddressApi(Lang, UserToken,
                                      fulladdress, addressTitleTextField.text,
                                      gps, Default,addressDet['customer_addresses_id']);
                                  if(res == null){
                                    ErrorDialogAlert(context,
                                        "respError".tr().toString());
                                  }
                                  else{
                                    if(res.errors == null && res.status ==200){
                                      SuccDialogAlertStaySameLayout(context, res.message,ValueKey("editAddress"));
                                      Timer(Duration(seconds: 3), () {
                                        Navigator.pop(context);
                                        Navigator.pushReplacement(context,
                                            MaterialPageRoute(
                                              builder: (
                                                  BuildContext context) =>
                                                  MyAddresses(Lang, UserToken,
                                                      AppCurrency,
                                                      design_Control),)
                                        );
                                      });
                                    }
                                    else{
                                      String error="";
                                      for(int i=0;i<res.errors.length;i++){
                                        error += res.errors[i] + "\n";
                                      }
                                      ErrorDialogAlert(context,error);
                                    }
                                  }

                                }
                                else{
                                  ErrorDialogAlert(context,
                                   "AddAllData".tr().toString());
                                }
                              },
                              child: Text("save".tr().toString(),
                                  style: TextStyle(
                                    fontFamily: Lang,
                                    fontWeight: FontWeight.normal,
                                    fontSize: SystemControls.font3,
                                    color: globals.accountColorsData['TextOnMainColor']!=null
                                        ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                                    height: 1.2,
                                  )
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}