import '../../provider/HomeProvider/HomeProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:provider/provider.dart';
import '../Controls.dart';

import 'package:easy_localization/easy_localization.dart';
import '../screens/register.dart';
import '../screens/forgetPassword.dart';
import '../screens/home.dart';
import '../screens/registerGetPhoneNumber.dart';

import '../dataControl/userModule.dart';
import '../locale/share_preferences_con.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';
import '../widgets/UI_Widgets_InputField.dart';
import '../globals.dart' as globals;

class login extends StatefulWidget {

  String Lang;
  int design_Control;
  login(this.Lang,this.design_Control);

  @override
  _loginState createState() => _loginState();
}

class _loginState extends State<login> with SharePreferenceData{
  TextEditingController emailTextField = TextEditingController();

  TextEditingController passwordTextField = TextEditingController();

  final FocusNode _emailFocus = FocusNode();

  final FocusNode _passwordFocus = FocusNode();

  String countryCode = "";

  @override
  Widget build(BuildContext context) {
    print("Lang");
    print(widget.Lang);
    print(widget.Lang);
    print("Lang");
    SystemControls().storeDataToPreferences("lang", "${widget.Lang}");

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: globals.accountColorsData['BGColor']!=null
                  ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
              image: DecorationImage(
                  image: AssetImage("assets/SplashBG.png",),
                  fit: BoxFit.cover
              ),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Container(
              margin: EdgeInsets.all(20),
              width: MediaQuery.of(context).size.shortestSide,
              child: ListView(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 70),
                    height: 150,
                    child: Center(
                      child: Image.asset(widget.Lang =="ar"? 'assets/logo.png':"assets/logoEn.png"),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Text("login".tr().toString(),//"Full name",
                      style: TextStyle(
                          fontFamily: widget.Lang,
                          fontWeight: FontWeight.bold,
                          fontSize: SystemControls.font2,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                      ),
                    ),
                  ),
/// see after translation
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        alignment: Alignment.center,
                        width: 90,
                         height: 51,
                          decoration: BoxDecoration(
                            color: Colors.grey.shade400,
                              borderRadius: widget.Lang =='en'? BorderRadius.only(topLeft: Radius.circular(8),bottomLeft: Radius.circular(8)):BorderRadius.only(topRight: Radius.circular(8),bottomRight: Radius.circular(8))
                          ),
                        margin: EdgeInsets.only(top: 15,right: 0),
                        child: globals.accounts_multiple_countries == "0" ?
                        Text(globals.countryCode,
                          style: TextStyle(
                              fontFamily: widget.Lang,
                              fontWeight: FontWeight.bold,
                              fontSize: SystemControls.font2,
                              color: globals.accountColorsData['TextHeaderColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                          ),
                          textAlign: TextAlign.center,
                        )
                            :CountryCodePicker(
                          onChanged: (code){
                            countryCode = code.dialCode;
                            print(countryCode);
                          },
                          // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                          initialSelection: 'EG',
                          // optional. Shows only country name and flag
                          showCountryOnly: false,
                          // optional. Shows only country name and flag when popup is closed.
                          showOnlyCountryWhenClosed: false,
                          // optional. aligns the flag and the Text left
                          alignLeft: false,
                          //Get the country information relevant to the initial selection
                          onInit: (code) {
                            countryCode = code.dialCode;
                            print("onInit "+countryCode);
                          },
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          margin: EdgeInsets.only(top: 15),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            controller: emailTextField,
                            focusNode: _emailFocus,

                            decoration: InputFieldDecorationPhone("phone".tr().toString(), widget.Lang),
                            keyboardType: TextInputType.phone,
                            //inputFormatters: <TextInputFormatter>[WhitelistingTextInputFormatter.digitsOnly],
                            onFieldSubmitted: (_){
                              _emailFocus.unfocus();
                              FocusScope.of(context).requestFocus(_passwordFocus);
                            },
                            //maxLines: 5,
                            style: new TextStyle(
                              fontFamily: widget.Lang,
                            ),
                          ),
                        ), //phone number
                      )
                    ],
                  ),//phoneNumber

                  Container(
                    margin: EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: TextFormField(
                      controller: passwordTextField,
                      focusNode: _passwordFocus,
                      decoration: InputFieldDecoration("password".tr().toString(), widget.Lang),
                      obscureText: true,
                      keyboardType: TextInputType.visiblePassword,
                      //maxLines: 5,
                      onFieldSubmitted: (_){
                        _passwordFocus.unfocus();
                      },
                      style: new TextStyle(
                        fontFamily: widget.Lang,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: InkWell(
                      child: Text("forgetPassword".tr().toString(),
                        style: TextStyle(
                            fontFamily: widget.Lang,
                            fontWeight: FontWeight.bold,
                            fontSize: SystemControls.font3,
                            color: globals.accountColorsData['TextHeaderColor']!=null
                                ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                        ),
                        textAlign: TextAlign.end,
                      ),
                      onTap: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>forgetPassword(widget.Lang,widget.design_Control)));
                      },
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: FlatButton(
                      onPressed: ()async{
                        SystemControls().storeDataToPreferences("lang", "${widget.Lang}");

                        if(emailTextField.text.isNotEmpty && passwordTextField.text.isNotEmpty
                            && emailTextField.text.length>=8){
                          if(globals.accounts_multiple_countries == "0") {
                            countryCode = globals.countryCode;
                          }
                          var res = await loginApi(widget.Lang, emailTextField.text,
                              passwordTextField.text,countryCode);
                          if(res == null){
                            ErrorDialogAlert(context,
                                "respError".tr().toString());
                          }
                          else{
                            if(res.status == 200){
                              dynamic user_data = res.data['customer'];
                              dynamic user_token = res.data['token'];
                              if(user_data['customers_status'] == "0"){
                                ErrorDialogAlert(context,res.message);
                              }
                              else if(user_token != null){
                                print(user_token['access_token'] + "\n\n\n " +
                                    user_data['customers_name']);
                                //storeDataToPreferences(
                                  //  "api_token", user_token['access_token']);
                                storeDataToPreferences(
                                    "name", user_data['customers_name']);
                                storeDataToPreferences(
                                    "email", user_data['customers_email']);
                                storeDataToPreferences("customers_phone",
                                    user_data['customers_phone']);
                                storeDataToPreferences("customers_country_code",
                                    user_data['customers_country_code']);

                                storeDataToPreferences("customers_birthday",
                                    user_data['customers_birthday']);
                                storeDataToPreferences("customers_gender",
                                    user_data['customers_gender']);
                                storeDataToPreferences("email_verified",
                                    user_data['email_verified']);
                                //setLoggingStatus(true);

                                storeDataToPreferences("api_token", user_token['access_token'])
                                    .then((bool isStored){
                                  setLoggingStatus(true)
                                      .then((bool isStored2){
                                        if(isStored && isStored2){
                                          SuccDialogAlertLogin(context, widget.Lang,res.message);
                                        }
                                  });
                                });


                              }
                              var homeProvider = Provider.of<HomeProvider>(context,listen: false);
                              homeProvider.GetCustomerData();
                            }
                            else{
                              String error= "";
                              for(int i=0;i<res.errors.length;i++){
                                error += res.errors[i] +"\n";
                              }
                              ErrorDialogAlert(context,error);
                            }
                          }
                        }
                        else if(emailTextField.text.isNotEmpty
                            && (emailTextField.text.length>10)){
                          ErrorDialogAlert(context,
                            "enterTruePhone".tr().toString());
                        }
                        else{
                          ErrorDialogAlert(context,
                            "AddAllData".tr().toString());
                        }
                      },
                      child: Center(
                        child: Text(
                         "login".tr().toString(),
                          style: TextStyle(
                              fontFamily: widget.Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font2,
                              color: globals.accountColorsData['TextOnMainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                          ),
                        ),
                      ),
                    ),
                  ), //Login

                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: InkWell(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text("newAccountMess".tr().toString(),
                            style: TextStyle(
                                fontFamily: widget.Lang,
                                fontWeight: FontWeight.bold,
                                fontSize: SystemControls.font2,
                                color: globals.accountColorsData['TextHeaderColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                            ),
                          ),
                          InkWell(
                            child: Text("signup".tr().toString(),
                              style: TextStyle(
                                  fontFamily: widget.Lang,
                                  fontWeight: FontWeight.bold,
                                  fontSize: SystemControls.font2,
                                  color: globals.accountColorsData['TextHeaderColor']!=null
                                      ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                              ),
                            ),
                          )
                        ],
                      ),
                      onTap: (){
                        Navigator.push(context,
                            MaterialPageRoute(builder: (BuildContext context) =>
                                //register(Lang,design_Control,"123+456+78")
                                registerEnterPhoneNumber(widget.Lang, widget.design_Control)
                            ));
                      },
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 15),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      border: null,
                    ),
                    child: FlatButton(
                      onPressed: (){
                        Navigator.pop(context);
                      },
                      child: Center(
                        child: Text(
                          "back".tr().toString(),
                          style: TextStyle(
                              fontFamily: widget.Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font2,
                              color: Colors.black
                          ),
                        ),
                      ),
                    ),
                  ), //back
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}