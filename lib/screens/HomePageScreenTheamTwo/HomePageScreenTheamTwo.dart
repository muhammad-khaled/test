import 'package:deodela/Repo/ReadNotificationRepo.dart';

import '../../widgets/Drawer/ModefiedDrawerWidget.dart';

import '../../Models/HomeModel/sharedModel.dart';
import '../../Models/NotificationModel.dart';
import '../../screens/HomePageScreenTheamTwo/BestSale.dart';
import '../../screens/SharedScreen/GetProdductByIdScreen.dart';
import '../../widgets/SharedWidget/OffersScreen.dart';
import '../../widgets/SharedWidget/SpecialProducts.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../Models/DiscountModel.dart';
import '../../Models/HomeModel/CategorySearchModel.dart';
import '../../Models/HomeModel/HomeProductsModel.dart';
import '../../Models/HomeModel/OfferModel.dart';
import '../../Models/HomeModel/applicationHomeModel.dart';
import '../../dataControl/cartModule.dart';
import '../../locale/HmoeMethods.dart';
import '../../provider/HomeProvider/HomeProvider.dart';
import '../../provider/HomeProvider/SearchProvider.dart';
import '../../provider/NotificaionProvider.dart';
import '../../provider/ProviderTheamTwo/DisplayCategoryTheamTwo.dart';
import '../../screens/SharedScreen/SearchScreen.dart';
import '../../services/HomeRepo.dart';
import '../../widgets/Drawer/Drawer.dart';
import '../../widgets/SharedWidget/ProductCardBase.dart';
import '../../widgets/SharedWidget/ProductItmsList.dart';
import '../../widgets/SharedWidget/ProgressIndecator.dart';
import 'package:animated_bottom_navigation_bar/animated_bottom_navigation_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:sizer/sizer.dart';
import '../../globals.dart' as globals;

import '../../provider/productsInCartProvider.dart';
import 'dart:async';
import 'package:async/async.dart';
import '../../widgets/UI_Alert_Widgets.dart';
import '../../widgets/UI_Wigdets.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

import '../../Controls.dart';
import '../cart1Items.dart';
import '../notificationLayoutList.dart';
import '../orderDetails.dart';
import 'CategorySelectedPageTheamTwo.dart';
import 'CategoryeTheamTwo.dart';

class HomePageScreenTheamTwo extends StatefulWidget {
  const HomePageScreenTheamTwo({Key key}) : super(key: key);

  @override
  _HomePageScreenTheamTwoState createState() => _HomePageScreenTheamTwoState();
}

class _HomePageScreenTheamTwoState extends State<HomePageScreenTheamTwo> {
  final SystemControls storage = SystemControls();

  bool hasRegistration = false;
  dynamic appInitialInfo;
  String userToken = "";
  String appCurrency = "";
  String advertisement = "";
  bool isLogging;

  void log() {
    setState(() {
      storage.getAppRegistration().then((value) {
        if (value == true) {
          hasRegistration = true;
        } else {
          hasRegistration = false;
        }
      });
      storage.getaccountsShowAdvertisements().then((value) {
        advertisement = value;
        print(value);
      });
      storage.getAppInfo().then((value) {
        if (value != null) {
          appInitialInfo = value;
        } else {
          appInitialInfo = "";
        }
      });
      print("Home isLogging -> in**" + isLogging.toString());
      storage.getCurrentLoggingStatus().then((status) {
        setState(() {
          if (status != null) {
            isLogging = status;
            print("Home isLogging ->" + isLogging.toString());
          } else {
            isLogging = false;
          }
        });
        storage.getDataFromPreferences('api_token').then((token) {
          if (token != null) {
            userToken = token;
            print("Home token ->" + userToken.toString());
          } else {
            userToken = "";
          }
          storage.getDataFromPreferences('currency').then((currency) {
            setState(() {
              appCurrency = currency;
            });

            print("Home AppCurrency ->" + appCurrency);
          });
        });
      });
    });

    print(appCurrency);
    print(appCurrency);
    print(appCurrency);
    print(appCurrency);
  }
   Future<String> future;
  Future<CartList> localCart = read_from_file();
  onClickNotificationButton() {
    if (userToken == "") {
      ErrorDialogAlertGoTOLogin(context, "mustLogin".tr().toString(),
          EasyLocalization.of(context).currentLocale.languageCode);
    } else {
      future = ReadNotificationRepo(lang: EasyLocalization.of(context).currentLocale.languageCode,token: userToken).readNotificationRepo;
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => NotificationList(
                  EasyLocalization.of(context).currentLocale.languageCode,
                  userToken,
                  appCurrency,
                  SystemControls.designControl)),
          (Route<dynamic> route) => true);
    }
  }

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();

  String tokenData;


  @override
  void initState() {
    // TODO: implement initState
    Provider.of<CartStateProvider>(context, listen: false).fetchCardNum();
    Provider.of<NotificationProvider>(context, listen: false).getcount();

    log();
    super.initState();

    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            MacOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );

    WidgetsFlutterBinding.ensureInitialized();
    // FirebaseMessaging.onBackgroundMessage((message){
    //   print('BackGrooooooooound');
    //   return displayNotificationAndroidBackGround(message);
    // });


    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print(message.data);
      print(message.notification.body);
      print(message.notification.title);
      print(message.messageId);
      print(message.messageType);
      displayNotificationAndroid(message, context);
      handleNotification(
          message: message.data,
          dialog: true,
          notificationType: 1,
          token: userToken,
          appCurrency: appCurrency,
          context: context);
    });

    // FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
    //   print('A new onMessageOpenedApp event was published!');
    //   displayNotificationAndroid(message, context);
    //
    // });
      FirebaseMessaging.onBackgroundMessage((message) => displayNotificationAndroid(message, context));
    FirebaseMessaging.instance.getToken().then((String token) {
      assert(token != null);
      String _homeScreenText = "Waiting for token...";
      _homeScreenText = "Push Messaging token: $token";
      tokenData = token;
      print(_homeScreenText);
    });
    ///////////////////////
    // CheckGpsAlert(context);
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    log();

    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(covariant HomePageScreenTheamTwo oldWidget) {
    // TODO: implement didUpdateWidget
    log();
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
    toggleDrawer() async {
      if (scaffoldKey.currentState.isDrawerOpen) {
        scaffoldKey.currentState.openEndDrawer();
      } else {
        scaffoldKey.currentState.openEndDrawer();
      }
    }

    return Consumer<HomeProvider>(builder: (context, homeData, _) {
      return Scaffold(
        backgroundColor: Colors.white,
        // drawer: Consumer<CartStateProvider>(
        //   builder: (context, cart, _) {
        //     return Drawer(
        //       child: DrawerWidget(
        //         token: userToken,
        //         hasRegistration: hasRegistration,
        //         isLogin: isLogging,
        //         currency: appCurrency,
        //         appInfo: appInitialInfo,
        //         drawer: toggleDrawer,
        //       ),
        //     );
        //   },
        // ),
        appBar: (homeData.currentHomeWidgetIndex == 3 || homeData.currentHomeWidgetIndex ==4 )
            ? AppBar(
          toolbarHeight: 0,
          elevation: 0,
        ) :AppBar(
                elevation: 0,
                centerTitle: false,
                backgroundColor: AppBarBGColor(),
                flexibleSpace:
                    AppBarElementsflexibleSpace(SystemControls.designControl),
                leading: Builder(
                  builder: (context) => Container(
                    height: 2.h,
                    width: 10.w,
                    child: InkWell(
                      onTap: onClickNotificationButton,
                      child: Stack(
                        children: [
                          Container(
                            child: Center(
                              child: IconButton(
                                icon: Icon(
                                  FontAwesomeIcons.solidBell,
                                  color: Colors.black,
                                  size: 20,
                                ),

                              ),
                            ),
                          ),
                          Container(
                            margin: EasyLocalization.of(context)
                                        .currentLocale
                                        .languageCode ==
                                    "en"
                                ? EdgeInsets.only(top: 6, left: 10)
                                : EdgeInsets.only(top: 6, right: 10),
                            width: 22,
                            height: 22,
                            decoration: BoxDecoration(
                              //color: Colors.white,
                              color: globals.accountColorsData['MainColor'] !=
                                      null
                                  ? Color(int.parse(
                                      globals.accountColorsData['MainColor']))
                                  : SystemControls.MainColorz,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(11)),
                            ),
                            child: Consumer<NotificationProvider>(
                              builder: (context, notification, _) {
                                notification.fetchNotificationCounter(EasyLocalization.of(context).currentLocale.languageCode, userToken, context);

                                return Center(
                                  child: StreamBuilder<NotificationModel>(
                                    stream: notification.streamNotificationCounter,
                                    builder: (context,snap){
                                      int x =0;
                                    if(snap.hasData){
                                      print('elemnts');
                                      snap.data.data!= null?    snap.data.data.notifications.forEach((elementf) {
                                           if(elementf.customers[0].readAt== null){
                                             x++;
                                          }

                                       }): print('');


                                       return Text(
                                        "$x",
                                        style: TextStyle(
                                          color: globals.accountColorsData[
                                          'TextOnMainColor'] !=
                                              null
                                              ? Color(int.parse(
                                              globals.accountColorsData[
                                              'TextOnMainColor']))
                                              : SystemControls.TextOnMainColorz,
                                          fontSize: SystemControls.font4,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        textAlign: TextAlign.center,
                                      );
                                    }else{
                                      return Text(
                                        "${notification.counter == null ? "0" : (notification.counter).ceil().toInt()}",
                                        style: TextStyle(
                                          color: globals.accountColorsData[
                                          'TextOnMainColor'] !=
                                              null
                                              ? Color(int.parse(
                                              globals.accountColorsData[
                                              'TextOnMainColor']))
                                              : SystemControls.TextOnMainColorz,
                                          fontSize: SystemControls.font4,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        textAlign: TextAlign.center,
                                      );
                                    }
                                  },),
                                );
                              },
                            ),
                          )
                        ],
                      ),
                      // onTap: onClickIcon,
                    ),
                  ),
                ),
                actions: [
                  Container(
                    child: Row(
                      children: [
                        AppBarTitleImg(EasyLocalization.of(context)
                            .currentLocale
                            .languageCode),
                        SizedBox(
                          width: 15,
                        ),
                      ],
                    ),
                  )
                ],
                bottom: PreferredSize(
                  preferredSize: homeData.currentHomeWidgetIndex == 3 ||
                          homeData.currentHomeWidgetIndex == 4
                      ? Size.fromHeight(0.h)
                      : Size.fromHeight(8.h),
                  child: Consumer2<CartStateProvider, SearchProvider>(
                    builder: (context, cart, search, _) {
                      return homeData.currentHomeWidgetIndex == 3 ||
                              homeData.currentHomeWidgetIndex == 4
                          ? Container(
                              height: 0,
                              width: 0,
                            )
                          : Container(
                              width: MediaQuery.of(context).size.width -3,
                              height: 8.h,
                              child: Row(
                                children: [
                                  InkWell(
                                    child: Container(
                                      height: 40,
                                      width: 40,
                                      margin: EdgeInsetsDirectional.fromSTEB(
                                          10, 0, 0, 0),
                                      decoration: BoxDecoration(
                                        color: globals.accountColorsData[
                                                    'MainColor'] !=
                                                null
                                            ? Color(int.parse(
                                                globals.accountColorsData[
                                                    'MainColor']))
                                            : SystemControls.MainColorz,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey,
                                            blurRadius:
                                                2.0, // has the effect of softening the shadow
                                            spreadRadius:
                                                0.5, // has the effect of extending the shadow
                                            offset: Offset(
                                              2, // horizontal, move right 10
                                              3.0, // vertical, move down 10
                                            ),
                                          )
                                        ],
                                      ),
                                      child: RotatedBox(
                                        quarterTurns: 2,
                                        child: Icon(
                                          Icons.send,
                                          color: globals.accountColorsData[
                                                      'TextOnMainColor'] !=
                                                  null
                                              ? Color(int.parse(
                                                  globals.accountColorsData[
                                                      'TextOnMainColor']))
                                              : SystemControls.TextOnMainColorz,
                                          size: 20,
                                        ),
                                      ),
                                    ),
                                    onTap: () {
                                      if(FocusScope.of(context).isFirstFocus) {
                                        FocusScope.of(context).requestFocus(new FocusNode());
                                      }
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  SearchScreenNew(
                                                    cartList: cart.cartList,
                                                    token: userToken,
                                                    appCurrency: appCurrency,
                                                  )));
                                      search.searchWithTitle(context,
                                          search.searchController.text);
                                    },
                                  ),
                                  Spacer(),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Spacer(),
                                      Container(
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                          Radius.circular(15),
                                        )),
                                        height: 6.h,
                                        width: 65.w,
                                        child: TextField(
                                          cursorColor: Colors.black,
                                          controller: search.searchController,
                                          decoration: InputDecoration(
                                            hintText: "searchText".tr().toString(),
                                            hintStyle: TextStyle(
                                                color: globals.accountColorsData[
                                                            'BorderColor'] !=
                                                        null
                                                    ? Color(int.parse(globals
                                                            .accountColorsData[
                                                        'BorderColor']))
                                                    : SystemControls
                                                        .BorderColorz),
                                            errorMaxLines: 1,
                                            errorStyle: TextStyle(
                                                fontSize: 0, height: 0),
                                            isDense: false,
                                            prefixIcon: Icon(Icons.search,
                                                color: globals.accountColorsData[
                                                            'BorderColor'] !=
                                                        null
                                                    ? Color(int.parse(globals
                                                            .accountColorsData[
                                                        'BorderColor']))
                                                    : SystemControls
                                                        .BorderColorz),
                                            filled: true,
                                            contentPadding: EdgeInsets.all(3),
                                            fillColor: globals
                                                            .accountColorsData[
                                                        'BorderColor'] !=
                                                    null
                                                ? Color(int.parse(globals
                                                            .accountColorsData[
                                                        'BorderColor']))
                                                    .withOpacity(.2)
                                                : SystemControls.BorderColorz,
                                            enabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: globals.accountColorsData[
                                                                'BorderColor'] !=
                                                            null
                                                        ? Color(int.parse(globals
                                                                    .accountColorsData[
                                                                'BorderColor']))
                                                            .withOpacity(.2)
                                                        : SystemControls
                                                            .BorderColorz,
                                                    width: 1),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8))),
                                            disabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: globals.accountColorsData[
                                                                'BorderColor'] !=
                                                            null
                                                        ? Color(int.parse(globals
                                                                    .accountColorsData[
                                                                'BorderColor']))
                                                            .withOpacity(.2)
                                                        : SystemControls
                                                            .BorderColorz,
                                                    width: 1),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8))),
                                            focusedBorder: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: globals.accountColorsData[
                                                                'BorderColor'] !=
                                                            null
                                                        ? Color(int.parse(globals
                                                                    .accountColorsData[
                                                                'BorderColor']))
                                                            .withOpacity(.2)
                                                        : SystemControls
                                                            .BorderColorz,
                                                    width: 1),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8))),
                                            errorBorder: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: globals.accountColorsData[
                                                                'BorderColor'] !=
                                                            null
                                                        ? Color(int.parse(globals
                                                                    .accountColorsData[
                                                                'BorderColor']))
                                                            .withOpacity(.2)
                                                        : SystemControls
                                                            .BorderColorz,
                                                    width: 1),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8))),
                                            focusedErrorBorder: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: globals.accountColorsData[
                                                                'BorderColor'] !=
                                                            null
                                                        ? Color(int.parse(globals
                                                                    .accountColorsData[
                                                                'BorderColor']))
                                                            .withOpacity(.2)
                                                        : SystemControls
                                                            .BorderColorz,
                                                    width: 1),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8))),
                                          ),
                                        ),
                                      ),
                                      Spacer(),
                                    ],
                                  ),
                                  Spacer(),
                                  Consumer2<SearchProvider, CartStateProvider>(
                                      builder: (context, search, cart, _) {
                                    // return IconButton(
                                    //     onPressed: () {
                                    //       search.controlSearchCategories(true);
                                    //       Navigator.push(
                                    //           context,
                                    //           MaterialPageRoute(
                                    //               builder: (context) =>
                                    //                   SearchScreenNew(
                                    //                     cartList: cart.cartList,
                                    //                     token: userToken,
                                    //                     appCurrency:
                                    //                         appCurrency,
                                    //                   )));
                                    //     },
                                    //     icon: Icon(
                                    //       Icons.filter_list,
                                    //       size: 30,
                                    //     ));
                                    return InkWell(
                                        onTap: () {
                                          if(FocusScope.of(context).isFirstFocus) {
                                            FocusScope.of(context).requestFocus(new FocusNode());
                                          }
                                          search.controlSearchCategories(true);
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      SearchScreenNew(
                                                        cartList: cart.cartList,
                                                        token: userToken,
                                                        appCurrency:
                                                            appCurrency,
                                                      )));
                                        },
                                        child: Container(
                                            width: 10.w,
                                            child: Center(
                                                child: Image.asset(
                                              "assets/filter_icon.png",
                                              fit: BoxFit.contain,
                                            ))));
                                  }),
                                  Spacer(),
                                ],
                              ),
                            );
                    },
                  ),
                ),
              ),


        bottomNavigationBar: Consumer<HomeProvider>(
          builder: (context, home, _) {
            return BottomNavigationBar(


              onTap: (index) {
                home.changeCurrentHomeWidgetIndex(index, context);
              },

              currentIndex: home.currentHomeWidgetIndex,
              unselectedItemColor: Colors.black,
              showUnselectedLabels: true,
              iconSize: 20,
              selectedItemColor: globals.accountColorsData['MainColor'] != null
                  ? Color(int.parse(globals.accountColorsData['MainColor']))
                  : SystemControls.MainColorz,
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                    icon: Icon(FontAwesomeIcons.home),
                    label: "${"home".tr().toString()}"),
                BottomNavigationBarItem(
                    icon: Icon(FontAwesomeIcons.listUl),
                    label: "${"categories".tr().toString()}"),
                BottomNavigationBarItem(
                    icon: Icon(
                      FontAwesomeIcons.percentage,
                    ),
                    label: "${"offers".tr().toString()}"),
                BottomNavigationBarItem(
                    icon: Consumer<CartStateProvider>(
                      builder: (context, cart, _) {
                        return StreamBuilder<CartList>(
                          stream: cart.streamCard,
                          builder: (context, Cartsnap) {
                            if (Cartsnap.connectionState ==
                                    ConnectionState.done &&
                                Cartsnap.hasData) {
                              cart.setCartList(Cartsnap.data.Cmeals);

                              CartStateProvider cartState =
                                  Provider.of<CartStateProvider>(context,
                                      listen: false);
                              if (cartState.productsCount == "0" &&
                                  cart.cartList.length != 0) {
                                cartState.setCurrentProductsCount(
                                    cart.cartList.length.toString());
                              }
                              return Container(
                                width: 30,
                                height: 30,
                                color: Colors.transparent,
                                child: Stack(
                                  children: [
                                    Positioned(
                                      top: 0,
                                      left: 9,
                                      child: Icon(
                                        FontAwesomeIcons.shoppingBasket,
                                        size: 20,
                                        color: home.currentHomeWidgetIndex == 3
                                            ? globals.accountColorsData[
                                                        'MainColor'] !=
                                                    null
                                                ? Color(int.parse(
                                                    globals.accountColorsData[
                                                        'MainColor']))
                                                : SystemControls.MainColorz
                                            : Colors.black,
                                      ),
                                    ),
                                    Positioned(
                                        top: -2,
                                        right: -2,
                                        child: Container(
                                            height: 23,
                                            width: 23,
                                            decoration: BoxDecoration(
                                                color: globals.accountColorsData[
                                                            'MainColor'] !=
                                                        null
                                                    ? Color(int.parse(globals
                                                            .accountColorsData[
                                                        'MainColor']))
                                                    : SystemControls.MainColorz,
                                                shape: BoxShape.circle,
                                                border: Border.all(
                                                    width: 1,
                                                    color: Colors.transparent)),
                                            child: Center(child:
                                                Consumer<CartStateProvider>(
                                              builder: (context, cart, _) {
                                                return StreamBuilder<CartList>(
                                                    stream: cart.streamCard,
                                                    builder: (context, snap) {
                                                      if (snap.hasData) {
                                                        double x = 0;
                                                        snap.data.Cmeals
                                                            .forEach((element) {
                                                          x = x +
                                                              element["count"];
                                                        });
                                                        print('xxxxxxx:$x');
                                                        return Text(
                                                          "$x".replaceAll(
                                                              ".0", ""),
                                                          style: TextStyle(
                                                            color: globals.accountColorsData[
                                                                        'TextOnMainColor'] !=
                                                                    null
                                                                ? Color(int.parse(
                                                                    globals.accountColorsData[
                                                                        'TextOnMainColor']))
                                                                : SystemControls
                                                                    .TextOnMainColorz,
                                                            fontSize:
                                                                SystemControls
                                                                    .font4,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                          textAlign:
                                                              TextAlign.center,
                                                        );
                                                      } else {
                                                        return Text(
                                                          "0",
                                                          style: TextStyle(
                                                            color: globals.accountColorsData[
                                                                        'TextOnMainColor'] !=
                                                                    null
                                                                ? Color(int.parse(
                                                                    globals.accountColorsData[
                                                                        'TextOnMainColor']))
                                                                : SystemControls
                                                                    .TextOnMainColorz,
                                                            fontSize:
                                                                SystemControls
                                                                    .font4,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                          textAlign:
                                                              TextAlign.center,
                                                        );
                                                      }
                                                    });
                                              },
                                            )))),
                                  ],
                                ),
                              );
                            }
                            return Container(

                              width: 30,
                              height: 23,
                              color: Colors.transparent,
                              child: Stack(
                                children: [
                                  Icon(
                                    FontAwesomeIcons.opencart,
                                    size: 20,
                                    color: home.currentHomeWidgetIndex == 3
                                        ? globals.accountColorsData[
                                                    'MainColor'] !=
                                                null
                                            ? Color(int.parse(
                                                globals.accountColorsData[
                                                    'MainColor']))
                                            : SystemControls.MainColorz
                                        : Colors.black,
                                  ),
                                  Positioned(
                                      top: 0,
                                      left: 0,
                                      child: Container(
                                          height: 15,
                                          width: 15,
                                          decoration: BoxDecoration(
                                              color: globals.accountColorsData[
                                                          'MainColor'] !=
                                                      null
                                                  ? Color(int.parse(
                                                      globals.accountColorsData[
                                                          'MainColor']))
                                                  : SystemControls.MainColorz,
                                              shape: BoxShape.circle,
                                              border: Border.all(
                                                  width: 1,
                                                  color: Colors.transparent)),
                                          child: Center(child:
                                              Consumer<CartStateProvider>(
                                            builder: (context, cart, _) {
                                              return StreamBuilder<CartList>(
                                                  stream: cart.streamCard,
                                                  builder: (context, snap) {
                                                    if (snap.hasData) {
                                                      double x = 0;
                                                      snap.data.Cmeals
                                                          .forEach((element) {
                                                        x = x +
                                                            element["count"];
                                                      });
                                                      print('xxxxxxx:$x');
                                                      return Text(
                                                        "${x.round()}"
                                                            .replaceAll(
                                                                ".0", ""),
                                                        style: TextStyle(
                                                          color: globals.accountColorsData[
                                                                      'TextOnMainColor'] !=
                                                                  null
                                                              ? Color(int.parse(
                                                                  globals.accountColorsData[
                                                                      'TextOnMainColor']))
                                                              : SystemControls
                                                                  .TextOnMainColorz,
                                                          fontSize:7.sp,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.center,
                                                      );
                                                    } else {
                                                      return Text(
                                                        "0",
                                                        style: TextStyle(
                                                          color: globals.accountColorsData[
                                                                      'TextOnMainColor'] !=
                                                                  null
                                                              ? Color(int.parse(
                                                                  globals.accountColorsData[
                                                                      'TextOnMainColor']))
                                                              : SystemControls
                                                                  .TextOnMainColorz,
                                                          fontSize:
                                                              SystemControls
                                                                  .font4,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                        textAlign:
                                                            TextAlign.center,
                                                      );
                                                    }
                                                  });
                                            },
                                          )))),
                                ],
                              ),
                            );
                          },
                        );
                      },
                    ),
                    label: "${"cart".tr().toString()}"),
                BottomNavigationBarItem(
                    icon: Icon(FontAwesomeIcons.bars), label: "${"menu".tr().toString()}"),
              ],
            );
          },
        ),
        body: HomePageThemaTwoScreens(
          token: userToken,
          appCureency: appCurrency,
          advertisement: advertisement,
          appInitialInfo: appInitialInfo,
          hasRegistration: hasRegistration,
          isLogin: isLogging,
        ),
      );
    });
  }
}

class HomePageThemaTwoScreens extends StatefulWidget {
  HomePageThemaTwoScreens(
      {Key key,
      this.appCureency,
      this.advertisement,
      this.token,
      this.hasRegistration,
      this.appInitialInfo,
      this.isLogin})
      : super(key: key);
  final String token;
  final String appCureency;
  final String advertisement;
  bool hasRegistration = false;
  dynamic appInitialInfo;
  bool isLogin;

  @override
  _HomePageThemaTwoScreensState createState() =>
      _HomePageThemaTwoScreensState();
}

class _HomePageThemaTwoScreensState extends State<HomePageThemaTwoScreens> {
  ScrollController discountController;
  disCountListener() {
    var homeProvider = Provider.of<HomeProvider>(context, listen: false);
    print('*******//////----/////******');
    print(homeProvider.currentPage);
    print(homeProvider.nextPage);
    print(homeProvider.lastPage);
    switch (homeProvider.currentHomeWidgetIndex) {
      case 2:
        if (homeProvider.currentPage < homeProvider.lastPage &&
            discountController.position.pixels ==
                discountController.position.maxScrollExtent &&
            homeProvider.currentHomeWidgetIndex == 2) {
          print('offers');
          if (homeProvider.currentPage < homeProvider.nextPage) {
            print(homeProvider.currentPage);
            print(homeProvider.nextPage);
            homeProvider.fetchDiscountProducts(
                context, "${homeProvider.currentPage++}");
          }
        }
        break;
    }
  }

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    discountController = ScrollController();
    discountController.addListener(disCountListener);
  }

  Future<CartList> localCart = read_from_file();

  @override
  Widget build(BuildContext context) {
    return Consumer2<HomeProvider, CartStateProvider>(
        builder: (context, home, cart, _) {
      switch (home.currentHomeWidgetIndex) {
        case 0:
          return HomePageViewTheamTwo(
            cart: cart,
            home: home,
            token: widget.token,
            appCurrency: widget.appCureency,
            advertisement: widget.advertisement,
          );
          break;
        case 1:
          return CategoryTheamTwo(
            token: widget.token,
            appCureency: widget.appCureency,
          );
          break;
        case 3:
          return CartLayout(
              EasyLocalization.of(context).currentLocale.languageCode,
              widget.token,
              widget.appCureency,
              SystemControls.designControl);
          break;
        case 2:
          print("value");
          print(widget.advertisement);
          return Container(
            child: SingleChildScrollView(
              controller: discountController,
              child: Column(
                children: [
                  Consumer<CartStateProvider>(
                    builder: (context, cart, _) {
                      return FutureBuilder<CartList>(
                        future: localCart,
                        builder: (context, Cartsnap) {
                          if (Cartsnap.connectionState ==
                                  ConnectionState.done &&
                              Cartsnap.hasData) {
                            cart.setCartList(Cartsnap.data.Cmeals);

                            CartStateProvider cartState =
                                Provider.of<CartStateProvider>(context,
                                    listen: false);
                            if (cartState.productsCount == "0" &&
                                cart.cartList.length != 0) {
                              cartState.setCurrentProductsCount(
                                  cart.cartList.length.toString());
                            }
                            return Container();
                          }
                          return Container();
                        },
                      );
                    },
                  ),
                  widget.advertisement == "0"
                      ? FutureBuilder<OfferModelModel>(
                          future:
                              GetOfferApiRepo(context: context).getOfferApiData,
                          builder: (context, snap) {
                            if (snap.hasData) {
                              snap.data.data.offers.removeWhere(
                                  (element) => element.offersPrice == "0");
                              return ListView.builder(
                                shrinkWrap: true,
                                physics: ScrollPhysics(),
                                itemCount: snap.data.data.offers.length,
                                itemBuilder: (context, index) {
                                  return InkWell(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => OfferScreen(
                                                    offer: snap.data.data
                                                        .offers[index],
                                                    token: widget.token,
                                                    appCurrency:
                                                        widget.appCureency,
                                                  )));
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        height: 20.h,
                                        width: 100.w,
                                        child: CachedNetworkImage(
                                            imageUrl: SystemControls()
                                                .GetImgPath(
                                                    snap.data.data.offers[index]
                                                        .offersImgMobile,
                                                    "original"),
                                            fit: BoxFit.fill,
                                            height: 20.h,
                                            width: 100.w,
                                            errorWidget:
                                                (context, url, error) =>
                                                    Icon(Icons.error)),
                                      ),
                                    ),
                                  );
                                },
                              );
                            } else {
                              return Container(
                                height: 30.h,
                                child: Indicator(),
                              );
                            }
                          },
                        )
                      : Container(),
                  StreamBuilder<DiscountDetailsModel>(
                    stream: home.streamDiscount,
                    builder: (context, snap) {
                      if (snap.hasData) {
                        if (home.currentPage == home.lastPage) {
                          home.showLoader = false;
                        }

                        home.lastPage = snap.data.data.pagination.lastPage;
                        home.currentPage =
                            snap.data.data.pagination.currentPage;
                        home.nextPage =
                            snap.data.data.pagination.currentPage + 1;

                        home.productDiscountList.removeWhere(
                            (element) => element.productsPriceAfterSale == "");
                        home.productDiscountList.toSet();
                        return ProductsGridView(
                          token: widget.token,
                          cartList: cart.cartList,
                          products: home.productDiscountList.toSet().toList(),
                          appCurrency: widget.appCureency,
                          search: false,
                        );
                      } else {
                        return Container(
                          height: 30.h,
                          child: Indicator(),
                        );
                      }
                    },
                  ),
                ],
              ),
            ),
          );
          break;
        case 4:
          return ModifiedDrawerWidget(
            token: widget.token,
            isLogin: widget.isLogin,
            currency: widget.appCureency,
            hasRegistration: widget.hasRegistration,
            appInfo: widget.appInitialInfo,
            drawer: () {},
          );
          break;

        default:
          return Container();
      }
    });
  }
}

class HomePageViewTheamTwo extends StatefulWidget {
  HomePageViewTheamTwo(
      {Key key,
      this.home,
      this.token,
      this.appCurrency,
      this.cart,
      this.advertisement})
      : super(key: key);
  HomeProvider home;
  CartStateProvider cart;
  String token;
  String appCurrency;
  String advertisement;

  @override
  _HomePageViewTheamTwoState createState() => _HomePageViewTheamTwoState();
}

class _HomePageViewTheamTwoState extends State<HomePageViewTheamTwo> {
  @override
  void initState() {
    // TODO: implement initState
    ApplicationHomeResponseRepo(context: context).getHomeApplicationItems;
    super.initState();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    Provider.of<HomeProvider>(context, listen: false).fetchHomeData(context);

    super.didChangeDependencies();
  }

  Future<CartList> localCart = read_from_file();
  launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  goForwardMethod(
      {String type,
      String value,
      String token,
      String appCurrency,
      List<dynamic> cartList,
      SearchProvider searchProvider,String url}) {
    print("object");
    print(url);
    print(url);
   if(value == null && url == null){
     print('');
   }else {
     if(type!= null){
       switch (type) {
         case "product":
           Provider.of<HomeProvider>(context,listen: false).changeSwiperIndex(0);

           Navigator.push(
               context,
               MaterialPageRoute(
                   builder: (context) => GetProductByIDScreen(
                     cartList: cartList,
                     token: token,
                     appCurrency: appCurrency,
                     id: int.parse("$value"),
                   )));
           break;
         case "filter":
           print(type);
           Navigator.push(
               context,
               MaterialPageRoute(
                   builder: (context) => SearchScreenNew(
                     cartList: cartList,
                     token: token,
                     appCurrency: appCurrency,
                   )));
           searchProvider.searchWithTitle(context, "$value");
           break;
         case "category":
           Navigator.push(
               context,
               MaterialPageRoute(
                   builder: (context) => SearchScreenNew(
                     cartList: cartList,
                     token: token,
                     appCurrency: appCurrency,
                   )));
           searchProvider.searchWithCatID(context, int.parse("$value"));
           break;
       }}else{
       launchURL(url);
     }
   }

  }

  @override
  Widget build(BuildContext context) {
    localCart = read_from_file();
    return Container(
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            Consumer<CartStateProvider>(
              builder: (context, cart, _) {
                return FutureBuilder<CartList>(
                  future: localCart,
                  builder: (context, Cartsnap) {
                    if (Cartsnap.connectionState == ConnectionState.done &&
                        Cartsnap.hasData) {
                      cart.setCartList(Cartsnap.data.Cmeals);

                      CartStateProvider cartState =
                          Provider.of<CartStateProvider>(context,
                              listen: false);
                      if (cartState.productsCount == "0" &&
                          cart.cartList.length != 0) {
                        cartState.setCurrentProductsCount(
                            cart.cartList.length.toString());
                      }
                      return Container();
                    }
                    return Container();
                  },
                );
              },
            ),
            Column(
              children: [
                FutureBuilder<ApplicationHomeModel>(
                    future: ApplicationHomeResponseRepo(context: context)
                        .getHomeApplicationItems,
                    builder: (context, snap) {
                      if (snap.hasData) {
                        List<EndPage> banners =
                            snap.data.data.advertisements.sectionalBanners;
                        return Column(
                          children: [
                            widget.advertisement == "0"
                                ? snap.data.data.offers.length > 0
                                    ? CarouselSlider.builder(
                                        itemCount: snap.data.data.offers.length,
                                        itemBuilder: (context, index, __) {
                                          return CachedNetworkImage(
                                              imageUrl: SystemControls()
                                                  .GetImgPath(
                                                      snap
                                                          .data
                                                          .data
                                                          .offers[index]
                                                          .offersImgMobile,
                                                      "original"),
                                              fit: BoxFit.cover,
                                              height: 20.h,
                                              width: 100.w,
                                              errorWidget:
                                                  (context, url, error) =>
                                                      Icon(Icons.error));
                                        },
                                        options: CarouselOptions(
                                          // autoPlayAnimationDuration: Duration(seconds: 1),
                                          height: 20.h,
                                          autoPlayCurve: Curves.fastOutSlowIn,
                                          aspectRatio: 16 / 9,
                                          viewportFraction: 1.0,
                                          initialPage: 0,
                                          enableInfiniteScroll: true,
                                          enlargeCenterPage: true,
                                          reverse: false,
                                          autoPlay: true,
                                          scrollDirection: Axis.horizontal,
                                        ))
                                    : Container()
                                : snap.data.data.advertisements.startPage !=
                                        null
                                    ? snap.data.data.advertisements.startPage
                                                .length >=
                                            1
                                        ? CarouselSlider.builder(
                                            itemCount: snap
                                                .data
                                                .data
                                                .advertisements
                                                .startPage
                                                .length,
                                            itemBuilder: (context, index, __) {
                                              return Consumer<SearchProvider>(
                                                  builder:
                                                      (context, search, _) {
                                                return InkWell(
                                                  onTap: () {
                                                    goForwardMethod(
                                                        token: widget.token,
                                                        url: snap
                                                            .data
                                                            .data
                                                            .advertisements
                                                            .startPage[index].advertisementsUrl,
                                                        appCurrency:
                                                            widget.appCurrency,
                                                        cartList: widget
                                                            .cart.cartList,
                                                        searchProvider: search,
                                                        type: snap
                                                            .data
                                                            .data
                                                            .advertisements
                                                            .startPage[index]
                                                            .advertisementsType,
                                                        value: snap
                                                            .data
                                                            .data
                                                            .advertisements
                                                            .startPage[index]
                                                            .advertisementsValue);

                                                  },
                                                  child: CachedNetworkImage(
                                                      imageUrl: SystemControls()
                                                          .GetImgPath(
                                                              snap
                                                                  .data
                                                                  .data
                                                                  .advertisements
                                                                  .startPage[
                                                                      index]
                                                                  .advertisementsImg,
                                                              "original"),
                                                      fit: BoxFit.cover,
                                                      height: 25.h,
                                                      width: 100.w,
                                                      errorWidget: (context,
                                                              url, error) =>
                                                          Icon(Icons.error)),
                                                );
                                              });
                                            },
                                            options: CarouselOptions(
                                              // autoPlayAnimationDuration: Duration(seconds: 1),
                                              height: 25.h,
                                              autoPlayCurve:
                                                  Curves.fastOutSlowIn,
                                              aspectRatio: 16 / 9,
                                              viewportFraction: 1.0,
                                              initialPage: 0,
                                              enableInfiniteScroll: true,
                                              enlargeCenterPage: true,
                                              reverse: false,
                                              autoPlay: true,
                                              scrollDirection: Axis.horizontal,
                                            ))
                                        : Container()
                                    : Container(),

                            SizedBox(
                              height: 3.h,
                            ),
                            Consumer2<HomeProvider, CartStateProvider>(
                                builder: (context, home, cart, _) {
                              return SectionTitle(
                                title: "specialProduct",
                                function: () {
                                  home.resetData();
                                  home.productDiscountList.clear();
                                  home.specialProducts.clear();
                                  home.fetchHomeItems(
                                      context, home.currentPage);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => SpecialProducts(
                                                token: widget.token,
                                                appCurrency: widget.appCurrency,
                                              )));
                                },
                              );
                            }),
                            SizedBox(
                              height: 1.h,
                            ),
                            SectionWidget(
                              productShared: snap.data.data.homeProducts,
                              token: widget.token,
                              cartList: widget.cart.cartList,
                              appCurrency: widget.appCurrency,
                            ),
                            SizedBox(
                              height: 1.h,
                            ),
                            snap.data.data.advertisements.middlePage != null
                                ? snap.data.data.advertisements.middlePage
                                            .length >=
                                        1
                                    ? Container(
                                        height: 30.h,
                                        child: ListView.builder(
                                          shrinkWrap: true,
                                          scrollDirection: Axis.horizontal,
                                          itemCount: snap.data.data
                                              .advertisements.middlePage.length,
                                          itemBuilder: (context, index) {
                                            return Consumer<SearchProvider>(
                                                builder: (context, search, _) {
                                              return InkWell(
                                                onTap: () {
                                                  goForwardMethod(
                                                      token: widget.token,
                                                      appCurrency:
                                                          widget.appCurrency,
                                                      url: snap
                                                          .data
                                                          .data
                                                          .advertisements
                                                          .middlePage[index].advertisementsUrl,
                                                      cartList:
                                                          widget.cart.cartList,
                                                      searchProvider: search,
                                                      type: snap
                                                          .data
                                                          .data
                                                          .advertisements
                                                          .middlePage[index]
                                                          .advertisementsType,
                                                      value: snap
                                                          .data
                                                          .data
                                                          .advertisements
                                                          .middlePage[index]
                                                          .advertisementsValue);
                                                },
                                                child: CachedNetworkImage(
                                                    imageUrl: SystemControls()
                                                        .GetImgPath(
                                                            snap
                                                                .data
                                                                .data
                                                                .advertisements
                                                                .middlePage[
                                                                    index]
                                                                .advertisementsImg,
                                                            "original"),
                                                    fit: BoxFit.cover,
                                                    height: 30.h,
                                                    width: 100.w,
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            Icon(Icons.error)),
                                              );
                                            });
                                          },
                                        ),
                                      )
                                    : Container()
                                : Container(),
                            SizedBox(
                              height: 1.h,
                            ),
                            Consumer2<HomeProvider, CartStateProvider>(
                                builder: (context, home, cart, _) {
                              return SectionTitle(
                                title: "BestSale",
                                function: () {
                                  home.resetData();
                                  home.bestSaleList.clear();
                                  home.fetchBestSaleProducts(
                                      context, home.currentPage);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => BestSaleWidet(
                                                token: widget.token,
                                                appCurrency: widget.appCurrency,
                                                cartList: cart.cartList,
                                                type: "BestSale",
                                              )));
                                },
                              );
                            }),
                            SizedBox(
                              height: 1.h,
                            ),
                            SectionWidget(
                              productShared: snap.data.data.bestSales,
                              token: widget.token,
                              cartList: widget.cart.cartList,
                              appCurrency: widget.appCurrency,
                            ),

                            snap.data.data.advertisements.sectionalBanners !=
                                    null
                                ? SizedBox(
                                    height: 3.h,
                                  )
                                : Container(),

                            snap.data.data.advertisements.sectionalBanners !=
                                    null
                                ? snap.data.data.advertisements.sectionalBanners
                                            .length >
                                        0
                                    ? snap.data.data.advertisements
                                                .sectionalBanners.length >
                                            4
                                        ? Container(
                                            child: ListView.builder(
                                              shrinkWrap: true,
                                              physics:
                                                  NeverScrollableScrollPhysics(),
                                              itemCount: (snap
                                                          .data
                                                          .data
                                                          .advertisements
                                                          .sectionalBanners
                                                          .length /
                                                      4)
                                                  .ceil(),
                                              itemBuilder: (context, index) {
                                                return snap
                                                                .data
                                                                .data
                                                                .advertisements
                                                                .sectionalBanners
                                                                .length -
                                                            (index * 4) <=
                                                        3
                                                    ? Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .only(top: 8.0),
                                                        child: Column(
                                                          children:
                                                              List.generate(
                                                                  snap
                                                                          .data
                                                                          .data
                                                                          .advertisements
                                                                          .sectionalBanners
                                                                          .length -
                                                                      (index *
                                                                          4),
                                                                  (secondIndex) =>
                                                                      Column(
                                                                        children: [
                                                                          BannersWidgetSection(
                                                                            image:
                                                                                snap.data.data.advertisements.sectionalBanners[4 + secondIndex].advertisementsImg,
                                                                            cartList:
                                                                                widget.cart.cartList,
                                                                            token:
                                                                                widget.token,
                                                                            appCurrency:
                                                                                widget.appCurrency,
                                                                            value:
                                                                                snap.data.data.advertisements.sectionalBanners[4 + secondIndex].advertisementsValue,
                                                                            type:
                                                                                snap.data.data.advertisements.sectionalBanners[4 + secondIndex].advertisementsType,
                                                                            url:
                                                                                snap.data.data.advertisements.sectionalBanners[4 + secondIndex].advertisementsUrl,

                                                                          ),
                                                                          SizedBox(
                                                                            height:
                                                                                2.h,
                                                                          ),
                                                                        ],
                                                                      )),
                                                        ),
                                                      )
                                                    : Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .only(top: 8.0),
                                                        child: Column(
                                                          children: [
                                                            BannersWidgetSection(

                                                              image: snap
                                                                  .data
                                                                  .data
                                                                  .advertisements
                                                                  .sectionalBanners[snap.data.data.advertisements.sectionalBanners.length *
                                                                              index ==
                                                                          0
                                                                      ? 0
                                                                      : snap
                                                                              .data
                                                                              .data
                                                                              .advertisements
                                                                              .sectionalBanners
                                                                              .length -
                                                                          index *
                                                                              4]
                                                                  .advertisementsImg,
                                                              cartList: widget
                                                                  .cart
                                                                  .cartList,
                                                              token:
                                                                  widget.token,
                                                              appCurrency: widget
                                                                  .appCurrency,
                                                              value: snap
                                                                  .data
                                                                  .data
                                                                  .advertisements
                                                                  .sectionalBanners[snap.data.data.advertisements.sectionalBanners.length *
                                                                              index ==
                                                                          0
                                                                      ? 0
                                                                      : snap
                                                                              .data
                                                                              .data
                                                                              .advertisements
                                                                              .sectionalBanners
                                                                              .length -
                                                                          index *
                                                                              4]
                                                                  .advertisementsValue,
                                                              type: snap
                                                                  .data
                                                                  .data
                                                                  .advertisements
                                                                  .sectionalBanners[snap.data.data.advertisements.sectionalBanners.length *
                                                                              index ==
                                                                          0
                                                                      ? 0
                                                                      : snap
                                                                              .data
                                                                              .data
                                                                              .advertisements
                                                                              .sectionalBanners
                                                                              .length -
                                                                          index *
                                                                              4]
                                                                  .advertisementsType,
                                                              url: snap
                                                                  .data
                                                                  .data
                                                                  .advertisements
                                                                  .sectionalBanners[snap.data.data.advertisements.sectionalBanners.length *
                                                                              index ==
                                                                          0
                                                                      ? 0
                                                                      : snap
                                                                              .data
                                                                              .data
                                                                              .advertisements
                                                                              .sectionalBanners
                                                                              .length -
                                                                          index *
                                                                              4]
                                                                  .advertisementsUrl,
                                                            ),
                                                            SizedBox(
                                                              height: 2.h,
                                                            ),
                                                            BannersWidgetSection(
                                                              image: snap
                                                                  .data
                                                                  .data
                                                                  .advertisements
                                                                  .sectionalBanners[snap.data.data.advertisements.sectionalBanners.length *
                                                                              index ==
                                                                          0
                                                                      ? 1
                                                                      : snap
                                                                              .data
                                                                              .data
                                                                              .advertisements
                                                                              .sectionalBanners
                                                                              .length -
                                                                          index *
                                                                              3]
                                                                  .advertisementsImg,
                                                              cartList: widget
                                                                  .cart
                                                                  .cartList,
                                                              token:
                                                                  widget.token,
                                                              appCurrency: widget
                                                                  .appCurrency,
                                                              value: snap
                                                                  .data
                                                                  .data
                                                                  .advertisements
                                                                  .sectionalBanners[snap.data.data.advertisements.sectionalBanners.length *
                                                                              index ==
                                                                          0
                                                                      ? 1
                                                                      : snap
                                                                              .data
                                                                              .data
                                                                              .advertisements
                                                                              .sectionalBanners
                                                                              .length -
                                                                          index *
                                                                              3]
                                                                  .advertisementsValue,
                                                              type: snap
                                                                  .data
                                                                  .data
                                                                  .advertisements
                                                                  .sectionalBanners[snap.data.data.advertisements.sectionalBanners.length *
                                                                              index ==
                                                                          0
                                                                      ? 1
                                                                      : snap
                                                                              .data
                                                                              .data
                                                                              .advertisements
                                                                              .sectionalBanners
                                                                              .length -
                                                                          index *
                                                                              1]
                                                                  .advertisementsType,
                                                              url: snap
                                                                  .data
                                                                  .data
                                                                  .advertisements
                                                                  .sectionalBanners[snap.data.data.advertisements.sectionalBanners.length *
                                                                              index ==
                                                                          0
                                                                      ? 1
                                                                      : snap
                                                                              .data
                                                                              .data
                                                                              .advertisements
                                                                              .sectionalBanners
                                                                              .length -
                                                                          index *
                                                                              1]
                                                                  .advertisementsUrl,
                                                            ),
                                                            SizedBox(
                                                              height: 2.h,
                                                            ),
                                                            Container(
                                                              width: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width -
                                                                  20,
                                                              height: 20.h,
                                                              child: Row(
                                                                children: [
                                                                  BannersWidgetSection(
                                                                    image: snap
                                                                        .data
                                                                        .data
                                                                        .advertisements
                                                                        .sectionalBanners[snap.data.data.advertisements.sectionalBanners.length * index ==
                                                                                0
                                                                            ? 2
                                                                            : snap.data.data.advertisements.sectionalBanners.length -
                                                                                index * 2]
                                                                        .advertisementsImg,
                                                                    cartList: widget
                                                                        .cart
                                                                        .cartList,
                                                                    widthRatio:
                                                                        2.05,
                                                                    token: widget
                                                                        .token,
                                                                    appCurrency:
                                                                        widget
                                                                            .appCurrency,
                                                                    value: snap
                                                                        .data
                                                                        .data
                                                                        .advertisements
                                                                        .sectionalBanners[snap.data.data.advertisements.sectionalBanners.length * index ==
                                                                                0
                                                                            ? 2
                                                                            : snap.data.data.advertisements.sectionalBanners.length -
                                                                                index * 2]
                                                                        .advertisementsValue,
                                                                    type: snap
                                                                        .data
                                                                        .data
                                                                        .advertisements
                                                                        .sectionalBanners[snap.data.data.advertisements.sectionalBanners.length * index ==
                                                                                0
                                                                            ? 2
                                                                            : snap.data.data.advertisements.sectionalBanners.length -
                                                                                index * 2]
                                                                        .advertisementsType,
                                                                    url: snap
                                                                        .data
                                                                        .data
                                                                        .advertisements
                                                                        .sectionalBanners[snap.data.data.advertisements.sectionalBanners.length * index ==
                                                                                0
                                                                            ? 2
                                                                            : snap.data.data.advertisements.sectionalBanners.length -
                                                                                index * 2]
                                                                        .advertisementsUrl,
                                                                  ),
                                                                  Spacer(),
                                                                  BannersWidgetSection(
                                                                    widthRatio:
                                                                        2.05,
                                                                    image: snap
                                                                        .data
                                                                        .data
                                                                        .advertisements
                                                                        .sectionalBanners[snap.data.data.advertisements.sectionalBanners.length * index ==
                                                                                0
                                                                            ? 3
                                                                            : snap.data.data.advertisements.sectionalBanners.length -
                                                                                index * 1]
                                                                        .advertisementsImg,
                                                                    cartList: widget
                                                                        .cart
                                                                        .cartList,
                                                                    token: widget
                                                                        .token,
                                                                    appCurrency:
                                                                        widget
                                                                            .appCurrency,
                                                                    value: snap
                                                                        .data
                                                                        .data
                                                                        .advertisements
                                                                        .sectionalBanners[snap.data.data.advertisements.sectionalBanners.length * index ==
                                                                                0
                                                                            ? 3
                                                                            : snap.data.data.advertisements.sectionalBanners.length -
                                                                                index * 1]
                                                                        .advertisementsValue,
                                                                    type: snap
                                                                        .data
                                                                        .data
                                                                        .advertisements
                                                                        .sectionalBanners[snap.data.data.advertisements.sectionalBanners.length * index ==
                                                                                0
                                                                            ? 3
                                                                            : snap.data.data.advertisements.sectionalBanners.length -
                                                                                index * 1]
                                                                        .advertisementsType,
                                                                    url: snap
                                                                        .data
                                                                        .data
                                                                        .advertisements
                                                                        .sectionalBanners[snap.data.data.advertisements.sectionalBanners.length * index ==
                                                                                0
                                                                            ? 3
                                                                            : snap.data.data.advertisements.sectionalBanners.length -
                                                                                index * 1]
                                                                        .advertisementsUrl,
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      );
                                              },
                                            ),
                                          )
                                        : Container(
                                            child: Column(
                                              children: List.generate(
                                                snap.data.data.advertisements
                                                    .sectionalBanners.length,
                                                (secondIndex) =>
                                                    BannersWidgetSection(
                                                  image: snap
                                                      .data
                                                      .data
                                                      .advertisements
                                                      .sectionalBanners[
                                                          secondIndex]
                                                      .advertisementsImg,
                                                  cartList:
                                                      widget.cart.cartList,
                                                  token: widget.token,
                                                  appCurrency:
                                                      widget.appCurrency,
                                                  value: snap
                                                      .data
                                                      .data
                                                      .advertisements
                                                      .sectionalBanners[
                                                          secondIndex]
                                                      .advertisementsValue,
                                                  type: snap
                                                      .data
                                                      .data
                                                      .advertisements
                                                      .sectionalBanners[
                                                          secondIndex]
                                                      .advertisementsType,
                                                      url: snap
                                                      .data
                                                      .data
                                                      .advertisements
                                                      .sectionalBanners[
                                                          secondIndex]
                                                      .advertisementsUrl,
                                                ),
                                              ),
                                            ),
                                          )
                                    : Container()
                                : Container(),
                            SizedBox(
                              height: 3.h,
                            ),

                            ///   seee

                            Consumer2<HomeProvider, CartStateProvider>(
                                builder: (context, home, cart, _) {
                              return SectionTitle(
                                title: "discount",
                                function: () {
                                  home.changeCurrentHomeWidgetIndex(2, context);
                                },
                              );
                            }),
                            SizedBox(
                              height: 1.h,
                            ),
                            SectionWidget(
                              productShared: snap.data.data.discountProducts,
                              token: widget.token,
                              cartList: widget.cart.cartList,
                              appCurrency: widget.appCurrency,
                            ),
                            SizedBox(
                              height: 2.h,
                            ),
                            Consumer2<HomeProvider, CartStateProvider>(
                                builder: (context, home, cart, _) {
                              return SectionTitle(
                                title: "seactions",
                                function: () {
                                  home.changeCurrentHomeWidgetIndex(1, context);
                                },
                              );
                            }),
                            SizedBox(
                              height: 1.h,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              height: EasyLocalization.of(context)
                                          .currentLocale
                                          .languageCode ==
                                      "en"
                                  ? 17.h
                                  : 19.h,
                              child: Consumer2<HomeProvider, SearchProvider>(
                                builder: (context, home, search, _) {
                                  return ListView.builder(
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    itemCount: snap.data.data.categories.length,
                                    itemBuilder: (context, index) {
                                      return Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: InkWell(
                                          onTap: () {
                                            search.restData();
                                            search.fetchProductById(
                                                context,
                                                snap.data.data.categories[index]
                                                    .categoriesId,
                                                search.currentPage);
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        TopTabBarProductDisplayTheamTwo(
                                                          mainCategoryId: snap
                                                              .data
                                                              .data
                                                              .categories[index]
                                                              .categoriesId,
                                                          title: snap
                                                              .data
                                                              .data
                                                              .categories[index]
                                                              .categoriesTitle,
                                                          categories: snap
                                                              .data
                                                              .data
                                                              .categories[index]
                                                              .subcategories,
                                                          indexData: 0,
                                                          appCurency: widget
                                                              .appCurrency,
                                                          token: widget.token,
                                                        )));
                                          },
                                          child: Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                2.5,
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    width: 1,
                                                    color: globals.accountColorsData[
                                                                'BorderColor'] !=
                                                            null
                                                        ? Color(int.parse(globals
                                                                .accountColorsData[
                                                            'BorderColor']))
                                                        : SystemControls
                                                            .BorderColorz),
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.only(
                                                          topLeft:
                                                              Radius.circular(
                                                                  10),
                                                          topRight:
                                                              Radius.circular(
                                                                  10)),
                                                  child: CachedNetworkImage(
                                                    imageUrl: SystemControls()
                                                        .GetImgPath(
                                                            snap
                                                                .data
                                                                .data
                                                                .categories[
                                                                    index]
                                                                .categoriesImg,
                                                            "original"),
                                                    placeholder:
                                                        (context, url) =>
                                                            Center(
                                                      child: SystemControls()
                                                          .circularProgress(),
                                                    ),
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            Center(
                                                      child: Icon(
                                                          Icons.broken_image),
                                                    ),
                                                    fit: BoxFit.cover,
                                                    height: 12.h,
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width /
                                                            2.5,
                                                  ),
                                                ),
                                                Spacer(),
                                                Text(
                                                  "${snap.data.data.categories[index].categoriesTitle}",
                                                  style: TextStyle(
                                                      color: globals.accountColorsData[
                                                                  'MainColor'] !=
                                                              null
                                                          ? Color(int.parse(
                                                              globals.accountColorsData[
                                                                  'MainColor']))
                                                          : SystemControls
                                                              .MainColorz,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                                Spacer(),
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  );
                                },
                              ),
                            ),
                            SizedBox(
                              height: 3.h,
                            ),
                            snap.data.data.advertisements.endPage != null
                                ? snap.data.data.advertisements.endPage
                                            .length >=
                                        1
                                    ? Consumer<SearchProvider>(
                                        builder: (context, search, _) {
                                        return Container(
                                          height: 30.h,
                                          child: ListView.builder(
                                            shrinkWrap: true,
                                            scrollDirection: Axis.horizontal,
                                            itemCount: snap.data.data
                                                .advertisements.endPage.length,
                                            itemBuilder: (context, index) {
                                              return InkWell(
                                                onTap: () {
                                                  goForwardMethod(
                                                      searchProvider: search,
                                                      value: snap
                                                          .data
                                                          .data
                                                          .advertisements
                                                          .endPage[index]
                                                          .advertisementsValue,
                                                      url: snap
                                                          .data
                                                          .data
                                                          .advertisements
                                                          .endPage[index].advertisementsUrl,
                                                      token: widget.token,
                                                      cartList:
                                                          widget.cart.cartList,
                                                      type: snap
                                                          .data
                                                          .data
                                                          .advertisements
                                                          .endPage[index]
                                                          .advertisementsType,
                                                      appCurrency:
                                                          widget.appCurrency);
                                                },
                                                child: CachedNetworkImage(
                                                    imageUrl: SystemControls()
                                                        .GetImgPath(
                                                            snap
                                                                .data
                                                                .data
                                                                .advertisements
                                                                .endPage[index]
                                                                .advertisementsImg,
                                                            "original"),
                                                    fit: BoxFit.cover,
                                                    height: 30.h,
                                                    width: 100.w,
                                                    errorWidget:
                                                        (context, url, error) =>
                                                            Icon(Icons.error)),
                                              );
                                            },
                                          ),
                                        );
                                      })
                                    : Container()
                                : Container(),
                            SizedBox(
                              height: 2.h,
                            ),
                          ],
                        );
                      } else {
                        return Container(
                          height: 100.h,
                          child: Indicator(),
                        );
                      }
                    }),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class SectionTitle extends StatelessWidget {
   SectionTitle({Key key, this.title, this.function}) : super(key: key);
  final String title;
  final Function function;
  Future<CartList> localCart = read_from_file();

  @override
  Widget build(BuildContext context) {
    localCart = read_from_file();
    return InkWell(
      onTap: function,
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 16),
              child: Text(
                "$title".tr().toString(),
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 17),
              ),
            ),
            Spacer(),
            // SpecialProducts
            IconButton(
              icon: Icon(
                Icons.arrow_forward,
                color: globals.accountColorsData['MainColor'] != null
                    ? Color(int.parse(globals.accountColorsData['MainColor']))
                    : SystemControls.MainColorz,
              ),
              onPressed: function,
            )
          ],
        ),
      ),
    );
  }
}

class SectionWidget extends StatelessWidget {
  Future<CartList> localCart = read_from_file();

  SectionWidget(
      {Key key,
      this.productShared,
      this.token,
      this.appCurrency,
      this.cartList})
      : super(key: key);
  final List<ProductShared> productShared;
  List<dynamic> cartList;
  final String token;
  final String appCurrency;
  @override
  Widget build(BuildContext context) {
    localCart = read_from_file();

    return Container(
      height: 40.h,
      child: ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: productShared.length,
        itemBuilder: (context, index) {
          return productShared.length == 0
              ? Container(
                  height: 100.h,
                  child: Center(
                    child: Indicator(),
                  ),
                )
              : ProductItemCardBase(
                  product: productShared[index],
                  cartList: cartList,
                  token: token,
                  appCurrency: appCurrency,
                  width: 45,
                  arabicFont: 10.5,
                );
        },
      ),
    );
  }
}

class BannersWidgetSection extends StatelessWidget {
  BannersWidgetSection(
      {Key key,
      this.token,
      this.appCurrency,
      this.cartList,
      this.image,
      this.type,
      this.value,
        this.url,
      this.widthRatio = 1})
      : super(key: key);
  List<dynamic> cartList;
  final String token;
  final String appCurrency;
  final String image;
  final String type;
  final String value;
  final String url;
  double widthRatio;

  launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  goForwardMethod(
      {String type,
      String value,
      String token,
      String appCurrency,
      List<dynamic> cartList,
      SearchProvider searchProvider,
      BuildContext context,String url}) {
    print("object");
    print(value);
    print(type);
     if(value == null && url  ==null){
       print('');
     }else{
       if(type!= null){
         switch (type) {
           case "product":
             Navigator.push(
                 context,
                 MaterialPageRoute(
                     builder: (context) => GetProductByIDScreen(
                       cartList: cartList,
                       token: token,
                       appCurrency: appCurrency,
                       id: int.parse("$value"),
                     )));
             break;
           case "filter":
             Navigator.push(
                 context,
                 MaterialPageRoute(
                     builder: (context) => SearchScreenNew(
                       cartList: cartList,
                       token: token,
                       appCurrency: appCurrency,
                     )));
             searchProvider.searchWithTitle(context, "$value");
             break;
           case "category":
             Navigator.push(
                 context,
                 MaterialPageRoute(
                     builder: (context) => SearchScreenNew(
                       cartList: cartList,
                       token: token,
                       appCurrency: appCurrency,
                     )));
             searchProvider.searchWithCatID(context, int.parse("$value"));
             break;
         }}else{
         launchURL(url);
       }
     }
  }
  Future<CartList> localCart = read_from_file();

  @override
  Widget build(BuildContext context) {
    localCart = read_from_file();

    return Consumer<SearchProvider>(
      builder: (context, search, _) {
        return InkWell(
          onTap: () {
            goForwardMethod(
                cartList: cartList,
                url:url,
                type: type,
                token: token,
                context: context,
                value: value,
                searchProvider: search,
                appCurrency: appCurrency);
          },
          child: Container(
            width: (MediaQuery.of(context).size.width - 20) / widthRatio,
            height: 20.h,
            child: CachedNetworkImage(
                imageUrl: SystemControls().GetImgPath(image, "original"),
                fit: BoxFit.cover,
                width: (MediaQuery.of(context).size.width - 20) / widthRatio,
                height: 20.h,
                errorWidget: (context, url, error) => Icon(Icons.error)),
          ),
        );
      },
    );
  }
}
