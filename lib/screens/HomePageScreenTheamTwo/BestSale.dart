

import '../../widgets/SharedWidget/NoItemScreen.dart';
import 'package:sizer/sizer.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../Models/HomeModel/BestSaleModel.dart';
import '../../Models/HomeModel/sharedModel.dart';
import '../../provider/HomeProvider/HomeProvider.dart';
import '../../provider/productsInCartProvider.dart';
import '../../widgets/SharedWidget/ProductItmsList.dart';
import '../../widgets/SharedWidget/ProgressIndecator.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:provider/provider.dart';
import '../../Controls.dart';
import '../../Repo/DiscountRepo.dart';
class BestSaleWidet extends StatefulWidget {
   BestSaleWidet({Key key,this.appCurrency,this.token,this.cartList,this.type});
  final String appCurrency;
  final String token;
  List<dynamic> cartList;
  final String type;


  @override
  _BestSaleWidetState createState() => _BestSaleWidetState();
}

class _BestSaleWidetState extends State<BestSaleWidet> {

  final PagingController<int, ProductShared> _pagingController =
  PagingController(firstPageKey: 0, invisibleItemsThreshold: 5);
  ScrollController scrollController;

  @override
  void initState() {
    scrollController = ScrollController(
      keepScrollOffset: true,initialScrollOffset: 0.0,
    );


    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });

    super.initState();
  }


  Future<void> _fetchPage(int pageKey) async {
    try {
      final newItems = await DiscountWithModelRepo(context:context,pageNum: "$pageKey").getDiscountProduct;
      bool isLastPage =  newItems.data.pagination.to >= newItems.data.pagination.total;


      print(newItems.data.pagination.currentPage);
      print(newItems.data.pagination.to);
      print(newItems.data.pagination.total);
        switch(isLastPage){
          case true:         _pagingController.appendLastPage(newItems.data.products);
          break;
          case false :
            _pagingController.appendPage(newItems.data.products, pageKey+1);
     break;
        }


    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _pagingController.dispose();
    super.dispose();

  }
  final _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {

     switch(widget.type){
       case "BestSale":
         return Consumer2<HomeProvider,CartStateProvider>(builder: (context,home,cart,_){
           return Scaffold(
             key: _scaffoldKey,

             appBar: AppBar(
               elevation: 0,
               title: Text( "BestSale".tr().toString(),),
               centerTitle: true,
             ),
             // body:  SingleChildScrollView(
             //   child: Column(
             //     children: [
             //       PagedGridView<int, ProductShared>(
             //
             //         // showNewPageProgressIndicatorAsGridChild: false,
             //         physics:  NeverScrollableScrollPhysics(),
             //         shrinkWrap: true,
             //         // showNewPageErrorIndicatorAsGridChild: false,
             //         // showNoMoreItemsIndicatorAsGridChild: false,
             //         pagingController: _pagingController,
             //         gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
             //           childAspectRatio: 2 / 2.7,
             //           crossAxisSpacing: 10,
             //           mainAxisSpacing: 10,
             //           crossAxisCount: 2,
             //         ),
             //
             //         builderDelegate: PagedChildBuilderDelegate<ProductShared>(
             //
             //
             //           itemBuilder: (context, item, index) => ProductItemCardBase(product: item,appCurrency:widget.appCurrency,token: widget.token,cartList: widget.cartList,),
             //         ),
             //       )
             //     ],
             //   ),
             // ),
             body: NotificationListener<ScrollNotification>(
               onNotification: ( scrollNotification){

                 if(scrollNotification.metrics.pixels ==
                     scrollNotification.metrics.maxScrollExtent &&home.currentPage < home.lastPage &&home.currentPage < home.nextPage){
                   print(scrollNotification.metrics.pixels);
                   home.fetchBestSaleProducts(
                       context,  home.currentPage++);
                 }
                 return false;
               },
               child: SingleChildScrollView(
                 // controller: scrollController,
                 child:
                 StreamBuilder<BestSaleModel>(
                   stream: home.streamBestSale,
                   builder: (context, snap) {

                     if (snap.hasData) {
                       if (snap.data.data.pagination.lastPage ==
                           home.currentPage) {
                         home.showLoader = false;

                       }
                       home.lastPage = snap.data.data.pagination.lastPage;
                       home.currentPage = snap.data.data.pagination.currentPage;
                       print('ffffffffffffffff');
                       print(home.bestSaleList.length);
                       print(snap.data.data.bestSales.length);
                       print(home.lastPage);
                       print(home.currentPage);
                       print("home.lastPage${home.lastPage}");

                       return snap.data.data.bestSales.length ==0? NoItemWidget(): ProductsGridView(
                         products: home.bestSaleList,
                         cartList: cart.cartList,
                         token: widget.token,
                         appCurrency: widget.appCurrency,
                         search: !home.showLoader,
                       );
                     } else if(snap.connectionState == ConnectionState.waiting) {
                       return Container(
                           height: 100.h,
                           child: Center(child: Indicator())
                       );
                     }else{
                       return Container();
                     }
                   },
                 ),
               ),
             ),
           );
         });
         break;
       case "SpecialProducts":
         return Consumer2<HomeProvider,CartStateProvider>(builder: (context,home,cart,_){
           return Scaffold(
             key: _scaffoldKey,

             appBar: AppBar(
               elevation: 0,
             ),
             // body:  SingleChildScrollView(
             //   child: Column(
             //     children: [
             //       PagedGridView<int, ProductShared>(
             //
             //         // showNewPageProgressIndicatorAsGridChild: false,
             //         physics:  NeverScrollableScrollPhysics(),
             //         shrinkWrap: true,
             //         // showNewPageErrorIndicatorAsGridChild: false,
             //         // showNoMoreItemsIndicatorAsGridChild: false,
             //         pagingController: _pagingController,
             //         gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
             //           childAspectRatio: 2 / 2.7,
             //           crossAxisSpacing: 10,
             //           mainAxisSpacing: 10,
             //           crossAxisCount: 2,
             //         ),
             //
             //         builderDelegate: PagedChildBuilderDelegate<ProductShared>(
             //
             //
             //           itemBuilder: (context, item, index) => ProductItemCardBase(product: item,appCurrency:widget.appCurrency,token: widget.token,cartList: widget.cartList,),
             //         ),
             //       )
             //     ],
             //   ),
             // ),
             body: NotificationListener<ScrollNotification>(
               onNotification: ( scrollNotification){

                 if(scrollNotification.metrics.pixels ==
                     scrollNotification.metrics.maxScrollExtent &&home.currentPage < home.lastPage &&home.currentPage < home.nextPage){
                   print(scrollNotification.metrics.pixels);
                   home.fetchBestSaleProducts(
                       context,  home.currentPage++);
                 }
                 return false;
               },
               child: SingleChildScrollView(
                 // controller: scrollController,
                 child:
                 StreamBuilder<BestSaleModel>(
                   stream: home.streamBestSale,
                   builder: (context, snap) {

                     if (snap.hasData) {
                       if (snap.data.data.pagination.lastPage ==
                           home.currentPage) {
                         home.showLoader = false;

                       }
                       home.lastPage = snap.data.data.pagination.lastPage;
                       home.currentPage = snap.data.data.pagination.currentPage;
                       print('ffffffffffffffff');
                       print(home.bestSaleList.length);
                       print(snap.data.data.bestSales.length);
                       print(home.lastPage);
                       print(home.currentPage);
                       print("home.lastPage${home.lastPage}");

                       return snap.data.data.bestSales.length ==0? NoItemWidget(): ProductsGridView(
                         products: home.bestSaleList,
                         cartList: cart.cartList,
                         token: widget.token,
                         appCurrency: widget.appCurrency,
                         search: !home.showLoader,
                       );
                     } else if(snap.connectionState == ConnectionState.waiting) {
                       return Container(
                           height: 100.h,
                           color: Colors.red,
                           child: Center(child: Indicator())
                       );
                     }else{
                       return Container();
                     }
                   },
                 ),
               ),
             ),
           );
         });
         break;
     }
  }
}


