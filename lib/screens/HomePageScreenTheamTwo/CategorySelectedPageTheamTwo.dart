import 'package:easy_localization/src/public_ext.dart';

import '../../Models/HomeModel/sharedModel.dart';
import '../../widgets/SharedWidget/NoItemScreen.dart';
import 'package:loadmore/loadmore.dart';

import '../../Models/HomeModel/CategoryDetailModel.dart';
import '../../Models/HomeModel/CategorySearchModel.dart';
import '../../provider/HomeProvider/SearchProvider.dart';
import '../../provider/productsInCartProvider.dart';
import '../../widgets/SharedWidget/ProductItmsList.dart';
import '../../widgets/SharedWidget/ProgressIndecator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../Controls.dart';
import '../../globals.dart' as globals;
import 'package:sizer/sizer.dart';

class TopTabBarProductDisplayTheamTwo extends StatefulWidget {
  TopTabBarProductDisplayTheamTwo(
      {Key key,
      this.categories,
      this.title,
      this.mainCategoryId,
      this.indexData,this.token,this.appCurency})
      : super(key: key);
  final String title;
  List<SharedCategory> categories;
  int mainCategoryId;
  int indexData;
  final String token;
  final String appCurency;

  @override
  _TopTabBarProductDisplayTheamTwoState createState() => _TopTabBarProductDisplayTheamTwoState();
}

class _TopTabBarProductDisplayTheamTwoState extends State<TopTabBarProductDisplayTheamTwo> {

  ScrollController scrollController;
  scrollListener() {
    // var searchProvider = Provider.of<SearchProvider>(context, listen: false);
    //
    // print(scrollController.position.pixels);
    // print(scrollController.position.maxScrollExtent);
    //
    //     if (searchProvider.currentPage < searchProvider.lastPage &&
    //         scrollController.position.pixels >=
    //             scrollController.position.maxScrollExtent ) {
    //       if (searchProvider.currentPage < searchProvider.nextPage) {
    //         print('products');
    //         searchProvider.fetchProductById(
    //             context, widget.mainCategoryId, searchProvider.currentPage++);
    //       }
    //     }




  }

  @override
  void initState() {
    // TODO: implement initState
    scrollController = ScrollController();
    scrollController.addListener(scrollListener);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Consumer2<SearchProvider,CartStateProvider>(builder: (context,search,cart,_){
      return DefaultTabController(
        initialIndex: widget.indexData,
        length: widget.categories.length + 1,
        child: Scaffold(
          backgroundColor: Colors.white,

          appBar: AppBar(
            centerTitle: true,
            title: Text("${widget.title}"),
            bottom: widget.categories.length != 0
                ? TabBar(

                isScrollable: true,
              onTap: (index) {
                search.restData();
              index == 0?   search.fetchProductById(context, widget.mainCategoryId, search.currentPage): search.fetchProductById(context, widget.categories[index-1].categoriesId, search.currentPage);
              },
              mouseCursor: MouseCursor.uncontrolled,
              tabs: List.generate(
                  widget.categories.length + 1,
                      (index) => index == 0
                      ? Container(
                          width: MediaQuery.of(context).size.width/2.4,
                          child: Text("allProducts".tr().toString()))
                      :  Container(
                          width: MediaQuery.of(context).size.width/ 2.4,child: Center(child: Text("${widget.categories[index-1].categoriesTitle}")))),
              indicatorWeight: 5.0,
              indicatorColor: globals.accountColorsData['MainColor'] != null
                  ? Color(int.parse(globals.accountColorsData['MainColor']))
                  : SystemControls.MainColorz,
            )
                : PreferredSize(
                child: Container(), preferredSize: Size.fromHeight(0)),
          ),
          body: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            children: List.generate(
              widget.categories.length + 1,
                  (index) => NotificationListener<ScrollNotification>(
                    onNotification: ( scrollNotification){

                  if(scrollNotification.metrics.pixels ==
                          scrollNotification.metrics.maxScrollExtent &&search.currentPage < search.lastPage &&search.currentPage < search.nextPage){
                    print(scrollNotification.metrics.pixels);
                    search.fetchProductById(
                            context, widget.mainCategoryId, search.currentPage++);
                      }
                      return false;
                    },
                    child: SingleChildScrollView(
                      // controller: scrollController,
                child:
                StreamBuilder<CategoriesDetailModel>(
                    stream: search.streamProductById,
                    builder: (context, snap) {

                      if (snap.hasData) {
                        if (snap.data.data.pagination.lastPage ==
                            search.currentPage) {
                          search.showLoader = false;

                        }
                        search.lastPage = snap.data.data.pagination.lastPage;
                        search.currentPage = snap.data.data.pagination.currentPage;
                        print('ffffffffffffffff');
                        print(search.lastPage);
                        print(search.currentPage);
                        print("home.lastPage${search.lastPage}");

                        return snap.data.data.products.length ==0? NoItemWidget(): ProductsGridView(
                          products: search.productSearchByIdList,
                          cartList: cart.cartList,
                          token: widget.token,
                          appCurrency: widget.appCurency,
                          search: !search.showLoader,
                        );
                      } else if(snap.connectionState == ConnectionState.waiting) {
                        return Container(
                          height: 100.h,
                          child: Center(child: Indicator())
                        );
                      }else{
                        return Container();
                      }
                    },
                ),
              ),
                  ),
            ),
          ),
        ),
      );
    });
  }
}
