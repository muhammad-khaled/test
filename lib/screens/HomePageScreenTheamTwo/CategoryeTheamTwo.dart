





import '../../Models/HomeModel/sharedModel.dart';
import '../../dataControl/cartModule.dart';
import '../../provider/productsInCartProvider.dart';

import '../../Models/HomeModel/CategorySearchModel.dart';
import '../../provider/ProviderTheamTwo/DisplayCategoryTheamTwo.dart';
import '../../services/HomeRepo.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import '../../provider/HomeProvider/SearchProvider.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../Controls.dart';
import '../../globals.dart' as globals;

import '../../widgets/SharedWidget/ProgressIndecator.dart';

import 'CategorySelectedPageTheamTwo.dart';
class CategoryTheamTwo extends StatelessWidget {
   CategoryTheamTwo({Key key,this.token,this.appCureency}) : super(key: key);
 final String token;
 final String appCureency;
  Future<CartList> localCart = read_from_file();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,

      // drawer: Consumer<CartStateProvider>(
      //   builder: (context, cart, _) {
      //     return DrawerWidget(
      //       token: userToken,
      //       hasRegistration: hasRegistration,
      //       isLogin: isLogging,
      //       currency: appCurrency,
      //       appInfo: appInitialInfo,drawer: toggleDrawer,);
      //   },
      // ),
      body: Consumer<DisplayCategoryTheamTwoProvider>(builder: (context,category,_){ return FutureBuilder<CategorySearchModel>(
          future:GetCategorySearchTreeRepo(context: context)
              .getCategorySearch,
          builder: (context,snap){

            if(snap.hasData){
              return Container(
                width: MediaQuery.of(context).size.width,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,

                  children: [
                    Consumer<CartStateProvider>(
                      builder: (context, cart, _) {
                        return FutureBuilder<CartList>(
                          future: localCart,
                          builder: (context, Cartsnap) {
                            if (Cartsnap.connectionState ==
                                ConnectionState.done &&
                                Cartsnap.hasData) {
                              cart.setCartList(Cartsnap.data.Cmeals);

                              CartStateProvider cartState =
                              Provider.of<CartStateProvider>(context,
                                  listen: false);
                              if (cartState.productsCount == "0" &&
                                  cart.cartList.length != 0) {
                                cartState.setCurrentProductsCount(
                                    cart.cartList.length.toString());
                              }
                              return Container();
                            }
                            return Container();
                          },
                        );
                      },
                    ),
                    SingleChildScrollView(
                      child: Container(
                        width: MediaQuery.of(context).size.width*.8/3,
                        height: 72.h,
                        // color: Colors.red,
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: snap.data.data.categories.length,
                          physics: ScrollPhysics(),
                          itemBuilder: (context,index){
                            return Padding(
                              padding: const EdgeInsets.only(top: 2,bottom: 2),
                              child: InkWell(
                                onTap: (){
                                  category.changeCategory(index);
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width*1/3,
                                  height: 12.h,
                                  decoration: BoxDecoration(
                                      border: EasyLocalization.of(context).currentLocale.languageCode =="en" ?Border(
                                          right:  BorderSide(
                                              color: category.currentCategory == index ?  globals.accountColorsData['MainColor'] !=
                                                  null
                                                  ? Color(int.parse(globals.accountColorsData[
                                              'MainColor']))
                                                  : SystemControls
                                                  .MainColorz :Colors.transparent,
                                              width: 5
                                          )
                                      ):Border(
                                          left:  BorderSide(
                                              color: category.currentCategory == index ?  globals.accountColorsData['MainColor'] !=
                                                  null
                                                  ? Color(int.parse(globals.accountColorsData[
                                              'MainColor']))
                                                  : SystemControls
                                                  .MainColorz :Colors.transparent,
                                              width: 5
                                          )
                                      )
                                  ),
                                  child: Stack(

                                    children: [
                                      Center(
                                        child: CachedNetworkImage(
                                          imageUrl: SystemControls()
                                              .GetImgPath(
                                              snap
                                                  .data
                                                  .data
                                                  .categories[
                                              index]
                                                  .categoriesImg
                                                  .toString(),
                                              "original"),
                                          placeholder:
                                              (context, url) =>
                                              Center(
                                                child: SystemControls()
                                                    .circularProgress(),
                                              ),
                                          errorWidget: (context,
                                              url, error) =>
                                              Center(
                                                child: Icon(
                                                    Icons.broken_image),
                                              ),
                                          fit: BoxFit.cover,
                                          width: 30.w,
                                          height: 11.h,
                                        ),
                                      ),
                                      Positioned(
                                        bottom: 0,
                                        left: 0,
                                        child: Container(
                                          height: 4.h,
                                          width: 26.w,
                                          color: Colors.transparent,
                                          child: Center(
                                            child: Text(

                                              "${ snap
                                                  .data
                                                  .data
                                                  .categories[
                                              index].categoriesTitle }"
                                                  ,
                                              style: TextStyle(

                                                color: Colors.white,
                                                fontSize: 9.sp,
                                                fontWeight: FontWeight.bold
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ),
                                      )

                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),

                      ),

                    ) ,Spacer(),
                    SingleChildScrollView(
                      child: Consumer2<SearchProvider,DisplayCategoryTheamTwoProvider>(
                        builder: (context,search,category,_){
                          return  Container(
                            width: MediaQuery.of(context).size.width*2/3,

                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                            snap.data.data.categories[category.currentCategory].categoriesBanner != null ? InkWell(
                              onTap: (){
                                search.restData();
                                search.fetchProductById(context, snap.data.data.categories[category.currentCategory].categoriesId, search.currentPage);
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>TopTabBarProductDisplayTheamTwo(mainCategoryId: snap.data.data.categories[category.currentCategory].categoriesId,title: snap.data.data.categories[category.currentCategory].categoriesTitle,categories: snap.data.data.categories[category.currentCategory].subcategories,indexData: 0,appCurency: appCureency ,token: token,)));

                              },
                              child: Container(height: 13.h,width: MediaQuery.of(context).size.width*2/3,child: CachedNetworkImage(
                                  imageUrl: SystemControls()
                                      .GetImgPath(
                                      snap.data.data.categories[category.currentCategory].categoriesBanner,
                                      "original"),
                                  fit: BoxFit.fill,
                                  height: 20.h,
                                  width: 100.w,
                                  errorWidget:
                                      (context, url, error) =>
                                      Icon(Icons.error)),),
                            ):Container(),
                              InkWell(
                                onTap: (){
                                  search.restData();
                                  search.fetchProductById(context, snap.data.data.categories[category.currentCategory].categoriesId, search.currentPage);
                                  Navigator.push(context, MaterialPageRoute(builder: (context)=>TopTabBarProductDisplayTheamTwo(mainCategoryId: snap.data.data.categories[category.currentCategory].categoriesId,title: snap.data.data.categories[category.currentCategory].categoriesTitle,categories: snap.data.data.categories[category.currentCategory].subcategories,indexData: 0,appCurency: appCureency ,token: token,)));

                                },
                                child: Container(height: 5.h,width: MediaQuery.of(context).size.width*2/3,
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Spacer(),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 18.0),
                                      child: Text("allProducts".tr().toString(),style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 16),),
                                    ),
                                    Spacer(),
                                  ],
                                ),),
                              ),
                              Consumer<SearchProvider>(builder: (context,search,_){return  CategoryBuilder(categoryProvider: category,categories: snap.data.data.categories[category.currentCategory].subcategories,token: token,appCureency: appCureency,style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 16),);})

                            ],),

                          );
                        },
                      ),
                    ),
                    Spacer(),
                  ],
                ),
              );
            }else{
              return Container(height: 100.h,child: Center(child: Indicator(),));
            }
          });}),
    );
  }
}
class CategoryBuilder extends StatelessWidget {
  CategoryBuilder({Key key,this.categoryProvider,this.categories,this.token,this.appCureency,this.style}) : super(key: key);
  DisplayCategoryTheamTwoProvider categoryProvider;
  List<SharedCategory> categories;
  final String token;
  final String appCureency;
  final TextStyle style;
  @override
  Widget build(BuildContext context) {
    return  Consumer<SearchProvider>(builder: (context,search,_){
      return ListView.builder(
        itemCount: categories.length,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context,index){
          return Column(
            children: [

              Theme(
                data: ThemeData(
                    dividerColor: Colors.transparent

                ),


                child: ExpansionTile(

                  iconColor: Colors.black,
                  initiallyExpanded: categories[index].subcategories.length >1 ? false : false,
                  title: Text("${categories[index].categoriesTitle}",style: style,),
                  children:[
                    Container(

                        padding:EasyLocalization.of(context).currentLocale.languageCode =="en" ? EdgeInsets.only(left: 16):EdgeInsets.only(right: 16),
                        width: MediaQuery.of(context).size.width*2/3,
                        height: 4.h,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            InkWell(
                              onTap: (){


                                search.restData();
                                search.fetchProductById(context, categories[index].categoriesId, search.currentPage);
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>TopTabBarProductDisplayTheamTwo(mainCategoryId: categories[index].categoriesId,title: categories[index].categoriesTitle,categories: categories[index].subcategories,indexData: 0,appCurency:appCureency ,token: token,)));
                              },
                              child: Container(
                                  height: 4.h,
                                  width: MediaQuery.of(context).size.width*2/3,
                                  child: Text("viewAll".tr().toString(),)),),
                            Spacer(flex: 3,),

                          ],
                        )),
                    Padding(
                      padding:EasyLocalization.of(context).currentLocale.languageCode =="en" ? const EdgeInsets.only(left: 8.0,bottom: 8):const EdgeInsets.only(right: 8.0,bottom: 8),
                      child: Column(

                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: List.generate(categories[index].subcategories.length, (secondIndex) => Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            InkWell(
                              onTap: (){


                                search.restData();
                                search.fetchProductById(context, categories[index].subcategories[secondIndex].categoriesId, search.currentPage);
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>TopTabBarProductDisplayTheamTwo(mainCategoryId: categories[index].subcategories[secondIndex].categoriesId,title: categories[index].subcategories[secondIndex].categoriesTitle,categories: categories[index].subcategories,indexData: secondIndex+1,appCurency:appCureency ,token: token,)));
                              },
                              child: Container(
                                  padding:EasyLocalization.of(context).currentLocale.languageCode =="en" ? EdgeInsets.only(left: 10):EdgeInsets.only(right: 10),
                                  width: MediaQuery.of(context).size.width*2/3,
                                  height:EasyLocalization.of(context).currentLocale.languageCode =="en" ?2.h: 4.h,
                                  child: Text("${categories[index].subcategories[secondIndex].categoriesTitle}")),
                            ),




                            Padding(
                              padding: const EdgeInsets.only(left: 10,bottom: 10),
                              child: CategoryBuilder(categories: categories[index].subcategories[secondIndex].subcategories,categoryProvider: categoryProvider,appCureency: appCureency,token: token,style: TextStyle(color: Colors.black,fontWeight: FontWeight.normal),),
                            )
                          ],
                        )),
                      ),
                    )
                  ],


                ),
              ),

            ],
          );
        },
      );
    });
  }
}
