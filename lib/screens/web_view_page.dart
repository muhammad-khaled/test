import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/webview_flutter.dart';
import '../screens/home.dart';
import '../screens/payMentStatusLayout.dart';
import '../widgets/UI_Alert_Widgets.dart';
import 'package:provider/provider.dart';
import 'dart:io';
import 'package:http/http.dart' as http;

class WebViewPage extends StatefulWidget {
  final String url;
  String lang;
  final String token;
  final Function(String) onPageLoadingFinishCallback;

  WebViewPage({
    this.url,
    this.token,
    this.onPageLoadingFinishCallback,
    this.lang
  });

  @override
  _WebViewPageState createState() => _WebViewPageState();
}

class _WebViewPageState extends State<WebViewPage> {

  // final flutterWebViewPlugin = FlutterWebviewPlugin();
  Completer<WebViewController> controller = Completer<WebViewController>();
  WebViewController _controller;
  WebViewController controllerr;
  // On urlChanged stream
  StreamSubscription<String> _onUrlChanged;

  void initState() {
    super.initState();
    // Add a listener to on url changed
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();

  }

  @override
  void dispose() {
    // Every listener should be canceled, the same should be done with this stream.
    _onUrlChanged.cancel();


    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    GlobalKey<NavigatorState> _key = GlobalKey();
    var verticalGestures = Factory<VerticalDragGestureRecognizer>(
            () => VerticalDragGestureRecognizer());

    var horizontalGestures = Factory<HorizontalDragGestureRecognizer>(
            () => HorizontalDragGestureRecognizer());

    var gestureSet = Set<Factory<OneSequenceGestureRecognizer>>.from([
      verticalGestures,
      horizontalGestures,
      Factory(() => EagerGestureRecognizer()),
    ]);

    print('\n\n url web_view: ${widget.url}\n\n');

    Map<String, String> headerParams = {
      HttpHeaders.authorizationHeader: "Bearer " + widget.token,
      "Accept": 'application/json',
    };

    return WillPopScope(
      onWillPop: () async {
        if (_key.currentState.canPop()) {
          _key.currentState.pop();
          Navigator.pop(context);
          Navigator.pop(context);
          return false;
        }
        return true;
      },
      child: SafeArea(
        // child: WebviewScaffold(
        //   key: _key,
        //   url: widget.url,
        //   withJavascript: true,
        //   ignoreSSLErrors: true,
        //   headers: headerParams,
        //   /*initialUrl: widget.url,
        //                   //initialUrl: _controller.,
        //
        //                   gestureRecognizers: gestureSet,
        //                   javascriptMode: JavascriptMode.unrestricted,
        //                   /*onPageFinished: (url) {
        //                     url = url.replaceAllMapped(RegExp(r'#.*'), (match) {
        //                       return '';
        //                     });
        //                     widget.onPageLoadingFinishCallback(url);
        //                   },*/
        //                   onWebViewCreated: (WebViewController controller) {
        //                     this.controller.complete(controller);
        //                   },*/
        // ),
        child: WebView(
          initialUrl: widget.url,
          gestureRecognizers: gestureSet,
          allowsInlineMediaPlayback: false,
          gestureNavigationEnabled: true,
          debuggingEnabled: false,
          javascriptMode: JavascriptMode.unrestricted,
          onPageFinished: (url){
            print('url');
            print('$url');
            print('url');
            var urlParts = url.split('/');
            String Lang;
            String Status="";
            if (url.contains('ar')) {
              Lang = 'ar';

            }
            else {
              Lang = 'en';
            }
            if (url.contains('paymentResult/')) {
              Status = urlParts[urlParts.length-1];
              List<String> urlList = url.split("/");
              // "Your payment Failed"
              print("Status"); print(Status); print("Status");
              print("Statuspay"); print(urlList.last); print("Status");
              String  message = urlList.last =="0" ?"paymentFailedMsg".tr().toString() :"paymentSuccessMsg".tr().toString() ;
              print('onUrlChanged: $url');
              Timer(Duration(seconds: 0), () =>
                  Navigator.pushAndRemoveUntil(context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              paymentStatues(widget.lang,int.parse("${urlList.last}"),message)),
                      ModalRoute.withName('/'))
              );
            }
          },

            onWebViewCreated: (WebViewController controller)async {
              this.controller.complete(controller);

            }
        ),
      ),
    );
  }

}