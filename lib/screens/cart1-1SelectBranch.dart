import '../../provider/productsInCartProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';

import '../dataControl/dataModule.dart';
import '../Controls.dart';


//imppp4
import '../dataControl/cartModule.dart';
import '../screens/home.dart';
import '../screens/cart3CheckOut.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';
import '../widgets/UI_Widgets_InputField.dart';

import '../globals.dart' as globals;
import '../screens/cart2Order.dart';

class CartSelectBranchLayout extends StatefulWidget{

  List<dynamic> _mealsCart = <dynamic>[];
  String Lang;
  String AppCurrency;
  String UserToken;
  int design_Control;
  List<dynamic> _addreses = <dynamic>[];
  String itemsNote;
  LatLng _center;
  double latitude, longitude;
  CartSelectBranchLayout(this.Lang,this.UserToken,this.AppCurrency,this._mealsCart,
      this.design_Control,this._addreses,this.latitude, this.longitude,
      this.itemsNote){
    _center = LatLng(latitude, longitude);
  }

  _CartSelectBranchLayout createState() =>
      _CartSelectBranchLayout(this.Lang, this.UserToken,this.AppCurrency,
          this._mealsCart,this.design_Control,this._addreses,
          this._center,this.itemsNote);
}

class _CartSelectBranchLayout extends State<CartSelectBranchLayout>{
  final Future<CartList> localCart = read_from_file();
  List<dynamic> _mealsCart = <dynamic>[];
  Future<ResponseApi> respBranches;
  List<dynamic> _branches = <dynamic>[];

  List<dynamic> _addreses = <dynamic>[];
  String Lang;
  String AppCurrency;
  String UserToken;
  int design_Control;
  String itemsNote;
  @override
  void initState() {
    super.initState();
    CreateItemsList(context);
  }
  CreateItemsList(BuildContext context){
    var cart = Provider.of<CartStateProvider>(context,listen: false);
    for(int i=0; i<_mealsCart.length; i++){
      Map item = {
        "id": _mealsCart[i]['meals_id'],
        "type": _mealsCart[i]['type'],
        "count": _mealsCart[i]['count'],
      };
      //Calcu total price and count items
      //total_items = total_items + _mealsCart[i]['count'];
      total_price = total_price +
          (double.parse(_mealsCart[i]['meals_price']) * double.parse("${_mealsCart[i]['count']}"));
    }
    total_price=  cart.couponType!=""? cart.couponType=="value" ? (total_price - double.parse(cart.couponValue)): (total_price-((total_price * double.parse(cart.couponValue))/100)):total_price;

    final_price = total_price;
  }
  _CartSelectBranchLayout(this.Lang, this.UserToken,this.AppCurrency,
      this._mealsCart,this.design_Control,this._addreses,
      this._center,this.itemsNote){

    respBranches = BranchesApi(Lang);
  }

  double camZomeValu=15;
  //int total_items = 0;
  double total_price = 0;
  double delivery_price = 0;
  double final_price = 0;

  LatLng _center;

  int _selectedIndex = 0;

  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);


    return Scaffold(
      appBar: AppBar(
        title: Text("selectDeliveryArea".tr().toString()),
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          LayoutBG(design_Control),

          Align(
            alignment: Alignment.center,
            child: Container(
              //width: mediaQuery.size.shortestSide,
              margin: EdgeInsets.only(top: 10,left: 10,right: 10,bottom: 120),
              child: FutureBuilder<ResponseApi>(
                  future: respBranches,
                  builder: (context, snap) {
                    if (snap.hasData) {
                      if (snap.data.status == 200) {
                        _branches = snap.data.data;
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[

                            globals.show_branches =="0"?Container(height: 0,width: 0,):    SizedBox(height: 10,),
                            globals.show_branches =="0"?Container(height: 0,width: 0,):    Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Text("selectBranchHeader".tr().toString(),
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.bold,
                                        fontSize: SystemControls.font3,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
                                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                      )
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 5,),
                         globals.show_branches =="0"?Container(height: 0,width: 0,):   Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    height: 40,
                                    child: InkWell(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          branchName==""?
                                              Container(
                                                width: 100,
                                                decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.all(Radius.circular(10)),
                                                    color: globals.accountColorsData['MainColor']!=null
                                                        ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz
                                                ),
                                                child: Center(
                                                  child: Text("selectBranch".tr().toString(),
                                                      style: TextStyle(
                                                        fontFamily: Lang,
                                                        fontWeight: FontWeight.normal,
                                                        fontSize: SystemControls.font3,
                                                        color: globals.accountColorsData['TextOnMainColor']!=null
                                                            ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                                                        height: 1.0,
                                                      )
                                                  ),
                                                ),
                                              ):
                                          Text(branchName==""?
                                         "select".tr().toString()
                                              :branchName,
                                              style: TextStyle(
                                                fontFamily: Lang,
                                                fontWeight: FontWeight.bold,
                                                fontSize: SystemControls.font3,
                                                color: globals.accountColorsData['TextHeaderColor']!=null
                                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                              )
                                          ),

                                        ],
                                      ),
                                      onTap: (){
                                        print("select ??????");
                                        showBottomSelect(context,_branches,true);
                                      },
                                    ),
                                  ),
                                ),
                                branchName!=""?
                                InkWell(
                                  onTap: (){
                                    showBottomSelect(context,_branches,true);
                                  },
                                  child: Icon(Icons.keyboard_arrow_down,color: Colors.black,
                                    size: 20,),
                                )
                                    :Container(),
                                SizedBox(width: 15),
                              ],
                            ),

                            SizedBox(height: 20,),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Text("selectAreaHeader".tr().toString(),
                                      style: TextStyle(
                                        fontFamily: Lang,
                                        fontWeight: FontWeight.bold,
                                        fontSize: SystemControls.font3,
                                        color: globals.accountColorsData['TextHeaderColor']!=null
                                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                      )
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 5,),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    height: 40,
                                    child: InkWell(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          areaName==""?
                                          Container(
                                            width: 100,
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                                color: globals.accountColorsData['MainColor']!=null
                                                    ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz
                                            ),
                                            child: Center(
                                              child: Text("selectArea".tr().toString(),
                                                  style: TextStyle(
                                                    fontFamily: Lang,
                                                    fontWeight: FontWeight.normal,
                                                    fontSize: SystemControls.font3,
                                                    color: globals.accountColorsData['TextOnMainColor']!=null
                                                        ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                                                    height: 1.0,
                                                  )
                                              ),
                                            ),
                                          ):
                                          Text(areaName,
                                              style: TextStyle(
                                                fontFamily: Lang,
                                                fontWeight: FontWeight.bold,
                                                fontSize: SystemControls.font3,
                                                color: globals.accountColorsData['TextHeaderColor']!=null
                                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                              )
                                          ),

                                        ],
                                      ),
                                      onTap: (){
                                        if(SelectedBranchindex == -1&& globals.show_branches !="0"){
                                          ErrorDialogAlert(context,
                                             "selectBranchWarning".tr().toString());
                                        }
                                        else if(globals.show_branches =="0") {

                                          showBottomSelect(
                                              context,
                                              _branches[0]['areas'], false);
                                        }else if(globals.show_branches =="1") {

                                          showBottomSelect(
                                              context,
                                              _branches[SelectedBranchindex]['areas'], false);
                                        }
                                      },
                                    ),
                                  ),
                                ),
                                areaName!=""?
                                InkWell(
                                  onTap: (){
                                    if(SelectedBranchindex == -1&& globals.show_branches !="0"){
                                      ErrorDialogAlert(context,
                                         "selectBranchWarning".tr().toString());
                                    }
                                    else if(globals.show_branches =="0") {

                                      showBottomSelect(
                                          context,
                                          _branches[0]['areas'], false);
                                    }else if(globals.show_branches =="1") {

                                      showBottomSelect(
                                          context,
                                          _branches[SelectedBranchindex]['areas'], false);
                                    }
                                  },
                                  child: Icon(Icons.keyboard_arrow_down,color: Colors.black,
                                    size: 20,),
                                )
                                    :Container(),
                                SizedBox(width: 15),
                              ],
                            ),
                          ],
                        );
                      }
                      else if (snap.data.status == 401) {
                        print("Cart select branch .... 401 cart");
                        SystemControls().LogoutSetUserData(
                            context, Lang);
                        ErrorDialogAlertBackHome(context, snap.data.message);
                      }
                      else {
                        _branches = [];
                      }
                      return Container();
                    }
                    if (snap.hasError) {
                      //TODO add error layout
                    }
                    return Center(
                      child: SystemControls().circularProgress(),
                    );
                  }
              ),
            ),
          ),

          Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              Container(
                color: globals.accountColorsData['BGColor']!=null
                    ? Color(int.parse(globals.accountColorsData['BGColor'])) : SystemControls.BGColorz,
                alignment: Alignment.bottomCenter,
                height: 120,
                width: mediaQuery.size.width,
              ),
              Container(
                width: mediaQuery.size.shortestSide,
                margin: EdgeInsets.only(right: 10,left: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      height: 1,
                      color: globals.accountColorsData['TextdetailsColor']!=null
                          ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                    ),
                    Padding(padding: EdgeInsets.all(3)),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Text("itemsPrice".tr().toString(),
                              style: TextStyle(
                                fontFamily: Lang,
                                fontWeight: FontWeight.normal,
                                fontSize: SystemControls.font3,
                                color: globals.accountColorsData['TextHeaderColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                height: 1.2,
                              )
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment
                                .start,
                            crossAxisAlignment: CrossAxisAlignment
                                .center,
                            children: <Widget>[
                              Text(
                                  total_price.toStringAsFixed(globals.numberDecimalDigits) +
                                      " " + AppCurrency,
                                  style: TextStyle(
                                    fontFamily: Lang,
                                    fontWeight: FontWeight.normal,
                                    fontSize: SystemControls
                                        .font3,
                                    color: globals.accountColorsData['TextHeaderColor']!=null
                                        ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                    height: 1.2,
                                  )
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    Padding(padding: EdgeInsets.all(3)),

                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Text("deliveryFees".tr().toString(),
                              style: TextStyle(
                                fontFamily: Lang,
                                fontWeight: FontWeight.normal,
                                fontSize: SystemControls.font3,
                                color: globals.accountColorsData['TextHeaderColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                height: 1.2,
                              )
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment
                                .start,
                            crossAxisAlignment: CrossAxisAlignment
                                .center,
                            children: <Widget>[
                              Text(
                                  deliveryFees.toStringAsFixed(globals.numberDecimalDigits) +
                                      " " + AppCurrency,
                                  style: TextStyle(
                                    fontFamily: Lang,
                                    fontWeight: FontWeight.normal,
                                    fontSize: SystemControls
                                        .font3,
                                    color: globals.accountColorsData['TextHeaderColor']!=null
                                        ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                    height: 1.2,
                                  )
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    Padding(padding: EdgeInsets.all(3)),

                    Container(
                      height: 1,
                      color: globals.accountColorsData['TextdetailsColor']!=null
                          ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz,
                    ),
                    Padding(padding: EdgeInsets.all(6)),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Text("TotalPrice".tr().toString(),
                              style: TextStyle(
                                fontFamily: Lang,
                                fontWeight: FontWeight.normal,
                                fontSize: SystemControls.font3,
                                color: globals.accountColorsData['TextHeaderColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                height: 1.2,
                              )
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Text("TotalPrice".tr().toString(),
                              style: TextStyle(
                                fontFamily: Lang,
                                fontWeight: FontWeight.normal,
                                fontSize: SystemControls.font3,
                                color: globals.accountColorsData['TextHeaderColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                height: 1.2,
                              )
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Text((final_price<=0?0:final_price).toStringAsFixed(globals.numberDecimalDigits)
                              +" "+AppCurrency,
                              style: TextStyle(
                                fontFamily: Lang,
                                fontWeight: FontWeight.normal,
                                fontSize: SystemControls.font3,
                                color: globals.accountColorsData['TextHeaderColor']!=null
                                    ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                                height: 1.2,
                              ),
                          ),
                        )
                      ],
                    ),

                    Padding(padding: EdgeInsets.all(6)),
                    Container(
                      width: mediaQuery.size.width,
                      height: 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: globals.accountColorsData['MainColor']!=null
                              ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz
                      ),
                      child: FlatButton(
                        onPressed: ()async{
                          if(UserToken == ""){
                            ErrorDialogAlert(context,
                                "mustLogin".tr().toString());
                          }else if(selectedAreaId == 0){
                            ErrorDialogAlert(context,
                            "selectAreaWarning".tr().toString());
                          }
                          else{
                            globals.deliveryCost = deliveryFees;
                            Navigator.push(context,
                                MaterialPageRoute(
                                    builder: (
                                        BuildContext context) =>
                                        CartOrderLayout(
                                            Lang,
                                            UserToken,
                                            AppCurrency,
                                            _mealsCart,
                                            design_Control,
                                            _addreses,
                                            widget.latitude,
                                            widget.longitude,
                                            itemsNote,
                                            selectedAreaId))
                                );
                          }
                        },
                        child: Text("confirmOrder".tr().toString(),
                            style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font3,
                              color: globals.accountColorsData['TextOnMainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                              height: 1.2,
                            )
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.all(10)),
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  int SelectedBranchindex = -1;
  String branchName = "";
  String areaName = "";
  int selectedAreaId = 0;
  dynamic deliveryFees = 0;

  Future<void> showBottomSelect(BuildContext context, List<dynamic> _choices,
      bool selectBrance){

    return showModalBottomSheet(
        context: context,
        isScrollControlled: false,
        builder: (context) {
          return ConstrainedBox(
            constraints: new BoxConstraints(
              minHeight: MediaQuery.of(context).size.height*0.2,
              maxHeight: MediaQuery.of(context).size.height*0.4,
            ),
            child: Container(
              color: Color(0xFF737373),
              child: Container(
                child: Stack(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.topCenter,
                      padding: EdgeInsets.all(15),
                      child: Text(selectBrance?
                      'selectBranch'.tr().toString():
                          'selectArea'.tr().toString(),
                        style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.bold,
                          fontSize: SystemControls.font3,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz,
                          height: 1.2,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(0),
                      child: IconButton(
                        icon: Icon(Icons.close,color: Colors.black,),
                        onPressed: (){
                          Navigator.pop(context);
                        },),
                    ),
                    DraggableScrollableSheet(
                      initialChildSize: 1,
                      minChildSize: 0.8,
                      builder: (context, ScrollController scrollController) {
                        return Container(
                          color: Colors.black12,
                          margin: EdgeInsets.only(top: 50),
                          child: ListView.builder(
                            controller: scrollController,
                            itemCount: _choices.length,
                            itemBuilder: (BuildContext context, int index) {
                              return ListTile(
                                title: Text(selectBrance?
                                    _choices[index]['branches_title']:
                                    _choices[index]['areas_title']),
                                onTap: (){
                                  setState(() {
                                    if(selectBrance) {
                                      SelectedBranchindex = index;
                                      branchName =
                                      _choices[index]['branches_title'];
                                      //reset
                                      areaName = "";
                                      selectedAreaId = 0;
                                      deliveryFees = 0;
                                      final_price =total_price ;
                                    }
                                    else{
                                      areaName = _choices[index]['areas_title'];
                                      selectedAreaId = _choices[index]['areas_id'];
                                      if(_choices[index]['areas_delivery_cost'] == null){
                                        deliveryFees = globals.deliveryCost;
                                      }
                                      else {
                                        deliveryFees =
                                            double.parse(
                                                _choices[index]['areas_delivery_cost']);
                                      }
                                      final_price =total_price + deliveryFees;
                                    }
                                    Navigator.pop(context);
                                  });
                                },
                              );
                            },
                          ),
                        );
                      },
                    ),
                  ],
                ),
                decoration: BoxDecoration(
                  color: Theme.of(context).canvasColor,
                  borderRadius: BorderRadius.only(
                    topLeft: const Radius.circular(12),
                    topRight: const Radius.circular(12),
                  ),
                ),
              ),
            ),
          );
        });
  }
}