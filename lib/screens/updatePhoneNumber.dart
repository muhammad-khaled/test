import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'package:country_code_picker/country_code_picker.dart';
import '../Controls.dart';

import 'package:easy_localization/easy_localization.dart';
import '../dataControl/userModule.dart';
import '../screens/home.dart';
import '../screens/updatePhoneVerify.dart';

import '../locale/share_preferences_con.dart';
import '../widgets/UI_Wigdets.dart';
import '../widgets/UI_Alert_Widgets.dart';
import '../widgets/UI_Widgets_InputField.dart';
import '../globals.dart' as globals;


import '../globals.dart' as globals;

class updatePhoneNumber extends StatefulWidget{
  String Lang;
  int design_Control;
  String UserToken;
  bool verified;

  updatePhoneNumber(this.Lang,this.UserToken,this.design_Control,this.verified);
  _updatePhoneNumber createState() =>
      _updatePhoneNumber(Lang, UserToken, design_Control);
}

class _updatePhoneNumber  extends State<updatePhoneNumber>{

  String Lang;
  String UserToken;
  bool verified;
  int design_Control;
  _updatePhoneNumber(this.Lang,this.UserToken,this.design_Control);
  //String email;
  //String name;
  void initState() {
    super.initState();
  }

  TextEditingController newPhoneTextField = TextEditingController();
  String countryCode = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("profile".tr().toString()),
        backgroundColor: globals.accountColorsData['AppbarBGColor']!=null
            ? Color(int.parse(globals.accountColorsData['AppbarBGColor'])) : SystemControls.AppbarBGColorz,
        flexibleSpace: AppBarElementsflexibleSpace(design_Control),
      ),
      body: Stack(
        children: <Widget>[
          LayoutBG(design_Control),
          Align(
            alignment: Alignment.center,
            child: Container(
              margin: EdgeInsets.all(20),
              width: MediaQuery.of(context).size.shortestSide,
              child: ListView(
                //mainAxisAlignment: MainAxisAlignment.start,
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 40),
                    child: Text("editPhoneNumber".tr().toString(),//"Full name",
                      style: TextStyle(
                          fontFamily: Lang,
                          fontWeight: FontWeight.normal,
                          fontSize: SystemControls.font2,
                          color: globals.accountColorsData['TextHeaderColor']!=null
                              ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                      ),
                    ),
                  ),

                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        alignment: Alignment.center,
                        width: 90,
                        margin: EdgeInsets.only(top: 12,right: 5),
                        child: globals.accounts_multiple_countries == "0" ?
                        Text(globals.countryCode,
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.bold,
                              fontSize: SystemControls.font2,
                              color: globals.accountColorsData['TextHeaderColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
                          ),
                          textAlign: TextAlign.center,
                        )
                            :CountryCodePicker(
                          onChanged: (code){
                            countryCode = code.dialCode;
                            print(countryCode);
                          },
                          // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                          initialSelection: 'EG',
                          // optional. Shows only country name and flag
                          showCountryOnly: false,
                          // optional. Shows only country name and flag when popup is closed.
                          showOnlyCountryWhenClosed: false,
                          // optional. aligns the flag and the Text left
                          alignLeft: false,
                          //Get the country information relevant to the initial selection
                          onInit: (code) {
                            countryCode = code.dialCode;
                            print("onInit "+countryCode);
                          },
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          margin: EdgeInsets.only(top: 15),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: TextFormField(
                            controller: newPhoneTextField,
                            decoration: InputFieldDecoration("newPhone".tr().toString(), Lang),
                            keyboardType: TextInputType.phone,
                            style: new TextStyle(
                              fontFamily: Lang,
                            ),
                          ),
                        ), //phone number
                      )
                    ],
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                      color: globals.accountColorsData['MainColor']!=null
                          ? Color(int.parse(globals.accountColorsData['MainColor'])) : SystemControls.MainColorz,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: FlatButton(
                      onPressed: () async{
                        bool wrongNumber = false;
                        if(globals.accounts_multiple_countries == "0"
                            && newPhoneTextField.text.length!=globals.phone_number_length)
                        {
                          wrongNumber = true;
                        }
                        else if(globals.accounts_multiple_countries == "1"
                            && newPhoneTextField.text.length < globals.phone_number_length)
                        {
                          wrongNumber = true;
                        }

                        if(newPhoneTextField.text.isNotEmpty && !wrongNumber
                            && newPhoneTextField.text.substring(0,1)!='0')
                        {
                          //TODO Use SMS verify method
                          if(globals.accounts_multiple_countries == "0") {
                            countryCode = globals.countryCode;
                          }
                          var get = await checkPhoneNumber(Lang,newPhoneTextField.text,countryCode);
                          if(get.status==200&&get.message!=null){
                            ErrorDialogAlert(context,get.message.toString());
                          }
                          else{
                            Navigator.push(context,
                                MaterialPageRoute(builder: (BuildContext context) =>
                                updatePhoneVerify(Lang, UserToken,
                                    design_Control,newPhoneTextField.text,countryCode,verified)
                                ));
                          }
                        }
                        else if(newPhoneTextField.text.isNotEmpty
                            && newPhoneTextField.text.substring(0,1)=='0')
                        {
                          ErrorDialogAlert(context,
                             "phoneNotStartWith0".tr().toString());
                        }
                        else if(newPhoneTextField.text.isNotEmpty && wrongNumber)
                        {
                          ErrorDialogAlert(context,
                              "enterTruePhone".tr().toString());
                        }
                        else{
                          ErrorDialogAlert(context,
                              "enterTruePhone".tr().toString());
                        }
                      },
                      child: Center(
                        child: Text(
                          "phoneNumberverifyButton".tr().toString(),
                          style: TextStyle(
                              fontFamily: Lang,
                              fontWeight: FontWeight.normal,
                              fontSize: SystemControls.font2,
                              color: globals.accountColorsData['TextOnMainColor']!=null
                                  ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,
                          ),
                        ),
                      ),
                    ),
                  ), //Login
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}