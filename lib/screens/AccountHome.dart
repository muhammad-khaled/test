import 'package:flutter/material.dart';
import '../Controls.dart';
import 'package:easy_localization/easy_localization.dart';
import '../locale/share_preferences_con.dart';

import '../dataControl/userModule.dart';
import '../screens/home.dart';
import '../screens/login.dart';
import '../screens/updateUser.dart';
import '../screens/updatePassword.dart';
import '../screens/favrs.dart';
import '../screens/ordersList.dart';
import '../screens/myAddresses.dart';
import '../screens/Branches.dart';
import '../widgets/UI_Alert_Widgets.dart';

import '../globals.dart' as globals;

Widget MyAccount(BuildContext context, dynamic info,String Lang, bool hasRegistration,
    bool isLogging, String Token, String Currency, List<dynamic> _CartMeals,
    int design_Control){


  TextStyle textStyleGeneral(){
    return TextStyle(
        fontFamily: Lang,
        fontWeight: FontWeight.bold,
        fontSize: SystemControls.font3,
        color: globals.accountColorsData['TextHeaderColor']!=null
            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz
    );
  }
  LoginButton(){
    if(isLogging == false){
      return Column(
        children: <Widget>[
          InkWell(
            onTap: (){
              Navigator.push(context,
                  MaterialPageRoute(builder: (BuildContext context) =>
                      login(Lang,design_Control)
                  ));
            },
            child: Container(
              margin: EdgeInsets.only(bottom: 3),
              color: Colors.white,
              height: 60,
              padding: EdgeInsets.only(right: 20,left: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("login".tr().toString(),
                    style: textStyleGeneral(),
                    textAlign: TextAlign.start,
                  ),
                  Icon(Icons.arrow_forward_ios,size: 16,color: globals.accountColorsData['NavigBarBorderColor']!=null
                      ? Color(int.parse(globals.accountColorsData['NavigBarBorderColor'])) : SystemControls.NavigBarBorderColorz,)
                ],
              ),
            ),
          ),
        ],
      );
    }
    else{
      return Container();
    }
  }
  SetAlreadyLoginArea(){
    if(isLogging == false){
      return Container();
    }
    else{
      return Column(
        children: <Widget>[
          InkWell(
            onTap: (){
              Navigator.push(context,
                  MaterialPageRoute(builder: (BuildContext context) =>
                      updateUser(Lang,Token,design_Control)
                  ));
            },
            child: Container(
              margin: EdgeInsets.only(bottom: 3),
              color: Colors.white,
              height: 60,
              padding: EdgeInsets.only(right: 20,left: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("profile".tr().toString(),
                    style: textStyleGeneral(),
                    textAlign: TextAlign.start,
                  ),
                  Icon(Icons.arrow_forward_ios,size: 16,color: globals.accountColorsData['NavigBarBorderColor']!=null
                      ? Color(int.parse(globals.accountColorsData['NavigBarBorderColor'])) : SystemControls.NavigBarBorderColorz,)
                ],
              ),
            ),
          ),
          InkWell(
            onTap: (){
              Navigator.push(context,
                  MaterialPageRoute(builder: (BuildContext context) =>
                      updatePassword(Lang,Token,design_Control)
                  ));
            },
            child: Container(
              margin: EdgeInsets.only(bottom: 3),
              color: Colors.white,
              height: 60,
              padding: EdgeInsets.only(right: 20,left: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("changePassword".tr().toString(),
                    style: textStyleGeneral(),
                    textAlign: TextAlign.start,
                  ),
                  Icon(Icons.arrow_forward_ios,size: 16,color: globals.accountColorsData['NavigBarBorderColor']!=null
                      ? Color(int.parse(globals.accountColorsData['NavigBarBorderColor'])) : SystemControls.NavigBarBorderColorz,)
                ],
              ),
            ),
          ),
          InkWell(
            onTap: ()async{
              var res = await logoutApi(Token,Lang);
              if(res.status == 200){
                print(res.message);
              }
              SharePreferenceData().storeDataToPreferences("api_token","");
              SharePreferenceData().storeDataToPreferences("name","");
              SharePreferenceData().storeDataToPreferences("email","");
              SharePreferenceData().storeDataToPreferences("customers_phone","");
              SharePreferenceData().setLoggingStatus(false);

              //Navigator.pop(context);
              Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          Home(Lang)),
                  ModalRoute.withName('/'));
            },
            child: Container(
              margin: EdgeInsets.only(bottom: 3),
              color: Colors.white,
              height: 60,
              padding: EdgeInsets.only(right: 20,left: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("logout".tr().toString(),
                    style: textStyleGeneral(),
                    textAlign: TextAlign.start,
                  ),
                  Icon(Icons.arrow_forward_ios,size: 16,color: globals.accountColorsData['NavigBarBorderColor']!=null
                      ? Color(int.parse(globals.accountColorsData['NavigBarBorderColor'])) : SystemControls.NavigBarBorderColorz,)
                ],
              ),
            ),
          ),
        ],
      );
    }
  }

  return Stack(
    children: <Widget>[

      Container(
        child: Column(
          children: <Widget>[
            LoginButton(),
            /*isLogging?Container():
            Container(
              height: 30,
            ),*/
            InkWell(
              onTap: (){
                if(isLogging == false){
                  ErrorDialogAlertGoTOLogin( context,
                      "mustLogin".tr().toString(),Lang);
                }
                else {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext context) =>
                          MyAddresses(Lang, Token, Currency, design_Control)
                      ));
                }
              },
              child: Container(
                margin: EdgeInsets.only(bottom: 3),
                color: Colors.white,
                height: 60,
                padding: EdgeInsets.only(right: 20,left: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("addresses".tr().toString(),
                      style: textStyleGeneral(),
                      textAlign: TextAlign.start,
                    ),
                    Icon(Icons.arrow_forward_ios,size: 16,color: globals.accountColorsData['NavigBarBorderColor']!=null
                        ? Color(int.parse(globals.accountColorsData['NavigBarBorderColor'])) : SystemControls.NavigBarBorderColorz,)
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: (){
                if(isLogging == false){
                  ErrorDialogAlertGoTOLogin(context,
                      "mustLogin".tr().toString(),Lang);
                }
                else{
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext context) =>
                          favsLay(_CartMeals, Lang, Token, Currency,
                              design_Control,hasRegistration)
                      ));
                }
              },
              child: Container(
                margin: EdgeInsets.only(bottom: 3),
                color: Colors.white,
                height: 60,
                padding: EdgeInsets.only(right: 20,left: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("favorite".tr().toString(),
                      style: textStyleGeneral(),
                      textAlign: TextAlign.start,
                    ),
                    Icon(Icons.arrow_forward_ios,size: 16,color: globals.accountColorsData['NavigBarBorderColor']!=null
                        ? Color(int.parse(globals.accountColorsData['NavigBarBorderColor'])) : SystemControls.NavigBarBorderColorz,)
                  ],
                ),
              ),
            ),
            SystemControls.branchesValid?(
                InkWell(
                  onTap: (){
                    Navigator.push(context,
                        MaterialPageRoute(builder: (BuildContext context) =>
                            BranchesLayout(Lang,Token,design_Control)
                        ));
                  },
                  child: Container(
                    margin: EdgeInsets.only(bottom: 3),
                    color: Colors.white,
                    height: 60,
                    padding: EdgeInsets.only(right: 20,left: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("branches".tr().toString(),
                          style: textStyleGeneral(),
                          textAlign: TextAlign.start,
                        ),
                        Icon(Icons.arrow_forward_ios,size: 16,color: globals.accountColorsData['NavigBarBorderColor']!=null
                            ? Color(int.parse(globals.accountColorsData['NavigBarBorderColor'])) : SystemControls.NavigBarBorderColorz,)
                      ],
                    ),
                  ),
                )
            ):(
                Container()
            ),
            InkWell(
              onTap: (){
                if(isLogging == false){
                  ErrorDialogAlertGoTOLogin(context,
                      "mustLogin".tr().toString(),Lang);
                }
                else{
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext context) =>
                          OrderList(Lang, Token, Currency,design_Control)
                      ));
                }
              },
              child: Container(
                margin: EdgeInsets.only(bottom: 3),
                color: Colors.white,
                height: 60,
                padding: EdgeInsets.only(right: 20,left: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("orders".tr().toString(),
                      style: textStyleGeneral(),
                      textAlign: TextAlign.start,
                    ),
                    Icon(Icons.arrow_forward_ios,size: 16,color: globals.accountColorsData['NavigBarBorderColor']!=null
                        ? Color(int.parse(globals.accountColorsData['NavigBarBorderColor'])) : SystemControls.NavigBarBorderColorz,)
                  ],
                ),
              ),
            ),

            Container(
              height: 25,
            ),
            SetAlreadyLoginArea(),
          ],
        ),
      ),
    ],
  );
}