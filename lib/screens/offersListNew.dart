import 'package:flutter/material.dart';
import '../Controls.dart';

import '../screens/categoryListNew.dart';

import '../globals.dart' as globals;

Widget offerListPage(
    String Lang,
    String UserToken,
    int selectedCatID,
    List<dynamic> _CartMeals,
    List<dynamic> _categories,
    String AppCurrency,
    int design_Control,
    bool hasRegistration,
    dynamic appInfo){

  return Stack(
    children: <Widget>[
      CategoryMealsListNew(_CartMeals,_categories,Lang,-2,UserToken,
          AppCurrency, -2,design_Control,hasRegistration,appInfo),
    ],
  );
}