






import '../../Models/DrawerModel/FavoriteModel.dart';
import 'package:flutter/cupertino.dart';

import 'GetFavoriteService.dart';

class UserFavoriteRepo{
  UserFavoriteRepo({this.context});
  BuildContext context;
  Future<FavoriteModel> get getFavoriteItems =>getUserFavoriteProduct(context);
}