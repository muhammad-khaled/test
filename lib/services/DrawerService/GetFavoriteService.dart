












import '../../Models/DrawerModel/FavoriteModel.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../Controls.dart';

Future<FavoriteModel> getUserFavoriteProduct(BuildContext context)async{
  // var homeProvider =Provider.of<HomeProvider>(context,listen: false);
  // homeProvider.showLoader = true;

  Dio dio =Dio();
  Response response;
  SharedPreferences _prefs = await SharedPreferences.getInstance();

  print('Diooooooooo');
  print("${SystemControls.baseURL+EasyLocalization.of(context).currentLocale.languageCode+SystemControls.baseURL2}customerFavs?accountId=${SystemControls.AccountId}");
  response =await dio.get("${SystemControls.baseURL+EasyLocalization.of(context).currentLocale.languageCode+SystemControls.baseURL2}customerFavs?accountId=${SystemControls.AccountId}",options: Options(
      headers: {
        "Accept": 'application/json',
        "Authorization": '${_prefs.get('isLoggedIn') == true ? "Bearer "+_prefs.get('api_token'):"" }',

      }
  ));


  if(response.statusCode ==200){
    if(response.data["status"]==200){
      FavoriteModel favoriteModel  =FavoriteModel.fromJson(response.data);

      return favoriteModel;
    }
  }
}