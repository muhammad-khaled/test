


import '../../Models/HomeModel/BestSaleModel.dart';
import '../../provider/HomeProvider/HomeProvider.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Controls.dart';

Future<BestSaleModel> getBestSaleApi(BuildContext context,int pageNum)async{
  var homeProvider =Provider.of<HomeProvider>(context,listen: false);
  String url = SystemControls.baseURL+EasyLocalization.of(context).currentLocale.languageCode+SystemControls.baseURL2+
      "bestSales?accountId="+SystemControls.AccountId+"&page="+"${homeProvider.currentPage}";

  homeProvider.showLoader = true;


  Dio dio = Dio();
  Response response;
  SharedPreferences _prefs = await SharedPreferences.getInstance();

  response = await dio .get(url,options: Options(
      headers: {
        "Accept": 'application/json',
        "Authorization": '${_prefs.get('isLoggedIn') == true ? "Bearer "+_prefs.get('api_token'):"" }',

      }
  ));

  if(response.statusCode ==200){
    BestSaleModel bestSaleModel = BestSaleModel.fromJson(response.data);
    homeProvider.nextPage = bestSaleModel.data.pagination.currentPage+1;

    bestSaleModel.data.bestSales.forEach((element) {

        homeProvider.bestSaleList.add(element);


    });
    homeProvider.bestSaleList.toSet().toList();






    return bestSaleModel;
  }else{
    print('DiscountError');
    print(response.statusMessage);
  }


}
