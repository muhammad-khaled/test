





import '../../Models/HomeModel/BestSaleModel.dart';
import '../../Models/HomeModel/ProductByIdModel.dart';

import '../Models/HomeModel/CategoryDetailModel.dart';
import '../Models/HomeModel/CategorySearchModel.dart';
import '../Models/HomeModel/HomeProductsModel.dart';
import '../Models/HomeModel/OfferModel.dart';
import '../Models/HomeModel/applicationHomeModel.dart';
import '../Models/HomeModel/getProductByIDModel.dart';
import 'BestSaleService.dart';
import 'HomeService/CategorySearchService.dart';
import 'HomeService/GetOfferApi.dart';
import 'HomeService/GetProductByIdService.dart';
import 'HomeService/getCategoryById.dart';
import 'HomeService/getHomeServiceApi.dart';
import 'package:flutter/cupertino.dart';

import 'HomeService/SerachApi.dart';
import 'HomeService/applicationHomeApi.dart';

class HomeProductsResponseRepo{
  HomeProductsResponseRepo({this.context,this.pageNum});
  final BuildContext context;
  final int pageNum;
  Future<HomeProductsModel> get  getHomeItems => getHomeProductsApi(context,pageNum);
}
class ApplicationHomeResponseRepo{
  ApplicationHomeResponseRepo({this.context});
  final BuildContext context;
  Future<ApplicationHomeModel> get  getHomeApplicationItems => getHomeApplicationApi(context);
}



class ProductsByIdRepo{
  ProductsByIdRepo({this.context,this.id,this.pageNum});
  final BuildContext context;
  final int id;
  final int pageNum;
  Future<CategoriesDetailModel> get  getProductsById => getCategoryById(context,id,pageNum);
}class ProductsSearchByIdRepo{
  ProductsSearchByIdRepo({this.context,this.id,this.pageNum});
  final BuildContext context;
  final int id;
  final int pageNum;
  Future<CategoriesDetailModel> get  getProductsById => getSearchCategoryById(context,id,pageNum);
}



class ProductsSearchByTitleRepo{
  ProductsSearchByTitleRepo({this.context,this.pageNum,this.title});
  final BuildContext context;
  final String title;
  final int pageNum;
  Future<CategoriesDetailModel> get  getProductsByTitle => getSearchCategoryByTitle(context,pageNum,title);
}




class GetProductByIdRepo{
  GetProductByIdRepo({this.context,this.id});
  final BuildContext context;
  final int id;
  Future<ProductByIdModel> get  getProductById => getProductByID(context,id);
}




class GetCategorySearchTreeRepo{
  GetCategorySearchTreeRepo({this.context});
  final BuildContext context;
  Future<CategorySearchModel> get  getCategorySearch => getCategoriesSearchApi(context);
}


class GetOfferApiRepo{
  GetOfferApiRepo({this.context});
  final BuildContext context;
  Future<OfferModelModel> get  getOfferApiData => getOfferApi(context);
}




class BestSaleModelRepo{

  BestSaleModelRepo({this.context,this.pageNum});
  int pageNum;
  final BuildContext context;
  Future<BestSaleModel>   get getBestSaleProduct => getBestSaleApi(context,pageNum);

}

