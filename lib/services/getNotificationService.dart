







import 'package:easy_localization/easy_localization.dart';

import '../Models/NotificationModel.dart';
import '../provider/NotificaionProvider.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../Controls.dart';

Future<List<NotificationModel>> getNotifcationService(String lang, String bearerToken,BuildContext context)async{
  var notificationProvider = Provider.of<NotificationProvider>(context,listen: false);
  String url = SystemControls.baseURL+lang+SystemControls.baseURL2+
      "getCustomerNotification?accountId="+SystemControls.AccountId;

  Dio dio = Dio();
  Response response;

  response = await dio .get(url,options: Options(
    headers: {
    "Authorization":  "Bearer  $bearerToken",
      "Accept": 'application/json',
    }
  ));


  if(response.statusCode ==200){

     if(response.data["status"]==200){
       NotificationModel notificationModel = NotificationModel.fromJson(response.data);
       notificationProvider.notificationList.clear();
       print(notificationModel.data.pagination.nextPageUrl);
       notificationProvider.urlLoadMore = notificationModel.data.pagination.nextPageUrl;
       notificationModel.data.notifications.forEach((element) {
         notificationProvider.notificationList.add(notificationModel);

       });
       return  notificationProvider.notificationList;
     }else if(response.data["status"]==401){
       SystemControls().LogoutSetUserData(context, EasyLocalization.of(context).currentLocale.languageCode);
     }

  }else{
    print(response.data);
  }
}




Future<NotificationModel> getNotifcationCounterService(String lang, String bearerToken,BuildContext context)async{
  final SystemControls storage = SystemControls();

  String url = SystemControls.baseURL+lang+SystemControls.baseURL2+
      "getCustomerNotification?accountId="+SystemControls.AccountId;

  Dio dio = Dio();
  Response response;

  response = await dio .get(url,options: Options(
    headers: {
    "Authorization":  "Bearer  ${await storage.getDataFromPreferences("api_token")}",
      "Accept": 'application/json',
    }
  ));


  if(response.statusCode ==200){

    NotificationModel notificationModel = NotificationModel.fromJson(response.data);

    return  notificationModel;

  }else{
    print(response.data);
  }
}

Future<NotificationModel> getNotifcationServiceCounter(String lang, String bearerToken,BuildContext context)async{
  var notificationProvider = Provider.of<NotificationProvider>(context,listen: false);
  String url = SystemControls.baseURL+lang+SystemControls.baseURL2+
      "getCustomerNotification?accountId="+SystemControls.AccountId;

  Dio dio = Dio();
  Response response;

  response = await dio .get(url,options: Options(
      headers: {
        "Authorization":  "Bearer  $bearerToken",
        "Accept": 'application/json',
      }
  ));


  if(response.statusCode ==200){

    NotificationModel notificationModel = NotificationModel.fromJson(response.data);


    return  notificationModel;

  }else{
    print(response.data);
  }
}

Future<NotificationModel> LoadMoreNotification(String url,String bearerToken,BuildContext context)async{
  Dio dio = Dio();
  Response response;
  var notificationProvider = Provider.of<NotificationProvider>(context,listen: false);

  response = await dio .get(url,options: Options(
      headers: {
        "Authorization":  "Bearer  $bearerToken",
        "Accept": 'application/json',
        // "accountId": '6',
      }
  ));


  if(response.statusCode ==200){

    NotificationModel notificationModel = NotificationModel.fromJson(response.data);

    notificationProvider.urlLoadMore = notificationModel.data.pagination.nextPageUrl;






    notificationModel.data.notifications.forEach((element) {
      notificationProvider.notificationList.add(notificationModel);

    });

    notificationProvider.getNotificationList.sink.add(notificationProvider.notificationList);
    return notificationModel;

  }else{
    print('errrrrrrrrrorrrrr');
    print(response.data);
  }
}