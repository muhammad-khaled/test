






import '../../Controls.dart';
import '../../provider/productsInCartProvider.dart';
import '../../widgets/UI_Alert_Widgets.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../globals.dart' as globals;

Future<String> validateCoupon(BuildContext context,String couponCode, String appCurrency,String totalPrice)async{
  var cartProvider = Provider.of<CartStateProvider>(context,listen: false);
  Dio dio =Dio();
  Response response;
  SharedPreferences _prefs = await SharedPreferences.getInstance();

  response =await dio.post("${SystemControls.baseURL+EasyLocalization.of(context).currentLocale.languageCode+SystemControls.baseURL2}checkValidCoupon",options: Options(
      headers: {
        "Accept": 'application/json',
        "Content-Type": 'application/json',
        "Authorization": '${_prefs.get('isLoggedIn') == true ? "Bearer "+_prefs.get('api_token'):"" }',

      }
  ),data: {
  "accountId": SystemControls.AccountId,
  "coupons_code" : "$couponCode"
  });


  if(response.statusCode ==200){
    if(response.data["status"]==200){
      print(response.data);
      print(response.data["data"]["coupons_type"]);
      print(response.data["data"]["coupons_value"]);
       if(response.data["data"]["coupons_type"] =="value" && double.parse(totalPrice)> double.parse(response.data["data"]["coupons_value"])) {
         cartProvider.changeCouponValue(
             "${response.data["data"]["coupons_value"]}");
         cartProvider.changeCouponType(
             "${response.data["data"]["coupons_type"]}");
         SuccDialogAlertCoupon(context: context,
             mes: response.data["data"]["coupons_value"],
             type: response.data["data"]["coupons_type"],
             appCurrency: appCurrency);
       }else if(response.data["data"]["coupons_type"] =="rate"){
         cartProvider.changeCouponValue(
             "${response.data["data"]["coupons_value"]}");
         cartProvider.changeCouponType(
             "${response.data["data"]["coupons_type"]}");
         SuccDialogAlertCoupon(context: context,
             mes: response.data["data"]["coupons_value"],
             type: response.data["data"]["coupons_type"],
             appCurrency: appCurrency);
       }else{
         cartProvider.couponController.clear();
         cartProvider.changeCouponValue(
             "");
         cartProvider.changeCouponType(
             "");
         couponNotValid(context,"priceTooLow".tr().toString());

       }

    }else if(response.data["status"]==401){
      SystemControls().LogoutSetUserData(context, EasyLocalization.of(context).currentLocale.languageCode);
    }else{
      print("error");
      print(response.data);
      cartProvider.couponController.clear();
      couponNotValid(context,response.data["message"]);
      // SuccDialogAlertStaySameLayout(context,response.data["message"]);

    }
  }
}