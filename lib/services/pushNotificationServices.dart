import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

// class PushNotificationService {
//   final FirebaseMessaging _fcm = FirebaseMessaging();
//
//   Future initialise(BuildContext context) async {
//     if (Platform.isIOS) {
//       _fcm.requestNotificationPermissions(IosNotificationSettings());
//     }
//
//     _fcm.configure(
//       onMessage: (Map<String, dynamic> message) async {
//         print("#####onMessage: $message");
//         final snackbar = SnackBar(
//           content: Text(message['notification']['title']),
//           action: SnackBarAction(
//             label: 'Go',
//             onPressed: null,
//           ),
//         );
//         Scaffold.of(context).showSnackBar(snackbar);
//       },
//       onLaunch: (Map<String, dynamic> message) async {
//         print("onLaunch: $message");
//       },
//       onResume: (Map<String, dynamic> message) async {
//         print("onResume: $message");
//       },
//     );
//   }
// }