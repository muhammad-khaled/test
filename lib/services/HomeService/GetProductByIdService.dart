






import '../../Models/HomeModel/ProductByIdModel.dart';


import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../Controls.dart';

Future<ProductByIdModel> getProductByID(BuildContext context, int id)async{

  Dio dio =Dio();
  Response response;

  SharedPreferences _prefs = await SharedPreferences.getInstance();

  response =await dio.get("${SystemControls.baseURL+EasyLocalization.of(context).currentLocale.languageCode+SystemControls.baseURL2}product/$id?accountId=${SystemControls.AccountId}",options: Options(
      headers: {
        "Accept": 'application/json',
        "Authorization": '${_prefs.get('isLoggedIn') == true ? "Bearer "+_prefs.get('api_token'):"" }',

      }
  ));


  if(response.statusCode ==200){
    if(response.data["status"]==200){
      ProductByIdModel getProductByIdModel  =ProductByIdModel.fromJson(response.data);

      return getProductByIdModel;
    }
  }
}