








import '../../Models/HomeModel/OfferModel.dart';
import '../../provider/HomeProvider/HomeProvider.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../Controls.dart';

Future<OfferModelModel> getOfferApi(BuildContext context)async{
  var homeProvider =Provider.of<HomeProvider>(context,listen: false);
  homeProvider.showLoader = true;

  Dio dio =Dio();
  Response response;
  SharedPreferences _prefs = await SharedPreferences.getInstance();

  response =await dio.get("${SystemControls.baseURL+EasyLocalization.of(context).currentLocale.languageCode+SystemControls.baseURL2}offers?accountId=${SystemControls.AccountId}",options: Options(
      headers: {
        "Accept": 'application/json',
        "Authorization": '${_prefs.get('isLoggedIn') == true ? "Bearer "+_prefs.get('api_token'):"" }',

      }
  ));


  if(response.statusCode ==200){
    if(response.data["status"]==200){
      OfferModelModel offerModelModel  =OfferModelModel.fromJson(response.data);
      print("offerModelModel.data.offers.length");
      print(offerModelModel.data.offers.length);
      print("offerModelModel.data.offers.length");
      return offerModelModel;
    }
  }
}