





import '../../Models/HomeModel/CategorySearchModel.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../Controls.dart';

Future<CategorySearchModel> getCategoriesSearchApi(BuildContext context)async{
  String url = SystemControls.baseURL+EasyLocalization.of(context).currentLocale.languageCode+
      SystemControls.baseURL2+"categoriesTreeList?accountId="+
      SystemControls.AccountId;

  Dio dio = Dio();
  Response response;
  SharedPreferences _prefs = await SharedPreferences.getInstance();

  response = await dio.get(url,options: Options(
      headers: {
        "Accept": 'application/json',
        "Authorization": '${_prefs.get('isLoggedIn') == true ? "Bearer "+_prefs.get('api_token'):"" }',

      }
  ));

  if(response.statusCode ==200){
    if(response.data["status"]==200){
      CategorySearchModel categorySearchModel  =CategorySearchModel.fromJson(response.data);


      return categorySearchModel;
    }else{
      print("error");
    }
  }



}