







import '../../Models/HomeModel/applicationHomeModel.dart';
import '../../provider/HomeProvider/HomeProvider.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../Controls.dart';

Future<ApplicationHomeModel> getHomeApplicationApi(BuildContext context)async{
  var homeProvider =Provider.of<HomeProvider>(context,listen: false);

  Dio dio =Dio();
  Response response;
  SharedPreferences _prefs = await SharedPreferences.getInstance();

  print("${SystemControls.baseURL+EasyLocalization.of(context).currentLocale.languageCode+SystemControls.baseURL2}home?accountId=${SystemControls.AccountId}");
  response =await dio.get("${SystemControls.baseURL+EasyLocalization.of(context).currentLocale.languageCode+SystemControls.baseURL2}home?accountId=${SystemControls.AccountId}",options: Options(
      headers: {
        "Accept": 'application/json',
        "Authorization": '${_prefs.get('isLoggedIn') == true ? "Bearer "+_prefs.get('api_token'):"" }',
      }
  ));

  if(response.statusCode ==200){
    if(response.data["status"]==200){
      ApplicationHomeModel applicationHomeModel  =ApplicationHomeModel.fromJson(response.data);

      return applicationHomeModel;
    }
  }
}


