








import '../../Models/HomeModel/CategoryDetailModel.dart';
import '../../provider/HomeProvider/SearchProvider.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../Controls.dart';

Future<CategoriesDetailModel> getSearchCategoryById(BuildContext context,int id,int pageNum)async{
  var searchProvider =Provider.of<SearchProvider>(context,listen: false);
  Dio dio =Dio();
  Response response;
  searchProvider.showLoader = true;
  SharedPreferences _prefs = await SharedPreferences.getInstance();

  response =await dio.get("${SystemControls.baseURL+EasyLocalization.of(context).currentLocale.languageCode+SystemControls.baseURL2}products?accountId=${SystemControls.AccountId}&cateId=$id&page=${searchProvider.currentPage}",options: Options(
      headers: {
        "Accept": 'application/json',
        "Authorization": '${_prefs.get('isLoggedIn') == true ? "Bearer "+_prefs.get('api_token'):"" }',

      }
  ));
  if(response.statusCode ==200){
    if(response.data["status"]==200){
      CategoriesDetailModel categoriesDetailModel  =CategoriesDetailModel.fromJson(response.data);
      searchProvider.nextPage = categoriesDetailModel.data.pagination.currentPage+1;
      categoriesDetailModel.data.products.forEach((element) {
        searchProvider.productSearchByIdList.add(element);
      });



      return categoriesDetailModel;
    }else{
      print("error");
    }
  }
}



Future<CategoriesDetailModel> getSearchCategoryByTitle(BuildContext context,int pageNum,String title)async{

  var searchProvider =Provider.of<SearchProvider>(context,listen: false);
  Dio dio =Dio();
  Response response;
  searchProvider.showLoader = true;
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  print('url:${"${SystemControls.baseURL+EasyLocalization.of(context).currentLocale.languageCode+SystemControls.baseURL2}products?accountId=${SystemControls.AccountId}&title=${searchProvider.searchController.text}&page=${searchProvider.currentPage}"}');
  response =await dio.get("${SystemControls.baseURL+EasyLocalization.of(context).currentLocale.languageCode+SystemControls.baseURL2}products?accountId=${SystemControls.AccountId}&title=${searchProvider.searchController.text}&page=${searchProvider.currentPage}",options: Options(
      headers: {
        "Accept": 'application/json',
        "Authorization": '${_prefs.get('isLoggedIn') == true ? "Bearer "+_prefs.get('api_token'):"" }',

      }
  ));
  if(response.statusCode ==200){
    if(response.data["status"]==200){
      CategoriesDetailModel categoriesDetailModel  =CategoriesDetailModel.fromJson(response.data);
      searchProvider.nextPage = categoriesDetailModel.data.pagination.currentPage+1;
      categoriesDetailModel.data.products.forEach((element) {
        searchProvider.productSearchByTitleList.add(element);
      });
      return categoriesDetailModel;
    }else{
      print("error");
    }
  }
}