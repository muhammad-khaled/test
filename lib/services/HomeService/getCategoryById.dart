






import '../../Models/HomeModel/CategoryDetailModel.dart';
import '../../provider/HomeProvider/HomeProvider.dart';
import '../../provider/HomeProvider/SearchProvider.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../Controls.dart';

Future<CategoriesDetailModel> getCategoryById(BuildContext context,int id,int pageNum)async{
  var homeProvider =Provider.of<HomeProvider>(context,listen: false);
  var searchProvider =Provider.of<SearchProvider>(context,listen: false);
  homeProvider.showLoader = true;
  Dio dio =Dio();
  Response response;

  SharedPreferences _prefs = await SharedPreferences.getInstance();

  response =await dio.get("${SystemControls.baseURL+EasyLocalization.of(context).currentLocale.languageCode+SystemControls.baseURL2}products?accountId=${SystemControls.AccountId}&cateId=$id&page=${homeProvider.currentPage}",options: Options(
      headers: {
        "Accept": 'application/json',
        "Authorization": '${_prefs.get('isLoggedIn') == true ? "Bearer "+_prefs.get('api_token'):"" }',

      }
  ));
  print("pageNum${homeProvider.currentPage}");
  if(response.statusCode ==200){
    if(response.data["status"]==200){
      CategoriesDetailModel categoriesDetailModel  =CategoriesDetailModel.fromJson(response.data);
      homeProvider.nextPage = categoriesDetailModel.data.pagination.currentPage+1;
       categoriesDetailModel.data.products.forEach((element) {
         homeProvider.productByIdList.add(element);
         searchProvider.productSearchByIdList.add(element);
       });

      return categoriesDetailModel;
    }else{
      print("error");
    }
  }
}




