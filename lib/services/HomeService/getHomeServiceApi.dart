import '../../Controls.dart';
import '../../Models/HomeModel/HomeProductsModel.dart';
import '../../provider/HomeProvider/HomeProvider.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<HomeProductsModel> getHomeProductsApi(
    BuildContext context, int page) async {
  var homeProvider = Provider.of<HomeProvider>(context, listen: false);
  homeProvider.showLoader = true;

  Dio dio = Dio();
  Response response;
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  print('homeProviderhomeProvider');
  print(homeProvider.currentPage);
  response = await dio.get(
      "${SystemControls.baseURL + EasyLocalization.of(context).currentLocale.languageCode + SystemControls.baseURL2}products?accountId=${SystemControls.AccountId}&home=1&page=${homeProvider.currentPage}",
      options: Options(headers: {
        "Accept": 'application/json',
        "Authorization":
            '${_prefs.get('isLoggedIn') == true ? "Bearer " + _prefs.get('api_token') : ""}',
      }));

  if (response.statusCode == 200) {
    if (response.data["status"] == 200) {
      print('DDDDDDDDDDD');
      HomeProductsModel homeResponseModel =
          HomeProductsModel.fromJson(response.data);
      homeProvider.nextPage = homeResponseModel.data.pagination.currentPage + 1;

      homeResponseModel.data.homeProducts.forEach((element) {
        homeProvider.productList.add(element);
        homeProvider.specialProducts.add(element);
      });

       homeProvider.specialProducts.toSet().toList();
      print("homeProvider.specialProducts.length");
      print(homeProvider.specialProducts.length);
      print("homeProvider.specialProducts.length");
      return homeResponseModel;
    }
  }
}
