import 'dart:math';

import '../../provider/HomeProvider/HomeProvider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Models/DiscountModel.dart';
import '../dataControl/dataModule.dart';
import '../provider/DiscountProvider.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../Controls.dart';

Future<DiscountDetailsModel> getDiscountApi(
    BuildContext context, String pageNum) async {
  var homeProvider = Provider.of<HomeProvider>(context, listen: false);
  String url = SystemControls.baseURL +
      EasyLocalization.of(context).currentLocale.languageCode +
      SystemControls.baseURL2 +
      "discounts?accountId=" +
      SystemControls.AccountId +
      "&page=" +
      "${homeProvider.currentPage}";

  homeProvider.showLoader = true;

  Dio dio = Dio();
  Response response;
  SharedPreferences _prefs = await SharedPreferences.getInstance();

  response = await dio.get(url,
      options: Options(headers: {
        "Accept": 'application/json',
        "Authorization":
            '${_prefs.get('isLoggedIn') == true ? "Bearer " + _prefs.get('api_token') : ""}',
      }));

  if (response.statusCode == 200) {
    DiscountDetailsModel discountDetails =
        DiscountDetailsModel.fromJson(response.data);
    homeProvider.nextPage = discountDetails.data.pagination.currentPage + 1;

    discountDetails.data.products.forEach((element) {
      if (element.productsPriceAfterSale != "") {
        homeProvider.productDiscountList.add(element);
      }
    });
    homeProvider.productDiscountList.toSet().toList();

    return discountDetails;
  } else {
    print('DiscountError');
    print(response.statusMessage);
  }
}

Future<ResponseApi> getDiscountApiresponce(
    String lang, BuildContext context) async {
  // String url = SystemControls.baseURL+lang+SystemControls.baseURL2+
  //     "discounts?accountId="+SystemControls.AccountId;
  // var discount = Provider.of<DiscountProvider>(context,listen: false);
  //
  // Dio dio = Dio();
  // Response response;
  //
  // response = await dio .get(url,options: Options(
  //     headers: {
  //       "Accept": 'application/json',
  //     }
  // ));
  //
  // if(response.statusCode ==200){
  //   ResponseApi discountDetails = ResponseApi.fromJson(response.data);
  //   return discountDetails;
  // }else{
  //   print('DiscountError');
  //   print(response.statusMessage);
  // }
}

Future<ResponseApi> getDiscountApiresponceNext(
    BuildContext context, String url, String pageNum) async {
  // String Url = url +
  //     "&accountId="+SystemControls.AccountId;
  // String Url2 = SystemControls.baseURL+EasyLocalization.of(context).currentLocale.languageCode+
  //     SystemControls.baseURL2+"discounts?accountId="+
  //     SystemControls.AccountId+"&page="+pageNum;
  // var discount = Provider.of<DiscountProvider>(context,listen: false);
  //
  // Dio dio = Dio();
  // Response response;
  //
  // response = await dio .get(Url2,options: Options(
  //     headers: {
  //       "Accept": 'application/json',
  //     }
  // ));
  //
  // if(response.statusCode ==200){
  //   ResponseApi discountDetails = ResponseApi.fromJson(response.data);
  //   // discount.getDiscountList.sink.add(discountDetails);
  //   print('DADADADADAD');
  //   print(response.data);
  //   return discountDetails;
  // }else{
  //   print('DiscountError');
  //   print(response.statusMessage);
  // }
}
