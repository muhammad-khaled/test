import 'package:deodela/screens/HomePageScreenTheamTwo/HomePageScreenTheamTwo.dart';
import 'package:deodela/screens/HomeTheamOne/HomePageScreenTheamOne.dart';
import 'package:flutter/material.dart';
import 'locale/share_preferences_con.dart';
import 'dart:ui';
import 'dart:ui' as ui;

import 'package:flutter_svg/flutter_svg.dart';
import 'globals.dart' as globals;

class SystemControls with SharePreferenceData{
  //TODO get it from APIs
  static const double  CategoryImgSize= 100;//100 true value ---- 20 to get screen shots
  static const bool branchesValid = false;
                                    //false;
  static const int designControl = 1;


  
  static const String baseURL =
      "http://www.polaris-sys.com/system/";
                                // "http://www.nmait.net/polaris/";
                                 //"http://www.magnifypro.com/o2restaurant/";
  static const String baseURL2 = "/api/v004/";
  static const String appName = "Deodela";

  static const String AccountId = "7";
  static const String theme = "2";

  //Add photo to cart (on create order) flag to enable or disable
  static const bool cartAddPhoto = false;
  //Time to wait until user receive activation code
  //if time out user can request resend code again
  static const int msgWaitingTine = 40;

  static const double RadiusCircValue = 10.0;

  //Minimum number of items at the row at the default case
  static const int MinNumberOfItemsOnRow = 2;
  //we have two type of card structure design select 1 or 2
  static const int cardViewItemNumber = 2;

  //(MainColor and MainColorOpacity)Have to be the same color
  // with different opacity
  static const Color MainColorz = Color.fromRGBO(185, 150, 60, 1);
  static const Color MainColorOpacityz = Color.fromRGBO(185, 150, 60, 0.5);

  static const Color AppbarBGColorz = Color.fromRGBO(255, 255, 255, 1);
  static const Color SearchBGColorz = Color.fromRGBO(255, 255, 255, 1);
  static const Color bottomNavigationBarBGColorz = Color.fromRGBO(255, 255, 255, 1);

  static const Color BGColorz = Color.fromRGBO(250, 250, 250, 1);
  static const Color TextHeaderColorz = Color.fromRGBO(15, 15, 15, 1);
  static const Color TextdetailsColorz = Color.fromRGBO(112, 112, 112, 1);
  static const Color BorderColorz = Color.fromRGBO(237, 237, 237, 1);
  static const Color TextOnMainColorz = Colors.white;

  static const Color NavigBarBorderColorz = Color.fromRGBO(142, 142, 147, 1);

  static double width = GetWidth();
  static double hight = GetHeight();
  static double DeviceRatio = hight/width;

  static double font1 = 0.08 * width ;
  static double font2 = 15;//0.028 * width ;//2
  static double font3 = 14;//0.026 * width ;//3
  static double font4 = 12;
  static double font4S = 0.02 * width * DeviceRatio;
  static double font5 = 10;


  static List<String> addresPartsFormat(String Lang){
    if(Lang=='en'){
      return ["Address *"];
    }
    else{
      return ["العنوان *"];
    }
  }
  static List<String> addresGeoCodeKey(){
    //GeoCode All options
    //"addressLine" // "adminArea" // "countryCode" // "countryName"
    //"featureName" // "locality" // "postalCode" // "subAdminArea"
    //"subLocality" // "subThoroughfare" // "thoroughfare"
    return ["addressLine"];
  }

  //static List<String> paymetWay = ["Cash","Knet","Fawry","Visa","Master card"];
  static List<String> paymentWayFun(String Lang){
    if(Lang=='en'){
      return ["Cash","Knet"];
    }
    else{
      return ["دفع عند الاستلام","Knet"];
    }
  }
  //static String version="null";
  static String Version(String lang){
    if(lang=='en'){
      return "Version: 1.0.12";
    }
    else{
      return "الاصدار:1.0.12";
    }
  }

  static double GetWidth(){
    double h = window.physicalSize.height;
    double w = window.physicalSize.width;
    if(w>h)
      return h;
    else
      return w;
    //print("Screen size: height-> "+x.toString()+" width-> "+ y.toString());
  }
  static double GetHeight(){
    double h = window.physicalSize.height;
    double w = window.physicalSize.width;
    if(w>h)
      return w;
    else
      return h;
  }

  GetIFDeviceTablet(){
    final double devicePixelRatio = ui.window.devicePixelRatio;
    final ui.Size size = ui.window.physicalSize;
    final double width = size.width;
    final double height = size.height;


    if(devicePixelRatio < 2 && (width >= 1000 || height >= 1000)) {
      //isTablet = true;
      //isPhone = false;
      print("isTablet");
      return true;
    }
    else if(devicePixelRatio == 2 && (width >= 1920 || height >= 1920)) {
      //isTablet = true;
      //isPhone = false;
      print("isTablet");
      return true;
    }
    else {
      //isTablet = false;
      //isPhone = true;
      print("isTablet no no");
      return false;
    }
  }
  String GetImgPath(String _imgName, String _imgType){
    if(_imgName == null){
      return "";
    }
    else{
      //TODO add account id when fix images path
      String ret = baseURL+"uploads/"+AccountId+"/images/"+_imgType+"/"+_imgName;
      // print(ret);
      return ret;

    }
  }
  mealButtonDecoration(){
    return BoxDecoration(
      borderRadius: BorderRadius.all(
        Radius.circular(SystemControls.RadiusCircValue/2),),
      //border: Border.all(color:SystemControls.TextdetailsColor,width: 1),
    );
  }
  circularProgress(){
    //return Image.asset("assets/loading.gif",height: 40,);
    return CircularProgressIndicator(
      //backgroundColor: Colors.grey,
      valueColor: AlwaysStoppedAnimation<Color>(
          globals.accountColorsData['MainColor']!=null?
              Color(int.parse(globals.accountColorsData['MainColor'])) : MainColorz)
    );
  }
  GetSVGImagesAsset(String imgPage,double height, Color color){
    return SvgPicture.asset(imgPage,
      height: height,
      color: color,
    );
  }

  ///Remome all user saved data
  LogoutSetUserData(BuildContext context,String Lang){
    SharePreferenceData().storeDataToPreferences("api_token","");
    SharePreferenceData().storeDataToPreferences("name","");
    SharePreferenceData().storeDataToPreferences("email","");
    SharePreferenceData().storeDataToPreferences("customers_phone","");
    SharePreferenceData().storeDataToPreferences("customers_country_code","");
    SharePreferenceData().setLoggingStatus(false);
    switch(SystemControls.theme){

      case "1":
        Navigator.of(context).popUntil((route) => route.isFirst);
        Navigator.pushReplacement(context,
            MaterialPageRoute(
              builder: (BuildContext context) => HomePageScreenTheamOne(),)

        );
        break; case "2":
      Navigator.of(context).popUntil((route) => route.isFirst);


      Navigator.pushReplacement(context,
          MaterialPageRoute(
            builder: (BuildContext context) => HomePageScreenTheamTwo(),)

      );
      break;
    }

  }
}
