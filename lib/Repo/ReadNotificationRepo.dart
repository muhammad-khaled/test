





import '../services/ReadNotificationService.dart';

class ReadNotificationRepo{
  ReadNotificationRepo({this.lang,this.token});
  final String lang;
  final String token;
  Future<String>   get readNotificationRepo => readNotification(lang,token);
}