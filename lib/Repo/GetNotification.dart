






import '../Models/NotificationModel.dart';
import '../services/getNotificationService.dart';
import 'package:flutter/cupertino.dart';
class NotificationRepoCounter{

  NotificationRepoCounter({this.lang,this.token,this.context});
  final String lang;
  final String token;
  final BuildContext context;
  Future<NotificationModel>   get getNotification => getNotifcationServiceCounter(lang,token,context);

}
class NotificationRepo{

  NotificationRepo({this.lang,this.token,this.context});
final String lang;
 final String token;
 final BuildContext context;
Future<List<NotificationModel>>   get getNotification => getNotifcationService(lang,token,context);
Future<NotificationModel>   get getNotificationCounter => getNotifcationCounterService(lang,token,context);

}
class NotificationLoadRepo{

  NotificationLoadRepo({this.url,this.token,this.context});
final String url;
 final String token;
  final BuildContext context;

  Future<NotificationModel>   get loadMoreNotification => LoadMoreNotification(url,token,context);

}