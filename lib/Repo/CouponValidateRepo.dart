



 import '../../services/ValidateCoupon.dart';
import 'package:flutter/cupertino.dart';

class ValidateCouponREpo{
  ValidateCouponREpo({this.context,this.couponCode,this.appCurrency,this.totalPrice});
  final BuildContext context;
  final String couponCode;
  final String appCurrency;
  final String totalPrice;

  Future<String> get validateCouponFun => validateCoupon(context,couponCode,appCurrency,totalPrice);
  }