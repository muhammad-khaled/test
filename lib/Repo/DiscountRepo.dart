



import '../../Models/DiscountModel.dart';

import '../dataControl/dataModule.dart';
import '../services/GetDiscountApi.dart';
import 'package:flutter/material.dart';

class DiscountRepo{

  DiscountRepo({this.lang,this.context});
  final String lang;
  final BuildContext context;
  Future<ResponseApi>   get getDiscount => getDiscountApiresponce(lang,context);

}
class DiscountWithModelRepo{

  DiscountWithModelRepo({this.context,this.pageNum});
   String pageNum;
  final BuildContext context;
  Future<DiscountDetailsModel>   get getDiscountProduct => getDiscountApi(context,pageNum);

}