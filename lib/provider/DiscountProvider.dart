


import '../Repo/DiscountRepo.dart';
import '../dataControl/dataModule.dart';
import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';

class DiscountProvider extends ChangeNotifier{
  List<ResponseApi> discontList =[];

  DiscountRepo getNotificationRepo = DiscountRepo();
  final getDiscountList = PublishSubject<ResponseApi>();
  Stream get streamDiscount =>
      getDiscountList.stream;
  var discount;

  fetchNotification( String lang,String token ,BuildContext context) async {
     discount = await DiscountRepo(lang: lang,context: context).getDiscount;
    getDiscountList.sink.add(discount);
    notifyListeners();
  }

  String urlLoadMore;


  @override
  void dispose() {
    // TODO: implement dispose
    getDiscountList.close();
    super.dispose();
  }

}