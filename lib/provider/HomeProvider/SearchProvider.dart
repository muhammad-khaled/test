


import '../../Models/HomeModel/CategoryDetailModel.dart';
import '../../Models/HomeModel/sharedModel.dart';
import '../../services/HomeRepo.dart';
import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';

class SearchProvider extends ChangeNotifier{
  bool showSearchCategories = true;
  int nextPage = 1;
  int currentPage = 1;
  int search =0;

  int lastPage;
  bool showLoader = false;
  controlSearchCategories(bool value){
    showSearchCategories = value;
    nextPage = 1;
    currentPage = 1;
    notifyListeners();
  }

  int categoryId ;

  searchWithCatID(BuildContext context,int catId){
    showSearchCategories = false;
    categoryId = catId;
     nextPage = 1;
     currentPage = 1;
     search =0;
    lastPage;
    productSearchByIdList.clear();
    fetchProductById(context,catId,currentPage);
    notifyListeners();

  }




  restData(){
     showSearchCategories = true;
     nextPage = 1;
     currentPage = 1;
     search =0;

     lastPage;
     showLoader = false;
     productSearchByIdList.clear();
    notifyListeners();
  }


  List<ProductShared> productSearchByIdList = [];
  final getProductById = BehaviorSubject<CategoriesDetailModel>();
  Stream get streamProductById => getProductById.stream;
  fetchProductById(BuildContext context, int id, int pageNum) async {
    CategoriesDetailModel _notificationList =
    await ProductsSearchByIdRepo(id: id, context: context, pageNum: pageNum)
        .getProductsById;
    getProductById.sink.add(_notificationList);
    // notifyListeners();

  }


  TextEditingController searchController = TextEditingController();

  searchWithTitle(BuildContext context,String title){
    print("title");
    print(title);
    print("title");
    showSearchCategories = false;
    nextPage = 1;
    currentPage = 1;
    search =1;
    lastPage;

    fetchProductByTitle(context,title,currentPage);
    notifyListeners();

  }

  List<ProductShared> productSearchByTitleList = [];
  final getProductByTitle = BehaviorSubject<CategoriesDetailModel>();
  Stream get streamProductByTitle=> getProductByTitle.stream;
  fetchProductByTitle(BuildContext context, String title, int pageNum) async {
    print('ttTitle');
    print(title);
    CategoriesDetailModel _notificationList =
    await ProductsSearchByTitleRepo(title: title, context: context, pageNum: pageNum)
        .getProductsByTitle;
    getProductByTitle.sink.add(_notificationList);
    notifyListeners();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    getProductById.close();
    getProductByTitle.close();
    super.dispose();
  }
}