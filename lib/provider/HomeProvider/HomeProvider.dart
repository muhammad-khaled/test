import '../../Models/HomeModel/BestSaleModel.dart';
import '../../Models/HomeModel/applicationHomeModel.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../Models/DiscountModel.dart';
import '../../Models/HomeModel/CategoryDetailModel.dart';
import '../../Models/HomeModel/HomeProductsModel.dart';
import '../../Models/HomeModel/sharedModel.dart';
import '../../Repo/DiscountRepo.dart';
import '../../dataControl/cartModule.dart';
import '../../services/HomeRepo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../globals.dart' as globals;

import '../../Controls.dart';

class HomeProvider extends ChangeNotifier {

   bool colored =false;
   changeColor(){
     colored =! colored;
     notifyListeners();
   }
  /// Product details Swiper Pagination
 int currentIndex =0;

  changeSwiperIndex(int value){
    currentIndex = value;
    notifyListeners();
  }
  ///
  ApplicationHomeModel value;

  Future<ApplicationHomeModel> getHomeApplicationApi(BuildContext context)async{
     if(value ==null){
    Dio dio =Dio();
    Response response;
    print('Diooooooooo');
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    print('**xx**');
    print(_prefs.get('isLoggedIn'));
    print(_prefs.get('api_token'));

    print("${SystemControls.baseURL+EasyLocalization.of(context).currentLocale.languageCode+SystemControls.baseURL2}home?accountId=${SystemControls.AccountId}");
    response =await dio.get("${SystemControls.baseURL+EasyLocalization.of(context).currentLocale.languageCode+SystemControls.baseURL2}home?accountId=${SystemControls.AccountId}",options: Options(
        headers: {
          "Accept": 'application/json',
          "Authorization": '${_prefs.get('isLoggedIn') == true ? "Bearer "+_prefs.get('api_token'):"" }',
        }
    ));

    if(response.statusCode ==200){
      if(response.data["status"]==200){
        ApplicationHomeModel applicationHomeModel  =ApplicationHomeModel.fromJson(response.data);
           value =applicationHomeModel;
      }

      }
    }
     return value;
  }









  int selectedCategory = -2;
  int isCategory = -2;
  int currentPage = 1;
  int nextPage = 1;
  int lastPage=999;
  bool showLoader = false;
  int categoryId = 0;



  changeIndex(int index, int type) {
    productList.clear();
    productDiscountList.clear();
    productByIdList.clear();
    currentPage = 1;
    selectedCategory = index;
    isCategory = type;
    notifyListeners();
  }

  getCategoryId(int id) {
    categoryId = id;
    print(categoryId);
    notifyListeners();
  }

  List<ProductShared> productList = [];
 ///// discount List
  List<ProductShared> productDiscountList = [];
  final getDiscountList = BehaviorSubject<DiscountDetailsModel>();
  Stream get streamDiscount => getDiscountList.stream;
  fetchDiscountProducts(BuildContext context, String pageNum) async {
    DiscountDetailsModel _notificationList =
        await DiscountWithModelRepo(pageNum: pageNum, context: context)
            .getDiscountProduct;
    getDiscountList.sink.add(_notificationList);
    // notifyListeners();
  }

  /////  product by Id list
  List<ProductShared> productByIdList = [];
  final getProductById = BehaviorSubject<CategoriesDetailModel>();
  Stream get streamProductById => getProductById.stream;
  fetchProductById(BuildContext context, int id, int pageNum) async {
    CategoriesDetailModel _notificationList =
        await ProductsByIdRepo(id: id, context: context, pageNum: pageNum)
            .getProductsById;
    getProductById.sink.add(_notificationList);
    // notifyListeners();
  }
  List<ProductShared> specialProducts = [];

  final getHomeItems = BehaviorSubject<HomeProductsModel>();
  Stream get streamHomeItems => getHomeItems.stream;
  fetchHomeItems(BuildContext context, int pageNum) async {
    print(pageNum);
    HomeProductsModel _notificationList =
        await HomeProductsResponseRepo(pageNum: pageNum, context: context)
            .getHomeItems;
    getHomeItems.sink.add(_notificationList);
    // notifyListeners();
  }



  ///// best sale  list

  List<ProductShared> bestSaleList = [];
  final getBestSaleList = BehaviorSubject<BestSaleModel>();
  Stream get streamBestSale => getBestSaleList.stream;
  fetchBestSaleProducts(BuildContext context, int pageNum) async {
    BestSaleModel _notificationList =
    await BestSaleModelRepo(pageNum: pageNum, context: context)
        .getBestSaleProduct;
    getBestSaleList.sink.add(_notificationList);
    // notifyListeners();
  }



  ///// Side Menu Data ///


  String userName;
  GetCustomerData() {
    SystemControls().getDataFromPreferences("name").then((String name) {

        userName = name;

        print("name");
        print(name);
        print("name");
      notifyListeners();

    });
  }
////////////   ///////////////  deo dela /////////  ///////////
  fetchHomeData(BuildContext context){
    productList.clear();
    productDiscountList.clear();
    fetchHomeItems(context, currentPage);
    fetchDiscountProducts(context, "$currentPage");
  }
  resetData(){
   productList.clear();
     selectedCategory = -2;
     isCategory = -2;
     currentPage = 1;
     nextPage = 1;
     lastPage=999;
     showLoader = false;
     categoryId = 0;
     notifyListeners();

  }
  int currentHomeWidgetIndex =0;
  changeCurrentHomeWidgetIndex(int index,BuildContext context){
    currentHomeWidgetIndex =index;
    print("currentHomeWidgetIndex");
    print(currentHomeWidgetIndex);
    print("currentHomeWidgetIndex");
    productList.clear();
    productDiscountList.clear();
    switch(index){
      case 0:
        resetData();
        productList.clear();
        productDiscountList.clear();
        fetchHomeItems(context, currentPage);
        fetchDiscountProducts(context, "$currentPage");
        break;
        case 2:
      resetData();
      productDiscountList.clear();

      fetchDiscountProducts(context, "$currentPage");


        break;
        case 3:
          resetData();
          productList.clear();
          Future<CartList> localCart = read_from_file();
    }
    notifyListeners();
  }
////////////   ///////////////  deo dela /////////  ///////////
@override
  void notifyListeners() {
    // TODO: implement notifyListeners
    super.notifyListeners();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    getDiscountList.close();
    getHomeItems.close();
    getProductById.close();
    getBestSaleList.close();
    super.dispose();
  }
}
