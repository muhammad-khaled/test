



import 'dart:async';

import '../locale/share_preferences_con.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Models/NotificationModel.dart';
import '../Repo/GetNotification.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class NotificationProvider extends ChangeNotifier{
  List<NotificationModel> notificationList =[];

  NotificationRepo getNotificationRepo = NotificationRepo();
  final getNotificationList = PublishSubject<List<NotificationModel>>();
  StreamSubscription<double> streamSubscription;
  Stream get streamNotification =>
      getNotificationList.stream;
  fetchNotification( String lang,String token ,BuildContext context) async {

     notificationList = await NotificationRepo(lang: lang,token: token,context: context).getNotification;
     print("Streeeeeeeeeeeeeeeeeeeeeeeeeeeeeem");
    getNotificationList.sink.add(notificationList);
    // notifyListeners();
  }

  String urlLoadMore;
  int counter ;

  getNum()async{
    int count = await SharePreferenceData().getNotificationCount();
    int total = count +1;

    SharedPreferences sharedPreferencesGetEndTime =
        await SharedPreferences.getInstance();
    sharedPreferencesGetEndTime.setInt(SharePreferenceData().notificationCount, total);


    counter = await SharePreferenceData().getNotificationCount();
    print(counter);
    notifyListeners();
  }

  getcount()async{
    counter = await SharePreferenceData().getNotificationCount();
    notifyListeners();
  }

  resetCounteer()async{
    counter =0;
    SharedPreferences sharedPreferencesGetEndTime =
        await SharedPreferences.getInstance();
    sharedPreferencesGetEndTime.setInt(SharePreferenceData().notificationCount, 0);
    notifyListeners();
  }


  NotificationRepo getNotificationCounterRepo = NotificationRepo();
  final getNotificationCounter = PublishSubject<NotificationModel>();
  Stream get streamNotificationCounter =>
      getNotificationCounter.stream;
  fetchNotificationCounter( String lang,String token ,BuildContext context) async {

    NotificationModel  notificationList = await NotificationRepo(lang: lang,token: token,context: context).getNotificationCounter;
    getNotificationCounter.sink.add(notificationList);
    // notifyListeners();
  }


 @override
  void notifyListeners() {
   // getcount();
    // TODO: implement notifyListeners
    super.notifyListeners();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    getNotificationList.close();
    getNotificationCounter.close();
    print('Clooooooooooooooooooosssseedd');
    super.dispose();
  }

}