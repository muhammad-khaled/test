import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

import '../../Models/HomeModel/sharedModel.dart';
import '../../dataControl/cartModule.dart';
import '../../widgets/SharedWidget/Radio/buttonValue.dart';
import '../../widgets/UI_Alert_Widgets.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import '../../globals.dart' as globals;
import 'package:sizer/sizer.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:easy_localization/easy_localization.dart';

import '../Controls.dart';
  class CartStateProvider with ChangeNotifier {
   TextEditingController couponController = TextEditingController();
   String couponType="";
   String couponValue="";

   changeCouponValue(String value){
      couponValue = value;


      print('Value');
      print(value);

    notifyListeners();
   }   changeCouponType(String value){
      couponType = value;
     notifyListeners();
   }
    int selectedSize = -1;
    int choiseId ;
    String choiceText;

    List<dynamic> cartList = <dynamic>[];
    changeSize(int index,int id,String choiceTextData) {
      selectedSize = index;
      choiseId = id;
      choiceText = choiceTextData;
      print("ididid");
      print(id);
      print(index);
    notifyListeners();
    }
  resetSize(){
    selectedSize = -1 ;
       notifyListeners();
  }

    setCartList(List<dynamic> list)async{
      cartList = list;
      print("list.lengthlist.length");
      print(list.length);
      print(cartList.length);
      CartList _notificationList =
      await read_from_file();


      print("_notificationList");
      print(list);
      print(list);
      print(list);
      getCart.sink.add(_notificationList);
      print("list.lengthlist.length");
      // notifyListeners();
      // fetchCardNum();
  }

    String productsCount1 = 0.toString();
    void setCurrentProductsCount(String count) {
      print(count + " asdfghjkl;'asdfghjk" );
      this.productsCount1 = count;
      // notifyListeners();
    }
    String get productsCount => this.productsCount1;
  
    PickedFile _imageFile;
    void setImgFilePicker(PickedFile imageFile) {
      print("set image picker asdfghjkl;'asdfghjk" );
      this._imageFile = imageFile;
      notifyListeners();
    }
    PickedFile get imageFile => this._imageFile;
  
    String cartImgName = "";
    void setCartImgName(String count) {
      this.cartImgName = count;
      notifyListeners();
    }
    String get myCartImgName => this.cartImgName;



    addToCartMethodProductDetails(
        ProductShared product, BuildContext context, List<dynamic> cartList) {
      print("add to cart");

      //TODO Remove it to add all items already at the cart
      if (true) {
        //sear == -1
        dynamic AddMeal = CartMeal();
        print('choiseId');
        print(choiseId);
        print('choiseId');
      AddMeal.quantityType =
            product.productsType; //Product follow weight or num
        AddMeal.minQuantity = product.productsLowestWeightAvailable;

        AddMeal.meals_id = product.productsId;
        AddMeal.meals_title = product.productsTitle;
        AddMeal.choice_id = choiseId;
        AddMeal.choice_text = choiceText;
        AddMeal.meals_img = product.productsImg;
        AddMeal.meals_desc = product.productsDesc;
        AddMeal.quantityType =
            product.productsType; //Product follow weight or num
        if (product.productsPriceAfterSale != "") {
          AddMeal.meals_price = product.productsPriceAfterSale.toString();
        } else {
          AddMeal.meals_price = product.productsPrice.toString();
        }
        AddMeal.follow_delivery_time =
            product.category.categoriesFollowDeliveryTime;
        AddMeal.type = "product";
        // if(products[index]['quantity'] == null){
        //   products[index]['quantity'] = 1;
        // }
        AddMeal.count = product.productsLowestWeightAvailable != null ? product.productsLowestWeightAvailable *(radioButtonValue+1):radioButtonValue+1;

        dynamic obj = AddMeal.toJson();
        cartList.add(obj);

        //onAddTocartSubmit(_CartMeals);


        int x = int.parse(productsCount);
        x++;
      setCurrentProductsCount(x.toString());

        write_to_file(obj);

        SuccDialogAlertStaySameLayout(context, 'orderAddtoCart'.tr().toString(),ValueKey("productCart1")).whenComplete(() => Navigator.pop(context));
        fetchCardNum();
        choiseId =null;
        selectedSize =-1;
        notifyListeners();
      }
    }

    addToCartMethodProduct(
        ProductShared product, BuildContext context, List<dynamic> cartList) {
      print("add to cart");

      //TODO Remove it to add all items already at the cart
      if (true) {
        //sear == -1
        dynamic AddMeal = CartMeal();
        print('choiseId');
        print(choiseId);
        print('choiseId');
        AddMeal.quantityType =
            product.productsType; //Product follow weight or num
        AddMeal.minQuantity = product.productsLowestWeightAvailable;

        AddMeal.meals_id = product.productsId;
        AddMeal.meals_title = product.productsTitle;
        AddMeal.choice_id = choiseId;
        AddMeal.choice_text = choiceText;
        AddMeal.meals_img = product.productsImg;
        AddMeal.meals_desc = product.productsDesc;
        AddMeal.quantityType =
            product.productsType; //Product follow weight or num

        print('product.productsPriceAfterSale');
        print(product.productsPriceAfterSale);
      if (product.productsPriceAfterSale != "" || product.productsPriceAfterSale.isNotEmpty) {
          AddMeal.meals_price = product.productsPriceAfterSale.toString();
        } else {
          AddMeal.meals_price = product.productsPrice.toString();
        }
        AddMeal.follow_delivery_time =
            product.category.categoriesFollowDeliveryTime;
        AddMeal.type = "product";
        // if(products[index]['quantity'] == null){
        //   products[index]['quantity'] = 1;
        // }
        AddMeal.count = quantity;

        dynamic obj = AddMeal.toJson();
        cartList.add(obj);

        //onAddTocartSubmit(_CartMeals);


        int x = int.parse(productsCount);
        x++;
        setCurrentProductsCount(x.toString());

        write_to_file(obj);

        SuccDialogAlertStaySameLayout(context, 'orderAddtoCart'.tr().toString(),ValueKey("productCart2")).whenComplete(() => Navigator.pop(context));
        fetchCardNum();
        choiseId =null;
        selectedSize =-1;
        notifyListeners();
      }
    }



   AddOfferToCart(var item,BuildContext context) {
     print("Get Action ...666.");
     print("add to cart");
     String Img;
     int sear;
     String followDeliveryTime;


       dynamic AddMeal = CartMeal();
       AddMeal.meals_id = item.offersId;
       AddMeal.meals_title = item.offersTitle;
       AddMeal.meals_img = Img;
       AddMeal.meals_desc = item.offersDesc;
       //Product follow weight or num

         AddMeal.meals_price = item.offersPrice;

       AddMeal.follow_delivery_time = "0";

         AddMeal.type = "offer";


       AddMeal.count = quantity;

       dynamic obj = AddMeal.toJson();
       write_to_file(obj);

     cartList.add(obj);

     //onAddTocartSubmit(_CartMeals);


     int x = int.parse(productsCount);
     x++;
     setCurrentProductsCount(x.toString());

     write_to_file(obj);

     SuccDialogAlertStaySameLayout(context, 'orderAddtoCart'.tr().toString(),ValueKey("productCart3")).whenComplete(() => Navigator.pop(context));
     fetchCardNum();
     choiseId =null;
     selectedSize =-1;
     notifyListeners();

   }
    int radioButtonValue =0;
    showAddToCartProductDetails(BuildContext context , ProductShared product,List<dynamic> cartList) {

      showModalBottomSheet(
          isScrollControlled: true,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15), topRight: Radius.circular(15))),
          context: context,
          builder: (context) => Container(
            padding: EdgeInsets.only(left: 12,top: 5,right: 12,bottom: 5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15))),
            height: 60.h,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,

              children: [
                SizedBox(
                  height: 5,
                ),
                Container(
                  height: 10,
                  width: 50.w,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.grey.shade300
                  ),
                ),
                SizedBox(
                  height: 2.h,
                ),
                Text("${product.productsTitle}",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
                Divider(),
                RadioButtonWidgetDetails(product:product),
                Spacer(),
                Container(
                  width: MediaQuery.of(context).size.width -60,
                  child: MaterialButton(
                    height: 48,
                    onPressed: (){

                      addToCartMethodProductDetails(product,context,cartList);
                      read_from_file();

                    },
                    color: globals.accountColorsData['MainColor'] != null
                        ? Color(int.parse(globals.accountColorsData['MainColor']))
                        : SystemControls.MainColorz,
                    child: Text("addToCart".tr().toString(),style: TextStyle(color: Colors.white),),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
                Consumer<CartStateProvider>(
                  builder: (context, cart, _) {
                    return FutureBuilder<CartList>(
                      future: localCart,
                      builder: (context, Cartsnap) {
                        if (Cartsnap.connectionState ==
                            ConnectionState.done &&
                            Cartsnap.hasData) {
                          cart.setCartList(Cartsnap.data.Cmeals);

                          CartStateProvider cartState =
                          Provider.of<CartStateProvider>(context,
                              listen: false);
                          if (cartState.productsCount == "0" &&
                              cart.cartList.length != 0) {
                            cartState.setCurrentProductsCount(
                                cart.cartList.length.toString());
                          }
                          return Container();
                        }
                        return Container();
                      },
                    );
                  },
                ),
                SizedBox(
                  height: 2.h,
                ),
              ],
            ),
          ));
    }

      double quantity =1.0;

   int currentQuantity =0;
   changeCurrentQuantity(int index){
     currentQuantity = index;
     // notifyListeners();
   }
   updateQuantity(double value){
     quantity =value;
     // notifyListeners();
   }

   increaseWeightQuantity(double value){
     quantity = quantity +value;
     notifyListeners();
   }

   minusWeightQuantity(double value){
     if(quantity> value){
       quantity=  quantity - value;
       notifyListeners();
     }
    }

    increaseQuantity(){
      quantity = quantity +1;
      notifyListeners();
    }

    minusQuantity(){
      if(quantity> 1){
        quantity --;
        notifyListeners();
      }
    }
   Future<CartList> localCart = read_from_file();

    showAddToCartProduct(BuildContext context , ProductShared product,List<dynamic> cartList,String appCurrency, CartStateProvider cartStateProvider) {

      showModalBottomSheet(
          isScrollControlled: true,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15), topRight: Radius.circular(15))),
          context: context,
          builder: (context){

            product.productsLowestWeightAvailable != null  ?product.productsLowestWeightAvailable !="" ?cartStateProvider.updateQuantity(double.parse("${product.productsLowestWeightAvailable.toString().replaceFirst("0", "")}")) :  print(''): print('') ;
            cartStateProvider.changeCurrentQuantity(0);
            return Consumer<CartStateProvider>(builder: (context,cart,_){
              return Container(
                padding: EdgeInsets.only(left: 12,top: 5,right: 12,bottom: 5),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15),
                        topRight: Radius.circular(15))),
                height: 40.h,

                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,

                  children: [
                    SizedBox(
                      height: 5,
                    ),
                    Container(
                      height: 10,
                      width: 50.w,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.grey.shade300
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Text("${product.productsTitle}",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
                    Container(
                        width: MediaQuery.of(context).size.width -60,
                        child: Divider()),
                    Container(
                        width: MediaQuery.of(context).size.width -45,
                        child: RadioButtonWidget(product:product)),
                    product.productsLowestWeightAvailable != null? product.productsLowestWeightAvailable !="" ? Consumer<CartStateProvider>(builder: (context,cart,_){
                      return Container(
                          width: MediaQuery.of(context).size.width ,
                          height: 6.h,
                          child: ListView.builder(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              itemCount: 20,
                              itemBuilder: (context ,index){
                                return Center(
                                  child: InkWell(
                                    onTap: (){
                                      cart.updateQuantity(double.parse("${product.productsLowestWeightAvailable.toString().replaceFirst("0", "")}")*(index+1));
                                      cart.changeCurrentQuantity(index);
                                      print('xxxx');
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: cart.currentQuantity != index? globals.accountColorsData[
                                            'BorderColor'] !=
                                                null
                                                ? Color(int.parse(globals
                                                .accountColorsData['BorderColor']))
                                                : SystemControls.BorderColorz:globals.accountColorsData['MainColor'] != null
                                                ? Color(int.parse(globals.accountColorsData['MainColor']))
                                                : SystemControls.MainColorz ,
                                            borderRadius: BorderRadius.circular(8)
                                        ),
                                        width: 18.w,
                                        height: 5.h,
                                        child: Center(child: Text("${double.parse("${product.productsLowestWeightAvailable.toString().replaceFirst("0", "")}")*(index+1)} ${'KG'.tr().toString()}",style: TextStyle(color:
                                        cart.currentQuantity != index?globals.accountColorsData['TextHeaderColor']!=null
                                            ? Color(int.parse(globals.accountColorsData['TextHeaderColor'])) : SystemControls.TextHeaderColorz : globals.accountColorsData['TextOnMainColor']!=null
                                            ? Color(int.parse(globals.accountColorsData['TextOnMainColor'])) : SystemControls.TextOnMainColorz,

                                        ),),),
                                      ),
                                    ),
                                  ),
                                );
                              })
                      );
                    }):Container():Container(),
                    Spacer(),
                    Container(
                      width: MediaQuery.of(context).size.width -50,
                      child:  Row(
                        children: [
                          Container(
                              height: 5.7.h,
                              decoration: BoxDecoration(
                                  color: globals.accountColorsData[
                                  'BorderColor'] !=
                                      null
                                      ? Color(int.parse(globals
                                      .accountColorsData['BorderColor']))
                                      : SystemControls.BorderColorz,
                                  borderRadius: EasyLocalization.of(context).currentLocale.languageCode =="ar"?BorderRadius.only(topRight: Radius.circular(8),bottomRight: Radius.circular(8)):BorderRadius.only(topLeft: Radius.circular(8),bottomLeft: Radius.circular(8))
                              ),
                              child: IconButton(onPressed: (){

                                product.productsLowestWeightAvailable != null ?    product.productsLowestWeightAvailable ==""?    cart.increaseQuantity():cart.increaseWeightQuantity(double.parse("${product.productsLowestWeightAvailable.toString().replaceFirst("0", "")}")):increaseQuantity();


                              }, icon: Icon(FontAwesomeIcons.plus,size: 15,))),
                          Container(
                            width:EasyLocalization.of(context).currentLocale.languageCode =="en"?20.w: 18.w,
                            height: 5.7.h,
                            decoration: BoxDecoration(
                              // borderRadius: BorderRadius.circular(8),
                                border: Border.all(
                                    color: globals.accountColorsData['BorderColor']!=null
                                        ? Color(int.parse(globals.accountColorsData['BorderColor'])) : SystemControls.BorderColorz
                                )
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Consumer<CartStateProvider>(builder: (context,cart,_){
                                  return Text("${ product.productsLowestWeightAvailable!= null? product.productsLowestWeightAvailable ==""? cart.quantity.round() :cart.quantity:cart.quantity.round()}");
                                }),
                                SizedBox(width: 2.w,),
                                Text(
                                  product.productsType ==
                                      'weight'
                                      ? 'KG'.tr().toString()
                                      :'piece'.tr().toString(),
                                  style: TextStyle(fontSize: 9.sp),
                                ),

                              ],
                            ),
                          ),
                          Container(
                            height: 5.7.h,
                            decoration: BoxDecoration(
                                color: globals.accountColorsData[
                                'BorderColor'] !=
                                    null
                                    ? Color(int.parse(globals
                                    .accountColorsData['BorderColor']))
                                    : SystemControls.BorderColorz,
                                borderRadius: EasyLocalization.of(context).currentLocale.languageCode =="en"?BorderRadius.only(topRight: Radius.circular(8),bottomRight: Radius.circular(8)):BorderRadius.only(topLeft: Radius.circular(8),bottomLeft: Radius.circular(8))
                            ),
                            child: IconButton(onPressed: (){
                              product.productsLowestWeightAvailable!= null?    product.productsLowestWeightAvailable ==""?  cart.minusQuantity(): cart.minusWeightQuantity(double.parse("${product.productsLowestWeightAvailable.toString().replaceFirst("0", "")}")):minusQuantity();
                            }, icon: Center(child: Icon(FontAwesomeIcons.minus,size: 15,))),
                          ),

                          SizedBox(width: 1.w,),
                          Container(
                            width:EasyLocalization.of(context).currentLocale.languageCode =="en"? MediaQuery.of(context).size.width / 2.5:MediaQuery.of(context).size.width / 2.15,
                            child:

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                RichText(
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  text:  TextSpan(

                                    text: product.productsPriceAfterSale.isNotEmpty ? "${product.productsPriceAfterSale} $appCurrency" : "${product.productsPrice} $appCurrency",

                                    style: TextStyle(
                                        color:  globals.accountColorsData['MainColor'] != null
                                            ? Color(int.parse(globals.accountColorsData['MainColor']))
                                            : SystemControls.MainColorz,fontWeight: FontWeight.bold,fontSize: EasyLocalization.of(context).currentLocale.languageCode =="ar" ? 11.sp:13.5.sp
                                    ),
                                    children: <TextSpan>[
                                      TextSpan(text: '   '),
                                      product.productsPriceAfterSale.isNotEmpty ? TextSpan(
                                          text: '${product.productsPrice} $appCurrency',
                                          style: TextStyle(fontSize: EasyLocalization.of(context).currentLocale.languageCode =="ar" ? (9-.5).sp:11.sp,fontWeight: FontWeight.normal,decoration: TextDecoration.lineThrough,color: globals.accountColorsData['TextdetailsColor']!=null
                                              ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz)):TextSpan(text: ""),

                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Spacer(),
                        ],
                      ),
                    ),
                    Spacer(),
                    Container(
                      width: MediaQuery.of(context).size.width -60,
                      child: MaterialButton(
                        height: 48,
                        onPressed: (){
                          if(   product.choices != null &&  product.choices.length != 0 && selectedSize ==-1){
                            youNeedToSelectChoiceDialog(context);
                          }else
                          if(selectedSize !=-1 && product.choices.length >=1){
                            addToCartMethodProduct(product,context,cartList);
                            read_from_file();
                            quantity =1;
                          }else if(product.choices == null){
                            addToCartMethodProduct(product,context,cartList);
                            read_from_file();
                            quantity =1;
                          }else if(product.choices.length <=0){
                            addToCartMethodProduct(product,context,cartList);
                            read_from_file();
                            quantity =1;
                          }

                        },
                        color: globals.accountColorsData['MainColor'] != null
                            ? Color(int.parse(globals.accountColorsData['MainColor']))
                            : SystemControls.MainColorz,
                        child: Text("addToCart".tr().toString(),style: TextStyle(color: Colors.white),),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    Consumer<CartStateProvider>(
                      builder: (context, cart, _) {
                        return FutureBuilder<CartList>(
                          future: localCart,
                          builder: (context, Cartsnap) {
                            if (Cartsnap.connectionState ==
                                ConnectionState.done &&
                                Cartsnap.hasData) {
                              cart.setCartList(Cartsnap.data.Cmeals);

                              CartStateProvider cartState =
                              Provider.of<CartStateProvider>(context,
                                  listen: false);
                              if (cartState.productsCount == "0" &&
                                  cart.cartList.length != 0) {
                                cartState.setCurrentProductsCount(
                                    cart.cartList.length.toString());
                              }
                              return Container();
                            }
                            return Container();
                          },
                        );
                      },
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                  ],
                ),
              );
            });
          });
    }
   showAddToCartOffer(BuildContext context , var product,List<dynamic> cartList,String appCurrency,CartStateProvider cartStateProvider) {

     showModalBottomSheet(
         isScrollControlled: true,
         shape: RoundedRectangleBorder(
             borderRadius: BorderRadius.only(
                 topLeft: Radius.circular(15), topRight: Radius.circular(15))),
         context: context,
         builder: (context){
           cartStateProvider.changeCurrentQuantity(0);
           return Consumer<CartStateProvider>(builder: (context,cart,_){
          return   Container(
               padding: EdgeInsets.only(left: 12,top: 5,right: 12,bottom: 5),
               decoration: BoxDecoration(
                   borderRadius: BorderRadius.only(
                       topLeft: Radius.circular(15),
                       topRight: Radius.circular(15))),
               height: 40.h,

               child: Column(
                 crossAxisAlignment: CrossAxisAlignment.center,

                 children: [
                   SizedBox(
                     height: 5,
                   ),
                   Container(
                     height: 10,
                     width: 50.w,
                     decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(20),
                         color: Colors.grey.shade300
                     ),
                   ),
                   SizedBox(
                     height: 2.h,
                   ),
                   Text("${product.offersTitle}",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
                   Container(
                       width: MediaQuery.of(context).size.width -60,
                       child: Divider()),


                   Spacer(),
                   Container(
                     width: MediaQuery.of(context).size.width -50,
                     child:  Row(
                       children: [
                         Container(
                             height: 5.7.h,
                             decoration: BoxDecoration(
                                 color: globals.accountColorsData[
                                 'BorderColor'] !=
                                     null
                                     ? Color(int.parse(globals
                                     .accountColorsData['BorderColor']))
                                     : SystemControls.BorderColorz,
                                 borderRadius: EasyLocalization.of(context).currentLocale.languageCode =="ar"?BorderRadius.only(topRight: Radius.circular(8),bottomRight: Radius.circular(8)):BorderRadius.only(topLeft: Radius.circular(8),bottomLeft: Radius.circular(8))
                             ),
                             child: IconButton(onPressed: (){
                               cart.increaseQuantity();


                             }, icon: Icon(FontAwesomeIcons.plus,size: 15,))),
                         Container(
                           width:EasyLocalization.of(context).currentLocale.languageCode =="en"?20.w: 18.w,
                           height: 5.7.h,
                           decoration: BoxDecoration(
                             // borderRadius: BorderRadius.circular(8),
                               border: Border.all(
                                   color: globals.accountColorsData['BorderColor']!=null
                                       ? Color(int.parse(globals.accountColorsData['BorderColor'])) : SystemControls.BorderColorz
                               )
                           ),
                           child: Row(
                             crossAxisAlignment: CrossAxisAlignment.center,
                             mainAxisAlignment: MainAxisAlignment.center,
                             children: [
                               Consumer<CartStateProvider>(builder: (context,cart,_){
                                 return Text("${ cart.quantity}");
                               }),
                               SizedBox(width: 2.w,),
                               // Text(
                               //   product.productsType ==
                               //       'weight'
                               //       ? 'KG'.tr().toString()
                               //       :'piece'.tr().toString(),
                               //   style: TextStyle(fontSize: 9.sp),
                               // ),

                             ],
                           ),
                         ),
                         Container(
                           height: 5.7.h,
                           decoration: BoxDecoration(
                               color: globals.accountColorsData[
                               'BorderColor'] !=
                                   null
                                   ? Color(int.parse(globals
                                   .accountColorsData['BorderColor']))
                                   : SystemControls.BorderColorz,
                               borderRadius: EasyLocalization.of(context).currentLocale.languageCode =="en"?BorderRadius.only(topRight: Radius.circular(8),bottomRight: Radius.circular(8)):BorderRadius.only(topLeft: Radius.circular(8),bottomLeft: Radius.circular(8))
                           ),
                           child: IconButton(onPressed: (){
                             cart.minusQuantity();
                           }, icon: Center(child: Icon(FontAwesomeIcons.minus,size: 15,))),
                         ),

                         SizedBox(width: 1.w,),
                         Container(
                           width:EasyLocalization.of(context).currentLocale.languageCode =="en"? MediaQuery.of(context).size.width / 2.5:MediaQuery.of(context).size.width / 2.25,
                           child:
                           // product.productsPriceAfterSale != ""
                           //     ? Row(
                           //   crossAxisAlignment: CrossAxisAlignment.center,
                           //   mainAxisAlignment: MainAxisAlignment.end,
                           //   children: [
                           //     Text(
                           //         "${product.productsPrice} $appCurrency",
                           //         overflow: TextOverflow.ellipsis,
                           //         style: TextStyle(
                           //
                           //           decoration: TextDecoration.lineThrough,
                           //           fontWeight: FontWeight.bold,
                           //           fontSize: EasyLocalization.of(context).currentLocale.languageCode =="en"?10.sp: 8.5.sp ,color: globals.accountColorsData['TextdetailsColor'] !=
                           //             null
                           //             ? Color(int.parse(globals
                           //             .accountColorsData['TextdetailsColor']))
                           //             : SystemControls.TextdetailsColorz,)),
                           //         SizedBox(width: 2.w,),
                           //     Text(
                           //         "${product.productsPriceAfterSale} $appCurrency",
                           //         overflow: TextOverflow.ellipsis,
                           //         style: TextStyle(
                           //             fontWeight: FontWeight.bold,
                           //             fontSize:EasyLocalization.of(context).currentLocale.languageCode =="en"?10.sp: 8.5.sp,color: globals.accountColorsData[
                           //         'MainColor'] !=
                           //             null
                           //             ? Color(int.parse(
                           //             globals.accountColorsData[
                           //             'MainColor']))
                           //             : SystemControls
                           //             .MainColorz)),
                           //   ],
                           // )
                           //     : Text("${product.productsPrice}"),
                           Row(
                             crossAxisAlignment: CrossAxisAlignment.center,
                             mainAxisAlignment: MainAxisAlignment.end,
                             children: [
                               Text(
                                   '${product.offersPrice} $appCurrency',
                                   style: TextStyle(fontSize: EasyLocalization.of(context).currentLocale.languageCode =="ar" ? (9-.5).sp:11.sp,fontWeight: FontWeight.normal,decoration: TextDecoration.lineThrough,color: globals.accountColorsData['TextdetailsColor']!=null
                                       ? Color(int.parse(globals.accountColorsData['TextdetailsColor'])) : SystemControls.TextdetailsColorz)
                               )
                             ],
                           ),
                         ),
                         Spacer(),
                       ],
                     ),
                   ),
                   Spacer(),
                   Container(
                     width: MediaQuery.of(context).size.width -60,
                     child: MaterialButton(
                       height: 48,
                       onPressed: (){
                         AddOfferToCart(product,context);
                         read_from_file();
                         quantity =1;
                       },
                       color: globals.accountColorsData['MainColor'] != null
                           ? Color(int.parse(globals.accountColorsData['MainColor']))
                           : SystemControls.MainColorz,
                       child: Text("addToCart".tr().toString(),style: TextStyle(color: Colors.white),),
                       shape: RoundedRectangleBorder(
                         borderRadius: BorderRadius.circular(10),
                       ),
                     ),
                   ),
                   Consumer<CartStateProvider>(
                     builder: (context, cart, _) {
                       return FutureBuilder<CartList>(
                         future: localCart,
                         builder: (context, Cartsnap) {
                           if (Cartsnap.connectionState ==
                               ConnectionState.done &&
                               Cartsnap.hasData) {
                             cart.setCartList(Cartsnap.data.Cmeals);

                             CartStateProvider cartState =
                             Provider.of<CartStateProvider>(context,
                                 listen: false);
                             if (cartState.productsCount == "0" &&
                                 cart.cartList.length != 0) {
                               cartState.setCurrentProductsCount(
                                   cart.cartList.length.toString());
                             }
                             return Container();
                           }
                           return Container();
                         },
                       );
                     },
                   ),
                   SizedBox(
                     height: 2.h,
                   ),
                 ],
               ),
             );
           },);
         });
   }
    final getCart = BehaviorSubject<CartList>();
    Stream get streamCard => getCart.stream;
    fetchCardNum() async {
      CartList _notificationList =
      await read_from_file();


      print("_notificationList.Cmeals");
      print(await read_from_file());

      getCart.sink.add(_notificationList);
      notifyListeners();
    }

    @override
  void dispose() {
    // TODO: implement dispose
      getCart.close();
    super.dispose();
  }
  }