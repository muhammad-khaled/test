



import '../../Models/DrawerModel/FavoriteModel.dart';
import '../../provider/productsInCartProvider.dart';
import '../../services/DrawerService/DrawerRepo.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class DrawerProvider extends CartStateProvider{

  final getFavoriteList = BehaviorSubject<FavoriteModel>();
  Stream get streamFavorite=> getFavoriteList.stream;
  fetchFavoriteProduct(BuildContext context) async {
    print('fffffffffccc');
    FavoriteModel _favoriteList =
    await UserFavoriteRepo(context: context)
        .getFavoriteItems;
    getFavoriteList.sink.add(_favoriteList);
    notifyListeners();
  }





@override
  void dispose() {
    
    getFavoriteList.close();
    // TODO: implement dispose
    super.dispose();
  }
}